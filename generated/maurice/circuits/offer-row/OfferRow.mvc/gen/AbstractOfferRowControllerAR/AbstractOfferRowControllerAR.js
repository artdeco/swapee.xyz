import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowControllerAR}
 */
function __AbstractOfferRowControllerAR() {}
__AbstractOfferRowControllerAR.prototype = /** @type {!_AbstractOfferRowControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
class _AbstractOfferRowControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOfferRowControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOfferRowControllerAR} ‎
 */
class AbstractOfferRowControllerAR extends newAbstract(
 _AbstractOfferRowControllerAR,'IOfferRowControllerAR',null,{
  asIOfferRowControllerAR:1,
  superOfferRowControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR} */
AbstractOfferRowControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR} */
function AbstractOfferRowControllerARClass(){}

export default AbstractOfferRowControllerAR


AbstractOfferRowControllerAR[$implementations]=[
 __AbstractOfferRowControllerAR,
 AR,
 AbstractOfferRowControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferRowControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]