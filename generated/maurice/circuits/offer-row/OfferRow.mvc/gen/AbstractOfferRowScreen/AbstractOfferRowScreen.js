import OfferRowClassesPQs from '../../pqs/OfferRowClassesPQs'
import AbstractOfferRowScreenAR from '../AbstractOfferRowScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OfferRowInputsPQs} from '../../pqs/OfferRowInputsPQs'
import {OfferRowMemoryQPs} from '../../pqs/OfferRowMemoryQPs'
import {OfferRowVdusPQs} from '../../pqs/OfferRowVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowScreen}
 */
function __AbstractOfferRowScreen() {}
__AbstractOfferRowScreen.prototype = /** @type {!_AbstractOfferRowScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowScreen}
 */
class _AbstractOfferRowScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractOfferRowScreen} ‎
 */
class AbstractOfferRowScreen extends newAbstract(
 _AbstractOfferRowScreen,'IOfferRowScreen',null,{
  asIOfferRowScreen:1,
  superOfferRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowScreen} */
AbstractOfferRowScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowScreen} */
function AbstractOfferRowScreenClass(){}

export default AbstractOfferRowScreen


AbstractOfferRowScreen[$implementations]=[
 __AbstractOfferRowScreen,
 AbstractOfferRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowScreen}*/({
  inputsPQs:OfferRowInputsPQs,
  classesPQs:OfferRowClassesPQs,
  memoryQPs:OfferRowMemoryQPs,
 }),
 Screen,
 AbstractOfferRowScreenAR,
 AbstractOfferRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowScreen}*/({
  vdusPQs:OfferRowVdusPQs,
 }),
]