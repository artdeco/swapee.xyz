import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferRowElementPort}
 */
function __OfferRowElementPort() {}
__OfferRowElementPort.prototype = /** @type {!_OfferRowElementPort} */ ({ })
/** @this {xyz.swapee.wc.OfferRowElementPort} */ function OfferRowElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IOfferRowElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    fixed: false,
    recommended: false,
    exchangeCollapsarOpts: {},
    offerExchangeOpts: {},
    bestOfferPlaqueOpts: {},
    progressVertLineOpts: {},
    coinImWrOpts: {},
    getDealLaOpts: {},
    cancelLaOpts: {},
    getDealBuAOpts: {},
    offerCryptoOpts: {},
    offerAmountOpts: {},
    logoOpts: {},
    etaOpts: {},
    getDealOpts: {},
    recHandleOpts: {},
    recPopupOpts: {},
    floatingIcoOpts: {},
    fixedIcoOpts: {},
    coinImOpts: {},
    floatingLaOpts: {},
    fixedLaOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowElementPort}
 */
class _OfferRowElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractOfferRowElementPort} ‎
 */
class OfferRowElementPort extends newAbstract(
 _OfferRowElementPort,'IOfferRowElementPort',OfferRowElementPortConstructor,{
  asIOfferRowElementPort:1,
  superOfferRowElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowElementPort} */
OfferRowElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowElementPort} */
function OfferRowElementPortClass(){}

export default OfferRowElementPort


OfferRowElementPort[$implementations]=[
 __OfferRowElementPort,
 OfferRowElementPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'exchange-collapsar-opts':undefined,
    'offer-exchange-opts':undefined,
    'best-offer-plaque-opts':undefined,
    'progress-vert-line-opts':undefined,
    'coin-im-wr-opts':undefined,
    'get-deal-la-opts':undefined,
    'cancel-la-opts':undefined,
    'get-deal-bu-a-opts':undefined,
    'offer-crypto-opts':undefined,
    'offer-amount-opts':undefined,
    'logo-opts':undefined,
    'eta-opts':undefined,
    'get-deal-opts':undefined,
    'rec-handle-opts':undefined,
    'rec-popup-opts':undefined,
    'floating-ico-opts':undefined,
    'fixed-ico-opts':undefined,
    'coin-im-opts':undefined,
    'floating-la-opts':undefined,
    'fixed-la-opts':undefined,
   })
  },
 }),
]