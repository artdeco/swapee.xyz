import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowControllerAT}
 */
function __AbstractOfferRowControllerAT() {}
__AbstractOfferRowControllerAT.prototype = /** @type {!_AbstractOfferRowControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
class _AbstractOfferRowControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferRowControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOfferRowControllerAT} ‎
 */
class AbstractOfferRowControllerAT extends newAbstract(
 _AbstractOfferRowControllerAT,'IOfferRowControllerAT',null,{
  asIOfferRowControllerAT:1,
  superOfferRowControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT} */
AbstractOfferRowControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT} */
function AbstractOfferRowControllerATClass(){}

export default AbstractOfferRowControllerAT


AbstractOfferRowControllerAT[$implementations]=[
 __AbstractOfferRowControllerAT,
 UartUniversal,
 AbstractOfferRowControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferRowControllerAT}*/({
  get asIOfferRowController(){
   return this
  },
 }),
]