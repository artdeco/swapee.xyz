import {mountPins} from '@type.engineering/seers'
import {OfferRowMemoryPQs} from '../../pqs/OfferRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferRowCore}
 */
function __OfferRowCore() {}
__OfferRowCore.prototype = /** @type {!_OfferRowCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowCore}
 */
class _OfferRowCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOfferRowCore} ‎
 */
class OfferRowCore extends newAbstract(
 _OfferRowCore,'IOfferRowCore',null,{
  asIOfferRowCore:1,
  superOfferRowCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowCore} */
OfferRowCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowCore} */
function OfferRowCoreClass(){}

export default OfferRowCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferRowOuterCore}
 */
function __OfferRowOuterCore() {}
__OfferRowOuterCore.prototype = /** @type {!_OfferRowOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OfferRowOuterCore} */
export function OfferRowOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOfferRowOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
class _OfferRowOuterCore { }
/**
 * The _IOfferRow_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOfferRowOuterCore} ‎
 */
export class OfferRowOuterCore extends newAbstract(
 _OfferRowOuterCore,'IOfferRowOuterCore',OfferRowOuterCoreConstructor,{
  asIOfferRowOuterCore:1,
  superOfferRowOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowOuterCore} */
OfferRowOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowOuterCore} */
function OfferRowOuterCoreClass(){}


OfferRowOuterCore[$implementations]=[
 __OfferRowOuterCore,
 OfferRowOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowOuterCore}*/({
  constructor(){
   mountPins(this.model,OfferRowMemoryPQs)

  },
 }),
]

OfferRowCore[$implementations]=[
 OfferRowCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowCore}*/({
  resetCore(){
   this.resetOfferRowCore()
  },
  resetOfferRowCore(){
   OfferRowOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OfferRowOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOfferRowOuterCore}*/(this)),
   )
  },
 }),
 __OfferRowCore,
 OfferRowOuterCore,
]

export {OfferRowCore}