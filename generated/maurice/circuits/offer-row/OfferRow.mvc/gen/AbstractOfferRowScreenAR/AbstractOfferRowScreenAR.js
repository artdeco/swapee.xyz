import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowScreenAR}
 */
function __AbstractOfferRowScreenAR() {}
__AbstractOfferRowScreenAR.prototype = /** @type {!_AbstractOfferRowScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
class _AbstractOfferRowScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOfferRowScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOfferRowScreenAR} ‎
 */
class AbstractOfferRowScreenAR extends newAbstract(
 _AbstractOfferRowScreenAR,'IOfferRowScreenAR',null,{
  asIOfferRowScreenAR:1,
  superOfferRowScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR} */
AbstractOfferRowScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR} */
function AbstractOfferRowScreenARClass(){}

export default AbstractOfferRowScreenAR


AbstractOfferRowScreenAR[$implementations]=[
 __AbstractOfferRowScreenAR,
 AR,
 AbstractOfferRowScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferRowScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOfferRowScreenAR}