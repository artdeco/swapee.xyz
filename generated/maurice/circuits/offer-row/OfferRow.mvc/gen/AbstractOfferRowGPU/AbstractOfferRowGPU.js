import AbstractOfferRowDisplay from '../AbstractOfferRowDisplayBack'
import OfferRowClassesPQs from '../../pqs/OfferRowClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {OfferRowClassesQPs} from '../../pqs/OfferRowClassesQPs'
import {OfferRowVdusPQs} from '../../pqs/OfferRowVdusPQs'
import {OfferRowVdusQPs} from '../../pqs/OfferRowVdusQPs'
import {OfferRowMemoryPQs} from '../../pqs/OfferRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowGPU}
 */
function __AbstractOfferRowGPU() {}
__AbstractOfferRowGPU.prototype = /** @type {!_AbstractOfferRowGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowGPU}
 */
class _AbstractOfferRowGPU { }
/**
 * Handles the periphery of the _IOfferRowDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOfferRowGPU} ‎
 */
class AbstractOfferRowGPU extends newAbstract(
 _AbstractOfferRowGPU,'IOfferRowGPU',null,{
  asIOfferRowGPU:1,
  superOfferRowGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowGPU} */
AbstractOfferRowGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowGPU} */
function AbstractOfferRowGPUClass(){}

export default AbstractOfferRowGPU


AbstractOfferRowGPU[$implementations]=[
 __AbstractOfferRowGPU,
 AbstractOfferRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowGPU}*/({
  classesQPs:OfferRowClassesQPs,
  vdusPQs:OfferRowVdusPQs,
  vdusQPs:OfferRowVdusQPs,
  memoryPQs:OfferRowMemoryPQs,
 }),
 AbstractOfferRowDisplay,
 BrowserView,
 AbstractOfferRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowGPU}*/({
  allocator(){
   pressFit(this.classes,'',OfferRowClassesPQs)
  },
 }),
]