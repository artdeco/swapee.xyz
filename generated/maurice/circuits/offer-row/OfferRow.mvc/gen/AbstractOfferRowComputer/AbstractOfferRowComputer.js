import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowComputer}
 */
function __AbstractOfferRowComputer() {}
__AbstractOfferRowComputer.prototype = /** @type {!_AbstractOfferRowComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowComputer}
 */
class _AbstractOfferRowComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOfferRowComputer} ‎
 */
export class AbstractOfferRowComputer extends newAbstract(
 _AbstractOfferRowComputer,'IOfferRowComputer',null,{
  asIOfferRowComputer:1,
  superOfferRowComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowComputer} */
AbstractOfferRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowComputer} */
function AbstractOfferRowComputerClass(){}


AbstractOfferRowComputer[$implementations]=[
 __AbstractOfferRowComputer,
 Adapter,
]


export default AbstractOfferRowComputer