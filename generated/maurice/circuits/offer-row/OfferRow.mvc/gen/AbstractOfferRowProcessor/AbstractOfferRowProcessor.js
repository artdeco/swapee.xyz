import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowProcessor}
 */
function __AbstractOfferRowProcessor() {}
__AbstractOfferRowProcessor.prototype = /** @type {!_AbstractOfferRowProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowProcessor}
 */
class _AbstractOfferRowProcessor { }
/**
 * The processor to compute changes to the memory for the _IOfferRow_.
 * @extends {xyz.swapee.wc.AbstractOfferRowProcessor} ‎
 */
class AbstractOfferRowProcessor extends newAbstract(
 _AbstractOfferRowProcessor,'IOfferRowProcessor',null,{
  asIOfferRowProcessor:1,
  superOfferRowProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowProcessor} */
AbstractOfferRowProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowProcessor} */
function AbstractOfferRowProcessorClass(){}

export default AbstractOfferRowProcessor


AbstractOfferRowProcessor[$implementations]=[
 __AbstractOfferRowProcessor,
]