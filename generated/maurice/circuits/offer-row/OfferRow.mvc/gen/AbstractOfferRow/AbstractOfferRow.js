import AbstractOfferRowProcessor from '../AbstractOfferRowProcessor'
import {OfferRowCore} from '../OfferRowCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOfferRowComputer} from '../AbstractOfferRowComputer'
import {AbstractOfferRowController} from '../AbstractOfferRowController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRow}
 */
function __AbstractOfferRow() {}
__AbstractOfferRow.prototype = /** @type {!_AbstractOfferRow} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRow}
 */
class _AbstractOfferRow { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOfferRow} ‎
 */
class AbstractOfferRow extends newAbstract(
 _AbstractOfferRow,'IOfferRow',null,{
  asIOfferRow:1,
  superOfferRow:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRow} */
AbstractOfferRow.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRow} */
function AbstractOfferRowClass(){}

export default AbstractOfferRow


AbstractOfferRow[$implementations]=[
 __AbstractOfferRow,
 OfferRowCore,
 AbstractOfferRowProcessor,
 IntegratedComponent,
 AbstractOfferRowComputer,
 AbstractOfferRowController,
]


export {AbstractOfferRow}