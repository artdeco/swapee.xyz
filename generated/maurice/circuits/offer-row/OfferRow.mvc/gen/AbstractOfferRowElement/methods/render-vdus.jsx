export default function OfferRowRenderVdus(){
 return (<div $id="OfferRow">
  <vdu $id="ExchangeCollapsar" />
  <vdu $id="OfferExchange" />
  <vdu $id="BestOfferPlaque" />
  <vdu $id="ProgressVertLine" />
  <vdu $id="CoinImWr" />
  <vdu $id="GetDealLa" />
  <vdu $id="CancelLa" />
  <vdu $id="GetDealBuA" />
  <vdu $id="OfferCrypto" />
  <vdu $id="OfferAmount" />
  <vdu $id="Logo" />
  <vdu $id="Eta" />
  <vdu $id="GetDeal" />
  <vdu $id="RecHandle" />
  <vdu $id="RecPopup" />
  <vdu $id="FloatingIco" />
  <vdu $id="FixedIco" />
  <vdu $id="CoinIm" />
  <vdu $id="FloatingLa" />
  <vdu $id="FixedLa" />
 </div>)
}