import OfferRowRenderVdus from './methods/render-vdus'
import OfferRowElementPort from '../OfferRowElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {OfferRowInputsPQs} from '../../pqs/OfferRowInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractOfferRow from '../AbstractOfferRow'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowElement}
 */
function __AbstractOfferRowElement() {}
__AbstractOfferRowElement.prototype = /** @type {!_AbstractOfferRowElement} */ ({ })
/** @this {xyz.swapee.wc.OfferRowElement} */ function OfferRowElementConstructor() {
  /** @type {string} */ this.X_EXCHANGE_COLLAPSAR_HANDLE='x-exchange-collapsar-handle'
  /** @type {string} */ this.X_RECOMMENDATION_INFO_HANDLE='x-recommendation-info-handle'
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowElement}
 */
class _AbstractOfferRowElement { }
/**
 * A component description.
 *
 * The _IOfferRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOfferRowElement} ‎
 */
class AbstractOfferRowElement extends newAbstract(
 _AbstractOfferRowElement,'IOfferRowElement',OfferRowElementConstructor,{
  asIOfferRowElement:1,
  superOfferRowElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowElement} */
AbstractOfferRowElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowElement} */
function AbstractOfferRowElementClass(){}

export default AbstractOfferRowElement


AbstractOfferRowElement[$implementations]=[
 __AbstractOfferRowElement,
 ElementBase,
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':fixed':fixedColAttr,
   ':recommended':recommendedColAttr,
   ':core':coreColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'fixed':fixedAttr,
    'recommended':recommendedAttr,
    'core':coreAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(fixedAttr===undefined?{'fixed':fixedColAttr}:{}),
    ...(recommendedAttr===undefined?{'recommended':recommendedColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
   }
  },
 }),
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'fixed':fixedAttr,
   'recommended':recommendedAttr,
   'core':coreAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    fixed:fixedAttr,
    recommended:recommendedAttr,
    core:coreAttr,
   }
  },
 }),
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  render:OfferRowRenderVdus,
 }),
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  classes:{
   'BestOffer': 'e8977',
   'Recommended': '65486',
   'ProgressVertLine': 'a7a04',
  },
  inputsPQs:OfferRowInputsPQs,
  vdus:{
   'OfferCrypto': 'h8a71',
   'OfferAmount': 'h8a72',
   'Eta': 'h8a75',
   'FloatingIco': 'h8a76',
   'FixedIco': 'h8a77',
   'FloatingLa': 'h8a78',
   'FixedLa': 'h8a79',
   'RecHandle': 'h8a710',
   'RecPopup': 'h8a711',
   'Logo': 'h8a712',
   'CoinIm': 'h8a713',
   'ExchangeCollapsar': 'h8a715',
   'GetDeal': 'h8a716',
   'GetDealLa': 'h8a717',
   'CancelLa': 'h8a718',
   'CoinImWr': 'h8a719',
   'BestOfferPlaque': 'h8a720',
   'ProgressVertLine': 'h8a721',
   'OfferExchange': 'h8a722',
   'GetDealBuA': 'h8a723',
  },
 }),
 IntegratedComponent,
 AbstractOfferRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','fixed','recommended','core','no-solder',':no-solder',':fixed',':recommended',':core','fe646','cec31','0aef2','a5d67','09b03','6497b','4971c','c35d8','928d8','fa5c0','f01d0','8e449','b679b','33629','ef570','6975f','8e283','93e03','40f16','5eba3','1a8cc','062ec','da65e','a74ad','children']),
   })
  },
  get Port(){
   return OfferRowElementPort
  },
 }),
]



AbstractOfferRowElement[$implementations]=[AbstractOfferRow,
 /** @type {!AbstractOfferRowElement} */ ({
  rootId:'OfferRow',
  __$id:8085433869,
  fqn:'xyz.swapee.wc.IOfferRow',
  maurice_element_v3:true,
 }),
]