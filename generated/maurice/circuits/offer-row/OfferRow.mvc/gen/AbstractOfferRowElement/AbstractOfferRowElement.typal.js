
import AbstractOfferRow from '../AbstractOfferRow'

/** @abstract {xyz.swapee.wc.IOfferRowElement} */
export default class AbstractOfferRowElement { }



AbstractOfferRowElement[$implementations]=[AbstractOfferRow,
 /** @type {!AbstractOfferRowElement} */ ({
  rootId:'OfferRow',
  __$id:8085433869,
  fqn:'xyz.swapee.wc.IOfferRow',
  maurice_element_v3:true,
 }),
]