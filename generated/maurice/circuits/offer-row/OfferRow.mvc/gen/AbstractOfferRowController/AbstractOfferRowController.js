import OfferRowBuffer from '../OfferRowBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OfferRowPortConnector} from '../OfferRowPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowController}
 */
function __AbstractOfferRowController() {}
__AbstractOfferRowController.prototype = /** @type {!_AbstractOfferRowController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowController}
 */
class _AbstractOfferRowController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOfferRowController} ‎
 */
export class AbstractOfferRowController extends newAbstract(
 _AbstractOfferRowController,'IOfferRowController',null,{
  asIOfferRowController:1,
  superOfferRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowController} */
AbstractOfferRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowController} */
function AbstractOfferRowControllerClass(){}


AbstractOfferRowController[$implementations]=[
 AbstractOfferRowControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOfferRowPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOfferRowController,
 OfferRowBuffer,
 IntegratedController,
 /**@type {!AbstractOfferRowController}*/(OfferRowPortConnector),
]


export default AbstractOfferRowController