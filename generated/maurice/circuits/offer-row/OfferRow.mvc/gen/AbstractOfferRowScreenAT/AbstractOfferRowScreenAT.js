import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowScreenAT}
 */
function __AbstractOfferRowScreenAT() {}
__AbstractOfferRowScreenAT.prototype = /** @type {!_AbstractOfferRowScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
class _AbstractOfferRowScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferRowScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOfferRowScreenAT} ‎
 */
class AbstractOfferRowScreenAT extends newAbstract(
 _AbstractOfferRowScreenAT,'IOfferRowScreenAT',null,{
  asIOfferRowScreenAT:1,
  superOfferRowScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT} */
AbstractOfferRowScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT} */
function AbstractOfferRowScreenATClass(){}

export default AbstractOfferRowScreenAT


AbstractOfferRowScreenAT[$implementations]=[
 __AbstractOfferRowScreenAT,
 UartUniversal,
]