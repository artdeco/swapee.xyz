import AbstractOfferRowControllerAR from '../AbstractOfferRowControllerAR'
import {AbstractOfferRowController} from '../AbstractOfferRowController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowControllerBack}
 */
function __AbstractOfferRowControllerBack() {}
__AbstractOfferRowControllerBack.prototype = /** @type {!_AbstractOfferRowControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferRowController}
 */
class _AbstractOfferRowControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferRowController} ‎
 */
class AbstractOfferRowControllerBack extends newAbstract(
 _AbstractOfferRowControllerBack,'IOfferRowController',null,{
  asIOfferRowController:1,
  superOfferRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowController} */
AbstractOfferRowControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowController} */
function AbstractOfferRowControllerBackClass(){}

export default AbstractOfferRowControllerBack


AbstractOfferRowControllerBack[$implementations]=[
 __AbstractOfferRowControllerBack,
 AbstractOfferRowController,
 AbstractOfferRowControllerAR,
 DriverBack,
]