import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OfferRowInputsPQs} from '../../pqs/OfferRowInputsPQs'
import {OfferRowOuterCoreConstructor} from '../OfferRowCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferRowPort}
 */
function __OfferRowPort() {}
__OfferRowPort.prototype = /** @type {!_OfferRowPort} */ ({ })
/** @this {xyz.swapee.wc.OfferRowPort} */ function OfferRowPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OfferRowOuterCore} */ ({model:null})
  OfferRowOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowPort}
 */
class _OfferRowPort { }
/**
 * The port that serves as an interface to the _IOfferRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOfferRowPort} ‎
 */
export class OfferRowPort extends newAbstract(
 _OfferRowPort,'IOfferRowPort',OfferRowPortConstructor,{
  asIOfferRowPort:1,
  superOfferRowPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowPort} */
OfferRowPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowPort} */
function OfferRowPortClass(){}

export const OfferRowPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOfferRow.Pinout>}*/({
 get Port() { return OfferRowPort },
})

OfferRowPort[$implementations]=[
 OfferRowPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowPort}*/({
  resetPort(){
   this.resetOfferRowPort()
  },
  resetOfferRowPort(){
   OfferRowPortConstructor.call(this)
  },
 }),
 __OfferRowPort,
 Parametric,
 OfferRowPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowPort}*/({
  constructor(){
   mountPins(this.inputs,OfferRowInputsPQs)
  },
 }),
]


export default OfferRowPort