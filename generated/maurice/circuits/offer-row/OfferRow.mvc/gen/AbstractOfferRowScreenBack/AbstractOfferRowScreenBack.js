import AbstractOfferRowScreenAT from '../AbstractOfferRowScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowScreenBack}
 */
function __AbstractOfferRowScreenBack() {}
__AbstractOfferRowScreenBack.prototype = /** @type {!_AbstractOfferRowScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
class _AbstractOfferRowScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferRowScreen} ‎
 */
class AbstractOfferRowScreenBack extends newAbstract(
 _AbstractOfferRowScreenBack,'IOfferRowScreen',null,{
  asIOfferRowScreen:1,
  superOfferRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreen} */
AbstractOfferRowScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreen} */
function AbstractOfferRowScreenBackClass(){}

export default AbstractOfferRowScreenBack


AbstractOfferRowScreenBack[$implementations]=[
 __AbstractOfferRowScreenBack,
 AbstractOfferRowScreenAT,
]