import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowDisplay}
 */
function __AbstractOfferRowDisplay() {}
__AbstractOfferRowDisplay.prototype = /** @type {!_AbstractOfferRowDisplay} */ ({ })
/** @this {xyz.swapee.wc.OfferRowDisplay} */ function OfferRowDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.ExchangeCollapsar=null
  /** @type {HTMLDivElement} */ this.OfferExchange=null
  /** @type {HTMLDivElement} */ this.BestOfferPlaque=null
  /** @type {HTMLSpanElement} */ this.ProgressVertLine=null
  /** @type {HTMLDivElement} */ this.CoinImWr=null
  /** @type {HTMLSpanElement} */ this.GetDealLa=null
  /** @type {HTMLSpanElement} */ this.CancelLa=null
  /** @type {HTMLAnchorElement} */ this.GetDealBuA=null
  /** @type {HTMLSpanElement} */ this.OfferCrypto=null
  /** @type {HTMLSpanElement} */ this.OfferAmount=null
  /** @type {HTMLImageElement} */ this.Logo=null
  /** @type {HTMLSpanElement} */ this.Eta=null
  /** @type {HTMLDivElement} */ this.GetDeal=null
  /** @type {HTMLSpanElement} */ this.RecHandle=null
  /** @type {HTMLSpanElement} */ this.RecPopup=null
  /** @type {HTMLImageElement} */ this.FloatingIco=null
  /** @type {HTMLImageElement} */ this.FixedIco=null
  /** @type {HTMLElement} */ this.CoinIm=null
  /** @type {HTMLParagraphElement} */ this.FloatingLa=null
  /** @type {HTMLParagraphElement} */ this.FixedLa=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowDisplay}
 */
class _AbstractOfferRowDisplay { }
/**
 * Display for presenting information from the _IOfferRow_.
 * @extends {xyz.swapee.wc.AbstractOfferRowDisplay} ‎
 */
class AbstractOfferRowDisplay extends newAbstract(
 _AbstractOfferRowDisplay,'IOfferRowDisplay',OfferRowDisplayConstructor,{
  asIOfferRowDisplay:1,
  superOfferRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowDisplay} */
AbstractOfferRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowDisplay} */
function AbstractOfferRowDisplayClass(){}

export default AbstractOfferRowDisplay


AbstractOfferRowDisplay[$implementations]=[
 __AbstractOfferRowDisplay,
 Display,
 AbstractOfferRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIOfferRowScreen:{vdusPQs:{
    ExchangeCollapsar:ExchangeCollapsar,
    OfferExchange:OfferExchange,
    BestOfferPlaque:BestOfferPlaque,
    ProgressVertLine:ProgressVertLine,
    CoinImWr:CoinImWr,
    GetDealLa:GetDealLa,
    CancelLa:CancelLa,
    GetDealBuA:GetDealBuA,
    OfferCrypto:OfferCrypto,
    OfferAmount:OfferAmount,
    Logo:Logo,
    Eta:Eta,
    GetDeal:GetDeal,
    RecHandle:RecHandle,
    RecPopup:RecPopup,
    FloatingIco:FloatingIco,
    FixedIco:FixedIco,
    CoinIm:CoinIm,
    FloatingLa:FloatingLa,
    FixedLa:FixedLa,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    ExchangeCollapsar:/**@type {HTMLDivElement}*/(children[ExchangeCollapsar]),
    OfferExchange:/**@type {HTMLDivElement}*/(children[OfferExchange]),
    BestOfferPlaque:/**@type {HTMLDivElement}*/(children[BestOfferPlaque]),
    ProgressVertLine:/**@type {HTMLSpanElement}*/(children[ProgressVertLine]),
    CoinImWr:/**@type {HTMLDivElement}*/(children[CoinImWr]),
    GetDealLa:/**@type {HTMLSpanElement}*/(children[GetDealLa]),
    CancelLa:/**@type {HTMLSpanElement}*/(children[CancelLa]),
    GetDealBuA:/**@type {HTMLAnchorElement}*/(children[GetDealBuA]),
    OfferCrypto:/**@type {HTMLSpanElement}*/(children[OfferCrypto]),
    OfferAmount:/**@type {HTMLSpanElement}*/(children[OfferAmount]),
    Logo:/**@type {HTMLImageElement}*/(children[Logo]),
    Eta:/**@type {HTMLSpanElement}*/(children[Eta]),
    GetDeal:/**@type {HTMLDivElement}*/(children[GetDeal]),
    RecHandle:/**@type {HTMLSpanElement}*/(children[RecHandle]),
    RecPopup:/**@type {HTMLSpanElement}*/(children[RecPopup]),
    FloatingIco:/**@type {HTMLImageElement}*/(children[FloatingIco]),
    FixedIco:/**@type {HTMLImageElement}*/(children[FixedIco]),
    CoinIm:/**@type {HTMLElement}*/(children[CoinIm]),
    FloatingLa:/**@type {HTMLParagraphElement}*/(children[FloatingLa]),
    FixedLa:/**@type {HTMLParagraphElement}*/(children[FixedLa]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OfferRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOfferRowDisplay.Initialese}*/({
   ExchangeCollapsar:1,
   OfferExchange:1,
   BestOfferPlaque:1,
   ProgressVertLine:1,
   CoinImWr:1,
   GetDealLa:1,
   CancelLa:1,
   GetDealBuA:1,
   OfferCrypto:1,
   OfferAmount:1,
   Logo:1,
   Eta:1,
   GetDeal:1,
   RecHandle:1,
   RecPopup:1,
   FloatingIco:1,
   FixedIco:1,
   CoinIm:1,
   FloatingLa:1,
   FixedLa:1,
  }),
  initializer({
   ExchangeCollapsar:_ExchangeCollapsar,
   OfferExchange:_OfferExchange,
   BestOfferPlaque:_BestOfferPlaque,
   ProgressVertLine:_ProgressVertLine,
   CoinImWr:_CoinImWr,
   GetDealLa:_GetDealLa,
   CancelLa:_CancelLa,
   GetDealBuA:_GetDealBuA,
   OfferCrypto:_OfferCrypto,
   OfferAmount:_OfferAmount,
   Logo:_Logo,
   Eta:_Eta,
   GetDeal:_GetDeal,
   RecHandle:_RecHandle,
   RecPopup:_RecPopup,
   FloatingIco:_FloatingIco,
   FixedIco:_FixedIco,
   CoinIm:_CoinIm,
   FloatingLa:_FloatingLa,
   FixedLa:_FixedLa,
  }) {
   if(_ExchangeCollapsar!==undefined) this.ExchangeCollapsar=_ExchangeCollapsar
   if(_OfferExchange!==undefined) this.OfferExchange=_OfferExchange
   if(_BestOfferPlaque!==undefined) this.BestOfferPlaque=_BestOfferPlaque
   if(_ProgressVertLine!==undefined) this.ProgressVertLine=_ProgressVertLine
   if(_CoinImWr!==undefined) this.CoinImWr=_CoinImWr
   if(_GetDealLa!==undefined) this.GetDealLa=_GetDealLa
   if(_CancelLa!==undefined) this.CancelLa=_CancelLa
   if(_GetDealBuA!==undefined) this.GetDealBuA=_GetDealBuA
   if(_OfferCrypto!==undefined) this.OfferCrypto=_OfferCrypto
   if(_OfferAmount!==undefined) this.OfferAmount=_OfferAmount
   if(_Logo!==undefined) this.Logo=_Logo
   if(_Eta!==undefined) this.Eta=_Eta
   if(_GetDeal!==undefined) this.GetDeal=_GetDeal
   if(_RecHandle!==undefined) this.RecHandle=_RecHandle
   if(_RecPopup!==undefined) this.RecPopup=_RecPopup
   if(_FloatingIco!==undefined) this.FloatingIco=_FloatingIco
   if(_FixedIco!==undefined) this.FixedIco=_FixedIco
   if(_CoinIm!==undefined) this.CoinIm=_CoinIm
   if(_FloatingLa!==undefined) this.FloatingLa=_FloatingLa
   if(_FixedLa!==undefined) this.FixedLa=_FixedLa
  },
 }),
]