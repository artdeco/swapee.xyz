import AbstractOfferRowGPU from '../AbstractOfferRowGPU'
import AbstractOfferRowScreenBack from '../AbstractOfferRowScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {OfferRowInputsQPs} from '../../pqs/OfferRowInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractOfferRow from '../AbstractOfferRow'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowHtmlComponent}
 */
function __AbstractOfferRowHtmlComponent() {}
__AbstractOfferRowHtmlComponent.prototype = /** @type {!_AbstractOfferRowHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
class _AbstractOfferRowHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OfferRowHtmlComponent} */ (res)
  }
}
/**
 * The _IOfferRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOfferRowHtmlComponent} ‎
 */
export class AbstractOfferRowHtmlComponent extends newAbstract(
 _AbstractOfferRowHtmlComponent,'IOfferRowHtmlComponent',null,{
  asIOfferRowHtmlComponent:1,
  superOfferRowHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent} */
AbstractOfferRowHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent} */
function AbstractOfferRowHtmlComponentClass(){}


AbstractOfferRowHtmlComponent[$implementations]=[
 __AbstractOfferRowHtmlComponent,
 HtmlComponent,
 AbstractOfferRow,
 AbstractOfferRowGPU,
 AbstractOfferRowScreenBack,
 AbstractOfferRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferRowHtmlComponent}*/({
  inputsQPs:OfferRowInputsQPs,
 }),
]