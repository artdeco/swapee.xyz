import {makeBuffers} from '@webcircuits/webcircuits'

export const OfferRowBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default OfferRowBuffer