import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferRowDisplay}
 */
function __AbstractOfferRowDisplay() {}
__AbstractOfferRowDisplay.prototype = /** @type {!_AbstractOfferRowDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
class _AbstractOfferRowDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOfferRowDisplay} ‎
 */
class AbstractOfferRowDisplay extends newAbstract(
 _AbstractOfferRowDisplay,'IOfferRowDisplay',null,{
  asIOfferRowDisplay:1,
  superOfferRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay} */
AbstractOfferRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay} */
function AbstractOfferRowDisplayClass(){}

export default AbstractOfferRowDisplay


AbstractOfferRowDisplay[$implementations]=[
 __AbstractOfferRowDisplay,
 GraphicsDriverBack,
 AbstractOfferRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferRowDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOfferRowDisplay}*/({
    ExchangeCollapsar:twinMock,
    OfferExchange:twinMock,
    BestOfferPlaque:twinMock,
    ProgressVertLine:twinMock,
    CoinImWr:twinMock,
    GetDealLa:twinMock,
    CancelLa:twinMock,
    GetDealBuA:twinMock,
    OfferCrypto:twinMock,
    OfferAmount:twinMock,
    Logo:twinMock,
    Eta:twinMock,
    GetDeal:twinMock,
    RecHandle:twinMock,
    RecPopup:twinMock,
    FloatingIco:twinMock,
    FixedIco:twinMock,
    CoinIm:twinMock,
    FloatingLa:twinMock,
    FixedLa:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OfferRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOfferRowDisplay.Initialese}*/({
   ExchangeCollapsar:1,
   OfferExchange:1,
   BestOfferPlaque:1,
   ProgressVertLine:1,
   CoinImWr:1,
   GetDealLa:1,
   CancelLa:1,
   GetDealBuA:1,
   OfferCrypto:1,
   OfferAmount:1,
   Logo:1,
   Eta:1,
   GetDeal:1,
   RecHandle:1,
   RecPopup:1,
   FloatingIco:1,
   FixedIco:1,
   CoinIm:1,
   FloatingLa:1,
   FixedLa:1,
  }),
  initializer({
   ExchangeCollapsar:_ExchangeCollapsar,
   OfferExchange:_OfferExchange,
   BestOfferPlaque:_BestOfferPlaque,
   ProgressVertLine:_ProgressVertLine,
   CoinImWr:_CoinImWr,
   GetDealLa:_GetDealLa,
   CancelLa:_CancelLa,
   GetDealBuA:_GetDealBuA,
   OfferCrypto:_OfferCrypto,
   OfferAmount:_OfferAmount,
   Logo:_Logo,
   Eta:_Eta,
   GetDeal:_GetDeal,
   RecHandle:_RecHandle,
   RecPopup:_RecPopup,
   FloatingIco:_FloatingIco,
   FixedIco:_FixedIco,
   CoinIm:_CoinIm,
   FloatingLa:_FloatingLa,
   FixedLa:_FixedLa,
  }) {
   if(_ExchangeCollapsar!==undefined) this.ExchangeCollapsar=_ExchangeCollapsar
   if(_OfferExchange!==undefined) this.OfferExchange=_OfferExchange
   if(_BestOfferPlaque!==undefined) this.BestOfferPlaque=_BestOfferPlaque
   if(_ProgressVertLine!==undefined) this.ProgressVertLine=_ProgressVertLine
   if(_CoinImWr!==undefined) this.CoinImWr=_CoinImWr
   if(_GetDealLa!==undefined) this.GetDealLa=_GetDealLa
   if(_CancelLa!==undefined) this.CancelLa=_CancelLa
   if(_GetDealBuA!==undefined) this.GetDealBuA=_GetDealBuA
   if(_OfferCrypto!==undefined) this.OfferCrypto=_OfferCrypto
   if(_OfferAmount!==undefined) this.OfferAmount=_OfferAmount
   if(_Logo!==undefined) this.Logo=_Logo
   if(_Eta!==undefined) this.Eta=_Eta
   if(_GetDeal!==undefined) this.GetDeal=_GetDeal
   if(_RecHandle!==undefined) this.RecHandle=_RecHandle
   if(_RecPopup!==undefined) this.RecPopup=_RecPopup
   if(_FloatingIco!==undefined) this.FloatingIco=_FloatingIco
   if(_FixedIco!==undefined) this.FixedIco=_FixedIco
   if(_CoinIm!==undefined) this.CoinIm=_CoinIm
   if(_FloatingLa!==undefined) this.FloatingLa=_FloatingLa
   if(_FixedLa!==undefined) this.FixedLa=_FixedLa
  },
 }),
]