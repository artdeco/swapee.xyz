import solder from './methods/solder'
import PostProcess from './methods/post-process'
import server from './methods/server'
import render from './methods/render'
import OfferRowServerController from '../OfferRowServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOfferRowElement from '../../gen/AbstractOfferRowElement'

/** @extends {xyz.swapee.wc.OfferRowElement} */
export default class OfferRowElement extends AbstractOfferRowElement.implements(
 OfferRowServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /**@type {!xyz.swapee.wc.OfferRowElement}*/({
 get exchangeCollapsarHandleSel(){
 const{asIOfferRowElement:{xExchangeCollapsarHandle:xExchangeCollapsarHandle}}=this
 return`[${xExchangeCollapsarHandle}]`
 },
 get recommendationInfoHandleSel(){
 const{asIOfferRowElement:{xRecommendationInfoHandle:xRecommendationInfoHandle}}=this
 return`[${xRecommendationInfoHandle}]`
 },
 allocator: function allocateSelectors(){
 const{
  attributes:{id:id},asIMaurice:{page:page},
  asIOfferRowElement:{
   X_EXCHANGE_COLLAPSAR_HANDLE:X_EXCHANGE_COLLAPSAR_HANDLE,
   X_RECOMMENDATION_INFO_HANDLE:X_RECOMMENDATION_INFO_HANDLE,
  },
 }=this
 if(!page) return // buildees
 let xExchangeCollapsarHandle=X_EXCHANGE_COLLAPSAR_HANDLE
 const exchangeCollapsarHandleKey='x:xyz.swapee.wc.IOfferRow::X_EXCHANGE_COLLAPSAR_HANDLE'
 if(id){
  xExchangeCollapsarHandle+=`-${id}`
 }else if(exchangeCollapsarHandleKey in page){
  let c=page[exchangeCollapsarHandleKey]
  c++
  page[exchangeCollapsarHandleKey]=c
  xExchangeCollapsarHandle+=`-${c}`
 }else{
  page[exchangeCollapsarHandleKey]=0
 }
 this.xExchangeCollapsarHandle=xExchangeCollapsarHandle
 let xRecommendationInfoHandle=X_RECOMMENDATION_INFO_HANDLE
 const recommendationInfoHandleKey='x:xyz.swapee.wc.IOfferRow::X_RECOMMENDATION_INFO_HANDLE'
 if(id){
  xRecommendationInfoHandle+=`-${id}`
 }else if(recommendationInfoHandleKey in page){
  let c=page[recommendationInfoHandleKey]
  c++
  page[recommendationInfoHandleKey]=c
  xRecommendationInfoHandle+=`-${c}`
 }else{
  page[recommendationInfoHandleKey]=0
 }
 this.xRecommendationInfoHandle=xRecommendationInfoHandle
 },
 }),
 /** @type {!xyz.swapee.wc.IOfferRowElement} */ ({
  solder:solder,
  PostProcess:PostProcess,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IOfferRowElement}*/({
   classesMap:        true,
   rootSelector:     `.OfferRow`,
   stylesheets:       [
    'html/styles/mixins.css',
    'html/styles/OfferRow.css',
    'html/styles/BestRow.css',
    'html/styles/Recommended.css',
   ],
   blockers:[
    'xyz.swapee.wc.IOfferExchange',
    'xyz.swapee.wc.IProgressColumn',
   ],
   blockName:        'html/OfferRowBlock.html',
  }),
){}

// thank you for using web circuits
