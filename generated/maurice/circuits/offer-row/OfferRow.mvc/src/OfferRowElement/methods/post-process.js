/** @type {xyz.swapee.wc.IOfferRowElement._PostProcess} */
export default function PostProcess(html) {
 const{asIProper:{props:{recommended:recommended,fixed:fixed}}}=/**@type {!xyz.swapee.wc.IOfferRowElement}*/(this)
 // todo:
 if(!recommended) {
  html=html.replace(/(<!--)( \$if: is-recommended -->[\s\S]+?<!-- \/\$if: is-recommended )(-->)/g,(m,cs,c,ce)=>{
   return `${cs}${c.replace(/\S/g,' ')}${ce}`
  })
 }
 if(!fixed) {
  html=html.replace(/(<!--)( \$if: is-fixed -->[\s\S]+?<!-- \/\$if: is-fixed )(-->)/g,(m,cs,c,ce)=>{
   return `${cs}${c.replace(/\S/g,' ')}${ce}`
  })
 }else{
  html=html.replace(/(<!--)( \$if: is-floating -->[\s\S]+?<!-- \/\$if: is-floating )(-->)/g,(m,cs,c,ce)=>{
   return `${cs}${c.replace(/\S/g,' ')}${ce}`
  })
 }

 return html
}