/** @type {xyz.swapee.wc.IOfferRowElement._server} */
export default function server(_,{fixed:fixed,recommended:recommended}) {
 const{asIOfferRowElement:{
  // xExchangeCollapsarHandle,
  // exchangeCollapsarHandleSel,
  xRecommendationInfoHandle,
  recommendationInfoHandleSel,
 }}=this
 // const exchangeCollapsarHandle={[xExchangeCollapsarHandle]:true}
 const recommendationInfoHandle={[xRecommendationInfoHandle]:true}

 return (<div $id="OfferRow" Recommended={recommended}>
  <img $id="FloatingIco" $conceal={fixed} />
  <img $id="FixedIco" $reveal={fixed} />

  {/* todo: specify handle as a combination of id and scope? */}
  <img $id="RecHandle" {...recommendationInfoHandle} />
  <img $id="RecPopup" query:handle={recommendationInfoHandleSel} />

  {/* <span $id="OfferCrypto" {...exchangeCollapsarHandle} />
  <span $id="OfferAmount" {...exchangeCollapsarHandle} />
  <span $id="CoinImWr" {...exchangeCollapsarHandle} />
  <span $id="BestOfferPlaque" {...exchangeCollapsarHandle} />
  <span $id="ProgressVertLine" {...exchangeCollapsarHandle} /> */}
  {/* todo: handle scopes */}

  {/* <div $id="GetDeal" {...exchangeCollapsarHandle} /> */}
  {/* <div $id="ExchangeCollapsar" query:handle={exchangeCollapsarHandleSel} id={xExchangeCollapsarHandle} /> */}

  <p $id="FloatingLa" $conceal={fixed} />
  <p $id="FixedLa" $reveal={fixed} />
 </div>)
}