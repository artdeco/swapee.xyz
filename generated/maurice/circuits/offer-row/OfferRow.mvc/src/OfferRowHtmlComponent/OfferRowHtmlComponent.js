import OfferRowHtmlController from '../OfferRowHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOfferRowHtmlComponent} from '../../gen/AbstractOfferRowHtmlComponent'

/** @extends {xyz.swapee.wc.OfferRowHtmlComponent} */
export default class extends AbstractOfferRowHtmlComponent.implements(
 OfferRowHtmlController,
 IntegratedComponentInitialiser,
){}