import AbstractOfferRowControllerAT from '../../gen/AbstractOfferRowControllerAT'
import OfferRowDisplay from '../OfferRowDisplay'
import AbstractOfferRowScreen from '../../gen/AbstractOfferRowScreen'

/** @extends {xyz.swapee.wc.OfferRowScreen} */
export default class extends AbstractOfferRowScreen.implements(
 AbstractOfferRowControllerAT,
 OfferRowDisplay,
 /**@type {!xyz.swapee.wc.IOfferRowScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOfferRowScreen} */ ({
  __$id:8085433869,
 }),
){}