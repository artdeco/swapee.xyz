import {OfferRowVdusPQs} from './OfferRowVdusPQs'
export const OfferRowVdusQPs=/**@type {!xyz.swapee.wc.OfferRowVdusQPs}*/(Object.keys(OfferRowVdusPQs)
 .reduce((a,k)=>{a[OfferRowVdusPQs[k]]=k;return a},{}))