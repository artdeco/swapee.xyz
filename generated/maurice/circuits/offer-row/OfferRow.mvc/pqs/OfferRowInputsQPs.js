import {OfferRowInputsPQs} from './OfferRowInputsPQs'
export const OfferRowInputsQPs=/**@type {!xyz.swapee.wc.OfferRowInputsQPs}*/(Object.keys(OfferRowInputsPQs)
 .reduce((a,k)=>{a[OfferRowInputsPQs[k]]=k;return a},{}))