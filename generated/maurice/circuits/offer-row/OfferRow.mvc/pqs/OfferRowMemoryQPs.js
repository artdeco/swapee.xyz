import {OfferRowMemoryPQs} from './OfferRowMemoryPQs'
export const OfferRowMemoryQPs=/**@type {!xyz.swapee.wc.OfferRowMemoryQPs}*/(Object.keys(OfferRowMemoryPQs)
 .reduce((a,k)=>{a[OfferRowMemoryPQs[k]]=k;return a},{}))