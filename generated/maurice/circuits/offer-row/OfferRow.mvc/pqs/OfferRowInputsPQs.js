import {OfferRowMemoryPQs} from './OfferRowMemoryPQs'
export const OfferRowInputsPQs=/**@type {!xyz.swapee.wc.OfferRowInputsQPs}*/({
 ...OfferRowMemoryPQs,
})