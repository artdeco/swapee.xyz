import OfferRowClassesPQs from './OfferRowClassesPQs'
export const OfferRowClassesQPs=/**@type {!xyz.swapee.wc.OfferRowClassesQPs}*/(Object.keys(OfferRowClassesPQs)
 .reduce((a,k)=>{a[OfferRowClassesPQs[k]]=k;return a},{}))