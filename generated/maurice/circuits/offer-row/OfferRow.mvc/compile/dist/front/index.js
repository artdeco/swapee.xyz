/**
 * Display for presenting information from the _IOfferRow_.
 * @extends {xyz.swapee.wc.OfferRowDisplay}
 */
class OfferRowDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.OfferRowScreen}
 */
class OfferRowScreen extends (class {/* lazy-loaded */}) {}

module.exports.OfferRowDisplay = OfferRowDisplay
module.exports.OfferRowScreen = OfferRowScreen