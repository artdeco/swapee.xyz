/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=c["372700389811"];function k(a,b,h,g){return c["372700389812"](a,b,h,g,!1,void 0)};function l(){}l.prototype={};class n{}class p extends k(n,80854338697,null,{J:1,qa:2}){}p[f]=[l];

const q=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const r=q["615055805212"],t=q["615055805218"],u=q["615055805233"];const v={core:"a74ad"};function x(){}x.prototype={};class y{}class z extends k(y,80854338696,null,{F:1,la:2}){}function A(){}A.prototype={};function B(){this.model={core:""}}class aa{}class C extends k(aa,80854338693,B,{H:1,oa:2}){}C[f]=[A,{constructor(){u(this.model,"",v)}}];z[f]=[{},x,C];
const D=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const ba=D.IntegratedComponentInitialiser,E=D.IntegratedComponent,ca=D["95173443851"];function F(){}F.prototype={};class da{}class G extends k(da,80854338691,null,{A:1,ja:2}){}G[f]=[F,t];const H={regulate:r({core:String})};
const I=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ea=I.IntegratedController,fa=I.Parametric;const J={...v};function K(){}K.prototype={};function ha(){const a={model:null};B.call(a);this.inputs=a.model}class ia{}class L extends k(ia,80854338695,ha,{I:1,pa:2}){}function M(){}L[f]=[M.prototype={},K,fa,M.prototype={constructor(){u(this.inputs,"",J)}}];function N(){}N.prototype={};class ja{}class O extends k(ja,808543386918,null,{D:1,ka:2}){}O[f]=[{},N,H,ea,{get Port(){return L}}];function P(){}P.prototype={};class ka{}class Q extends k(ka,80854338698,null,{v:1,ia:2}){}Q[f]=[P,z,p,E,G,O];function la(){return null};function ma(a){const {asIProper:{props:{i:b,fixed:h}}}=this;b||(a=a.replace(/(\x3c!--)( \$if: is-recommended --\x3e[\s\S]+?\x3c!-- \/\$if: is-recommended )(--\x3e)/g,(g,d,e,m)=>`${d}${e.replace(/\S/g," ")}${m}`));return a=h?a.replace(/(\x3c!--)( \$if: is-floating --\x3e[\s\S]+?\x3c!-- \/\$if: is-floating )(--\x3e)/g,(g,d,e,m)=>`${d}${e.replace(/\S/g," ")}${m}`):a.replace(/(\x3c!--)( \$if: is-fixed --\x3e[\s\S]+?\x3c!-- \/\$if: is-fixed )(--\x3e)/g,(g,d,e,m)=>`${d}${e.replace(/\S/g," ")}${m}`)};require(eval('"@type.engineering/web-computing"'));const R=require(eval('"@type.engineering/web-computing"')).h;
function na(a,{fixed:b,i:h}){const {g:{j:g,s:d,l:e,u:m}}=this;a={[g]:!0};const w={[e]:!0};return R("div",{$id:"OfferRow",Recommended:h},R("img",{$id:"FloatingIco",$conceal:b}),R("img",{$id:"FixedIco",$reveal:b}),R("img",{$id:"RecHandle",...w}),R("img",{$id:"RecPopup","query:handle":m}),R("span",{$id:"OfferCrypto",...a}),R("span",{$id:"OfferAmount",...a}),R("span",{$id:"CoinImWr",...a}),R("span",{$id:"BestOfferPlaque",...a}),R("span",{$id:"ProgressVertLine",...a}),R("div",{$id:"GetDeal",...a}),R("div",
{$id:"ExchangeCollapsar","query:handle":d,id:g}),R("p",{$id:"FloatingLa",$conceal:b}),R("p",{$id:"FixedLa",$reveal:b}))};require(eval('"@type.engineering/web-computing"'));const oa=require(eval('"@type.engineering/web-computing"')).h;function pa(){return oa("div",{$id:"OfferRow"})};var S=class extends O.implements(){};require("https");require("http");const qa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}qa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ra=T.ElementBase,sa=T.HTMLBlocker;require(eval('"@type.engineering/web-computing"'));const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function ta(){this.inputs={noSolder:!1,fixed:!1,i:!1,U:{},ea:{},K:{},fa:{},R:{},Z:{},M:{},da:{},ba:{},aa:{},T:{},$:{},ga:{},ha:{},X:{},V:{},P:{},Y:{},W:{}}}class ua{}class W extends k(ua,808543386912,ta,{G:1,na:2}){}
W[f]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"exchange-collapsar-opts":void 0,"offer-exchange-opts":void 0,"best-offer-plaque-opts":void 0,"progress-vert-line-opts":void 0,"coin-im-wr-opts":void 0,"get-deal-la-opts":void 0,"cancel-la-opts":void 0,"offer-crypto-opts":void 0,"offer-amount-opts":void 0,"logo-opts":void 0,"eta-opts":void 0,"get-deal-opts":void 0,"rec-handle-opts":void 0,"rec-popup-opts":void 0,"floating-ico-opts":void 0,"fixed-ico-opts":void 0,
"coin-im-opts":void 0,"floating-la-opts":void 0,"fixed-la-opts":void 0})}}];function X(){}X.prototype={};function va(){this.m="x-exchange-collapsar-handle";this.o="x-recommendation-info-handle"}class wa{}class Y extends k(wa,808543386911,va,{g:1,ma:2}){}function Z(){}
Y[f]=[X,ra,Z.prototype={calibrate:function({":no-solder":a,":fixed":b,":recommended":h,":core":g}){const {attributes:{"no-solder":d,fixed:e,recommended:m,core:w}}=this;return{...(void 0===d?{"no-solder":a}:{}),...(void 0===e?{fixed:b}:{}),...(void 0===m?{recommended:h}:{}),...(void 0===w?{core:g}:{})}}},Z.prototype={calibrate:({"no-solder":a,fixed:b,recommended:h,core:g})=>({noSolder:a,fixed:b,i:h,core:g})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",
{$id:"OfferRow"},U("vdu",{$id:"ExchangeCollapsar"}),U("vdu",{$id:"OfferExchange"}),U("vdu",{$id:"BestOfferPlaque"}),U("vdu",{$id:"ProgressVertLine"}),U("vdu",{$id:"CoinImWr"}),U("vdu",{$id:"GetDealLa"}),U("vdu",{$id:"CancelLa"}),U("vdu",{$id:"OfferCrypto"}),U("vdu",{$id:"OfferAmount"}),U("vdu",{$id:"Logo"}),U("vdu",{$id:"Eta"}),U("vdu",{$id:"GetDeal"}),U("vdu",{$id:"RecHandle"}),U("vdu",{$id:"RecPopup"}),U("vdu",{$id:"FloatingIco"}),U("vdu",{$id:"FixedIco"}),U("vdu",{$id:"CoinIm"}),U("vdu",{$id:"FloatingLa"}),
U("vdu",{$id:"FixedLa"}))}},Z.prototype={classes:{BestOffer:"e8977",Recommended:"65486",ProgressVertLine:"a7a04"},inputsPQs:J,vdus:{OfferCrypto:"h8a71",OfferAmount:"h8a72",Eta:"h8a75",FloatingIco:"h8a76",FixedIco:"h8a77",FloatingLa:"h8a78",FixedLa:"h8a79",RecHandle:"h8a710",RecPopup:"h8a711",Logo:"h8a712",CoinIm:"h8a713",ExchangeCollapsar:"h8a715",GetDeal:"h8a716",GetDealLa:"h8a717",CancelLa:"h8a718",CoinImWr:"h8a719",BestOfferPlaque:"h8a720",ProgressVertLine:"h8a721",OfferExchange:"h8a722"}},E,Z.prototype=
{constructor(){Object.assign(this,{knownInputs:new Set("noSolder fixed recommended core no-solder :no-solder :fixed :recommended :core fe646 cec31 0aef2 a5d67 09b03 6497b 4971c c35d8 928d8 fa5c0 8e449 b679b 33629 ef570 6975f 8e283 93e03 40f16 5eba3 1a8cc 062ec da65e a74ad children".split(" "))})},get Port(){return W}}];Y[f]=[Q,{rootId:"OfferRow",__$id:8085433869,fqn:"xyz.swapee.wc.IOfferRow",maurice_element_v3:!0}];class xa extends Y.implements(S,ca,sa,ba,{get s(){const {g:{j:a}}=this;return`[${a}]`},get u(){const {g:{l:a}}=this;return`[${a}]`},allocator:function(){const {attributes:{id:a},asIMaurice:{page:b},g:{m:h,o:g}}=this;var d=h;if(a)d+=`-${a}`;else if("x:xyz.swapee.wc.IOfferRow::X_EXCHANGE_COLLAPSAR_HANDLE"in b){var e=b["x:xyz.swapee.wc.IOfferRow::X_EXCHANGE_COLLAPSAR_HANDLE"];e++;b["x:xyz.swapee.wc.IOfferRow::X_EXCHANGE_COLLAPSAR_HANDLE"]=e;d+=`-${e}`}else b["x:xyz.swapee.wc.IOfferRow::X_EXCHANGE_COLLAPSAR_HANDLE"]=
0;this.j=d;d=g;a?d+=`-${a}`:"x:xyz.swapee.wc.IOfferRow::X_RECOMMENDATION_INFO_HANDLE"in b?(e=b["x:xyz.swapee.wc.IOfferRow::X_RECOMMENDATION_INFO_HANDLE"],e++,b["x:xyz.swapee.wc.IOfferRow::X_RECOMMENDATION_INFO_HANDLE"]=e,d+=`-${e}`):b["x:xyz.swapee.wc.IOfferRow::X_RECOMMENDATION_INFO_HANDLE"]=0;this.l=d}},{solder:la,PostProcess:ma,server:na,render:pa},{classesMap:!0,rootSelector:".OfferRow",stylesheets:["html/styles/mixins.css","html/styles/OfferRow.css","html/styles/BestRow.css","html/styles/Recommended.css"],
blockers:["xyz.swapee.wc.IChangellyExchange","xyz.swapee.wc.IProgressColumn"],blockName:"html/OfferRowBlock.html"}){};module.exports["80854338690"]=Q;module.exports["80854338691"]=Q;module.exports["80854338693"]=L;module.exports["80854338694"]=O;module.exports["80854338698"]=xa;module.exports["808543386911"]=H;module.exports["808543386930"]=G;module.exports["808543386961"]=S;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['8085433869']=module.exports