/**
 * An abstract class of `xyz.swapee.wc.IOfferRow` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRow}
 */
class AbstractOfferRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferRowPort}
 */
class OfferRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRowController}
 */
class AbstractOfferRowController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOfferRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OfferRowElement}
 */
class OfferRowElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferRowBuffer}
 */
class OfferRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRowComputer}
 */
class AbstractOfferRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OfferRowController}
 */
class OfferRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferRow = AbstractOfferRow
module.exports.OfferRowPort = OfferRowPort
module.exports.AbstractOfferRowController = AbstractOfferRowController
module.exports.OfferRowElement = OfferRowElement
module.exports.OfferRowBuffer = OfferRowBuffer
module.exports.AbstractOfferRowComputer = AbstractOfferRowComputer
module.exports.OfferRowController = OfferRowController

Object.defineProperties(module.exports, {
 'AbstractOfferRow': {get: () => require('./precompile/internal')[80854338691]},
 [80854338691]: {get: () => module.exports['AbstractOfferRow']},
 'OfferRowPort': {get: () => require('./precompile/internal')[80854338693]},
 [80854338693]: {get: () => module.exports['OfferRowPort']},
 'AbstractOfferRowController': {get: () => require('./precompile/internal')[80854338694]},
 [80854338694]: {get: () => module.exports['AbstractOfferRowController']},
 'OfferRowElement': {get: () => require('./precompile/internal')[80854338698]},
 [80854338698]: {get: () => module.exports['OfferRowElement']},
 'OfferRowBuffer': {get: () => require('./precompile/internal')[808543386911]},
 [808543386911]: {get: () => module.exports['OfferRowBuffer']},
 'AbstractOfferRowComputer': {get: () => require('./precompile/internal')[808543386930]},
 [808543386930]: {get: () => module.exports['AbstractOfferRowComputer']},
 'OfferRowController': {get: () => require('./precompile/internal')[808543386961]},
 [808543386961]: {get: () => module.exports['OfferRowController']},
})