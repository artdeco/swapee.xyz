import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractOfferRow}*/
export class AbstractOfferRow extends Module['80854338691'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferRow} */
AbstractOfferRow.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferRowPort} */
export const OfferRowPort=Module['80854338693']
/**@extends {xyz.swapee.wc.AbstractOfferRowController}*/
export class AbstractOfferRowController extends Module['80854338694'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowController} */
AbstractOfferRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferRowHtmlComponent} */
export const OfferRowHtmlComponent=Module['808543386910']
/** @type {typeof xyz.swapee.wc.OfferRowBuffer} */
export const OfferRowBuffer=Module['808543386911']
/**@extends {xyz.swapee.wc.AbstractOfferRowComputer}*/
export class AbstractOfferRowComputer extends Module['808543386930'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferRowComputer} */
AbstractOfferRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.back.OfferRowController} */
export const OfferRowController=Module['808543386961']