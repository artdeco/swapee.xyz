/**
 * An abstract class of `xyz.swapee.wc.IOfferRow` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRow}
 */
class AbstractOfferRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferRowPort}
 */
class OfferRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRowController}
 */
class AbstractOfferRowController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOfferRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OfferRowHtmlComponent}
 */
class OfferRowHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferRowBuffer}
 */
class OfferRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferRowComputer}
 */
class AbstractOfferRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OfferRowController}
 */
class OfferRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferRow = AbstractOfferRow
module.exports.OfferRowPort = OfferRowPort
module.exports.AbstractOfferRowController = AbstractOfferRowController
module.exports.OfferRowHtmlComponent = OfferRowHtmlComponent
module.exports.OfferRowBuffer = OfferRowBuffer
module.exports.AbstractOfferRowComputer = AbstractOfferRowComputer
module.exports.OfferRowController = OfferRowController