import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {8085433869} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const ha=c["372700389810"],d=c["372700389811"];function f(a,b,e,g){return c["372700389812"](a,b,e,g,!1,void 0)};function h(){}h.prototype={};class ia{}class k extends f(ia,80854338697,null,{ba:1,oa:2}){}k[d]=[h];
const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ja=l["61505580523"],ka=l["615055805212"],la=l["615055805218"],ma=l["615055805221"],na=l["615055805223"],m=l["615055805233"];const n={L:"a74ad"};function p(){}p.prototype={};class oa{}class q extends f(oa,80854338696,null,{W:1,ha:2}){}function r(){}r.prototype={};function t(){this.model={L:""}}class pa{}class u extends f(pa,80854338693,t,{$:1,la:2}){}u[d]=[r,{constructor(){m(this.model,"",n)}}];q[d]=[{},p,u];

const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const qa=v.IntegratedController,ra=v.Parametric;
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const sa=w.IntegratedComponentInitialiser,ta=w.IntegratedComponent,ua=w["38"];function x(){}x.prototype={};class va{}class y extends f(va,80854338691,null,{U:1,fa:2}){}y[d]=[x,la];const z={regulate:ka({L:String})};const A={...n};function B(){}B.prototype={};function wa(){const a={model:null};t.call(a);this.inputs=a.model}class xa{}class C extends f(xa,80854338695,wa,{aa:1,ma:2}){}function D(){}C[d]=[D.prototype={},B,ra,D.prototype={constructor(){m(this.inputs,"",A)}}];function E(){}E.prototype={};class ya{}class F extends f(ya,808543386918,null,{M:1,O:2}){}F[d]=[{},E,z,qa,{get Port(){return C}}];function G(){}G.prototype={};class za{}class H extends f(za,80854338698,null,{T:1,ea:2}){}H[d]=[G,q,k,ta,y,F];
const I=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const Aa=I["12817393923"],Ba=I["12817393924"],Ca=I["12817393925"],Da=I["12817393926"];function J(){}J.prototype={};class Ea{}class K extends f(Ea,808543386921,null,{V:1,ga:2}){}K[d]=[J,Da,{allocator(){this.methods={}}}];function L(){}L.prototype={};class Fa{}class M extends f(Fa,808543386920,null,{M:1,O:2}){}M[d]=[L,F,K,Aa];var N=class extends M.implements(){};function O(){}O.prototype={};class Ga{}class P extends f(Ga,808543386917,null,{X:1,ia:2}){}
P[d]=[O,Ba,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{o:a,I:a,h:a,g:a,l:a,D:a,i:a,H:a,G:a,F:a,m:a,C:a,J:a,K:a,v:a,s:a,j:a,A:a,u:a})}},{[ha]:{o:1,I:1,h:1,g:1,l:1,D:1,i:1,H:1,G:1,F:1,m:1,C:1,J:1,K:1,v:1,s:1,j:1,A:1,u:1},initializer({o:a,I:b,h:e,g,l:R,D:S,i:T,H:U,G:V,F:W,m:X,C:Y,J:Z,K:aa,v:ba,s:ca,j:da,A:ea,u:fa}){void 0!==a&&(this.o=a);void 0!==b&&(this.I=b);void 0!==e&&(this.h=e);void 0!==g&&(this.g=g);void 0!==R&&(this.l=R);void 0!==
S&&(this.D=S);void 0!==T&&(this.i=T);void 0!==U&&(this.H=U);void 0!==V&&(this.G=V);void 0!==W&&(this.F=W);void 0!==X&&(this.m=X);void 0!==Y&&(this.C=Y);void 0!==Z&&(this.J=Z);void 0!==aa&&(this.K=aa);void 0!==ba&&(this.v=ba);void 0!==ca&&(this.s=ca);void 0!==da&&(this.j=da);void 0!==ea&&(this.A=ea);void 0!==fa&&(this.u=fa)}}];const Q={P:"e8977",R:"65486",g:"a7a04"};const Ha=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});const Ia={H:"h8a71",G:"h8a72",m:"h8a75",v:"h8a76",s:"h8a77",A:"h8a78",u:"h8a79",J:"h8a710",K:"h8a711",F:"h8a712",j:"h8a713",o:"h8a715",C:"h8a716",D:"h8a717",i:"h8a718",l:"h8a719",h:"h8a720",g:"h8a721",I:"h8a722"};const Ja=Object.keys(Ia).reduce((a,b)=>{a[Ia[b]]=b;return a},{});function Ka(){}Ka.prototype={};class La{}class Ma extends f(La,808543386914,null,{Y:1,ja:2}){}function Na(){}Ma[d]=[Ka,Na.prototype={classesQPs:Ha,vdusQPs:Ja,memoryPQs:n},P,ja,Na.prototype={allocator(){ua(this.classes,"",Q)}}];function Oa(){}Oa.prototype={};class Pa{}class Qa extends f(Pa,808543386926,null,{da:1,qa:2}){}Qa[d]=[Oa,Ca];function Ra(){}Ra.prototype={};class Sa{}class Ta extends f(Sa,808543386924,null,{ca:1,pa:2}){}Ta[d]=[Ra,Qa];const Ua=Object.keys(A).reduce((a,b)=>{a[A[b]]=b;return a},{});function Va(){}Va.prototype={};class Wa{static mvc(a,b,e){return na(this,a,b,null,e)}}class Xa extends f(Wa,808543386910,null,{Z:1,ka:2}){}Xa[d]=[Va,ma,H,Ma,Ta,{inputsQPs:Ua}];var Ya=class extends Xa.implements(N,sa){};module.exports["80854338690"]=H;module.exports["80854338691"]=H;module.exports["80854338693"]=C;module.exports["80854338694"]=F;module.exports["808543386910"]=Ya;module.exports["808543386911"]=z;module.exports["808543386930"]=y;module.exports["808543386961"]=N;
/*! @embed-object-end {8085433869} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule