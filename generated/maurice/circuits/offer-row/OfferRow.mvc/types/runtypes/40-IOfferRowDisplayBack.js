/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay.Initialese filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OfferRowClasses>}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.ExchangeCollapsar
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferExchange
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.BestOfferPlaque
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.ProgressVertLine
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CoinImWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDealLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CancelLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDealBuA
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferCrypto
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.Logo
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.Eta
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDeal
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.RecHandle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.RecPopup
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FloatingIco
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FixedIco
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CoinIm
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FloatingLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FixedLa
/** @typedef {$xyz.swapee.wc.back.IOfferRowDisplay.Initialese} */
xyz.swapee.wc.back.IOfferRowDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplayFields filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ExchangeCollapsar
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferExchange
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.BestOfferPlaque
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ProgressVertLine
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinImWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CancelLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealBuA
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferCrypto
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Logo
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Eta
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDeal
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecHandle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecPopup
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingIco
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedIco
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinIm
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplayFields}
 */
xyz.swapee.wc.back.IOfferRowDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplayCaster filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowDisplay} */
$xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/** @type {!xyz.swapee.wc.back.BoundOfferRowDisplay} */
$xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.superOfferRowDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplayCaster}
 */
xyz.swapee.wc.back.IOfferRowDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.OfferRowClasses, null>}
 */
$xyz.swapee.wc.back.IOfferRowDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplay}
 */
xyz.swapee.wc.back.IOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.OfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOfferRowDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowDisplay.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.OfferRowDisplay
/** @type {function(new: xyz.swapee.wc.back.IOfferRowDisplay)} */
xyz.swapee.wc.back.OfferRowDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.OfferRowDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.AbstractOfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OfferRowDisplay}
 */
$xyz.swapee.wc.back.AbstractOfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowDisplay)} */
xyz.swapee.wc.back.AbstractOfferRowDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.RecordIOfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @typedef {{ paint: xyz.swapee.wc.back.IOfferRowDisplay.paint }} */
xyz.swapee.wc.back.RecordIOfferRowDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.BoundIOfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOfferRowDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.OfferRowClasses, null>}
 */
$xyz.swapee.wc.back.BoundIOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowDisplay} */
xyz.swapee.wc.back.BoundIOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.BoundOfferRowDisplay filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowDisplay} */
xyz.swapee.wc.back.BoundOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay.paint filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OfferRowMemory=, null=): void} */
xyz.swapee.wc.back.IOfferRowDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOfferRowDisplay, !xyz.swapee.wc.OfferRowMemory=, null=): void} */
xyz.swapee.wc.back.IOfferRowDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IOfferRowDisplay.__paint} */
xyz.swapee.wc.back.IOfferRowDisplay.__paint

// nss:xyz.swapee.wc.back.IOfferRowDisplay,$xyz.swapee.wc.back.IOfferRowDisplay,xyz.swapee.wc.back
/* @typal-end */