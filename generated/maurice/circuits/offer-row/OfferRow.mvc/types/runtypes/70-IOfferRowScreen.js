/** @const {?} */ $xyz.swapee.wc.IOfferRowScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreen.Initialese filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IOfferRowDisplay.Initialese}
 */
$xyz.swapee.wc.IOfferRowScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowScreen.Initialese} */
xyz.swapee.wc.IOfferRowScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreenCaster filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @interface */
$xyz.swapee.wc.IOfferRowScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowScreen} */
$xyz.swapee.wc.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.BoundOfferRowScreen} */
$xyz.swapee.wc.IOfferRowScreenCaster.prototype.superOfferRowScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowScreenCaster}
 */
xyz.swapee.wc.IOfferRowScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IOfferRowController}
 * @extends {xyz.swapee.wc.IOfferRowDisplay}
 */
$xyz.swapee.wc.IOfferRowScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowScreen}
 */
xyz.swapee.wc.IOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.OfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowScreen.Initialese>}
 */
$xyz.swapee.wc.OfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.OfferRowScreen
/** @type {function(new: xyz.swapee.wc.IOfferRowScreen, ...!xyz.swapee.wc.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.OfferRowScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.OfferRowScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.AbstractOfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowScreen}
 */
$xyz.swapee.wc.AbstractOfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowScreen)} */
xyz.swapee.wc.AbstractOfferRowScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.OfferRowScreenConstructor filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowScreen, ...!xyz.swapee.wc.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.OfferRowScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.RecordIOfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.BoundIOfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIOfferRowController}
 * @extends {xyz.swapee.wc.BoundIOfferRowDisplay}
 */
$xyz.swapee.wc.BoundIOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowScreen} */
xyz.swapee.wc.BoundIOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.BoundOfferRowScreen filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowScreen} */
xyz.swapee.wc.BoundOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */