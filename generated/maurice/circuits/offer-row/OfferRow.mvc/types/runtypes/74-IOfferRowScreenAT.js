/** @const {?} */ $xyz.swapee.wc.back.IOfferRowScreenAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenAT.Initialese filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} */
xyz.swapee.wc.back.IOfferRowScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenATCaster filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowScreenAT} */
$xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.asIOfferRowScreenAT
/** @type {!xyz.swapee.wc.back.BoundOfferRowScreenAT} */
$xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.superOfferRowScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenATCaster}
 */
xyz.swapee.wc.back.IOfferRowScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IOfferRowScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenAT}
 */
xyz.swapee.wc.back.IOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.OfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.OfferRowScreenAT
/** @type {function(new: xyz.swapee.wc.back.IOfferRowScreenAT, ...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.OfferRowScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.AbstractOfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowScreenAT}
 */
$xyz.swapee.wc.back.AbstractOfferRowScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowScreenAT)} */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.OfferRowScreenATConstructor filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreenAT, ...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.RecordIOfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.BoundIOfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIOfferRowScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowScreenAT} */
xyz.swapee.wc.back.BoundIOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.BoundOfferRowScreenAT filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowScreenAT} */
xyz.swapee.wc.back.BoundOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */