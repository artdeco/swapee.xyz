/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Initialese filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings>}
 */
$xyz.swapee.wc.IOfferRowDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.ExchangeCollapsar
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferExchange
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.BestOfferPlaque
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.ProgressVertLine
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CoinImWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDealLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CancelLa
/** @type {HTMLAnchorElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDealBuA
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferCrypto
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferAmount
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.Logo
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.Eta
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDeal
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.RecHandle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.RecPopup
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FloatingIco
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FixedIco
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CoinIm
/** @type {HTMLParagraphElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FloatingLa
/** @type {HTMLParagraphElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FixedLa
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Initialese} */
xyz.swapee.wc.IOfferRowDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplayFields filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @interface */
$xyz.swapee.wc.IOfferRowDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowDisplay.Settings} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOfferRowDisplay.Queries} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.ExchangeCollapsar
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferExchange
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.BestOfferPlaque
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.ProgressVertLine
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinImWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CancelLa
/** @type {HTMLAnchorElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealBuA
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferCrypto
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferAmount
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.Logo
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.Eta
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDeal
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecHandle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecPopup
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingIco
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedIco
/** @type {HTMLElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinIm
/** @type {HTMLParagraphElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingLa
/** @type {HTMLParagraphElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplayFields}
 */
xyz.swapee.wc.IOfferRowDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplayCaster filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @interface */
$xyz.swapee.wc.IOfferRowDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowDisplay} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/** @type {!xyz.swapee.wc.BoundIOfferRowScreen} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.BoundOfferRowDisplay} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.superOfferRowDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplayCaster}
 */
xyz.swapee.wc.IOfferRowDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OfferRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 */
$xyz.swapee.wc.IOfferRowDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplay}
 */
xyz.swapee.wc.IOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.OfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowDisplay.Initialese>}
 */
$xyz.swapee.wc.OfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.OfferRowDisplay
/** @type {function(new: xyz.swapee.wc.IOfferRowDisplay, ...!xyz.swapee.wc.IOfferRowDisplay.Initialese)} */
xyz.swapee.wc.OfferRowDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.OfferRowDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.AbstractOfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowDisplay}
 */
$xyz.swapee.wc.AbstractOfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowDisplay)} */
xyz.swapee.wc.AbstractOfferRowDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.OfferRowDisplayConstructor filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowDisplay, ...!xyz.swapee.wc.IOfferRowDisplay.Initialese)} */
xyz.swapee.wc.OfferRowDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.RecordIOfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @typedef {{ paint: xyz.swapee.wc.IOfferRowDisplay.paint }} */
xyz.swapee.wc.RecordIOfferRowDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.BoundIOfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OfferRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowDisplay} */
xyz.swapee.wc.BoundIOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.BoundOfferRowDisplay filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowDisplay} */
xyz.swapee.wc.BoundOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.paint filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OfferRowMemory, null): void} */
xyz.swapee.wc.IOfferRowDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOfferRowDisplay, !xyz.swapee.wc.OfferRowMemory, null): void} */
xyz.swapee.wc.IOfferRowDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IOfferRowDisplay.__paint} */
xyz.swapee.wc.IOfferRowDisplay.__paint

// nss:xyz.swapee.wc.IOfferRowDisplay,$xyz.swapee.wc.IOfferRowDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Queries filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @record */
$xyz.swapee.wc.IOfferRowDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Queries} */
xyz.swapee.wc.IOfferRowDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Settings filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowDisplay.Queries}
 */
$xyz.swapee.wc.IOfferRowDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Settings} */
xyz.swapee.wc.IOfferRowDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */