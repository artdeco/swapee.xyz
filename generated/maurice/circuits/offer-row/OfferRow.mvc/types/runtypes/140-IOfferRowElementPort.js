/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts.getDealBuOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts.getDealBuOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts.prototype.getDealBuOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe.prototype.getDealBuOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts.prototype.getDealBuOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe.prototype.getDealBuOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Initialese filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOfferRowElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Initialese} */
xyz.swapee.wc.IOfferRowElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPortFields filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @interface */
$xyz.swapee.wc.IOfferRowElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowElementPort.Inputs} */
$xyz.swapee.wc.IOfferRowElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOfferRowElementPort.Inputs} */
$xyz.swapee.wc.IOfferRowElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElementPortFields}
 */
xyz.swapee.wc.IOfferRowElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPortCaster filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @interface */
$xyz.swapee.wc.IOfferRowElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowElementPort} */
$xyz.swapee.wc.IOfferRowElementPortCaster.prototype.asIOfferRowElementPort
/** @type {!xyz.swapee.wc.BoundOfferRowElementPort} */
$xyz.swapee.wc.IOfferRowElementPortCaster.prototype.superOfferRowElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElementPortCaster}
 */
xyz.swapee.wc.IOfferRowElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferRowElementPort.Inputs>}
 */
$xyz.swapee.wc.IOfferRowElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElementPort}
 */
xyz.swapee.wc.IOfferRowElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.OfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElementPort.Initialese>}
 */
$xyz.swapee.wc.OfferRowElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.OfferRowElementPort
/** @type {function(new: xyz.swapee.wc.IOfferRowElementPort, ...!xyz.swapee.wc.IOfferRowElementPort.Initialese)} */
xyz.swapee.wc.OfferRowElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.OfferRowElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.AbstractOfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowElementPort}
 */
$xyz.swapee.wc.AbstractOfferRowElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowElementPort)} */
xyz.swapee.wc.AbstractOfferRowElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.OfferRowElementPortConstructor filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementPort, ...!xyz.swapee.wc.IOfferRowElementPort.Initialese)} */
xyz.swapee.wc.OfferRowElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.RecordIOfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.BoundIOfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowElementPortFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOfferRowElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOfferRowElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowElementPort} */
xyz.swapee.wc.BoundIOfferRowElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.BoundOfferRowElementPort filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowElementPort} */
xyz.swapee.wc.BoundOfferRowElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {boolean} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.fixed filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {boolean} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.fixed

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.recommended filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {boolean} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.recommended

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.exchangeCollapsarOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.exchangeCollapsarOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.offerExchangeOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.offerExchangeOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.bestOfferPlaqueOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.bestOfferPlaqueOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.progressVertLineOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.progressVertLineOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.coinImWrOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.coinImWrOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.getDealLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.getDealLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.cancelLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.cancelLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.getDealBuAOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.getDealBuAOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.offerCryptoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.offerCryptoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.offerAmountOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.offerAmountOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.logoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.logoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.etaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.etaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.getDealOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.getDealOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.recHandleOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.recHandleOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.recPopupOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.recPopupOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.floatingIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.floatingIcoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.fixedIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.fixedIcoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.coinImOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.coinImOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.floatingLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.floatingLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.fixedLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @typedef {!Object} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.fixedLaOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.prototype.fixed
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.prototype.recommended
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.prototype.exchangeCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.prototype.offerExchangeOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.prototype.bestOfferPlaqueOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.prototype.progressVertLineOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.prototype.getDealLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.prototype.cancelLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.prototype.getDealBuAOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.prototype.offerCryptoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.prototype.offerAmountOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.prototype.logoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.prototype.etaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.prototype.getDealOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.prototype.recHandleOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.prototype.recPopupOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.prototype.floatingIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.prototype.fixedIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.prototype.coinImOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.prototype.floatingLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.prototype.fixedLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts}
 */
$xyz.swapee.wc.IOfferRowElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowElementPort.Inputs}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed.prototype.fixed
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended.prototype.recommended
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts.prototype.exchangeCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts.prototype.offerExchangeOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts.prototype.bestOfferPlaqueOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts.prototype.progressVertLineOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts.prototype.getDealLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts.prototype.cancelLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts.prototype.getDealBuAOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts.prototype.offerCryptoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts.prototype.offerAmountOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts.prototype.logoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts.prototype.etaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts.prototype.getDealOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts.prototype.recHandleOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts.prototype.recPopupOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts.prototype.floatingIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts.prototype.fixedIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts.prototype.coinImOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts.prototype.floatingLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts.prototype.fixedLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts}
 */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs}
 */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe.prototype.fixed
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe.prototype.recommended
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe.prototype.exchangeCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe.prototype.offerExchangeOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe.prototype.bestOfferPlaqueOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe.prototype.progressVertLineOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe.prototype.getDealLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe.prototype.cancelLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe.prototype.getDealBuAOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe.prototype.offerCryptoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe.prototype.offerAmountOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe.prototype.logoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe.prototype.etaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe.prototype.getDealOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe.prototype.recHandleOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe.prototype.recPopupOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe.prototype.floatingIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe.prototype.fixedIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe.prototype.coinImOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe.prototype.floatingLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe.prototype.fixedLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe.prototype.fixed
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe.prototype.recommended
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe.prototype.exchangeCollapsarOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe.prototype.offerExchangeOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe.prototype.bestOfferPlaqueOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe.prototype.progressVertLineOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe.prototype.coinImWrOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe.prototype.getDealLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe.prototype.cancelLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe.prototype.getDealBuAOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe.prototype.offerCryptoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe.prototype.offerAmountOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe.prototype.logoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe.prototype.etaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe.prototype.getDealOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe.prototype.recHandleOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe.prototype.recPopupOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe.prototype.floatingIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe.prototype.fixedIcoOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe.prototype.coinImOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe.prototype.floatingLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe filter:!ControllerPlugin~props 844e3a823d24887f6f52f62fb176f812 */
/** @record */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe.prototype.fixedLaOpts
/** @typedef {$xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe} */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElementPort.WeakInputs
/* @typal-end */