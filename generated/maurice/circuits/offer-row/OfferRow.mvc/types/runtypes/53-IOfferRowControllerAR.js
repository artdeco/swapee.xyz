/** @const {?} */ $xyz.swapee.wc.back.IOfferRowControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerAR.Initialese filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} */
xyz.swapee.wc.back.IOfferRowControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerARCaster filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowControllerAR} */
$xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.asIOfferRowControllerAR
/** @type {!xyz.swapee.wc.back.BoundOfferRowControllerAR} */
$xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.superOfferRowControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerARCaster}
 */
xyz.swapee.wc.back.IOfferRowControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOfferRowController}
 */
$xyz.swapee.wc.back.IOfferRowControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerAR}
 */
xyz.swapee.wc.back.IOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.OfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.OfferRowControllerAR
/** @type {function(new: xyz.swapee.wc.back.IOfferRowControllerAR, ...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.OfferRowControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.AbstractOfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowControllerAR}
 */
$xyz.swapee.wc.back.AbstractOfferRowControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowControllerAR)} */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.OfferRowControllerARConstructor filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowControllerAR, ...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.RecordIOfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.BoundIOfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 */
$xyz.swapee.wc.back.BoundIOfferRowControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowControllerAR} */
xyz.swapee.wc.back.BoundIOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.BoundOfferRowControllerAR filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowControllerAR} */
xyz.swapee.wc.back.BoundOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */