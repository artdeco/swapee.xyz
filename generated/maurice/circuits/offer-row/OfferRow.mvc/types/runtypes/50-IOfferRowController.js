/** @const {?} */ $xyz.swapee.wc.IOfferRowController
/** @const {?} */ xyz.swapee.wc.IOfferRowController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.Initialese filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.Initialese} */
xyz.swapee.wc.IOfferRowController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowControllerFields filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @interface */
$xyz.swapee.wc.IOfferRowControllerFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowController.Inputs} */
$xyz.swapee.wc.IOfferRowControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowControllerFields}
 */
xyz.swapee.wc.IOfferRowControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowControllerCaster filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @interface */
$xyz.swapee.wc.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowController} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.BoundIOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowProcessor
/** @type {!xyz.swapee.wc.BoundOfferRowController} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowControllerCaster}
 */
xyz.swapee.wc.IOfferRowControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.OfferRowMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.IOfferRowController = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowController.prototype.resetPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowController}
 */
xyz.swapee.wc.IOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.OfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.OfferRowController
/** @type {function(new: xyz.swapee.wc.IOfferRowController, ...!xyz.swapee.wc.IOfferRowController.Initialese)} */
xyz.swapee.wc.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.OfferRowController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.AbstractOfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowController}
 */
$xyz.swapee.wc.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowController)} */
xyz.swapee.wc.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.OfferRowControllerConstructor filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowController, ...!xyz.swapee.wc.IOfferRowController.Initialese)} */
xyz.swapee.wc.OfferRowControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.RecordIOfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @typedef {{ resetPort: xyz.swapee.wc.IOfferRowController.resetPort }} */
xyz.swapee.wc.RecordIOfferRowController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.BoundIOfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowControllerFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.OfferRowMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowController} */
xyz.swapee.wc.BoundIOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.BoundOfferRowController filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowController} */
xyz.swapee.wc.BoundOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.resetPort filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowController): void} */
xyz.swapee.wc.IOfferRowController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowController.__resetPort} */
xyz.swapee.wc.IOfferRowController.__resetPort

// nss:xyz.swapee.wc.IOfferRowController,$xyz.swapee.wc.IOfferRowController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.Inputs filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPort.Inputs}
 */
$xyz.swapee.wc.IOfferRowController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.Inputs} */
xyz.swapee.wc.IOfferRowController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.WeakInputs filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPort.WeakInputs}
 */
$xyz.swapee.wc.IOfferRowController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.WeakInputs} */
xyz.swapee.wc.IOfferRowController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */