/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowMemoryPQs.prototype.core
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowMemoryPQs}
 */
xyz.swapee.wc.OfferRowMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowMemoryQPs.prototype.a74ad
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowMemoryQPs}
 */
xyz.swapee.wc.OfferRowMemoryQPs
/** @type {function(new: xyz.swapee.wc.OfferRowMemoryQPs)} */
xyz.swapee.wc.OfferRowMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OfferRowMemoryPQs}
 */
$xyz.swapee.wc.OfferRowInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowInputsPQs}
 */
xyz.swapee.wc.OfferRowInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.OfferRowInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowInputsQPs}
 */
xyz.swapee.wc.OfferRowInputsQPs
/** @type {function(new: xyz.swapee.wc.OfferRowInputsQPs)} */
xyz.swapee.wc.OfferRowInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.OfferCrypto
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.OfferAmount
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.LogoDark
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.LogoLight
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.Eta
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FloatingIco
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FixedIco
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FloatingLa
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowVdusPQs}
 */
xyz.swapee.wc.OfferRowVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a71
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a72
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a73
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a74
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a75
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a76
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a77
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a78
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a79
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowVdusQPs}
 */
xyz.swapee.wc.OfferRowVdusQPs
/** @type {function(new: xyz.swapee.wc.OfferRowVdusQPs)} */
xyz.swapee.wc.OfferRowVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowClassesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowClassesPQs.prototype.BestOffer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowClassesPQs}
 */
xyz.swapee.wc.OfferRowClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowClassesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowClassesQPs.prototype.e8977
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowClassesQPs}
 */
xyz.swapee.wc.OfferRowClassesQPs
/** @type {function(new: xyz.swapee.wc.OfferRowClassesQPs)} */
xyz.swapee.wc.OfferRowClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */