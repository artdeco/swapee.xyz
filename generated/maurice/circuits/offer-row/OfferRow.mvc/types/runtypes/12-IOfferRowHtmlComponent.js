/** @const {?} */ $xyz.swapee.wc.IOfferRowHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponent.Initialese filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowController.Initialese}
 * @extends {xyz.swapee.wc.back.IOfferRowScreen.Initialese}
 * @extends {xyz.swapee.wc.IOfferRow.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 */
$xyz.swapee.wc.IOfferRowHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} */
xyz.swapee.wc.IOfferRowHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponentCaster filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @interface */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowHtmlComponent} */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.asIOfferRowHtmlComponent
/** @type {!xyz.swapee.wc.BoundOfferRowHtmlComponent} */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.superOfferRowHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 */
xyz.swapee.wc.IOfferRowHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOfferRowController}
 * @extends {xyz.swapee.wc.back.IOfferRowScreen}
 * @extends {xyz.swapee.wc.IOfferRow}
 * @extends {xyz.swapee.wc.IOfferRowGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IOfferRowProcessor}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 */
$xyz.swapee.wc.IOfferRowHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowHtmlComponent}
 */
xyz.swapee.wc.IOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.OfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.OfferRowHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.OfferRowHtmlComponent
/** @type {function(new: xyz.swapee.wc.IOfferRowHtmlComponent, ...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese)} */
xyz.swapee.wc.OfferRowHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.OfferRowHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.AbstractOfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowHtmlComponent}
 */
$xyz.swapee.wc.AbstractOfferRowHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowHtmlComponent)} */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.OfferRowHtmlComponentConstructor filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowHtmlComponent, ...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese)} */
xyz.swapee.wc.OfferRowHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.RecordIOfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.BoundIOfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowController}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreen}
 * @extends {xyz.swapee.wc.BoundIOfferRow}
 * @extends {xyz.swapee.wc.BoundIOfferRowGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 */
$xyz.swapee.wc.BoundIOfferRowHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowHtmlComponent} */
xyz.swapee.wc.BoundIOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.BoundOfferRowHtmlComponent filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowHtmlComponent} */
xyz.swapee.wc.BoundOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */