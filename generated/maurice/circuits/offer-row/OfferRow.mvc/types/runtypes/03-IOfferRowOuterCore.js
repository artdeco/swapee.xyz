/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore
/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IOfferRowCore
/** @const {?} */ $xyz.swapee.wc.IOfferRowCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Initialese filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Initialese} */
xyz.swapee.wc.IOfferRowOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCoreFields filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @interface */
$xyz.swapee.wc.IOfferRowOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowOuterCore.Model} */
$xyz.swapee.wc.IOfferRowOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCoreFields}
 */
xyz.swapee.wc.IOfferRowOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCoreCaster filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @interface */
$xyz.swapee.wc.IOfferRowOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowOuterCore} */
$xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.asIOfferRowOuterCore
/** @type {!xyz.swapee.wc.BoundOfferRowOuterCore} */
$xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.superOfferRowOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
xyz.swapee.wc.IOfferRowOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
$xyz.swapee.wc.IOfferRowOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCore}
 */
xyz.swapee.wc.IOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.OfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowOuterCore.Initialese>}
 */
$xyz.swapee.wc.OfferRowOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.OfferRowOuterCore
/** @type {function(new: xyz.swapee.wc.IOfferRowOuterCore)} */
xyz.swapee.wc.OfferRowOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.OfferRowOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.AbstractOfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowOuterCore}
 */
$xyz.swapee.wc.AbstractOfferRowOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowOuterCore)} */
xyz.swapee.wc.AbstractOfferRowOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.RecordIOfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.BoundIOfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIOfferRowOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowOuterCore} */
xyz.swapee.wc.BoundIOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.BoundOfferRowOuterCore filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowOuterCore} */
xyz.swapee.wc.BoundOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core.core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @typedef {string} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model.Core} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core}
 */
$xyz.swapee.wc.IOfferRowOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model} */
xyz.swapee.wc.IOfferRowOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.Inputs.Core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Inputs.Core} */
xyz.swapee.wc.IOfferRowPort.Inputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe} */
xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core} */
xyz.swapee.wc.IOfferRowPort.WeakInputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe} */
xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowCore.Model.Core filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core}
 */
$xyz.swapee.wc.IOfferRowCore.Model.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model.Core} */
xyz.swapee.wc.IOfferRowCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowCore.Model.Core_Safe filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowCore.Model.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model.Core_Safe} */
xyz.swapee.wc.IOfferRowCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore.Model
/* @typal-end */