/** @const {?} */ $xyz.swapee.wc.IOfferRowGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowDisplay.Initialese}
 */
$xyz.swapee.wc.IOfferRowGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowGPU.Initialese} */
xyz.swapee.wc.IOfferRowGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOfferRowGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IOfferRowGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPUFields}
 */
xyz.swapee.wc.IOfferRowGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOfferRowGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowGPU} */
$xyz.swapee.wc.IOfferRowGPUCaster.prototype.asIOfferRowGPU
/** @type {!xyz.swapee.wc.BoundOfferRowGPU} */
$xyz.swapee.wc.IOfferRowGPUCaster.prototype.superOfferRowGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPUCaster}
 */
xyz.swapee.wc.IOfferRowGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OfferRowMemory,>}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplay}
 */
$xyz.swapee.wc.IOfferRowGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPU}
 */
xyz.swapee.wc.IOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.OfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowGPU.Initialese>}
 */
$xyz.swapee.wc.OfferRowGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.OfferRowGPU
/** @type {function(new: xyz.swapee.wc.IOfferRowGPU, ...!xyz.swapee.wc.IOfferRowGPU.Initialese)} */
xyz.swapee.wc.OfferRowGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.OfferRowGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.AbstractOfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowGPU}
 */
$xyz.swapee.wc.AbstractOfferRowGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowGPU)} */
xyz.swapee.wc.AbstractOfferRowGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.OfferRowGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowGPU, ...!xyz.swapee.wc.IOfferRowGPU.Initialese)} */
xyz.swapee.wc.OfferRowGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.RecordIOfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.BoundIOfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowGPUFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OfferRowMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowDisplay}
 */
$xyz.swapee.wc.BoundIOfferRowGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowGPU} */
xyz.swapee.wc.BoundIOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.BoundOfferRowGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowGPU} */
xyz.swapee.wc.BoundOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */