/** @const {?} */ $xyz.swapee.wc.IOfferRow
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowEnv filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @record */
$xyz.swapee.wc.OfferRowEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IOfferRow} */
$xyz.swapee.wc.OfferRowEnv.prototype.offerRow
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowEnv}
 */
xyz.swapee.wc.OfferRowEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.Initialese filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {xyz.swapee.wc.IOfferRowProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.IOfferRow.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRow.Initialese} */
xyz.swapee.wc.IOfferRow.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowFields filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @interface */
$xyz.swapee.wc.IOfferRowFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRow.Pinout} */
$xyz.swapee.wc.IOfferRowFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowFields}
 */
xyz.swapee.wc.IOfferRowFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowCaster filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @interface */
$xyz.swapee.wc.IOfferRowCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRow} */
$xyz.swapee.wc.IOfferRowCaster.prototype.asIOfferRow
/** @type {!xyz.swapee.wc.BoundOfferRow} */
$xyz.swapee.wc.IOfferRowCaster.prototype.superOfferRow
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCaster}
 */
xyz.swapee.wc.IOfferRowCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCaster}
 * @extends {xyz.swapee.wc.IOfferRowProcessor}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 * @extends {xyz.swapee.wc.IOfferRowController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, null>}
 */
$xyz.swapee.wc.IOfferRow = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRow}
 */
xyz.swapee.wc.IOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRow}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRow.Initialese>}
 */
$xyz.swapee.wc.OfferRow = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.OfferRow
/** @type {function(new: xyz.swapee.wc.IOfferRow, ...!xyz.swapee.wc.IOfferRow.Initialese)} */
xyz.swapee.wc.OfferRow.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.OfferRow.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.AbstractOfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init
 * @extends {xyz.swapee.wc.OfferRow}
 */
$xyz.swapee.wc.AbstractOfferRow = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRow}
 */
xyz.swapee.wc.AbstractOfferRow
/** @type {function(new: xyz.swapee.wc.AbstractOfferRow)} */
xyz.swapee.wc.AbstractOfferRow.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRow.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowConstructor filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRow, ...!xyz.swapee.wc.IOfferRow.Initialese)} */
xyz.swapee.wc.OfferRowConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.MVCOptions filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @record */
$xyz.swapee.wc.IOfferRow.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOfferRow.Pinout)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOfferRow.Pinout)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOfferRow.Pinout} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OfferRowMemory)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OfferRowClasses)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IOfferRow.MVCOptions} */
xyz.swapee.wc.IOfferRow.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.RecordIOfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.BoundIOfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowFields}
 * @extends {xyz.swapee.wc.RecordIOfferRow}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, null>}
 */
$xyz.swapee.wc.BoundIOfferRow = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRow} */
xyz.swapee.wc.BoundIOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.BoundOfferRow filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRow}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRow = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRow} */
xyz.swapee.wc.BoundOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.Pinout filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowController.Inputs}
 */
$xyz.swapee.wc.IOfferRow.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRow.Pinout} */
xyz.swapee.wc.IOfferRow.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowBuffer filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.IOfferRowBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowBuffer}
 */
xyz.swapee.wc.IOfferRowBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowBuffer filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowBuffer}
 */
$xyz.swapee.wc.OfferRowBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowBuffer}
 */
xyz.swapee.wc.OfferRowBuffer
/** @type {function(new: xyz.swapee.wc.IOfferRowBuffer)} */
xyz.swapee.wc.OfferRowBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */