/** @const {?} */ $xyz.swapee.wc.IOfferRowComputer
/** @const {?} */ xyz.swapee.wc.IOfferRowComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer.Initialese filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IOfferRowComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowComputer.Initialese} */
xyz.swapee.wc.IOfferRowComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputerCaster filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @interface */
$xyz.swapee.wc.IOfferRowComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowComputer} */
$xyz.swapee.wc.IOfferRowComputerCaster.prototype.asIOfferRowComputer
/** @type {!xyz.swapee.wc.BoundOfferRowComputer} */
$xyz.swapee.wc.IOfferRowComputerCaster.prototype.superOfferRowComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowComputerCaster}
 */
xyz.swapee.wc.IOfferRowComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OfferRowMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.IOfferRowComputer = function() {}
/**
 * @param {xyz.swapee.wc.OfferRowMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowComputer}
 */
xyz.swapee.wc.IOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.OfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowComputer.Initialese>}
 */
$xyz.swapee.wc.OfferRowComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.OfferRowComputer
/** @type {function(new: xyz.swapee.wc.IOfferRowComputer, ...!xyz.swapee.wc.IOfferRowComputer.Initialese)} */
xyz.swapee.wc.OfferRowComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.OfferRowComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.AbstractOfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowComputer}
 */
$xyz.swapee.wc.AbstractOfferRowComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowComputer)} */
xyz.swapee.wc.AbstractOfferRowComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.OfferRowComputerConstructor filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowComputer, ...!xyz.swapee.wc.IOfferRowComputer.Initialese)} */
xyz.swapee.wc.OfferRowComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.RecordIOfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @typedef {{ compute: xyz.swapee.wc.IOfferRowComputer.compute }} */
xyz.swapee.wc.RecordIOfferRowComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.BoundIOfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OfferRowMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundIOfferRowComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowComputer} */
xyz.swapee.wc.BoundIOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.BoundOfferRowComputer filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowComputer} */
xyz.swapee.wc.BoundOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer.compute filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.OfferRowMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.OfferRowMemory): void} */
xyz.swapee.wc.IOfferRowComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IOfferRowComputer, xyz.swapee.wc.OfferRowMemory): void} */
xyz.swapee.wc.IOfferRowComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IOfferRowComputer.__compute} */
xyz.swapee.wc.IOfferRowComputer.__compute

// nss:xyz.swapee.wc.IOfferRowComputer,$xyz.swapee.wc.IOfferRowComputer,xyz.swapee.wc
/* @typal-end */