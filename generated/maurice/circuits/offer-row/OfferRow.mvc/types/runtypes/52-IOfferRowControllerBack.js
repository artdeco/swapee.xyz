/** @const {?} */ $xyz.swapee.wc.back.IOfferRowController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowController.Initialese filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowController.Initialese} */
xyz.swapee.wc.back.IOfferRowController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowControllerCaster filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowController} */
$xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.back.BoundOfferRowController} */
$xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerCaster}
 */
xyz.swapee.wc.back.IOfferRowControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.IOfferRowController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.back.IOfferRowController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowController}
 */
xyz.swapee.wc.back.IOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.OfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.OfferRowController
/** @type {function(new: xyz.swapee.wc.back.IOfferRowController, ...!xyz.swapee.wc.back.IOfferRowController.Initialese)} */
xyz.swapee.wc.back.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.OfferRowController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.AbstractOfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowController}
 */
$xyz.swapee.wc.back.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowController)} */
xyz.swapee.wc.back.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.OfferRowControllerConstructor filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowController, ...!xyz.swapee.wc.back.IOfferRowController.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.RecordIOfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.BoundIOfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowController} */
xyz.swapee.wc.back.BoundIOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.BoundOfferRowController filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowController} */
xyz.swapee.wc.back.BoundOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */