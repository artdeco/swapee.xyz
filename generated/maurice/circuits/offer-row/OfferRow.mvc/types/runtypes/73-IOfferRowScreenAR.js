/** @const {?} */ $xyz.swapee.wc.front.IOfferRowScreenAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenAR.Initialese filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowScreen.Initialese}
 */
$xyz.swapee.wc.front.IOfferRowScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} */
xyz.swapee.wc.front.IOfferRowScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenARCaster filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @interface */
$xyz.swapee.wc.front.IOfferRowScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowScreenAR} */
$xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.asIOfferRowScreenAR
/** @type {!xyz.swapee.wc.front.BoundOfferRowScreenAR} */
$xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.superOfferRowScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowScreenARCaster}
 */
xyz.swapee.wc.front.IOfferRowScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOfferRowScreen}
 */
$xyz.swapee.wc.front.IOfferRowScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowScreenAR}
 */
xyz.swapee.wc.front.IOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.OfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.OfferRowScreenAR
/** @type {function(new: xyz.swapee.wc.front.IOfferRowScreenAR, ...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese)} */
xyz.swapee.wc.front.OfferRowScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.OfferRowScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.AbstractOfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowScreenAR}
 */
$xyz.swapee.wc.front.AbstractOfferRowScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowScreenAR)} */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.OfferRowScreenARConstructor filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowScreenAR, ...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese)} */
xyz.swapee.wc.front.OfferRowScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.RecordIOfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.BoundIOfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOfferRowScreen}
 */
$xyz.swapee.wc.front.BoundIOfferRowScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowScreenAR} */
xyz.swapee.wc.front.BoundIOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.BoundOfferRowScreenAR filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowScreenAR} */
xyz.swapee.wc.front.BoundOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */