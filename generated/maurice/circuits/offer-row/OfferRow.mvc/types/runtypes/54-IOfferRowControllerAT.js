/** @const {?} */ $xyz.swapee.wc.front.IOfferRowControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerAT.Initialese filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IOfferRowControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} */
xyz.swapee.wc.front.IOfferRowControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerATCaster filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @interface */
$xyz.swapee.wc.front.IOfferRowControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowControllerAT} */
$xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.asIOfferRowControllerAT
/** @type {!xyz.swapee.wc.front.BoundOfferRowControllerAT} */
$xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.superOfferRowControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerATCaster}
 */
xyz.swapee.wc.front.IOfferRowControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IOfferRowControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerAT}
 */
xyz.swapee.wc.front.IOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.OfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.OfferRowControllerAT
/** @type {function(new: xyz.swapee.wc.front.IOfferRowControllerAT, ...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.OfferRowControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.AbstractOfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowControllerAT}
 */
$xyz.swapee.wc.front.AbstractOfferRowControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowControllerAT)} */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.OfferRowControllerATConstructor filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowControllerAT, ...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.RecordIOfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.BoundIOfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIOfferRowControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowControllerAT} */
xyz.swapee.wc.front.BoundIOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.BoundOfferRowControllerAT filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowControllerAT} */
xyz.swapee.wc.front.BoundOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */