/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/102-OfferRowInputs.xml} xyz.swapee.wc.front.OfferRowInputs filter:!ControllerPlugin~props ac9945ff9e3cb0ee98b9a82d4b2c817c */
/** @record */
$xyz.swapee.wc.front.OfferRowInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.OfferRowInputs.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.OfferRowInputs}
 */
xyz.swapee.wc.front.OfferRowInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */