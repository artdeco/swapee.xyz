/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner
/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @interface */
$xyz.swapee.wc.IOfferRowDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDesigner}
 */
xyz.swapee.wc.IOfferRowDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.OfferRowDesigner filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowDesigner}
 */
$xyz.swapee.wc.OfferRowDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowDesigner}
 */
xyz.swapee.wc.OfferRowDesigner
/** @type {function(new: xyz.swapee.wc.IOfferRowDesigner)} */
xyz.swapee.wc.OfferRowDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh.prototype.OfferRow
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh} */
xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.relay.Mesh filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh.prototype.OfferRow
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh} */
xyz.swapee.wc.IOfferRowDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.relay.MemPool filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.OfferRowMemory} */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool.prototype.OfferRow
/** @type {!xyz.swapee.wc.OfferRowMemory} */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool} */
xyz.swapee.wc.IOfferRowDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.relay
/* @typal-end */