/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/41-OfferRowClasses.xml} xyz.swapee.wc.OfferRowClasses filter:!ControllerPlugin~props 636dd29c56ccc1aee1b43bb34f644c6d */
/** @record */
$xyz.swapee.wc.OfferRowClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.BestOffer
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.Recommended
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.ProgressVertLine
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowClasses}
 */
xyz.swapee.wc.OfferRowClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */