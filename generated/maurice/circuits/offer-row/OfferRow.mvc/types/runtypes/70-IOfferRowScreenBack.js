/** @const {?} */ $xyz.swapee.wc.back.IOfferRowScreen
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreen.Initialese filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowScreenAT.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowScreen.Initialese} */
xyz.swapee.wc.back.IOfferRowScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreenCaster filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowScreen} */
$xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.back.BoundOfferRowScreen} */
$xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.superOfferRowScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenCaster}
 */
xyz.swapee.wc.back.IOfferRowScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenCaster}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenAT}
 */
$xyz.swapee.wc.back.IOfferRowScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreen}
 */
xyz.swapee.wc.back.IOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.OfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreen.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.OfferRowScreen
/** @type {function(new: xyz.swapee.wc.back.IOfferRowScreen, ...!xyz.swapee.wc.back.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.back.OfferRowScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.OfferRowScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.AbstractOfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowScreen}
 */
$xyz.swapee.wc.back.AbstractOfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowScreen)} */
xyz.swapee.wc.back.AbstractOfferRowScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.OfferRowScreenConstructor filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreen, ...!xyz.swapee.wc.back.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.RecordIOfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.BoundIOfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreenAT}
 */
$xyz.swapee.wc.back.BoundIOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowScreen} */
xyz.swapee.wc.back.BoundIOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.BoundOfferRowScreen filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowScreen} */
xyz.swapee.wc.back.BoundOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */