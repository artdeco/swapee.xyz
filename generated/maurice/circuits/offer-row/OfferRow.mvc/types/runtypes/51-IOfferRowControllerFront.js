/** @const {?} */ $xyz.swapee.wc.front.IOfferRowController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowController.Initialese filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @record */
$xyz.swapee.wc.front.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowController.Initialese} */
xyz.swapee.wc.front.IOfferRowController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowControllerCaster filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @interface */
$xyz.swapee.wc.front.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowController} */
$xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.front.BoundOfferRowController} */
$xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerCaster}
 */
xyz.swapee.wc.front.IOfferRowControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerAT}
 */
$xyz.swapee.wc.front.IOfferRowController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowController}
 */
xyz.swapee.wc.front.IOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.OfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.OfferRowController
/** @type {function(new: xyz.swapee.wc.front.IOfferRowController, ...!xyz.swapee.wc.front.IOfferRowController.Initialese)} */
xyz.swapee.wc.front.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.OfferRowController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.AbstractOfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowController}
 */
$xyz.swapee.wc.front.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowController)} */
xyz.swapee.wc.front.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.OfferRowControllerConstructor filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowController, ...!xyz.swapee.wc.front.IOfferRowController.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.RecordIOfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.BoundIOfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOfferRowControllerAT}
 */
$xyz.swapee.wc.front.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowController} */
xyz.swapee.wc.front.BoundIOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.BoundOfferRowController filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowController} */
xyz.swapee.wc.front.BoundOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */