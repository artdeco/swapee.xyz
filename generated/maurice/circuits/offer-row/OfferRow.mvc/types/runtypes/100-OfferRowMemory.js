/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/100-OfferRowMemory.xml} xyz.swapee.wc.OfferRowMemory filter:!ControllerPlugin~props dd6c3b0cc30e7f8b22116642c3e4be0f */
/** @record */
$xyz.swapee.wc.OfferRowMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowMemory.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowMemory}
 */
xyz.swapee.wc.OfferRowMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */