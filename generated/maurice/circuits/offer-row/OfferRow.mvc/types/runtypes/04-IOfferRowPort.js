/** @const {?} */ $xyz.swapee.wc.IOfferRowPort
/** @const {?} */ $xyz.swapee.wc.IOfferRowPortInterface
/** @const {?} */ xyz.swapee.wc.IOfferRowPort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.Initialese filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOfferRowPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Initialese} */
xyz.swapee.wc.IOfferRowPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortFields filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */
$xyz.swapee.wc.IOfferRowPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */
$xyz.swapee.wc.IOfferRowPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortFields}
 */
xyz.swapee.wc.IOfferRowPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortCaster filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowPort} */
$xyz.swapee.wc.IOfferRowPortCaster.prototype.asIOfferRowPort
/** @type {!xyz.swapee.wc.BoundOfferRowPort} */
$xyz.swapee.wc.IOfferRowPortCaster.prototype.superOfferRowPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortCaster}
 */
xyz.swapee.wc.IOfferRowPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferRowPort.Inputs>}
 */
$xyz.swapee.wc.IOfferRowPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowPort.prototype.resetOfferRowPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPort}
 */
xyz.swapee.wc.IOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowPort.Initialese>}
 */
$xyz.swapee.wc.OfferRowPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.OfferRowPort
/** @type {function(new: xyz.swapee.wc.IOfferRowPort, ...!xyz.swapee.wc.IOfferRowPort.Initialese)} */
xyz.swapee.wc.OfferRowPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.OfferRowPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.AbstractOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowPort}
 */
$xyz.swapee.wc.AbstractOfferRowPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowPort)} */
xyz.swapee.wc.AbstractOfferRowPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPortConstructor filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowPort, ...!xyz.swapee.wc.IOfferRowPort.Initialese)} */
xyz.swapee.wc.OfferRowPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.RecordIOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @typedef {{ resetPort: xyz.swapee.wc.IOfferRowPort.resetPort, resetOfferRowPort: xyz.swapee.wc.IOfferRowPort.resetOfferRowPort }} */
xyz.swapee.wc.RecordIOfferRowPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.BoundIOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPortFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOfferRowPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOfferRowPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowPort} */
xyz.swapee.wc.BoundIOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.BoundOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowPort} */
xyz.swapee.wc.BoundOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.resetPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowPort): void} */
xyz.swapee.wc.IOfferRowPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowPort.__resetPort} */
xyz.swapee.wc.IOfferRowPort.__resetPort

// nss:xyz.swapee.wc.IOfferRowPort,$xyz.swapee.wc.IOfferRowPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.resetOfferRowPort filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowPort.resetOfferRowPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowPort): void} */
xyz.swapee.wc.IOfferRowPort._resetOfferRowPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort} */
xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort

// nss:xyz.swapee.wc.IOfferRowPort,$xyz.swapee.wc.IOfferRowPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.Inputs filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowPort.Inputs}
 */
xyz.swapee.wc.IOfferRowPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowPort.WeakInputs}
 */
xyz.swapee.wc.IOfferRowPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortInterface filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortInterface}
 */
xyz.swapee.wc.IOfferRowPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPortInterface filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowPortInterface}
 */
$xyz.swapee.wc.OfferRowPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowPortInterface}
 */
xyz.swapee.wc.OfferRowPortInterface
/** @type {function(new: xyz.swapee.wc.IOfferRowPortInterface)} */
xyz.swapee.wc.OfferRowPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortInterface.Props filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @record */
$xyz.swapee.wc.IOfferRowPortInterface.Props = function() {}
/** @type {string} */
$xyz.swapee.wc.IOfferRowPortInterface.Props.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowPortInterface.Props} */
xyz.swapee.wc.IOfferRowPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPortInterface
/* @typal-end */