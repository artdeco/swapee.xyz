/** @const {?} */ $xyz.swapee.wc.IOfferRowProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessor.Initialese filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.IOfferRowProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowProcessor.Initialese} */
xyz.swapee.wc.IOfferRowProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessorCaster filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @interface */
$xyz.swapee.wc.IOfferRowProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowProcessorCaster.prototype.asIOfferRowProcessor
/** @type {!xyz.swapee.wc.BoundOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowProcessorCaster.prototype.superOfferRowProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowProcessorCaster}
 */
xyz.swapee.wc.IOfferRowProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowProcessorCaster}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 * @extends {xyz.swapee.wc.IOfferRowCore}
 * @extends {xyz.swapee.wc.IOfferRowController}
 */
$xyz.swapee.wc.IOfferRowProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowProcessor}
 */
xyz.swapee.wc.IOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.OfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowProcessor.Initialese>}
 */
$xyz.swapee.wc.OfferRowProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.OfferRowProcessor
/** @type {function(new: xyz.swapee.wc.IOfferRowProcessor, ...!xyz.swapee.wc.IOfferRowProcessor.Initialese)} */
xyz.swapee.wc.OfferRowProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.OfferRowProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.AbstractOfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowProcessor}
 */
$xyz.swapee.wc.AbstractOfferRowProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowProcessor)} */
xyz.swapee.wc.AbstractOfferRowProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.OfferRowProcessorConstructor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowProcessor, ...!xyz.swapee.wc.IOfferRowProcessor.Initialese)} */
xyz.swapee.wc.OfferRowProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.RecordIOfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.BoundIOfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {xyz.swapee.wc.BoundIOfferRowCore}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 */
$xyz.swapee.wc.BoundIOfferRowProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowProcessor} */
xyz.swapee.wc.BoundIOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.BoundOfferRowProcessor filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowProcessor} */
xyz.swapee.wc.BoundOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */