/** @const {?} */ $xyz.swapee.wc.IOfferRowCore
/** @const {?} */ xyz.swapee.wc.IOfferRowCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.Initialese filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @record */
$xyz.swapee.wc.IOfferRowCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Initialese} */
xyz.swapee.wc.IOfferRowCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCoreFields filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @interface */
$xyz.swapee.wc.IOfferRowCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowCore.Model} */
$xyz.swapee.wc.IOfferRowCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCoreFields}
 */
xyz.swapee.wc.IOfferRowCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCoreCaster filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @interface */
$xyz.swapee.wc.IOfferRowCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowCore} */
$xyz.swapee.wc.IOfferRowCoreCaster.prototype.asIOfferRowCore
/** @type {!xyz.swapee.wc.BoundOfferRowCore} */
$xyz.swapee.wc.IOfferRowCoreCaster.prototype.superOfferRowCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCoreCaster}
 */
xyz.swapee.wc.IOfferRowCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCoreCaster}
 * @extends {xyz.swapee.wc.IOfferRowOuterCore}
 */
$xyz.swapee.wc.IOfferRowCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowCore.prototype.resetOfferRowCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCore}
 */
xyz.swapee.wc.IOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.OfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowCore.Initialese>}
 */
$xyz.swapee.wc.OfferRowCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.OfferRowCore
/** @type {function(new: xyz.swapee.wc.IOfferRowCore)} */
xyz.swapee.wc.OfferRowCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.OfferRowCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.AbstractOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowCore}
 */
$xyz.swapee.wc.AbstractOfferRowCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowCore)} */
xyz.swapee.wc.AbstractOfferRowCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.RecordIOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @typedef {{ resetCore: xyz.swapee.wc.IOfferRowCore.resetCore, resetOfferRowCore: xyz.swapee.wc.IOfferRowCore.resetOfferRowCore }} */
xyz.swapee.wc.RecordIOfferRowCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.BoundIOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowCoreFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowOuterCore}
 */
$xyz.swapee.wc.BoundIOfferRowCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowCore} */
xyz.swapee.wc.BoundIOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.BoundOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowCore} */
xyz.swapee.wc.BoundOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.resetCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOfferRowCore): void} */
xyz.swapee.wc.IOfferRowCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IOfferRowCore.__resetCore} */
xyz.swapee.wc.IOfferRowCore.__resetCore

// nss:xyz.swapee.wc.IOfferRowCore,$xyz.swapee.wc.IOfferRowCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.resetOfferRowCore filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowCore.resetOfferRowCore
/** @typedef {function(this: xyz.swapee.wc.IOfferRowCore): void} */
xyz.swapee.wc.IOfferRowCore._resetOfferRowCore
/** @typedef {typeof $xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore} */
xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore

// nss:xyz.swapee.wc.IOfferRowCore,$xyz.swapee.wc.IOfferRowCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.Model filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model}
 */
$xyz.swapee.wc.IOfferRowCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model} */
xyz.swapee.wc.IOfferRowCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore
/* @typal-end */