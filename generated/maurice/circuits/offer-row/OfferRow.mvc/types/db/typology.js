/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IOfferRowComputer': {
  'id': 80854338691,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.OfferRowMemoryPQs': {
  'id': 80854338692,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowOuterCore': {
  'id': 80854338693,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferRowInputsPQs': {
  'id': 80854338694,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowPort': {
  'id': 80854338695,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetOfferRowPort': 2
  }
 },
 'xyz.swapee.wc.IOfferRowCore': {
  'id': 80854338696,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetOfferRowCore': 2
  }
 },
 'xyz.swapee.wc.IOfferRowProcessor': {
  'id': 80854338697,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRow': {
  'id': 80854338698,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowBuffer': {
  'id': 80854338699,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowHtmlComponent': {
  'id': 808543386910,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowElement': {
  'id': 808543386911,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IOfferRowElementPort': {
  'id': 808543386912,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowDesigner': {
  'id': 808543386913,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IOfferRowGPU': {
  'id': 808543386914,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowDisplay': {
  'id': 808543386915,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.OfferRowVdusPQs': {
  'id': 808543386916,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IOfferRowDisplay': {
  'id': 808543386917,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IOfferRowController': {
  'id': 808543386918,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IOfferRowController': {
  'id': 808543386919,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowController': {
  'id': 808543386920,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowControllerAR': {
  'id': 808543386921,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferRowControllerAT': {
  'id': 808543386922,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IOfferRowScreen': {
  'id': 808543386923,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowScreen': {
  'id': 808543386924,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IOfferRowScreenAR': {
  'id': 808543386925,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IOfferRowScreenAT': {
  'id': 808543386926,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.OfferRowClassesPQs': {
  'id': 808543386927,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IOfferRowPortInterface': {
  'id': 808543386928,
  'symbols': {},
  'methods': {}
 }
})