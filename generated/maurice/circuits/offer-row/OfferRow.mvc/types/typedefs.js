/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOfferRowComputer={}
xyz.swapee.wc.IOfferRowOuterCore={}
xyz.swapee.wc.IOfferRowOuterCore.Model={}
xyz.swapee.wc.IOfferRowOuterCore.Model.Core={}
xyz.swapee.wc.IOfferRowOuterCore.WeakModel={}
xyz.swapee.wc.IOfferRowPort={}
xyz.swapee.wc.IOfferRowPort.Inputs={}
xyz.swapee.wc.IOfferRowPort.WeakInputs={}
xyz.swapee.wc.IOfferRowCore={}
xyz.swapee.wc.IOfferRowCore.Model={}
xyz.swapee.wc.IOfferRowPortInterface={}
xyz.swapee.wc.IOfferRowProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOfferRowController={}
xyz.swapee.wc.front.IOfferRowControllerAT={}
xyz.swapee.wc.front.IOfferRowScreenAR={}
xyz.swapee.wc.IOfferRow={}
xyz.swapee.wc.IOfferRowHtmlComponent={}
xyz.swapee.wc.IOfferRowElement={}
xyz.swapee.wc.IOfferRowElementPort={}
xyz.swapee.wc.IOfferRowElementPort.Inputs={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts={}
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts={}
xyz.swapee.wc.IOfferRowElementPort.WeakInputs={}
xyz.swapee.wc.IOfferRowDesigner={}
xyz.swapee.wc.IOfferRowDesigner.communicator={}
xyz.swapee.wc.IOfferRowDesigner.relay={}
xyz.swapee.wc.IOfferRowDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOfferRowDisplay={}
xyz.swapee.wc.back.IOfferRowController={}
xyz.swapee.wc.back.IOfferRowControllerAR={}
xyz.swapee.wc.back.IOfferRowScreen={}
xyz.swapee.wc.back.IOfferRowScreenAT={}
xyz.swapee.wc.IOfferRowController={}
xyz.swapee.wc.IOfferRowScreen={}
xyz.swapee.wc.IOfferRowGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml}  9e8780d3351ed26bf834d03a46c250ce */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IOfferRowComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowComputer)} xyz.swapee.wc.AbstractOfferRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowComputer} xyz.swapee.wc.OfferRowComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowComputer` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowComputer
 */
xyz.swapee.wc.AbstractOfferRowComputer = class extends /** @type {xyz.swapee.wc.AbstractOfferRowComputer.constructor&xyz.swapee.wc.OfferRowComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowComputer.prototype.constructor = xyz.swapee.wc.AbstractOfferRowComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowComputer.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowComputer.Initialese[]) => xyz.swapee.wc.IOfferRowComputer} xyz.swapee.wc.OfferRowComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferRowComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.OfferRowMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IOfferRowComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IOfferRowComputer
 */
xyz.swapee.wc.IOfferRowComputer = class extends /** @type {xyz.swapee.wc.IOfferRowComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferRowComputer.compute} */
xyz.swapee.wc.IOfferRowComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowComputer.Initialese>)} xyz.swapee.wc.OfferRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowComputer} xyz.swapee.wc.IOfferRowComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferRowComputer_ instances.
 * @constructor xyz.swapee.wc.OfferRowComputer
 * @implements {xyz.swapee.wc.IOfferRowComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowComputer.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowComputer = class extends /** @type {xyz.swapee.wc.OfferRowComputer.constructor&xyz.swapee.wc.IOfferRowComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.OfferRowComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferRowComputer} */
xyz.swapee.wc.RecordIOfferRowComputer

/** @typedef {xyz.swapee.wc.IOfferRowComputer} xyz.swapee.wc.BoundIOfferRowComputer */

/** @typedef {xyz.swapee.wc.OfferRowComputer} xyz.swapee.wc.BoundOfferRowComputer */

/**
 * Contains getters to cast the _IOfferRowComputer_ interface.
 * @interface xyz.swapee.wc.IOfferRowComputerCaster
 */
xyz.swapee.wc.IOfferRowComputerCaster = class { }
/**
 * Cast the _IOfferRowComputer_ instance into the _BoundIOfferRowComputer_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowComputer}
 */
xyz.swapee.wc.IOfferRowComputerCaster.prototype.asIOfferRowComputer
/**
 * Access the _OfferRowComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowComputer}
 */
xyz.swapee.wc.IOfferRowComputerCaster.prototype.superOfferRowComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.OfferRowMemory) => void} xyz.swapee.wc.IOfferRowComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowComputer.__compute<!xyz.swapee.wc.IOfferRowComputer>} xyz.swapee.wc.IOfferRowComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IOfferRowComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.OfferRowMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml}  5e367668441bad07d65fdb13d2baff20 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferRowOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowOuterCore)} xyz.swapee.wc.AbstractOfferRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowOuterCore} xyz.swapee.wc.OfferRowOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowOuterCore
 */
xyz.swapee.wc.AbstractOfferRowOuterCore = class extends /** @type {xyz.swapee.wc.AbstractOfferRowOuterCore.constructor&xyz.swapee.wc.OfferRowOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowOuterCore.prototype.constructor = xyz.swapee.wc.AbstractOfferRowOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowOuterCoreCaster)} xyz.swapee.wc.IOfferRowOuterCore.constructor */
/**
 * The _IOfferRow_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IOfferRowOuterCore
 */
xyz.swapee.wc.IOfferRowOuterCore = class extends /** @type {xyz.swapee.wc.IOfferRowOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowOuterCore.prototype.constructor = xyz.swapee.wc.IOfferRowOuterCore

/** @typedef {function(new: xyz.swapee.wc.IOfferRowOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowOuterCore.Initialese>)} xyz.swapee.wc.OfferRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowOuterCore} xyz.swapee.wc.IOfferRowOuterCore.typeof */
/**
 * A concrete class of _IOfferRowOuterCore_ instances.
 * @constructor xyz.swapee.wc.OfferRowOuterCore
 * @implements {xyz.swapee.wc.IOfferRowOuterCore} The _IOfferRow_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowOuterCore = class extends /** @type {xyz.swapee.wc.OfferRowOuterCore.constructor&xyz.swapee.wc.IOfferRowOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OfferRowOuterCore.prototype.constructor = xyz.swapee.wc.OfferRowOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.OfferRowOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowOuterCore.
 * @interface xyz.swapee.wc.IOfferRowOuterCoreFields
 */
xyz.swapee.wc.IOfferRowOuterCoreFields = class { }
/**
 * The _IOfferRow_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IOfferRowOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOfferRowOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore} */
xyz.swapee.wc.RecordIOfferRowOuterCore

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore} xyz.swapee.wc.BoundIOfferRowOuterCore */

/** @typedef {xyz.swapee.wc.OfferRowOuterCore} xyz.swapee.wc.BoundOfferRowOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.Model.Core} xyz.swapee.wc.IOfferRowOuterCore.Model The _IOfferRow_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferRowOuterCore.WeakModel The _IOfferRow_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IOfferRowOuterCore_ interface.
 * @interface xyz.swapee.wc.IOfferRowOuterCoreCaster
 */
xyz.swapee.wc.IOfferRowOuterCoreCaster = class { }
/**
 * Cast the _IOfferRowOuterCore_ instance into the _BoundIOfferRowOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowOuterCore}
 */
xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.asIOfferRowOuterCore
/**
 * Access the _OfferRowOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowOuterCore}
 */
xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.superOfferRowOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferRowPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.Model.Core} xyz.swapee.wc.IOfferRowCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe} xyz.swapee.wc.IOfferRowCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml}  375394a170d6c5fc69f298dd78c4741a */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOfferRowPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowPort)} xyz.swapee.wc.AbstractOfferRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowPort} xyz.swapee.wc.OfferRowPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowPort` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowPort
 */
xyz.swapee.wc.AbstractOfferRowPort = class extends /** @type {xyz.swapee.wc.AbstractOfferRowPort.constructor&xyz.swapee.wc.OfferRowPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowPort.prototype.constructor = xyz.swapee.wc.AbstractOfferRowPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowPort.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowPort.Initialese[]) => xyz.swapee.wc.IOfferRowPort} xyz.swapee.wc.OfferRowPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferRowPort.Inputs>)} xyz.swapee.wc.IOfferRowPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IOfferRow_, providing input
 * pins.
 * @interface xyz.swapee.wc.IOfferRowPort
 */
xyz.swapee.wc.IOfferRowPort = class extends /** @type {xyz.swapee.wc.IOfferRowPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferRowPort.resetPort} */
xyz.swapee.wc.IOfferRowPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOfferRowPort.resetOfferRowPort} */
xyz.swapee.wc.IOfferRowPort.prototype.resetOfferRowPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowPort.Initialese>)} xyz.swapee.wc.OfferRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowPort} xyz.swapee.wc.IOfferRowPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferRowPort_ instances.
 * @constructor xyz.swapee.wc.OfferRowPort
 * @implements {xyz.swapee.wc.IOfferRowPort} The port that serves as an interface to the _IOfferRow_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowPort.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowPort = class extends /** @type {xyz.swapee.wc.OfferRowPort.constructor&xyz.swapee.wc.IOfferRowPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.OfferRowPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowPort.
 * @interface xyz.swapee.wc.IOfferRowPortFields
 */
xyz.swapee.wc.IOfferRowPortFields = class { }
/**
 * The inputs to the _IOfferRow_'s controller via its port.
 */
xyz.swapee.wc.IOfferRowPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOfferRowPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowPort} */
xyz.swapee.wc.RecordIOfferRowPort

/** @typedef {xyz.swapee.wc.IOfferRowPort} xyz.swapee.wc.BoundIOfferRowPort */

/** @typedef {xyz.swapee.wc.OfferRowPort} xyz.swapee.wc.BoundOfferRowPort */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowOuterCore.WeakModel)} xyz.swapee.wc.IOfferRowPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowOuterCore.WeakModel} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IOfferRow_'s controller via its port.
 * @record xyz.swapee.wc.IOfferRowPort.Inputs
 */
xyz.swapee.wc.IOfferRowPort.Inputs = class extends /** @type {xyz.swapee.wc.IOfferRowPort.Inputs.constructor&xyz.swapee.wc.IOfferRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowPort.Inputs.prototype.constructor = xyz.swapee.wc.IOfferRowPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOfferRowOuterCore.WeakModel)} xyz.swapee.wc.IOfferRowPort.WeakInputs.constructor */
/**
 * The inputs to the _IOfferRow_'s controller via its port.
 * @record xyz.swapee.wc.IOfferRowPort.WeakInputs
 */
xyz.swapee.wc.IOfferRowPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOfferRowPort.WeakInputs.constructor&xyz.swapee.wc.IOfferRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOfferRowPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IOfferRowPortInterface
 */
xyz.swapee.wc.IOfferRowPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IOfferRowPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IOfferRowPortInterface.prototype.constructor = xyz.swapee.wc.IOfferRowPortInterface

/**
 * A concrete class of _IOfferRowPortInterface_ instances.
 * @constructor xyz.swapee.wc.OfferRowPortInterface
 * @implements {xyz.swapee.wc.IOfferRowPortInterface} The port interface.
 */
xyz.swapee.wc.OfferRowPortInterface = class extends xyz.swapee.wc.IOfferRowPortInterface { }
xyz.swapee.wc.OfferRowPortInterface.prototype.constructor = xyz.swapee.wc.OfferRowPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowPortInterface.Props
 * @prop {string} core The core property.
 */

/**
 * Contains getters to cast the _IOfferRowPort_ interface.
 * @interface xyz.swapee.wc.IOfferRowPortCaster
 */
xyz.swapee.wc.IOfferRowPortCaster = class { }
/**
 * Cast the _IOfferRowPort_ instance into the _BoundIOfferRowPort_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowPort}
 */
xyz.swapee.wc.IOfferRowPortCaster.prototype.asIOfferRowPort
/**
 * Access the _OfferRowPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowPort}
 */
xyz.swapee.wc.IOfferRowPortCaster.prototype.superOfferRowPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferRowPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowPort.__resetPort<!xyz.swapee.wc.IOfferRowPort>} xyz.swapee.wc.IOfferRowPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOfferRowPort.resetPort} */
/**
 * Resets the _IOfferRow_ port.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort<!xyz.swapee.wc.IOfferRowPort>} xyz.swapee.wc.IOfferRowPort._resetOfferRowPort */
/** @typedef {typeof xyz.swapee.wc.IOfferRowPort.resetOfferRowPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowPort.resetOfferRowPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml}  3286d91c81ea55c64f7aadd9a48e048c */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferRowCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowCore)} xyz.swapee.wc.AbstractOfferRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowCore} xyz.swapee.wc.OfferRowCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowCore` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowCore
 */
xyz.swapee.wc.AbstractOfferRowCore = class extends /** @type {xyz.swapee.wc.AbstractOfferRowCore.constructor&xyz.swapee.wc.OfferRowCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowCore.prototype.constructor = xyz.swapee.wc.AbstractOfferRowCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowCore.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowCoreCaster&xyz.swapee.wc.IOfferRowOuterCore)} xyz.swapee.wc.IOfferRowCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IOfferRowCore
 */
xyz.swapee.wc.IOfferRowCore = class extends /** @type {xyz.swapee.wc.IOfferRowCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferRowOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOfferRowCore.resetCore} */
xyz.swapee.wc.IOfferRowCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IOfferRowCore.resetOfferRowCore} */
xyz.swapee.wc.IOfferRowCore.prototype.resetOfferRowCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowCore.Initialese>)} xyz.swapee.wc.OfferRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowCore} xyz.swapee.wc.IOfferRowCore.typeof */
/**
 * A concrete class of _IOfferRowCore_ instances.
 * @constructor xyz.swapee.wc.OfferRowCore
 * @implements {xyz.swapee.wc.IOfferRowCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowCore.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowCore = class extends /** @type {xyz.swapee.wc.OfferRowCore.constructor&xyz.swapee.wc.IOfferRowCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OfferRowCore.prototype.constructor = xyz.swapee.wc.OfferRowCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.OfferRowCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowCore.
 * @interface xyz.swapee.wc.IOfferRowCoreFields
 */
xyz.swapee.wc.IOfferRowCoreFields = class { }
/**
 * The _IOfferRow_'s memory.
 */
xyz.swapee.wc.IOfferRowCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOfferRowCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IOfferRowCoreFields.prototype.props = /** @type {xyz.swapee.wc.IOfferRowCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowCore} */
xyz.swapee.wc.RecordIOfferRowCore

/** @typedef {xyz.swapee.wc.IOfferRowCore} xyz.swapee.wc.BoundIOfferRowCore */

/** @typedef {xyz.swapee.wc.OfferRowCore} xyz.swapee.wc.BoundOfferRowCore */

/** @typedef {xyz.swapee.wc.IOfferRowOuterCore.Model} xyz.swapee.wc.IOfferRowCore.Model The _IOfferRow_'s memory. */

/**
 * Contains getters to cast the _IOfferRowCore_ interface.
 * @interface xyz.swapee.wc.IOfferRowCoreCaster
 */
xyz.swapee.wc.IOfferRowCoreCaster = class { }
/**
 * Cast the _IOfferRowCore_ instance into the _BoundIOfferRowCore_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowCore}
 */
xyz.swapee.wc.IOfferRowCoreCaster.prototype.asIOfferRowCore
/**
 * Access the _OfferRowCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowCore}
 */
xyz.swapee.wc.IOfferRowCoreCaster.prototype.superOfferRowCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferRowCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowCore.__resetCore<!xyz.swapee.wc.IOfferRowCore>} xyz.swapee.wc.IOfferRowCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IOfferRowCore.resetCore} */
/**
 * Resets the _IOfferRow_ core.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore<!xyz.swapee.wc.IOfferRowCore>} xyz.swapee.wc.IOfferRowCore._resetOfferRowCore */
/** @typedef {typeof xyz.swapee.wc.IOfferRowCore.resetOfferRowCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowCore.resetOfferRowCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml}  8cd8ce57ccabe1263ae474971ff19a0a */
/** @typedef {xyz.swapee.wc.IOfferRowComputer.Initialese&xyz.swapee.wc.IOfferRowController.Initialese} xyz.swapee.wc.IOfferRowProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowProcessor)} xyz.swapee.wc.AbstractOfferRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowProcessor} xyz.swapee.wc.OfferRowProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowProcessor
 */
xyz.swapee.wc.AbstractOfferRowProcessor = class extends /** @type {xyz.swapee.wc.AbstractOfferRowProcessor.constructor&xyz.swapee.wc.OfferRowProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowProcessor.prototype.constructor = xyz.swapee.wc.AbstractOfferRowProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowProcessor.Initialese[]) => xyz.swapee.wc.IOfferRowProcessor} xyz.swapee.wc.OfferRowProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferRowProcessorCaster&xyz.swapee.wc.IOfferRowComputer&xyz.swapee.wc.IOfferRowCore&xyz.swapee.wc.IOfferRowController)} xyz.swapee.wc.IOfferRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowController} xyz.swapee.wc.IOfferRowController.typeof */
/**
 * The processor to compute changes to the memory for the _IOfferRow_.
 * @interface xyz.swapee.wc.IOfferRowProcessor
 */
xyz.swapee.wc.IOfferRowProcessor = class extends /** @type {xyz.swapee.wc.IOfferRowProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferRowComputer.typeof&xyz.swapee.wc.IOfferRowCore.typeof&xyz.swapee.wc.IOfferRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowProcessor.Initialese>)} xyz.swapee.wc.OfferRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowProcessor} xyz.swapee.wc.IOfferRowProcessor.typeof */
/**
 * A concrete class of _IOfferRowProcessor_ instances.
 * @constructor xyz.swapee.wc.OfferRowProcessor
 * @implements {xyz.swapee.wc.IOfferRowProcessor} The processor to compute changes to the memory for the _IOfferRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowProcessor.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowProcessor = class extends /** @type {xyz.swapee.wc.OfferRowProcessor.constructor&xyz.swapee.wc.IOfferRowProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.OfferRowProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferRowProcessor} */
xyz.swapee.wc.RecordIOfferRowProcessor

/** @typedef {xyz.swapee.wc.IOfferRowProcessor} xyz.swapee.wc.BoundIOfferRowProcessor */

/** @typedef {xyz.swapee.wc.OfferRowProcessor} xyz.swapee.wc.BoundOfferRowProcessor */

/**
 * Contains getters to cast the _IOfferRowProcessor_ interface.
 * @interface xyz.swapee.wc.IOfferRowProcessorCaster
 */
xyz.swapee.wc.IOfferRowProcessorCaster = class { }
/**
 * Cast the _IOfferRowProcessor_ instance into the _BoundIOfferRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowProcessor}
 */
xyz.swapee.wc.IOfferRowProcessorCaster.prototype.asIOfferRowProcessor
/**
 * Access the _OfferRowProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowProcessor}
 */
xyz.swapee.wc.IOfferRowProcessorCaster.prototype.superOfferRowProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/100-OfferRowMemory.xml}  dd6c3b0cc30e7f8b22116642c3e4be0f */
/**
 * The memory of the _IOfferRow_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.OfferRowMemory
 */
xyz.swapee.wc.OfferRowMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.OfferRowMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/102-OfferRowInputs.xml}  ac9945ff9e3cb0ee98b9a82d4b2c817c */
/**
 * The inputs of the _IOfferRow_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.OfferRowInputs
 */
xyz.swapee.wc.front.OfferRowInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.OfferRowInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml}  9cb88428f2e7f8c2419673d48954b536 */
/**
 * An atomic wrapper for the _IOfferRow_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.OfferRowEnv
 */
xyz.swapee.wc.OfferRowEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.OfferRowEnv.prototype.offerRow = /** @type {xyz.swapee.wc.IOfferRow} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs>&xyz.swapee.wc.IOfferRowProcessor.Initialese&xyz.swapee.wc.IOfferRowComputer.Initialese&xyz.swapee.wc.IOfferRowController.Initialese} xyz.swapee.wc.IOfferRow.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferRow)} xyz.swapee.wc.AbstractOfferRow.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRow} xyz.swapee.wc.OfferRow.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRow` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRow
 */
xyz.swapee.wc.AbstractOfferRow = class extends /** @type {xyz.swapee.wc.AbstractOfferRow.constructor&xyz.swapee.wc.OfferRow.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRow.prototype.constructor = xyz.swapee.wc.AbstractOfferRow
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRow.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRow} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRow}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRow.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRow.Initialese[]) => xyz.swapee.wc.IOfferRow} xyz.swapee.wc.OfferRowConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRow.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IOfferRow.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IOfferRow.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IOfferRow.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.OfferRowMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.OfferRowClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowCaster&xyz.swapee.wc.IOfferRowProcessor&xyz.swapee.wc.IOfferRowComputer&xyz.swapee.wc.IOfferRowController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, null>)} xyz.swapee.wc.IOfferRow.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IOfferRow
 */
xyz.swapee.wc.IOfferRow = class extends /** @type {xyz.swapee.wc.IOfferRow.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferRowProcessor.typeof&xyz.swapee.wc.IOfferRowComputer.typeof&xyz.swapee.wc.IOfferRowController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRow.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRow&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRow.Initialese>)} xyz.swapee.wc.OfferRow.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRow} xyz.swapee.wc.IOfferRow.typeof */
/**
 * A concrete class of _IOfferRow_ instances.
 * @constructor xyz.swapee.wc.OfferRow
 * @implements {xyz.swapee.wc.IOfferRow} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRow.Initialese>} ‎
 */
xyz.swapee.wc.OfferRow = class extends /** @type {xyz.swapee.wc.OfferRow.constructor&xyz.swapee.wc.IOfferRow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRow* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRow.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.OfferRow.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRow.
 * @interface xyz.swapee.wc.IOfferRowFields
 */
xyz.swapee.wc.IOfferRowFields = class { }
/**
 * The input pins of the _IOfferRow_ port.
 */
xyz.swapee.wc.IOfferRowFields.prototype.pinout = /** @type {!xyz.swapee.wc.IOfferRow.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRow} */
xyz.swapee.wc.RecordIOfferRow

/** @typedef {xyz.swapee.wc.IOfferRow} xyz.swapee.wc.BoundIOfferRow */

/** @typedef {xyz.swapee.wc.OfferRow} xyz.swapee.wc.BoundOfferRow */

/** @typedef {xyz.swapee.wc.IOfferRowController.Inputs} xyz.swapee.wc.IOfferRow.Pinout The input pins of the _IOfferRow_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>)} xyz.swapee.wc.IOfferRowBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IOfferRowBuffer
 */
xyz.swapee.wc.IOfferRowBuffer = class extends /** @type {xyz.swapee.wc.IOfferRowBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowBuffer.prototype.constructor = xyz.swapee.wc.IOfferRowBuffer

/**
 * A concrete class of _IOfferRowBuffer_ instances.
 * @constructor xyz.swapee.wc.OfferRowBuffer
 * @implements {xyz.swapee.wc.IOfferRowBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.OfferRowBuffer = class extends xyz.swapee.wc.IOfferRowBuffer { }
xyz.swapee.wc.OfferRowBuffer.prototype.constructor = xyz.swapee.wc.OfferRowBuffer

/**
 * Contains getters to cast the _IOfferRow_ interface.
 * @interface xyz.swapee.wc.IOfferRowCaster
 */
xyz.swapee.wc.IOfferRowCaster = class { }
/**
 * Cast the _IOfferRow_ instance into the _BoundIOfferRow_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRow}
 */
xyz.swapee.wc.IOfferRowCaster.prototype.asIOfferRow
/**
 * Access the _OfferRow_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRow}
 */
xyz.swapee.wc.IOfferRowCaster.prototype.superOfferRow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferRowMemoryPQs
 */
xyz.swapee.wc.OfferRowMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OfferRowMemoryPQs.prototype.constructor = xyz.swapee.wc.OfferRowMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferRowMemoryQPs
 * @dict
 */
xyz.swapee.wc.OfferRowMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.OfferRowMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.OfferRowMemoryPQs)} xyz.swapee.wc.OfferRowInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowMemoryPQs} xyz.swapee.wc.OfferRowMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferRowInputsPQs
 */
xyz.swapee.wc.OfferRowInputsPQs = class extends /** @type {xyz.swapee.wc.OfferRowInputsPQs.constructor&xyz.swapee.wc.OfferRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OfferRowInputsPQs.prototype.constructor = xyz.swapee.wc.OfferRowInputsPQs

/** @typedef {function(new: xyz.swapee.wc.OfferRowMemoryPQs)} xyz.swapee.wc.OfferRowInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferRowInputsQPs
 * @dict
 */
xyz.swapee.wc.OfferRowInputsQPs = class extends /** @type {xyz.swapee.wc.OfferRowInputsQPs.constructor&xyz.swapee.wc.OfferRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OfferRowInputsQPs.prototype.constructor = xyz.swapee.wc.OfferRowInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferRowVdusPQs
 */
xyz.swapee.wc.OfferRowVdusPQs = class {
  constructor() {
    /**
     * `h8a71`
     */
    this.OfferCrypto=/** @type {string} */ (void 0)
    /**
     * `h8a72`
     */
    this.OfferAmount=/** @type {string} */ (void 0)
    /**
     * `h8a73`
     */
    this.LogoDark=/** @type {string} */ (void 0)
    /**
     * `h8a74`
     */
    this.LogoLight=/** @type {string} */ (void 0)
    /**
     * `h8a75`
     */
    this.Eta=/** @type {string} */ (void 0)
    /**
     * `h8a76`
     */
    this.FloatingIco=/** @type {string} */ (void 0)
    /**
     * `h8a77`
     */
    this.FixedIco=/** @type {string} */ (void 0)
    /**
     * `h8a78`
     */
    this.FloatingLa=/** @type {string} */ (void 0)
    /**
     * `h8a79`
     */
    this.FixedLa=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OfferRowVdusPQs.prototype.constructor = xyz.swapee.wc.OfferRowVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferRowVdusQPs
 * @dict
 */
xyz.swapee.wc.OfferRowVdusQPs = class { }
/**
 * `OfferCrypto`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a71 = /** @type {string} */ (void 0)
/**
 * `OfferAmount`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a72 = /** @type {string} */ (void 0)
/**
 * `LogoDark`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a73 = /** @type {string} */ (void 0)
/**
 * `LogoLight`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a74 = /** @type {string} */ (void 0)
/**
 * `Eta`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a75 = /** @type {string} */ (void 0)
/**
 * `FloatingIco`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a76 = /** @type {string} */ (void 0)
/**
 * `FixedIco`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a77 = /** @type {string} */ (void 0)
/**
 * `FloatingLa`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a78 = /** @type {string} */ (void 0)
/**
 * `FixedLa`
 */
xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a79 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml}  584d450b1e202ae3f31b2031b74d72f2 */
/** @typedef {xyz.swapee.wc.back.IOfferRowController.Initialese&xyz.swapee.wc.back.IOfferRowScreen.Initialese&xyz.swapee.wc.IOfferRow.Initialese&xyz.swapee.wc.IOfferRowGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IOfferRowProcessor.Initialese&xyz.swapee.wc.IOfferRowComputer.Initialese} xyz.swapee.wc.IOfferRowHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowHtmlComponent)} xyz.swapee.wc.AbstractOfferRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowHtmlComponent} xyz.swapee.wc.OfferRowHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowHtmlComponent
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractOfferRowHtmlComponent.constructor&xyz.swapee.wc.OfferRowHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractOfferRowHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowHtmlComponent.Initialese[]) => xyz.swapee.wc.IOfferRowHtmlComponent} xyz.swapee.wc.OfferRowHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferRowHtmlComponentCaster&xyz.swapee.wc.back.IOfferRowController&xyz.swapee.wc.back.IOfferRowScreen&xyz.swapee.wc.IOfferRow&xyz.swapee.wc.IOfferRowGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IOfferRowProcessor&xyz.swapee.wc.IOfferRowComputer)} xyz.swapee.wc.IOfferRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowController} xyz.swapee.wc.back.IOfferRowController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowScreen} xyz.swapee.wc.back.IOfferRowScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowGPU} xyz.swapee.wc.IOfferRowGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IOfferRow_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IOfferRowHtmlComponent
 */
xyz.swapee.wc.IOfferRowHtmlComponent = class extends /** @type {xyz.swapee.wc.IOfferRowHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOfferRowController.typeof&xyz.swapee.wc.back.IOfferRowScreen.typeof&xyz.swapee.wc.IOfferRow.typeof&xyz.swapee.wc.IOfferRowGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IOfferRowProcessor.typeof&xyz.swapee.wc.IOfferRowComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese>)} xyz.swapee.wc.OfferRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowHtmlComponent} xyz.swapee.wc.IOfferRowHtmlComponent.typeof */
/**
 * A concrete class of _IOfferRowHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.OfferRowHtmlComponent
 * @implements {xyz.swapee.wc.IOfferRowHtmlComponent} The _IOfferRow_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowHtmlComponent = class extends /** @type {xyz.swapee.wc.OfferRowHtmlComponent.constructor&xyz.swapee.wc.IOfferRowHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.OfferRowHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferRowHtmlComponent} */
xyz.swapee.wc.RecordIOfferRowHtmlComponent

/** @typedef {xyz.swapee.wc.IOfferRowHtmlComponent} xyz.swapee.wc.BoundIOfferRowHtmlComponent */

/** @typedef {xyz.swapee.wc.OfferRowHtmlComponent} xyz.swapee.wc.BoundOfferRowHtmlComponent */

/**
 * Contains getters to cast the _IOfferRowHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IOfferRowHtmlComponentCaster
 */
xyz.swapee.wc.IOfferRowHtmlComponentCaster = class { }
/**
 * Cast the _IOfferRowHtmlComponent_ instance into the _BoundIOfferRowHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowHtmlComponent}
 */
xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.asIOfferRowHtmlComponent
/**
 * Access the _OfferRowHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowHtmlComponent}
 */
xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.superOfferRowHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml}  aedfbfea40cb5c42cffbec20c43ba60c */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IOfferRowElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowElement)} xyz.swapee.wc.AbstractOfferRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowElement} xyz.swapee.wc.OfferRowElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowElement` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowElement
 */
xyz.swapee.wc.AbstractOfferRowElement = class extends /** @type {xyz.swapee.wc.AbstractOfferRowElement.constructor&xyz.swapee.wc.OfferRowElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowElement.prototype.constructor = xyz.swapee.wc.AbstractOfferRowElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowElement.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowElement.Initialese[]) => xyz.swapee.wc.IOfferRowElement} xyz.swapee.wc.OfferRowElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs, null>)} xyz.swapee.wc.IOfferRowElement.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 *
 * The _IOfferRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IOfferRowElement
 */
xyz.swapee.wc.IOfferRowElement = class extends /** @type {xyz.swapee.wc.IOfferRowElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferRowElement.solder} */
xyz.swapee.wc.IOfferRowElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IOfferRowElement.render} */
xyz.swapee.wc.IOfferRowElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IOfferRowElement.server} */
xyz.swapee.wc.IOfferRowElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IOfferRowElement.inducer} */
xyz.swapee.wc.IOfferRowElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElement&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElement.Initialese>)} xyz.swapee.wc.OfferRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElement} xyz.swapee.wc.IOfferRowElement.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferRowElement_ instances.
 * @constructor xyz.swapee.wc.OfferRowElement
 * @implements {xyz.swapee.wc.IOfferRowElement} A component description.
 *
 * The _IOfferRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElement.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowElement = class extends /** @type {xyz.swapee.wc.OfferRowElement.constructor&xyz.swapee.wc.IOfferRowElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.OfferRowElement.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowElement.
 * @interface xyz.swapee.wc.IOfferRowElementFields
 */
xyz.swapee.wc.IOfferRowElementFields = class { }
/**
 * The element-specific inputs to the _IOfferRow_ component.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferRowElement.Inputs} */ (void 0)
/**
 * The base prefix for _exchangeCollapsarHandle_ selectors. Default `x-exchange-collapsar-handle`.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.X_EXCHANGE_COLLAPSAR_HANDLE = /** @type {string} */ (void 0)
/**
 * The auto-generated _x-attribute_ value for selecting which is guaranteed
 * to be unique on the page.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.xExchangeCollapsarHandle = /** @type {string} */ (void 0)
/**
 * The actual selector based on the _x-attribute_, such as `[x-info-5]`.
 * The handles selector for exchange collapsar.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.exchangeCollapsarHandleSel = /** @type {string} */ (void 0)
/**
 * The base prefix for _recommendationInfoHandle_ selectors. Default `x-recommendation-info-handle`.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.X_RECOMMENDATION_INFO_HANDLE = /** @type {string} */ (void 0)
/**
 * The auto-generated _x-attribute_ value for selecting which is guaranteed
 * to be unique on the page.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.xRecommendationInfoHandle = /** @type {string} */ (void 0)
/**
 * The actual selector based on the _x-attribute_, such as `[x-info-5]`.
 * The handle for the recommendation info.
 */
xyz.swapee.wc.IOfferRowElementFields.prototype.recommendationInfoHandleSel = /** @type {string} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowElement} */
xyz.swapee.wc.RecordIOfferRowElement

/** @typedef {xyz.swapee.wc.IOfferRowElement} xyz.swapee.wc.BoundIOfferRowElement */

/** @typedef {xyz.swapee.wc.OfferRowElement} xyz.swapee.wc.BoundOfferRowElement */

/** @typedef {xyz.swapee.wc.IOfferRowPort.Inputs&xyz.swapee.wc.IOfferRowDisplay.Queries&xyz.swapee.wc.IOfferRowController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IOfferRowElementPort.Inputs} xyz.swapee.wc.IOfferRowElement.Inputs The element-specific inputs to the _IOfferRow_ component. */

/**
 * Contains getters to cast the _IOfferRowElement_ interface.
 * @interface xyz.swapee.wc.IOfferRowElementCaster
 */
xyz.swapee.wc.IOfferRowElementCaster = class { }
/**
 * Cast the _IOfferRowElement_ instance into the _BoundIOfferRowElement_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowElement}
 */
xyz.swapee.wc.IOfferRowElementCaster.prototype.asIOfferRowElement
/**
 * Access the _OfferRowElement_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowElement}
 */
xyz.swapee.wc.IOfferRowElementCaster.prototype.superOfferRowElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OfferRowMemory, props: !xyz.swapee.wc.IOfferRowElement.Inputs) => Object<string, *>} xyz.swapee.wc.IOfferRowElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowElement.__solder<!xyz.swapee.wc.IOfferRowElement>} xyz.swapee.wc.IOfferRowElement._solder */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.OfferRowMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferRowOuterCore.WeakModel.Core* ⤴ *IOfferRowOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[fixed=false]` _boolean?_ Whether this is a fixed-rate offer. ⤴ *IOfferRowElementPort.Inputs.Fixed* Default `false`.
 * - `[recommended=false]` _boolean?_ Whether this is a recommended offer that will be highlighted. ⤴ *IOfferRowElementPort.Inputs.Recommended* Default `false`.
 * - `[exchangeCollapsarOpts]` _!Object?_ The options to pass to the _ExchangeCollapsar_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeCollapsarOpts* Default `{}`.
 * - `[exchangeOpts]` _!Object?_ The options to pass to the _Exchange_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeOpts* Default `{}`.
 * - `[bestOfferPlaqueOpts]` _!Object?_ The options to pass to the _BestOfferPlaque_ vdu. ⤴ *IOfferRowElementPort.Inputs.BestOfferPlaqueOpts* Default `{}`.
 * - `[progressVertLineOpts]` _!Object?_ The options to pass to the _ProgressVertLine_ vdu. ⤴ *IOfferRowElementPort.Inputs.ProgressVertLineOpts* Default `{}`.
 * - `[coinImWrOpts]` _!Object?_ The options to pass to the _CoinImWr_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImWrOpts* Default `{}`.
 * - `[getDealLaOpts]` _!Object?_ The options to pass to the _GetDealLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealLaOpts* Default `{}`.
 * - `[cancelLaOpts]` _!Object?_ The options to pass to the _CancelLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.CancelLaOpts* Default `{}`.
 * - `[offerCryptoOpts]` _!Object?_ The options to pass to the _OfferCrypto_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferCryptoOpts* Default `{}`.
 * - `[offerAmountOpts]` _!Object?_ The options to pass to the _OfferAmount_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferAmountOpts* Default `{}`.
 * - `[logoOpts]` _!Object?_ The options to pass to the _Logo_ vdu. ⤴ *IOfferRowElementPort.Inputs.LogoOpts* Default `{}`.
 * - `[etaOpts]` _!Object?_ The options to pass to the _Eta_ vdu. ⤴ *IOfferRowElementPort.Inputs.EtaOpts* Default `{}`.
 * - `[getDealOpts]` _!Object?_ The options to pass to the _GetDeal_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealOpts* Default `{}`.
 * - `[recHandleOpts]` _!Object?_ The options to pass to the _RecHandle_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecHandleOpts* Default `{}`.
 * - `[recPopupOpts]` _!Object?_ The options to pass to the _RecPopup_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecPopupOpts* Default `{}`.
 * - `[floatingIcoOpts]` _!Object?_ The options to pass to the _FloatingIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingIcoOpts* Default `{}`.
 * - `[fixedIcoOpts]` _!Object?_ The options to pass to the _FixedIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedIcoOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedLaOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IOfferRowElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OfferRowMemory, instance?: !xyz.swapee.wc.IOfferRowScreen&xyz.swapee.wc.IOfferRowController) => !engineering.type.VNode} xyz.swapee.wc.IOfferRowElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowElement.__render<!xyz.swapee.wc.IOfferRowElement>} xyz.swapee.wc.IOfferRowElement._render */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.OfferRowMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferRowScreen&xyz.swapee.wc.IOfferRowController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOfferRowElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OfferRowMemory, inputs: !xyz.swapee.wc.IOfferRowElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IOfferRowElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowElement.__server<!xyz.swapee.wc.IOfferRowElement>} xyz.swapee.wc.IOfferRowElement._server */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.OfferRowMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferRowOuterCore.WeakModel.Core* ⤴ *IOfferRowOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[fixed=false]` _boolean?_ Whether this is a fixed-rate offer. ⤴ *IOfferRowElementPort.Inputs.Fixed* Default `false`.
 * - `[recommended=false]` _boolean?_ Whether this is a recommended offer that will be highlighted. ⤴ *IOfferRowElementPort.Inputs.Recommended* Default `false`.
 * - `[exchangeCollapsarOpts]` _!Object?_ The options to pass to the _ExchangeCollapsar_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeCollapsarOpts* Default `{}`.
 * - `[exchangeOpts]` _!Object?_ The options to pass to the _Exchange_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeOpts* Default `{}`.
 * - `[bestOfferPlaqueOpts]` _!Object?_ The options to pass to the _BestOfferPlaque_ vdu. ⤴ *IOfferRowElementPort.Inputs.BestOfferPlaqueOpts* Default `{}`.
 * - `[progressVertLineOpts]` _!Object?_ The options to pass to the _ProgressVertLine_ vdu. ⤴ *IOfferRowElementPort.Inputs.ProgressVertLineOpts* Default `{}`.
 * - `[coinImWrOpts]` _!Object?_ The options to pass to the _CoinImWr_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImWrOpts* Default `{}`.
 * - `[getDealLaOpts]` _!Object?_ The options to pass to the _GetDealLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealLaOpts* Default `{}`.
 * - `[cancelLaOpts]` _!Object?_ The options to pass to the _CancelLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.CancelLaOpts* Default `{}`.
 * - `[offerCryptoOpts]` _!Object?_ The options to pass to the _OfferCrypto_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferCryptoOpts* Default `{}`.
 * - `[offerAmountOpts]` _!Object?_ The options to pass to the _OfferAmount_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferAmountOpts* Default `{}`.
 * - `[logoOpts]` _!Object?_ The options to pass to the _Logo_ vdu. ⤴ *IOfferRowElementPort.Inputs.LogoOpts* Default `{}`.
 * - `[etaOpts]` _!Object?_ The options to pass to the _Eta_ vdu. ⤴ *IOfferRowElementPort.Inputs.EtaOpts* Default `{}`.
 * - `[getDealOpts]` _!Object?_ The options to pass to the _GetDeal_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealOpts* Default `{}`.
 * - `[recHandleOpts]` _!Object?_ The options to pass to the _RecHandle_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecHandleOpts* Default `{}`.
 * - `[recPopupOpts]` _!Object?_ The options to pass to the _RecPopup_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecPopupOpts* Default `{}`.
 * - `[floatingIcoOpts]` _!Object?_ The options to pass to the _FloatingIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingIcoOpts* Default `{}`.
 * - `[fixedIcoOpts]` _!Object?_ The options to pass to the _FixedIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedIcoOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedLaOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOfferRowElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OfferRowMemory, port?: !xyz.swapee.wc.IOfferRowElement.Inputs) => ?} xyz.swapee.wc.IOfferRowElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowElement.__inducer<!xyz.swapee.wc.IOfferRowElement>} xyz.swapee.wc.IOfferRowElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.OfferRowMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferRowOuterCore.WeakModel.Core* ⤴ *IOfferRowOuterCore.WeakModel.Core* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[fixed=false]` _boolean?_ Whether this is a fixed-rate offer. ⤴ *IOfferRowElementPort.Inputs.Fixed* Default `false`.
 * - `[recommended=false]` _boolean?_ Whether this is a recommended offer that will be highlighted. ⤴ *IOfferRowElementPort.Inputs.Recommended* Default `false`.
 * - `[exchangeCollapsarOpts]` _!Object?_ The options to pass to the _ExchangeCollapsar_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeCollapsarOpts* Default `{}`.
 * - `[exchangeOpts]` _!Object?_ The options to pass to the _Exchange_ vdu. ⤴ *IOfferRowElementPort.Inputs.ExchangeOpts* Default `{}`.
 * - `[bestOfferPlaqueOpts]` _!Object?_ The options to pass to the _BestOfferPlaque_ vdu. ⤴ *IOfferRowElementPort.Inputs.BestOfferPlaqueOpts* Default `{}`.
 * - `[progressVertLineOpts]` _!Object?_ The options to pass to the _ProgressVertLine_ vdu. ⤴ *IOfferRowElementPort.Inputs.ProgressVertLineOpts* Default `{}`.
 * - `[coinImWrOpts]` _!Object?_ The options to pass to the _CoinImWr_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImWrOpts* Default `{}`.
 * - `[getDealLaOpts]` _!Object?_ The options to pass to the _GetDealLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealLaOpts* Default `{}`.
 * - `[cancelLaOpts]` _!Object?_ The options to pass to the _CancelLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.CancelLaOpts* Default `{}`.
 * - `[offerCryptoOpts]` _!Object?_ The options to pass to the _OfferCrypto_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferCryptoOpts* Default `{}`.
 * - `[offerAmountOpts]` _!Object?_ The options to pass to the _OfferAmount_ vdu. ⤴ *IOfferRowElementPort.Inputs.OfferAmountOpts* Default `{}`.
 * - `[logoOpts]` _!Object?_ The options to pass to the _Logo_ vdu. ⤴ *IOfferRowElementPort.Inputs.LogoOpts* Default `{}`.
 * - `[etaOpts]` _!Object?_ The options to pass to the _Eta_ vdu. ⤴ *IOfferRowElementPort.Inputs.EtaOpts* Default `{}`.
 * - `[getDealOpts]` _!Object?_ The options to pass to the _GetDeal_ vdu. ⤴ *IOfferRowElementPort.Inputs.GetDealOpts* Default `{}`.
 * - `[recHandleOpts]` _!Object?_ The options to pass to the _RecHandle_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecHandleOpts* Default `{}`.
 * - `[recPopupOpts]` _!Object?_ The options to pass to the _RecPopup_ vdu. ⤴ *IOfferRowElementPort.Inputs.RecPopupOpts* Default `{}`.
 * - `[floatingIcoOpts]` _!Object?_ The options to pass to the _FloatingIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingIcoOpts* Default `{}`.
 * - `[fixedIcoOpts]` _!Object?_ The options to pass to the _FixedIco_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedIcoOpts* Default `{}`.
 * - `[coinImOpts]` _!Object?_ The options to pass to the _CoinIm_ vdu. ⤴ *IOfferRowElementPort.Inputs.CoinImOpts* Default `{}`.
 * - `[floatingLaOpts]` _!Object?_ The options to pass to the _FloatingLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FloatingLaOpts* Default `{}`.
 * - `[fixedLaOpts]` _!Object?_ The options to pass to the _FixedLa_ vdu. ⤴ *IOfferRowElementPort.Inputs.FixedLaOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IOfferRowElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/140-IOfferRowElementPort.xml}  844e3a823d24887f6f52f62fb176f812 */
/**
 * The options to pass to the _GetDealBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts.getDealBuOpts

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts The options to pass to the _GetDealBu_ vdu (optional overlay).
 * @prop {!Object} [getDealBuOpts] The options to pass to the _GetDealBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuOpts_Safe The options to pass to the _GetDealBu_ vdu (required overlay).
 * @prop {!Object} getDealBuOpts The options to pass to the _GetDealBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts The options to pass to the _GetDealBu_ vdu (optional overlay).
 * @prop {*} [getDealBuOpts=null] The options to pass to the _GetDealBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuOpts_Safe The options to pass to the _GetDealBu_ vdu (required overlay).
 * @prop {*} getDealBuOpts The options to pass to the _GetDealBu_ vdu.
 */

/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOfferRowElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowElementPort)} xyz.swapee.wc.AbstractOfferRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowElementPort} xyz.swapee.wc.OfferRowElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowElementPort
 */
xyz.swapee.wc.AbstractOfferRowElementPort = class extends /** @type {xyz.swapee.wc.AbstractOfferRowElementPort.constructor&xyz.swapee.wc.OfferRowElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowElementPort.prototype.constructor = xyz.swapee.wc.AbstractOfferRowElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowElementPort|typeof xyz.swapee.wc.OfferRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.AbstractOfferRowElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowElementPort.Initialese[]) => xyz.swapee.wc.IOfferRowElementPort} xyz.swapee.wc.OfferRowElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferRowElementPort.Inputs>)} xyz.swapee.wc.IOfferRowElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IOfferRowElementPort
 */
xyz.swapee.wc.IOfferRowElementPort = class extends /** @type {xyz.swapee.wc.IOfferRowElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElementPort.Initialese>)} xyz.swapee.wc.OfferRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort} xyz.swapee.wc.IOfferRowElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferRowElementPort_ instances.
 * @constructor xyz.swapee.wc.OfferRowElementPort
 * @implements {xyz.swapee.wc.IOfferRowElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElementPort.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowElementPort = class extends /** @type {xyz.swapee.wc.OfferRowElementPort.constructor&xyz.swapee.wc.IOfferRowElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElementPort}
 */
xyz.swapee.wc.OfferRowElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowElementPort.
 * @interface xyz.swapee.wc.IOfferRowElementPortFields
 */
xyz.swapee.wc.IOfferRowElementPortFields = class { }
/**
 * The inputs to the _IOfferRowElement_'s controller via its element port.
 */
xyz.swapee.wc.IOfferRowElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferRowElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOfferRowElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOfferRowElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowElementPort} */
xyz.swapee.wc.RecordIOfferRowElementPort

/** @typedef {xyz.swapee.wc.IOfferRowElementPort} xyz.swapee.wc.BoundIOfferRowElementPort */

/** @typedef {xyz.swapee.wc.OfferRowElementPort} xyz.swapee.wc.BoundOfferRowElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.noSolder

/**
 * Whether this is a fixed-rate offer.
 * @typedef {boolean}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.fixed

/**
 * Whether this is a recommended offer that will be highlighted.
 * @typedef {boolean}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.recommended

/**
 * The options to pass to the _ExchangeCollapsar_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.exchangeCollapsarOpts

/**
 * The options to pass to the _OfferExchange_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.offerExchangeOpts

/**
 * The options to pass to the _BestOfferPlaque_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.bestOfferPlaqueOpts

/**
 * The options to pass to the _ProgressVertLine_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.progressVertLineOpts

/**
 * The options to pass to the _CoinImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.coinImWrOpts

/**
 * The options to pass to the _GetDealLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.getDealLaOpts

/**
 * The options to pass to the _CancelLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.cancelLaOpts

/**
 * The options to pass to the _GetDealBuA_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.getDealBuAOpts

/**
 * The options to pass to the _OfferCrypto_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.offerCryptoOpts

/**
 * The options to pass to the _OfferAmount_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.offerAmountOpts

/**
 * The options to pass to the _Logo_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.logoOpts

/**
 * The options to pass to the _Eta_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.etaOpts

/**
 * The options to pass to the _GetDeal_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.getDealOpts

/**
 * The options to pass to the _RecHandle_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.recHandleOpts

/**
 * The options to pass to the _RecPopup_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.recPopupOpts

/**
 * The options to pass to the _FloatingIco_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.floatingIcoOpts

/**
 * The options to pass to the _FixedIco_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.fixedIcoOpts

/**
 * The options to pass to the _CoinIm_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.coinImOpts

/**
 * The options to pass to the _FloatingLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.floatingLaOpts

/**
 * The options to pass to the _FixedLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.fixedLaOpts

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder&xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed&xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended&xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts&xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts)} xyz.swapee.wc.IOfferRowElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.typeof */
/**
 * The inputs to the _IOfferRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOfferRowElementPort.Inputs
 */
xyz.swapee.wc.IOfferRowElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IOfferRowElementPort.Inputs.constructor&xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IOfferRowElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts)} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts.typeof */
/**
 * The inputs to the _IOfferRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOfferRowElementPort.WeakInputs
 */
xyz.swapee.wc.IOfferRowElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOfferRowElementPort.WeakInputs.constructor&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts.typeof&xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferRowElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOfferRowElementPort.WeakInputs

/**
 * Contains getters to cast the _IOfferRowElementPort_ interface.
 * @interface xyz.swapee.wc.IOfferRowElementPortCaster
 */
xyz.swapee.wc.IOfferRowElementPortCaster = class { }
/**
 * Cast the _IOfferRowElementPort_ instance into the _BoundIOfferRowElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowElementPort}
 */
xyz.swapee.wc.IOfferRowElementPortCaster.prototype.asIOfferRowElementPort
/**
 * Access the _OfferRowElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowElementPort}
 */
xyz.swapee.wc.IOfferRowElementPortCaster.prototype.superOfferRowElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed Whether this is a fixed-rate offer (optional overlay).
 * @prop {boolean} [fixed=false] Whether this is a fixed-rate offer. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.Fixed_Safe Whether this is a fixed-rate offer (required overlay).
 * @prop {boolean} fixed Whether this is a fixed-rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended Whether this is a recommended offer that will be highlighted (optional overlay).
 * @prop {boolean} [recommended=false] Whether this is a recommended offer that will be highlighted. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.Recommended_Safe Whether this is a recommended offer that will be highlighted (required overlay).
 * @prop {boolean} recommended Whether this is a recommended offer that will be highlighted.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts The options to pass to the _ExchangeCollapsar_ vdu (optional overlay).
 * @prop {!Object} [exchangeCollapsarOpts] The options to pass to the _ExchangeCollapsar_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.ExchangeCollapsarOpts_Safe The options to pass to the _ExchangeCollapsar_ vdu (required overlay).
 * @prop {!Object} exchangeCollapsarOpts The options to pass to the _ExchangeCollapsar_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts The options to pass to the _OfferExchange_ vdu (optional overlay).
 * @prop {!Object} [offerExchangeOpts] The options to pass to the _OfferExchange_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferExchangeOpts_Safe The options to pass to the _OfferExchange_ vdu (required overlay).
 * @prop {!Object} offerExchangeOpts The options to pass to the _OfferExchange_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts The options to pass to the _BestOfferPlaque_ vdu (optional overlay).
 * @prop {!Object} [bestOfferPlaqueOpts] The options to pass to the _BestOfferPlaque_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.BestOfferPlaqueOpts_Safe The options to pass to the _BestOfferPlaque_ vdu (required overlay).
 * @prop {!Object} bestOfferPlaqueOpts The options to pass to the _BestOfferPlaque_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts The options to pass to the _ProgressVertLine_ vdu (optional overlay).
 * @prop {!Object} [progressVertLineOpts] The options to pass to the _ProgressVertLine_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.ProgressVertLineOpts_Safe The options to pass to the _ProgressVertLine_ vdu (required overlay).
 * @prop {!Object} progressVertLineOpts The options to pass to the _ProgressVertLine_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts The options to pass to the _CoinImWr_ vdu (optional overlay).
 * @prop {!Object} [coinImWrOpts] The options to pass to the _CoinImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImWrOpts_Safe The options to pass to the _CoinImWr_ vdu (required overlay).
 * @prop {!Object} coinImWrOpts The options to pass to the _CoinImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts The options to pass to the _GetDealLa_ vdu (optional overlay).
 * @prop {!Object} [getDealLaOpts] The options to pass to the _GetDealLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealLaOpts_Safe The options to pass to the _GetDealLa_ vdu (required overlay).
 * @prop {!Object} getDealLaOpts The options to pass to the _GetDealLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts The options to pass to the _CancelLa_ vdu (optional overlay).
 * @prop {!Object} [cancelLaOpts] The options to pass to the _CancelLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CancelLaOpts_Safe The options to pass to the _CancelLa_ vdu (required overlay).
 * @prop {!Object} cancelLaOpts The options to pass to the _CancelLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts The options to pass to the _GetDealBuA_ vdu (optional overlay).
 * @prop {!Object} [getDealBuAOpts] The options to pass to the _GetDealBuA_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealBuAOpts_Safe The options to pass to the _GetDealBuA_ vdu (required overlay).
 * @prop {!Object} getDealBuAOpts The options to pass to the _GetDealBuA_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts The options to pass to the _OfferCrypto_ vdu (optional overlay).
 * @prop {!Object} [offerCryptoOpts] The options to pass to the _OfferCrypto_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferCryptoOpts_Safe The options to pass to the _OfferCrypto_ vdu (required overlay).
 * @prop {!Object} offerCryptoOpts The options to pass to the _OfferCrypto_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts The options to pass to the _OfferAmount_ vdu (optional overlay).
 * @prop {!Object} [offerAmountOpts] The options to pass to the _OfferAmount_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.OfferAmountOpts_Safe The options to pass to the _OfferAmount_ vdu (required overlay).
 * @prop {!Object} offerAmountOpts The options to pass to the _OfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts The options to pass to the _Logo_ vdu (optional overlay).
 * @prop {!Object} [logoOpts] The options to pass to the _Logo_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.LogoOpts_Safe The options to pass to the _Logo_ vdu (required overlay).
 * @prop {!Object} logoOpts The options to pass to the _Logo_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts The options to pass to the _Eta_ vdu (optional overlay).
 * @prop {!Object} [etaOpts] The options to pass to the _Eta_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.EtaOpts_Safe The options to pass to the _Eta_ vdu (required overlay).
 * @prop {!Object} etaOpts The options to pass to the _Eta_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts The options to pass to the _GetDeal_ vdu (optional overlay).
 * @prop {!Object} [getDealOpts] The options to pass to the _GetDeal_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.GetDealOpts_Safe The options to pass to the _GetDeal_ vdu (required overlay).
 * @prop {!Object} getDealOpts The options to pass to the _GetDeal_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts The options to pass to the _RecHandle_ vdu (optional overlay).
 * @prop {!Object} [recHandleOpts] The options to pass to the _RecHandle_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecHandleOpts_Safe The options to pass to the _RecHandle_ vdu (required overlay).
 * @prop {!Object} recHandleOpts The options to pass to the _RecHandle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts The options to pass to the _RecPopup_ vdu (optional overlay).
 * @prop {!Object} [recPopupOpts] The options to pass to the _RecPopup_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.RecPopupOpts_Safe The options to pass to the _RecPopup_ vdu (required overlay).
 * @prop {!Object} recPopupOpts The options to pass to the _RecPopup_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts The options to pass to the _FloatingIco_ vdu (optional overlay).
 * @prop {!Object} [floatingIcoOpts] The options to pass to the _FloatingIco_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingIcoOpts_Safe The options to pass to the _FloatingIco_ vdu (required overlay).
 * @prop {!Object} floatingIcoOpts The options to pass to the _FloatingIco_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts The options to pass to the _FixedIco_ vdu (optional overlay).
 * @prop {!Object} [fixedIcoOpts] The options to pass to the _FixedIco_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedIcoOpts_Safe The options to pass to the _FixedIco_ vdu (required overlay).
 * @prop {!Object} fixedIcoOpts The options to pass to the _FixedIco_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts The options to pass to the _CoinIm_ vdu (optional overlay).
 * @prop {!Object} [coinImOpts] The options to pass to the _CoinIm_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.CoinImOpts_Safe The options to pass to the _CoinIm_ vdu (required overlay).
 * @prop {!Object} coinImOpts The options to pass to the _CoinIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts The options to pass to the _FloatingLa_ vdu (optional overlay).
 * @prop {!Object} [floatingLaOpts] The options to pass to the _FloatingLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FloatingLaOpts_Safe The options to pass to the _FloatingLa_ vdu (required overlay).
 * @prop {!Object} floatingLaOpts The options to pass to the _FloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts The options to pass to the _FixedLa_ vdu (optional overlay).
 * @prop {!Object} [fixedLaOpts] The options to pass to the _FixedLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.Inputs.FixedLaOpts_Safe The options to pass to the _FixedLa_ vdu (required overlay).
 * @prop {!Object} fixedLaOpts The options to pass to the _FixedLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed Whether this is a fixed-rate offer (optional overlay).
 * @prop {*} [fixed=null] Whether this is a fixed-rate offer. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Fixed_Safe Whether this is a fixed-rate offer (required overlay).
 * @prop {*} fixed Whether this is a fixed-rate offer.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended Whether this is a recommended offer that will be highlighted (optional overlay).
 * @prop {*} [recommended=null] Whether this is a recommended offer that will be highlighted. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.Recommended_Safe Whether this is a recommended offer that will be highlighted (required overlay).
 * @prop {*} recommended Whether this is a recommended offer that will be highlighted.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts The options to pass to the _ExchangeCollapsar_ vdu (optional overlay).
 * @prop {*} [exchangeCollapsarOpts=null] The options to pass to the _ExchangeCollapsar_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ExchangeCollapsarOpts_Safe The options to pass to the _ExchangeCollapsar_ vdu (required overlay).
 * @prop {*} exchangeCollapsarOpts The options to pass to the _ExchangeCollapsar_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts The options to pass to the _OfferExchange_ vdu (optional overlay).
 * @prop {*} [offerExchangeOpts=null] The options to pass to the _OfferExchange_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferExchangeOpts_Safe The options to pass to the _OfferExchange_ vdu (required overlay).
 * @prop {*} offerExchangeOpts The options to pass to the _OfferExchange_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts The options to pass to the _BestOfferPlaque_ vdu (optional overlay).
 * @prop {*} [bestOfferPlaqueOpts=null] The options to pass to the _BestOfferPlaque_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.BestOfferPlaqueOpts_Safe The options to pass to the _BestOfferPlaque_ vdu (required overlay).
 * @prop {*} bestOfferPlaqueOpts The options to pass to the _BestOfferPlaque_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts The options to pass to the _ProgressVertLine_ vdu (optional overlay).
 * @prop {*} [progressVertLineOpts=null] The options to pass to the _ProgressVertLine_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.ProgressVertLineOpts_Safe The options to pass to the _ProgressVertLine_ vdu (required overlay).
 * @prop {*} progressVertLineOpts The options to pass to the _ProgressVertLine_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts The options to pass to the _CoinImWr_ vdu (optional overlay).
 * @prop {*} [coinImWrOpts=null] The options to pass to the _CoinImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImWrOpts_Safe The options to pass to the _CoinImWr_ vdu (required overlay).
 * @prop {*} coinImWrOpts The options to pass to the _CoinImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts The options to pass to the _GetDealLa_ vdu (optional overlay).
 * @prop {*} [getDealLaOpts=null] The options to pass to the _GetDealLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealLaOpts_Safe The options to pass to the _GetDealLa_ vdu (required overlay).
 * @prop {*} getDealLaOpts The options to pass to the _GetDealLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts The options to pass to the _CancelLa_ vdu (optional overlay).
 * @prop {*} [cancelLaOpts=null] The options to pass to the _CancelLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CancelLaOpts_Safe The options to pass to the _CancelLa_ vdu (required overlay).
 * @prop {*} cancelLaOpts The options to pass to the _CancelLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts The options to pass to the _GetDealBuA_ vdu (optional overlay).
 * @prop {*} [getDealBuAOpts=null] The options to pass to the _GetDealBuA_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealBuAOpts_Safe The options to pass to the _GetDealBuA_ vdu (required overlay).
 * @prop {*} getDealBuAOpts The options to pass to the _GetDealBuA_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts The options to pass to the _OfferCrypto_ vdu (optional overlay).
 * @prop {*} [offerCryptoOpts=null] The options to pass to the _OfferCrypto_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferCryptoOpts_Safe The options to pass to the _OfferCrypto_ vdu (required overlay).
 * @prop {*} offerCryptoOpts The options to pass to the _OfferCrypto_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts The options to pass to the _OfferAmount_ vdu (optional overlay).
 * @prop {*} [offerAmountOpts=null] The options to pass to the _OfferAmount_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.OfferAmountOpts_Safe The options to pass to the _OfferAmount_ vdu (required overlay).
 * @prop {*} offerAmountOpts The options to pass to the _OfferAmount_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts The options to pass to the _Logo_ vdu (optional overlay).
 * @prop {*} [logoOpts=null] The options to pass to the _Logo_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.LogoOpts_Safe The options to pass to the _Logo_ vdu (required overlay).
 * @prop {*} logoOpts The options to pass to the _Logo_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts The options to pass to the _Eta_ vdu (optional overlay).
 * @prop {*} [etaOpts=null] The options to pass to the _Eta_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.EtaOpts_Safe The options to pass to the _Eta_ vdu (required overlay).
 * @prop {*} etaOpts The options to pass to the _Eta_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts The options to pass to the _GetDeal_ vdu (optional overlay).
 * @prop {*} [getDealOpts=null] The options to pass to the _GetDeal_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.GetDealOpts_Safe The options to pass to the _GetDeal_ vdu (required overlay).
 * @prop {*} getDealOpts The options to pass to the _GetDeal_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts The options to pass to the _RecHandle_ vdu (optional overlay).
 * @prop {*} [recHandleOpts=null] The options to pass to the _RecHandle_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecHandleOpts_Safe The options to pass to the _RecHandle_ vdu (required overlay).
 * @prop {*} recHandleOpts The options to pass to the _RecHandle_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts The options to pass to the _RecPopup_ vdu (optional overlay).
 * @prop {*} [recPopupOpts=null] The options to pass to the _RecPopup_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.RecPopupOpts_Safe The options to pass to the _RecPopup_ vdu (required overlay).
 * @prop {*} recPopupOpts The options to pass to the _RecPopup_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts The options to pass to the _FloatingIco_ vdu (optional overlay).
 * @prop {*} [floatingIcoOpts=null] The options to pass to the _FloatingIco_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingIcoOpts_Safe The options to pass to the _FloatingIco_ vdu (required overlay).
 * @prop {*} floatingIcoOpts The options to pass to the _FloatingIco_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts The options to pass to the _FixedIco_ vdu (optional overlay).
 * @prop {*} [fixedIcoOpts=null] The options to pass to the _FixedIco_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedIcoOpts_Safe The options to pass to the _FixedIco_ vdu (required overlay).
 * @prop {*} fixedIcoOpts The options to pass to the _FixedIco_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts The options to pass to the _CoinIm_ vdu (optional overlay).
 * @prop {*} [coinImOpts=null] The options to pass to the _CoinIm_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.CoinImOpts_Safe The options to pass to the _CoinIm_ vdu (required overlay).
 * @prop {*} coinImOpts The options to pass to the _CoinIm_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts The options to pass to the _FloatingLa_ vdu (optional overlay).
 * @prop {*} [floatingLaOpts=null] The options to pass to the _FloatingLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FloatingLaOpts_Safe The options to pass to the _FloatingLa_ vdu (required overlay).
 * @prop {*} floatingLaOpts The options to pass to the _FloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts The options to pass to the _FixedLa_ vdu (optional overlay).
 * @prop {*} [fixedLaOpts=null] The options to pass to the _FixedLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowElementPort.WeakInputs.FixedLaOpts_Safe The options to pass to the _FixedLa_ vdu (required overlay).
 * @prop {*} fixedLaOpts The options to pass to the _FixedLa_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml}  ad1d1910718a9aacadbc6b1a762e658f */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IOfferRowDesigner
 */
xyz.swapee.wc.IOfferRowDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.OfferRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOfferRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OfferRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOfferRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OfferRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `OfferRow` _typeof IOfferRowController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `OfferRow` _typeof IOfferRowController_
   * - `This` _typeof IOfferRowController_
   * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `OfferRow` _!OfferRowMemory_
   * - `This` _!OfferRowMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OfferRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OfferRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IOfferRowDesigner.prototype.constructor = xyz.swapee.wc.IOfferRowDesigner

/**
 * A concrete class of _IOfferRowDesigner_ instances.
 * @constructor xyz.swapee.wc.OfferRowDesigner
 * @implements {xyz.swapee.wc.IOfferRowDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.OfferRowDesigner = class extends xyz.swapee.wc.IOfferRowDesigner { }
xyz.swapee.wc.OfferRowDesigner.prototype.constructor = xyz.swapee.wc.OfferRowDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IOfferRowController} OfferRow
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IOfferRowController} OfferRow
 * @prop {typeof xyz.swapee.wc.IOfferRowController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferRowDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.OfferRowMemory} OfferRow
 * @prop {!xyz.swapee.wc.OfferRowMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml}  0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @typedef {Object} $xyz.swapee.wc.IOfferRowDisplay.Initialese
 * @prop {HTMLDivElement} [ExchangeCollapsar] The collapsar with the exchange.
 * @prop {HTMLDivElement} [OfferExchange] The container for the exchange.
 * @prop {HTMLDivElement} [BestOfferPlaque]
 * @prop {HTMLSpanElement} [ProgressVertLine]
 * @prop {HTMLDivElement} [CoinImWr]
 * @prop {HTMLSpanElement} [GetDealLa]
 * @prop {HTMLSpanElement} [CancelLa]
 * @prop {HTMLAnchorElement} [GetDealBuA]
 * @prop {HTMLSpanElement} [OfferCrypto]
 * @prop {HTMLSpanElement} [OfferAmount]
 * @prop {HTMLImageElement} [Logo]
 * @prop {HTMLSpanElement} [Eta]
 * @prop {HTMLDivElement} [GetDeal]
 * @prop {HTMLSpanElement} [RecHandle]
 * @prop {HTMLSpanElement} [RecPopup]
 * @prop {HTMLImageElement} [FloatingIco]
 * @prop {HTMLImageElement} [FixedIco]
 * @prop {HTMLElement} [CoinIm]
 * @prop {HTMLParagraphElement} [FloatingLa]
 * @prop {HTMLParagraphElement} [FixedLa]
 */
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings>} xyz.swapee.wc.IOfferRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferRowDisplay)} xyz.swapee.wc.AbstractOfferRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowDisplay} xyz.swapee.wc.OfferRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowDisplay
 */
xyz.swapee.wc.AbstractOfferRowDisplay = class extends /** @type {xyz.swapee.wc.AbstractOfferRowDisplay.constructor&xyz.swapee.wc.OfferRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowDisplay.prototype.constructor = xyz.swapee.wc.AbstractOfferRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowDisplay.Initialese[]) => xyz.swapee.wc.IOfferRowDisplay} xyz.swapee.wc.OfferRowDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.OfferRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, xyz.swapee.wc.IOfferRowDisplay.Queries, null>)} xyz.swapee.wc.IOfferRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IOfferRow_.
 * @interface xyz.swapee.wc.IOfferRowDisplay
 */
xyz.swapee.wc.IOfferRowDisplay = class extends /** @type {xyz.swapee.wc.IOfferRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferRowDisplay.paint} */
xyz.swapee.wc.IOfferRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowDisplay.Initialese>)} xyz.swapee.wc.OfferRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowDisplay} xyz.swapee.wc.IOfferRowDisplay.typeof */
/**
 * A concrete class of _IOfferRowDisplay_ instances.
 * @constructor xyz.swapee.wc.OfferRowDisplay
 * @implements {xyz.swapee.wc.IOfferRowDisplay} Display for presenting information from the _IOfferRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowDisplay = class extends /** @type {xyz.swapee.wc.OfferRowDisplay.constructor&xyz.swapee.wc.IOfferRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.OfferRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowDisplay.
 * @interface xyz.swapee.wc.IOfferRowDisplayFields
 */
xyz.swapee.wc.IOfferRowDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IOfferRowDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IOfferRowDisplay.Queries} */ (void 0)
/**
 * The collapsar with the exchange. Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.ExchangeCollapsar = /** @type {HTMLDivElement} */ (void 0)
/**
 * The container for the exchange. Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferExchange = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.BestOfferPlaque = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.ProgressVertLine = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinImWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.CancelLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealBuA = /** @type {HTMLAnchorElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferCrypto = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferAmount = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.Logo = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.Eta = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDeal = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecHandle = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecPopup = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingIco = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedIco = /** @type {HTMLImageElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinIm = /** @type {HTMLElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingLa = /** @type {HTMLParagraphElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedLa = /** @type {HTMLParagraphElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowDisplay} */
xyz.swapee.wc.RecordIOfferRowDisplay

/** @typedef {xyz.swapee.wc.IOfferRowDisplay} xyz.swapee.wc.BoundIOfferRowDisplay */

/** @typedef {xyz.swapee.wc.OfferRowDisplay} xyz.swapee.wc.BoundOfferRowDisplay */

/** @typedef {xyz.swapee.wc.IOfferRowDisplay.Queries} xyz.swapee.wc.IOfferRowDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferRowDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IOfferRowDisplay_ interface.
 * @interface xyz.swapee.wc.IOfferRowDisplayCaster
 */
xyz.swapee.wc.IOfferRowDisplayCaster = class { }
/**
 * Cast the _IOfferRowDisplay_ instance into the _BoundIOfferRowDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowDisplay}
 */
xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/**
 * Cast the _IOfferRowDisplay_ instance into the _BoundIOfferRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowScreen}
 */
xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowScreen
/**
 * Access the _OfferRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowDisplay}
 */
xyz.swapee.wc.IOfferRowDisplayCaster.prototype.superOfferRowDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OfferRowMemory, land: null) => void} xyz.swapee.wc.IOfferRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowDisplay.__paint<!xyz.swapee.wc.IOfferRowDisplay>} xyz.swapee.wc.IOfferRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IOfferRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OfferRowMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml}  0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IOfferRowDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeCollapsar] The collapsar with the exchange.
 * @prop {!com.webcircuits.IHtmlTwin} [OfferExchange] The container for the exchange.
 * @prop {!com.webcircuits.IHtmlTwin} [BestOfferPlaque]
 * @prop {!com.webcircuits.IHtmlTwin} [ProgressVertLine]
 * @prop {!com.webcircuits.IHtmlTwin} [CoinImWr]
 * @prop {!com.webcircuits.IHtmlTwin} [GetDealLa]
 * @prop {!com.webcircuits.IHtmlTwin} [CancelLa]
 * @prop {!com.webcircuits.IHtmlTwin} [GetDealBuA]
 * @prop {!com.webcircuits.IHtmlTwin} [OfferCrypto]
 * @prop {!com.webcircuits.IHtmlTwin} [OfferAmount]
 * @prop {!com.webcircuits.IHtmlTwin} [Logo]
 * @prop {!com.webcircuits.IHtmlTwin} [Eta]
 * @prop {!com.webcircuits.IHtmlTwin} [GetDeal]
 * @prop {!com.webcircuits.IHtmlTwin} [RecHandle]
 * @prop {!com.webcircuits.IHtmlTwin} [RecPopup]
 * @prop {!com.webcircuits.IHtmlTwin} [FloatingIco]
 * @prop {!com.webcircuits.IHtmlTwin} [FixedIco]
 * @prop {!com.webcircuits.IHtmlTwin} [CoinIm]
 * @prop {!com.webcircuits.IHtmlTwin} [FloatingLa]
 * @prop {!com.webcircuits.IHtmlTwin} [FixedLa]
 */
/** @typedef {$xyz.swapee.wc.back.IOfferRowDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OfferRowClasses>} xyz.swapee.wc.back.IOfferRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OfferRowDisplay)} xyz.swapee.wc.back.AbstractOfferRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferRowDisplay} xyz.swapee.wc.back.OfferRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferRowDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferRowDisplay
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractOfferRowDisplay.constructor&xyz.swapee.wc.back.OfferRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferRowDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractOfferRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IOfferRowDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.OfferRowClasses, null>)} xyz.swapee.wc.back.IOfferRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IOfferRowDisplay
 */
xyz.swapee.wc.back.IOfferRowDisplay = class extends /** @type {xyz.swapee.wc.back.IOfferRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IOfferRowDisplay.paint} */
xyz.swapee.wc.back.IOfferRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowDisplay.Initialese>)} xyz.swapee.wc.back.OfferRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowDisplay} xyz.swapee.wc.back.IOfferRowDisplay.typeof */
/**
 * A concrete class of _IOfferRowDisplay_ instances.
 * @constructor xyz.swapee.wc.back.OfferRowDisplay
 * @implements {xyz.swapee.wc.back.IOfferRowDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferRowDisplay = class extends /** @type {xyz.swapee.wc.back.OfferRowDisplay.constructor&xyz.swapee.wc.back.IOfferRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.OfferRowDisplay.prototype.constructor = xyz.swapee.wc.back.OfferRowDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.OfferRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowDisplay.
 * @interface xyz.swapee.wc.back.IOfferRowDisplayFields
 */
xyz.swapee.wc.back.IOfferRowDisplayFields = class { }
/**
 * The collapsar with the exchange.
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ExchangeCollapsar = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The container for the exchange.
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferExchange = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.BestOfferPlaque = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ProgressVertLine = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinImWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CancelLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealBuA = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferCrypto = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferAmount = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Logo = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Eta = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDeal = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecHandle = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecPopup = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingIco = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedIco = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinIm = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IOfferRowDisplay} */
xyz.swapee.wc.back.RecordIOfferRowDisplay

/** @typedef {xyz.swapee.wc.back.IOfferRowDisplay} xyz.swapee.wc.back.BoundIOfferRowDisplay */

/** @typedef {xyz.swapee.wc.back.OfferRowDisplay} xyz.swapee.wc.back.BoundOfferRowDisplay */

/**
 * Contains getters to cast the _IOfferRowDisplay_ interface.
 * @interface xyz.swapee.wc.back.IOfferRowDisplayCaster
 */
xyz.swapee.wc.back.IOfferRowDisplayCaster = class { }
/**
 * Cast the _IOfferRowDisplay_ instance into the _BoundIOfferRowDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferRowDisplay}
 */
xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/**
 * Access the _OfferRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferRowDisplay}
 */
xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.superOfferRowDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.OfferRowMemory, land?: null) => void} xyz.swapee.wc.back.IOfferRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IOfferRowDisplay.__paint<!xyz.swapee.wc.back.IOfferRowDisplay>} xyz.swapee.wc.back.IOfferRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OfferRowMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IOfferRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IOfferRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/41-OfferRowClasses.xml}  636dd29c56ccc1aee1b43bb34f644c6d */
/**
 * The classes of the _IOfferRowDisplay_.
 * @record xyz.swapee.wc.OfferRowClasses
 */
xyz.swapee.wc.OfferRowClasses = class { }
/**
 * The best offer.
 */
xyz.swapee.wc.OfferRowClasses.prototype.BestOffer = /** @type {string|undefined} */ (void 0)
/**
 * The recommended offer.
 */
xyz.swapee.wc.OfferRowClasses.prototype.Recommended = /** @type {string|undefined} */ (void 0)
/**
 * The vertical line for progress.
 */
xyz.swapee.wc.OfferRowClasses.prototype.ProgressVertLine = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.OfferRowClasses.prototype.props = /** @type {xyz.swapee.wc.OfferRowClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml}  61014d20fdf80ac0d90742221e207e0c */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>} xyz.swapee.wc.IOfferRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferRowController)} xyz.swapee.wc.AbstractOfferRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowController} xyz.swapee.wc.OfferRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowController` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowController
 */
xyz.swapee.wc.AbstractOfferRowController = class extends /** @type {xyz.swapee.wc.AbstractOfferRowController.constructor&xyz.swapee.wc.OfferRowController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowController.prototype.constructor = xyz.swapee.wc.AbstractOfferRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowController.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowController.Initialese[]) => xyz.swapee.wc.IOfferRowController} xyz.swapee.wc.OfferRowControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.OfferRowMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOfferRowController.Inputs>)} xyz.swapee.wc.IOfferRowController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IOfferRowController
 */
xyz.swapee.wc.IOfferRowController = class extends /** @type {xyz.swapee.wc.IOfferRowController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferRowController.resetPort} */
xyz.swapee.wc.IOfferRowController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowController&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowController.Initialese>)} xyz.swapee.wc.OfferRowController.constructor */
/**
 * A concrete class of _IOfferRowController_ instances.
 * @constructor xyz.swapee.wc.OfferRowController
 * @implements {xyz.swapee.wc.IOfferRowController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowController.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowController = class extends /** @type {xyz.swapee.wc.OfferRowController.constructor&xyz.swapee.wc.IOfferRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.OfferRowController.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowController.
 * @interface xyz.swapee.wc.IOfferRowControllerFields
 */
xyz.swapee.wc.IOfferRowControllerFields = class { }
/**
 * The inputs to the _IOfferRow_'s controller.
 */
xyz.swapee.wc.IOfferRowControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferRowController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IOfferRowControllerFields.prototype.props = /** @type {xyz.swapee.wc.IOfferRowController} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowController} */
xyz.swapee.wc.RecordIOfferRowController

/** @typedef {xyz.swapee.wc.IOfferRowController} xyz.swapee.wc.BoundIOfferRowController */

/** @typedef {xyz.swapee.wc.OfferRowController} xyz.swapee.wc.BoundOfferRowController */

/** @typedef {xyz.swapee.wc.IOfferRowPort.Inputs} xyz.swapee.wc.IOfferRowController.Inputs The inputs to the _IOfferRow_'s controller. */

/** @typedef {xyz.swapee.wc.IOfferRowPort.WeakInputs} xyz.swapee.wc.IOfferRowController.WeakInputs The inputs to the _IOfferRow_'s controller. */

/**
 * Contains getters to cast the _IOfferRowController_ interface.
 * @interface xyz.swapee.wc.IOfferRowControllerCaster
 */
xyz.swapee.wc.IOfferRowControllerCaster = class { }
/**
 * Cast the _IOfferRowController_ instance into the _BoundIOfferRowController_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowController}
 */
xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowController
/**
 * Cast the _IOfferRowController_ instance into the _BoundIOfferRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowProcessor}
 */
xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowProcessor
/**
 * Access the _OfferRowController_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowController}
 */
xyz.swapee.wc.IOfferRowControllerCaster.prototype.superOfferRowController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferRowController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferRowController.__resetPort<!xyz.swapee.wc.IOfferRowController>} xyz.swapee.wc.IOfferRowController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOfferRowController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IOfferRowController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml}  36205a9c97a719eb4a9ff88cb0187295 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IOfferRowController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferRowController)} xyz.swapee.wc.front.AbstractOfferRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferRowController} xyz.swapee.wc.front.OfferRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferRowController` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferRowController
 */
xyz.swapee.wc.front.AbstractOfferRowController = class extends /** @type {xyz.swapee.wc.front.AbstractOfferRowController.constructor&xyz.swapee.wc.front.OfferRowController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferRowController.prototype.constructor = xyz.swapee.wc.front.AbstractOfferRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferRowController.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferRowController.Initialese[]) => xyz.swapee.wc.front.IOfferRowController} xyz.swapee.wc.front.OfferRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferRowControllerCaster&xyz.swapee.wc.front.IOfferRowControllerAT)} xyz.swapee.wc.front.IOfferRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferRowControllerAT} xyz.swapee.wc.front.IOfferRowControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IOfferRowController
 */
xyz.swapee.wc.front.IOfferRowController = class extends /** @type {xyz.swapee.wc.front.IOfferRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOfferRowControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowController.Initialese>)} xyz.swapee.wc.front.OfferRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferRowController} xyz.swapee.wc.front.IOfferRowController.typeof */
/**
 * A concrete class of _IOfferRowController_ instances.
 * @constructor xyz.swapee.wc.front.OfferRowController
 * @implements {xyz.swapee.wc.front.IOfferRowController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowController.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferRowController = class extends /** @type {xyz.swapee.wc.front.OfferRowController.constructor&xyz.swapee.wc.front.IOfferRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.OfferRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferRowController} */
xyz.swapee.wc.front.RecordIOfferRowController

/** @typedef {xyz.swapee.wc.front.IOfferRowController} xyz.swapee.wc.front.BoundIOfferRowController */

/** @typedef {xyz.swapee.wc.front.OfferRowController} xyz.swapee.wc.front.BoundOfferRowController */

/**
 * Contains getters to cast the _IOfferRowController_ interface.
 * @interface xyz.swapee.wc.front.IOfferRowControllerCaster
 */
xyz.swapee.wc.front.IOfferRowControllerCaster = class { }
/**
 * Cast the _IOfferRowController_ instance into the _BoundIOfferRowController_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferRowController}
 */
xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.asIOfferRowController
/**
 * Access the _OfferRowController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferRowController}
 */
xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.superOfferRowController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml}  619c14cb00310a057dd6fab19f2d9717 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>&xyz.swapee.wc.IOfferRowController.Initialese} xyz.swapee.wc.back.IOfferRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OfferRowController)} xyz.swapee.wc.back.AbstractOfferRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferRowController} xyz.swapee.wc.back.OfferRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferRowController` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferRowController
 */
xyz.swapee.wc.back.AbstractOfferRowController = class extends /** @type {xyz.swapee.wc.back.AbstractOfferRowController.constructor&xyz.swapee.wc.back.OfferRowController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferRowController.prototype.constructor = xyz.swapee.wc.back.AbstractOfferRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferRowController.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferRowController.Initialese[]) => xyz.swapee.wc.back.IOfferRowController} xyz.swapee.wc.back.OfferRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferRowControllerCaster&xyz.swapee.wc.IOfferRowController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IOfferRowController.Inputs>)} xyz.swapee.wc.back.IOfferRowController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IOfferRowController
 */
xyz.swapee.wc.back.IOfferRowController = class extends /** @type {xyz.swapee.wc.back.IOfferRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferRowController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowController.Initialese>)} xyz.swapee.wc.back.OfferRowController.constructor */
/**
 * A concrete class of _IOfferRowController_ instances.
 * @constructor xyz.swapee.wc.back.OfferRowController
 * @implements {xyz.swapee.wc.back.IOfferRowController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowController.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferRowController = class extends /** @type {xyz.swapee.wc.back.OfferRowController.constructor&xyz.swapee.wc.back.IOfferRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.OfferRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferRowController} */
xyz.swapee.wc.back.RecordIOfferRowController

/** @typedef {xyz.swapee.wc.back.IOfferRowController} xyz.swapee.wc.back.BoundIOfferRowController */

/** @typedef {xyz.swapee.wc.back.OfferRowController} xyz.swapee.wc.back.BoundOfferRowController */

/**
 * Contains getters to cast the _IOfferRowController_ interface.
 * @interface xyz.swapee.wc.back.IOfferRowControllerCaster
 */
xyz.swapee.wc.back.IOfferRowControllerCaster = class { }
/**
 * Cast the _IOfferRowController_ instance into the _BoundIOfferRowController_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferRowController}
 */
xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.asIOfferRowController
/**
 * Access the _OfferRowController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferRowController}
 */
xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.superOfferRowController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml}  a7288818b9a2364b615313e4a21f6ce6 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOfferRowController.Initialese} xyz.swapee.wc.back.IOfferRowControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferRowControllerAR)} xyz.swapee.wc.back.AbstractOfferRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferRowControllerAR} xyz.swapee.wc.back.OfferRowControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferRowControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferRowControllerAR
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractOfferRowControllerAR.constructor&xyz.swapee.wc.back.OfferRowControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferRowControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractOfferRowControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferRowControllerAR.Initialese[]) => xyz.swapee.wc.back.IOfferRowControllerAR} xyz.swapee.wc.back.OfferRowControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferRowControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOfferRowController)} xyz.swapee.wc.back.IOfferRowControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOfferRowControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IOfferRowControllerAR
 */
xyz.swapee.wc.back.IOfferRowControllerAR = class extends /** @type {xyz.swapee.wc.back.IOfferRowControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOfferRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferRowControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese>)} xyz.swapee.wc.back.OfferRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowControllerAR} xyz.swapee.wc.back.IOfferRowControllerAR.typeof */
/**
 * A concrete class of _IOfferRowControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.OfferRowControllerAR
 * @implements {xyz.swapee.wc.back.IOfferRowControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IOfferRowControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferRowControllerAR = class extends /** @type {xyz.swapee.wc.back.OfferRowControllerAR.constructor&xyz.swapee.wc.back.IOfferRowControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferRowControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.OfferRowControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferRowControllerAR} */
xyz.swapee.wc.back.RecordIOfferRowControllerAR

/** @typedef {xyz.swapee.wc.back.IOfferRowControllerAR} xyz.swapee.wc.back.BoundIOfferRowControllerAR */

/** @typedef {xyz.swapee.wc.back.OfferRowControllerAR} xyz.swapee.wc.back.BoundOfferRowControllerAR */

/**
 * Contains getters to cast the _IOfferRowControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IOfferRowControllerARCaster
 */
xyz.swapee.wc.back.IOfferRowControllerARCaster = class { }
/**
 * Cast the _IOfferRowControllerAR_ instance into the _BoundIOfferRowControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferRowControllerAR}
 */
xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.asIOfferRowControllerAR
/**
 * Access the _OfferRowControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferRowControllerAR}
 */
xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.superOfferRowControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml}  d7a2c2c00dec88f399fa3f96b2627353 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IOfferRowControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferRowControllerAT)} xyz.swapee.wc.front.AbstractOfferRowControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferRowControllerAT} xyz.swapee.wc.front.OfferRowControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferRowControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferRowControllerAT
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractOfferRowControllerAT.constructor&xyz.swapee.wc.front.OfferRowControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferRowControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractOfferRowControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferRowControllerAT.Initialese[]) => xyz.swapee.wc.front.IOfferRowControllerAT} xyz.swapee.wc.front.OfferRowControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferRowControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IOfferRowControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferRowControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IOfferRowControllerAT
 */
xyz.swapee.wc.front.IOfferRowControllerAT = class extends /** @type {xyz.swapee.wc.front.IOfferRowControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferRowControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese>)} xyz.swapee.wc.front.OfferRowControllerAT.constructor */
/**
 * A concrete class of _IOfferRowControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.OfferRowControllerAT
 * @implements {xyz.swapee.wc.front.IOfferRowControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferRowControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferRowControllerAT = class extends /** @type {xyz.swapee.wc.front.OfferRowControllerAT.constructor&xyz.swapee.wc.front.IOfferRowControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferRowControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.OfferRowControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferRowControllerAT} */
xyz.swapee.wc.front.RecordIOfferRowControllerAT

/** @typedef {xyz.swapee.wc.front.IOfferRowControllerAT} xyz.swapee.wc.front.BoundIOfferRowControllerAT */

/** @typedef {xyz.swapee.wc.front.OfferRowControllerAT} xyz.swapee.wc.front.BoundOfferRowControllerAT */

/**
 * Contains getters to cast the _IOfferRowControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IOfferRowControllerATCaster
 */
xyz.swapee.wc.front.IOfferRowControllerATCaster = class { }
/**
 * Cast the _IOfferRowControllerAT_ instance into the _BoundIOfferRowControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferRowControllerAT}
 */
xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.asIOfferRowControllerAT
/**
 * Access the _OfferRowControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferRowControllerAT}
 */
xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.superOfferRowControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml}  72526e49bbcb557a3cd2db158ea115a0 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null>&xyz.swapee.wc.IOfferRowDisplay.Initialese} xyz.swapee.wc.IOfferRowScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferRowScreen)} xyz.swapee.wc.AbstractOfferRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowScreen} xyz.swapee.wc.OfferRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowScreen` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowScreen
 */
xyz.swapee.wc.AbstractOfferRowScreen = class extends /** @type {xyz.swapee.wc.AbstractOfferRowScreen.constructor&xyz.swapee.wc.OfferRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowScreen.prototype.constructor = xyz.swapee.wc.AbstractOfferRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowScreen.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowScreen.Initialese[]) => xyz.swapee.wc.IOfferRowScreen} xyz.swapee.wc.OfferRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferRowScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null, null>&xyz.swapee.wc.front.IOfferRowController&xyz.swapee.wc.IOfferRowDisplay)} xyz.swapee.wc.IOfferRowScreen.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.front.IOfferRowController} xyz.swapee.wc.front.IOfferRowController.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferRowDisplay} xyz.swapee.wc.IOfferRowDisplay.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IOfferRowScreen
 */
xyz.swapee.wc.IOfferRowScreen = class extends /** @type {xyz.swapee.wc.IOfferRowScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IOfferRowController.typeof&xyz.swapee.wc.IOfferRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowScreen.Initialese>)} xyz.swapee.wc.OfferRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferRowScreen} xyz.swapee.wc.IOfferRowScreen.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferRowScreen_ instances.
 * @constructor xyz.swapee.wc.OfferRowScreen
 * @implements {xyz.swapee.wc.IOfferRowScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowScreen = class extends /** @type {xyz.swapee.wc.OfferRowScreen.constructor&xyz.swapee.wc.IOfferRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.OfferRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferRowScreen} */
xyz.swapee.wc.RecordIOfferRowScreen

/** @typedef {xyz.swapee.wc.IOfferRowScreen} xyz.swapee.wc.BoundIOfferRowScreen */

/** @typedef {xyz.swapee.wc.OfferRowScreen} xyz.swapee.wc.BoundOfferRowScreen */

/**
 * Contains getters to cast the _IOfferRowScreen_ interface.
 * @interface xyz.swapee.wc.IOfferRowScreenCaster
 */
xyz.swapee.wc.IOfferRowScreenCaster = class { }
/**
 * Cast the _IOfferRowScreen_ instance into the _BoundIOfferRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowScreen}
 */
xyz.swapee.wc.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/**
 * Access the _OfferRowScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowScreen}
 */
xyz.swapee.wc.IOfferRowScreenCaster.prototype.superOfferRowScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml}  32fabe642a3896e68ab527694ef37182 */
/** @typedef {xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} xyz.swapee.wc.back.IOfferRowScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferRowScreen)} xyz.swapee.wc.back.AbstractOfferRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferRowScreen} xyz.swapee.wc.back.OfferRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferRowScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferRowScreen
 */
xyz.swapee.wc.back.AbstractOfferRowScreen = class extends /** @type {xyz.swapee.wc.back.AbstractOfferRowScreen.constructor&xyz.swapee.wc.back.OfferRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferRowScreen.prototype.constructor = xyz.swapee.wc.back.AbstractOfferRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferRowScreen.Initialese[]) => xyz.swapee.wc.back.IOfferRowScreen} xyz.swapee.wc.back.OfferRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferRowScreenCaster&xyz.swapee.wc.back.IOfferRowScreenAT)} xyz.swapee.wc.back.IOfferRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferRowScreenAT} xyz.swapee.wc.back.IOfferRowScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IOfferRowScreen
 */
xyz.swapee.wc.back.IOfferRowScreen = class extends /** @type {xyz.swapee.wc.back.IOfferRowScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOfferRowScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreen.Initialese>)} xyz.swapee.wc.back.OfferRowScreen.constructor */
/**
 * A concrete class of _IOfferRowScreen_ instances.
 * @constructor xyz.swapee.wc.back.OfferRowScreen
 * @implements {xyz.swapee.wc.back.IOfferRowScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferRowScreen = class extends /** @type {xyz.swapee.wc.back.OfferRowScreen.constructor&xyz.swapee.wc.back.IOfferRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.OfferRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferRowScreen} */
xyz.swapee.wc.back.RecordIOfferRowScreen

/** @typedef {xyz.swapee.wc.back.IOfferRowScreen} xyz.swapee.wc.back.BoundIOfferRowScreen */

/** @typedef {xyz.swapee.wc.back.OfferRowScreen} xyz.swapee.wc.back.BoundOfferRowScreen */

/**
 * Contains getters to cast the _IOfferRowScreen_ interface.
 * @interface xyz.swapee.wc.back.IOfferRowScreenCaster
 */
xyz.swapee.wc.back.IOfferRowScreenCaster = class { }
/**
 * Cast the _IOfferRowScreen_ instance into the _BoundIOfferRowScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferRowScreen}
 */
xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/**
 * Access the _OfferRowScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferRowScreen}
 */
xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.superOfferRowScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml}  0d065f35bf84b82967777859642362bf */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOfferRowScreen.Initialese} xyz.swapee.wc.front.IOfferRowScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferRowScreenAR)} xyz.swapee.wc.front.AbstractOfferRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferRowScreenAR} xyz.swapee.wc.front.OfferRowScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferRowScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferRowScreenAR
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractOfferRowScreenAR.constructor&xyz.swapee.wc.front.OfferRowScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferRowScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractOfferRowScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferRowScreenAR.Initialese[]) => xyz.swapee.wc.front.IOfferRowScreenAR} xyz.swapee.wc.front.OfferRowScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferRowScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOfferRowScreen)} xyz.swapee.wc.front.IOfferRowScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOfferRowScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IOfferRowScreenAR
 */
xyz.swapee.wc.front.IOfferRowScreenAR = class extends /** @type {xyz.swapee.wc.front.IOfferRowScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOfferRowScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferRowScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese>)} xyz.swapee.wc.front.OfferRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferRowScreenAR} xyz.swapee.wc.front.IOfferRowScreenAR.typeof */
/**
 * A concrete class of _IOfferRowScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.OfferRowScreenAR
 * @implements {xyz.swapee.wc.front.IOfferRowScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IOfferRowScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferRowScreenAR = class extends /** @type {xyz.swapee.wc.front.OfferRowScreenAR.constructor&xyz.swapee.wc.front.IOfferRowScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferRowScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.OfferRowScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferRowScreenAR} */
xyz.swapee.wc.front.RecordIOfferRowScreenAR

/** @typedef {xyz.swapee.wc.front.IOfferRowScreenAR} xyz.swapee.wc.front.BoundIOfferRowScreenAR */

/** @typedef {xyz.swapee.wc.front.OfferRowScreenAR} xyz.swapee.wc.front.BoundOfferRowScreenAR */

/**
 * Contains getters to cast the _IOfferRowScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IOfferRowScreenARCaster
 */
xyz.swapee.wc.front.IOfferRowScreenARCaster = class { }
/**
 * Cast the _IOfferRowScreenAR_ instance into the _BoundIOfferRowScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferRowScreenAR}
 */
xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.asIOfferRowScreenAR
/**
 * Access the _OfferRowScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferRowScreenAR}
 */
xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.superOfferRowScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml}  bb05620a959396e5142b606c6957acf6 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IOfferRowScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferRowScreenAT)} xyz.swapee.wc.back.AbstractOfferRowScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferRowScreenAT} xyz.swapee.wc.back.OfferRowScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferRowScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferRowScreenAT
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractOfferRowScreenAT.constructor&xyz.swapee.wc.back.OfferRowScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferRowScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractOfferRowScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferRowScreenAT.Initialese[]) => xyz.swapee.wc.back.IOfferRowScreenAT} xyz.swapee.wc.back.OfferRowScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferRowScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IOfferRowScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferRowScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IOfferRowScreenAT
 */
xyz.swapee.wc.back.IOfferRowScreenAT = class extends /** @type {xyz.swapee.wc.back.IOfferRowScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferRowScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese>)} xyz.swapee.wc.back.OfferRowScreenAT.constructor */
/**
 * A concrete class of _IOfferRowScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.OfferRowScreenAT
 * @implements {xyz.swapee.wc.back.IOfferRowScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferRowScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferRowScreenAT = class extends /** @type {xyz.swapee.wc.back.OfferRowScreenAT.constructor&xyz.swapee.wc.back.IOfferRowScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferRowScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.OfferRowScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferRowScreenAT} */
xyz.swapee.wc.back.RecordIOfferRowScreenAT

/** @typedef {xyz.swapee.wc.back.IOfferRowScreenAT} xyz.swapee.wc.back.BoundIOfferRowScreenAT */

/** @typedef {xyz.swapee.wc.back.OfferRowScreenAT} xyz.swapee.wc.back.BoundOfferRowScreenAT */

/**
 * Contains getters to cast the _IOfferRowScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IOfferRowScreenATCaster
 */
xyz.swapee.wc.back.IOfferRowScreenATCaster = class { }
/**
 * Cast the _IOfferRowScreenAT_ instance into the _BoundIOfferRowScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferRowScreenAT}
 */
xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.asIOfferRowScreenAT
/**
 * Access the _OfferRowScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferRowScreenAT}
 */
xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.superOfferRowScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IOfferRowDisplay.Initialese} xyz.swapee.wc.IOfferRowGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferRowGPU)} xyz.swapee.wc.AbstractOfferRowGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferRowGPU} xyz.swapee.wc.OfferRowGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferRowGPU` interface.
 * @constructor xyz.swapee.wc.AbstractOfferRowGPU
 */
xyz.swapee.wc.AbstractOfferRowGPU = class extends /** @type {xyz.swapee.wc.AbstractOfferRowGPU.constructor&xyz.swapee.wc.OfferRowGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferRowGPU.prototype.constructor = xyz.swapee.wc.AbstractOfferRowGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferRowGPU.class = /** @type {typeof xyz.swapee.wc.AbstractOfferRowGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferRowGPU.Initialese[]) => xyz.swapee.wc.IOfferRowGPU} xyz.swapee.wc.OfferRowGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferRowGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferRowGPUCaster&com.webcircuits.IBrowserView<.!OfferRowMemory,>&xyz.swapee.wc.back.IOfferRowDisplay)} xyz.swapee.wc.IOfferRowGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!OfferRowMemory,>} com.webcircuits.IBrowserView<.!OfferRowMemory,>.typeof */
/**
 * Handles the periphery of the _IOfferRowDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IOfferRowGPU
 */
xyz.swapee.wc.IOfferRowGPU = class extends /** @type {xyz.swapee.wc.IOfferRowGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!OfferRowMemory,>.typeof&xyz.swapee.wc.back.IOfferRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferRowGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferRowGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowGPU.Initialese>)} xyz.swapee.wc.OfferRowGPU.constructor */
/**
 * A concrete class of _IOfferRowGPU_ instances.
 * @constructor xyz.swapee.wc.OfferRowGPU
 * @implements {xyz.swapee.wc.IOfferRowGPU} Handles the periphery of the _IOfferRowDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowGPU.Initialese>} ‎
 */
xyz.swapee.wc.OfferRowGPU = class extends /** @type {xyz.swapee.wc.OfferRowGPU.constructor&xyz.swapee.wc.IOfferRowGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferRowGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferRowGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.OfferRowGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferRowGPU.
 * @interface xyz.swapee.wc.IOfferRowGPUFields
 */
xyz.swapee.wc.IOfferRowGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IOfferRowGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferRowGPU} */
xyz.swapee.wc.RecordIOfferRowGPU

/** @typedef {xyz.swapee.wc.IOfferRowGPU} xyz.swapee.wc.BoundIOfferRowGPU */

/** @typedef {xyz.swapee.wc.OfferRowGPU} xyz.swapee.wc.BoundOfferRowGPU */

/**
 * Contains getters to cast the _IOfferRowGPU_ interface.
 * @interface xyz.swapee.wc.IOfferRowGPUCaster
 */
xyz.swapee.wc.IOfferRowGPUCaster = class { }
/**
 * Cast the _IOfferRowGPU_ instance into the _BoundIOfferRowGPU_ type.
 * @type {!xyz.swapee.wc.BoundIOfferRowGPU}
 */
xyz.swapee.wc.IOfferRowGPUCaster.prototype.asIOfferRowGPU
/**
 * Access the _OfferRowGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferRowGPU}
 */
xyz.swapee.wc.IOfferRowGPUCaster.prototype.superOfferRowGPU

// nss:xyz.swapee.wc
/* @typal-end */