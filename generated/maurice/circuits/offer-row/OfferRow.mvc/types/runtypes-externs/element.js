/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.Initialese  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IOfferRowElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowElement.Initialese} */
xyz.swapee.wc.IOfferRowElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElementFields  aedfbfea40cb5c42cffbec20c43ba60c */
/** @interface */
$xyz.swapee.wc.IOfferRowElementFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowElement.Inputs} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.inputs
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.X_EXCHANGE_COLLAPSAR_HANDLE
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.xExchangeCollapsarHandle
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.exchangeCollapsarHandleSel
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.X_RECOMMENDATION_INFO_HANDLE
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.xRecommendationInfoHandle
/** @type {string} */
$xyz.swapee.wc.IOfferRowElementFields.prototype.recommendationInfoHandleSel
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElementFields}
 */
xyz.swapee.wc.IOfferRowElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElementCaster  aedfbfea40cb5c42cffbec20c43ba60c */
/** @interface */
$xyz.swapee.wc.IOfferRowElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowElement} */
$xyz.swapee.wc.IOfferRowElementCaster.prototype.asIOfferRowElement
/** @type {!xyz.swapee.wc.BoundOfferRowElement} */
$xyz.swapee.wc.IOfferRowElementCaster.prototype.superOfferRowElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElementCaster}
 */
xyz.swapee.wc.IOfferRowElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs, null>}
 */
$xyz.swapee.wc.IOfferRowElement = function() {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} model
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOfferRowElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IOfferRowElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowElement}
 */
xyz.swapee.wc.IOfferRowElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.OfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowElement.Initialese>}
 */
$xyz.swapee.wc.OfferRowElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.OfferRowElement
/** @type {function(new: xyz.swapee.wc.IOfferRowElement, ...!xyz.swapee.wc.IOfferRowElement.Initialese)} */
xyz.swapee.wc.OfferRowElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.OfferRowElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.AbstractOfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowElement}
 */
$xyz.swapee.wc.AbstractOfferRowElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowElement)} */
xyz.swapee.wc.AbstractOfferRowElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowElement|typeof xyz.swapee.wc.OfferRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowElement}
 */
xyz.swapee.wc.AbstractOfferRowElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.OfferRowElementConstructor  aedfbfea40cb5c42cffbec20c43ba60c */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowElement, ...!xyz.swapee.wc.IOfferRowElement.Initialese)} */
xyz.swapee.wc.OfferRowElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.RecordIOfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/** @typedef {{ solder: xyz.swapee.wc.IOfferRowElement.solder, render: xyz.swapee.wc.IOfferRowElement.render, server: xyz.swapee.wc.IOfferRowElement.server, inducer: xyz.swapee.wc.IOfferRowElement.inducer }} */
xyz.swapee.wc.RecordIOfferRowElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.BoundIOfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowElementFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowElement.Inputs, null>}
 */
$xyz.swapee.wc.BoundIOfferRowElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowElement} */
xyz.swapee.wc.BoundIOfferRowElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.BoundOfferRowElement  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowElement} */
xyz.swapee.wc.BoundOfferRowElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.solder  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} model
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IOfferRowElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} model
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IOfferRowElement}
 */
$xyz.swapee.wc.IOfferRowElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} model
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IOfferRowElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.solder} */
xyz.swapee.wc.IOfferRowElement.solder
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement._solder} */
xyz.swapee.wc.IOfferRowElement._solder
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.__solder} */
xyz.swapee.wc.IOfferRowElement.__solder

// nss:xyz.swapee.wc.IOfferRowElement,$xyz.swapee.wc.IOfferRowElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.render  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.OfferRowMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOfferRowElement.render
/** @typedef {function(this: xyz.swapee.wc.IOfferRowElement, !xyz.swapee.wc.OfferRowMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IOfferRowElement._render
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.__render} */
xyz.swapee.wc.IOfferRowElement.__render

// nss:xyz.swapee.wc.IOfferRowElement,$xyz.swapee.wc.IOfferRowElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.server  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IOfferRowElement}
 */
$xyz.swapee.wc.IOfferRowElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IOfferRowElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.server} */
xyz.swapee.wc.IOfferRowElement.server
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement._server} */
xyz.swapee.wc.IOfferRowElement._server
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.__server} */
xyz.swapee.wc.IOfferRowElement.__server

// nss:xyz.swapee.wc.IOfferRowElement,$xyz.swapee.wc.IOfferRowElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.inducer  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} [port]
 */
$xyz.swapee.wc.IOfferRowElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} [port]
 * @this {xyz.swapee.wc.IOfferRowElement}
 */
$xyz.swapee.wc.IOfferRowElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} [model]
 * @param {!xyz.swapee.wc.IOfferRowElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IOfferRowElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.inducer} */
xyz.swapee.wc.IOfferRowElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement._inducer} */
xyz.swapee.wc.IOfferRowElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IOfferRowElement.__inducer} */
xyz.swapee.wc.IOfferRowElement.__inducer

// nss:xyz.swapee.wc.IOfferRowElement,$xyz.swapee.wc.IOfferRowElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/130-IOfferRowElement.xml} xyz.swapee.wc.IOfferRowElement.Inputs  aedfbfea40cb5c42cffbec20c43ba60c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPort.Inputs}
 * @extends {xyz.swapee.wc.IOfferRowDisplay.Queries}
 * @extends {xyz.swapee.wc.IOfferRowController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IOfferRowElementPort.Inputs}
 */
$xyz.swapee.wc.IOfferRowElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowElement.Inputs} */
xyz.swapee.wc.IOfferRowElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowElement
/* @typal-end */