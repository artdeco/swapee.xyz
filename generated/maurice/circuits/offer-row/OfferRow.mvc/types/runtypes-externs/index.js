/** @const {?} */ $xyz.swapee.wc.IOfferRowComputer
/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore
/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IOfferRowPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IOfferRowPortInterface
/** @const {?} */ $xyz.swapee.wc.IOfferRowCore
/** @const {?} */ $xyz.swapee.wc.IOfferRowCore.Model
/** @const {?} */ $xyz.swapee.wc.IOfferRowController
/** @const {?} */ $xyz.swapee.wc.IOfferRowProcessor
/** @const {?} */ $xyz.swapee.wc.IOfferRow
/** @const {?} */ $xyz.swapee.wc.IOfferRowGPU
/** @const {?} */ $xyz.swapee.wc.IOfferRowHtmlComponent
/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner
/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IOfferRowDesigner.relay
/** @const {?} */ $xyz.swapee.wc.front.IOfferRowController
/** @const {?} */ $xyz.swapee.wc.front.IOfferRowControllerAT
/** @const {?} */ $xyz.swapee.wc.front.IOfferRowScreenAR
/** @const {?} */ $xyz.swapee.wc.back.IOfferRowController
/** @const {?} */ $xyz.swapee.wc.back.IOfferRowControllerAR
/** @const {?} */ $xyz.swapee.wc.back.IOfferRowScreenAT
/** @const {?} */ $xyz.swapee.wc.back.IOfferRowScreen
/** @const {?} */ $xyz.swapee.wc.IOfferRowScreen
/** @const {?} */ xyz.swapee.wc.IOfferRowComputer
/** @const {?} */ xyz.swapee.wc.IOfferRowPort
/** @const {?} */ xyz.swapee.wc.IOfferRowCore
/** @const {?} */ xyz.swapee.wc.IOfferRowController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IOfferRowComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowComputer.Initialese} */
xyz.swapee.wc.IOfferRowComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @interface */
$xyz.swapee.wc.IOfferRowComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowComputer} */
$xyz.swapee.wc.IOfferRowComputerCaster.prototype.asIOfferRowComputer
/** @type {!xyz.swapee.wc.BoundOfferRowComputer} */
$xyz.swapee.wc.IOfferRowComputerCaster.prototype.superOfferRowComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowComputerCaster}
 */
xyz.swapee.wc.IOfferRowComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.OfferRowMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.IOfferRowComputer = function() {}
/**
 * @param {xyz.swapee.wc.OfferRowMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowComputer}
 */
xyz.swapee.wc.IOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.OfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowComputer.Initialese>}
 */
$xyz.swapee.wc.OfferRowComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.OfferRowComputer
/** @type {function(new: xyz.swapee.wc.IOfferRowComputer, ...!xyz.swapee.wc.IOfferRowComputer.Initialese)} */
xyz.swapee.wc.OfferRowComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.OfferRowComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.AbstractOfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowComputer}
 */
$xyz.swapee.wc.AbstractOfferRowComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowComputer)} */
xyz.swapee.wc.AbstractOfferRowComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowComputer}
 */
xyz.swapee.wc.AbstractOfferRowComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.OfferRowComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowComputer, ...!xyz.swapee.wc.IOfferRowComputer.Initialese)} */
xyz.swapee.wc.OfferRowComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.RecordIOfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/** @typedef {{ compute: xyz.swapee.wc.IOfferRowComputer.compute }} */
xyz.swapee.wc.RecordIOfferRowComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.BoundIOfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.OfferRowMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundIOfferRowComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowComputer} */
xyz.swapee.wc.BoundIOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.BoundOfferRowComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowComputer} */
xyz.swapee.wc.BoundOfferRowComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/02-IOfferRowComputer.xml} xyz.swapee.wc.IOfferRowComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9e8780d3351ed26bf834d03a46c250ce */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.OfferRowMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.OfferRowMemory): void} */
xyz.swapee.wc.IOfferRowComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IOfferRowComputer, xyz.swapee.wc.OfferRowMemory): void} */
xyz.swapee.wc.IOfferRowComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IOfferRowComputer.__compute} */
xyz.swapee.wc.IOfferRowComputer.__compute

// nss:xyz.swapee.wc.IOfferRowComputer,$xyz.swapee.wc.IOfferRowComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Initialese} */
xyz.swapee.wc.IOfferRowOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @interface */
$xyz.swapee.wc.IOfferRowOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowOuterCore.Model} */
$xyz.swapee.wc.IOfferRowOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCoreFields}
 */
xyz.swapee.wc.IOfferRowOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @interface */
$xyz.swapee.wc.IOfferRowOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowOuterCore} */
$xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.asIOfferRowOuterCore
/** @type {!xyz.swapee.wc.BoundOfferRowOuterCore} */
$xyz.swapee.wc.IOfferRowOuterCoreCaster.prototype.superOfferRowOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
xyz.swapee.wc.IOfferRowOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
$xyz.swapee.wc.IOfferRowOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowOuterCore}
 */
xyz.swapee.wc.IOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.OfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowOuterCore.Initialese>}
 */
$xyz.swapee.wc.OfferRowOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.OfferRowOuterCore
/** @type {function(new: xyz.swapee.wc.IOfferRowOuterCore)} */
xyz.swapee.wc.OfferRowOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.OfferRowOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.AbstractOfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowOuterCore}
 */
$xyz.swapee.wc.AbstractOfferRowOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowOuterCore)} */
xyz.swapee.wc.AbstractOfferRowOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferRowOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowMemoryPQs.prototype.core
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowMemoryPQs}
 */
xyz.swapee.wc.OfferRowMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowMemoryQPs.prototype.a74ad
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowMemoryQPs}
 */
xyz.swapee.wc.OfferRowMemoryQPs
/** @type {function(new: xyz.swapee.wc.OfferRowMemoryQPs)} */
xyz.swapee.wc.OfferRowMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.RecordIOfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.BoundIOfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIOfferRowOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowOuterCore} */
xyz.swapee.wc.BoundIOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.BoundOfferRowOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowOuterCore} */
xyz.swapee.wc.BoundOfferRowOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core.core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @typedef {string} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core.core

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model.Core} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core}
 */
$xyz.swapee.wc.IOfferRowOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model} */
xyz.swapee.wc.IOfferRowOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IOfferRowPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Initialese} */
xyz.swapee.wc.IOfferRowPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */
$xyz.swapee.wc.IOfferRowPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IOfferRowPort.Inputs} */
$xyz.swapee.wc.IOfferRowPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortFields}
 */
xyz.swapee.wc.IOfferRowPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowPort} */
$xyz.swapee.wc.IOfferRowPortCaster.prototype.asIOfferRowPort
/** @type {!xyz.swapee.wc.BoundOfferRowPort} */
$xyz.swapee.wc.IOfferRowPortCaster.prototype.superOfferRowPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortCaster}
 */
xyz.swapee.wc.IOfferRowPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferRowPort.Inputs>}
 */
$xyz.swapee.wc.IOfferRowPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowPort.prototype.resetOfferRowPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPort}
 */
xyz.swapee.wc.IOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowPort.Initialese>}
 */
$xyz.swapee.wc.OfferRowPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.OfferRowPort
/** @type {function(new: xyz.swapee.wc.IOfferRowPort, ...!xyz.swapee.wc.IOfferRowPort.Initialese)} */
xyz.swapee.wc.OfferRowPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.OfferRowPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.AbstractOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowPort}
 */
$xyz.swapee.wc.AbstractOfferRowPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowPort)} */
xyz.swapee.wc.AbstractOfferRowPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowPort|typeof xyz.swapee.wc.OfferRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowPort}
 */
xyz.swapee.wc.AbstractOfferRowPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowPort, ...!xyz.swapee.wc.IOfferRowPort.Initialese)} */
xyz.swapee.wc.OfferRowPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.OfferRowMemoryPQs}
 */
$xyz.swapee.wc.OfferRowInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowInputsPQs}
 */
xyz.swapee.wc.OfferRowInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.OfferRowInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowInputsQPs}
 */
xyz.swapee.wc.OfferRowInputsQPs
/** @type {function(new: xyz.swapee.wc.OfferRowInputsQPs)} */
xyz.swapee.wc.OfferRowInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.RecordIOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @typedef {{ resetPort: xyz.swapee.wc.IOfferRowPort.resetPort, resetOfferRowPort: xyz.swapee.wc.IOfferRowPort.resetOfferRowPort }} */
xyz.swapee.wc.RecordIOfferRowPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.BoundIOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPortFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IOfferRowPort.Inputs>}
 */
$xyz.swapee.wc.BoundIOfferRowPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowPort} */
xyz.swapee.wc.BoundIOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.BoundOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowPort} */
xyz.swapee.wc.BoundOfferRowPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowPort): void} */
xyz.swapee.wc.IOfferRowPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowPort.__resetPort} */
xyz.swapee.wc.IOfferRowPort.__resetPort

// nss:xyz.swapee.wc.IOfferRowPort,$xyz.swapee.wc.IOfferRowPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.resetOfferRowPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowPort.resetOfferRowPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowPort): void} */
xyz.swapee.wc.IOfferRowPort._resetOfferRowPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort} */
xyz.swapee.wc.IOfferRowPort.__resetOfferRowPort

// nss:xyz.swapee.wc.IOfferRowPort,$xyz.swapee.wc.IOfferRowPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowPort.Inputs}
 */
xyz.swapee.wc.IOfferRowPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IOfferRowPort.WeakInputs}
 */
xyz.swapee.wc.IOfferRowPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @interface */
$xyz.swapee.wc.IOfferRowPortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowPortInterface}
 */
xyz.swapee.wc.IOfferRowPortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.OfferRowPortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowPortInterface}
 */
$xyz.swapee.wc.OfferRowPortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowPortInterface}
 */
xyz.swapee.wc.OfferRowPortInterface
/** @type {function(new: xyz.swapee.wc.IOfferRowPortInterface)} */
xyz.swapee.wc.OfferRowPortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/04-IOfferRowPort.xml} xyz.swapee.wc.IOfferRowPortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 375394a170d6c5fc69f298dd78c4741a */
/** @record */
$xyz.swapee.wc.IOfferRowPortInterface.Props = function() {}
/** @type {string} */
$xyz.swapee.wc.IOfferRowPortInterface.Props.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowPortInterface.Props} */
xyz.swapee.wc.IOfferRowPortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPortInterface
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @record */
$xyz.swapee.wc.IOfferRowCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Initialese} */
xyz.swapee.wc.IOfferRowCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @interface */
$xyz.swapee.wc.IOfferRowCoreFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowCore.Model} */
$xyz.swapee.wc.IOfferRowCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCoreFields}
 */
xyz.swapee.wc.IOfferRowCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @interface */
$xyz.swapee.wc.IOfferRowCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowCore} */
$xyz.swapee.wc.IOfferRowCoreCaster.prototype.asIOfferRowCore
/** @type {!xyz.swapee.wc.BoundOfferRowCore} */
$xyz.swapee.wc.IOfferRowCoreCaster.prototype.superOfferRowCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCoreCaster}
 */
xyz.swapee.wc.IOfferRowCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCoreCaster}
 * @extends {xyz.swapee.wc.IOfferRowOuterCore}
 */
$xyz.swapee.wc.IOfferRowCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowCore.prototype.resetOfferRowCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCore}
 */
xyz.swapee.wc.IOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.OfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowCore.Initialese>}
 */
$xyz.swapee.wc.OfferRowCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.OfferRowCore
/** @type {function(new: xyz.swapee.wc.IOfferRowCore)} */
xyz.swapee.wc.OfferRowCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.OfferRowCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.AbstractOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @constructor
 * @extends {xyz.swapee.wc.OfferRowCore}
 */
$xyz.swapee.wc.AbstractOfferRowCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractOfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowCore)} */
xyz.swapee.wc.AbstractOfferRowCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowOuterCore|typeof xyz.swapee.wc.OfferRowOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowCore}
 */
xyz.swapee.wc.AbstractOfferRowCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.RecordIOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/** @typedef {{ resetCore: xyz.swapee.wc.IOfferRowCore.resetCore, resetOfferRowCore: xyz.swapee.wc.IOfferRowCore.resetOfferRowCore }} */
xyz.swapee.wc.RecordIOfferRowCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.BoundIOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowCoreFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCoreCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowOuterCore}
 */
$xyz.swapee.wc.BoundIOfferRowCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowCore} */
xyz.swapee.wc.BoundIOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.BoundOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowCore} */
xyz.swapee.wc.BoundOfferRowCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IOfferRowCore): void} */
xyz.swapee.wc.IOfferRowCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IOfferRowCore.__resetCore} */
xyz.swapee.wc.IOfferRowCore.__resetCore

// nss:xyz.swapee.wc.IOfferRowCore,$xyz.swapee.wc.IOfferRowCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.resetOfferRowCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowCore.resetOfferRowCore
/** @typedef {function(this: xyz.swapee.wc.IOfferRowCore): void} */
xyz.swapee.wc.IOfferRowCore._resetOfferRowCore
/** @typedef {typeof $xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore} */
xyz.swapee.wc.IOfferRowCore.__resetOfferRowCore

// nss:xyz.swapee.wc.IOfferRowCore,$xyz.swapee.wc.IOfferRowCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/09-IOfferRowCore.xml} xyz.swapee.wc.IOfferRowCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 3286d91c81ea55c64f7aadd9a48e048c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model}
 */
$xyz.swapee.wc.IOfferRowCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model} */
xyz.swapee.wc.IOfferRowCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.Initialese} */
xyz.swapee.wc.IOfferRowController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.IOfferRowProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowProcessor.Initialese} */
xyz.swapee.wc.IOfferRowProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @interface */
$xyz.swapee.wc.IOfferRowProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowProcessorCaster.prototype.asIOfferRowProcessor
/** @type {!xyz.swapee.wc.BoundOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowProcessorCaster.prototype.superOfferRowProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowProcessorCaster}
 */
xyz.swapee.wc.IOfferRowProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @interface */
$xyz.swapee.wc.IOfferRowControllerFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowController.Inputs} */
$xyz.swapee.wc.IOfferRowControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowControllerFields}
 */
xyz.swapee.wc.IOfferRowControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @interface */
$xyz.swapee.wc.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowController} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.BoundIOfferRowProcessor} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.asIOfferRowProcessor
/** @type {!xyz.swapee.wc.BoundOfferRowController} */
$xyz.swapee.wc.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowControllerCaster}
 */
xyz.swapee.wc.IOfferRowControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.OfferRowMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.IOfferRowController = function() {}
/** @return {void} */
$xyz.swapee.wc.IOfferRowController.prototype.resetPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowController}
 */
xyz.swapee.wc.IOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.IOfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowProcessorCaster}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 * @extends {xyz.swapee.wc.IOfferRowCore}
 * @extends {xyz.swapee.wc.IOfferRowController}
 */
$xyz.swapee.wc.IOfferRowProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowProcessor}
 */
xyz.swapee.wc.IOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.OfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowProcessor.Initialese>}
 */
$xyz.swapee.wc.OfferRowProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.OfferRowProcessor
/** @type {function(new: xyz.swapee.wc.IOfferRowProcessor, ...!xyz.swapee.wc.IOfferRowProcessor.Initialese)} */
xyz.swapee.wc.OfferRowProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.OfferRowProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.AbstractOfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowProcessor}
 */
$xyz.swapee.wc.AbstractOfferRowProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowProcessor)} */
xyz.swapee.wc.AbstractOfferRowProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowCore|typeof xyz.swapee.wc.OfferRowCore)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowProcessor}
 */
xyz.swapee.wc.AbstractOfferRowProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.OfferRowProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowProcessor, ...!xyz.swapee.wc.IOfferRowProcessor.Initialese)} */
xyz.swapee.wc.OfferRowProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.RecordIOfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.RecordIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @typedef {{ resetPort: xyz.swapee.wc.IOfferRowController.resetPort }} */
xyz.swapee.wc.RecordIOfferRowController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.BoundIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowControllerFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IOfferRowOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.IOfferRowController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IOfferRowController.Inputs, !xyz.swapee.wc.OfferRowMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowController} */
xyz.swapee.wc.BoundIOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.BoundIOfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {xyz.swapee.wc.BoundIOfferRowCore}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 */
$xyz.swapee.wc.BoundIOfferRowProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowProcessor} */
xyz.swapee.wc.BoundIOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/10-IOfferRowProcessor.xml} xyz.swapee.wc.BoundOfferRowProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8cd8ce57ccabe1263ae474971ff19a0a */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowProcessor} */
xyz.swapee.wc.BoundOfferRowProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/100-OfferRowMemory.xml} xyz.swapee.wc.OfferRowMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props dd6c3b0cc30e7f8b22116642c3e4be0f */
/** @record */
$xyz.swapee.wc.OfferRowMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowMemory.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowMemory}
 */
xyz.swapee.wc.OfferRowMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/102-OfferRowInputs.xml} xyz.swapee.wc.front.OfferRowInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ac9945ff9e3cb0ee98b9a82d4b2c817c */
/** @record */
$xyz.swapee.wc.front.OfferRowInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.OfferRowInputs.prototype.core
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.OfferRowInputs}
 */
xyz.swapee.wc.front.OfferRowInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @record */
$xyz.swapee.wc.OfferRowEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IOfferRow} */
$xyz.swapee.wc.OfferRowEnv.prototype.offerRow
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowEnv}
 */
xyz.swapee.wc.OfferRowEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {xyz.swapee.wc.IOfferRowProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.IOfferRow.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRow.Initialese} */
xyz.swapee.wc.IOfferRow.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @interface */
$xyz.swapee.wc.IOfferRowFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRow.Pinout} */
$xyz.swapee.wc.IOfferRowFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowFields}
 */
xyz.swapee.wc.IOfferRowFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @interface */
$xyz.swapee.wc.IOfferRowCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRow} */
$xyz.swapee.wc.IOfferRowCaster.prototype.asIOfferRow
/** @type {!xyz.swapee.wc.BoundOfferRow} */
$xyz.swapee.wc.IOfferRowCaster.prototype.superOfferRow
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowCaster}
 */
xyz.swapee.wc.IOfferRowCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCaster}
 * @extends {xyz.swapee.wc.IOfferRowProcessor}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 * @extends {xyz.swapee.wc.IOfferRowController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, null>}
 */
$xyz.swapee.wc.IOfferRow = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRow}
 */
xyz.swapee.wc.IOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRow}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRow.Initialese>}
 */
$xyz.swapee.wc.OfferRow = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.OfferRow
/** @type {function(new: xyz.swapee.wc.IOfferRow, ...!xyz.swapee.wc.IOfferRow.Initialese)} */
xyz.swapee.wc.OfferRow.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.OfferRow.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.AbstractOfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init
 * @extends {xyz.swapee.wc.OfferRow}
 */
$xyz.swapee.wc.AbstractOfferRow = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRow.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRow}
 */
xyz.swapee.wc.AbstractOfferRow
/** @type {function(new: xyz.swapee.wc.AbstractOfferRow)} */
xyz.swapee.wc.AbstractOfferRow.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRow.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRow}
 */
xyz.swapee.wc.AbstractOfferRow.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRow, ...!xyz.swapee.wc.IOfferRow.Initialese)} */
xyz.swapee.wc.OfferRowConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @record */
$xyz.swapee.wc.IOfferRow.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IOfferRow.Pinout)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IOfferRow.Pinout)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IOfferRow.Pinout} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.OfferRowMemory)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.OfferRowClasses)|undefined} */
$xyz.swapee.wc.IOfferRow.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IOfferRow.MVCOptions} */
xyz.swapee.wc.IOfferRow.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.RecordIOfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.BoundIOfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowFields}
 * @extends {xyz.swapee.wc.RecordIOfferRow}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, null>}
 */
$xyz.swapee.wc.BoundIOfferRow = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRow} */
xyz.swapee.wc.BoundIOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.BoundOfferRow exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRow}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRow = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRow} */
xyz.swapee.wc.BoundOfferRow

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPort.Inputs}
 */
$xyz.swapee.wc.IOfferRowController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.Inputs} */
xyz.swapee.wc.IOfferRowController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRow.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowController.Inputs}
 */
$xyz.swapee.wc.IOfferRow.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRow.Pinout} */
xyz.swapee.wc.IOfferRow.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRow
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.IOfferRowBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.IOfferRowBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowBuffer}
 */
xyz.swapee.wc.IOfferRowBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/11-IOfferRow.xml} xyz.swapee.wc.OfferRowBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 9cb88428f2e7f8c2419673d48954b536 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowBuffer}
 */
$xyz.swapee.wc.OfferRowBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowBuffer}
 */
xyz.swapee.wc.OfferRowBuffer
/** @type {function(new: xyz.swapee.wc.IOfferRowBuffer)} */
xyz.swapee.wc.OfferRowBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowDisplay.Initialese}
 */
$xyz.swapee.wc.IOfferRowGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowGPU.Initialese} */
xyz.swapee.wc.IOfferRowGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowController.Initialese}
 * @extends {xyz.swapee.wc.back.IOfferRowScreen.Initialese}
 * @extends {xyz.swapee.wc.IOfferRow.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowProcessor.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowComputer.Initialese}
 */
$xyz.swapee.wc.IOfferRowHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} */
xyz.swapee.wc.IOfferRowHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @interface */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowHtmlComponent} */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.asIOfferRowHtmlComponent
/** @type {!xyz.swapee.wc.BoundOfferRowHtmlComponent} */
$xyz.swapee.wc.IOfferRowHtmlComponentCaster.prototype.superOfferRowHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 */
xyz.swapee.wc.IOfferRowHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOfferRowGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IOfferRowGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPUFields}
 */
xyz.swapee.wc.IOfferRowGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IOfferRowGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowGPU} */
$xyz.swapee.wc.IOfferRowGPUCaster.prototype.asIOfferRowGPU
/** @type {!xyz.swapee.wc.BoundOfferRowGPU} */
$xyz.swapee.wc.IOfferRowGPUCaster.prototype.superOfferRowGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPUCaster}
 */
xyz.swapee.wc.IOfferRowGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.IOfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!OfferRowMemory,>}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplay}
 */
$xyz.swapee.wc.IOfferRowGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowGPU}
 */
xyz.swapee.wc.IOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.IOfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IOfferRowController}
 * @extends {xyz.swapee.wc.back.IOfferRowScreen}
 * @extends {xyz.swapee.wc.IOfferRow}
 * @extends {xyz.swapee.wc.IOfferRowGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.IOfferRowProcessor}
 * @extends {xyz.swapee.wc.IOfferRowComputer}
 */
$xyz.swapee.wc.IOfferRowHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowHtmlComponent}
 */
xyz.swapee.wc.IOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.OfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.OfferRowHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.OfferRowHtmlComponent
/** @type {function(new: xyz.swapee.wc.IOfferRowHtmlComponent, ...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese)} */
xyz.swapee.wc.OfferRowHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.OfferRowHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.AbstractOfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowHtmlComponent}
 */
$xyz.swapee.wc.AbstractOfferRowHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowHtmlComponent)} */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowHtmlComponent|typeof xyz.swapee.wc.OfferRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.IOfferRow|typeof xyz.swapee.wc.OfferRow)|(!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferRowProcessor|typeof xyz.swapee.wc.OfferRowProcessor)|(!xyz.swapee.wc.IOfferRowComputer|typeof xyz.swapee.wc.OfferRowComputer))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferRowHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.OfferRowHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowHtmlComponent, ...!xyz.swapee.wc.IOfferRowHtmlComponent.Initialese)} */
xyz.swapee.wc.OfferRowHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.RecordIOfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.RecordIOfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.BoundIOfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowGPUFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!OfferRowMemory,>}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowDisplay}
 */
$xyz.swapee.wc.BoundIOfferRowGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowGPU} */
xyz.swapee.wc.BoundIOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.BoundIOfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowController}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreen}
 * @extends {xyz.swapee.wc.BoundIOfferRow}
 * @extends {xyz.swapee.wc.BoundIOfferRowGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.IOfferRowController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundIOfferRowProcessor}
 * @extends {xyz.swapee.wc.BoundIOfferRowComputer}
 */
$xyz.swapee.wc.BoundIOfferRowHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowHtmlComponent} */
xyz.swapee.wc.BoundIOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/12-IOfferRowHtmlComponent.xml} xyz.swapee.wc.BoundOfferRowHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 584d450b1e202ae3f31b2031b74d72f2 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowHtmlComponent} */
xyz.swapee.wc.BoundOfferRowHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @interface */
$xyz.swapee.wc.IOfferRowDesigner = function() {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IOfferRowDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.OfferRowClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IOfferRowDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDesigner}
 */
xyz.swapee.wc.IOfferRowDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.OfferRowDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IOfferRowDesigner}
 */
$xyz.swapee.wc.OfferRowDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowDesigner}
 */
xyz.swapee.wc.OfferRowDesigner
/** @type {function(new: xyz.swapee.wc.IOfferRowDesigner)} */
xyz.swapee.wc.OfferRowDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh.prototype.OfferRow
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh} */
xyz.swapee.wc.IOfferRowDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh.prototype.OfferRow
/** @type {typeof xyz.swapee.wc.IOfferRowController} */
$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.relay.Mesh} */
xyz.swapee.wc.IOfferRowDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/170-IOfferRowDesigner.xml} xyz.swapee.wc.IOfferRowDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ad1d1910718a9aacadbc6b1a762e658f */
/** @record */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.OfferRowMemory} */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool.prototype.OfferRow
/** @type {!xyz.swapee.wc.OfferRowMemory} */
$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IOfferRowDesigner.relay.MemPool} */
xyz.swapee.wc.IOfferRowDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings>}
 */
$xyz.swapee.wc.IOfferRowDisplay.Initialese = function() {}
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.ExchangeCollapsar
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferExchange
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.BestOfferPlaque
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.ProgressVertLine
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CoinImWr
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDealLa
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CancelLa
/** @type {HTMLAnchorElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDealBuA
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferCrypto
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.OfferAmount
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.Logo
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.Eta
/** @type {HTMLDivElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.GetDeal
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.RecHandle
/** @type {HTMLSpanElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.RecPopup
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FloatingIco
/** @type {HTMLImageElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FixedIco
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.CoinIm
/** @type {HTMLParagraphElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FloatingLa
/** @type {HTMLParagraphElement|undefined} */
$xyz.swapee.wc.IOfferRowDisplay.Initialese.prototype.FixedLa
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Initialese} */
xyz.swapee.wc.IOfferRowDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @interface */
$xyz.swapee.wc.IOfferRowDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IOfferRowDisplay.Settings} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IOfferRowDisplay.Queries} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.queries
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.ExchangeCollapsar
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferExchange
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.BestOfferPlaque
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.ProgressVertLine
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinImWr
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealLa
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CancelLa
/** @type {HTMLAnchorElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDealBuA
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferCrypto
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.OfferAmount
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.Logo
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.Eta
/** @type {HTMLDivElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.GetDeal
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecHandle
/** @type {HTMLSpanElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.RecPopup
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingIco
/** @type {HTMLImageElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedIco
/** @type {HTMLElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.CoinIm
/** @type {HTMLParagraphElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FloatingLa
/** @type {HTMLParagraphElement} */
$xyz.swapee.wc.IOfferRowDisplayFields.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplayFields}
 */
xyz.swapee.wc.IOfferRowDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @interface */
$xyz.swapee.wc.IOfferRowDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowDisplay} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/** @type {!xyz.swapee.wc.BoundIOfferRowScreen} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.BoundOfferRowDisplay} */
$xyz.swapee.wc.IOfferRowDisplayCaster.prototype.superOfferRowDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplayCaster}
 */
xyz.swapee.wc.IOfferRowDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IOfferRowDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.OfferRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 */
$xyz.swapee.wc.IOfferRowDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowDisplay}
 */
xyz.swapee.wc.IOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.OfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowDisplay.Initialese>}
 */
$xyz.swapee.wc.OfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.OfferRowDisplay
/** @type {function(new: xyz.swapee.wc.IOfferRowDisplay, ...!xyz.swapee.wc.IOfferRowDisplay.Initialese)} */
xyz.swapee.wc.OfferRowDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.OfferRowDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.AbstractOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowDisplay}
 */
$xyz.swapee.wc.AbstractOfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowDisplay)} */
xyz.swapee.wc.AbstractOfferRowDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowDisplay}
 */
xyz.swapee.wc.AbstractOfferRowDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.OfferRowDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowDisplay, ...!xyz.swapee.wc.IOfferRowDisplay.Initialese)} */
xyz.swapee.wc.OfferRowDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.OfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowGPU.Initialese>}
 */
$xyz.swapee.wc.OfferRowGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.OfferRowGPU
/** @type {function(new: xyz.swapee.wc.IOfferRowGPU, ...!xyz.swapee.wc.IOfferRowGPU.Initialese)} */
xyz.swapee.wc.OfferRowGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.OfferRowGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.AbstractOfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowGPU}
 */
$xyz.swapee.wc.AbstractOfferRowGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowGPU)} */
xyz.swapee.wc.AbstractOfferRowGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowGPU|typeof xyz.swapee.wc.OfferRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowGPU}
 */
xyz.swapee.wc.AbstractOfferRowGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.OfferRowGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowGPU, ...!xyz.swapee.wc.IOfferRowGPU.Initialese)} */
xyz.swapee.wc.OfferRowGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/80-IOfferRowGPU.xml} xyz.swapee.wc.BoundOfferRowGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowGPU} */
xyz.swapee.wc.BoundOfferRowGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.RecordIOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @typedef {{ paint: xyz.swapee.wc.IOfferRowDisplay.paint }} */
xyz.swapee.wc.RecordIOfferRowDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.BoundIOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowDisplayFields}
 * @extends {xyz.swapee.wc.RecordIOfferRowDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.OfferRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowDisplay} */
xyz.swapee.wc.BoundIOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.BoundOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowDisplay} */
xyz.swapee.wc.BoundOfferRowDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OfferRowMemory, null): void} */
xyz.swapee.wc.IOfferRowDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IOfferRowDisplay, !xyz.swapee.wc.OfferRowMemory, null): void} */
xyz.swapee.wc.IOfferRowDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IOfferRowDisplay.__paint} */
xyz.swapee.wc.IOfferRowDisplay.__paint

// nss:xyz.swapee.wc.IOfferRowDisplay,$xyz.swapee.wc.IOfferRowDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/** @record */
$xyz.swapee.wc.IOfferRowDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Queries} */
xyz.swapee.wc.IOfferRowDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplay.xml} xyz.swapee.wc.IOfferRowDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0008d60c8ca4dcb38836a45ed9b69696 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowDisplay.Queries}
 */
$xyz.swapee.wc.IOfferRowDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowDisplay.Settings} */
xyz.swapee.wc.IOfferRowDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OfferRowClasses>}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.ExchangeCollapsar
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferExchange
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.BestOfferPlaque
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.ProgressVertLine
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CoinImWr
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDealLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CancelLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDealBuA
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferCrypto
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.OfferAmount
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.Logo
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.Eta
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.GetDeal
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.RecHandle
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.RecPopup
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FloatingIco
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FixedIco
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.CoinIm
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FloatingLa
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IOfferRowDisplay.Initialese.prototype.FixedLa
/** @typedef {$xyz.swapee.wc.back.IOfferRowDisplay.Initialese} */
xyz.swapee.wc.back.IOfferRowDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ExchangeCollapsar
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferExchange
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.BestOfferPlaque
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.ProgressVertLine
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinImWr
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CancelLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDealBuA
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferCrypto
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.OfferAmount
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Logo
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.Eta
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.GetDeal
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecHandle
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.RecPopup
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingIco
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedIco
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.CoinIm
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FloatingLa
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IOfferRowDisplayFields.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplayFields}
 */
xyz.swapee.wc.back.IOfferRowDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowDisplay} */
$xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.asIOfferRowDisplay
/** @type {!xyz.swapee.wc.back.BoundOfferRowDisplay} */
$xyz.swapee.wc.back.IOfferRowDisplayCaster.prototype.superOfferRowDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplayCaster}
 */
xyz.swapee.wc.back.IOfferRowDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.OfferRowClasses, null>}
 */
$xyz.swapee.wc.back.IOfferRowDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.OfferRowMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowDisplay}
 */
xyz.swapee.wc.back.IOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.OfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IOfferRowDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowDisplay.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.OfferRowDisplay
/** @type {function(new: xyz.swapee.wc.back.IOfferRowDisplay)} */
xyz.swapee.wc.back.OfferRowDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.OfferRowDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.AbstractOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.OfferRowDisplay}
 */
$xyz.swapee.wc.back.AbstractOfferRowDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowDisplay)} */
xyz.swapee.wc.back.AbstractOfferRowDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowDisplay|typeof xyz.swapee.wc.back.OfferRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferRowDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.OfferCrypto
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.OfferAmount
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.LogoDark
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.LogoLight
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.Eta
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FloatingIco
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FixedIco
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FloatingLa
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusPQs.prototype.FixedLa
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowVdusPQs}
 */
xyz.swapee.wc.OfferRowVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a71
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a72
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a73
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a74
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a75
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a76
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a77
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a78
/** @type {string} */
$xyz.swapee.wc.OfferRowVdusQPs.prototype.h8a79
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowVdusQPs}
 */
xyz.swapee.wc.OfferRowVdusQPs
/** @type {function(new: xyz.swapee.wc.OfferRowVdusQPs)} */
xyz.swapee.wc.OfferRowVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.RecordIOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/** @typedef {{ paint: xyz.swapee.wc.back.IOfferRowDisplay.paint }} */
xyz.swapee.wc.back.RecordIOfferRowDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.BoundIOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIOfferRowDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.OfferRowClasses, null>}
 */
$xyz.swapee.wc.back.BoundIOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowDisplay} */
xyz.swapee.wc.back.BoundIOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.BoundOfferRowDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowDisplay} */
xyz.swapee.wc.back.BoundOfferRowDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/40-IOfferRowDisplayBack.xml} xyz.swapee.wc.back.IOfferRowDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0179b240847b8ba9aef4cbcc91aeb1f0 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.OfferRowMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IOfferRowDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.OfferRowMemory=, null=): void} */
xyz.swapee.wc.back.IOfferRowDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IOfferRowDisplay, !xyz.swapee.wc.OfferRowMemory=, null=): void} */
xyz.swapee.wc.back.IOfferRowDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IOfferRowDisplay.__paint} */
xyz.swapee.wc.back.IOfferRowDisplay.__paint

// nss:xyz.swapee.wc.back.IOfferRowDisplay,$xyz.swapee.wc.back.IOfferRowDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowClassesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.OfferRowClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.OfferRowClassesPQs.prototype.BestOffer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.OfferRowClassesPQs}
 */
xyz.swapee.wc.OfferRowClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/110-OfferRowSerDes.xml} xyz.swapee.wc.OfferRowClassesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.OfferRowClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.OfferRowClassesQPs.prototype.e8977
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.OfferRowClassesQPs}
 */
xyz.swapee.wc.OfferRowClassesQPs
/** @type {function(new: xyz.swapee.wc.OfferRowClassesQPs)} */
xyz.swapee.wc.OfferRowClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/41-OfferRowClasses.xml} xyz.swapee.wc.OfferRowClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 636dd29c56ccc1aee1b43bb34f644c6d */
/** @record */
$xyz.swapee.wc.OfferRowClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.BestOffer
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.Recommended
/** @type {string|undefined} */
$xyz.swapee.wc.OfferRowClasses.prototype.ProgressVertLine
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.OfferRowClasses}
 */
xyz.swapee.wc.OfferRowClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.OfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.OfferRowController
/** @type {function(new: xyz.swapee.wc.IOfferRowController, ...!xyz.swapee.wc.IOfferRowController.Initialese)} */
xyz.swapee.wc.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.OfferRowController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.AbstractOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowController}
 */
$xyz.swapee.wc.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowController)} */
xyz.swapee.wc.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowController}
 */
xyz.swapee.wc.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.OfferRowControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowController, ...!xyz.swapee.wc.IOfferRowController.Initialese)} */
xyz.swapee.wc.OfferRowControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.BoundOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowController} */
xyz.swapee.wc.BoundOfferRowController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IOfferRowController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IOfferRowController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IOfferRowController): void} */
xyz.swapee.wc.IOfferRowController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IOfferRowController.__resetPort} */
xyz.swapee.wc.IOfferRowController.__resetPort

// nss:xyz.swapee.wc.IOfferRowController,$xyz.swapee.wc.IOfferRowController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/50-IOfferRowController.xml} xyz.swapee.wc.IOfferRowController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61014d20fdf80ac0d90742221e207e0c */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowPort.WeakInputs}
 */
$xyz.swapee.wc.IOfferRowController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowController.WeakInputs} */
xyz.swapee.wc.IOfferRowController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @record */
$xyz.swapee.wc.front.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowController.Initialese} */
xyz.swapee.wc.front.IOfferRowController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @interface */
$xyz.swapee.wc.front.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowController} */
$xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.front.BoundOfferRowController} */
$xyz.swapee.wc.front.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerCaster}
 */
xyz.swapee.wc.front.IOfferRowControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @interface */
$xyz.swapee.wc.front.IOfferRowControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowControllerAT} */
$xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.asIOfferRowControllerAT
/** @type {!xyz.swapee.wc.front.BoundOfferRowControllerAT} */
$xyz.swapee.wc.front.IOfferRowControllerATCaster.prototype.superOfferRowControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerATCaster}
 */
xyz.swapee.wc.front.IOfferRowControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IOfferRowControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowControllerAT}
 */
xyz.swapee.wc.front.IOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.IOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerAT}
 */
$xyz.swapee.wc.front.IOfferRowController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowController}
 */
xyz.swapee.wc.front.IOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.OfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.OfferRowController
/** @type {function(new: xyz.swapee.wc.front.IOfferRowController, ...!xyz.swapee.wc.front.IOfferRowController.Initialese)} */
xyz.swapee.wc.front.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.OfferRowController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.AbstractOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowController}
 */
$xyz.swapee.wc.front.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowController)} */
xyz.swapee.wc.front.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowController}
 */
xyz.swapee.wc.front.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.OfferRowControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowController, ...!xyz.swapee.wc.front.IOfferRowController.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.RecordIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.RecordIOfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.BoundIOfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIOfferRowControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowControllerAT} */
xyz.swapee.wc.front.BoundIOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.BoundIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIOfferRowControllerAT}
 */
$xyz.swapee.wc.front.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowController} */
xyz.swapee.wc.front.BoundIOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/51-IOfferRowControllerFront.xml} xyz.swapee.wc.front.BoundOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 36205a9c97a719eb4a9ff88cb0187295 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowController} */
xyz.swapee.wc.front.BoundOfferRowController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOfferRowController.Inputs>}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowController.Initialese} */
xyz.swapee.wc.back.IOfferRowController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowController} */
$xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.asIOfferRowController
/** @type {!xyz.swapee.wc.back.BoundOfferRowController} */
$xyz.swapee.wc.back.IOfferRowControllerCaster.prototype.superOfferRowController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerCaster}
 */
xyz.swapee.wc.back.IOfferRowControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.IOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.IOfferRowController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.back.IOfferRowController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowController}
 */
xyz.swapee.wc.back.IOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.OfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowController.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.OfferRowController
/** @type {function(new: xyz.swapee.wc.back.IOfferRowController, ...!xyz.swapee.wc.back.IOfferRowController.Initialese)} */
xyz.swapee.wc.back.OfferRowController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.OfferRowController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.AbstractOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowController}
 */
$xyz.swapee.wc.back.AbstractOfferRowController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowController)} */
xyz.swapee.wc.back.AbstractOfferRowController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowController|typeof xyz.swapee.wc.back.OfferRowController)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowController}
 */
xyz.swapee.wc.back.AbstractOfferRowController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.OfferRowControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowController, ...!xyz.swapee.wc.back.IOfferRowController.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.RecordIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.BoundIOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerCaster}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IOfferRowController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowController} */
xyz.swapee.wc.back.BoundIOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/52-IOfferRowControllerBack.xml} xyz.swapee.wc.back.BoundOfferRowController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 619c14cb00310a057dd6fab19f2d9717 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowController} */
xyz.swapee.wc.back.BoundOfferRowController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowController.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} */
xyz.swapee.wc.back.IOfferRowControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowControllerAR} */
$xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.asIOfferRowControllerAR
/** @type {!xyz.swapee.wc.back.BoundOfferRowControllerAR} */
$xyz.swapee.wc.back.IOfferRowControllerARCaster.prototype.superOfferRowControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerARCaster}
 */
xyz.swapee.wc.back.IOfferRowControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.IOfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOfferRowController}
 */
$xyz.swapee.wc.back.IOfferRowControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowControllerAR}
 */
xyz.swapee.wc.back.IOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.OfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.OfferRowControllerAR
/** @type {function(new: xyz.swapee.wc.back.IOfferRowControllerAR, ...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.OfferRowControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.AbstractOfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowControllerAR}
 */
$xyz.swapee.wc.back.AbstractOfferRowControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowControllerAR)} */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowControllerAR|typeof xyz.swapee.wc.back.OfferRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowController|typeof xyz.swapee.wc.OfferRowController))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferRowControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.OfferRowControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowControllerAR, ...!xyz.swapee.wc.back.IOfferRowControllerAR.Initialese)} */
xyz.swapee.wc.back.OfferRowControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.RecordIOfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.BoundIOfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOfferRowController}
 */
$xyz.swapee.wc.back.BoundIOfferRowControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowControllerAR} */
xyz.swapee.wc.back.BoundIOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/53-IOfferRowControllerAR.xml} xyz.swapee.wc.back.BoundOfferRowControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a7288818b9a2364b615313e4a21f6ce6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowControllerAR} */
xyz.swapee.wc.back.BoundOfferRowControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.IOfferRowControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IOfferRowControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} */
xyz.swapee.wc.front.IOfferRowControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.OfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.OfferRowControllerAT
/** @type {function(new: xyz.swapee.wc.front.IOfferRowControllerAT, ...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.OfferRowControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.AbstractOfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowControllerAT}
 */
$xyz.swapee.wc.front.AbstractOfferRowControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowControllerAT)} */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowControllerAT|typeof xyz.swapee.wc.front.OfferRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferRowControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.OfferRowControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowControllerAT, ...!xyz.swapee.wc.front.IOfferRowControllerAT.Initialese)} */
xyz.swapee.wc.front.OfferRowControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/54-IOfferRowControllerAT.xml} xyz.swapee.wc.front.BoundOfferRowControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d7a2c2c00dec88f399fa3f96b2627353 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowControllerAT} */
xyz.swapee.wc.front.BoundOfferRowControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IOfferRowDisplay.Initialese}
 */
$xyz.swapee.wc.IOfferRowScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowScreen.Initialese} */
xyz.swapee.wc.IOfferRowScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @interface */
$xyz.swapee.wc.IOfferRowScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIOfferRowScreen} */
$xyz.swapee.wc.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.BoundOfferRowScreen} */
$xyz.swapee.wc.IOfferRowScreenCaster.prototype.superOfferRowScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowScreenCaster}
 */
xyz.swapee.wc.IOfferRowScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.IOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IOfferRowScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IOfferRowController}
 * @extends {xyz.swapee.wc.IOfferRowDisplay}
 */
$xyz.swapee.wc.IOfferRowScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IOfferRowScreen}
 */
xyz.swapee.wc.IOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.OfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init
 * @implements {xyz.swapee.wc.IOfferRowScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferRowScreen.Initialese>}
 */
$xyz.swapee.wc.OfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.OfferRowScreen
/** @type {function(new: xyz.swapee.wc.IOfferRowScreen, ...!xyz.swapee.wc.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.OfferRowScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.OfferRowScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.AbstractOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init
 * @extends {xyz.swapee.wc.OfferRowScreen}
 */
$xyz.swapee.wc.AbstractOfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractOfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen
/** @type {function(new: xyz.swapee.wc.AbstractOfferRowScreen)} */
xyz.swapee.wc.AbstractOfferRowScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferRowScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractOfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.continues
/**
 * @param {...((!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferRowController|typeof xyz.swapee.wc.front.OfferRowController)|(!xyz.swapee.wc.IOfferRowDisplay|typeof xyz.swapee.wc.OfferRowDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.OfferRowScreen}
 */
xyz.swapee.wc.AbstractOfferRowScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.OfferRowScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @typedef {function(new: xyz.swapee.wc.IOfferRowScreen, ...!xyz.swapee.wc.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.OfferRowScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.RecordIOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIOfferRowScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.BoundIOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIOfferRowScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IOfferRowScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.OfferRowMemory, !xyz.swapee.wc.front.OfferRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferRowDisplay.Settings, !xyz.swapee.wc.IOfferRowDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIOfferRowController}
 * @extends {xyz.swapee.wc.BoundIOfferRowDisplay}
 */
$xyz.swapee.wc.BoundIOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundIOfferRowScreen} */
xyz.swapee.wc.BoundIOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreen.xml} xyz.swapee.wc.BoundOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 72526e49bbcb557a3cd2db158ea115a0 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIOfferRowScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundOfferRowScreen} */
xyz.swapee.wc.BoundOfferRowScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} */
xyz.swapee.wc.back.IOfferRowScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IOfferRowScreenAT.Initialese}
 */
$xyz.swapee.wc.back.IOfferRowScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IOfferRowScreen.Initialese} */
xyz.swapee.wc.back.IOfferRowScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IOfferRowScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowScreen} */
$xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.asIOfferRowScreen
/** @type {!xyz.swapee.wc.back.BoundOfferRowScreen} */
$xyz.swapee.wc.back.IOfferRowScreenCaster.prototype.superOfferRowScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenCaster}
 */
xyz.swapee.wc.back.IOfferRowScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @interface */
$xyz.swapee.wc.back.IOfferRowScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIOfferRowScreenAT} */
$xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.asIOfferRowScreenAT
/** @type {!xyz.swapee.wc.back.BoundOfferRowScreenAT} */
$xyz.swapee.wc.back.IOfferRowScreenATCaster.prototype.superOfferRowScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenATCaster}
 */
xyz.swapee.wc.back.IOfferRowScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.IOfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.IOfferRowScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreenAT}
 */
xyz.swapee.wc.back.IOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.IOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenCaster}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenAT}
 */
$xyz.swapee.wc.back.IOfferRowScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IOfferRowScreen}
 */
xyz.swapee.wc.back.IOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.OfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreen.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.OfferRowScreen
/** @type {function(new: xyz.swapee.wc.back.IOfferRowScreen, ...!xyz.swapee.wc.back.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.back.OfferRowScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.OfferRowScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.AbstractOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowScreen}
 */
$xyz.swapee.wc.back.AbstractOfferRowScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowScreen)} */
xyz.swapee.wc.back.AbstractOfferRowScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreen|typeof xyz.swapee.wc.back.OfferRowScreen)|(!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferRowScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.OfferRowScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreen, ...!xyz.swapee.wc.back.IOfferRowScreen.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.RecordIOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.RecordIOfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIOfferRowScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.BoundIOfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundIOfferRowScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowScreenAT} */
xyz.swapee.wc.back.BoundIOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.BoundIOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIOfferRowScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IOfferRowScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreenAT}
 */
$xyz.swapee.wc.back.BoundIOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIOfferRowScreen} */
xyz.swapee.wc.back.BoundIOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/70-IOfferRowScreenBack.xml} xyz.swapee.wc.back.BoundOfferRowScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 32fabe642a3896e68ab527694ef37182 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowScreen} */
xyz.swapee.wc.back.BoundOfferRowScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IOfferRowScreen.Initialese}
 */
$xyz.swapee.wc.front.IOfferRowScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} */
xyz.swapee.wc.front.IOfferRowScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IOfferRowScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @interface */
$xyz.swapee.wc.front.IOfferRowScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIOfferRowScreenAR} */
$xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.asIOfferRowScreenAR
/** @type {!xyz.swapee.wc.front.BoundOfferRowScreenAR} */
$xyz.swapee.wc.front.IOfferRowScreenARCaster.prototype.superOfferRowScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowScreenARCaster}
 */
xyz.swapee.wc.front.IOfferRowScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.IOfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IOfferRowScreen}
 */
$xyz.swapee.wc.front.IOfferRowScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IOfferRowScreenAR}
 */
xyz.swapee.wc.front.IOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.OfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IOfferRowScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.OfferRowScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.OfferRowScreenAR
/** @type {function(new: xyz.swapee.wc.front.IOfferRowScreenAR, ...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese)} */
xyz.swapee.wc.front.OfferRowScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.OfferRowScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.AbstractOfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.OfferRowScreenAR}
 */
$xyz.swapee.wc.front.AbstractOfferRowScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractOfferRowScreenAR)} */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractOfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.IOfferRowScreenAR|typeof xyz.swapee.wc.front.OfferRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferRowScreen|typeof xyz.swapee.wc.OfferRowScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.OfferRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferRowScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.OfferRowScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @typedef {function(new: xyz.swapee.wc.front.IOfferRowScreenAR, ...!xyz.swapee.wc.front.IOfferRowScreenAR.Initialese)} */
xyz.swapee.wc.front.OfferRowScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.RecordIOfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIOfferRowScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.BoundIOfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIOfferRowScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IOfferRowScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIOfferRowScreen}
 */
$xyz.swapee.wc.front.BoundIOfferRowScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIOfferRowScreenAR} */
xyz.swapee.wc.front.BoundIOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/73-IOfferRowScreenAR.xml} xyz.swapee.wc.front.BoundOfferRowScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0d065f35bf84b82967777859642362bf */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIOfferRowScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundOfferRowScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundOfferRowScreenAR} */
xyz.swapee.wc.front.BoundOfferRowScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.OfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IOfferRowScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.OfferRowScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.OfferRowScreenAT
/** @type {function(new: xyz.swapee.wc.back.IOfferRowScreenAT, ...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.OfferRowScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.AbstractOfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.OfferRowScreenAT}
 */
$xyz.swapee.wc.back.AbstractOfferRowScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractOfferRowScreenAT)} */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractOfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.IOfferRowScreenAT|typeof xyz.swapee.wc.back.OfferRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.OfferRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferRowScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.OfferRowScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/** @typedef {function(new: xyz.swapee.wc.back.IOfferRowScreenAT, ...!xyz.swapee.wc.back.IOfferRowScreenAT.Initialese)} */
xyz.swapee.wc.back.OfferRowScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/74-IOfferRowScreenAT.xml} xyz.swapee.wc.back.BoundOfferRowScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bb05620a959396e5142b606c6957acf6 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIOfferRowScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundOfferRowScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundOfferRowScreenAT} */
xyz.swapee.wc.back.BoundOfferRowScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe} */
xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/** @record */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe.prototype.core
/** @typedef {$xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe} */
xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.Inputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Inputs.Core} */
xyz.swapee.wc.IOfferRowPort.Inputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe} */
xyz.swapee.wc.IOfferRowPort.Inputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core} */
xyz.swapee.wc.IOfferRowPort.WeakInputs.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.WeakModel.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe} */
xyz.swapee.wc.IOfferRowPort.WeakInputs.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowCore.Model.Core exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core}
 */
$xyz.swapee.wc.IOfferRowCore.Model.Core = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model.Core} */
xyz.swapee.wc.IOfferRowCore.Model.Core

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-row/OfferRow.mvc/design/03-IOfferRowOuterCore.xml} xyz.swapee.wc.IOfferRowCore.Model.Core_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5e367668441bad07d65fdb13d2baff20 */
/**
 * @record
 * @extends {xyz.swapee.wc.IOfferRowOuterCore.Model.Core_Safe}
 */
$xyz.swapee.wc.IOfferRowCore.Model.Core_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IOfferRowCore.Model.Core_Safe} */
xyz.swapee.wc.IOfferRowCore.Model.Core_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IOfferRowCore.Model
/* @typal-end */