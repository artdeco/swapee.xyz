import AbstractHyperMotionPathInterruptLine from '../../../../gen/AbstractMotionPathInterruptLine/hyper/AbstractHyperMotionPathInterruptLine'
import MotionPathInterruptLine from '../../MotionPathInterruptLine'
import MotionPathInterruptLineGeneralAspects from '../MotionPathInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperMotionPathInterruptLine} */
export default class extends AbstractHyperMotionPathInterruptLine
 .consults(
  MotionPathInterruptLineGeneralAspects,
 )
 .implements(
  MotionPathInterruptLine,
 )
{}