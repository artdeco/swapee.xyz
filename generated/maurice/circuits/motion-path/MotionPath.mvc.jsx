/** @extends {xyz.swapee.wc.AbstractMotionPath} */
export default class AbstractMotionPath extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractMotionPathComputer} */
export class AbstractMotionPathComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractMotionPathController} */
export class AbstractMotionPathController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractMotionPathPort} */
export class MotionPathPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractMotionPathView} */
export class AbstractMotionPathView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractMotionPathElement} */
export class AbstractMotionPathElement extends (<element v3 html mv>
 <block src="./MotionPath.mvc/src/MotionPathElement/methods/render.jsx" />
 <inducer src="./MotionPath.mvc/src/MotionPathElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractMotionPathHtmlComponent} */
export class AbstractMotionPathHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>