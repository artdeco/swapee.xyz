import {MotionPathInputsPQs} from './MotionPathInputsPQs'
export const MotionPathInputsQPs=/**@type {!xyz.swapee.wc.MotionPathInputsQPs}*/(Object.keys(MotionPathInputsPQs)
 .reduce((a,k)=>{a[MotionPathInputsPQs[k]]=k;return a},{}))