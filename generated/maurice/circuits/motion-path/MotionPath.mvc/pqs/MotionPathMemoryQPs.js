import {MotionPathMemoryPQs} from './MotionPathMemoryPQs'
export const MotionPathMemoryQPs=/**@type {!xyz.swapee.wc.MotionPathMemoryQPs}*/(Object.keys(MotionPathMemoryPQs)
 .reduce((a,k)=>{a[MotionPathMemoryPQs[k]]=k;return a},{}))