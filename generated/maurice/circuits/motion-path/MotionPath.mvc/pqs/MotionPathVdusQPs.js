import {MotionPathVdusPQs} from './MotionPathVdusPQs'
export const MotionPathVdusQPs=/**@type {!xyz.swapee.wc.MotionPathVdusQPs}*/(Object.keys(MotionPathVdusPQs)
 .reduce((a,k)=>{a[MotionPathVdusPQs[k]]=k;return a},{}))