/**
 * Display for presenting information from the _IMotionPath_.
 * @extends {xyz.swapee.wc.MotionPathDisplay}
 */
class MotionPathDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.MotionPathScreen}
 */
class MotionPathScreen extends (class {/* lazy-loaded */}) {}

module.exports.MotionPathDisplay = MotionPathDisplay
module.exports.MotionPathScreen = MotionPathScreen