/**
 * An abstract class of `xyz.swapee.wc.IMotionPath` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPath}
 */
class AbstractMotionPath extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.MotionPathPort}
 */
class MotionPathPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathController` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPathController}
 */
class AbstractMotionPathController extends (class {/* lazy-loaded */}) {}
/**
 * The _IMotionPath_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.MotionPathHtmlComponent}
 */
class MotionPathHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.MotionPathBuffer}
 */
class MotionPathBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathComputer` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPathComputer}
 */
class AbstractMotionPathComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.MotionPathController}
 */
class MotionPathController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractMotionPath = AbstractMotionPath
module.exports.MotionPathPort = MotionPathPort
module.exports.AbstractMotionPathController = AbstractMotionPathController
module.exports.MotionPathHtmlComponent = MotionPathHtmlComponent
module.exports.MotionPathBuffer = MotionPathBuffer
module.exports.AbstractMotionPathComputer = AbstractMotionPathComputer
module.exports.MotionPathController = MotionPathController