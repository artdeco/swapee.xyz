import Module from './element'

/**@extends {xyz.swapee.wc.AbstractMotionPath}*/
export class AbstractMotionPath extends Module['32099806271'] {}
/** @type {typeof xyz.swapee.wc.AbstractMotionPath} */
AbstractMotionPath.class=function(){}
/** @type {typeof xyz.swapee.wc.MotionPathPort} */
export const MotionPathPort=Module['32099806273']
/**@extends {xyz.swapee.wc.AbstractMotionPathController}*/
export class AbstractMotionPathController extends Module['32099806274'] {}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathController} */
AbstractMotionPathController.class=function(){}
/** @type {typeof xyz.swapee.wc.MotionPathElement} */
export const MotionPathElement=Module['32099806278']
/** @type {typeof xyz.swapee.wc.MotionPathBuffer} */
export const MotionPathBuffer=Module['320998062711']
/**@extends {xyz.swapee.wc.AbstractMotionPathComputer}*/
export class AbstractMotionPathComputer extends Module['320998062730'] {}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathComputer} */
AbstractMotionPathComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.MotionPathController} */
export const MotionPathController=Module['320998062761']