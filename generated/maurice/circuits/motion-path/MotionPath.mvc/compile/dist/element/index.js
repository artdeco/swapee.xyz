/**
 * An abstract class of `xyz.swapee.wc.IMotionPath` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPath}
 */
class AbstractMotionPath extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.MotionPathPort}
 */
class MotionPathPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathController` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPathController}
 */
class AbstractMotionPathController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.MotionPathElement}
 */
class MotionPathElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.MotionPathBuffer}
 */
class MotionPathBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathComputer` interface.
 * @extends {xyz.swapee.wc.AbstractMotionPathComputer}
 */
class AbstractMotionPathComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.MotionPathController}
 */
class MotionPathController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractMotionPath = AbstractMotionPath
module.exports.MotionPathPort = MotionPathPort
module.exports.AbstractMotionPathController = AbstractMotionPathController
module.exports.MotionPathElement = MotionPathElement
module.exports.MotionPathBuffer = MotionPathBuffer
module.exports.AbstractMotionPathComputer = AbstractMotionPathComputer
module.exports.MotionPathController = MotionPathController

Object.defineProperties(module.exports, {
 'AbstractMotionPath': {get: () => require('./precompile/internal')[32099806271]},
 [32099806271]: {get: () => module.exports['AbstractMotionPath']},
 'MotionPathPort': {get: () => require('./precompile/internal')[32099806273]},
 [32099806273]: {get: () => module.exports['MotionPathPort']},
 'AbstractMotionPathController': {get: () => require('./precompile/internal')[32099806274]},
 [32099806274]: {get: () => module.exports['AbstractMotionPathController']},
 'MotionPathElement': {get: () => require('./precompile/internal')[32099806278]},
 [32099806278]: {get: () => module.exports['MotionPathElement']},
 'MotionPathBuffer': {get: () => require('./precompile/internal')[320998062711]},
 [320998062711]: {get: () => module.exports['MotionPathBuffer']},
 'AbstractMotionPathComputer': {get: () => require('./precompile/internal')[320998062730]},
 [320998062730]: {get: () => module.exports['AbstractMotionPathComputer']},
 'MotionPathController': {get: () => require('./precompile/internal')[320998062761]},
 [320998062761]: {get: () => module.exports['MotionPathController']},
})