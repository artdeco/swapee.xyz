import { AbstractMotionPath, MotionPathPort, AbstractMotionPathController,
 MotionPathElement, MotionPathBuffer, AbstractMotionPathComputer,
 MotionPathController } from './element-exports'

/** @lazy @api {xyz.swapee.wc.AbstractMotionPath} */
export { AbstractMotionPath }
/** @lazy @api {xyz.swapee.wc.MotionPathPort} */
export { MotionPathPort }
/** @lazy @api {xyz.swapee.wc.AbstractMotionPathController} */
export { AbstractMotionPathController }
/** @lazy @api {xyz.swapee.wc.MotionPathElement} */
export { MotionPathElement }
/** @lazy @api {xyz.swapee.wc.MotionPathBuffer} */
export { MotionPathBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractMotionPathComputer} */
export { AbstractMotionPathComputer }
/** @lazy @api {xyz.swapee.wc.MotionPathController} */
export { MotionPathController }