import { AbstractMotionPath, MotionPathPort, AbstractMotionPathController,
 MotionPathHtmlComponent, MotionPathBuffer, AbstractMotionPathComputer,
 MotionPathController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractMotionPath} */
export { AbstractMotionPath }
/** @lazy @api {xyz.swapee.wc.MotionPathPort} */
export { MotionPathPort }
/** @lazy @api {xyz.swapee.wc.AbstractMotionPathController} */
export { AbstractMotionPathController }
/** @lazy @api {xyz.swapee.wc.MotionPathHtmlComponent} */
export { MotionPathHtmlComponent }
/** @lazy @api {xyz.swapee.wc.MotionPathBuffer} */
export { MotionPathBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractMotionPathComputer} */
export { AbstractMotionPathComputer }
/** @lazy @api {xyz.swapee.wc.back.MotionPathController} */
export { MotionPathController }