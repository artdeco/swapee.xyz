import { MotionPathDisplay, MotionPathScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.MotionPathDisplay} */
export { MotionPathDisplay }
/** @lazy @api {xyz.swapee.wc.MotionPathScreen} */
export { MotionPathScreen }