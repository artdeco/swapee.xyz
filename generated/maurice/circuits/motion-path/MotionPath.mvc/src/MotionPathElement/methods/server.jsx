/** @type {xyz.swapee.wc.IMotionPathElement._server} */
export default function server({gap:gap,count}) {
 // let d=0
 const alphaStep=1/count

 return (<div $id="MotionPath">
  <div $id="ParticlesWr">
   {Array.from({length:count}).map((a,i)=>{
    return (<div Particle animation-delay={(gap*i).toFixed(3).replace(/0+$/,'')+'s'} opacity={(1-alphaStep*i).toFixed(1)}></div>)
   })}
  </div>
  <div $id="ParticlesWr2">
   {Array.from({length:count}).map((a,i)=>{
    return (<div Particle Particle2 animation-delay={(gap*i-(23/2)).toFixed(3).replace(/0+$/,'')+'s'} opacity={(1-alphaStep*i).toFixed(1)}></div>)
   })}
  </div>
 </div>)
}