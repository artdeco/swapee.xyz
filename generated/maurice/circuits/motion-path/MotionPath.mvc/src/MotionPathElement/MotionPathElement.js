import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import MotionPathServerController from '../MotionPathServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractMotionPathElement from '../../gen/AbstractMotionPathElement'

/** @extends {xyz.swapee.wc.MotionPathElement} */
export default class MotionPathElement extends AbstractMotionPathElement.implements(
 MotionPathServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IMotionPathElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
   classesMap: true,
   rootSelector:     `.MotionPath`,
   stylesheet:       'html/styles/MotionPath.css',
   blockName:        'html/MotionPathBlock.html',
  }),
){}

// thank you for using web circuits
