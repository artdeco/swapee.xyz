import deduceInputs from './methods/deduce-inputs'
import AbstractMotionPathControllerAT from '../../gen/AbstractMotionPathControllerAT'
import MotionPathDisplay from '../MotionPathDisplay'
import AbstractMotionPathScreen from '../../gen/AbstractMotionPathScreen'

/** @extends {xyz.swapee.wc.MotionPathScreen} */
export default class extends AbstractMotionPathScreen.implements(
 AbstractMotionPathControllerAT,
 MotionPathDisplay,
 /**@type {!xyz.swapee.wc.IMotionPathScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IMotionPathScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:3209980627,
 }),
){}