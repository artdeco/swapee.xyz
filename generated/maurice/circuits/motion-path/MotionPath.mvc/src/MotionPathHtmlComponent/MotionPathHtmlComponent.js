import MotionPathHtmlController from '../MotionPathHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractMotionPathHtmlComponent} from '../../gen/AbstractMotionPathHtmlComponent'

/** @extends {xyz.swapee.wc.MotionPathHtmlComponent} */
export default class extends AbstractMotionPathHtmlComponent.implements(
 MotionPathHtmlController,
 IntegratedComponentInitialiser,
){}