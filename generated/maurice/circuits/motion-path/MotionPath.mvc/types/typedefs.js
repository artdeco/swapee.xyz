/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IMotionPathComputer={}
xyz.swapee.wc.IMotionPathOuterCore={}
xyz.swapee.wc.IMotionPathOuterCore.Model={}
xyz.swapee.wc.IMotionPathOuterCore.Model.Gap={}
xyz.swapee.wc.IMotionPathOuterCore.Model.Count={}
xyz.swapee.wc.IMotionPathOuterCore.WeakModel={}
xyz.swapee.wc.IMotionPathPort={}
xyz.swapee.wc.IMotionPathPort.Inputs={}
xyz.swapee.wc.IMotionPathPort.WeakInputs={}
xyz.swapee.wc.IMotionPathCore={}
xyz.swapee.wc.IMotionPathCore.Model={}
xyz.swapee.wc.IMotionPathPortInterface={}
xyz.swapee.wc.IMotionPathProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IMotionPathController={}
xyz.swapee.wc.front.IMotionPathControllerAT={}
xyz.swapee.wc.front.IMotionPathScreenAR={}
xyz.swapee.wc.IMotionPath={}
xyz.swapee.wc.IMotionPathHtmlComponent={}
xyz.swapee.wc.IMotionPathElement={}
xyz.swapee.wc.IMotionPathElementPort={}
xyz.swapee.wc.IMotionPathElementPort.Inputs={}
xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts={}
xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts={}
xyz.swapee.wc.IMotionPathElementPort.WeakInputs={}
xyz.swapee.wc.IMotionPathDesigner={}
xyz.swapee.wc.IMotionPathDesigner.communicator={}
xyz.swapee.wc.IMotionPathDesigner.relay={}
xyz.swapee.wc.IMotionPathDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IMotionPathDisplay={}
xyz.swapee.wc.back.IMotionPathController={}
xyz.swapee.wc.back.IMotionPathControllerAR={}
xyz.swapee.wc.back.IMotionPathScreen={}
xyz.swapee.wc.back.IMotionPathScreenAT={}
xyz.swapee.wc.IMotionPathController={}
xyz.swapee.wc.IMotionPathScreen={}
xyz.swapee.wc.IMotionPathGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/02-IMotionPathComputer.xml}  fdb876e96c3a6eea1cf69fca1a53c2da */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IMotionPathComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathComputer)} xyz.swapee.wc.AbstractMotionPathComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathComputer} xyz.swapee.wc.MotionPathComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathComputer` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathComputer
 */
xyz.swapee.wc.AbstractMotionPathComputer = class extends /** @type {xyz.swapee.wc.AbstractMotionPathComputer.constructor&xyz.swapee.wc.MotionPathComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathComputer.prototype.constructor = xyz.swapee.wc.AbstractMotionPathComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathComputer.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathComputer}
 */
xyz.swapee.wc.AbstractMotionPathComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathComputer}
 */
xyz.swapee.wc.AbstractMotionPathComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathComputer}
 */
xyz.swapee.wc.AbstractMotionPathComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathComputer}
 */
xyz.swapee.wc.AbstractMotionPathComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathComputer.Initialese[]) => xyz.swapee.wc.IMotionPathComputer} xyz.swapee.wc.MotionPathComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IMotionPathComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.MotionPathMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IMotionPathComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IMotionPathComputer
 */
xyz.swapee.wc.IMotionPathComputer = class extends /** @type {xyz.swapee.wc.IMotionPathComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IMotionPathComputer.compute} */
xyz.swapee.wc.IMotionPathComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathComputer.Initialese>)} xyz.swapee.wc.MotionPathComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathComputer} xyz.swapee.wc.IMotionPathComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IMotionPathComputer_ instances.
 * @constructor xyz.swapee.wc.MotionPathComputer
 * @implements {xyz.swapee.wc.IMotionPathComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathComputer.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathComputer = class extends /** @type {xyz.swapee.wc.MotionPathComputer.constructor&xyz.swapee.wc.IMotionPathComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathComputer}
 */
xyz.swapee.wc.MotionPathComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IMotionPathComputer} */
xyz.swapee.wc.RecordIMotionPathComputer

/** @typedef {xyz.swapee.wc.IMotionPathComputer} xyz.swapee.wc.BoundIMotionPathComputer */

/** @typedef {xyz.swapee.wc.MotionPathComputer} xyz.swapee.wc.BoundMotionPathComputer */

/**
 * Contains getters to cast the _IMotionPathComputer_ interface.
 * @interface xyz.swapee.wc.IMotionPathComputerCaster
 */
xyz.swapee.wc.IMotionPathComputerCaster = class { }
/**
 * Cast the _IMotionPathComputer_ instance into the _BoundIMotionPathComputer_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathComputer}
 */
xyz.swapee.wc.IMotionPathComputerCaster.prototype.asIMotionPathComputer
/**
 * Access the _MotionPathComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathComputer}
 */
xyz.swapee.wc.IMotionPathComputerCaster.prototype.superMotionPathComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.MotionPathMemory) => void} xyz.swapee.wc.IMotionPathComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathComputer.__compute<!xyz.swapee.wc.IMotionPathComputer>} xyz.swapee.wc.IMotionPathComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IMotionPathComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.MotionPathMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/03-IMotionPathOuterCore.xml}  b2bbabc856f0f02d8c9532d069450da0 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IMotionPathOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathOuterCore)} xyz.swapee.wc.AbstractMotionPathOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathOuterCore} xyz.swapee.wc.MotionPathOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathOuterCore
 */
xyz.swapee.wc.AbstractMotionPathOuterCore = class extends /** @type {xyz.swapee.wc.AbstractMotionPathOuterCore.constructor&xyz.swapee.wc.MotionPathOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathOuterCore.prototype.constructor = xyz.swapee.wc.AbstractMotionPathOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathOuterCore}
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathOuterCore}
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathOuterCore}
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathOuterCore}
 */
xyz.swapee.wc.AbstractMotionPathOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathOuterCoreCaster)} xyz.swapee.wc.IMotionPathOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IMotionPath_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IMotionPathOuterCore
 */
xyz.swapee.wc.IMotionPathOuterCore = class extends /** @type {xyz.swapee.wc.IMotionPathOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathOuterCore.prototype.constructor = xyz.swapee.wc.IMotionPathOuterCore

/** @typedef {function(new: xyz.swapee.wc.IMotionPathOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathOuterCore.Initialese>)} xyz.swapee.wc.MotionPathOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathOuterCore} xyz.swapee.wc.IMotionPathOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IMotionPathOuterCore_ instances.
 * @constructor xyz.swapee.wc.MotionPathOuterCore
 * @implements {xyz.swapee.wc.IMotionPathOuterCore} The _IMotionPath_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathOuterCore = class extends /** @type {xyz.swapee.wc.MotionPathOuterCore.constructor&xyz.swapee.wc.IMotionPathOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.MotionPathOuterCore.prototype.constructor = xyz.swapee.wc.MotionPathOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathOuterCore}
 */
xyz.swapee.wc.MotionPathOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathOuterCore.
 * @interface xyz.swapee.wc.IMotionPathOuterCoreFields
 */
xyz.swapee.wc.IMotionPathOuterCoreFields = class { }
/**
 * The _IMotionPath_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IMotionPathOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IMotionPathOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore} */
xyz.swapee.wc.RecordIMotionPathOuterCore

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore} xyz.swapee.wc.BoundIMotionPathOuterCore */

/** @typedef {xyz.swapee.wc.MotionPathOuterCore} xyz.swapee.wc.BoundMotionPathOuterCore */

/**
 * The gap in terms of delay between moving particles.
 * @typedef {number}
 */
xyz.swapee.wc.IMotionPathOuterCore.Model.Gap.gap

/**
 * How manu particles to add.
 * @typedef {number}
 */
xyz.swapee.wc.IMotionPathOuterCore.Model.Count.count

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model.Gap&xyz.swapee.wc.IMotionPathOuterCore.Model.Count} xyz.swapee.wc.IMotionPathOuterCore.Model The _IMotionPath_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap&xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count} xyz.swapee.wc.IMotionPathOuterCore.WeakModel The _IMotionPath_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IMotionPathOuterCore_ interface.
 * @interface xyz.swapee.wc.IMotionPathOuterCoreCaster
 */
xyz.swapee.wc.IMotionPathOuterCoreCaster = class { }
/**
 * Cast the _IMotionPathOuterCore_ instance into the _BoundIMotionPathOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathOuterCore}
 */
xyz.swapee.wc.IMotionPathOuterCoreCaster.prototype.asIMotionPathOuterCore
/**
 * Access the _MotionPathOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathOuterCore}
 */
xyz.swapee.wc.IMotionPathOuterCoreCaster.prototype.superMotionPathOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.Model.Gap The gap in terms of delay between moving particles (optional overlay).
 * @prop {number} [gap=0.035] The gap in terms of delay between moving particles. Default `0.035`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.Model.Gap_Safe The gap in terms of delay between moving particles (required overlay).
 * @prop {number} gap The gap in terms of delay between moving particles.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.Model.Count How manu particles to add (optional overlay).
 * @prop {number} [count=12] How manu particles to add. Default `12`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.Model.Count_Safe How manu particles to add (required overlay).
 * @prop {number} count How manu particles to add.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap The gap in terms of delay between moving particles (optional overlay).
 * @prop {*} [gap="0.035"] The gap in terms of delay between moving particles. Default `0.035`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap_Safe The gap in terms of delay between moving particles (required overlay).
 * @prop {*} gap The gap in terms of delay between moving particles.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count How manu particles to add (optional overlay).
 * @prop {*} [count=12] How manu particles to add. Default `12`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count_Safe How manu particles to add (required overlay).
 * @prop {*} count How manu particles to add.
 */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap} xyz.swapee.wc.IMotionPathPort.Inputs.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap_Safe} xyz.swapee.wc.IMotionPathPort.Inputs.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count} xyz.swapee.wc.IMotionPathPort.Inputs.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count_Safe} xyz.swapee.wc.IMotionPathPort.Inputs.Count_Safe How manu particles to add (required overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap} xyz.swapee.wc.IMotionPathPort.WeakInputs.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Gap_Safe} xyz.swapee.wc.IMotionPathPort.WeakInputs.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count} xyz.swapee.wc.IMotionPathPort.WeakInputs.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.WeakModel.Count_Safe} xyz.swapee.wc.IMotionPathPort.WeakInputs.Count_Safe How manu particles to add (required overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model.Gap} xyz.swapee.wc.IMotionPathCore.Model.Gap The gap in terms of delay between moving particles (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model.Gap_Safe} xyz.swapee.wc.IMotionPathCore.Model.Gap_Safe The gap in terms of delay between moving particles (required overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model.Count} xyz.swapee.wc.IMotionPathCore.Model.Count How manu particles to add (optional overlay). */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model.Count_Safe} xyz.swapee.wc.IMotionPathCore.Model.Count_Safe How manu particles to add (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/04-IMotionPathPort.xml}  f253a09a885a356f678cf3ddb1bb224b */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IMotionPathPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathPort)} xyz.swapee.wc.AbstractMotionPathPort.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathPort} xyz.swapee.wc.MotionPathPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathPort` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathPort
 */
xyz.swapee.wc.AbstractMotionPathPort = class extends /** @type {xyz.swapee.wc.AbstractMotionPathPort.constructor&xyz.swapee.wc.MotionPathPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathPort.prototype.constructor = xyz.swapee.wc.AbstractMotionPathPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathPort.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathPort|typeof xyz.swapee.wc.MotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathPort}
 */
xyz.swapee.wc.AbstractMotionPathPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathPort}
 */
xyz.swapee.wc.AbstractMotionPathPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathPort|typeof xyz.swapee.wc.MotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathPort}
 */
xyz.swapee.wc.AbstractMotionPathPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathPort|typeof xyz.swapee.wc.MotionPathPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathPort}
 */
xyz.swapee.wc.AbstractMotionPathPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathPort.Initialese[]) => xyz.swapee.wc.IMotionPathPort} xyz.swapee.wc.MotionPathPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathPortFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IMotionPathPort.Inputs>)} xyz.swapee.wc.IMotionPathPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IMotionPath_, providing input
 * pins.
 * @interface xyz.swapee.wc.IMotionPathPort
 */
xyz.swapee.wc.IMotionPathPort = class extends /** @type {xyz.swapee.wc.IMotionPathPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IMotionPathPort.resetPort} */
xyz.swapee.wc.IMotionPathPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IMotionPathPort.resetMotionPathPort} */
xyz.swapee.wc.IMotionPathPort.prototype.resetMotionPathPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathPort&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathPort.Initialese>)} xyz.swapee.wc.MotionPathPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathPort} xyz.swapee.wc.IMotionPathPort.typeof */
/**
 * A concrete class of _IMotionPathPort_ instances.
 * @constructor xyz.swapee.wc.MotionPathPort
 * @implements {xyz.swapee.wc.IMotionPathPort} The port that serves as an interface to the _IMotionPath_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathPort.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathPort = class extends /** @type {xyz.swapee.wc.MotionPathPort.constructor&xyz.swapee.wc.IMotionPathPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathPort}
 */
xyz.swapee.wc.MotionPathPort.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathPort.
 * @interface xyz.swapee.wc.IMotionPathPortFields
 */
xyz.swapee.wc.IMotionPathPortFields = class { }
/**
 * The inputs to the _IMotionPath_'s controller via its port.
 */
xyz.swapee.wc.IMotionPathPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IMotionPathPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IMotionPathPortFields.prototype.props = /** @type {!xyz.swapee.wc.IMotionPathPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathPort} */
xyz.swapee.wc.RecordIMotionPathPort

/** @typedef {xyz.swapee.wc.IMotionPathPort} xyz.swapee.wc.BoundIMotionPathPort */

/** @typedef {xyz.swapee.wc.MotionPathPort} xyz.swapee.wc.BoundMotionPathPort */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathOuterCore.WeakModel)} xyz.swapee.wc.IMotionPathPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathOuterCore.WeakModel} xyz.swapee.wc.IMotionPathOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IMotionPath_'s controller via its port.
 * @record xyz.swapee.wc.IMotionPathPort.Inputs
 */
xyz.swapee.wc.IMotionPathPort.Inputs = class extends /** @type {xyz.swapee.wc.IMotionPathPort.Inputs.constructor&xyz.swapee.wc.IMotionPathOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathPort.Inputs.prototype.constructor = xyz.swapee.wc.IMotionPathPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IMotionPathOuterCore.WeakModel)} xyz.swapee.wc.IMotionPathPort.WeakInputs.constructor */
/**
 * The inputs to the _IMotionPath_'s controller via its port.
 * @record xyz.swapee.wc.IMotionPathPort.WeakInputs
 */
xyz.swapee.wc.IMotionPathPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IMotionPathPort.WeakInputs.constructor&xyz.swapee.wc.IMotionPathOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IMotionPathPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IMotionPathPortInterface
 */
xyz.swapee.wc.IMotionPathPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IMotionPathPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IMotionPathPortInterface.prototype.constructor = xyz.swapee.wc.IMotionPathPortInterface

/**
 * A concrete class of _IMotionPathPortInterface_ instances.
 * @constructor xyz.swapee.wc.MotionPathPortInterface
 * @implements {xyz.swapee.wc.IMotionPathPortInterface} The port interface.
 */
xyz.swapee.wc.MotionPathPortInterface = class extends xyz.swapee.wc.IMotionPathPortInterface { }
xyz.swapee.wc.MotionPathPortInterface.prototype.constructor = xyz.swapee.wc.MotionPathPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathPortInterface.Props
 * @prop {number} [gap=0.035] The gap in terms of delay between moving particles. Default `0.035`.
 * @prop {number} [count=12] How manu particles to add. Default `12`.
 */

/**
 * Contains getters to cast the _IMotionPathPort_ interface.
 * @interface xyz.swapee.wc.IMotionPathPortCaster
 */
xyz.swapee.wc.IMotionPathPortCaster = class { }
/**
 * Cast the _IMotionPathPort_ instance into the _BoundIMotionPathPort_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathPort}
 */
xyz.swapee.wc.IMotionPathPortCaster.prototype.asIMotionPathPort
/**
 * Access the _MotionPathPort_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathPort}
 */
xyz.swapee.wc.IMotionPathPortCaster.prototype.superMotionPathPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IMotionPathPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathPort.__resetPort<!xyz.swapee.wc.IMotionPathPort>} xyz.swapee.wc.IMotionPathPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IMotionPathPort.resetPort} */
/**
 * Resets the _IMotionPath_ port.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IMotionPathPort.__resetMotionPathPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathPort.__resetMotionPathPort<!xyz.swapee.wc.IMotionPathPort>} xyz.swapee.wc.IMotionPathPort._resetMotionPathPort */
/** @typedef {typeof xyz.swapee.wc.IMotionPathPort.resetMotionPathPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathPort.resetMotionPathPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/09-IMotionPathCore.xml}  4a28dce372225d64846621dc459757b4 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IMotionPathCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathCore)} xyz.swapee.wc.AbstractMotionPathCore.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathCore} xyz.swapee.wc.MotionPathCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathCore` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathCore
 */
xyz.swapee.wc.AbstractMotionPathCore = class extends /** @type {xyz.swapee.wc.AbstractMotionPathCore.constructor&xyz.swapee.wc.MotionPathCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathCore.prototype.constructor = xyz.swapee.wc.AbstractMotionPathCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathCore.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathCore}
 */
xyz.swapee.wc.AbstractMotionPathCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathCore}
 */
xyz.swapee.wc.AbstractMotionPathCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathCore}
 */
xyz.swapee.wc.AbstractMotionPathCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathOuterCore|typeof xyz.swapee.wc.MotionPathOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathCore}
 */
xyz.swapee.wc.AbstractMotionPathCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathCoreCaster&xyz.swapee.wc.IMotionPathOuterCore)} xyz.swapee.wc.IMotionPathCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IMotionPathCore
 */
xyz.swapee.wc.IMotionPathCore = class extends /** @type {xyz.swapee.wc.IMotionPathCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IMotionPathOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IMotionPathCore.resetCore} */
xyz.swapee.wc.IMotionPathCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IMotionPathCore.resetMotionPathCore} */
xyz.swapee.wc.IMotionPathCore.prototype.resetMotionPathCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathCore&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathCore.Initialese>)} xyz.swapee.wc.MotionPathCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathCore} xyz.swapee.wc.IMotionPathCore.typeof */
/**
 * A concrete class of _IMotionPathCore_ instances.
 * @constructor xyz.swapee.wc.MotionPathCore
 * @implements {xyz.swapee.wc.IMotionPathCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathCore.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathCore = class extends /** @type {xyz.swapee.wc.MotionPathCore.constructor&xyz.swapee.wc.IMotionPathCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.MotionPathCore.prototype.constructor = xyz.swapee.wc.MotionPathCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathCore}
 */
xyz.swapee.wc.MotionPathCore.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathCore.
 * @interface xyz.swapee.wc.IMotionPathCoreFields
 */
xyz.swapee.wc.IMotionPathCoreFields = class { }
/**
 * The _IMotionPath_'s memory.
 */
xyz.swapee.wc.IMotionPathCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IMotionPathCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IMotionPathCoreFields.prototype.props = /** @type {xyz.swapee.wc.IMotionPathCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathCore} */
xyz.swapee.wc.RecordIMotionPathCore

/** @typedef {xyz.swapee.wc.IMotionPathCore} xyz.swapee.wc.BoundIMotionPathCore */

/** @typedef {xyz.swapee.wc.MotionPathCore} xyz.swapee.wc.BoundMotionPathCore */

/** @typedef {xyz.swapee.wc.IMotionPathOuterCore.Model} xyz.swapee.wc.IMotionPathCore.Model The _IMotionPath_'s memory. */

/**
 * Contains getters to cast the _IMotionPathCore_ interface.
 * @interface xyz.swapee.wc.IMotionPathCoreCaster
 */
xyz.swapee.wc.IMotionPathCoreCaster = class { }
/**
 * Cast the _IMotionPathCore_ instance into the _BoundIMotionPathCore_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathCore}
 */
xyz.swapee.wc.IMotionPathCoreCaster.prototype.asIMotionPathCore
/**
 * Access the _MotionPathCore_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathCore}
 */
xyz.swapee.wc.IMotionPathCoreCaster.prototype.superMotionPathCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IMotionPathCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathCore.__resetCore<!xyz.swapee.wc.IMotionPathCore>} xyz.swapee.wc.IMotionPathCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IMotionPathCore.resetCore} */
/**
 * Resets the _IMotionPath_ core.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IMotionPathCore.__resetMotionPathCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathCore.__resetMotionPathCore<!xyz.swapee.wc.IMotionPathCore>} xyz.swapee.wc.IMotionPathCore._resetMotionPathCore */
/** @typedef {typeof xyz.swapee.wc.IMotionPathCore.resetMotionPathCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathCore.resetMotionPathCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/10-IMotionPathProcessor.xml}  e8f407578963d67cb635b1c23b2a03b2 */
/** @typedef {xyz.swapee.wc.IMotionPathComputer.Initialese&xyz.swapee.wc.IMotionPathController.Initialese} xyz.swapee.wc.IMotionPathProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathProcessor)} xyz.swapee.wc.AbstractMotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathProcessor} xyz.swapee.wc.MotionPathProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathProcessor
 */
xyz.swapee.wc.AbstractMotionPathProcessor = class extends /** @type {xyz.swapee.wc.AbstractMotionPathProcessor.constructor&xyz.swapee.wc.MotionPathProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathProcessor.prototype.constructor = xyz.swapee.wc.AbstractMotionPathProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathProcessor}
 */
xyz.swapee.wc.AbstractMotionPathProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathProcessor}
 */
xyz.swapee.wc.AbstractMotionPathProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathProcessor}
 */
xyz.swapee.wc.AbstractMotionPathProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathCore|typeof xyz.swapee.wc.MotionPathCore)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathProcessor}
 */
xyz.swapee.wc.AbstractMotionPathProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathProcessor.Initialese[]) => xyz.swapee.wc.IMotionPathProcessor} xyz.swapee.wc.MotionPathProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IMotionPathProcessorCaster&xyz.swapee.wc.IMotionPathComputer&xyz.swapee.wc.IMotionPathCore&xyz.swapee.wc.IMotionPathController)} xyz.swapee.wc.IMotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathController} xyz.swapee.wc.IMotionPathController.typeof */
/**
 * The processor to compute changes to the memory for the _IMotionPath_.
 * @interface xyz.swapee.wc.IMotionPathProcessor
 */
xyz.swapee.wc.IMotionPathProcessor = class extends /** @type {xyz.swapee.wc.IMotionPathProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IMotionPathComputer.typeof&xyz.swapee.wc.IMotionPathCore.typeof&xyz.swapee.wc.IMotionPathController.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathProcessor.Initialese>)} xyz.swapee.wc.MotionPathProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathProcessor} xyz.swapee.wc.IMotionPathProcessor.typeof */
/**
 * A concrete class of _IMotionPathProcessor_ instances.
 * @constructor xyz.swapee.wc.MotionPathProcessor
 * @implements {xyz.swapee.wc.IMotionPathProcessor} The processor to compute changes to the memory for the _IMotionPath_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathProcessor.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathProcessor = class extends /** @type {xyz.swapee.wc.MotionPathProcessor.constructor&xyz.swapee.wc.IMotionPathProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathProcessor}
 */
xyz.swapee.wc.MotionPathProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IMotionPathProcessor} */
xyz.swapee.wc.RecordIMotionPathProcessor

/** @typedef {xyz.swapee.wc.IMotionPathProcessor} xyz.swapee.wc.BoundIMotionPathProcessor */

/** @typedef {xyz.swapee.wc.MotionPathProcessor} xyz.swapee.wc.BoundMotionPathProcessor */

/**
 * Contains getters to cast the _IMotionPathProcessor_ interface.
 * @interface xyz.swapee.wc.IMotionPathProcessorCaster
 */
xyz.swapee.wc.IMotionPathProcessorCaster = class { }
/**
 * Cast the _IMotionPathProcessor_ instance into the _BoundIMotionPathProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathProcessor}
 */
xyz.swapee.wc.IMotionPathProcessorCaster.prototype.asIMotionPathProcessor
/**
 * Access the _MotionPathProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathProcessor}
 */
xyz.swapee.wc.IMotionPathProcessorCaster.prototype.superMotionPathProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/100-MotionPathMemory.xml}  4792431538af49e0264930129d9f43ba */
/**
 * The memory of the _IMotionPath_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.MotionPathMemory
 */
xyz.swapee.wc.MotionPathMemory = class { }
/**
 * The gap in terms of delay between moving particles. Default `0`.
 */
xyz.swapee.wc.MotionPathMemory.prototype.gap = /** @type {number} */ (void 0)
/**
 * How manu particles to add. Default `0`.
 */
xyz.swapee.wc.MotionPathMemory.prototype.count = /** @type {number} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/102-MotionPathInputs.xml}  9dc3e7b8c7937711e01777571c62b184 */
/**
 * The inputs of the _IMotionPath_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.MotionPathInputs
 */
xyz.swapee.wc.front.MotionPathInputs = class { }
/**
 * The gap in terms of delay between moving particles. Default `0.035`.
 */
xyz.swapee.wc.front.MotionPathInputs.prototype.gap = /** @type {number|undefined} */ (void 0)
/**
 * How manu particles to add. Default `12`.
 */
xyz.swapee.wc.front.MotionPathInputs.prototype.count = /** @type {number|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/11-IMotionPath.xml}  96c065a5dbb89ada84ee32a7af640624 */
/**
 * An atomic wrapper for the _IMotionPath_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.MotionPathEnv
 */
xyz.swapee.wc.MotionPathEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.MotionPathEnv.prototype.motionPath = /** @type {xyz.swapee.wc.IMotionPath} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathController.Inputs>&xyz.swapee.wc.IMotionPathProcessor.Initialese&xyz.swapee.wc.IMotionPathComputer.Initialese&xyz.swapee.wc.IMotionPathController.Initialese} xyz.swapee.wc.IMotionPath.Initialese */

/** @typedef {function(new: xyz.swapee.wc.MotionPath)} xyz.swapee.wc.AbstractMotionPath.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPath} xyz.swapee.wc.MotionPath.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPath` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPath
 */
xyz.swapee.wc.AbstractMotionPath = class extends /** @type {xyz.swapee.wc.AbstractMotionPath.constructor&xyz.swapee.wc.MotionPath.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPath.prototype.constructor = xyz.swapee.wc.AbstractMotionPath
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPath.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPath} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPath}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPath.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPath}
 */
xyz.swapee.wc.AbstractMotionPath.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPath}
 */
xyz.swapee.wc.AbstractMotionPath.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPath}
 */
xyz.swapee.wc.AbstractMotionPath.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPath}
 */
xyz.swapee.wc.AbstractMotionPath.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPath.Initialese[]) => xyz.swapee.wc.IMotionPath} xyz.swapee.wc.MotionPathConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPath.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IMotionPath.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IMotionPath.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IMotionPath.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.MotionPathMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.MotionPathClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathCaster&xyz.swapee.wc.IMotionPathProcessor&xyz.swapee.wc.IMotionPathComputer&xyz.swapee.wc.IMotionPathController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathController.Inputs, null>)} xyz.swapee.wc.IMotionPath.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IMotionPath
 */
xyz.swapee.wc.IMotionPath = class extends /** @type {xyz.swapee.wc.IMotionPath.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IMotionPathProcessor.typeof&xyz.swapee.wc.IMotionPathComputer.typeof&xyz.swapee.wc.IMotionPathController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPath* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPath.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPath.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPath&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPath.Initialese>)} xyz.swapee.wc.MotionPath.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPath} xyz.swapee.wc.IMotionPath.typeof */
/**
 * A concrete class of _IMotionPath_ instances.
 * @constructor xyz.swapee.wc.MotionPath
 * @implements {xyz.swapee.wc.IMotionPath} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPath.Initialese>} ‎
 */
xyz.swapee.wc.MotionPath = class extends /** @type {xyz.swapee.wc.MotionPath.constructor&xyz.swapee.wc.IMotionPath.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPath* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPath.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPath* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPath.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPath.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPath}
 */
xyz.swapee.wc.MotionPath.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPath.
 * @interface xyz.swapee.wc.IMotionPathFields
 */
xyz.swapee.wc.IMotionPathFields = class { }
/**
 * The input pins of the _IMotionPath_ port.
 */
xyz.swapee.wc.IMotionPathFields.prototype.pinout = /** @type {!xyz.swapee.wc.IMotionPath.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPath} */
xyz.swapee.wc.RecordIMotionPath

/** @typedef {xyz.swapee.wc.IMotionPath} xyz.swapee.wc.BoundIMotionPath */

/** @typedef {xyz.swapee.wc.MotionPath} xyz.swapee.wc.BoundMotionPath */

/** @typedef {xyz.swapee.wc.IMotionPathController.Inputs} xyz.swapee.wc.IMotionPath.Pinout The input pins of the _IMotionPath_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IMotionPathController.Inputs>)} xyz.swapee.wc.IMotionPathBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IMotionPathBuffer
 */
xyz.swapee.wc.IMotionPathBuffer = class extends /** @type {xyz.swapee.wc.IMotionPathBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathBuffer.prototype.constructor = xyz.swapee.wc.IMotionPathBuffer

/**
 * A concrete class of _IMotionPathBuffer_ instances.
 * @constructor xyz.swapee.wc.MotionPathBuffer
 * @implements {xyz.swapee.wc.IMotionPathBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.MotionPathBuffer = class extends xyz.swapee.wc.IMotionPathBuffer { }
xyz.swapee.wc.MotionPathBuffer.prototype.constructor = xyz.swapee.wc.MotionPathBuffer

/**
 * Contains getters to cast the _IMotionPath_ interface.
 * @interface xyz.swapee.wc.IMotionPathCaster
 */
xyz.swapee.wc.IMotionPathCaster = class { }
/**
 * Cast the _IMotionPath_ instance into the _BoundIMotionPath_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPath}
 */
xyz.swapee.wc.IMotionPathCaster.prototype.asIMotionPath
/**
 * Access the _MotionPath_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPath}
 */
xyz.swapee.wc.IMotionPathCaster.prototype.superMotionPath

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/110-MotionPathSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.MotionPathMemoryPQs
 */
xyz.swapee.wc.MotionPathMemoryPQs = class {
  constructor() {
    /**
     * `df9bc`
     */
    this.gap=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.MotionPathMemoryPQs.prototype.constructor = xyz.swapee.wc.MotionPathMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.MotionPathMemoryQPs
 * @dict
 */
xyz.swapee.wc.MotionPathMemoryQPs = class { }
/**
 * `gap`
 */
xyz.swapee.wc.MotionPathMemoryQPs.prototype.df9bc = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.MotionPathMemoryPQs)} xyz.swapee.wc.MotionPathInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathMemoryPQs} xyz.swapee.wc.MotionPathMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.MotionPathInputsPQs
 */
xyz.swapee.wc.MotionPathInputsPQs = class extends /** @type {xyz.swapee.wc.MotionPathInputsPQs.constructor&xyz.swapee.wc.MotionPathMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.MotionPathInputsPQs.prototype.constructor = xyz.swapee.wc.MotionPathInputsPQs

/** @typedef {function(new: xyz.swapee.wc.MotionPathMemoryPQs)} xyz.swapee.wc.MotionPathInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.MotionPathInputsQPs
 * @dict
 */
xyz.swapee.wc.MotionPathInputsQPs = class extends /** @type {xyz.swapee.wc.MotionPathInputsQPs.constructor&xyz.swapee.wc.MotionPathMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.MotionPathInputsQPs.prototype.constructor = xyz.swapee.wc.MotionPathInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.MotionPathVdusPQs
 */
xyz.swapee.wc.MotionPathVdusPQs = class {
  constructor() {
    /**
     * `e2311`
     */
    this.ParticlesWr=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.MotionPathVdusPQs.prototype.constructor = xyz.swapee.wc.MotionPathVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.MotionPathVdusQPs
 * @dict
 */
xyz.swapee.wc.MotionPathVdusQPs = class { }
/**
 * `ParticlesWr`
 */
xyz.swapee.wc.MotionPathVdusQPs.prototype.e2311 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/12-IMotionPathHtmlComponent.xml}  030d7971473e0a8a5460ede2bafde11a */
/** @typedef {xyz.swapee.wc.back.IMotionPathController.Initialese&xyz.swapee.wc.back.IMotionPathScreen.Initialese&xyz.swapee.wc.IMotionPath.Initialese&xyz.swapee.wc.IMotionPathGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IMotionPathProcessor.Initialese&xyz.swapee.wc.IMotionPathComputer.Initialese} xyz.swapee.wc.IMotionPathHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathHtmlComponent)} xyz.swapee.wc.AbstractMotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathHtmlComponent} xyz.swapee.wc.MotionPathHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathHtmlComponent
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractMotionPathHtmlComponent.constructor&xyz.swapee.wc.MotionPathHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractMotionPathHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathHtmlComponent|typeof xyz.swapee.wc.MotionPathHtmlComponent)|(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathHtmlComponent|typeof xyz.swapee.wc.MotionPathHtmlComponent)|(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathHtmlComponent|typeof xyz.swapee.wc.MotionPathHtmlComponent)|(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.IMotionPath|typeof xyz.swapee.wc.MotionPath)|(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IMotionPathProcessor|typeof xyz.swapee.wc.MotionPathProcessor)|(!xyz.swapee.wc.IMotionPathComputer|typeof xyz.swapee.wc.MotionPathComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathHtmlComponent}
 */
xyz.swapee.wc.AbstractMotionPathHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathHtmlComponent.Initialese[]) => xyz.swapee.wc.IMotionPathHtmlComponent} xyz.swapee.wc.MotionPathHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IMotionPathHtmlComponentCaster&xyz.swapee.wc.back.IMotionPathController&xyz.swapee.wc.back.IMotionPathScreen&xyz.swapee.wc.IMotionPath&xyz.swapee.wc.IMotionPathGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IMotionPathProcessor&xyz.swapee.wc.IMotionPathComputer)} xyz.swapee.wc.IMotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathController} xyz.swapee.wc.back.IMotionPathController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathScreen} xyz.swapee.wc.back.IMotionPathScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IMotionPathGPU} xyz.swapee.wc.IMotionPathGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IMotionPath_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IMotionPathHtmlComponent
 */
xyz.swapee.wc.IMotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.IMotionPathHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IMotionPathController.typeof&xyz.swapee.wc.back.IMotionPathScreen.typeof&xyz.swapee.wc.IMotionPath.typeof&xyz.swapee.wc.IMotionPathGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IMotionPathProcessor.typeof&xyz.swapee.wc.IMotionPathComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathHtmlComponent.Initialese>)} xyz.swapee.wc.MotionPathHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathHtmlComponent} xyz.swapee.wc.IMotionPathHtmlComponent.typeof */
/**
 * A concrete class of _IMotionPathHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.MotionPathHtmlComponent
 * @implements {xyz.swapee.wc.IMotionPathHtmlComponent} The _IMotionPath_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathHtmlComponent = class extends /** @type {xyz.swapee.wc.MotionPathHtmlComponent.constructor&xyz.swapee.wc.IMotionPathHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathHtmlComponent}
 */
xyz.swapee.wc.MotionPathHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IMotionPathHtmlComponent} */
xyz.swapee.wc.RecordIMotionPathHtmlComponent

/** @typedef {xyz.swapee.wc.IMotionPathHtmlComponent} xyz.swapee.wc.BoundIMotionPathHtmlComponent */

/** @typedef {xyz.swapee.wc.MotionPathHtmlComponent} xyz.swapee.wc.BoundMotionPathHtmlComponent */

/**
 * Contains getters to cast the _IMotionPathHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IMotionPathHtmlComponentCaster
 */
xyz.swapee.wc.IMotionPathHtmlComponentCaster = class { }
/**
 * Cast the _IMotionPathHtmlComponent_ instance into the _BoundIMotionPathHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathHtmlComponent}
 */
xyz.swapee.wc.IMotionPathHtmlComponentCaster.prototype.asIMotionPathHtmlComponent
/**
 * Access the _MotionPathHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathHtmlComponent}
 */
xyz.swapee.wc.IMotionPathHtmlComponentCaster.prototype.superMotionPathHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/130-IMotionPathElement.xml}  2860a25f18315725f625f84586f08106 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IMotionPathElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathElement)} xyz.swapee.wc.AbstractMotionPathElement.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathElement} xyz.swapee.wc.MotionPathElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathElement` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathElement
 */
xyz.swapee.wc.AbstractMotionPathElement = class extends /** @type {xyz.swapee.wc.AbstractMotionPathElement.constructor&xyz.swapee.wc.MotionPathElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathElement.prototype.constructor = xyz.swapee.wc.AbstractMotionPathElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathElement.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathElement|typeof xyz.swapee.wc.MotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathElement}
 */
xyz.swapee.wc.AbstractMotionPathElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElement}
 */
xyz.swapee.wc.AbstractMotionPathElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathElement|typeof xyz.swapee.wc.MotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElement}
 */
xyz.swapee.wc.AbstractMotionPathElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathElement|typeof xyz.swapee.wc.MotionPathElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElement}
 */
xyz.swapee.wc.AbstractMotionPathElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathElement.Initialese[]) => xyz.swapee.wc.IMotionPathElement} xyz.swapee.wc.MotionPathElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElementFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.IMotionPathElement.Inputs, null>)} xyz.swapee.wc.IMotionPathElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IMotionPathElement
 */
xyz.swapee.wc.IMotionPathElement = class extends /** @type {xyz.swapee.wc.IMotionPathElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IMotionPathElement.solder} */
xyz.swapee.wc.IMotionPathElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IMotionPathElement.render} */
xyz.swapee.wc.IMotionPathElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IMotionPathElement.server} */
xyz.swapee.wc.IMotionPathElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IMotionPathElement.inducer} */
xyz.swapee.wc.IMotionPathElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElement&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathElement.Initialese>)} xyz.swapee.wc.MotionPathElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElement} xyz.swapee.wc.IMotionPathElement.typeof */
/**
 * A concrete class of _IMotionPathElement_ instances.
 * @constructor xyz.swapee.wc.MotionPathElement
 * @implements {xyz.swapee.wc.IMotionPathElement} A component description.
 *
 * The _IMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathElement.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathElement = class extends /** @type {xyz.swapee.wc.MotionPathElement.constructor&xyz.swapee.wc.IMotionPathElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathElement}
 */
xyz.swapee.wc.MotionPathElement.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathElement.
 * @interface xyz.swapee.wc.IMotionPathElementFields
 */
xyz.swapee.wc.IMotionPathElementFields = class { }
/**
 * The element-specific inputs to the _IMotionPath_ component.
 */
xyz.swapee.wc.IMotionPathElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IMotionPathElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathElement} */
xyz.swapee.wc.RecordIMotionPathElement

/** @typedef {xyz.swapee.wc.IMotionPathElement} xyz.swapee.wc.BoundIMotionPathElement */

/** @typedef {xyz.swapee.wc.MotionPathElement} xyz.swapee.wc.BoundMotionPathElement */

/** @typedef {xyz.swapee.wc.IMotionPathPort.Inputs&xyz.swapee.wc.IMotionPathDisplay.Queries&xyz.swapee.wc.IMotionPathController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IMotionPathElementPort.Inputs} xyz.swapee.wc.IMotionPathElement.Inputs The element-specific inputs to the _IMotionPath_ component. */

/**
 * Contains getters to cast the _IMotionPathElement_ interface.
 * @interface xyz.swapee.wc.IMotionPathElementCaster
 */
xyz.swapee.wc.IMotionPathElementCaster = class { }
/**
 * Cast the _IMotionPathElement_ instance into the _BoundIMotionPathElement_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathElement}
 */
xyz.swapee.wc.IMotionPathElementCaster.prototype.asIMotionPathElement
/**
 * Access the _MotionPathElement_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathElement}
 */
xyz.swapee.wc.IMotionPathElementCaster.prototype.superMotionPathElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.MotionPathMemory, props: !xyz.swapee.wc.IMotionPathElement.Inputs) => Object<string, *>} xyz.swapee.wc.IMotionPathElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathElement.__solder<!xyz.swapee.wc.IMotionPathElement>} xyz.swapee.wc.IMotionPathElement._solder */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.MotionPathMemory} model The model.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * @param {!xyz.swapee.wc.IMotionPathElement.Inputs} props The element props.
 * - `[gap=null]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IMotionPathOuterCore.WeakModel.Gap* ⤴ *IMotionPathOuterCore.WeakModel.Gap* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IMotionPathElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.MotionPathMemory, instance?: !xyz.swapee.wc.IMotionPathScreen&xyz.swapee.wc.IMotionPathController) => !engineering.type.VNode} xyz.swapee.wc.IMotionPathElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathElement.__render<!xyz.swapee.wc.IMotionPathElement>} xyz.swapee.wc.IMotionPathElement._render */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.MotionPathMemory} [model] The model for the view.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * @param {!xyz.swapee.wc.IMotionPathScreen&xyz.swapee.wc.IMotionPathController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IMotionPathElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.MotionPathMemory, inputs: !xyz.swapee.wc.IMotionPathElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IMotionPathElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathElement.__server<!xyz.swapee.wc.IMotionPathElement>} xyz.swapee.wc.IMotionPathElement._server */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.MotionPathMemory} memory The memory registers.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * @param {!xyz.swapee.wc.IMotionPathElement.Inputs} inputs The inputs to the port.
 * - `[gap=null]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IMotionPathOuterCore.WeakModel.Gap* ⤴ *IMotionPathOuterCore.WeakModel.Gap* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IMotionPathElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.MotionPathMemory, port?: !xyz.swapee.wc.IMotionPathElement.Inputs) => ?} xyz.swapee.wc.IMotionPathElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathElement.__inducer<!xyz.swapee.wc.IMotionPathElement>} xyz.swapee.wc.IMotionPathElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.MotionPathMemory} [model] The model of the component into which to induce the state.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * @param {!xyz.swapee.wc.IMotionPathElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[gap=null]` _&#42;?_ The gap in terms of delay between moving particles. ⤴ *IMotionPathOuterCore.WeakModel.Gap* ⤴ *IMotionPathOuterCore.WeakModel.Gap* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IMotionPathElementPort.Inputs.NoSolder* Default `false`.
 * - `[particlesWrOpts]` _!Object?_ The options to pass to the _ParticlesWr_ vdu. ⤴ *IMotionPathElementPort.Inputs.ParticlesWrOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IMotionPathElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/140-IMotionPathElementPort.xml}  b1978c1835ec65b27e829e99443754ed */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IMotionPathElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathElementPort)} xyz.swapee.wc.AbstractMotionPathElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathElementPort} xyz.swapee.wc.MotionPathElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathElementPort
 */
xyz.swapee.wc.AbstractMotionPathElementPort = class extends /** @type {xyz.swapee.wc.AbstractMotionPathElementPort.constructor&xyz.swapee.wc.MotionPathElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathElementPort.prototype.constructor = xyz.swapee.wc.AbstractMotionPathElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathElementPort|typeof xyz.swapee.wc.MotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathElementPort}
 */
xyz.swapee.wc.AbstractMotionPathElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElementPort}
 */
xyz.swapee.wc.AbstractMotionPathElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathElementPort|typeof xyz.swapee.wc.MotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElementPort}
 */
xyz.swapee.wc.AbstractMotionPathElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathElementPort|typeof xyz.swapee.wc.MotionPathElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathElementPort}
 */
xyz.swapee.wc.AbstractMotionPathElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathElementPort.Initialese[]) => xyz.swapee.wc.IMotionPathElementPort} xyz.swapee.wc.MotionPathElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IMotionPathElementPort.Inputs>)} xyz.swapee.wc.IMotionPathElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IMotionPathElementPort
 */
xyz.swapee.wc.IMotionPathElementPort = class extends /** @type {xyz.swapee.wc.IMotionPathElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathElementPort.Initialese>)} xyz.swapee.wc.MotionPathElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort} xyz.swapee.wc.IMotionPathElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IMotionPathElementPort_ instances.
 * @constructor xyz.swapee.wc.MotionPathElementPort
 * @implements {xyz.swapee.wc.IMotionPathElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathElementPort.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathElementPort = class extends /** @type {xyz.swapee.wc.MotionPathElementPort.constructor&xyz.swapee.wc.IMotionPathElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathElementPort}
 */
xyz.swapee.wc.MotionPathElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathElementPort.
 * @interface xyz.swapee.wc.IMotionPathElementPortFields
 */
xyz.swapee.wc.IMotionPathElementPortFields = class { }
/**
 * The inputs to the _IMotionPathElement_'s controller via its element port.
 */
xyz.swapee.wc.IMotionPathElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IMotionPathElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IMotionPathElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IMotionPathElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathElementPort} */
xyz.swapee.wc.RecordIMotionPathElementPort

/** @typedef {xyz.swapee.wc.IMotionPathElementPort} xyz.swapee.wc.BoundIMotionPathElementPort */

/** @typedef {xyz.swapee.wc.MotionPathElementPort} xyz.swapee.wc.BoundMotionPathElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ParticlesWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts.particlesWrOpts

/**
 * The options to pass to the _ParticlesWr2_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts.particlesWr2Opts

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder&xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts&xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts)} xyz.swapee.wc.IMotionPathElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder} xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts.typeof */
/**
 * The inputs to the _IMotionPathElement_'s controller via its element port.
 * @record xyz.swapee.wc.IMotionPathElementPort.Inputs
 */
xyz.swapee.wc.IMotionPathElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IMotionPathElementPort.Inputs.constructor&xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts.typeof&xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IMotionPathElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts&xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts)} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts.typeof */
/**
 * The inputs to the _IMotionPathElement_'s controller via its element port.
 * @record xyz.swapee.wc.IMotionPathElementPort.WeakInputs
 */
xyz.swapee.wc.IMotionPathElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IMotionPathElementPort.WeakInputs.constructor&xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts.typeof&xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts.typeof} */ (class {}) { }
xyz.swapee.wc.IMotionPathElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IMotionPathElementPort.WeakInputs

/**
 * Contains getters to cast the _IMotionPathElementPort_ interface.
 * @interface xyz.swapee.wc.IMotionPathElementPortCaster
 */
xyz.swapee.wc.IMotionPathElementPortCaster = class { }
/**
 * Cast the _IMotionPathElementPort_ instance into the _BoundIMotionPathElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathElementPort}
 */
xyz.swapee.wc.IMotionPathElementPortCaster.prototype.asIMotionPathElementPort
/**
 * Access the _MotionPathElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathElementPort}
 */
xyz.swapee.wc.IMotionPathElementPortCaster.prototype.superMotionPathElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts The options to pass to the _ParticlesWr_ vdu (optional overlay).
 * @prop {!Object} [particlesWrOpts] The options to pass to the _ParticlesWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWrOpts_Safe The options to pass to the _ParticlesWr_ vdu (required overlay).
 * @prop {!Object} particlesWrOpts The options to pass to the _ParticlesWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts The options to pass to the _ParticlesWr2_ vdu (optional overlay).
 * @prop {!Object} [particlesWr2Opts] The options to pass to the _ParticlesWr2_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.Inputs.ParticlesWr2Opts_Safe The options to pass to the _ParticlesWr2_ vdu (required overlay).
 * @prop {!Object} particlesWr2Opts The options to pass to the _ParticlesWr2_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts The options to pass to the _ParticlesWr_ vdu (optional overlay).
 * @prop {*} [particlesWrOpts=null] The options to pass to the _ParticlesWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWrOpts_Safe The options to pass to the _ParticlesWr_ vdu (required overlay).
 * @prop {*} particlesWrOpts The options to pass to the _ParticlesWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts The options to pass to the _ParticlesWr2_ vdu (optional overlay).
 * @prop {*} [particlesWr2Opts=null] The options to pass to the _ParticlesWr2_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathElementPort.WeakInputs.ParticlesWr2Opts_Safe The options to pass to the _ParticlesWr2_ vdu (required overlay).
 * @prop {*} particlesWr2Opts The options to pass to the _ParticlesWr2_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/170-IMotionPathDesigner.xml}  a37520d8c47fa01133bf22fe1aa22550 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IMotionPathDesigner
 */
xyz.swapee.wc.IMotionPathDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.MotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IMotionPath />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.MotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IMotionPath />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.MotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IMotionPathDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `MotionPath` _typeof IMotionPathController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IMotionPathDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `MotionPath` _typeof IMotionPathController_
   * - `This` _typeof IMotionPathController_
   * @param {!xyz.swapee.wc.IMotionPathDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `MotionPath` _!MotionPathMemory_
   * - `This` _!MotionPathMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.MotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.MotionPathClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IMotionPathDesigner.prototype.constructor = xyz.swapee.wc.IMotionPathDesigner

/**
 * A concrete class of _IMotionPathDesigner_ instances.
 * @constructor xyz.swapee.wc.MotionPathDesigner
 * @implements {xyz.swapee.wc.IMotionPathDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.MotionPathDesigner = class extends xyz.swapee.wc.IMotionPathDesigner { }
xyz.swapee.wc.MotionPathDesigner.prototype.constructor = xyz.swapee.wc.MotionPathDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IMotionPathController} MotionPath
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IMotionPathController} MotionPath
 * @prop {typeof xyz.swapee.wc.IMotionPathController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IMotionPathDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.MotionPathMemory} MotionPath
 * @prop {!xyz.swapee.wc.MotionPathMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/40-IMotionPathDisplay.xml}  8ec9bbff43fb984cc9e704949e4c46f3 */
/**
 * @typedef {Object} $xyz.swapee.wc.IMotionPathDisplay.Initialese
 * @prop {HTMLDivElement} [ParticlesWr]
 * @prop {HTMLDivElement} [ParticlesWr2]
 */
/** @typedef {$xyz.swapee.wc.IMotionPathDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IMotionPathDisplay.Settings>} xyz.swapee.wc.IMotionPathDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.MotionPathDisplay)} xyz.swapee.wc.AbstractMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathDisplay} xyz.swapee.wc.MotionPathDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathDisplay
 */
xyz.swapee.wc.AbstractMotionPathDisplay = class extends /** @type {xyz.swapee.wc.AbstractMotionPathDisplay.constructor&xyz.swapee.wc.MotionPathDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathDisplay.prototype.constructor = xyz.swapee.wc.AbstractMotionPathDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathDisplay}
 */
xyz.swapee.wc.AbstractMotionPathDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathDisplay}
 */
xyz.swapee.wc.AbstractMotionPathDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathDisplay}
 */
xyz.swapee.wc.AbstractMotionPathDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathDisplay}
 */
xyz.swapee.wc.AbstractMotionPathDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathDisplay.Initialese[]) => xyz.swapee.wc.IMotionPathDisplay} xyz.swapee.wc.MotionPathDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.MotionPathMemory, !HTMLDivElement, !xyz.swapee.wc.IMotionPathDisplay.Settings, xyz.swapee.wc.IMotionPathDisplay.Queries, null>)} xyz.swapee.wc.IMotionPathDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IMotionPath_.
 * @interface xyz.swapee.wc.IMotionPathDisplay
 */
xyz.swapee.wc.IMotionPathDisplay = class extends /** @type {xyz.swapee.wc.IMotionPathDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IMotionPathDisplay.paint} */
xyz.swapee.wc.IMotionPathDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathDisplay.Initialese>)} xyz.swapee.wc.MotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathDisplay} xyz.swapee.wc.IMotionPathDisplay.typeof */
/**
 * A concrete class of _IMotionPathDisplay_ instances.
 * @constructor xyz.swapee.wc.MotionPathDisplay
 * @implements {xyz.swapee.wc.IMotionPathDisplay} Display for presenting information from the _IMotionPath_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathDisplay.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathDisplay = class extends /** @type {xyz.swapee.wc.MotionPathDisplay.constructor&xyz.swapee.wc.IMotionPathDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathDisplay}
 */
xyz.swapee.wc.MotionPathDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathDisplay.
 * @interface xyz.swapee.wc.IMotionPathDisplayFields
 */
xyz.swapee.wc.IMotionPathDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IMotionPathDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IMotionPathDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IMotionPathDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IMotionPathDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IMotionPathDisplayFields.prototype.ParticlesWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IMotionPathDisplayFields.prototype.ParticlesWr2 = /** @type {HTMLDivElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathDisplay} */
xyz.swapee.wc.RecordIMotionPathDisplay

/** @typedef {xyz.swapee.wc.IMotionPathDisplay} xyz.swapee.wc.BoundIMotionPathDisplay */

/** @typedef {xyz.swapee.wc.MotionPathDisplay} xyz.swapee.wc.BoundMotionPathDisplay */

/** @typedef {xyz.swapee.wc.IMotionPathDisplay.Queries} xyz.swapee.wc.IMotionPathDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IMotionPathDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IMotionPathDisplay_ interface.
 * @interface xyz.swapee.wc.IMotionPathDisplayCaster
 */
xyz.swapee.wc.IMotionPathDisplayCaster = class { }
/**
 * Cast the _IMotionPathDisplay_ instance into the _BoundIMotionPathDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathDisplay}
 */
xyz.swapee.wc.IMotionPathDisplayCaster.prototype.asIMotionPathDisplay
/**
 * Cast the _IMotionPathDisplay_ instance into the _BoundIMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathScreen}
 */
xyz.swapee.wc.IMotionPathDisplayCaster.prototype.asIMotionPathScreen
/**
 * Access the _MotionPathDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathDisplay}
 */
xyz.swapee.wc.IMotionPathDisplayCaster.prototype.superMotionPathDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.MotionPathMemory, land: null) => void} xyz.swapee.wc.IMotionPathDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathDisplay.__paint<!xyz.swapee.wc.IMotionPathDisplay>} xyz.swapee.wc.IMotionPathDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IMotionPathDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.MotionPathMemory} memory The display data.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * - `count` _number_ How manu particles to add. Default `0`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/40-IMotionPathDisplayBack.xml}  7f0abf5992b63c7d678911a2009fc4ab */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IMotionPathDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ParticlesWr]
 * @prop {!com.webcircuits.IHtmlTwin} [ParticlesWr2]
 */
/** @typedef {$xyz.swapee.wc.back.IMotionPathDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.MotionPathClasses>} xyz.swapee.wc.back.IMotionPathDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.MotionPathDisplay)} xyz.swapee.wc.back.AbstractMotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.MotionPathDisplay} xyz.swapee.wc.back.MotionPathDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IMotionPathDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractMotionPathDisplay
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractMotionPathDisplay.constructor&xyz.swapee.wc.back.MotionPathDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractMotionPathDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractMotionPathDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractMotionPathDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractMotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathDisplay}
 */
xyz.swapee.wc.back.AbstractMotionPathDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IMotionPathDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.MotionPathClasses, null>)} xyz.swapee.wc.back.IMotionPathDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IMotionPathDisplay
 */
xyz.swapee.wc.back.IMotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.IMotionPathDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IMotionPathDisplay.paint} */
xyz.swapee.wc.back.IMotionPathDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathDisplay.Initialese>)} xyz.swapee.wc.back.MotionPathDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathDisplay} xyz.swapee.wc.back.IMotionPathDisplay.typeof */
/**
 * A concrete class of _IMotionPathDisplay_ instances.
 * @constructor xyz.swapee.wc.back.MotionPathDisplay
 * @implements {xyz.swapee.wc.back.IMotionPathDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.MotionPathDisplay = class extends /** @type {xyz.swapee.wc.back.MotionPathDisplay.constructor&xyz.swapee.wc.back.IMotionPathDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.MotionPathDisplay.prototype.constructor = xyz.swapee.wc.back.MotionPathDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.MotionPathDisplay}
 */
xyz.swapee.wc.back.MotionPathDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathDisplay.
 * @interface xyz.swapee.wc.back.IMotionPathDisplayFields
 */
xyz.swapee.wc.back.IMotionPathDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IMotionPathDisplayFields.prototype.ParticlesWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IMotionPathDisplayFields.prototype.ParticlesWr2 = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IMotionPathDisplay} */
xyz.swapee.wc.back.RecordIMotionPathDisplay

/** @typedef {xyz.swapee.wc.back.IMotionPathDisplay} xyz.swapee.wc.back.BoundIMotionPathDisplay */

/** @typedef {xyz.swapee.wc.back.MotionPathDisplay} xyz.swapee.wc.back.BoundMotionPathDisplay */

/**
 * Contains getters to cast the _IMotionPathDisplay_ interface.
 * @interface xyz.swapee.wc.back.IMotionPathDisplayCaster
 */
xyz.swapee.wc.back.IMotionPathDisplayCaster = class { }
/**
 * Cast the _IMotionPathDisplay_ instance into the _BoundIMotionPathDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIMotionPathDisplay}
 */
xyz.swapee.wc.back.IMotionPathDisplayCaster.prototype.asIMotionPathDisplay
/**
 * Access the _MotionPathDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundMotionPathDisplay}
 */
xyz.swapee.wc.back.IMotionPathDisplayCaster.prototype.superMotionPathDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.MotionPathMemory, land?: null) => void} xyz.swapee.wc.back.IMotionPathDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IMotionPathDisplay.__paint<!xyz.swapee.wc.back.IMotionPathDisplay>} xyz.swapee.wc.back.IMotionPathDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.MotionPathMemory} [memory] The display data.
 * - `gap` _number_ The gap in terms of delay between moving particles. Default `0`.
 * - `count` _number_ How manu particles to add. Default `0`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IMotionPathDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IMotionPathDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/41-MotionPathClasses.xml}  d8f616260db49b2699b6965317ff3b56 */
/**
 * The classes of the _IMotionPathDisplay_.
 * @record xyz.swapee.wc.MotionPathClasses
 */
xyz.swapee.wc.MotionPathClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.MotionPathClasses.prototype.props = /** @type {xyz.swapee.wc.MotionPathClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/50-IMotionPathController.xml}  a63f946f19a197d4303679e05828a034 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IMotionPathController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IMotionPathController.Inputs, !xyz.swapee.wc.IMotionPathOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IMotionPathOuterCore.WeakModel>} xyz.swapee.wc.IMotionPathController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.MotionPathController)} xyz.swapee.wc.AbstractMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathController} xyz.swapee.wc.MotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathController` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathController
 */
xyz.swapee.wc.AbstractMotionPathController = class extends /** @type {xyz.swapee.wc.AbstractMotionPathController.constructor&xyz.swapee.wc.MotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathController.prototype.constructor = xyz.swapee.wc.AbstractMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathController.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathController}
 */
xyz.swapee.wc.AbstractMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathController}
 */
xyz.swapee.wc.AbstractMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathController}
 */
xyz.swapee.wc.AbstractMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathController}
 */
xyz.swapee.wc.AbstractMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathController.Initialese[]) => xyz.swapee.wc.IMotionPathController} xyz.swapee.wc.MotionPathControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IMotionPathController.Inputs, !xyz.swapee.wc.IMotionPathOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IMotionPathOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IMotionPathController.Inputs, !xyz.swapee.wc.IMotionPathController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IMotionPathController.Inputs, !xyz.swapee.wc.MotionPathMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IMotionPathController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IMotionPathController.Inputs>)} xyz.swapee.wc.IMotionPathController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IMotionPathController
 */
xyz.swapee.wc.IMotionPathController = class extends /** @type {xyz.swapee.wc.IMotionPathController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IMotionPathController.resetPort} */
xyz.swapee.wc.IMotionPathController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathController.Initialese>)} xyz.swapee.wc.MotionPathController.constructor */
/**
 * A concrete class of _IMotionPathController_ instances.
 * @constructor xyz.swapee.wc.MotionPathController
 * @implements {xyz.swapee.wc.IMotionPathController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathController = class extends /** @type {xyz.swapee.wc.MotionPathController.constructor&xyz.swapee.wc.IMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathController}
 */
xyz.swapee.wc.MotionPathController.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathController.
 * @interface xyz.swapee.wc.IMotionPathControllerFields
 */
xyz.swapee.wc.IMotionPathControllerFields = class { }
/**
 * The inputs to the _IMotionPath_'s controller.
 */
xyz.swapee.wc.IMotionPathControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IMotionPathController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IMotionPathControllerFields.prototype.props = /** @type {xyz.swapee.wc.IMotionPathController} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathController} */
xyz.swapee.wc.RecordIMotionPathController

/** @typedef {xyz.swapee.wc.IMotionPathController} xyz.swapee.wc.BoundIMotionPathController */

/** @typedef {xyz.swapee.wc.MotionPathController} xyz.swapee.wc.BoundMotionPathController */

/** @typedef {xyz.swapee.wc.IMotionPathPort.Inputs} xyz.swapee.wc.IMotionPathController.Inputs The inputs to the _IMotionPath_'s controller. */

/** @typedef {xyz.swapee.wc.IMotionPathPort.WeakInputs} xyz.swapee.wc.IMotionPathController.WeakInputs The inputs to the _IMotionPath_'s controller. */

/**
 * Contains getters to cast the _IMotionPathController_ interface.
 * @interface xyz.swapee.wc.IMotionPathControllerCaster
 */
xyz.swapee.wc.IMotionPathControllerCaster = class { }
/**
 * Cast the _IMotionPathController_ instance into the _BoundIMotionPathController_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathController}
 */
xyz.swapee.wc.IMotionPathControllerCaster.prototype.asIMotionPathController
/**
 * Cast the _IMotionPathController_ instance into the _BoundIMotionPathProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathProcessor}
 */
xyz.swapee.wc.IMotionPathControllerCaster.prototype.asIMotionPathProcessor
/**
 * Access the _MotionPathController_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathController}
 */
xyz.swapee.wc.IMotionPathControllerCaster.prototype.superMotionPathController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IMotionPathController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IMotionPathController.__resetPort<!xyz.swapee.wc.IMotionPathController>} xyz.swapee.wc.IMotionPathController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IMotionPathController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IMotionPathController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IMotionPathController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/51-IMotionPathControllerFront.xml}  488d3c2f597a06e2506d0f4bd4c862d8 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IMotionPathController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.MotionPathController)} xyz.swapee.wc.front.AbstractMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.MotionPathController} xyz.swapee.wc.front.MotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IMotionPathController` interface.
 * @constructor xyz.swapee.wc.front.AbstractMotionPathController
 */
xyz.swapee.wc.front.AbstractMotionPathController = class extends /** @type {xyz.swapee.wc.front.AbstractMotionPathController.constructor&xyz.swapee.wc.front.MotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractMotionPathController.prototype.constructor = xyz.swapee.wc.front.AbstractMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractMotionPathController.class = /** @type {typeof xyz.swapee.wc.front.AbstractMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractMotionPathController}
 */
xyz.swapee.wc.front.AbstractMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathController}
 */
xyz.swapee.wc.front.AbstractMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathController}
 */
xyz.swapee.wc.front.AbstractMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathController}
 */
xyz.swapee.wc.front.AbstractMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IMotionPathController.Initialese[]) => xyz.swapee.wc.front.IMotionPathController} xyz.swapee.wc.front.MotionPathControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IMotionPathControllerCaster&xyz.swapee.wc.front.IMotionPathControllerAT)} xyz.swapee.wc.front.IMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IMotionPathControllerAT} xyz.swapee.wc.front.IMotionPathControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IMotionPathController
 */
xyz.swapee.wc.front.IMotionPathController = class extends /** @type {xyz.swapee.wc.front.IMotionPathController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IMotionPathControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IMotionPathController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathController.Initialese>)} xyz.swapee.wc.front.MotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IMotionPathController} xyz.swapee.wc.front.IMotionPathController.typeof */
/**
 * A concrete class of _IMotionPathController_ instances.
 * @constructor xyz.swapee.wc.front.MotionPathController
 * @implements {xyz.swapee.wc.front.IMotionPathController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.front.MotionPathController = class extends /** @type {xyz.swapee.wc.front.MotionPathController.constructor&xyz.swapee.wc.front.IMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.MotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.MotionPathController}
 */
xyz.swapee.wc.front.MotionPathController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IMotionPathController} */
xyz.swapee.wc.front.RecordIMotionPathController

/** @typedef {xyz.swapee.wc.front.IMotionPathController} xyz.swapee.wc.front.BoundIMotionPathController */

/** @typedef {xyz.swapee.wc.front.MotionPathController} xyz.swapee.wc.front.BoundMotionPathController */

/**
 * Contains getters to cast the _IMotionPathController_ interface.
 * @interface xyz.swapee.wc.front.IMotionPathControllerCaster
 */
xyz.swapee.wc.front.IMotionPathControllerCaster = class { }
/**
 * Cast the _IMotionPathController_ instance into the _BoundIMotionPathController_ type.
 * @type {!xyz.swapee.wc.front.BoundIMotionPathController}
 */
xyz.swapee.wc.front.IMotionPathControllerCaster.prototype.asIMotionPathController
/**
 * Access the _MotionPathController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundMotionPathController}
 */
xyz.swapee.wc.front.IMotionPathControllerCaster.prototype.superMotionPathController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/52-IMotionPathControllerBack.xml}  d7c33f3063979e507bd2bd446e664752 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IMotionPathController.Inputs>&xyz.swapee.wc.IMotionPathController.Initialese} xyz.swapee.wc.back.IMotionPathController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.MotionPathController)} xyz.swapee.wc.back.AbstractMotionPathController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.MotionPathController} xyz.swapee.wc.back.MotionPathController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IMotionPathController` interface.
 * @constructor xyz.swapee.wc.back.AbstractMotionPathController
 */
xyz.swapee.wc.back.AbstractMotionPathController = class extends /** @type {xyz.swapee.wc.back.AbstractMotionPathController.constructor&xyz.swapee.wc.back.MotionPathController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractMotionPathController.prototype.constructor = xyz.swapee.wc.back.AbstractMotionPathController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractMotionPathController.class = /** @type {typeof xyz.swapee.wc.back.AbstractMotionPathController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractMotionPathController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractMotionPathController}
 */
xyz.swapee.wc.back.AbstractMotionPathController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathController}
 */
xyz.swapee.wc.back.AbstractMotionPathController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathController}
 */
xyz.swapee.wc.back.AbstractMotionPathController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IMotionPathController|typeof xyz.swapee.wc.back.MotionPathController)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathController}
 */
xyz.swapee.wc.back.AbstractMotionPathController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IMotionPathController.Initialese[]) => xyz.swapee.wc.back.IMotionPathController} xyz.swapee.wc.back.MotionPathControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IMotionPathControllerCaster&xyz.swapee.wc.IMotionPathController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IMotionPathController.Inputs>)} xyz.swapee.wc.back.IMotionPathController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IMotionPathController
 */
xyz.swapee.wc.back.IMotionPathController = class extends /** @type {xyz.swapee.wc.back.IMotionPathController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IMotionPathController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IMotionPathController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathController.Initialese>)} xyz.swapee.wc.back.MotionPathController.constructor */
/**
 * A concrete class of _IMotionPathController_ instances.
 * @constructor xyz.swapee.wc.back.MotionPathController
 * @implements {xyz.swapee.wc.back.IMotionPathController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathController.Initialese>} ‎
 */
xyz.swapee.wc.back.MotionPathController = class extends /** @type {xyz.swapee.wc.back.MotionPathController.constructor&xyz.swapee.wc.back.IMotionPathController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IMotionPathController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.MotionPathController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.MotionPathController}
 */
xyz.swapee.wc.back.MotionPathController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IMotionPathController} */
xyz.swapee.wc.back.RecordIMotionPathController

/** @typedef {xyz.swapee.wc.back.IMotionPathController} xyz.swapee.wc.back.BoundIMotionPathController */

/** @typedef {xyz.swapee.wc.back.MotionPathController} xyz.swapee.wc.back.BoundMotionPathController */

/**
 * Contains getters to cast the _IMotionPathController_ interface.
 * @interface xyz.swapee.wc.back.IMotionPathControllerCaster
 */
xyz.swapee.wc.back.IMotionPathControllerCaster = class { }
/**
 * Cast the _IMotionPathController_ instance into the _BoundIMotionPathController_ type.
 * @type {!xyz.swapee.wc.back.BoundIMotionPathController}
 */
xyz.swapee.wc.back.IMotionPathControllerCaster.prototype.asIMotionPathController
/**
 * Access the _MotionPathController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundMotionPathController}
 */
xyz.swapee.wc.back.IMotionPathControllerCaster.prototype.superMotionPathController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/53-IMotionPathControllerAR.xml}  32d4bac9683227eb42258e9075744bea */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IMotionPathController.Initialese} xyz.swapee.wc.back.IMotionPathControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.MotionPathControllerAR)} xyz.swapee.wc.back.AbstractMotionPathControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.MotionPathControllerAR} xyz.swapee.wc.back.MotionPathControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IMotionPathControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractMotionPathControllerAR
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractMotionPathControllerAR.constructor&xyz.swapee.wc.back.MotionPathControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractMotionPathControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractMotionPathControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractMotionPathControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IMotionPathControllerAR|typeof xyz.swapee.wc.back.MotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractMotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IMotionPathControllerAR|typeof xyz.swapee.wc.back.MotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IMotionPathControllerAR|typeof xyz.swapee.wc.back.MotionPathControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathController|typeof xyz.swapee.wc.MotionPathController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathControllerAR}
 */
xyz.swapee.wc.back.AbstractMotionPathControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IMotionPathControllerAR.Initialese[]) => xyz.swapee.wc.back.IMotionPathControllerAR} xyz.swapee.wc.back.MotionPathControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IMotionPathControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IMotionPathController)} xyz.swapee.wc.back.IMotionPathControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IMotionPathControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IMotionPathControllerAR
 */
xyz.swapee.wc.back.IMotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.IMotionPathControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IMotionPathController.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IMotionPathControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathControllerAR.Initialese>)} xyz.swapee.wc.back.MotionPathControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathControllerAR} xyz.swapee.wc.back.IMotionPathControllerAR.typeof */
/**
 * A concrete class of _IMotionPathControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.MotionPathControllerAR
 * @implements {xyz.swapee.wc.back.IMotionPathControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IMotionPathControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.MotionPathControllerAR = class extends /** @type {xyz.swapee.wc.back.MotionPathControllerAR.constructor&xyz.swapee.wc.back.IMotionPathControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IMotionPathControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.MotionPathControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.MotionPathControllerAR}
 */
xyz.swapee.wc.back.MotionPathControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IMotionPathControllerAR} */
xyz.swapee.wc.back.RecordIMotionPathControllerAR

/** @typedef {xyz.swapee.wc.back.IMotionPathControllerAR} xyz.swapee.wc.back.BoundIMotionPathControllerAR */

/** @typedef {xyz.swapee.wc.back.MotionPathControllerAR} xyz.swapee.wc.back.BoundMotionPathControllerAR */

/**
 * Contains getters to cast the _IMotionPathControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IMotionPathControllerARCaster
 */
xyz.swapee.wc.back.IMotionPathControllerARCaster = class { }
/**
 * Cast the _IMotionPathControllerAR_ instance into the _BoundIMotionPathControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIMotionPathControllerAR}
 */
xyz.swapee.wc.back.IMotionPathControllerARCaster.prototype.asIMotionPathControllerAR
/**
 * Access the _MotionPathControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundMotionPathControllerAR}
 */
xyz.swapee.wc.back.IMotionPathControllerARCaster.prototype.superMotionPathControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/54-IMotionPathControllerAT.xml}  0252a2cb5d66ece914b2ff27dc787570 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IMotionPathControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.MotionPathControllerAT)} xyz.swapee.wc.front.AbstractMotionPathControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.MotionPathControllerAT} xyz.swapee.wc.front.MotionPathControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IMotionPathControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractMotionPathControllerAT
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractMotionPathControllerAT.constructor&xyz.swapee.wc.front.MotionPathControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractMotionPathControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractMotionPathControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractMotionPathControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractMotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IMotionPathControllerAT|typeof xyz.swapee.wc.front.MotionPathControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathControllerAT}
 */
xyz.swapee.wc.front.AbstractMotionPathControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IMotionPathControllerAT.Initialese[]) => xyz.swapee.wc.front.IMotionPathControllerAT} xyz.swapee.wc.front.MotionPathControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IMotionPathControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IMotionPathControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IMotionPathControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IMotionPathControllerAT
 */
xyz.swapee.wc.front.IMotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.IMotionPathControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IMotionPathControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IMotionPathControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathControllerAT.Initialese>)} xyz.swapee.wc.front.MotionPathControllerAT.constructor */
/**
 * A concrete class of _IMotionPathControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.MotionPathControllerAT
 * @implements {xyz.swapee.wc.front.IMotionPathControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IMotionPathControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.MotionPathControllerAT = class extends /** @type {xyz.swapee.wc.front.MotionPathControllerAT.constructor&xyz.swapee.wc.front.IMotionPathControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IMotionPathControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.MotionPathControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.MotionPathControllerAT}
 */
xyz.swapee.wc.front.MotionPathControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IMotionPathControllerAT} */
xyz.swapee.wc.front.RecordIMotionPathControllerAT

/** @typedef {xyz.swapee.wc.front.IMotionPathControllerAT} xyz.swapee.wc.front.BoundIMotionPathControllerAT */

/** @typedef {xyz.swapee.wc.front.MotionPathControllerAT} xyz.swapee.wc.front.BoundMotionPathControllerAT */

/**
 * Contains getters to cast the _IMotionPathControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IMotionPathControllerATCaster
 */
xyz.swapee.wc.front.IMotionPathControllerATCaster = class { }
/**
 * Cast the _IMotionPathControllerAT_ instance into the _BoundIMotionPathControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIMotionPathControllerAT}
 */
xyz.swapee.wc.front.IMotionPathControllerATCaster.prototype.asIMotionPathControllerAT
/**
 * Access the _MotionPathControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundMotionPathControllerAT}
 */
xyz.swapee.wc.front.IMotionPathControllerATCaster.prototype.superMotionPathControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/70-IMotionPathScreen.xml}  2ca30c3f693e56cd34b1f6902adc7ff7 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.front.MotionPathInputs, !HTMLDivElement, !xyz.swapee.wc.IMotionPathDisplay.Settings, !xyz.swapee.wc.IMotionPathDisplay.Queries, null>&xyz.swapee.wc.IMotionPathDisplay.Initialese} xyz.swapee.wc.IMotionPathScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.MotionPathScreen)} xyz.swapee.wc.AbstractMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathScreen} xyz.swapee.wc.MotionPathScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathScreen` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathScreen
 */
xyz.swapee.wc.AbstractMotionPathScreen = class extends /** @type {xyz.swapee.wc.AbstractMotionPathScreen.constructor&xyz.swapee.wc.MotionPathScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathScreen.prototype.constructor = xyz.swapee.wc.AbstractMotionPathScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathScreen.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathScreen}
 */
xyz.swapee.wc.AbstractMotionPathScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathScreen}
 */
xyz.swapee.wc.AbstractMotionPathScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathScreen}
 */
xyz.swapee.wc.AbstractMotionPathScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IMotionPathController|typeof xyz.swapee.wc.front.MotionPathController)|(!xyz.swapee.wc.IMotionPathDisplay|typeof xyz.swapee.wc.MotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathScreen}
 */
xyz.swapee.wc.AbstractMotionPathScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathScreen.Initialese[]) => xyz.swapee.wc.IMotionPathScreen} xyz.swapee.wc.MotionPathScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IMotionPathScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.MotionPathMemory, !xyz.swapee.wc.front.MotionPathInputs, !HTMLDivElement, !xyz.swapee.wc.IMotionPathDisplay.Settings, !xyz.swapee.wc.IMotionPathDisplay.Queries, null, null>&xyz.swapee.wc.front.IMotionPathController&xyz.swapee.wc.IMotionPathDisplay)} xyz.swapee.wc.IMotionPathScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IMotionPathScreen
 */
xyz.swapee.wc.IMotionPathScreen = class extends /** @type {xyz.swapee.wc.IMotionPathScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IMotionPathController.typeof&xyz.swapee.wc.IMotionPathDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathScreen.Initialese>)} xyz.swapee.wc.MotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IMotionPathScreen} xyz.swapee.wc.IMotionPathScreen.typeof */
/**
 * A concrete class of _IMotionPathScreen_ instances.
 * @constructor xyz.swapee.wc.MotionPathScreen
 * @implements {xyz.swapee.wc.IMotionPathScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathScreen.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathScreen = class extends /** @type {xyz.swapee.wc.MotionPathScreen.constructor&xyz.swapee.wc.IMotionPathScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathScreen}
 */
xyz.swapee.wc.MotionPathScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IMotionPathScreen} */
xyz.swapee.wc.RecordIMotionPathScreen

/** @typedef {xyz.swapee.wc.IMotionPathScreen} xyz.swapee.wc.BoundIMotionPathScreen */

/** @typedef {xyz.swapee.wc.MotionPathScreen} xyz.swapee.wc.BoundMotionPathScreen */

/**
 * Contains getters to cast the _IMotionPathScreen_ interface.
 * @interface xyz.swapee.wc.IMotionPathScreenCaster
 */
xyz.swapee.wc.IMotionPathScreenCaster = class { }
/**
 * Cast the _IMotionPathScreen_ instance into the _BoundIMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathScreen}
 */
xyz.swapee.wc.IMotionPathScreenCaster.prototype.asIMotionPathScreen
/**
 * Access the _MotionPathScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathScreen}
 */
xyz.swapee.wc.IMotionPathScreenCaster.prototype.superMotionPathScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/70-IMotionPathScreenBack.xml}  4e2866d7c7470110245c0ea6f7439a05 */
/** @typedef {xyz.swapee.wc.back.IMotionPathScreenAT.Initialese} xyz.swapee.wc.back.IMotionPathScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.MotionPathScreen)} xyz.swapee.wc.back.AbstractMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.MotionPathScreen} xyz.swapee.wc.back.MotionPathScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IMotionPathScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractMotionPathScreen
 */
xyz.swapee.wc.back.AbstractMotionPathScreen = class extends /** @type {xyz.swapee.wc.back.AbstractMotionPathScreen.constructor&xyz.swapee.wc.back.MotionPathScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractMotionPathScreen.prototype.constructor = xyz.swapee.wc.back.AbstractMotionPathScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractMotionPathScreen}
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreen}
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreen}
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreen|typeof xyz.swapee.wc.back.MotionPathScreen)|(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreen}
 */
xyz.swapee.wc.back.AbstractMotionPathScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IMotionPathScreen.Initialese[]) => xyz.swapee.wc.back.IMotionPathScreen} xyz.swapee.wc.back.MotionPathScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IMotionPathScreenCaster&xyz.swapee.wc.back.IMotionPathScreenAT)} xyz.swapee.wc.back.IMotionPathScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IMotionPathScreenAT} xyz.swapee.wc.back.IMotionPathScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IMotionPathScreen
 */
xyz.swapee.wc.back.IMotionPathScreen = class extends /** @type {xyz.swapee.wc.back.IMotionPathScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IMotionPathScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IMotionPathScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathScreen.Initialese>)} xyz.swapee.wc.back.MotionPathScreen.constructor */
/**
 * A concrete class of _IMotionPathScreen_ instances.
 * @constructor xyz.swapee.wc.back.MotionPathScreen
 * @implements {xyz.swapee.wc.back.IMotionPathScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.MotionPathScreen = class extends /** @type {xyz.swapee.wc.back.MotionPathScreen.constructor&xyz.swapee.wc.back.IMotionPathScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IMotionPathScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.MotionPathScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.MotionPathScreen}
 */
xyz.swapee.wc.back.MotionPathScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IMotionPathScreen} */
xyz.swapee.wc.back.RecordIMotionPathScreen

/** @typedef {xyz.swapee.wc.back.IMotionPathScreen} xyz.swapee.wc.back.BoundIMotionPathScreen */

/** @typedef {xyz.swapee.wc.back.MotionPathScreen} xyz.swapee.wc.back.BoundMotionPathScreen */

/**
 * Contains getters to cast the _IMotionPathScreen_ interface.
 * @interface xyz.swapee.wc.back.IMotionPathScreenCaster
 */
xyz.swapee.wc.back.IMotionPathScreenCaster = class { }
/**
 * Cast the _IMotionPathScreen_ instance into the _BoundIMotionPathScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIMotionPathScreen}
 */
xyz.swapee.wc.back.IMotionPathScreenCaster.prototype.asIMotionPathScreen
/**
 * Access the _MotionPathScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundMotionPathScreen}
 */
xyz.swapee.wc.back.IMotionPathScreenCaster.prototype.superMotionPathScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/73-IMotionPathScreenAR.xml}  1a16ac2205c9897a025013a8ecceb195 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IMotionPathScreen.Initialese} xyz.swapee.wc.front.IMotionPathScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.MotionPathScreenAR)} xyz.swapee.wc.front.AbstractMotionPathScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.MotionPathScreenAR} xyz.swapee.wc.front.MotionPathScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IMotionPathScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractMotionPathScreenAR
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractMotionPathScreenAR.constructor&xyz.swapee.wc.front.MotionPathScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractMotionPathScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractMotionPathScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractMotionPathScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IMotionPathScreenAR|typeof xyz.swapee.wc.front.MotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractMotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IMotionPathScreenAR|typeof xyz.swapee.wc.front.MotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IMotionPathScreenAR|typeof xyz.swapee.wc.front.MotionPathScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IMotionPathScreen|typeof xyz.swapee.wc.MotionPathScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.MotionPathScreenAR}
 */
xyz.swapee.wc.front.AbstractMotionPathScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IMotionPathScreenAR.Initialese[]) => xyz.swapee.wc.front.IMotionPathScreenAR} xyz.swapee.wc.front.MotionPathScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IMotionPathScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IMotionPathScreen)} xyz.swapee.wc.front.IMotionPathScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IMotionPathScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IMotionPathScreenAR
 */
xyz.swapee.wc.front.IMotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.IMotionPathScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IMotionPathScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IMotionPathScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IMotionPathScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathScreenAR.Initialese>)} xyz.swapee.wc.front.MotionPathScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IMotionPathScreenAR} xyz.swapee.wc.front.IMotionPathScreenAR.typeof */
/**
 * A concrete class of _IMotionPathScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.MotionPathScreenAR
 * @implements {xyz.swapee.wc.front.IMotionPathScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IMotionPathScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IMotionPathScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.MotionPathScreenAR = class extends /** @type {xyz.swapee.wc.front.MotionPathScreenAR.constructor&xyz.swapee.wc.front.IMotionPathScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IMotionPathScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IMotionPathScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.MotionPathScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.MotionPathScreenAR}
 */
xyz.swapee.wc.front.MotionPathScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IMotionPathScreenAR} */
xyz.swapee.wc.front.RecordIMotionPathScreenAR

/** @typedef {xyz.swapee.wc.front.IMotionPathScreenAR} xyz.swapee.wc.front.BoundIMotionPathScreenAR */

/** @typedef {xyz.swapee.wc.front.MotionPathScreenAR} xyz.swapee.wc.front.BoundMotionPathScreenAR */

/**
 * Contains getters to cast the _IMotionPathScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IMotionPathScreenARCaster
 */
xyz.swapee.wc.front.IMotionPathScreenARCaster = class { }
/**
 * Cast the _IMotionPathScreenAR_ instance into the _BoundIMotionPathScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIMotionPathScreenAR}
 */
xyz.swapee.wc.front.IMotionPathScreenARCaster.prototype.asIMotionPathScreenAR
/**
 * Access the _MotionPathScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundMotionPathScreenAR}
 */
xyz.swapee.wc.front.IMotionPathScreenARCaster.prototype.superMotionPathScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/74-IMotionPathScreenAT.xml}  d81848493bd9dfaaf16cb5cb5458d906 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IMotionPathScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.MotionPathScreenAT)} xyz.swapee.wc.back.AbstractMotionPathScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.MotionPathScreenAT} xyz.swapee.wc.back.MotionPathScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IMotionPathScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractMotionPathScreenAT
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractMotionPathScreenAT.constructor&xyz.swapee.wc.back.MotionPathScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractMotionPathScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractMotionPathScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractMotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IMotionPathScreenAT|typeof xyz.swapee.wc.back.MotionPathScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.MotionPathScreenAT}
 */
xyz.swapee.wc.back.AbstractMotionPathScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IMotionPathScreenAT.Initialese[]) => xyz.swapee.wc.back.IMotionPathScreenAT} xyz.swapee.wc.back.MotionPathScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IMotionPathScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IMotionPathScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IMotionPathScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IMotionPathScreenAT
 */
xyz.swapee.wc.back.IMotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.IMotionPathScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IMotionPathScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IMotionPathScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathScreenAT.Initialese>)} xyz.swapee.wc.back.MotionPathScreenAT.constructor */
/**
 * A concrete class of _IMotionPathScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.MotionPathScreenAT
 * @implements {xyz.swapee.wc.back.IMotionPathScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IMotionPathScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IMotionPathScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.MotionPathScreenAT = class extends /** @type {xyz.swapee.wc.back.MotionPathScreenAT.constructor&xyz.swapee.wc.back.IMotionPathScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IMotionPathScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IMotionPathScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.MotionPathScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.MotionPathScreenAT}
 */
xyz.swapee.wc.back.MotionPathScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IMotionPathScreenAT} */
xyz.swapee.wc.back.RecordIMotionPathScreenAT

/** @typedef {xyz.swapee.wc.back.IMotionPathScreenAT} xyz.swapee.wc.back.BoundIMotionPathScreenAT */

/** @typedef {xyz.swapee.wc.back.MotionPathScreenAT} xyz.swapee.wc.back.BoundMotionPathScreenAT */

/**
 * Contains getters to cast the _IMotionPathScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IMotionPathScreenATCaster
 */
xyz.swapee.wc.back.IMotionPathScreenATCaster = class { }
/**
 * Cast the _IMotionPathScreenAT_ instance into the _BoundIMotionPathScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIMotionPathScreenAT}
 */
xyz.swapee.wc.back.IMotionPathScreenATCaster.prototype.asIMotionPathScreenAT
/**
 * Access the _MotionPathScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundMotionPathScreenAT}
 */
xyz.swapee.wc.back.IMotionPathScreenATCaster.prototype.superMotionPathScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/motion-path/MotionPath.mvc/design/80-IMotionPathGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IMotionPathDisplay.Initialese} xyz.swapee.wc.IMotionPathGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.MotionPathGPU)} xyz.swapee.wc.AbstractMotionPathGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.MotionPathGPU} xyz.swapee.wc.MotionPathGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IMotionPathGPU` interface.
 * @constructor xyz.swapee.wc.AbstractMotionPathGPU
 */
xyz.swapee.wc.AbstractMotionPathGPU = class extends /** @type {xyz.swapee.wc.AbstractMotionPathGPU.constructor&xyz.swapee.wc.MotionPathGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractMotionPathGPU.prototype.constructor = xyz.swapee.wc.AbstractMotionPathGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractMotionPathGPU.class = /** @type {typeof xyz.swapee.wc.AbstractMotionPathGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractMotionPathGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractMotionPathGPU}
 */
xyz.swapee.wc.AbstractMotionPathGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathGPU}
 */
xyz.swapee.wc.AbstractMotionPathGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathGPU}
 */
xyz.swapee.wc.AbstractMotionPathGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IMotionPathGPU|typeof xyz.swapee.wc.MotionPathGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IMotionPathDisplay|typeof xyz.swapee.wc.back.MotionPathDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.MotionPathGPU}
 */
xyz.swapee.wc.AbstractMotionPathGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IMotionPathGPU.Initialese[]) => xyz.swapee.wc.IMotionPathGPU} xyz.swapee.wc.MotionPathGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IMotionPathGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IMotionPathGPUCaster&com.webcircuits.IBrowserView<.!MotionPathMemory,>&xyz.swapee.wc.back.IMotionPathDisplay)} xyz.swapee.wc.IMotionPathGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!MotionPathMemory,>} com.webcircuits.IBrowserView<.!MotionPathMemory,>.typeof */
/**
 * Handles the periphery of the _IMotionPathDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IMotionPathGPU
 */
xyz.swapee.wc.IMotionPathGPU = class extends /** @type {xyz.swapee.wc.IMotionPathGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!MotionPathMemory,>.typeof&xyz.swapee.wc.back.IMotionPathDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IMotionPathGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IMotionPathGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IMotionPathGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathGPU.Initialese>)} xyz.swapee.wc.MotionPathGPU.constructor */
/**
 * A concrete class of _IMotionPathGPU_ instances.
 * @constructor xyz.swapee.wc.MotionPathGPU
 * @implements {xyz.swapee.wc.IMotionPathGPU} Handles the periphery of the _IMotionPathDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IMotionPathGPU.Initialese>} ‎
 */
xyz.swapee.wc.MotionPathGPU = class extends /** @type {xyz.swapee.wc.MotionPathGPU.constructor&xyz.swapee.wc.IMotionPathGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IMotionPathGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IMotionPathGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IMotionPathGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IMotionPathGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.MotionPathGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.MotionPathGPU}
 */
xyz.swapee.wc.MotionPathGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IMotionPathGPU.
 * @interface xyz.swapee.wc.IMotionPathGPUFields
 */
xyz.swapee.wc.IMotionPathGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IMotionPathGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IMotionPathGPU} */
xyz.swapee.wc.RecordIMotionPathGPU

/** @typedef {xyz.swapee.wc.IMotionPathGPU} xyz.swapee.wc.BoundIMotionPathGPU */

/** @typedef {xyz.swapee.wc.MotionPathGPU} xyz.swapee.wc.BoundMotionPathGPU */

/**
 * Contains getters to cast the _IMotionPathGPU_ interface.
 * @interface xyz.swapee.wc.IMotionPathGPUCaster
 */
xyz.swapee.wc.IMotionPathGPUCaster = class { }
/**
 * Cast the _IMotionPathGPU_ instance into the _BoundIMotionPathGPU_ type.
 * @type {!xyz.swapee.wc.BoundIMotionPathGPU}
 */
xyz.swapee.wc.IMotionPathGPUCaster.prototype.asIMotionPathGPU
/**
 * Access the _MotionPathGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundMotionPathGPU}
 */
xyz.swapee.wc.IMotionPathGPUCaster.prototype.superMotionPathGPU

// nss:xyz.swapee.wc
/* @typal-end */