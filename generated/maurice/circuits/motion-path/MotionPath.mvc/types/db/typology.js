/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IMotionPathComputer': {
  'id': 32099806271,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.MotionPathMemoryPQs': {
  'id': 32099806272,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IMotionPathOuterCore': {
  'id': 32099806273,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.MotionPathInputsPQs': {
  'id': 32099806274,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IMotionPathPort': {
  'id': 32099806275,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetMotionPathPort': 2
  }
 },
 'xyz.swapee.wc.IMotionPathPortInterface': {
  'id': 32099806276,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathCore': {
  'id': 32099806277,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetMotionPathCore': 2
  }
 },
 'xyz.swapee.wc.IMotionPathProcessor': {
  'id': 32099806278,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPath': {
  'id': 32099806279,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathBuffer': {
  'id': 320998062710,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathHtmlComponent': {
  'id': 320998062711,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathElement': {
  'id': 320998062712,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'server': 3,
   'inducer': 4
  }
 },
 'xyz.swapee.wc.IMotionPathElementPort': {
  'id': 320998062713,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathDesigner': {
  'id': 320998062714,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IMotionPathGPU': {
  'id': 320998062715,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathDisplay': {
  'id': 320998062716,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.MotionPathVdusPQs': {
  'id': 320998062717,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IMotionPathDisplay': {
  'id': 320998062718,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IMotionPathController': {
  'id': 320998062719,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IMotionPathController': {
  'id': 320998062720,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathController': {
  'id': 320998062721,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathControllerAR': {
  'id': 320998062722,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IMotionPathControllerAT': {
  'id': 320998062723,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IMotionPathScreen': {
  'id': 320998062724,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathScreen': {
  'id': 320998062725,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IMotionPathScreenAR': {
  'id': 320998062726,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IMotionPathScreenAT': {
  'id': 320998062727,
  'symbols': {},
  'methods': {}
 }
})