
import AbstractMotionPath from '../AbstractMotionPath'

/** @abstract {xyz.swapee.wc.IMotionPathElement} */
export default class AbstractMotionPathElement { }



AbstractMotionPathElement[$implementations]=[AbstractMotionPath,
 /** @type {!AbstractMotionPathElement} */ ({
  rootId:'MotionPath',
  __$id:3209980627,
  fqn:'xyz.swapee.wc.IMotionPath',
  maurice_element_v3:true,
 }),
]