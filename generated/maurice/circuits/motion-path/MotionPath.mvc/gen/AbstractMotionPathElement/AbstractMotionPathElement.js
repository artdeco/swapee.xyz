import MotionPathRenderVdus from './methods/render-vdus'
import MotionPathElementPort from '../MotionPathElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {MotionPathInputsPQs} from '../../pqs/MotionPathInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractMotionPath from '../AbstractMotionPath'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathElement}
 */
function __AbstractMotionPathElement() {}
__AbstractMotionPathElement.prototype = /** @type {!_AbstractMotionPathElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathElement}
 */
class _AbstractMotionPathElement { }
/**
 * A component description.
 *
 * The _IMotionPath_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractMotionPathElement} ‎
 */
class AbstractMotionPathElement extends newAbstract(
 _AbstractMotionPathElement,320998062712,null,{
  asIMotionPathElement:1,
  superMotionPathElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathElement} */
AbstractMotionPathElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathElement} */
function AbstractMotionPathElementClass(){}

export default AbstractMotionPathElement


AbstractMotionPathElement[$implementations]=[
 __AbstractMotionPathElement,
 ElementBase,
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':gap':gapColAttr,
   ':count':countColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'gap':gapAttr,
    'count':countAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(gapAttr===undefined?{'gap':gapColAttr}:{}),
    ...(countAttr===undefined?{'count':countColAttr}:{}),
   }
  },
 }),
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'gap':gapAttr,
   'count':countAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    gap:gapAttr,
    count:countAttr,
   }
  },
 }),
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  render:MotionPathRenderVdus,
 }),
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  inputsPQs:MotionPathInputsPQs,
  vdus:{
   'ParticlesWr': 'e2311',
   'ParticlesWr2': 'e2312',
  },
 }),
 IntegratedComponent,
 AbstractMotionPathElementClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','gap','count','no-solder',':no-solder',':gap',':count','fe646','7cbd8','a8a6a','df9bc','e2942','children']),
   })
  },
  get Port(){
   return MotionPathElementPort
  },
 }),
]



AbstractMotionPathElement[$implementations]=[AbstractMotionPath,
 /** @type {!AbstractMotionPathElement} */ ({
  rootId:'MotionPath',
  __$id:3209980627,
  fqn:'xyz.swapee.wc.IMotionPath',
  maurice_element_v3:true,
 }),
]