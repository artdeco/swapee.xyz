import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathScreenAT}
 */
function __AbstractMotionPathScreenAT() {}
__AbstractMotionPathScreenAT.prototype = /** @type {!_AbstractMotionPathScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractMotionPathScreenAT}
 */
class _AbstractMotionPathScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IMotionPathScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractMotionPathScreenAT} ‎
 */
class AbstractMotionPathScreenAT extends newAbstract(
 _AbstractMotionPathScreenAT,320998062727,null,{
  asIMotionPathScreenAT:1,
  superMotionPathScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreenAT} */
AbstractMotionPathScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreenAT} */
function AbstractMotionPathScreenATClass(){}

export default AbstractMotionPathScreenAT


AbstractMotionPathScreenAT[$implementations]=[
 __AbstractMotionPathScreenAT,
 UartUniversal,
]