import MotionPathBuffer from '../MotionPathBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {MotionPathPortConnector} from '../MotionPathPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathController}
 */
function __AbstractMotionPathController() {}
__AbstractMotionPathController.prototype = /** @type {!_AbstractMotionPathController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathController}
 */
class _AbstractMotionPathController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractMotionPathController} ‎
 */
export class AbstractMotionPathController extends newAbstract(
 _AbstractMotionPathController,320998062719,null,{
  asIMotionPathController:1,
  superMotionPathController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathController} */
AbstractMotionPathController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathController} */
function AbstractMotionPathControllerClass(){}


AbstractMotionPathController[$implementations]=[
 AbstractMotionPathControllerClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IMotionPathPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractMotionPathController,
 MotionPathBuffer,
 IntegratedController,
 /**@type {!AbstractMotionPathController}*/(MotionPathPortConnector),
]


export default AbstractMotionPathController