import {makeBuffers} from '@webcircuits/webcircuits'

export const MotionPathBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  gap:Number,
  count:Number,
 }),
})

export default MotionPathBuffer