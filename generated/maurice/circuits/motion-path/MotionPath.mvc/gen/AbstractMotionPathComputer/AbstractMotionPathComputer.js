import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathComputer}
 */
function __AbstractMotionPathComputer() {}
__AbstractMotionPathComputer.prototype = /** @type {!_AbstractMotionPathComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathComputer}
 */
class _AbstractMotionPathComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractMotionPathComputer} ‎
 */
export class AbstractMotionPathComputer extends newAbstract(
 _AbstractMotionPathComputer,32099806271,null,{
  asIMotionPathComputer:1,
  superMotionPathComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathComputer} */
AbstractMotionPathComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathComputer} */
function AbstractMotionPathComputerClass(){}


AbstractMotionPathComputer[$implementations]=[
 __AbstractMotionPathComputer,
 Adapter,
]


export default AbstractMotionPathComputer