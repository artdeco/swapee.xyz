import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathControllerAT}
 */
function __AbstractMotionPathControllerAT() {}
__AbstractMotionPathControllerAT.prototype = /** @type {!_AbstractMotionPathControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractMotionPathControllerAT}
 */
class _AbstractMotionPathControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IMotionPathControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractMotionPathControllerAT} ‎
 */
class AbstractMotionPathControllerAT extends newAbstract(
 _AbstractMotionPathControllerAT,320998062723,null,{
  asIMotionPathControllerAT:1,
  superMotionPathControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractMotionPathControllerAT} */
AbstractMotionPathControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractMotionPathControllerAT} */
function AbstractMotionPathControllerATClass(){}

export default AbstractMotionPathControllerAT


AbstractMotionPathControllerAT[$implementations]=[
 __AbstractMotionPathControllerAT,
 UartUniversal,
 AbstractMotionPathControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IMotionPathControllerAT}*/({
  get asIMotionPathController(){
   return this
  },
 }),
]