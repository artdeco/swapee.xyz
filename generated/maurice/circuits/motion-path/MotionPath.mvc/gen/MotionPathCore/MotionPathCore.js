import {mountPins} from '@webcircuits/webcircuits'
import {MotionPathMemoryPQs} from '../../pqs/MotionPathMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_MotionPathCore}
 */
function __MotionPathCore() {}
__MotionPathCore.prototype = /** @type {!_MotionPathCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathCore}
 */
class _MotionPathCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractMotionPathCore} ‎
 */
class MotionPathCore extends newAbstract(
 _MotionPathCore,32099806277,null,{
  asIMotionPathCore:1,
  superMotionPathCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathCore} */
MotionPathCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathCore} */
function MotionPathCoreClass(){}

export default MotionPathCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_MotionPathOuterCore}
 */
function __MotionPathOuterCore() {}
__MotionPathOuterCore.prototype = /** @type {!_MotionPathOuterCore} */ ({ })
/** @this {xyz.swapee.wc.MotionPathOuterCore} */
export function MotionPathOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IMotionPathOuterCore.Model}*/
  this.model={
    gap: 0.035,
    count: 12,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathOuterCore}
 */
class _MotionPathOuterCore { }
/**
 * The _IMotionPath_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractMotionPathOuterCore} ‎
 */
export class MotionPathOuterCore extends newAbstract(
 _MotionPathOuterCore,32099806273,MotionPathOuterCoreConstructor,{
  asIMotionPathOuterCore:1,
  superMotionPathOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathOuterCore} */
MotionPathOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathOuterCore} */
function MotionPathOuterCoreClass(){}


MotionPathOuterCore[$implementations]=[
 __MotionPathOuterCore,
 MotionPathOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathOuterCore}*/({
  constructor(){
   mountPins(this.model,'',MotionPathMemoryPQs)

  },
 }),
]

MotionPathCore[$implementations]=[
 MotionPathCoreClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathCore}*/({
  resetCore(){
   this.resetMotionPathCore()
  },
  resetMotionPathCore(){
   MotionPathOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.MotionPathOuterCore}*/(
     /**@type {!xyz.swapee.wc.IMotionPathOuterCore}*/(this)),
   )
  },
 }),
 __MotionPathCore,
 MotionPathOuterCore,
]

export {MotionPathCore}