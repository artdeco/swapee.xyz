import AbstractMotionPathScreenAT from '../AbstractMotionPathScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathScreenBack}
 */
function __AbstractMotionPathScreenBack() {}
__AbstractMotionPathScreenBack.prototype = /** @type {!_AbstractMotionPathScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractMotionPathScreen}
 */
class _AbstractMotionPathScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractMotionPathScreen} ‎
 */
class AbstractMotionPathScreenBack extends newAbstract(
 _AbstractMotionPathScreenBack,320998062725,null,{
  asIMotionPathScreen:1,
  superMotionPathScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreen} */
AbstractMotionPathScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathScreen} */
function AbstractMotionPathScreenBackClass(){}

export default AbstractMotionPathScreenBack


AbstractMotionPathScreenBack[$implementations]=[
 __AbstractMotionPathScreenBack,
 AbstractMotionPathScreenAT,
]