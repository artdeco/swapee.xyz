import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_MotionPathElementPort}
 */
function __MotionPathElementPort() {}
__MotionPathElementPort.prototype = /** @type {!_MotionPathElementPort} */ ({ })
/** @this {xyz.swapee.wc.MotionPathElementPort} */ function MotionPathElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IMotionPathElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    particlesWrOpts: {},
    particlesWr2Opts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathElementPort}
 */
class _MotionPathElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractMotionPathElementPort} ‎
 */
class MotionPathElementPort extends newAbstract(
 _MotionPathElementPort,320998062713,MotionPathElementPortConstructor,{
  asIMotionPathElementPort:1,
  superMotionPathElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathElementPort} */
MotionPathElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathElementPort} */
function MotionPathElementPortClass(){}

export default MotionPathElementPort


MotionPathElementPort[$implementations]=[
 __MotionPathElementPort,
 MotionPathElementPortClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'particles-wr-opts':undefined,
    'particles-wr2-opts':undefined,
   })
  },
 }),
]