import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathControllerAR}
 */
function __AbstractMotionPathControllerAR() {}
__AbstractMotionPathControllerAR.prototype = /** @type {!_AbstractMotionPathControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractMotionPathControllerAR}
 */
class _AbstractMotionPathControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IMotionPathControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractMotionPathControllerAR} ‎
 */
class AbstractMotionPathControllerAR extends newAbstract(
 _AbstractMotionPathControllerAR,320998062722,null,{
  asIMotionPathControllerAR:1,
  superMotionPathControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathControllerAR} */
AbstractMotionPathControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathControllerAR} */
function AbstractMotionPathControllerARClass(){}

export default AbstractMotionPathControllerAR


AbstractMotionPathControllerAR[$implementations]=[
 __AbstractMotionPathControllerAR,
 AR,
 AbstractMotionPathControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IMotionPathControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]