import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {MotionPathInputsPQs} from '../../pqs/MotionPathInputsPQs'
import {MotionPathOuterCoreConstructor} from '../MotionPathCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_MotionPathPort}
 */
function __MotionPathPort() {}
__MotionPathPort.prototype = /** @type {!_MotionPathPort} */ ({ })
/** @this {xyz.swapee.wc.MotionPathPort} */ function MotionPathPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.MotionPathOuterCore} */ ({model:null})
  MotionPathOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathPort}
 */
class _MotionPathPort { }
/**
 * The port that serves as an interface to the _IMotionPath_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractMotionPathPort} ‎
 */
export class MotionPathPort extends newAbstract(
 _MotionPathPort,32099806275,MotionPathPortConstructor,{
  asIMotionPathPort:1,
  superMotionPathPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathPort} */
MotionPathPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathPort} */
function MotionPathPortClass(){}

export const MotionPathPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IMotionPath.Pinout>}*/({
 get Port() { return MotionPathPort },
})

MotionPathPort[$implementations]=[
 MotionPathPortClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathPort}*/({
  resetPort(){
   this.resetMotionPathPort()
  },
  resetMotionPathPort(){
   MotionPathPortConstructor.call(this)
  },
 }),
 __MotionPathPort,
 Parametric,
 MotionPathPortClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathPort}*/({
  constructor(){
   mountPins(this.inputs,'',MotionPathInputsPQs)
  },
 }),
]


export default MotionPathPort