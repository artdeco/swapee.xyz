import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathDisplay}
 */
function __AbstractMotionPathDisplay() {}
__AbstractMotionPathDisplay.prototype = /** @type {!_AbstractMotionPathDisplay} */ ({ })
/** @this {xyz.swapee.wc.MotionPathDisplay} */ function MotionPathDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.ParticlesWr=null
  /** @type {HTMLDivElement} */ this.ParticlesWr2=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathDisplay}
 */
class _AbstractMotionPathDisplay { }
/**
 * Display for presenting information from the _IMotionPath_.
 * @extends {xyz.swapee.wc.AbstractMotionPathDisplay} ‎
 */
class AbstractMotionPathDisplay extends newAbstract(
 _AbstractMotionPathDisplay,320998062716,MotionPathDisplayConstructor,{
  asIMotionPathDisplay:1,
  superMotionPathDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathDisplay} */
AbstractMotionPathDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathDisplay} */
function AbstractMotionPathDisplayClass(){}

export default AbstractMotionPathDisplay


AbstractMotionPathDisplay[$implementations]=[
 __AbstractMotionPathDisplay,
 Display,
 AbstractMotionPathDisplayClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIMotionPathScreen:{vdusPQs:{
    ParticlesWr:ParticlesWr,
    ParticlesWr2:ParticlesWr2,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    ParticlesWr:/**@type {HTMLDivElement}*/(children[ParticlesWr]),
    ParticlesWr2:/**@type {HTMLDivElement}*/(children[ParticlesWr2]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.MotionPathDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IMotionPathDisplay.Initialese}*/({
   ParticlesWr:1,
   ParticlesWr2:1,
  }),
  initializer({
   ParticlesWr:_ParticlesWr,
   ParticlesWr2:_ParticlesWr2,
  }) {
   if(_ParticlesWr!==undefined) this.ParticlesWr=_ParticlesWr
   if(_ParticlesWr2!==undefined) this.ParticlesWr2=_ParticlesWr2
  },
 }),
]