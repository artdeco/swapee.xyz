import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathProcessor}
 */
function __AbstractMotionPathProcessor() {}
__AbstractMotionPathProcessor.prototype = /** @type {!_AbstractMotionPathProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathProcessor}
 */
class _AbstractMotionPathProcessor { }
/**
 * The processor to compute changes to the memory for the _IMotionPath_.
 * @extends {xyz.swapee.wc.AbstractMotionPathProcessor} ‎
 */
class AbstractMotionPathProcessor extends newAbstract(
 _AbstractMotionPathProcessor,32099806278,null,{
  asIMotionPathProcessor:1,
  superMotionPathProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathProcessor} */
AbstractMotionPathProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathProcessor} */
function AbstractMotionPathProcessorClass(){}

export default AbstractMotionPathProcessor


AbstractMotionPathProcessor[$implementations]=[
 __AbstractMotionPathProcessor,
]