import AbstractMotionPathProcessor from '../AbstractMotionPathProcessor'
import {MotionPathCore} from '../MotionPathCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractMotionPathComputer} from '../AbstractMotionPathComputer'
import {AbstractMotionPathController} from '../AbstractMotionPathController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPath}
 */
function __AbstractMotionPath() {}
__AbstractMotionPath.prototype = /** @type {!_AbstractMotionPath} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPath}
 */
class _AbstractMotionPath { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractMotionPath} ‎
 */
class AbstractMotionPath extends newAbstract(
 _AbstractMotionPath,32099806279,null,{
  asIMotionPath:1,
  superMotionPath:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPath} */
AbstractMotionPath.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPath} */
function AbstractMotionPathClass(){}

export default AbstractMotionPath


AbstractMotionPath[$implementations]=[
 __AbstractMotionPath,
 MotionPathCore,
 AbstractMotionPathProcessor,
 IntegratedComponent,
 AbstractMotionPathComputer,
 AbstractMotionPathController,
]


export {AbstractMotionPath}