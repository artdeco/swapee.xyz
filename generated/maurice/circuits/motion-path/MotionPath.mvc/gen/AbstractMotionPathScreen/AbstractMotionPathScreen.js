import AbstractMotionPathScreenAR from '../AbstractMotionPathScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {MotionPathInputsPQs} from '../../pqs/MotionPathInputsPQs'
import {MotionPathMemoryQPs} from '../../pqs/MotionPathMemoryQPs'
import {MotionPathVdusPQs} from '../../pqs/MotionPathVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathScreen}
 */
function __AbstractMotionPathScreen() {}
__AbstractMotionPathScreen.prototype = /** @type {!_AbstractMotionPathScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathScreen}
 */
class _AbstractMotionPathScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractMotionPathScreen} ‎
 */
class AbstractMotionPathScreen extends newAbstract(
 _AbstractMotionPathScreen,320998062724,null,{
  asIMotionPathScreen:1,
  superMotionPathScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathScreen} */
AbstractMotionPathScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathScreen} */
function AbstractMotionPathScreenClass(){}

export default AbstractMotionPathScreen


AbstractMotionPathScreen[$implementations]=[
 __AbstractMotionPathScreen,
 AbstractMotionPathScreenClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathScreen}*/({
  inputsPQs:MotionPathInputsPQs,
  memoryQPs:MotionPathMemoryQPs,
 }),
 Screen,
 AbstractMotionPathScreenAR,
 AbstractMotionPathScreenClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathScreen}*/({
  vdusPQs:MotionPathVdusPQs,
 }),
]