import AbstractMotionPathDisplay from '../AbstractMotionPathDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {MotionPathVdusPQs} from '../../pqs/MotionPathVdusPQs'
import {MotionPathVdusQPs} from '../../pqs/MotionPathVdusQPs'
import {MotionPathMemoryPQs} from '../../pqs/MotionPathMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathGPU}
 */
function __AbstractMotionPathGPU() {}
__AbstractMotionPathGPU.prototype = /** @type {!_AbstractMotionPathGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathGPU}
 */
class _AbstractMotionPathGPU { }
/**
 * Handles the periphery of the _IMotionPathDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractMotionPathGPU} ‎
 */
class AbstractMotionPathGPU extends newAbstract(
 _AbstractMotionPathGPU,320998062715,null,{
  asIMotionPathGPU:1,
  superMotionPathGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathGPU} */
AbstractMotionPathGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathGPU} */
function AbstractMotionPathGPUClass(){}

export default AbstractMotionPathGPU


AbstractMotionPathGPU[$implementations]=[
 __AbstractMotionPathGPU,
 AbstractMotionPathGPUClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathGPU}*/({
  vdusPQs:MotionPathVdusPQs,
  vdusQPs:MotionPathVdusQPs,
  memoryPQs:MotionPathMemoryPQs,
 }),
 AbstractMotionPathDisplay,
 BrowserView,
]