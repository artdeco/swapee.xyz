import AbstractMotionPathControllerAR from '../AbstractMotionPathControllerAR'
import {AbstractMotionPathController} from '../AbstractMotionPathController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathControllerBack}
 */
function __AbstractMotionPathControllerBack() {}
__AbstractMotionPathControllerBack.prototype = /** @type {!_AbstractMotionPathControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractMotionPathController}
 */
class _AbstractMotionPathControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractMotionPathController} ‎
 */
class AbstractMotionPathControllerBack extends newAbstract(
 _AbstractMotionPathControllerBack,320998062721,null,{
  asIMotionPathController:1,
  superMotionPathController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathController} */
AbstractMotionPathControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathController} */
function AbstractMotionPathControllerBackClass(){}

export default AbstractMotionPathControllerBack


AbstractMotionPathControllerBack[$implementations]=[
 __AbstractMotionPathControllerBack,
 AbstractMotionPathController,
 AbstractMotionPathControllerAR,
 DriverBack,
]