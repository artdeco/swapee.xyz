import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathScreenAR}
 */
function __AbstractMotionPathScreenAR() {}
__AbstractMotionPathScreenAR.prototype = /** @type {!_AbstractMotionPathScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractMotionPathScreenAR}
 */
class _AbstractMotionPathScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IMotionPathScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractMotionPathScreenAR} ‎
 */
class AbstractMotionPathScreenAR extends newAbstract(
 _AbstractMotionPathScreenAR,320998062726,null,{
  asIMotionPathScreenAR:1,
  superMotionPathScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractMotionPathScreenAR} */
AbstractMotionPathScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractMotionPathScreenAR} */
function AbstractMotionPathScreenARClass(){}

export default AbstractMotionPathScreenAR


AbstractMotionPathScreenAR[$implementations]=[
 __AbstractMotionPathScreenAR,
 AR,
 AbstractMotionPathScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IMotionPathScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractMotionPathScreenAR}