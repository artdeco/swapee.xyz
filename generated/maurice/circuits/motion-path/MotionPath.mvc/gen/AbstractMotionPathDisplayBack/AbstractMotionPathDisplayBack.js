import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathDisplay}
 */
function __AbstractMotionPathDisplay() {}
__AbstractMotionPathDisplay.prototype = /** @type {!_AbstractMotionPathDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractMotionPathDisplay}
 */
class _AbstractMotionPathDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractMotionPathDisplay} ‎
 */
class AbstractMotionPathDisplay extends newAbstract(
 _AbstractMotionPathDisplay,320998062718,null,{
  asIMotionPathDisplay:1,
  superMotionPathDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathDisplay} */
AbstractMotionPathDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractMotionPathDisplay} */
function AbstractMotionPathDisplayClass(){}

export default AbstractMotionPathDisplay


AbstractMotionPathDisplay[$implementations]=[
 __AbstractMotionPathDisplay,
 GraphicsDriverBack,
 AbstractMotionPathDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IMotionPathDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IMotionPathDisplay}*/({
    ParticlesWr:twinMock,
    ParticlesWr2:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.MotionPathDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IMotionPathDisplay.Initialese}*/({
   ParticlesWr:1,
   ParticlesWr2:1,
  }),
  initializer({
   ParticlesWr:_ParticlesWr,
   ParticlesWr2:_ParticlesWr2,
  }) {
   if(_ParticlesWr!==undefined) this.ParticlesWr=_ParticlesWr
   if(_ParticlesWr2!==undefined) this.ParticlesWr2=_ParticlesWr2
  },
 }),
]