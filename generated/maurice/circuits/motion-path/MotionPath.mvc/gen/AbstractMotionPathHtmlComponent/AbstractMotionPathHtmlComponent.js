import AbstractMotionPathGPU from '../AbstractMotionPathGPU'
import AbstractMotionPathScreenBack from '../AbstractMotionPathScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {MotionPathInputsQPs} from '../../pqs/MotionPathInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractMotionPath from '../AbstractMotionPath'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractMotionPathHtmlComponent}
 */
function __AbstractMotionPathHtmlComponent() {}
__AbstractMotionPathHtmlComponent.prototype = /** @type {!_AbstractMotionPathHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractMotionPathHtmlComponent}
 */
class _AbstractMotionPathHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.MotionPathHtmlComponent} */ (res)
  }
}
/**
 * The _IMotionPath_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractMotionPathHtmlComponent} ‎
 */
export class AbstractMotionPathHtmlComponent extends newAbstract(
 _AbstractMotionPathHtmlComponent,320998062711,null,{
  asIMotionPathHtmlComponent:1,
  superMotionPathHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractMotionPathHtmlComponent} */
AbstractMotionPathHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractMotionPathHtmlComponent} */
function AbstractMotionPathHtmlComponentClass(){}


AbstractMotionPathHtmlComponent[$implementations]=[
 __AbstractMotionPathHtmlComponent,
 HtmlComponent,
 AbstractMotionPath,
 AbstractMotionPathGPU,
 AbstractMotionPathScreenBack,
 AbstractMotionPathHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IMotionPathHtmlComponent}*/({
  inputsQPs:MotionPathInputsQPs,
 }),
]