/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice': {
  'id': 1,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice': {
  'id': 2,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel': {
  'id': 3,
  'symbols': {},
  'methods': {
   'beforeAtInputFocus': 1,
   'afterAtInputFocus': 2,
   'afterAtInputFocusThrows': 3,
   'afterAtInputFocusReturns': 4,
   'afterAtInputFocusCancels': 5,
   'beforeAtInputBlur': 6,
   'afterAtInputBlur': 7,
   'afterAtInputBlurThrows': 8,
   'afterAtInputBlurReturns': 9,
   'afterAtInputBlurCancels': 10,
   'beforePulseCreateTransaction': 11,
   'afterPulseCreateTransaction': 12,
   'afterPulseCreateTransactionThrows': 13,
   'afterPulseCreateTransactionReturns': 14,
   'afterPulseCreateTransactionCancels': 15,
   'beforeAtAddressPopupClick': 16,
   'afterAtAddressPopupClick': 17,
   'afterAtAddressPopupClickThrows': 18,
   'afterAtAddressPopupClickReturns': 19,
   'afterAtAddressPopupClickCancels': 20,
   'beforeAtRefundPopupClick': 21,
   'afterAtRefundPopupClick': 22,
   'afterAtRefundPopupClickThrows': 23,
   'afterAtRefundPopupClickReturns': 24,
   'afterAtRefundPopupClickCancels': 25,
   'beforePulseCreateChangeNowTransaction': 26,
   'afterPulseCreateChangeNowTransaction': 27,
   'afterPulseCreateChangeNowTransactionThrows': 28,
   'afterPulseCreateChangeNowTransactionReturns': 29,
   'afterPulseCreateChangeNowTransactionCancels': 30
  }
 },
 'xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller': {
  'id': 4,
  'symbols': {},
  'methods': {
   'atInputFocus': 1,
   'atInputBlur': 2,
   'pulseCreateTransaction': 3,
   'atAddressPopupClick': 4,
   'atRefundPopupClick': 5,
   'pulseCreateChangeNowTransaction': 6
  }
 },
 'xyz.swapee.wc.BChangellyExchangeInterruptLineAspects': {
  'id': 5,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeInterruptLineAspects': {
  'id': 6,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IHyperChangellyExchangeInterruptLine': {
  'id': 7,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeInterruptLine': {
  'id': 8,
  'symbols': {},
  'methods': {
   'atInputFocus': 1,
   'atInputBlur': 2,
   'pulseCreateTransaction': 3,
   'atAddressPopupClick': 4,
   'atRefundPopupClick': 5,
   'pulseCreateChangeNowTransaction': 6
  }
 }
})