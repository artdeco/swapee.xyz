/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @record */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineCaster filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.asIChangellyExchangeInterruptLine
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeTouchscreen} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.asIChangellyExchangeTouchscreen
/** @type {!xyz.swapee.wc.BoundChangellyExchangeInterruptLine} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.superChangellyExchangeInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineCaster}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeController}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLine = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atInputFocus = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atInputBlur = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atAddressPopupClick = function() {}
/** @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atRefundPopupClick = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLine
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLine)} */
xyz.swapee.wc.ChangellyExchangeInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.AbstractChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
$xyz.swapee.wc.AbstractChangellyExchangeInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeInterruptLine)} */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.beforeAtInputFocus
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputFocus
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputFocusThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputFocusReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputFocusCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.beforeAtInputBlur
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputBlur
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputBlurThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputBlurReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtInputBlurCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.beforeAtAddressPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtAddressPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtAddressPopupClickThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtAddressPopupClickReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtAddressPopupClickCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.beforeAtRefundPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtRefundPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtRefundPopupClickThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtRefundPopupClickReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.afterAtRefundPopupClickCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice}
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice = function() {}
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAtInputFocus
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputFocus
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputFocusThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputFocusReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputFocusCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAtInputBlur
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputBlur
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputBlurThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputBlurReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtInputBlurCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAtAddressPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAddressPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAddressPopupClickThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAddressPopupClickReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtAddressPopupClickCancels
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.beforeAtRefundPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtRefundPopupClick
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtRefundPopupClickThrows
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtRefundPopupClickReturns
/** @type {(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels<THIS>>)} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.afterAtRefundPopupClickCancels
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @template THIS
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtInputFocus = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocus = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtInputBlur = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlur = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtAddressPopupClick = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClick = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickCancels = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtRefundPopupClick = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClick = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickThrows = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickReturns = function(data) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickCancels = function(data) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.RecordIChangellyExchangeInterruptLineJoinpointModel filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ beforeAtInputFocus: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus, afterAtInputFocus: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus, afterAtInputFocusThrows: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows, afterAtInputFocusReturns: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns, afterAtInputFocusCancels: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels, beforeAtInputBlur: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur, afterAtInputBlur: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur, afterAtInputBlurThrows: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows, afterAtInputBlurReturns: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns, afterAtInputBlurCancels: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels, beforeAtAddressPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick, afterAtAddressPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick, afterAtAddressPopupClickThrows: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows, afterAtAddressPopupClickReturns: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns, afterAtAddressPopupClickCancels: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels, beforeAtRefundPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick, afterAtRefundPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick, afterAtRefundPopupClickThrows: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows, afterAtRefundPopupClickReturns: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns, afterAtRefundPopupClickCancels: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels }} */
xyz.swapee.wc.RecordIChangellyExchangeInterruptLineJoinpointModel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeInterruptLineJoinpointModel}
 */
$xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundChangellyExchangeInterruptLineJoinpointModel filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeInterruptLineJoinpointModel = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeInterruptLineJoinpointModel} */
xyz.swapee.wc.BoundChangellyExchangeInterruptLineJoinpointModel

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData} [data]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels = function(data) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel, !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData=): void} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @record */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller = function() {}
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.beforeAtInputFocus
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputFocus
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputFocusThrows
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputFocusReturns
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputFocusCancels
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.beforeAtInputBlur
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputBlur
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputBlurThrows
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputBlurReturns
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtInputBlurCancels
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.beforeAtAddressPopupClick
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtAddressPopupClick
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtAddressPopupClickThrows
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtAddressPopupClickReturns
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtAddressPopupClickCancels
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.beforeAtRefundPopupClick
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtRefundPopupClick
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtRefundPopupClickThrows
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtRefundPopupClickReturns
/** @type {number} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.afterAtRefundPopupClickCancels
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.atInputFocus = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.atInputBlur = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.atAddressPopupClick = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.atRefundPopupClick = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
$xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller)} */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.__extend
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.continues
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstallerConstructor filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller, ...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstallerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ ticket: symbol, proc: !Function }} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData = function() {}
/** @type {!function(!Object<string, *>): void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData.prototype.cond
/**
 * @param {string} [reason]
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData.prototype.cancel = function(reason) {}
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData.prototype.res
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData = function() {}
/** @type {!Error} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData.prototype.err
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData.prototype.hide = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData = function() {}
/** @type {!xyz.swapee.wc.front.ChangellyExchangeInputs} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData.prototype.res
/**
 * @param {!xyz.swapee.wc.front.ChangellyExchangeInputs} value
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData.prototype.sub = function(value) {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData = function() {}
/** @type {!Set<string>} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData.prototype.reasons
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @template THIS
 */
$xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine} */
$xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster.prototype.asIChangellyExchangeInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster<THIS>}
 */
xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BChangellyExchangeInterruptLineAspects filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @extends {xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster<THIS>}
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 * @template THIS
 */
$xyz.swapee.wc.BChangellyExchangeInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @template THIS
 * @extends {$xyz.swapee.wc.BChangellyExchangeInterruptLineAspects<THIS>}
 */
xyz.swapee.wc.BChangellyExchangeInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @record */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeInterruptLineAspects
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine} */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster.prototype.asIChangellyExchangeInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLineAspects filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster}
 * @extends {xyz.swapee.wc.BChangellyExchangeInterruptLineAspects<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects>}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLineAspects = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspects

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineAspects filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineAspects}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspects, ...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
$xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects)} */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsConstructor filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspects, ...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese)} */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese}
 */
$xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IHyperChangellyExchangeInterruptLine
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @interface */
$xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine} */
$xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster.prototype.asIHyperChangellyExchangeInterruptLine
/** @type {!xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine} */
$xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster.prototype.superHyperChangellyExchangeInterruptLine
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster}
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IHyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLine}
 */
$xyz.swapee.wc.IHyperChangellyExchangeInterruptLine = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.HyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} init
 * @implements {xyz.swapee.wc.IHyperChangellyExchangeInterruptLine}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese>}
 */
$xyz.swapee.wc.HyperChangellyExchangeInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine
/** @type {function(new: xyz.swapee.wc.IHyperChangellyExchangeInterruptLine, ...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese)} */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @constructor
 * @extends {xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
$xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine
/** @type {function(new: xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine)} */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.__extend
/**
 * @param {...((!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.continues
/**
 * @param {...((!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine))} Implementations
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.__trait
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspects))} aides
 * @return {typeof xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.consults

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.HyperChangellyExchangeInterruptLineConstructor filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {function(new: xyz.swapee.wc.IHyperChangellyExchangeInterruptLine, ...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese)} */
xyz.swapee.wc.HyperChangellyExchangeInterruptLineConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.RecordIHyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIHyperChangellyExchangeInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIHyperChangellyExchangeInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster}
 */
$xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine} */
xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine} */
xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.RecordIChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {{ atInputFocus: xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus, atInputBlur: xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur, atAddressPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick, atRefundPopupClick: xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick }} */
xyz.swapee.wc.RecordIChangellyExchangeInterruptLine

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundIChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeInterruptLine}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineCaster}
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeController}
 */
$xyz.swapee.wc.BoundIChangellyExchangeInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeInterruptLine} */
xyz.swapee.wc.BoundIChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.BoundChangellyExchangeInterruptLine filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeInterruptLine}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeInterruptLine = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeInterruptLine} */
xyz.swapee.wc.BoundChangellyExchangeInterruptLine

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputFocus = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLine): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine._atInputFocus
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputFocus

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLine,$xyz.swapee.wc.IChangellyExchangeInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputBlur = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLine): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine._atInputBlur
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputBlur

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLine,$xyz.swapee.wc.IChangellyExchangeInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.__atAddressPopupClick = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLine): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine._atAddressPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLine.__atAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.__atAddressPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLine,$xyz.swapee.wc.IChangellyExchangeInterruptLine,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/**
 * @this {THIS}
 * @template THIS
 * @return {(undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)}
 */
$xyz.swapee.wc.IChangellyExchangeInterruptLine.__atRefundPopupClick = function() {}
/** @typedef {function(): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeInterruptLine): (undefined|!xyz.swapee.wc.front.ChangellyExchangeInputs)} */
xyz.swapee.wc.IChangellyExchangeInterruptLine._atRefundPopupClick
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeInterruptLine.__atRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.__atRefundPopupClick

// nss:xyz.swapee.wc.IChangellyExchangeInterruptLine,$xyz.swapee.wc.IChangellyExchangeInterruptLine,xyz.swapee.wc
/* @typal-end */