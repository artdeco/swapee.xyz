/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IChangellyExchangeInterruptLine={}
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller={}
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel={}
xyz.swapee.wc.IChangellyExchangeInterruptLineAspects={}
xyz.swapee.wc.IHyperChangellyExchangeInterruptLine={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/parts/120-interrupt-line/types/design/120-IChangellyExchangeInterruptLine.xml} filter:!ControllerPlugin~props 2b13881baedde719655706c9b5e3b2fe */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ChangellyExchangeInterruptLine)} xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine} xyz.swapee.wc.ChangellyExchangeInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractChangellyExchangeInterruptLine
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.constructor&xyz.swapee.wc.ChangellyExchangeInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractChangellyExchangeInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLine.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputFocus=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputFocus=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputFocusThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputFocusReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputFocusCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputBlur=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputBlur=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputBlurThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputBlurReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputBlurCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAddressPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAddressPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAddressPopupClickThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAddressPopupClickReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAddressPopupClickCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtRefundPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtRefundPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtRefundPopupClickThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtRefundPopupClickReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtRefundPopupClickCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels>} */ (void 0)
  }
}
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice

/**
 * A concrete class of _IChangellyExchangeInterruptLineJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice = class extends xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelHyperslice { }
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputFocus=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputFocus=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputFocusThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputFocusReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputFocusCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtInputBlur=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtInputBlur=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtInputBlurThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtInputBlurReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtInputBlurCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtAddressPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtAddressPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtAddressPopupClickThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtAddressPopupClickReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtAddressPopupClickCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<THIS>>} */ (void 0)
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeAtRefundPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterAtRefundPopupClick=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterAtRefundPopupClickThrows=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterAtRefundPopupClickReturns=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterAtRefundPopupClickCancels=/** @type {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice

/**
 * A concrete class of _IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice { }
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IChangellyExchangeInterruptLine`'s methods.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel = class { }
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtInputFocus = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocus = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusThrows = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusReturns = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputFocusCancels = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtInputBlur = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlur = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurThrows = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurReturns = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtInputBlurCancels = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickThrows = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickReturns = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtAddressPopupClickCancels = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.beforeAtRefundPopupClick = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClick = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickThrows = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickReturns = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels} */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.prototype.afterAtRefundPopupClickCancels = function() {}

/**
 * A concrete class of _IChangellyExchangeInterruptLineJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel} An interface that enumerates the joinpoints of `IChangellyExchangeInterruptLine`'s methods.
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel = class extends xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel { }
xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel.prototype.constructor = xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel} */
xyz.swapee.wc.RecordIChangellyExchangeInterruptLineJoinpointModel

/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel} xyz.swapee.wc.BoundIChangellyExchangeInterruptLineJoinpointModel */

/** @typedef {xyz.swapee.wc.ChangellyExchangeInterruptLineJoinpointModel} xyz.swapee.wc.BoundChangellyExchangeInterruptLineJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller)} xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese[]) => xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeAtInputFocus=/** @type {number} */ (void 0)
    this.afterAtInputFocus=/** @type {number} */ (void 0)
    this.afterAtInputFocusThrows=/** @type {number} */ (void 0)
    this.afterAtInputFocusReturns=/** @type {number} */ (void 0)
    this.afterAtInputFocusCancels=/** @type {number} */ (void 0)
    this.beforeAtInputBlur=/** @type {number} */ (void 0)
    this.afterAtInputBlur=/** @type {number} */ (void 0)
    this.afterAtInputBlurThrows=/** @type {number} */ (void 0)
    this.afterAtInputBlurReturns=/** @type {number} */ (void 0)
    this.afterAtInputBlurCancels=/** @type {number} */ (void 0)
    this.beforeAtAddressPopupClick=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClick=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickThrows=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickReturns=/** @type {number} */ (void 0)
    this.afterAtAddressPopupClickCancels=/** @type {number} */ (void 0)
    this.beforeAtRefundPopupClick=/** @type {number} */ (void 0)
    this.afterAtRefundPopupClick=/** @type {number} */ (void 0)
    this.afterAtRefundPopupClickThrows=/** @type {number} */ (void 0)
    this.afterAtRefundPopupClickReturns=/** @type {number} */ (void 0)
    this.afterAtRefundPopupClickCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {void}
   */
  atInputFocus() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  atInputBlur() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  atAddressPopupClick() { }
  /**
   * A joinpointer.
   * @return {void}
   */
  atRefundPopupClick() { }
}
/**
 * Create a new *IChangellyExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese>)} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller} xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IChangellyExchangeInterruptLineAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller = class extends /** @type {xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.constructor&xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangeInterruptLineAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atInputFocus` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => void} sub Cancels a call to `atInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atInputFocus` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atInputBlur` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => void} sub Cancels a call to `atInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atInputBlur` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atAddressPopupClick` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => void} sub Cancels a call to `atAddressPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atAddressPopupClick` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData The pointcut data passed to _afterCancels_ aspects. */

/**
 * @typedef {Object} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(reason?: string) => void} cancel Allows to stop the `atRefundPopupClick` method from being executed.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => void} sub Cancels a call to `atRefundPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData The pointcut data passed to _before_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `atRefundPopupClick` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData The pointcut data passed to _afterThrows_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData
 * @prop {!xyz.swapee.wc.front.ChangellyExchangeInputs} res The return of the method after it's successfully run.
 * @prop {(value: !xyz.swapee.wc.front.ChangellyExchangeInputs) => ?} sub Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {function(new: xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster<THIS>&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice<THIS>)} xyz.swapee.wc.BChangellyExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.typeof */
/**
 * The aspects of the *IChangellyExchangeInterruptLine* that bind to an instance.
 * @interface xyz.swapee.wc.BChangellyExchangeInterruptLineAspects
 * @template THIS
 */
xyz.swapee.wc.BChangellyExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.BChangellyExchangeInterruptLineAspects.constructor&xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModelBindingHyperslice.typeof} */ (class {}) { }
xyz.swapee.wc.BChangellyExchangeInterruptLineAspects.prototype.constructor = xyz.swapee.wc.BChangellyExchangeInterruptLineAspects

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)} xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects} xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeInterruptLineAspects` interface.
 * @constructor xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.constructor&xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.prototype.constructor = xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.class = /** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects)|(!xyz.swapee.wc.BChangellyExchangeInterruptLineAspects|typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.AbstractChangellyExchangeInterruptLineAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese[]) => xyz.swapee.wc.IChangellyExchangeInterruptLineAspects} xyz.swapee.wc.ChangellyExchangeInterruptLineAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster&xyz.swapee.wc.BChangellyExchangeInterruptLineAspects<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects>)} xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.BChangellyExchangeInterruptLineAspects} xyz.swapee.wc.BChangellyExchangeInterruptLineAspects.typeof */
/**
 * The aspects of the *IChangellyExchangeInterruptLine*.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineAspects
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.BChangellyExchangeInterruptLineAspects.typeof} */ (class {}) {
}
/**
 * Create a new *IChangellyExchangeInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspects&engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese>)} xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineAspects} xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.typeof */
/**
 * A concrete class of _IChangellyExchangeInterruptLineAspects_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLineAspects
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLineAspects} The aspects of the *IChangellyExchangeInterruptLine*.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese>} ‎
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects = class extends /** @type {xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.constructor&xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IChangellyExchangeInterruptLineAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IChangellyExchangeInterruptLineAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLineAspects}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLineAspects.__extend = function(...Extensions) {}

/**
 * Contains getters to cast the _BChangellyExchangeInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster
 * @template THIS
 */
xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster = class { }
/**
 * Cast the _BChangellyExchangeInterruptLineAspects_ instance into the _BoundIChangellyExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.BChangellyExchangeInterruptLineAspectsCaster.prototype.asIChangellyExchangeInterruptLine

/**
 * Contains getters to cast the _IChangellyExchangeInterruptLineAspects_ interface.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster = class { }
/**
 * Cast the _IChangellyExchangeInterruptLineAspects_ instance into the _BoundIChangellyExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineAspectsCaster.prototype.asIChangellyExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese} xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperChangellyExchangeInterruptLine)} xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine} xyz.swapee.wc.HyperChangellyExchangeInterruptLine.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperChangellyExchangeInterruptLine` interface.
 * @constructor xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.constructor&xyz.swapee.wc.HyperChangellyExchangeInterruptLine.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.class = /** @type {typeof xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine|typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine)|(!xyz.swapee.wc.IChangellyExchangeInterruptLine|typeof xyz.swapee.wc.ChangellyExchangeInterruptLine)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.IChangellyExchangeInterruptLineAspects|function(new: xyz.swapee.wc.IChangellyExchangeInterruptLineAspects)} aides The list of aides that advise the IChangellyExchangeInterruptLine to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperChangellyExchangeInterruptLine.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese[]) => xyz.swapee.wc.IHyperChangellyExchangeInterruptLine} xyz.swapee.wc.HyperChangellyExchangeInterruptLineConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster&xyz.swapee.wc.IChangellyExchangeInterruptLine)} xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLine} xyz.swapee.wc.IChangellyExchangeInterruptLine.typeof */
/** @interface xyz.swapee.wc.IHyperChangellyExchangeInterruptLine */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IChangellyExchangeInterruptLine.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperChangellyExchangeInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperChangellyExchangeInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese>)} xyz.swapee.wc.HyperChangellyExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperChangellyExchangeInterruptLine} xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.typeof */
/**
 * A concrete class of _IHyperChangellyExchangeInterruptLine_ instances.
 * @constructor xyz.swapee.wc.HyperChangellyExchangeInterruptLine
 * @implements {xyz.swapee.wc.IHyperChangellyExchangeInterruptLine} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.HyperChangellyExchangeInterruptLine.constructor&xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperChangellyExchangeInterruptLine* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperChangellyExchangeInterruptLine* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperChangellyExchangeInterruptLine.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.HyperChangellyExchangeInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperChangellyExchangeInterruptLine} */
xyz.swapee.wc.RecordIHyperChangellyExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IHyperChangellyExchangeInterruptLine} xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine */

/** @typedef {xyz.swapee.wc.HyperChangellyExchangeInterruptLine} xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine */

/**
 * Contains getters to cast the _IHyperChangellyExchangeInterruptLine_ interface.
 * @interface xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster = class { }
/**
 * Cast the _IHyperChangellyExchangeInterruptLine_ instance into the _BoundIHyperChangellyExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster.prototype.asIHyperChangellyExchangeInterruptLine
/**
 * Access the _HyperChangellyExchangeInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IHyperChangellyExchangeInterruptLineCaster.prototype.superHyperChangellyExchangeInterruptLine

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IChangellyExchangeInterruptLineCaster&xyz.swapee.wc.front.IChangellyExchangeController)} xyz.swapee.wc.IChangellyExchangeInterruptLine.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IChangellyExchangeController} xyz.swapee.wc.front.IChangellyExchangeController.typeof */
/**
 * Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLine
 */
xyz.swapee.wc.IChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.IChangellyExchangeInterruptLine.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IChangellyExchangeController.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atInputFocus = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atInputBlur = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atAddressPopupClick = function() {}
/** @type {xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick} */
xyz.swapee.wc.IChangellyExchangeInterruptLine.prototype.atRefundPopupClick = function() {}

/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeInterruptLine&engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese>)} xyz.swapee.wc.ChangellyExchangeInterruptLine.constructor */
/**
 * A concrete class of _IChangellyExchangeInterruptLine_ instances.
 * @constructor xyz.swapee.wc.ChangellyExchangeInterruptLine
 * @implements {xyz.swapee.wc.IChangellyExchangeInterruptLine} Handles user's gestures on the touchscreen. Provides access to front events
 * which are otherwise not available to the controller on the back.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeInterruptLine.Initialese>} ‎
 */
xyz.swapee.wc.ChangellyExchangeInterruptLine = class extends /** @type {xyz.swapee.wc.ChangellyExchangeInterruptLine.constructor&xyz.swapee.wc.IChangellyExchangeInterruptLine.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ChangellyExchangeInterruptLine.prototype.constructor = xyz.swapee.wc.ChangellyExchangeInterruptLine
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.ChangellyExchangeInterruptLine.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine} */
xyz.swapee.wc.RecordIChangellyExchangeInterruptLine

/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine} xyz.swapee.wc.BoundIChangellyExchangeInterruptLine */

/** @typedef {xyz.swapee.wc.ChangellyExchangeInterruptLine} xyz.swapee.wc.BoundChangellyExchangeInterruptLine */

/**
 * Contains getters to cast the _IChangellyExchangeInterruptLine_ interface.
 * @interface xyz.swapee.wc.IChangellyExchangeInterruptLineCaster
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineCaster = class { }
/**
 * Cast the _IChangellyExchangeInterruptLine_ instance into the _BoundIChangellyExchangeInterruptLine_ type.
 * @type {!xyz.swapee.wc.BoundIChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.asIChangellyExchangeInterruptLine
/**
 * Cast the _IChangellyExchangeInterruptLine_ instance into the _.BoundIChangellyExchangeTouchscreen_ type.
 * @type {!xyz.swapee.wc.BoundIChangellyExchangeTouchscreen}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.asIChangellyExchangeTouchscreen
/**
 * Access the _ChangellyExchangeInterruptLine_ prototype.
 * @type {!xyz.swapee.wc.BoundChangellyExchangeInterruptLine}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineCaster.prototype.superChangellyExchangeInterruptLine

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputFocus<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputFocus */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atInputFocus` method from being executed.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; void_ Cancels a call to `atInputFocus` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocus<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocus */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `after` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocus = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusThrows<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusThrows */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atInputFocus` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusReturns<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusReturns */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterReturns` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputFocusCancels<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputFocusCancels */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputFocusPointcutData} [data] Metadata passed to the pointcuts of _atInputFocus_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputFocusPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputFocusCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtInputBlur<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtInputBlur */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atInputBlur` method from being executed.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; void_ Cancels a call to `atInputBlur` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlur<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlur */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `after` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlur = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurThrows<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurThrows */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atInputBlur` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurReturns<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurReturns */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterReturns` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtInputBlurCancels<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtInputBlurCancels */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtInputBlurPointcutData} [data] Metadata passed to the pointcuts of _atInputBlur_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtInputBlurPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtInputBlurCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtAddressPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atAddressPopupClick` method from being executed.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; void_ Cancels a call to `atAddressPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtAddressPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `after` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickThrows<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickThrows */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atAddressPopupClick` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickReturns<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickReturns */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterReturns` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtAddressPopupClickCancels<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtAddressPopupClickCancels */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtAddressPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atAddressPopupClick_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtAddressPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtAddressPopupClickCancels = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__beforeAtRefundPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._beforeAtRefundPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.BeforeAtRefundPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atRefundPopupClick_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `atRefundPopupClick` method from being executed.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; void_ Cancels a call to `atRefundPopupClick` and returns the supplied value. If the
 * value passed is _undefined_, continues execution.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.beforeAtRefundPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterAtRefundPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atRefundPopupClick_ at the `after` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClick = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickThrows<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickThrows */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterThrowsAtRefundPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atRefundPopupClick_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `atRefundPopupClick` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickReturns<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickReturns */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterReturnsAtRefundPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atRefundPopupClick_ at the `afterReturns` joinpoint.
 * - `res` _!front.ChangellyExchangeInputs_ The return of the method after it's successfully run.
 * - `sub` _(value: !front.ChangellyExchangeInputs) =&gt; ?_ Substituites the return value of by the method to the supplied one. If called
 * synchronously, all future aspects will also receive the new value.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData) => void} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.__afterAtRefundPopupClickCancels<!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel>} xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel._afterAtRefundPopupClickCancels */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.AfterCancelsAtRefundPopupClickPointcutData} [data] Metadata passed to the pointcuts of _atRefundPopupClick_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IChangellyExchangeInterruptLineJoinpointModel.AtRefundPopupClickPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel.afterAtRefundPopupClickCancels = function(data) {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.ChangellyExchangeInputs)} xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputFocus
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputFocus<!xyz.swapee.wc.IChangellyExchangeInterruptLine>} xyz.swapee.wc.IChangellyExchangeInterruptLine._atInputFocus */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus} */
/** @return {void|!xyz.swapee.wc.front.ChangellyExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputFocus = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.ChangellyExchangeInputs)} xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputBlur
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine.__atInputBlur<!xyz.swapee.wc.IChangellyExchangeInterruptLine>} xyz.swapee.wc.IChangellyExchangeInterruptLine._atInputBlur */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur} */
/** @return {void|!xyz.swapee.wc.front.ChangellyExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atInputBlur = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.ChangellyExchangeInputs)} xyz.swapee.wc.IChangellyExchangeInterruptLine.__atAddressPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine.__atAddressPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLine>} xyz.swapee.wc.IChangellyExchangeInterruptLine._atAddressPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick} */
/** @return {void|!xyz.swapee.wc.front.ChangellyExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atAddressPopupClick = function() {}

/**
 * @typedef {(this: THIS) => (void|!xyz.swapee.wc.front.ChangellyExchangeInputs)} xyz.swapee.wc.IChangellyExchangeInterruptLine.__atRefundPopupClick
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IChangellyExchangeInterruptLine.__atRefundPopupClick<!xyz.swapee.wc.IChangellyExchangeInterruptLine>} xyz.swapee.wc.IChangellyExchangeInterruptLine._atRefundPopupClick */
/** @typedef {typeof xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick} */
/** @return {void|!xyz.swapee.wc.front.ChangellyExchangeInputs} The inputs to update on the port controller. */
xyz.swapee.wc.IChangellyExchangeInterruptLine.atRefundPopupClick = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IChangellyExchangeInterruptLineJoinpointModel,xyz.swapee.wc.IChangellyExchangeInterruptLine
/* @typal-end */