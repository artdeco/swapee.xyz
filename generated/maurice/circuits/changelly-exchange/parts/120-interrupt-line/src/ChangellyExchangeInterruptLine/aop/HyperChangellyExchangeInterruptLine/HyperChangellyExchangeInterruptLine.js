import AbstractHyperChangellyExchangeInterruptLine from '../../../../gen/AbstractChangellyExchangeInterruptLine/hyper/AbstractHyperChangellyExchangeInterruptLine'
import ChangellyExchangeInterruptLine from '../../ChangellyExchangeInterruptLine'
import ChangellyExchangeInterruptLineGeneralAspects from '../ChangellyExchangeInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperChangellyExchangeInterruptLine} */
export default class extends AbstractHyperChangellyExchangeInterruptLine
 .consults(
  ChangellyExchangeInterruptLineGeneralAspects,
 )
 .implements(
  ChangellyExchangeInterruptLine,
 )
{}