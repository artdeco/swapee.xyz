import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeTouchscreen._deduceInputs} */
export default function deduceInputs(el) {
 const{Address:Address,RefundAddress:RefundAddress}=this
 return {
  address:Address.value,
  refundAddress:RefundAddress.value,
 }
 // deduce
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBOExHLFNBQVMsWUFBWSxDQUFDO0NBQ3JCLE1BQU0sZUFBZSxDQUFDLDZCQUE2QjtDQUNuRDtFQUNDLGVBQWUsQ0FBQyxLQUFLO0VBQ3JCLDJCQUEyQixDQUFDLEtBQUs7QUFDbkM7Q0FDQyxHQUFHO0FBQ0osQ0FBRiJ9