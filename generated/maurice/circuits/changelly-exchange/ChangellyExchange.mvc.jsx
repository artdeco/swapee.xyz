/** @extends {xyz.swapee.wc.AbstractChangellyExchange} */
export default class AbstractChangellyExchange extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractChangellyExchangeComputer} */
export class AbstractChangellyExchangeComputer extends (<computer>
   <adapter name="adaptRegion">
    <xyz.swapee.wc.IRegionSelectorCore region="real" />

    <outputs>
     <xyz.swapee.wc.IChangellyExchangeCore region />
    </outputs>
    The adapter scans the region.
   </adapter>

   <adapter name="adaptGettingOffer">
    <xyz.swapee.wc.IChangellyExchangeCore loadingGetOffer loadingGetFixedOffer />
    <outputs>
     <xyz.swapee.wc.IDealBrokerPort gettingOffer />
    </outputs>
   </adapter>
   <adapter name="adaptGetOfferError">
    <xyz.swapee.wc.IChangellyExchangeCore loadGetOfferError loadGetFixedOfferError />
    <outputs>
     <xyz.swapee.wc.IDealBrokerPort getOfferError />
    </outputs>
   </adapter>

   <adapter name="adaptCreatingTransaction">
    <xyz.swapee.wc.IChangellyExchangeCore loadingCreateTransaction loadingCreateFixedTransaction />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerPort creatingTransaction />
    </outputs>
   </adapter>
   <adapter name="adaptCreateTransactionError">
    <xyz.swapee.wc.IChangellyExchangeCore loadCreateTransactionError loadCreateFixedTransactionError />
    <outputs>
     <xyz.swapee.wc.IExchangeBrokerPort createTransactionError />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractChangellyExchangeController} */
export class AbstractChangellyExchangeController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractChangellyExchangePort} */
export class ChangellyExchangePort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractChangellyExchangeView} */
export class AbstractChangellyExchangeView extends (<view>
  <classes>
   <string opt name="_" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractChangellyExchangeElement} */
export class AbstractChangellyExchangeElement extends (<element v3 html mv>
 <block src="./ChangellyExchange.mvc/src/ChangellyExchangeElement/methods/render.jsx" />
 <inducer src="./ChangellyExchange.mvc/src/ChangellyExchangeElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent} */
export class AbstractChangellyExchangeHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IExchangeBroker via="ExchangeBroker" />
   <xyz.swapee.wc.IDealBroker via="DealBroker" />
   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
   <xyz.swapee.wc.ITransactionInfo via="TransactionInfo" />
   <xyz.swapee.wc.IRegionSelector via="RegionSelector" />
  </connectors>

</html-ic>) { }
// </class-end>