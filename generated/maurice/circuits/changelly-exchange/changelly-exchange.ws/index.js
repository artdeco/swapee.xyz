const d=new Date
let _ChangellyExchangePage,_changellyExchangeArcsIds,_changellyExchangeMethodsIds
// _getChangellyExchange

// const PROD=true
const PROD=false

;({
 ChangellyExchangePage:_ChangellyExchangePage,
 // getChangellyExchange:_getChangellyExchange,
 changellyExchangeArcsIds:_changellyExchangeArcsIds,
 changellyExchangeMethodsIds:_changellyExchangeMethodsIds,
}=require(PROD?'xyz/swapee/rc/changelly-exchange/prod':'./changelly-exchange.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'changelly-exchange',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/changelly-exchange'

export const ChangellyExchangePage=/**@type {typeof xyz.swapee.rc.ChangellyExchangePage}*/(_ChangellyExchangePage)
export const changellyExchangeArcsIds=/**@type {typeof xyz.swapee.rc.changellyExchangeArcsIds}*/(_changellyExchangeArcsIds)
export const changellyExchangeMethodsIds=/**@type {typeof xyz.swapee.rc.changellyExchangeMethodsIds}*/(_changellyExchangeMethodsIds)
// export const getChangellyExchange=/**@type {xyz.swapee.rc.getChangellyExchange}*/(_getChangellyExchange)