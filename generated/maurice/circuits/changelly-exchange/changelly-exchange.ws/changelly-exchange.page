import 'xyz/swapee/rc/changelly-exchange'
import {ChangellyUniversal} from '@type.community/changelly.com'
import {ChangeNowUniversal} from '@swapee.xyz/swapee.xyz'
import {LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'
import filterCheckPayment from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCheckPayment/filter-check-payment'
import filterGetFixedOffer from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetFixedOffer/filter-get-fixed-offer'
import filterGetOffer from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetOffer/filter-get-offer'
import filterCreateTransaction from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCreateTransaction/filter-create-transaction'
import filterGetTransaction from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterGetTransaction/filter-get-transaction'
import filterCreateFixedTransaction from '../ChangellyExchange.mvc/src/ChangellyExchangeService/methods/filterCreateFixedTransaction/filter-create-fixed-transaction'
import {ser} from '@type.engineering/type-engineer'

export default /**@type {xyz.swapee.rc.changellyExchange}*/(<>
 <form>
 </form>
 <answers>
 </answers>
 <validation>
 </validation>
 <errors>
 </errors>
 <ctx>
 </ctx>

 <action name="filterGetFixedOffer" mid="22dec">
  <form>
   <prop name="amountFrom" />
   <prop name="currencyFrom" />
   <prop name="currencyTo" />
   <prop name="fixed" />
   <prop name="ready" />
   <prop name="getOffer" />
  </form>
  <answers>
   <prop name="rate" />
   <prop name="fixedId" />
   <prop name="estimatedFixedAmountTo" />
   <prop name="minAmount" />
   <prop name="maxAmount" />
  </answers>
  Generates a fixed offer.
 </action>

 <action name="filterGetOffer" mid="d8a4f">
  <form>
   <prop name="amountFrom" />
   <prop name="currencyFrom" />
   <prop name="currencyTo" />
   <prop name="fixed" />
   <prop name="ready" />
   <prop name="getOffer" />
  </form>
  <answers>
   <prop name="rate" />
   <prop name="estimatedFloatAmountTo" />
   <prop name="networkFee" />
   <prop name="partnerFee" />
   <prop name="visibleAmount" />
   <prop name="minAmount" />
   <prop name="maxAmount" />
  </answers>
  Generates a floating-rate offer. Some exchanges like _Changelly_ return data
  about fees immediately without creating a transaction.
 </action>

 <action name="filterCreateTransaction" mid="321e2">
  <form>
   <prop name="amountFrom" />
   <prop name="currencyFrom" />
   <prop name="currencyTo" />
   <prop name="fixed" />
   <prop name="address" />
   <prop name="refundAddress" />
   <prop name="createTransaction" />
  </form>
  <answers>
   <prop name="id" />
   <prop name="createTransactionError" />
   <prop name="createdAt" />
   <prop name="payinAddress" />
   <prop name="payinExtraId" />
   <prop name="status" />
   <prop name="kycRequired" />
   <prop name="confirmedAmountFrom" />
  </answers>
  Creates a pair of deposit and payout addresses.
 </action>

 <action name="filterGetTransaction" mid="6a2c5">
  <form>
   <prop name="tid" />
  </form>
  <answers>
   <prop name="fixed" />
   <prop name="currencyFrom" />
   <prop name="currencyTo" />
   <prop name="amountFrom" />
   <prop name="amountTo" />
   <prop name="networkFee" />
   <prop name="partnerFee" />
   <prop name="visibleAmount" />
   <prop name="notFound" />
   <prop name="rate" />
   <prop name="id" />
   <prop name="createdAt" />
   <prop name="payinAddress" />
   <prop name="payinExtraId" />
   <prop name="status" />
   <prop name="confirmedAmountFrom" />
  </answers>
  Looks up the transaction info.
 </action>

 <action name="filterCreateFixedTransaction" mid="1344b">
  <form>
   <prop name="amountFrom" />
   <prop name="currencyFrom" />
   <prop name="currencyTo" />
   <prop name="fixed" />
   <prop name="address" />
   <prop name="refundAddress" />
   <prop name="createTransaction" />
   <prop name="fixedId" />
  </form>
  <answers>
   <prop name="getOffer" />
   <prop name="id" />
   <prop name="createTransactionError" />
   <prop name="createdAt" />
   <prop name="payinAddress" />
   <prop name="payinExtraId" />
   <prop name="status" />
   <prop name="kycRequired" />
   <prop name="confirmedAmountFrom" />
  </answers>
  Creates a pair of deposit and payout addresses for fixed-rate offers.
 </action>

 <action name="filterCheckPayment" mid="c3eae">
  <form>
   <prop name="id" />
   <prop name="checkPayment" />
  </form>
  <answers>
   <prop name="status" />
   <prop name="checkPaymentError" />
  </answers>
  Updates the status of the transaction.
 </action>

 <com.changelly.UChangelly>{ChangellyUniversal}</com.changelly.UChangelly>

 <io.changenow.UChangeNow>{ChangeNowUniversal}</io.changenow.UChangeNow>

 <io.letsexchange.ULetsExchange>{LetsExchangeUniversal}</io.letsexchange.ULetsExchange>
</>)({
 filterCheckPayment:[
  filterCheckPayment,
 ],
 afterFilterCheckPayment:[
  ServeData,
 ],
 afterFilterCheckPaymentThrows:[
  ServeError,
 ],
 filterGetFixedOffer:[
  filterGetFixedOffer,
 ],
 afterFilterGetFixedOffer:[
  ServeData,
 ],
 afterFilterGetFixedOfferThrows:[
  ServeError,
 ],
 filterGetOffer:[
  filterGetOffer,
 ],
 afterFilterGetOffer:[
  ServeData,
 ],
 afterFilterGetOfferThrows:[
  ServeError,
 ],
 filterCreateTransaction:[
  filterCreateTransaction,
 ],
 afterFilterCreateTransaction:[
  ServeData,
 ],
 afterFilterCreateTransactionThrows:[
  ServeError,
 ],
 filterGetTransaction:[
  filterGetTransaction,
 ],
 afterFilterGetTransaction:[
  ServeData,
 ],
 afterFilterGetTransactionThrows:[
  ServeError,
 ],
 filterCreateFixedTransaction:[
  filterCreateFixedTransaction,
 ],
 afterFilterCreateFixedTransaction:[
  ServeData,
 ],
 afterFilterCreateFixedTransactionThrows:[
  ServeError,
 ],
})

function ServeError({hide:hide,err:err,args:{ctx:ctx}}){
 if(err.message.startsWith('!')) {
  hide(err)
  ctx.body={error:err.message.replace('!','')}
 }
}
function ServeData({args:{ctx:ctx,answers:answers}}){
 const data=ser(answers,PQs)
 ctx.body={data:data}
}

const PQs={
 rate:'67942',
 fixedId:'2568a',
 estimatedFixedAmountTo:'ea0a0',
 minAmount:'ae0d3',
 maxAmount:'d0b0c',
 estimatedFloatAmountTo:'8ca05',
 networkFee:'5fd9c',
 partnerFee:'96f44',
 visibleAmount:'4685c',
 id:'b80bb',
 createTransactionError:'601f8',
 createdAt:'97def',
 payinAddress:'59af8',
 payinExtraId:'7a19b',
 status:'9acb4',
 kycRequired:'2b803',
 confirmedAmountFrom:'6103f',
 fixed:'cec31',
 currencyFrom:'96c88',
 currencyTo:'c23cd',
 amountFrom:'748e6',
 amountTo:'9dc9f',
 notFound:'5ccf4',
 getOffer:'b370a',
 checkPaymentError:'afbad',
}