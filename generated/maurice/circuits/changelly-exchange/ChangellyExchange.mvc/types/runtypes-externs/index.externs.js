/**
 * @fileoverview
 * @externs
 */

xyz.swapee.wc.IChangellyExchangeComputer={}
xyz.swapee.wc.IChangellyExchangePort={}
xyz.swapee.wc.IChangellyExchangeCore={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IChangellyExchangeController={}
xyz.swapee.wc.IChangellyExchangeRadio={}
xyz.swapee.wc.IChangellyExchangeService={}
xyz.swapee.wc.IChangellyExchangeDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IChangellyExchangeDisplay={}
xyz.swapee.wc.IChangellyExchangeController={}
/** @const */
var $$xyz={}
$$xyz.swapee={}
$$xyz.swapee.wc={}
$$xyz.swapee.wc.IChangellyExchangeComputer={}
$$xyz.swapee.wc.IChangellyExchangePort={}
$$xyz.swapee.wc.IChangellyExchangeCore={}
$$xyz.swapee.wc.IChangellyExchangeRadio={}
$$xyz.swapee.wc.IChangellyExchangeService={}
$$xyz.swapee.wc.IChangellyExchangeDisplay={}
$$xyz.swapee.wc.back={}
$$xyz.swapee.wc.back.IChangellyExchangeDisplay={}
$$xyz.swapee.wc.IChangellyExchangeController={}
$$xyz.swapee.wc.front={}
$$xyz.swapee.wc.front.IChangellyExchangeController={}
$$xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil={}
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeComputer.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeComputerCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeComputer} */
xyz.swapee.wc.IChangellyExchangeComputerCaster.prototype.asIChangellyExchangeComputer
/** @type {!xyz.swapee.wc.BoundChangellyExchangeComputer} */
xyz.swapee.wc.IChangellyExchangeComputerCaster.prototype.superChangellyExchangeComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.IChangellyExchangeComputer = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCheckPayment = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptRegion = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGettingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetOfferError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreatingTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateTransactionError = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeComputer.prototype.compute = function(mem, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.ChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeComputer.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeComputer = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init */
xyz.swapee.wc.ChangellyExchangeComputer.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.ChangellyExchangeComputer.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.AbstractChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.ChangellyExchangeComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeComputer, ...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese)} */
xyz.swapee.wc.ChangellyExchangeComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.RecordIChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @typedef {{ adaptGetFixedOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer, adaptGetOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer, adaptCreateTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction, adaptGetTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction, adaptCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction, adaptCheckPayment: xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment, adaptRegion: xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion, adaptGettingOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer, adaptGetOfferError: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError, adaptCreatingTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction, adaptCreateTransactionError: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError, compute: xyz.swapee.wc.IChangellyExchangeComputer.compute }} */
xyz.swapee.wc.RecordIChangellyExchangeComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.BoundIChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.BoundIChangellyExchangeComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.BoundChangellyExchangeComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeComputer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCheckPayment
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptRegion
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGettingOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOfferError
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreatingTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, !xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form, xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form): (undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransactionError
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeComputer.__compute = function(mem, land) {}
/** @typedef {function(xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeComputer.compute.Land): void} */
xyz.swapee.wc.IChangellyExchangeComputer.compute
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeComputer, xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeComputer.compute.Land): void} */
xyz.swapee.wc.IChangellyExchangeComputer._compute
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeComputer.__compute} */
xyz.swapee.wc.IChangellyExchangeComputer.__compute

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CheckPayment_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CheckPaymentError}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IRegionSelectorCore.Model.Region_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region = function() {}
/** @type {(?string)|undefined} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.prototype.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region}
 */
xyz.swapee.wc.IChangellyExchangeCore.Model.Region = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.Region}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe.prototype.loadingGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe.prototype.loadingGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GettingOffer}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe.prototype.loadGetOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe.prototype.loadGetFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOfferError}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe.prototype.loadingCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe.prototype.loadingCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatingTransaction}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe.prototype.loadCreateTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe.prototype.loadCreateFixedTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.compute.Land exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @record */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land = function() {}
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.ExchangeBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.DealBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.TransactionInfo
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.RegionSelector

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeOuterCoreFields
/** @type {!xyz.swapee.wc.IChangellyExchangeOuterCore.Model} */
xyz.swapee.wc.IChangellyExchangeOuterCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeOuterCoreCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeOuterCore} */
xyz.swapee.wc.IChangellyExchangeOuterCoreCaster.prototype.asIChangellyExchangeOuterCore
/** @type {!xyz.swapee.wc.BoundChangellyExchangeOuterCore} */
xyz.swapee.wc.IChangellyExchangeOuterCoreCaster.prototype.superChangellyExchangeOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreCaster}
 */
xyz.swapee.wc.IChangellyExchangeOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.ChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeOuterCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.ChangellyExchangeOuterCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.AbstractChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore = function() {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.ChangellyExchangeMemoryPQs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeMemoryPQs.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeMemoryQPs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeMemoryQPs.prototype.g7b3d

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.RecordIChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.BoundIChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreCaster}
 */
xyz.swapee.wc.BoundIChangellyExchangeOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.BoundChangellyExchangeOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeOuterCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region}
 */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region = function() {}
/** @type {(*)|undefined} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region.prototype.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
xyz.swapee.wc.IChangellyExchangePort.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
xyz.swapee.wc.IChangellyExchangePortFields
/** @type {!xyz.swapee.wc.IChangellyExchangePort.Inputs} */
xyz.swapee.wc.IChangellyExchangePortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IChangellyExchangePort.Inputs} */
xyz.swapee.wc.IChangellyExchangePortFields.prototype.props

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
xyz.swapee.wc.IChangellyExchangePortCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangePort} */
xyz.swapee.wc.IChangellyExchangePortCaster.prototype.asIChangellyExchangePort
/** @type {!xyz.swapee.wc.BoundChangellyExchangePort} */
xyz.swapee.wc.IChangellyExchangePortCaster.prototype.superChangellyExchangePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangePortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangePortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IChangellyExchangePort.Inputs>}
 */
xyz.swapee.wc.IChangellyExchangePort = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init */
xyz.swapee.wc.IChangellyExchangePort.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangePort.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangePort.prototype.resetChangellyExchangePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangePort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangePort.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangePort = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init */
xyz.swapee.wc.ChangellyExchangePort.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.ChangellyExchangePort.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.AbstractChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangePort.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangePort, ...!xyz.swapee.wc.IChangellyExchangePort.Initialese)} */
xyz.swapee.wc.ChangellyExchangePortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ChangellyExchangeMemoryPQs}
 */
xyz.swapee.wc.ChangellyExchangeInputsPQs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeMemoryPQs}
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeInputsQPs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.RecordIChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @typedef {{ resetPort: xyz.swapee.wc.IChangellyExchangePort.resetPort, resetChangellyExchangePort: xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort }} */
xyz.swapee.wc.RecordIChangellyExchangePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.BoundIChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePortFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangePort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangePortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IChangellyExchangePort.Inputs>}
 */
xyz.swapee.wc.BoundIChangellyExchangePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.BoundChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangePort}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangePort = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangePort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangePort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangePort): void} */
xyz.swapee.wc.IChangellyExchangePort._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangePort.__resetPort} */
xyz.swapee.wc.IChangellyExchangePort.__resetPort

// nss:xyz.swapee.wc.IChangellyExchangePort,$$xyz.swapee.wc.IChangellyExchangePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangePort): void} */
xyz.swapee.wc.IChangellyExchangePort._resetChangellyExchangePort
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort} */
xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort

// nss:xyz.swapee.wc.IChangellyExchangePort,$$xyz.swapee.wc.IChangellyExchangePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
xyz.swapee.wc.IChangellyExchangePortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePortInterface exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangePortInterface}
 */
xyz.swapee.wc.ChangellyExchangePortInterface = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortInterface.Props exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @record */
xyz.swapee.wc.IChangellyExchangePortInterface.Props = function() {}
/** @type {string} */
xyz.swapee.wc.IChangellyExchangePortInterface.Props.prototype.host
/** @type {?string} */
xyz.swapee.wc.IChangellyExchangePortInterface.Props.prototype.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeCoreFields
/** @type {!xyz.swapee.wc.IChangellyExchangeCore.Model} */
xyz.swapee.wc.IChangellyExchangeCoreFields.prototype.model

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeCoreCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeCore} */
xyz.swapee.wc.IChangellyExchangeCoreCaster.prototype.asIChangellyExchangeCore
/** @type {!xyz.swapee.wc.BoundChangellyExchangeCore} */
xyz.swapee.wc.IChangellyExchangeCoreCaster.prototype.superChangellyExchangeCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore}
 */
xyz.swapee.wc.IChangellyExchangeCore = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeCore.prototype.resetCore = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeCore.prototype.resetChangellyExchangeCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.ChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeCore.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeCore = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.ChangellyExchangeCore.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.AbstractChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore = function() {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeCachePQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.ChangellyExchangeCachePQs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.loadingCreateTransaction
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.hasMoreCreateTransaction
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.loadCreateTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeCacheQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeCacheQPs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.cee07
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.db41a
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.h6bbc

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.RecordIChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {{ resetCore: xyz.swapee.wc.IChangellyExchangeCore.resetCore, resetChangellyExchangeCore: xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore }} */
xyz.swapee.wc.RecordIChangellyExchangeCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.BoundIChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeOuterCore}
 */
xyz.swapee.wc.BoundIChangellyExchangeCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.BoundChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeCore = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeCore): void} */
xyz.swapee.wc.IChangellyExchangeCore._resetCore
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeCore.__resetCore} */
xyz.swapee.wc.IChangellyExchangeCore.__resetCore

// nss:xyz.swapee.wc.IChangellyExchangeCore,$$xyz.swapee.wc.IChangellyExchangeCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeCore): void} */
xyz.swapee.wc.IChangellyExchangeCore._resetChangellyExchangeCore
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore} */
xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore

// nss:xyz.swapee.wc.IChangellyExchangeCore,$$xyz.swapee.wc.IChangellyExchangeCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.loadingGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.loadingGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.hasMoreGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.hasMoreGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.loadGetFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.loadGetFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.loadingGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.loadingGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.hasMoreGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.hasMoreGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.loadGetOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.loadGetOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.loadingCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.loadingCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.hasMoreCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.hasMoreCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.loadCreateTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.loadCreateTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.loadingGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.loadingGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.hasMoreGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.hasMoreGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.loadGetTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.loadGetTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.loadingCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.loadingCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.hasMoreCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.hasMoreCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.loadCreateFixedTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.loadCreateFixedTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.loadingCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.loadingCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.hasMoreCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.hasMoreCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.loadCheckPaymentError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.loadCheckPaymentError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.prototype.loadingGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.prototype.hasMoreGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.prototype.loadGetFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.prototype.loadingGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.prototype.hasMoreGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.prototype.loadGetOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.prototype.loadingCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.prototype.hasMoreCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.prototype.loadCreateTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.prototype.loadingGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.prototype.hasMoreGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.prototype.loadGetTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.prototype.loadingCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.prototype.hasMoreCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.prototype.loadCreateFixedTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment = function() {}
/** @type {boolean|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.prototype.loadingCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment = function() {}
/** @type {(?boolean)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.prototype.hasMoreCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError = function() {}
/** @type {(?Error)|undefined} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.prototype.loadCheckPaymentError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError}
 */
xyz.swapee.wc.IChangellyExchangeCore.Model = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 */
xyz.swapee.wc.IChangellyExchangeController.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeProcessor.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @interface */
xyz.swapee.wc.IChangellyExchangeProcessorCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeProcessor} */
xyz.swapee.wc.IChangellyExchangeProcessorCaster.prototype.asIChangellyExchangeProcessor
/** @type {!xyz.swapee.wc.BoundChangellyExchangeProcessor} */
xyz.swapee.wc.IChangellyExchangeProcessorCaster.prototype.superChangellyExchangeProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeControllerFields
/** @type {!xyz.swapee.wc.IChangellyExchangeController.Inputs} */
xyz.swapee.wc.IChangellyExchangeControllerFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeControllerCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeController} */
xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeProcessor} */
xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeProcessor
/** @type {!xyz.swapee.wc.BoundChangellyExchangeController} */
xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
xyz.swapee.wc.IChangellyExchangeController = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.IChangellyExchangeController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.resetPort = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetOffer = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadCreateTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadCreateFixedTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.IChangellyExchangeController.prototype.loadCheckPayment = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessorCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 */
xyz.swapee.wc.IChangellyExchangeProcessor = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init */
xyz.swapee.wc.IChangellyExchangeProcessor.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.ChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeProcessor = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init */
xyz.swapee.wc.ChangellyExchangeProcessor.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.ChangellyExchangeProcessor.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.AbstractChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.ChangellyExchangeProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeProcessor, ...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese)} */
xyz.swapee.wc.ChangellyExchangeProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.RecordIChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.RecordIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @typedef {{ resetPort: xyz.swapee.wc.IChangellyExchangeController.resetPort, loadGetFixedOffer: xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer, loadGetOffer: xyz.swapee.wc.IChangellyExchangeController.loadGetOffer, loadCreateTransaction: xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction, loadGetTransaction: xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction, loadCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction, loadCheckPayment: xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment }} */
xyz.swapee.wc.RecordIChangellyExchangeController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.BoundIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
xyz.swapee.wc.BoundIChangellyExchangeController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.BoundIChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeCore}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 */
xyz.swapee.wc.BoundIChangellyExchangeProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.BoundChangellyExchangeProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeProcessor = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/100-ChangellyExchangeMemory.xml} xyz.swapee.wc.ChangellyExchangeMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 4c69ed3cac2b99c8bab35288bd6292df */
/** @record */
xyz.swapee.wc.ChangellyExchangeMemory = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.host
/** @type {?string} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.region
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetFixedOffer
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetFixedOffer
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetFixedOfferError
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetOffer
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetOffer
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetOfferError
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCreateTransaction
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCreateTransaction
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCreateTransactionError
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetTransaction
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetTransaction
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetTransactionError
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCreateFixedTransaction
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCreateFixedTransaction
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCreateFixedTransactionError
/** @type {boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCheckPayment
/** @type {?boolean} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCheckPayment
/** @type {?Error} */
xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCheckPaymentError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/102-ChangellyExchangeInputs.xml} xyz.swapee.wc.front.ChangellyExchangeInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8927ef629fd1fb022b52d17fc4b31d4b */
/** @record */
xyz.swapee.wc.front.ChangellyExchangeInputs = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.front.ChangellyExchangeInputs.prototype.host
/** @type {(?string)|undefined} */
xyz.swapee.wc.front.ChangellyExchangeInputs.prototype.region

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @record */
xyz.swapee.wc.ChangellyExchangeEnv = function() {}
/** @type {xyz.swapee.wc.IChangellyExchange} */
xyz.swapee.wc.ChangellyExchangeEnv.prototype.changellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
xyz.swapee.wc.IChangellyExchange.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeFields
/** @type {!xyz.swapee.wc.IChangellyExchange.Pinout} */
xyz.swapee.wc.IChangellyExchangeFields.prototype.pinout

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchange} */
xyz.swapee.wc.IChangellyExchangeCaster.prototype.asIChangellyExchange
/** @type {!xyz.swapee.wc.BoundChangellyExchange} */
xyz.swapee.wc.IChangellyExchangeCaster.prototype.superChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.IChangellyExchange = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init */
xyz.swapee.wc.IChangellyExchange.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchange}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchange.Initialese>}
 */
xyz.swapee.wc.ChangellyExchange = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init */
xyz.swapee.wc.ChangellyExchange.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.ChangellyExchange.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.AbstractChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchange.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchange, ...!xyz.swapee.wc.IChangellyExchange.Initialese)} */
xyz.swapee.wc.ChangellyExchangeConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @record */
xyz.swapee.wc.IChangellyExchange.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IChangellyExchange.Pinout)|undefined} */
xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IChangellyExchange.Pinout)|undefined} */
xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IChangellyExchange.Pinout} */
xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.ChangellyExchangeMemory)|undefined} */
xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.ChangellyExchangeClasses)|undefined} */
xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.classes

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.RecordIChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.BoundIChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchange}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.BoundIChangellyExchange = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.BoundChangellyExchange exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchange}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchange = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePort.Inputs}
 */
xyz.swapee.wc.IChangellyExchangeController.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Inputs}
 */
xyz.swapee.wc.IChangellyExchange.Pinout = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
xyz.swapee.wc.IChangellyExchangeBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeBuffer}
 */
xyz.swapee.wc.ChangellyExchangeBuffer = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeGPU.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeController.Initialese}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchange.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster.prototype.asIChangellyExchangeHtmlComponent
/** @type {!xyz.swapee.wc.BoundChangellyExchangeHtmlComponent} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster.prototype.superChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IChangellyExchangeGPUFields
/** @type {!Object<string, string>} */
xyz.swapee.wc.IChangellyExchangeGPUFields.prototype.vdusPQs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
xyz.swapee.wc.IChangellyExchangeGPUCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeGPU} */
xyz.swapee.wc.IChangellyExchangeGPUCaster.prototype.asIChangellyExchangeGPU
/** @type {!xyz.swapee.wc.BoundChangellyExchangeGPU} */
xyz.swapee.wc.IChangellyExchangeGPUCaster.prototype.superChangellyExchangeGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!ChangellyExchangeMemory,.!ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplay}
 */
xyz.swapee.wc.IChangellyExchangeGPU = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init */
xyz.swapee.wc.IChangellyExchangeGPU.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeController}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreen}
 * @extends {xyz.swapee.wc.IChangellyExchange}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponent = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init */
xyz.swapee.wc.IChangellyExchangeHtmlComponent.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.ChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeHtmlComponent = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init */
xyz.swapee.wc.ChangellyExchangeHtmlComponent.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.ChangellyExchangeHtmlComponent.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.ChangellyExchangeHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeHtmlComponent, ...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese)} */
xyz.swapee.wc.ChangellyExchangeHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @interface */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields
/** @type {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields.prototype.RouterNet
/** @type {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields.prototype.RouterPorts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts} [ports]
 * @return {?}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.prototype.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.ChangellyExchangeHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil}
 */
xyz.swapee.wc.ChangellyExchangeHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.RecordIChangellyExchangeHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @typedef {{ router: xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.router }} */
xyz.swapee.wc.RecordIChangellyExchangeHtmlComponentUtil

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.BoundIChangellyExchangeHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentUtilFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeHtmlComponentUtil}
 */
xyz.swapee.wc.BoundIChangellyExchangeHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.BoundChangellyExchangeHtmlComponentUtil exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeHtmlComponentUtil}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeHtmlComponentUtil = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.router exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet} [net]
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores} [cores]
 * @param {!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts} [ports]
 */
$$xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.__router = function(net, cores, ports) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.router
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil, !xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet=, !xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores=, !xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts=)} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil._router
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.__router} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.__router

// nss:xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil,$$xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet = function() {}
/** @type {typeof xyz.swapee.wc.IExchangeBrokerPort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.ExchangeBroker
/** @type {typeof xyz.swapee.wc.IDealBrokerPort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.DealBroker
/** @type {typeof xyz.swapee.wc.IExchangeIntentPort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.ExchangeIntent
/** @type {typeof xyz.swapee.wc.ITransactionInfoPort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.TransactionInfo
/** @type {typeof xyz.swapee.wc.IRegionSelectorPort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.RegionSelector
/** @type {typeof xyz.swapee.wc.IChangellyExchangePort} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterNet.prototype.ChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores = function() {}
/** @type {!xyz.swapee.wc.ExchangeBrokerMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.ExchangeBroker
/** @type {!xyz.swapee.wc.DealBrokerMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.DealBroker
/** @type {!xyz.swapee.wc.ExchangeIntentMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.TransactionInfoMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.TransactionInfo
/** @type {!xyz.swapee.wc.RegionSelectorMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.RegionSelector
/** @type {!xyz.swapee.wc.ChangellyExchangeMemory} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterCores.prototype.ChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.element.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props  */
/** @record */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts = function() {}
/** @type {!xyz.swapee.wc.IExchangeBroker.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.ExchangeBroker
/** @type {!xyz.swapee.wc.IDealBroker.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.DealBroker
/** @type {!xyz.swapee.wc.IExchangeIntent.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.ExchangeIntent
/** @type {!xyz.swapee.wc.ITransactionInfo.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.TransactionInfo
/** @type {!xyz.swapee.wc.IRegionSelector.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.RegionSelector
/** @type {!xyz.swapee.wc.IChangellyExchange.Pinout} */
xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil.RouterPorts.prototype.ChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.RecordIChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.BoundIChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!ChangellyExchangeMemory,.!ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeDisplay}
 */
xyz.swapee.wc.BoundIChangellyExchangeGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeController}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeScreen}
 * @extends {xyz.swapee.wc.BoundIChangellyExchange}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 */
xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.BoundChangellyExchangeHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeHtmlComponent = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeRadio.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadioCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeRadioCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeRadio} */
xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.asIChangellyExchangeRadio
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeComputer} */
xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.asIChangellyExchangeComputer
/** @type {!xyz.swapee.wc.BoundChangellyExchangeRadio} */
xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.superChangellyExchangeRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeRadioCaster}
 */
xyz.swapee.wc.IChangellyExchangeRadio = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 */
xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCheckPayment = function(form, changes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.ChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeRadio.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeRadio = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.ChangellyExchangeRadio.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.AbstractChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio = function() {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.__extend = function(...Extensions) {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.continues = function(...Implementations) {}
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.RecordIChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @typedef {{ adaptLoadGetFixedOffer: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer, adaptLoadGetOffer: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer, adaptLoadCreateTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction, adaptLoadGetTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction, adaptLoadCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction, adaptLoadCheckPayment: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment }} */
xyz.swapee.wc.RecordIChangellyExchangeRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.BoundIChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeRadioCaster}
 */
xyz.swapee.wc.BoundIChangellyExchangeRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.BoundChangellyExchangeRadio exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeRadio = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form, IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form, IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form, IChangellyExchangeComputer.adaptLoadGetOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form, IChangellyExchangeComputer.adaptLoadGetOffer.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form, IChangellyExchangeComputer.adaptLoadCreateTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form, IChangellyExchangeComputer.adaptLoadCreateTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form, IChangellyExchangeComputer.adaptLoadGetTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form, IChangellyExchangeComputer.adaptLoadGetTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form, IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form, IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 */
$$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment = function(form, changes) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form, IChangellyExchangeComputer.adaptLoadCheckPayment.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeRadio, !xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form, IChangellyExchangeComputer.adaptLoadCheckPayment.Form): (undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCheckPayment
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CheckPayment_Safe}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CheckPaymentError}
 */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeDesigner = function() {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IChangellyExchangeDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IChangellyExchangeDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh} mesh
 * @return {?}
 */
xyz.swapee.wc.IChangellyExchangeDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool} memPool
 * @return {?}
 */
xyz.swapee.wc.IChangellyExchangeDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.IChangellyExchangeDesigner.prototype.lendClasses = function(classes) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.ChangellyExchangeDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeDesigner}
 */
xyz.swapee.wc.ChangellyExchangeDesigner = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ExchangeBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.DealBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.TransactionInfo
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.RegionSelector
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh = function() {}
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ExchangeBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.DealBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.TransactionInfo
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.RegionSelector
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ChangellyExchange
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool = function() {}
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ExchangeBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.DealBroker
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.TransactionInfo
/** @type {!Object} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.RegionSelector
/** @type {!xyz.swapee.wc.ChangellyExchangeMemory} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ChangellyExchange
/** @type {!xyz.swapee.wc.ChangellyExchangeMemory} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.This

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {com.changelly.UChangelly.Initialese}
 * @extends {io.changenow.UChangeNow.Initialese}
 * @extends {io.letsexchange.ULetsExchange.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeService.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeServiceCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/** @interface */
xyz.swapee.wc.IChangellyExchangeServiceCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeService} */
xyz.swapee.wc.IChangellyExchangeServiceCaster.prototype.asIChangellyExchangeService
/** @type {!xyz.swapee.wc.BoundChangellyExchangeService} */
xyz.swapee.wc.IChangellyExchangeServiceCaster.prototype.superChangellyExchangeService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeServiceCaster}
 * @extends {com.changelly.UChangelly}
 * @extends {io.changenow.UChangeNow}
 * @extends {io.letsexchange.ULetsExchange}
 */
xyz.swapee.wc.IChangellyExchangeService = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterGetFixedOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterGetOffer = function(form) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterCreateTransaction = function(form) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterGetTransaction = function(form) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterCreateFixedTransaction = function(form) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return>}
 */
xyz.swapee.wc.IChangellyExchangeService.prototype.filterCheckPayment = function(form) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.ChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeService}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeService.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeService = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeService}
 */
xyz.swapee.wc.ChangellyExchangeService.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.AbstractChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeService}
 */
xyz.swapee.wc.AbstractChangellyExchangeService = function() {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeService|typeof xyz.swapee.wc.ChangellyExchangeService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeService.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeService}
 */
xyz.swapee.wc.AbstractChangellyExchangeService.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeService}
 */
xyz.swapee.wc.AbstractChangellyExchangeService.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeService|typeof xyz.swapee.wc.ChangellyExchangeService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeService}
 */
xyz.swapee.wc.AbstractChangellyExchangeService.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeService|typeof xyz.swapee.wc.ChangellyExchangeService)|(!com.changelly.UChangelly|typeof com.changelly.UChangelly)|(!io.changenow.UChangeNow|typeof io.changenow.UChangeNow)|(!io.letsexchange.ULetsExchange|typeof io.letsexchange.ULetsExchange))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeService}
 */
xyz.swapee.wc.AbstractChangellyExchangeService.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.RecordIChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/** @typedef {{ filterGetFixedOffer: xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer, filterGetOffer: xyz.swapee.wc.IChangellyExchangeService.filterGetOffer, filterCreateTransaction: xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction, filterGetTransaction: xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction, filterCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction, filterCheckPayment: xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment }} */
xyz.swapee.wc.RecordIChangellyExchangeService

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.BoundIChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeService}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeServiceCaster}
 * @extends {com.changelly.BoundUChangelly}
 * @extends {io.changenow.BoundUChangeNow}
 * @extends {io.letsexchange.BoundULetsExchange}
 */
xyz.swapee.wc.BoundIChangellyExchangeService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.BoundChangellyExchangeService exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeService}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeService = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterGetFixedOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterGetFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeService.__filterGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterGetOffer = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterGetOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterGetOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterGetOffer} */
xyz.swapee.wc.IChangellyExchangeService.__filterGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterCreateTransaction = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterCreateTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeService.__filterCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterGetTransaction = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterGetTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterGetTransaction} */
xyz.swapee.wc.IChangellyExchangeService.__filterGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterCreateFixedTransaction = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeService.__filterCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form} form
 * @return {!Promise<xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return>}
 */
$$xyz.swapee.wc.IChangellyExchangeService.__filterCheckPayment = function(form) {}
/** @typedef {function(!xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return>} */
xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeService, !xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form): !Promise<xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return>} */
xyz.swapee.wc.IChangellyExchangeService._filterCheckPayment
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeService.__filterCheckPayment} */
xyz.swapee.wc.IChangellyExchangeService.__filterCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeService,$$xyz.swapee.wc.IChangellyExchangeService,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.FixedId}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.EstimatedFixedAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetFixedOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.EstimatedFloatAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.NetworkFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.PartnerFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.VisibleAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.MaxAmount}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetOffer.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCreateTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.Fixed}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.CurrencyFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.CurrencyTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.AmountFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.AmountTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.NetworkFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.PartnerFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.VisibleAmount}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.NotFound}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Model.Rate}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeService.filterGetTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Model.GetOffer}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.ConfirmedAmountFrom}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCreateFixedTransaction.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CheckPayment_Safe}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Form = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/190-IChangellyExchangeService.xml} xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 34e72f5ff200d6fc41c78ab905c554ca */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Model.CheckPaymentError}
 */
xyz.swapee.wc.IChangellyExchangeService.filterCheckPayment.Return = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/200-ChangellyExchangeLand.xml} xyz.swapee.wc.ChangellyExchangeLand exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7f5bde22c623d36bfc676cd7fa42b84c */
/** @record */
xyz.swapee.wc.ChangellyExchangeLand = function() {}
/** @type {!Object} */
xyz.swapee.wc.ChangellyExchangeLand.prototype.ExchangeBroker
/** @type {!Object} */
xyz.swapee.wc.ChangellyExchangeLand.prototype.DealBroker
/** @type {!Object} */
xyz.swapee.wc.ChangellyExchangeLand.prototype.ExchangeIntent
/** @type {!Object} */
xyz.swapee.wc.ChangellyExchangeLand.prototype.TransactionInfo
/** @type {!Object} */
xyz.swapee.wc.ChangellyExchangeLand.prototype.RegionSelector

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings>}
 */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese = function() {}
/** @type {HTMLPreElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.Debug
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.ExchangeBroker
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.DealBroker
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.ExchangeIntent
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.TransactionInfo
/** @type {HTMLElement|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.RegionSelector

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeDisplayFields
/** @type {!xyz.swapee.wc.IChangellyExchangeDisplay.Settings} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IChangellyExchangeDisplay.Queries} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.queries
/** @type {HTMLPreElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.Debug
/** @type {HTMLElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.ExchangeBroker
/** @type {HTMLElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.DealBroker
/** @type {HTMLElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.ExchangeIntent
/** @type {HTMLElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.TransactionInfo
/** @type {HTMLElement} */
xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.RegionSelector

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeDisplayCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeDisplay} */
xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeDisplay
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeScreen} */
xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeScreen
/** @type {!xyz.swapee.wc.BoundChangellyExchangeDisplay} */
xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.superChangellyExchangeDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.ChangellyExchangeMemory, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null>}
 */
xyz.swapee.wc.IChangellyExchangeDisplay = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init */
xyz.swapee.wc.IChangellyExchangeDisplay.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {null} land
 * @return {void}
 */
xyz.swapee.wc.IChangellyExchangeDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.ChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeDisplay = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init */
xyz.swapee.wc.ChangellyExchangeDisplay.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.ChangellyExchangeDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.AbstractChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.ChangellyExchangeDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeDisplay, ...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese)} */
xyz.swapee.wc.ChangellyExchangeDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.ChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeGPU.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeGPU = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init */
xyz.swapee.wc.ChangellyExchangeGPU.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.ChangellyExchangeGPU.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.AbstractChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.ChangellyExchangeGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeGPU, ...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese)} */
xyz.swapee.wc.ChangellyExchangeGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.BoundChangellyExchangeGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeGPU = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.RecordIChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @typedef {{ paint: xyz.swapee.wc.IChangellyExchangeDisplay.paint }} */
xyz.swapee.wc.RecordIChangellyExchangeDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.BoundIChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.ChangellyExchangeMemory, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null>}
 */
xyz.swapee.wc.BoundIChangellyExchangeDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.BoundChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeDisplay = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {null} land
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ChangellyExchangeMemory, null): void} */
xyz.swapee.wc.IChangellyExchangeDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeDisplay, !xyz.swapee.wc.ChangellyExchangeMemory, null): void} */
xyz.swapee.wc.IChangellyExchangeDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeDisplay.__paint} */
xyz.swapee.wc.IChangellyExchangeDisplay.__paint

// nss:xyz.swapee.wc.IChangellyExchangeDisplay,$$xyz.swapee.wc.IChangellyExchangeDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @record */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.exchangeBrokerSel
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.dealBrokerSel
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.exchangeIntentSel
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.transactionInfoSel
/** @type {string|undefined} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.regionSelectorSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplay.Queries}
 */
xyz.swapee.wc.IChangellyExchangeDisplay.Settings = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeQueriesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.ChangellyExchangeQueriesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeQueriesPQs.prototype.exchangeBrokerSel

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeQueriesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeQueriesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeQueriesQPs.prototype.ecfe5

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ChangellyExchangeClasses>}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.Debug
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.ExchangeBroker
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.DealBroker
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.ExchangeIntent
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.TransactionInfo
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.RegionSelector

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.Debug
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.ExchangeBroker
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.DealBroker
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.ExchangeIntent
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.TransactionInfo
/** @type {!com.webcircuits.IHtmlTwin} */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.RegionSelector

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeDisplayCaster
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeDisplay} */
xyz.swapee.wc.back.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeDisplay
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeDisplay} */
xyz.swapee.wc.back.IChangellyExchangeDisplayCaster.prototype.superChangellyExchangeDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.ChangellyExchangeClasses, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [memory]
 * @param {!xyz.swapee.wc.ChangellyExchangeLand} [land]
 * @return {void}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplay.prototype.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.ChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IChangellyExchangeDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese>}
 */
xyz.swapee.wc.back.ChangellyExchangeDisplay = function() {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.ChangellyExchangeDisplay.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.AbstractChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay = function() {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.ChangellyExchangeVdusPQs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeVdusPQs.prototype.ExchangeBroker

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeVdusQPs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeVdusQPs.prototype.j4a11

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.RecordIChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @typedef {{ paint: xyz.swapee.wc.back.IChangellyExchangeDisplay.paint }} */
xyz.swapee.wc.back.RecordIChangellyExchangeDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.BoundIChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.ChangellyExchangeClasses, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
xyz.swapee.wc.back.BoundIChangellyExchangeDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.BoundChangellyExchangeDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundChangellyExchangeDisplay = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [memory]
 * @param {!xyz.swapee.wc.ChangellyExchangeLand} [land]
 * @return {void}
 */
$$xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ChangellyExchangeMemory=, !xyz.swapee.wc.ChangellyExchangeLand=): void} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IChangellyExchangeDisplay, !xyz.swapee.wc.ChangellyExchangeMemory=, !xyz.swapee.wc.ChangellyExchangeLand=): void} */
xyz.swapee.wc.back.IChangellyExchangeDisplay._paint
/** @typedef {typeof $$xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint

// nss:xyz.swapee.wc.back.IChangellyExchangeDisplay,$$xyz.swapee.wc.back.IChangellyExchangeDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeClassesPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
xyz.swapee.wc.ChangellyExchangeClassesPQs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeClassesPQs.prototype._

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeClassesQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
xyz.swapee.wc.ChangellyExchangeClassesQPs = function() {}
/** @type {string} */
xyz.swapee.wc.ChangellyExchangeClassesQPs.prototype.b14a7

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/41-ChangellyExchangeClasses.xml} xyz.swapee.wc.ChangellyExchangeClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b37b4f3129c85e11017205599969c599 */
/** @record */
xyz.swapee.wc.ChangellyExchangeClasses = function() {}
/** @type {string|undefined} */
xyz.swapee.wc.ChangellyExchangeClasses.prototype._

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.ChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeController.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeController = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.ChangellyExchangeController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.ChangellyExchangeController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.AbstractChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.ChangellyExchangeControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeController, ...!xyz.swapee.wc.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.BoundChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeController = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._resetPort
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__resetPort} */
xyz.swapee.wc.IChangellyExchangeController.__resetPort

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetOffer
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCreateTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCheckPayment
/** @typedef {typeof $$xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeController,$$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePort.WeakInputs}
 */
xyz.swapee.wc.IChangellyExchangeController.WeakInputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @record */
xyz.swapee.wc.front.IChangellyExchangeController.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @interface */
xyz.swapee.wc.front.IChangellyExchangeControllerCaster
/** @type {!xyz.swapee.wc.front.BoundIChangellyExchangeController} */
xyz.swapee.wc.front.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.front.BoundChangellyExchangeController} */
xyz.swapee.wc.front.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @interface */
xyz.swapee.wc.front.IChangellyExchangeControllerATCaster
/** @type {!xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT} */
xyz.swapee.wc.front.IChangellyExchangeControllerATCaster.prototype.asIChangellyExchangeControllerAT
/** @type {!xyz.swapee.wc.front.BoundChangellyExchangeControllerAT} */
xyz.swapee.wc.front.IChangellyExchangeControllerATCaster.prototype.superChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.front.IChangellyExchangeControllerAT = function() {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init */
xyz.swapee.wc.front.IChangellyExchangeControllerAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.IChangellyExchangeController = function() {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.constructor = function(...init) {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetFixedOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetOffer = function() {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCreateTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCreateFixedTransaction = function() {}
/** @return {void} */
xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCheckPayment = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.ChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.front.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IChangellyExchangeController.Initialese>}
 */
xyz.swapee.wc.front.ChangellyExchangeController = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.front.ChangellyExchangeController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.ChangellyExchangeController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.AbstractChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.ChangellyExchangeControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @typedef {function(new: xyz.swapee.wc.front.IChangellyExchangeController, ...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.RecordIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @typedef {{ loadGetFixedOffer: xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer, loadGetOffer: xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer, loadCreateTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction, loadGetTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction, loadCreateFixedTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction, loadCheckPayment: xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment }} */
xyz.swapee.wc.front.RecordIChangellyExchangeController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.BoundIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.BoundIChangellyExchangeController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.BoundChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundChangellyExchangeController = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetFixedOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetOffer
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCreateTransaction
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetTransaction
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCreateFixedTransaction
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$$xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCheckPayment
/** @typedef {typeof $$xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
xyz.swapee.wc.back.IChangellyExchangeController.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeControllerCaster
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeController} */
xyz.swapee.wc.back.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeController} */
xyz.swapee.wc.back.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
xyz.swapee.wc.back.IChangellyExchangeController = function() {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.back.IChangellyExchangeController.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.ChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeController.Initialese>}
 */
xyz.swapee.wc.back.ChangellyExchangeController = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init */
xyz.swapee.wc.back.ChangellyExchangeController.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.ChangellyExchangeController.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.AbstractChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.ChangellyExchangeControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeController, ...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.RecordIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.BoundIChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
xyz.swapee.wc.back.BoundIChangellyExchangeController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.BoundChangellyExchangeController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundChangellyExchangeController = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeControllerARCaster
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR} */
xyz.swapee.wc.back.IChangellyExchangeControllerARCaster.prototype.asIChangellyExchangeControllerAR
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeControllerAR} */
xyz.swapee.wc.back.IChangellyExchangeControllerARCaster.prototype.superChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 */
xyz.swapee.wc.back.IChangellyExchangeControllerAR = function() {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init */
xyz.swapee.wc.back.IChangellyExchangeControllerAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.ChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese>}
 */
xyz.swapee.wc.back.ChangellyExchangeControllerAR = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init */
xyz.swapee.wc.back.ChangellyExchangeControllerAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.ChangellyExchangeControllerAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.ChangellyExchangeControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeControllerAR, ...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 */
xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.BoundChangellyExchangeControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundChangellyExchangeControllerAR = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.ChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IChangellyExchangeControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese>}
 */
xyz.swapee.wc.front.ChangellyExchangeControllerAT = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init */
xyz.swapee.wc.front.ChangellyExchangeControllerAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.ChangellyExchangeControllerAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.ChangellyExchangeControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @typedef {function(new: xyz.swapee.wc.front.IChangellyExchangeControllerAT, ...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.BoundChangellyExchangeControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundChangellyExchangeControllerAT = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.IChangellyExchangeScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.front.ChangellyExchangeInputs, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, !xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplay.Initialese}
 */
xyz.swapee.wc.IChangellyExchangeScreen.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.IChangellyExchangeScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/** @interface */
xyz.swapee.wc.IChangellyExchangeScreenCaster
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeScreen} */
xyz.swapee.wc.IChangellyExchangeScreenCaster.prototype.asIChangellyExchangeScreen
/** @type {!xyz.swapee.wc.BoundChangellyExchangeScreen} */
xyz.swapee.wc.IChangellyExchangeScreenCaster.prototype.superChangellyExchangeScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.IChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.front.ChangellyExchangeInputs, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, !xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeController}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplay}
 */
xyz.swapee.wc.IChangellyExchangeScreen = function() {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeScreen.Initialese} init */
xyz.swapee.wc.IChangellyExchangeScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.ChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeScreen.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeScreen.Initialese>}
 */
xyz.swapee.wc.ChangellyExchangeScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.IChangellyExchangeScreen.Initialese} init */
xyz.swapee.wc.ChangellyExchangeScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeScreen}
 */
xyz.swapee.wc.ChangellyExchangeScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.AbstractChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeScreen.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeScreen}
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeScreen}
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeScreen}
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeScreen}
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeScreen}
 */
xyz.swapee.wc.AbstractChangellyExchangeScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.ChangellyExchangeScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeScreen, ...!xyz.swapee.wc.IChangellyExchangeScreen.Initialese)} */
xyz.swapee.wc.ChangellyExchangeScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.RecordIChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.BoundIChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.front.ChangellyExchangeInputs, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, !xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeController}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeDisplay}
 */
xyz.swapee.wc.BoundIChangellyExchangeScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreen.xml} xyz.swapee.wc.BoundChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 391d0d14ffecf68896861071e7755f61 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundChangellyExchangeScreen = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese}
 */
xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.IChangellyExchangeScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeScreenCaster
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeScreen} */
xyz.swapee.wc.back.IChangellyExchangeScreenCaster.prototype.asIChangellyExchangeScreen
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeScreen} */
xyz.swapee.wc.back.IChangellyExchangeScreenCaster.prototype.superChangellyExchangeScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.IChangellyExchangeScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/** @interface */
xyz.swapee.wc.back.IChangellyExchangeScreenATCaster
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeScreenAT} */
xyz.swapee.wc.back.IChangellyExchangeScreenATCaster.prototype.asIChangellyExchangeScreenAT
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeScreenAT} */
xyz.swapee.wc.back.IChangellyExchangeScreenATCaster.prototype.superChangellyExchangeScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.IChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
xyz.swapee.wc.back.IChangellyExchangeScreenAT = function() {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese} init */
xyz.swapee.wc.back.IChangellyExchangeScreenAT.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.IChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenCaster}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.IChangellyExchangeScreen = function() {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese} init */
xyz.swapee.wc.back.IChangellyExchangeScreen.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.ChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese>}
 */
xyz.swapee.wc.back.ChangellyExchangeScreen = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese} init */
xyz.swapee.wc.back.ChangellyExchangeScreen.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreen}
 */
xyz.swapee.wc.back.ChangellyExchangeScreen.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.AbstractChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeScreen}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreen}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreen}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreen}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreen}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreen.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.ChangellyExchangeScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeScreen, ...!xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.RecordIChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.RecordIChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.BoundIChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
xyz.swapee.wc.back.BoundIChangellyExchangeScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.BoundIChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.BoundIChangellyExchangeScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/70-IChangellyExchangeScreenBack.xml} xyz.swapee.wc.back.BoundChangellyExchangeScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 7150f86e3b31381362f9dcdcdc953cee */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundChangellyExchangeScreen = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeScreen.Initialese}
 */
xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.IChangellyExchangeScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/** @interface */
xyz.swapee.wc.front.IChangellyExchangeScreenARCaster
/** @type {!xyz.swapee.wc.front.BoundIChangellyExchangeScreenAR} */
xyz.swapee.wc.front.IChangellyExchangeScreenARCaster.prototype.asIChangellyExchangeScreenAR
/** @type {!xyz.swapee.wc.front.BoundChangellyExchangeScreenAR} */
xyz.swapee.wc.front.IChangellyExchangeScreenARCaster.prototype.superChangellyExchangeScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.IChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IChangellyExchangeScreen}
 */
xyz.swapee.wc.front.IChangellyExchangeScreenAR = function() {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese} init */
xyz.swapee.wc.front.IChangellyExchangeScreenAR.prototype.constructor = function(...init) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.ChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.IChangellyExchangeScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese>}
 */
xyz.swapee.wc.front.ChangellyExchangeScreenAR = function(...init) {}
/** @param {...!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese} init */
xyz.swapee.wc.front.ChangellyExchangeScreenAR.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.ChangellyExchangeScreenAR.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeScreenAR|typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeScreenAR|typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeScreenAR|typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeScreen|typeof xyz.swapee.wc.ChangellyExchangeScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeScreenAR}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.ChangellyExchangeScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/** @typedef {function(new: xyz.swapee.wc.front.IChangellyExchangeScreenAR, ...!xyz.swapee.wc.front.IChangellyExchangeScreenAR.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.RecordIChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIChangellyExchangeScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.BoundIChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIChangellyExchangeScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeScreen}
 */
xyz.swapee.wc.front.BoundIChangellyExchangeScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/73-IChangellyExchangeScreenAR.xml} xyz.swapee.wc.front.BoundChangellyExchangeScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c3942c61e1b75eab2c56c4c8a9e6ed49 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.front.BoundChangellyExchangeScreenAR = function() {}

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.ChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese>}
 */
xyz.swapee.wc.back.ChangellyExchangeScreenAT = function(...init) {}
/** @param {...!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese} init */
xyz.swapee.wc.back.ChangellyExchangeScreenAT.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.ChangellyExchangeScreenAT.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeScreenAT|typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeScreenAT}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.ChangellyExchangeScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeScreenAT, ...!xyz.swapee.wc.back.IChangellyExchangeScreenAT.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/74-IChangellyExchangeScreenAT.xml} xyz.swapee.wc.back.BoundChangellyExchangeScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c6e7e39865c631dd0b31df9fe58426cb */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.back.BoundChangellyExchangeScreenAT = function() {}

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe = function() {}
/** @type {string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe = function() {}
/** @type {?string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe.prototype.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe.prototype.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe = function() {}
/** @type {*} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe.prototype.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Region = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe.prototype.hasMoreGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe.prototype.hasMoreGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe.prototype.hasMoreCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe.prototype.loadingGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe.prototype.hasMoreGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe.prototype.loadGetTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe.prototype.hasMoreCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe = function() {}
/** @type {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe.prototype.loadingCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe = function() {}
/** @type {?boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe.prototype.hasMoreCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe = function() {}
/** @type {?Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe.prototype.loadCheckPaymentError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Host exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host}
 */
xyz.swapee.wc.IChangellyExchangeCore.Model.Host = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe}
 */
xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe}
 */
xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe = function() {}

// nss:xyz.swapee.wc
/* @typal-end */