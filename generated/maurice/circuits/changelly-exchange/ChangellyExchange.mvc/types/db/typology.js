/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IChangellyExchangeComputer': {
  'id': 20133923801,
  'symbols': {},
  'methods': {
   'adaptCreateTransaction': 1,
   'compute': 2,
   'adaptCheckPayment': 3,
   'adaptRegion': 4,
   'adaptGetOffer': 5,
   'adaptCreateFixedTransaction': 6,
   'adaptGetFixedOffer': 7,
   'adaptGettingOffer': 8,
   'adaptGetOfferError': 9,
   'adaptCreatingTransaction': 10,
   'adaptCreateTransactionError': 11,
   'adaptGetTransaction': 12
  }
 },
 'xyz.swapee.wc.ChangellyExchangeMemoryPQs': {
  'id': 20133923802,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeOuterCore': {
  'id': 20133923803,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ChangellyExchangeInputsPQs': {
  'id': 20133923804,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangePort': {
  'id': 20133923805,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetChangellyExchangePort': 2
  }
 },
 'xyz.swapee.wc.IChangellyExchangePortInterface': {
  'id': 20133923806,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ChangellyExchangeCachePQs': {
  'id': 20133923807,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeCore': {
  'id': 20133923808,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetChangellyExchangeCore': 2
  }
 },
 'xyz.swapee.wc.IChangellyExchangeProcessor': {
  'id': 20133923809,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchange': {
  'id': 201339238010,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeBuffer': {
  'id': 201339238011,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponentUtil': {
  'id': 201339238012,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeHtmlComponent': {
  'id': 201339238013,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeElement': {
  'id': 201339238014,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeBroker': 4,
   'short': 5,
   'server': 6,
   'inducer': 7,
   'buildRegionSelector': 8,
   'buildDealBroker': 9,
   'buildExchangeIntent': 10,
   'buildTransactionInfo': 12
  }
 },
 'xyz.swapee.wc.IChangellyExchangeElementPort': {
  'id': 201339238015,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeRadio': {
  'id': 201339238016,
  'symbols': {},
  'methods': {
   'adaptLoadCreateTransaction': 1,
   'adaptLoadCheckPayment': 2,
   'adaptLoadGetOffer': 3,
   'adaptLoadCreateFixedTransaction': 4,
   'adaptLoadGetFixedOffer': 5,
   'adaptLoadGetTransaction': 6
  }
 },
 'xyz.swapee.wc.IChangellyExchangeDesigner': {
  'id': 201339238017,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IChangellyExchangeService': {
  'id': 201339238018,
  'symbols': {},
  'methods': {
   'filterCreateTransaction': 1,
   'filterCheckPayment': 2,
   'filterGetOffer': 3,
   'filterCreateFixedTransaction': 4,
   'filterGetFixedOffer': 5,
   'filterGetTransaction': 6
  }
 },
 'xyz.swapee.wc.IChangellyExchangeGPU': {
  'id': 201339238019,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeDisplay': {
  'id': 201339238020,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeQueriesPQs': {
  'id': 201339238021,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeVdusPQs': {
  'id': 201339238022,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeDisplay': {
  'id': 201339238023,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ChangellyExchangeClassesPQs': {
  'id': 201339238024,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IChangellyExchangeController': {
  'id': 201339238025,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'loadCreateTransaction': 2,
   'loadCheckPayment': 3,
   'loadGetOffer': 4,
   'loadCreateFixedTransaction': 5,
   'loadGetFixedOffer': 6,
   'loadGetTransaction': 7
  }
 },
 'xyz.swapee.wc.front.IChangellyExchangeController': {
  'id': 201339238026,
  'symbols': {},
  'methods': {
   'loadCreateTransaction': 1,
   'loadCheckPayment': 2,
   'loadGetOffer': 3,
   'loadCreateFixedTransaction': 4,
   'loadGetFixedOffer': 5,
   'loadGetTransaction': 6
  }
 },
 'xyz.swapee.wc.back.IChangellyExchangeController': {
  'id': 201339238027,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeControllerAR': {
  'id': 201339238028,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeControllerAT': {
  'id': 201339238029,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IChangellyExchangeScreen': {
  'id': 201339238030,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeScreen': {
  'id': 201339238031,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IChangellyExchangeScreenAR': {
  'id': 201339238032,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IChangellyExchangeScreenAT': {
  'id': 201339238033,
  'symbols': {},
  'methods': {}
 }
})