/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeController
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.Initialese filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 */
$xyz.swapee.wc.IChangellyExchangeController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeController.Initialese} */
xyz.swapee.wc.IChangellyExchangeController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeControllerFields filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeControllerFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeController.Inputs} */
$xyz.swapee.wc.IChangellyExchangeControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeControllerFields}
 */
xyz.swapee.wc.IChangellyExchangeControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeControllerCaster filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeController} */
$xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeProcessor} */
$xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeProcessor
/** @type {!xyz.swapee.wc.BoundChangellyExchangeController} */
$xyz.swapee.wc.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeControllerCaster}
 */
xyz.swapee.wc.IChangellyExchangeControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
$xyz.swapee.wc.IChangellyExchangeController = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadCreateTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadGetTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadCreateFixedTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeController.prototype.loadCheckPayment = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeController}
 */
xyz.swapee.wc.IChangellyExchangeController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.ChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeController.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.ChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeController, ...!xyz.swapee.wc.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.ChangellyExchangeController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.ChangellyExchangeController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.AbstractChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeController}
 */
$xyz.swapee.wc.AbstractChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeController)} */
xyz.swapee.wc.AbstractChangellyExchangeController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeController}
 */
xyz.swapee.wc.AbstractChangellyExchangeController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.ChangellyExchangeControllerConstructor filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeController, ...!xyz.swapee.wc.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.RecordIChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/** @typedef {{ resetPort: xyz.swapee.wc.IChangellyExchangeController.resetPort, loadGetFixedOffer: xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer, loadGetOffer: xyz.swapee.wc.IChangellyExchangeController.loadGetOffer, loadCreateTransaction: xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction, loadGetTransaction: xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction, loadCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction, loadCheckPayment: xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment }} */
xyz.swapee.wc.RecordIChangellyExchangeController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.BoundIChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.IChangellyExchangeController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
$xyz.swapee.wc.BoundIChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeController} */
xyz.swapee.wc.BoundIChangellyExchangeController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.BoundChangellyExchangeController filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeController} */
xyz.swapee.wc.BoundChangellyExchangeController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.resetPort filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.resetPort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._resetPort
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__resetPort} */
xyz.swapee.wc.IChangellyExchangeController.__resetPort

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetOffer filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetOffer
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadGetTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadGetTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeController.__loadCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeController.loadCheckPayment
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeController): void} */
xyz.swapee.wc.IChangellyExchangeController._loadCheckPayment
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeController.__loadCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeController,$xyz.swapee.wc.IChangellyExchangeController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.Inputs filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePort.Inputs}
 */
$xyz.swapee.wc.IChangellyExchangeController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeController.Inputs} */
xyz.swapee.wc.IChangellyExchangeController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/50-IChangellyExchangeController.xml} xyz.swapee.wc.IChangellyExchangeController.WeakInputs filter:!ControllerPlugin~props 724bb9d374e21fc85dd1a6e1a4206e47 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePort.WeakInputs}
 */
$xyz.swapee.wc.IChangellyExchangeController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeController.WeakInputs} */
xyz.swapee.wc.IChangellyExchangeController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeController
/* @typal-end */