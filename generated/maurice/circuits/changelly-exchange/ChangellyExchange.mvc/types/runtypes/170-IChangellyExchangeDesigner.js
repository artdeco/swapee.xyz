/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeDesigner
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeDesigner = function() {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeDesigner}
 */
xyz.swapee.wc.IChangellyExchangeDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.ChangellyExchangeDesigner filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeDesigner}
 */
$xyz.swapee.wc.ChangellyExchangeDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeDesigner}
 */
xyz.swapee.wc.ChangellyExchangeDesigner
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeDesigner)} */
xyz.swapee.wc.ChangellyExchangeDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.RegionSelector
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh.prototype.ChangellyExchange
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh} */
xyz.swapee.wc.IChangellyExchangeDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.RegionSelector
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.ChangellyExchange
/** @type {typeof xyz.swapee.wc.IChangellyExchangeController} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/170-IChangellyExchangeDesigner.xml} xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool filter:!ControllerPlugin~props 61085dc94dbacb0d0e874c80bc13a472 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.RegionSelector
/** @type {!xyz.swapee.wc.ChangellyExchangeMemory} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.ChangellyExchange
/** @type {!xyz.swapee.wc.ChangellyExchangeMemory} */
$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool} */
xyz.swapee.wc.IChangellyExchangeDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDesigner.relay
/* @typal-end */