/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeOuterCore
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeOuterCore.Model
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangePort
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangePort.Inputs
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangePort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeCore
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCoreFields filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeOuterCore.Model} */
$xyz.swapee.wc.IChangellyExchangeOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeOuterCoreFields}
 */
xyz.swapee.wc.IChangellyExchangeOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCoreCaster filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeOuterCore} */
$xyz.swapee.wc.IChangellyExchangeOuterCoreCaster.prototype.asIChangellyExchangeOuterCore
/** @type {!xyz.swapee.wc.BoundChangellyExchangeOuterCore} */
$xyz.swapee.wc.IChangellyExchangeOuterCoreCaster.prototype.superChangellyExchangeOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeOuterCoreCaster}
 */
xyz.swapee.wc.IChangellyExchangeOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreCaster}
 */
$xyz.swapee.wc.IChangellyExchangeOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeOuterCore}
 */
xyz.swapee.wc.IChangellyExchangeOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.ChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeOuterCore.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.ChangellyExchangeOuterCore
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeOuterCore)} */
xyz.swapee.wc.ChangellyExchangeOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.ChangellyExchangeOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.AbstractChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
$xyz.swapee.wc.AbstractChangellyExchangeOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeOuterCore)} */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeOuterCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.RecordIChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.BoundIChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCoreCaster}
 */
$xyz.swapee.wc.BoundIChangellyExchangeOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeOuterCore} */
xyz.swapee.wc.BoundIChangellyExchangeOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.BoundChangellyExchangeOuterCore filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeOuterCore} */
xyz.swapee.wc.BoundChangellyExchangeOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.host

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @typedef {string} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.region

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host.prototype.host
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region = function() {}
/** @type {(?string)|undefined} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region.prototype.region
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region}
 */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Model} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host.prototype.host
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region.prototype.region
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe.prototype.host
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe = function() {}
/** @type {?string} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe.prototype.region
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe} */
xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe.prototype.host
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe.prototype.region
/** @typedef {$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe} */
xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 */
$xyz.swapee.wc.IChangellyExchangePort.Inputs.Host = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.Inputs.Host} */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe}
 */
$xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe} */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
$xyz.swapee.wc.IChangellyExchangePort.Inputs.Region = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.Inputs.Region} */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Region

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe}
 */
$xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe} */
xyz.swapee.wc.IChangellyExchangePort.Inputs.Region_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host}
 */
$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host} */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Host_Safe}
 */
$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe} */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region}
 */
$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region} */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel.Region_Safe}
 */
$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe} */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs.Region_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Host filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host}
 */
$xyz.swapee.wc.IChangellyExchangeCore.Model.Host = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.Host} */
xyz.swapee.wc.IChangellyExchangeCore.Model.Host

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Host_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.Host_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Region filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region}
 */
$xyz.swapee.wc.IChangellyExchangeCore.Model.Region = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.Region} */
xyz.swapee.wc.IChangellyExchangeCore.Model.Region

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/03-IChangellyExchangeOuterCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe filter:!ControllerPlugin~props df46fbefd48811d8d4a7dc4654a4a654 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model.Region_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.Region_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */