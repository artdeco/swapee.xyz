/** @const {?} */ $xyz.swapee.wc.IChangellyExchangePort
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangePortInterface
/** @const {?} */ xyz.swapee.wc.IChangellyExchangePort
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.Initialese filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangePort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangePort.Initialese} */
xyz.swapee.wc.IChangellyExchangePort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortFields filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
$xyz.swapee.wc.IChangellyExchangePortFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangePort.Inputs} */
$xyz.swapee.wc.IChangellyExchangePortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IChangellyExchangePort.Inputs} */
$xyz.swapee.wc.IChangellyExchangePortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangePortFields}
 */
xyz.swapee.wc.IChangellyExchangePortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortCaster filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
$xyz.swapee.wc.IChangellyExchangePortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangePort} */
$xyz.swapee.wc.IChangellyExchangePortCaster.prototype.asIChangellyExchangePort
/** @type {!xyz.swapee.wc.BoundChangellyExchangePort} */
$xyz.swapee.wc.IChangellyExchangePortCaster.prototype.superChangellyExchangePort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangePortCaster}
 */
xyz.swapee.wc.IChangellyExchangePortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangePortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangePortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IChangellyExchangePort.Inputs>}
 */
$xyz.swapee.wc.IChangellyExchangePort = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangePort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangePort.prototype.resetChangellyExchangePort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangePort}
 */
xyz.swapee.wc.IChangellyExchangePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangePort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangePort.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangePort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.ChangellyExchangePort
/** @type {function(new: xyz.swapee.wc.IChangellyExchangePort, ...!xyz.swapee.wc.IChangellyExchangePort.Initialese)} */
xyz.swapee.wc.ChangellyExchangePort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.ChangellyExchangePort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.AbstractChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangePort}
 */
$xyz.swapee.wc.AbstractChangellyExchangePort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangePort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangePort)} */
xyz.swapee.wc.AbstractChangellyExchangePort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangePort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangePort|typeof xyz.swapee.wc.ChangellyExchangePort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangePort}
 */
xyz.swapee.wc.AbstractChangellyExchangePort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePortConstructor filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangePort, ...!xyz.swapee.wc.IChangellyExchangePort.Initialese)} */
xyz.swapee.wc.ChangellyExchangePortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.RecordIChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @typedef {{ resetPort: xyz.swapee.wc.IChangellyExchangePort.resetPort, resetChangellyExchangePort: xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort }} */
xyz.swapee.wc.RecordIChangellyExchangePort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.BoundIChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePortFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangePort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangePortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IChangellyExchangePort.Inputs>}
 */
$xyz.swapee.wc.BoundIChangellyExchangePort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangePort} */
xyz.swapee.wc.BoundIChangellyExchangePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.BoundChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangePort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangePort = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangePort} */
xyz.swapee.wc.BoundChangellyExchangePort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.resetPort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangePort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangePort.resetPort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangePort): void} */
xyz.swapee.wc.IChangellyExchangePort._resetPort
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangePort.__resetPort} */
xyz.swapee.wc.IChangellyExchangePort.__resetPort

// nss:xyz.swapee.wc.IChangellyExchangePort,$xyz.swapee.wc.IChangellyExchangePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangePort.resetChangellyExchangePort
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangePort): void} */
xyz.swapee.wc.IChangellyExchangePort._resetChangellyExchangePort
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort} */
xyz.swapee.wc.IChangellyExchangePort.__resetChangellyExchangePort

// nss:xyz.swapee.wc.IChangellyExchangePort,$xyz.swapee.wc.IChangellyExchangePort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.Inputs filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel}
 */
$xyz.swapee.wc.IChangellyExchangePort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IChangellyExchangePort.Inputs}
 */
xyz.swapee.wc.IChangellyExchangePort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePort.WeakInputs filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.WeakModel}
 */
$xyz.swapee.wc.IChangellyExchangePort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IChangellyExchangePort.WeakInputs}
 */
xyz.swapee.wc.IChangellyExchangePort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortInterface filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @interface */
$xyz.swapee.wc.IChangellyExchangePortInterface = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangePortInterface}
 */
xyz.swapee.wc.IChangellyExchangePortInterface

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.ChangellyExchangePortInterface filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangePortInterface}
 */
$xyz.swapee.wc.ChangellyExchangePortInterface = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangePortInterface}
 */
xyz.swapee.wc.ChangellyExchangePortInterface
/** @type {function(new: xyz.swapee.wc.IChangellyExchangePortInterface)} */
xyz.swapee.wc.ChangellyExchangePortInterface.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/04-IChangellyExchangePort.xml} xyz.swapee.wc.IChangellyExchangePortInterface.Props filter:!ControllerPlugin~props bf133ff04f92f72cc71e7b1493ef600b */
/** @record */
$xyz.swapee.wc.IChangellyExchangePortInterface.Props = function() {}
/** @type {string} */
$xyz.swapee.wc.IChangellyExchangePortInterface.Props.prototype.host
/** @type {?string} */
$xyz.swapee.wc.IChangellyExchangePortInterface.Props.prototype.region
/** @typedef {$xyz.swapee.wc.IChangellyExchangePortInterface.Props} */
xyz.swapee.wc.IChangellyExchangePortInterface.Props

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangePortInterface
/* @typal-end */