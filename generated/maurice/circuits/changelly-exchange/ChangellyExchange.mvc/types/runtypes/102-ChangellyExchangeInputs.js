/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/102-ChangellyExchangeInputs.xml} xyz.swapee.wc.front.ChangellyExchangeInputs filter:!ControllerPlugin~props 8927ef629fd1fb022b52d17fc4b31d4b */
/** @record */
$xyz.swapee.wc.front.ChangellyExchangeInputs = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.front.ChangellyExchangeInputs.prototype.host
/** @type {(?string)|undefined} */
$xyz.swapee.wc.front.ChangellyExchangeInputs.prototype.region
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.ChangellyExchangeInputs}
 */
xyz.swapee.wc.front.ChangellyExchangeInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */