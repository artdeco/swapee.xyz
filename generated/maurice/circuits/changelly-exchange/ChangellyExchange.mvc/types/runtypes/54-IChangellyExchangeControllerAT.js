/** @const {?} */ $xyz.swapee.wc.front.IChangellyExchangeControllerAT
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} */
xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IChangellyExchangeControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerATCaster filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @interface */
$xyz.swapee.wc.front.IChangellyExchangeControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT} */
$xyz.swapee.wc.front.IChangellyExchangeControllerATCaster.prototype.asIChangellyExchangeControllerAT
/** @type {!xyz.swapee.wc.front.BoundChangellyExchangeControllerAT} */
$xyz.swapee.wc.front.IChangellyExchangeControllerATCaster.prototype.superChangellyExchangeControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IChangellyExchangeControllerATCaster}
 */
xyz.swapee.wc.front.IChangellyExchangeControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.IChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.IChangellyExchangeControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.IChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.ChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.IChangellyExchangeControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.ChangellyExchangeControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.ChangellyExchangeControllerAT
/** @type {function(new: xyz.swapee.wc.front.IChangellyExchangeControllerAT, ...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.ChangellyExchangeControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
$xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT)} */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.ChangellyExchangeControllerATConstructor filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @typedef {function(new: xyz.swapee.wc.front.IChangellyExchangeControllerAT, ...!xyz.swapee.wc.front.IChangellyExchangeControllerAT.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIChangellyExchangeControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT} */
xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/54-IChangellyExchangeControllerAT.xml} xyz.swapee.wc.front.BoundChangellyExchangeControllerAT filter:!ControllerPlugin~props 00be104bbc1271e5f0a7306da1e34820 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundChangellyExchangeControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundChangellyExchangeControllerAT} */
xyz.swapee.wc.front.BoundChangellyExchangeControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */