/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeComputer.compute
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeComputer
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.Initialese filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.Initialese} */
xyz.swapee.wc.IChangellyExchangeComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputerCaster filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeComputer} */
$xyz.swapee.wc.IChangellyExchangeComputerCaster.prototype.asIChangellyExchangeComputer
/** @type {!xyz.swapee.wc.BoundChangellyExchangeComputer} */
$xyz.swapee.wc.IChangellyExchangeComputerCaster.prototype.superChangellyExchangeComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeComputerCaster}
 */
xyz.swapee.wc.IChangellyExchangeComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.IChangellyExchangeComputer = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCheckPayment = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptRegion = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGettingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptGetOfferError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreatingTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.adaptCreateTransactionError = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.prototype.compute = function(mem, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeComputer}
 */
xyz.swapee.wc.IChangellyExchangeComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.ChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeComputer.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.ChangellyExchangeComputer
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeComputer, ...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese)} */
xyz.swapee.wc.ChangellyExchangeComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.ChangellyExchangeComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.AbstractChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeComputer}
 */
$xyz.swapee.wc.AbstractChangellyExchangeComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeComputer)} */
xyz.swapee.wc.AbstractChangellyExchangeComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeComputer}
 */
xyz.swapee.wc.AbstractChangellyExchangeComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.ChangellyExchangeComputerConstructor filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeComputer, ...!xyz.swapee.wc.IChangellyExchangeComputer.Initialese)} */
xyz.swapee.wc.ChangellyExchangeComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.RecordIChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @typedef {{ adaptGetFixedOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer, adaptGetOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer, adaptCreateTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction, adaptGetTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction, adaptCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction, adaptCheckPayment: xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment, adaptRegion: xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion, adaptGettingOffer: xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer, adaptGetOfferError: xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError, adaptCreatingTransaction: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction, adaptCreateTransactionError: xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError, compute: xyz.swapee.wc.IChangellyExchangeComputer.compute }} */
xyz.swapee.wc.RecordIChangellyExchangeComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.BoundIChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.ChangellyExchangeMemory>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.BoundIChangellyExchangeComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeComputer} */
xyz.swapee.wc.BoundIChangellyExchangeComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.BoundChangellyExchangeComputer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeComputer} */
xyz.swapee.wc.BoundChangellyExchangeComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptGetFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOffer} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptGetTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptGetTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateFixedTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptCheckPayment = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptCheckPayment} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCheckPayment
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptRegion = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptRegion} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptRegion
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptRegion

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptGettingOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptGettingOffer} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGettingOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGettingOffer

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOfferError = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOfferError} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptGetOfferError
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptGetOfferError

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptCreatingTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptCreatingTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreatingTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreatingTransaction

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransactionError = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} form
 * @param {xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} changes
 * @return {(undefined|xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransactionError} */
xyz.swapee.wc.IChangellyExchangeComputer._adaptCreateTransactionError
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError} */
xyz.swapee.wc.IChangellyExchangeComputer.__adaptCreateTransactionError

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.compute filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.compute = function(mem, land) {}
/**
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 * @this {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer._compute = function(mem, land) {}
/**
 * @template THIS
 * @param {xyz.swapee.wc.ChangellyExchangeMemory} mem
 * @param {!xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} land
 * @return {void}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.__compute = function(mem, land) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.compute} */
xyz.swapee.wc.IChangellyExchangeComputer.compute
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer._compute} */
xyz.swapee.wc.IChangellyExchangeComputer._compute
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeComputer.__compute} */
xyz.swapee.wc.IChangellyExchangeComputer.__compute

// nss:xyz.swapee.wc.IChangellyExchangeComputer,$xyz.swapee.wc.IChangellyExchangeComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CheckPayment_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CheckPaymentError}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IRegionSelectorCore.Model.Region_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.Region}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GettingOffer}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOfferError}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatingTransaction}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 */
$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return} */
xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/02-IChangellyExchangeComputer.xml} xyz.swapee.wc.IChangellyExchangeComputer.compute.Land filter:!ControllerPlugin~props 933aa2c9ed296905b5ae130b49b2f699 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeComputer.compute.Land} */
xyz.swapee.wc.IChangellyExchangeComputer.compute.Land

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeComputer.compute
/* @typal-end */