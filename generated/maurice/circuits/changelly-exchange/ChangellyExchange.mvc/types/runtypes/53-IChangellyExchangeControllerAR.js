/** @const {?} */ $xyz.swapee.wc.back.IChangellyExchangeControllerAR
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
$xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} */
xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IChangellyExchangeControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerARCaster filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @interface */
$xyz.swapee.wc.back.IChangellyExchangeControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR} */
$xyz.swapee.wc.back.IChangellyExchangeControllerARCaster.prototype.asIChangellyExchangeControllerAR
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeControllerAR} */
$xyz.swapee.wc.back.IChangellyExchangeControllerARCaster.prototype.superChangellyExchangeControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeControllerARCaster}
 */
xyz.swapee.wc.back.IChangellyExchangeControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.IChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 */
$xyz.swapee.wc.back.IChangellyExchangeControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.IChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.ChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.ChangellyExchangeControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.ChangellyExchangeControllerAR
/** @type {function(new: xyz.swapee.wc.back.IChangellyExchangeControllerAR, ...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.ChangellyExchangeControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
$xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR)} */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeControllerAR|typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeControllerAR}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.ChangellyExchangeControllerARConstructor filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeControllerAR, ...!xyz.swapee.wc.back.IChangellyExchangeControllerAR.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 */
$xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR} */
xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/53-IChangellyExchangeControllerAR.xml} xyz.swapee.wc.back.BoundChangellyExchangeControllerAR filter:!ControllerPlugin~props ce3865a5e4ecc499efad6e91c83e14ff */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundChangellyExchangeControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundChangellyExchangeControllerAR} */
xyz.swapee.wc.back.BoundChangellyExchangeControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */