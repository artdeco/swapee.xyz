/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElement
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElement.build
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElement.short
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeElement
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.Initialese filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @record
 * @extends {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.IExchangeBroker|xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.ITransactionInfo|xyz.swapee.wc.IRegionSelector)>}
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeElement.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.Initialese} */
xyz.swapee.wc.IChangellyExchangeElement.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElementFields filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeElementFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} */
$xyz.swapee.wc.IChangellyExchangeElementFields.prototype.inputs
/** @type {!Object<string, !Object<string, number>>} */
$xyz.swapee.wc.IChangellyExchangeElementFields.prototype.buildees
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementFields}
 */
xyz.swapee.wc.IChangellyExchangeElementFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElementCaster filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeElementCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeElement} */
$xyz.swapee.wc.IChangellyExchangeElementCaster.prototype.asIChangellyExchangeElement
/** @type {!xyz.swapee.wc.BoundChangellyExchangeElement} */
$xyz.swapee.wc.IChangellyExchangeElementCaster.prototype.superChangellyExchangeElement
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementCaster}
 */
xyz.swapee.wc.IChangellyExchangeElementCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeElement.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {guest.maurice.IMilleu<!(xyz.swapee.wc.IExchangeBroker|xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.ITransactionInfo|xyz.swapee.wc.IRegionSelector)>}
 */
$xyz.swapee.wc.IChangellyExchangeElement = function() {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Instances} instances
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.build = function(cores, instances) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.buildExchangeBroker = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.buildDealBroker = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.buildExchangeIntent = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.buildTransactionInfo = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.buildRegionSelector = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Cores} cores
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} [port]
 * @return {?}
 */
$xyz.swapee.wc.IChangellyExchangeElement.prototype.inducer = function(model, port) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElement}
 */
xyz.swapee.wc.IChangellyExchangeElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.ChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElement.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeElement.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeElement}
 */
xyz.swapee.wc.ChangellyExchangeElement
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeElement, ...!xyz.swapee.wc.IChangellyExchangeElement.Initialese)} */
xyz.swapee.wc.ChangellyExchangeElement.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElement}
 */
xyz.swapee.wc.ChangellyExchangeElement.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.AbstractChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElement.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeElement}
 */
$xyz.swapee.wc.AbstractChangellyExchangeElement = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElement.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeElement}
 */
xyz.swapee.wc.AbstractChangellyExchangeElement
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeElement)} */
xyz.swapee.wc.AbstractChangellyExchangeElement.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElement|typeof xyz.swapee.wc.ChangellyExchangeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeElement.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeElement}
 */
xyz.swapee.wc.AbstractChangellyExchangeElement.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElement}
 */
xyz.swapee.wc.AbstractChangellyExchangeElement.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElement|typeof xyz.swapee.wc.ChangellyExchangeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElement}
 */
xyz.swapee.wc.AbstractChangellyExchangeElement.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElement|typeof xyz.swapee.wc.ChangellyExchangeElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElement}
 */
xyz.swapee.wc.AbstractChangellyExchangeElement.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.ChangellyExchangeElementConstructor filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeElement, ...!xyz.swapee.wc.IChangellyExchangeElement.Initialese)} */
xyz.swapee.wc.ChangellyExchangeElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.RecordIChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @typedef {{ solder: xyz.swapee.wc.IChangellyExchangeElement.solder, render: xyz.swapee.wc.IChangellyExchangeElement.render, build: xyz.swapee.wc.IChangellyExchangeElement.build, buildExchangeBroker: xyz.swapee.wc.IChangellyExchangeElement.buildExchangeBroker, buildDealBroker: xyz.swapee.wc.IChangellyExchangeElement.buildDealBroker, buildExchangeIntent: xyz.swapee.wc.IChangellyExchangeElement.buildExchangeIntent, buildTransactionInfo: xyz.swapee.wc.IChangellyExchangeElement.buildTransactionInfo, buildRegionSelector: xyz.swapee.wc.IChangellyExchangeElement.buildRegionSelector, short: xyz.swapee.wc.IChangellyExchangeElement.short, server: xyz.swapee.wc.IChangellyExchangeElement.server, inducer: xyz.swapee.wc.IChangellyExchangeElement.inducer }} */
xyz.swapee.wc.RecordIChangellyExchangeElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.BoundIChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeElementFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeElement.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {guest.maurice.BoundIMilleu<!(xyz.swapee.wc.IExchangeBroker|xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IExchangeIntent|xyz.swapee.wc.ITransactionInfo|xyz.swapee.wc.IRegionSelector)>}
 */
$xyz.swapee.wc.BoundIChangellyExchangeElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeElement} */
xyz.swapee.wc.BoundIChangellyExchangeElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.BoundChangellyExchangeElement filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeElement = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeElement} */
xyz.swapee.wc.BoundChangellyExchangeElement

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.solder filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} props
 * @return {Object<string, *>}
 */
$xyz.swapee.wc.IChangellyExchangeElement.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} props
 * @return {Object<string, *>}
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._solder = function(model, props) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} props
 * @return {Object<string, *>}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__solder = function(model, props) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.solder} */
xyz.swapee.wc.IChangellyExchangeElement.solder
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._solder} */
xyz.swapee.wc.IChangellyExchangeElement._solder
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__solder} */
xyz.swapee.wc.IChangellyExchangeElement.__solder

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.render filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.ChangellyExchangeMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IChangellyExchangeElement.render
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeElement, !xyz.swapee.wc.ChangellyExchangeMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.IChangellyExchangeElement._render
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__render} */
xyz.swapee.wc.IChangellyExchangeElement.__render

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.build filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Instances} instances
 */
$xyz.swapee.wc.IChangellyExchangeElement.build = function(cores, instances) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Instances} instances
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._build = function(cores, instances) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Cores} cores
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.build.Instances} instances
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__build = function(cores, instances) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.build} */
xyz.swapee.wc.IChangellyExchangeElement.build
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._build} */
xyz.swapee.wc.IChangellyExchangeElement._build
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__build} */
xyz.swapee.wc.IChangellyExchangeElement.__build

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.buildExchangeBroker filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IChangellyExchangeElement.buildExchangeBroker = function(model, instance) {}
/**
 * @param {!Object} model
 * @param {!Object} instance
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._buildExchangeBroker = function(model, instance) {}
/**
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeBroker = function(model, instance) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.buildExchangeBroker} */
xyz.swapee.wc.IChangellyExchangeElement.buildExchangeBroker
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._buildExchangeBroker} */
xyz.swapee.wc.IChangellyExchangeElement._buildExchangeBroker
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeBroker} */
xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeBroker

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.buildDealBroker filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IChangellyExchangeElement.__buildDealBroker = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement.buildDealBroker
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeElement, !Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement._buildDealBroker
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__buildDealBroker} */
xyz.swapee.wc.IChangellyExchangeElement.__buildDealBroker

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.buildExchangeIntent filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeIntent = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement.buildExchangeIntent
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeElement, !Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement._buildExchangeIntent
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeIntent} */
xyz.swapee.wc.IChangellyExchangeElement.__buildExchangeIntent

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.buildTransactionInfo filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IChangellyExchangeElement.__buildTransactionInfo = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement.buildTransactionInfo
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeElement, !Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement._buildTransactionInfo
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__buildTransactionInfo} */
xyz.swapee.wc.IChangellyExchangeElement.__buildTransactionInfo

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.buildRegionSelector filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @this {THIS}
 * @template THIS
 * @param {!Object} model
 * @param {!Object} instance
 */
$xyz.swapee.wc.IChangellyExchangeElement.__buildRegionSelector = function(model, instance) {}
/** @typedef {function(!Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement.buildRegionSelector
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeElement, !Object, !Object)} */
xyz.swapee.wc.IChangellyExchangeElement._buildRegionSelector
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__buildRegionSelector} */
xyz.swapee.wc.IChangellyExchangeElement.__buildRegionSelector

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.short filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Cores} cores
 */
$xyz.swapee.wc.IChangellyExchangeElement.short = function(model, ports, cores) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Cores} cores
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._short = function(model, ports, cores) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} model
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Ports} ports
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.short.Cores} cores
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__short = function(model, ports, cores) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.short} */
xyz.swapee.wc.IChangellyExchangeElement.short
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._short} */
xyz.swapee.wc.IChangellyExchangeElement._short
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__short} */
xyz.swapee.wc.IChangellyExchangeElement.__short

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.server filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.IChangellyExchangeElement.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._server = function(memory, inputs) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__server = function(memory, inputs) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.server} */
xyz.swapee.wc.IChangellyExchangeElement.server
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._server} */
xyz.swapee.wc.IChangellyExchangeElement._server
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__server} */
xyz.swapee.wc.IChangellyExchangeElement.__server

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.inducer filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} [port]
 */
$xyz.swapee.wc.IChangellyExchangeElement.inducer = function(model, port) {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} [port]
 * @this {xyz.swapee.wc.IChangellyExchangeElement}
 */
$xyz.swapee.wc.IChangellyExchangeElement._inducer = function(model, port) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [model]
 * @param {!xyz.swapee.wc.IChangellyExchangeElement.Inputs} [port]
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeElement.__inducer = function(model, port) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.inducer} */
xyz.swapee.wc.IChangellyExchangeElement.inducer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement._inducer} */
xyz.swapee.wc.IChangellyExchangeElement._inducer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeElement.__inducer} */
xyz.swapee.wc.IChangellyExchangeElement.__inducer

// nss:xyz.swapee.wc.IChangellyExchangeElement,$xyz.swapee.wc.IChangellyExchangeElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.Inputs filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangePort.Inputs}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplay.Queries}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs}
 */
$xyz.swapee.wc.IChangellyExchangeElement.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.Inputs} */
xyz.swapee.wc.IChangellyExchangeElement.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.build.Cores filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Cores.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.build.Cores} */
xyz.swapee.wc.IChangellyExchangeElement.build.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.build.Instances filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.build.Instances.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.build.Instances} */
xyz.swapee.wc.IChangellyExchangeElement.build.Instances

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement.build
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.short.Ports filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Ports.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.short.Ports} */
xyz.swapee.wc.IChangellyExchangeElement.short.Ports

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement.short
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/130-IChangellyExchangeElement.xml} xyz.swapee.wc.IChangellyExchangeElement.short.Cores filter:!ControllerPlugin~props 31e5f4c5ddcc7f0fc3745415c8dee6be */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores.prototype.ExchangeBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores.prototype.DealBroker
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores.prototype.ExchangeIntent
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores.prototype.TransactionInfo
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElement.short.Cores.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElement.short.Cores} */
xyz.swapee.wc.IChangellyExchangeElement.short.Cores

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElement.short
/* @typal-end */