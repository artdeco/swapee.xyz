/** @const {?} */ $xyz.swapee.wc.back.IChangellyExchangeDisplay
/** @const {?} */ xyz.swapee.wc.back.IChangellyExchangeDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ChangellyExchangeClasses>}
 */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese = function() {}
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.Debug
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.ExchangeBroker
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.DealBroker
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.ExchangeIntent
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.TransactionInfo
/** @type {(!com.webcircuits.IHtmlTwin)|undefined} */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IChangellyExchangeDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplayFields filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @interface */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields = function() {}
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.Debug
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.ExchangeBroker
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.DealBroker
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.ExchangeIntent
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.TransactionInfo
/** @type {!com.webcircuits.IHtmlTwin} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayFields.prototype.RegionSelector
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeDisplayFields}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplayFields

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplayCaster filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @interface */
$xyz.swapee.wc.back.IChangellyExchangeDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeDisplay} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeDisplay
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeDisplay} */
$xyz.swapee.wc.back.IChangellyExchangeDisplayCaster.prototype.superChangellyExchangeDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeDisplayCaster}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @interface
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.ChangellyExchangeClasses, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.back.IChangellyExchangeDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [memory]
 * @param {!xyz.swapee.wc.ChangellyExchangeLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.IChangellyExchangeDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.ChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.IChangellyExchangeDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese>}
 */
$xyz.swapee.wc.back.ChangellyExchangeDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.ChangellyExchangeDisplay
/** @type {function(new: xyz.swapee.wc.back.IChangellyExchangeDisplay)} */
xyz.swapee.wc.back.ChangellyExchangeDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.ChangellyExchangeDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.AbstractChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
$xyz.swapee.wc.back.AbstractChangellyExchangeDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractChangellyExchangeDisplay)} */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.RecordIChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/** @typedef {{ paint: xyz.swapee.wc.back.IChangellyExchangeDisplay.paint }} */
xyz.swapee.wc.back.RecordIChangellyExchangeDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.BoundIChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayFields}
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.ChangellyExchangeClasses, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.back.BoundIChangellyExchangeDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIChangellyExchangeDisplay} */
xyz.swapee.wc.back.BoundIChangellyExchangeDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.BoundChangellyExchangeDisplay filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundChangellyExchangeDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundChangellyExchangeDisplay} */
xyz.swapee.wc.back.BoundChangellyExchangeDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplayBack.xml} xyz.swapee.wc.back.IChangellyExchangeDisplay.paint filter:!ControllerPlugin~props 88134650ddf500c77d663147b984da2d */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} [memory]
 * @param {!xyz.swapee.wc.ChangellyExchangeLand} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ChangellyExchangeMemory=, !xyz.swapee.wc.ChangellyExchangeLand=): void} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.IChangellyExchangeDisplay, !xyz.swapee.wc.ChangellyExchangeMemory=, !xyz.swapee.wc.ChangellyExchangeLand=): void} */
xyz.swapee.wc.back.IChangellyExchangeDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint} */
xyz.swapee.wc.back.IChangellyExchangeDisplay.__paint

// nss:xyz.swapee.wc.back.IChangellyExchangeDisplay,$xyz.swapee.wc.back.IChangellyExchangeDisplay,xyz.swapee.wc.back
/* @typal-end */