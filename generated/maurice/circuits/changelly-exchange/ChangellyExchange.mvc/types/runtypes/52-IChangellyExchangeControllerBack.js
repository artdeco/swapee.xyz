/** @const {?} */ $xyz.swapee.wc.back.IChangellyExchangeController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeController.Initialese filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
$xyz.swapee.wc.back.IChangellyExchangeController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.IChangellyExchangeController.Initialese} */
xyz.swapee.wc.back.IChangellyExchangeController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.IChangellyExchangeController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeControllerCaster filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @interface */
$xyz.swapee.wc.back.IChangellyExchangeControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundIChangellyExchangeController} */
$xyz.swapee.wc.back.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.back.BoundChangellyExchangeController} */
$xyz.swapee.wc.back.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeControllerCaster}
 */
xyz.swapee.wc.back.IChangellyExchangeControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.IChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
$xyz.swapee.wc.back.IChangellyExchangeController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.IChangellyExchangeController}
 */
xyz.swapee.wc.back.IChangellyExchangeController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.ChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.back.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IChangellyExchangeController.Initialese>}
 */
$xyz.swapee.wc.back.ChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.ChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.back.IChangellyExchangeController, ...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.ChangellyExchangeController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.AbstractChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.back.ChangellyExchangeController}
 */
$xyz.swapee.wc.back.AbstractChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.back.AbstractChangellyExchangeController)} */
xyz.swapee.wc.back.AbstractChangellyExchangeController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.continues
/**
 * @param {...((!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.ChangellyExchangeController}
 */
xyz.swapee.wc.back.AbstractChangellyExchangeController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.ChangellyExchangeControllerConstructor filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @typedef {function(new: xyz.swapee.wc.back.IChangellyExchangeController, ...!xyz.swapee.wc.back.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.back.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.RecordIChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordIChangellyExchangeController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.BoundIChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
$xyz.swapee.wc.back.BoundIChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundIChangellyExchangeController} */
xyz.swapee.wc.back.BoundIChangellyExchangeController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/52-IChangellyExchangeControllerBack.xml} xyz.swapee.wc.back.BoundChangellyExchangeController filter:!ControllerPlugin~props 94438de719238849c4b9123a2557b1ce */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundChangellyExchangeController} */
xyz.swapee.wc.back.BoundChangellyExchangeController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */