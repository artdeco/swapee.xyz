/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeHtmlComponent
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeController.Initialese}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreen.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchange.Initialese}
 * @extends {com.webcircuits.ILanded.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} */
xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent} */
$xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster.prototype.asIChangellyExchangeHtmlComponent
/** @type {!xyz.swapee.wc.BoundChangellyExchangeHtmlComponent} */
$xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster.prototype.superChangellyExchangeHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.IChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeController}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeScreen}
 * @extends {xyz.swapee.wc.IChangellyExchange}
 * @extends {com.webcircuits.ILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 */
$xyz.swapee.wc.IChangellyExchangeHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.IChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.ChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.ChangellyExchangeHtmlComponent
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeHtmlComponent, ...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese)} */
xyz.swapee.wc.ChangellyExchangeHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.ChangellyExchangeHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
$xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent)} */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeHtmlComponent|typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent)|(!xyz.swapee.wc.back.IChangellyExchangeController|typeof xyz.swapee.wc.back.ChangellyExchangeController)|(!xyz.swapee.wc.back.IChangellyExchangeScreen|typeof xyz.swapee.wc.back.ChangellyExchangeScreen)|(!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.ChangellyExchangeHtmlComponentConstructor filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeHtmlComponent, ...!xyz.swapee.wc.IChangellyExchangeHtmlComponent.Initialese)} */
xyz.swapee.wc.ChangellyExchangeHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeController}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeScreen}
 * @extends {xyz.swapee.wc.BoundIChangellyExchange}
 * @extends {com.webcircuits.BoundILanded<!xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 */
$xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent} */
xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/12-IChangellyExchangeHtmlComponent.xml} xyz.swapee.wc.BoundChangellyExchangeHtmlComponent filter:!ControllerPlugin~props a21cf6e2351993ccc18c6d8b9b9dd1b3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeHtmlComponent} */
xyz.swapee.wc.BoundChangellyExchangeHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */