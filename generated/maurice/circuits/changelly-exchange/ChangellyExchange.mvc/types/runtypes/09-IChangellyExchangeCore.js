/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeCore
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeCore.Model
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeCore
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Initialese filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Initialese} */
xyz.swapee.wc.IChangellyExchangeCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCoreFields filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeCoreFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeCore.Model} */
$xyz.swapee.wc.IChangellyExchangeCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeCoreFields}
 */
xyz.swapee.wc.IChangellyExchangeCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCoreCaster filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeCore} */
$xyz.swapee.wc.IChangellyExchangeCoreCaster.prototype.asIChangellyExchangeCore
/** @type {!xyz.swapee.wc.BoundChangellyExchangeCore} */
$xyz.swapee.wc.IChangellyExchangeCoreCaster.prototype.superChangellyExchangeCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeCoreCaster}
 */
xyz.swapee.wc.IChangellyExchangeCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore}
 */
$xyz.swapee.wc.IChangellyExchangeCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.IChangellyExchangeCore.prototype.resetChangellyExchangeCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeCore}
 */
xyz.swapee.wc.IChangellyExchangeCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.ChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeCore.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.ChangellyExchangeCore
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeCore)} */
xyz.swapee.wc.ChangellyExchangeCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.ChangellyExchangeCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.AbstractChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeCore}
 */
$xyz.swapee.wc.AbstractChangellyExchangeCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeCore)} */
xyz.swapee.wc.AbstractChangellyExchangeCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeOuterCore|typeof xyz.swapee.wc.ChangellyExchangeOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeCore}
 */
xyz.swapee.wc.AbstractChangellyExchangeCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.RecordIChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {{ resetCore: xyz.swapee.wc.IChangellyExchangeCore.resetCore, resetChangellyExchangeCore: xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore }} */
xyz.swapee.wc.RecordIChangellyExchangeCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.BoundIChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCoreCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeOuterCore}
 */
$xyz.swapee.wc.BoundIChangellyExchangeCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeCore} */
xyz.swapee.wc.BoundIChangellyExchangeCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.BoundChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeCore} */
xyz.swapee.wc.BoundChangellyExchangeCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.resetCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeCore): void} */
xyz.swapee.wc.IChangellyExchangeCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeCore.__resetCore} */
xyz.swapee.wc.IChangellyExchangeCore.__resetCore

// nss:xyz.swapee.wc.IChangellyExchangeCore,$xyz.swapee.wc.IChangellyExchangeCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.IChangellyExchangeCore.resetChangellyExchangeCore
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeCore): void} */
xyz.swapee.wc.IChangellyExchangeCore._resetChangellyExchangeCore
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore} */
xyz.swapee.wc.IChangellyExchangeCore.__resetChangellyExchangeCore

// nss:xyz.swapee.wc.IChangellyExchangeCore,$xyz.swapee.wc.IChangellyExchangeCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.loadingGetFixedOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.loadingGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.hasMoreGetFixedOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.hasMoreGetFixedOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.loadGetFixedOfferError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.loadGetFixedOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.loadingGetOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.loadingGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.hasMoreGetOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.hasMoreGetOffer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.loadGetOfferError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.loadGetOfferError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.loadingCreateTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.loadingCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.hasMoreCreateTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.hasMoreCreateTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.loadCreateTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.loadCreateTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.loadingGetTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.loadingGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.hasMoreGetTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.hasMoreGetTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.loadGetTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.loadGetTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.loadingCreateFixedTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.loadingCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.hasMoreCreateFixedTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.hasMoreCreateFixedTransaction

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.loadCreateFixedTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.loadCreateFixedTransactionError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.loadingCheckPayment filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.loadingCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.hasMoreCheckPayment filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.hasMoreCheckPayment

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.loadCheckPaymentError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @typedef {Error} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.loadCheckPaymentError

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer.prototype.loadingGetFixedOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer.prototype.hasMoreGetFixedOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError.prototype.loadGetFixedOfferError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer.prototype.loadingGetOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer.prototype.hasMoreGetOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError.prototype.loadGetOfferError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction.prototype.loadingCreateTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction.prototype.hasMoreCreateTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError.prototype.loadCreateTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction.prototype.loadingGetTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction.prototype.hasMoreGetTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError.prototype.loadGetTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction.prototype.loadingCreateFixedTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction.prototype.hasMoreCreateFixedTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError.prototype.loadCreateFixedTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment.prototype.loadingCheckPayment
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment = function() {}
/** @type {(?boolean)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment.prototype.hasMoreCheckPayment
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError = function() {}
/** @type {(?Error)|undefined} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError.prototype.loadCheckPaymentError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeOuterCore.Model}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError}
 */
$xyz.swapee.wc.IChangellyExchangeCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model} */
xyz.swapee.wc.IChangellyExchangeCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe.prototype.loadingGetFixedOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe.prototype.hasMoreGetFixedOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetFixedOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe.prototype.loadGetFixedOfferError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetFixedOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe.prototype.loadingGetOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe.prototype.hasMoreGetOffer
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetOffer_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe.prototype.loadGetOfferError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetOfferError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe.prototype.loadingCreateTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe.prototype.hasMoreCreateTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe.prototype.loadCreateTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateTransactionError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe.prototype.loadingGetTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingGetTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe.prototype.hasMoreGetTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreGetTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe.prototype.loadGetTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadGetTransactionError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe.prototype.loadingCreateFixedTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCreateFixedTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe.prototype.hasMoreCreateFixedTransaction
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCreateFixedTransaction_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe.prototype.loadCreateFixedTransactionError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCreateFixedTransactionError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe.prototype.loadingCheckPayment
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadingCheckPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe = function() {}
/** @type {?boolean} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe.prototype.hasMoreCheckPayment
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.HasMoreCheckPayment_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/09-IChangellyExchangeCore.xml} xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe filter:!ControllerPlugin~props 29db7cd2c9c075a3ed1a64fa6aa43ad8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe = function() {}
/** @type {?Error} */
$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe.prototype.loadCheckPaymentError
/** @typedef {$xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe} */
xyz.swapee.wc.IChangellyExchangeCore.Model.LoadCheckPaymentError_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeCore.Model
/* @typal-end */