/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeProcessor
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessor.Initialese filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} */
xyz.swapee.wc.IChangellyExchangeProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessorCaster filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeProcessor} */
$xyz.swapee.wc.IChangellyExchangeProcessorCaster.prototype.asIChangellyExchangeProcessor
/** @type {!xyz.swapee.wc.BoundChangellyExchangeProcessor} */
$xyz.swapee.wc.IChangellyExchangeProcessorCaster.prototype.superChangellyExchangeProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeProcessorCaster}
 */
xyz.swapee.wc.IChangellyExchangeProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.IChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessorCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCore}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 */
$xyz.swapee.wc.IChangellyExchangeProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeProcessor}
 */
xyz.swapee.wc.IChangellyExchangeProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.ChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.ChangellyExchangeProcessor
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeProcessor, ...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese)} */
xyz.swapee.wc.ChangellyExchangeProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.ChangellyExchangeProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.AbstractChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeProcessor}
 */
$xyz.swapee.wc.AbstractChangellyExchangeProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeProcessor)} */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeCore|typeof xyz.swapee.wc.ChangellyExchangeCore)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeProcessor}
 */
xyz.swapee.wc.AbstractChangellyExchangeProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.ChangellyExchangeProcessorConstructor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeProcessor, ...!xyz.swapee.wc.IChangellyExchangeProcessor.Initialese)} */
xyz.swapee.wc.ChangellyExchangeProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.RecordIChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.BoundIChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessorCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeCore}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 */
$xyz.swapee.wc.BoundIChangellyExchangeProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeProcessor} */
xyz.swapee.wc.BoundIChangellyExchangeProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/10-IChangellyExchangeProcessor.xml} xyz.swapee.wc.BoundChangellyExchangeProcessor filter:!ControllerPlugin~props 166546d57810ff9707dfd9b988158a4e */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeProcessor} */
xyz.swapee.wc.BoundChangellyExchangeProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */