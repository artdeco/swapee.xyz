/** @const {?} */ $xyz.swapee.wc.front.IChangellyExchangeController
/** @const {?} */ xyz.swapee.wc.front.IChangellyExchangeController
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.Initialese filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @record */
$xyz.swapee.wc.front.IChangellyExchangeController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.IChangellyExchangeController.Initialese} */
xyz.swapee.wc.front.IChangellyExchangeController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.IChangellyExchangeController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeControllerCaster filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @interface */
$xyz.swapee.wc.front.IChangellyExchangeControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundIChangellyExchangeController} */
$xyz.swapee.wc.front.IChangellyExchangeControllerCaster.prototype.asIChangellyExchangeController
/** @type {!xyz.swapee.wc.front.BoundChangellyExchangeController} */
$xyz.swapee.wc.front.IChangellyExchangeControllerCaster.prototype.superChangellyExchangeController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IChangellyExchangeControllerCaster}
 */
xyz.swapee.wc.front.IChangellyExchangeControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerAT}
 */
$xyz.swapee.wc.front.IChangellyExchangeController = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetFixedOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetOffer = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCreateTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadGetTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCreateFixedTransaction = function() {}
/** @return {void} */
$xyz.swapee.wc.front.IChangellyExchangeController.prototype.loadCheckPayment = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.IChangellyExchangeController}
 */
xyz.swapee.wc.front.IChangellyExchangeController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.ChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init
 * @implements {xyz.swapee.wc.front.IChangellyExchangeController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IChangellyExchangeController.Initialese>}
 */
$xyz.swapee.wc.front.ChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.ChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.front.IChangellyExchangeController, ...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.ChangellyExchangeController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.AbstractChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init
 * @extends {xyz.swapee.wc.front.ChangellyExchangeController}
 */
$xyz.swapee.wc.front.AbstractChangellyExchangeController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController
/** @type {function(new: xyz.swapee.wc.front.AbstractChangellyExchangeController)} */
xyz.swapee.wc.front.AbstractChangellyExchangeController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.continues
/**
 * @param {...((!xyz.swapee.wc.front.IChangellyExchangeController|typeof xyz.swapee.wc.front.ChangellyExchangeController)|(!xyz.swapee.wc.front.IChangellyExchangeControllerAT|typeof xyz.swapee.wc.front.ChangellyExchangeControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.ChangellyExchangeController}
 */
xyz.swapee.wc.front.AbstractChangellyExchangeController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.ChangellyExchangeControllerConstructor filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @typedef {function(new: xyz.swapee.wc.front.IChangellyExchangeController, ...!xyz.swapee.wc.front.IChangellyExchangeController.Initialese)} */
xyz.swapee.wc.front.ChangellyExchangeControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.RecordIChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/** @typedef {{ loadGetFixedOffer: xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer, loadGetOffer: xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer, loadCreateTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction, loadGetTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction, loadCreateFixedTransaction: xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction, loadCheckPayment: xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment }} */
xyz.swapee.wc.front.RecordIChangellyExchangeController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.BoundIChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordIChangellyExchangeController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.IChangellyExchangeControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeControllerAT}
 */
$xyz.swapee.wc.front.BoundIChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundIChangellyExchangeController} */
xyz.swapee.wc.front.BoundIChangellyExchangeController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.BoundChangellyExchangeController filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundIChangellyExchangeController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundChangellyExchangeController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundChangellyExchangeController} */
xyz.swapee.wc.front.BoundChangellyExchangeController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetFixedOffer
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetFixedOffer

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetOffer
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetOffer
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetOffer

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCreateTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadGetTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadGetTransaction
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadGetTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCreateFixedTransaction
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCreateFixedTransaction

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/51-IChangellyExchangeControllerFront.xml} xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment filter:!ControllerPlugin~props ede0c008a912f57ad9b2159e4f75b532 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.front.IChangellyExchangeController.loadCheckPayment
/** @typedef {function(this: xyz.swapee.wc.front.IChangellyExchangeController): void} */
xyz.swapee.wc.front.IChangellyExchangeController._loadCheckPayment
/** @typedef {typeof $xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment} */
xyz.swapee.wc.front.IChangellyExchangeController.__loadCheckPayment

// nss:xyz.swapee.wc.front.IChangellyExchangeController,$xyz.swapee.wc.front.IChangellyExchangeController,xyz.swapee.wc.front
/* @typal-end */