/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/100-ChangellyExchangeMemory.xml} xyz.swapee.wc.ChangellyExchangeMemory filter:!ControllerPlugin~props 4c69ed3cac2b99c8bab35288bd6292df */
/** @record */
$xyz.swapee.wc.ChangellyExchangeMemory = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.host
/** @type {?string} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.region
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetFixedOffer
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetFixedOffer
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetFixedOfferError
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetOffer
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetOffer
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetOfferError
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCreateTransaction
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCreateTransaction
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCreateTransactionError
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingGetTransaction
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreGetTransaction
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadGetTransactionError
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCreateFixedTransaction
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCreateFixedTransaction
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCreateFixedTransactionError
/** @type {boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadingCheckPayment
/** @type {?boolean} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.hasMoreCheckPayment
/** @type {?Error} */
$xyz.swapee.wc.ChangellyExchangeMemory.prototype.loadCheckPaymentError
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ChangellyExchangeMemory}
 */
xyz.swapee.wc.ChangellyExchangeMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */