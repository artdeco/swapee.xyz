/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/41-ChangellyExchangeClasses.xml} xyz.swapee.wc.ChangellyExchangeClasses filter:!ControllerPlugin~props b37b4f3129c85e11017205599969c599 */
/** @record */
$xyz.swapee.wc.ChangellyExchangeClasses = __$te_Mixin()
/** @type {string|undefined} */
$xyz.swapee.wc.ChangellyExchangeClasses.prototype._
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ChangellyExchangeClasses}
 */
xyz.swapee.wc.ChangellyExchangeClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */