/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeDisplay
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeDisplay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Initialese filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings>}
 */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese = function() {}
/** @type {HTMLPreElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.Debug
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.ExchangeBroker
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.DealBroker
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.ExchangeIntent
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.TransactionInfo
/** @type {HTMLElement|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese.prototype.RegionSelector
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} */
xyz.swapee.wc.IChangellyExchangeDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplayFields filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeDisplayFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeDisplay.Settings} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.IChangellyExchangeDisplay.Queries} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.queries
/** @type {HTMLPreElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.Debug
/** @type {HTMLElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.ExchangeBroker
/** @type {HTMLElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.DealBroker
/** @type {HTMLElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.ExchangeIntent
/** @type {HTMLElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.TransactionInfo
/** @type {HTMLElement} */
$xyz.swapee.wc.IChangellyExchangeDisplayFields.prototype.RegionSelector
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeDisplayFields}
 */
xyz.swapee.wc.IChangellyExchangeDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplayCaster filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeDisplay} */
$xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeDisplay
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeScreen} */
$xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.asIChangellyExchangeScreen
/** @type {!xyz.swapee.wc.BoundChangellyExchangeDisplay} */
$xyz.swapee.wc.IChangellyExchangeDisplayCaster.prototype.superChangellyExchangeDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeDisplayCaster}
 */
xyz.swapee.wc.IChangellyExchangeDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.ChangellyExchangeMemory, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null>}
 */
$xyz.swapee.wc.IChangellyExchangeDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeDisplay}
 */
xyz.swapee.wc.IChangellyExchangeDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.ChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.ChangellyExchangeDisplay
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeDisplay, ...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese)} */
xyz.swapee.wc.ChangellyExchangeDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.ChangellyExchangeDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.AbstractChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeDisplay}
 */
$xyz.swapee.wc.AbstractChangellyExchangeDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeDisplay)} */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeDisplay|typeof xyz.swapee.wc.ChangellyExchangeDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeDisplay}
 */
xyz.swapee.wc.AbstractChangellyExchangeDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.ChangellyExchangeDisplayConstructor filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeDisplay, ...!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese)} */
xyz.swapee.wc.ChangellyExchangeDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.RecordIChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @typedef {{ paint: xyz.swapee.wc.IChangellyExchangeDisplay.paint }} */
xyz.swapee.wc.RecordIChangellyExchangeDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.BoundIChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.ChangellyExchangeMemory, !HTMLDivElement, !xyz.swapee.wc.IChangellyExchangeDisplay.Settings, xyz.swapee.wc.IChangellyExchangeDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundIChangellyExchangeDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeDisplay} */
xyz.swapee.wc.BoundIChangellyExchangeDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.BoundChangellyExchangeDisplay filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeDisplay} */
xyz.swapee.wc.BoundChangellyExchangeDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.paint filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.ChangellyExchangeMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.IChangellyExchangeDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.ChangellyExchangeMemory, null): void} */
xyz.swapee.wc.IChangellyExchangeDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.IChangellyExchangeDisplay, !xyz.swapee.wc.ChangellyExchangeMemory, null): void} */
xyz.swapee.wc.IChangellyExchangeDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeDisplay.__paint} */
xyz.swapee.wc.IChangellyExchangeDisplay.__paint

// nss:xyz.swapee.wc.IChangellyExchangeDisplay,$xyz.swapee.wc.IChangellyExchangeDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Queries filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.exchangeBrokerSel
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.dealBrokerSel
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.exchangeIntentSel
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.transactionInfoSel
/** @type {string|undefined} */
$xyz.swapee.wc.IChangellyExchangeDisplay.Queries.prototype.regionSelectorSel
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDisplay.Queries} */
xyz.swapee.wc.IChangellyExchangeDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/40-IChangellyExchangeDisplay.xml} xyz.swapee.wc.IChangellyExchangeDisplay.Settings filter:!ControllerPlugin~props cfaa85e192f93e71b522574bcf8ee300 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeDisplay.Queries}
 */
$xyz.swapee.wc.IChangellyExchangeDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeDisplay.Settings} */
xyz.swapee.wc.IChangellyExchangeDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeDisplay
/* @typal-end */