/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeGPU
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeGPU.Initialese} */
xyz.swapee.wc.IChangellyExchangeGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.IChangellyExchangeGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeGPUFields}
 */
xyz.swapee.wc.IChangellyExchangeGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeGPU} */
$xyz.swapee.wc.IChangellyExchangeGPUCaster.prototype.asIChangellyExchangeGPU
/** @type {!xyz.swapee.wc.BoundChangellyExchangeGPU} */
$xyz.swapee.wc.IChangellyExchangeGPUCaster.prototype.superChangellyExchangeGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeGPUCaster}
 */
xyz.swapee.wc.IChangellyExchangeGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.IChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!ChangellyExchangeMemory,.!ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.back.IChangellyExchangeDisplay}
 */
$xyz.swapee.wc.IChangellyExchangeGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeGPU}
 */
xyz.swapee.wc.IChangellyExchangeGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.ChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeGPU.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.ChangellyExchangeGPU
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeGPU, ...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese)} */
xyz.swapee.wc.ChangellyExchangeGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.ChangellyExchangeGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.AbstractChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeGPU}
 */
$xyz.swapee.wc.AbstractChangellyExchangeGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeGPU)} */
xyz.swapee.wc.AbstractChangellyExchangeGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeGPU|typeof xyz.swapee.wc.ChangellyExchangeGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IChangellyExchangeDisplay|typeof xyz.swapee.wc.back.ChangellyExchangeDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeGPU}
 */
xyz.swapee.wc.AbstractChangellyExchangeGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.ChangellyExchangeGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeGPU, ...!xyz.swapee.wc.IChangellyExchangeGPU.Initialese)} */
xyz.swapee.wc.ChangellyExchangeGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.RecordIChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.BoundIChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!ChangellyExchangeMemory,.!ChangellyExchangeLand>}
 * @extends {xyz.swapee.wc.back.BoundIChangellyExchangeDisplay}
 */
$xyz.swapee.wc.BoundIChangellyExchangeGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeGPU} */
xyz.swapee.wc.BoundIChangellyExchangeGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/80-IChangellyExchangeGPU.xml} xyz.swapee.wc.BoundChangellyExchangeGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeGPU} */
xyz.swapee.wc.BoundChangellyExchangeGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */