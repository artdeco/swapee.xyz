/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment
/** @const {?} */ xyz.swapee.wc.IChangellyExchangeRadio
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.Initialese filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeRadio.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.Initialese} */
xyz.swapee.wc.IChangellyExchangeRadio.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadioCaster filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeRadioCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeRadio} */
$xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.asIChangellyExchangeRadio
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeComputer} */
$xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.asIChangellyExchangeComputer
/** @type {!xyz.swapee.wc.BoundChangellyExchangeRadio} */
$xyz.swapee.wc.IChangellyExchangeRadioCaster.prototype.superChangellyExchangeRadio
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeRadioCaster}
 */
xyz.swapee.wc.IChangellyExchangeRadioCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeRadioCaster}
 */
$xyz.swapee.wc.IChangellyExchangeRadio = function() {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.prototype.adaptLoadCheckPayment = function(form, changes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeRadio}
 */
xyz.swapee.wc.IChangellyExchangeRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.ChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeRadio}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeRadio.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.ChangellyExchangeRadio
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeRadio)} */
xyz.swapee.wc.ChangellyExchangeRadio.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.ChangellyExchangeRadio.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.AbstractChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeRadio}
 */
$xyz.swapee.wc.AbstractChangellyExchangeRadio = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeRadio)} */
xyz.swapee.wc.AbstractChangellyExchangeRadio.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.__extend
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.continues
/**
 * @param {...(!xyz.swapee.wc.IChangellyExchangeRadio|typeof xyz.swapee.wc.ChangellyExchangeRadio)} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeRadio}
 */
xyz.swapee.wc.AbstractChangellyExchangeRadio.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.RecordIChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/** @typedef {{ adaptLoadGetFixedOffer: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer, adaptLoadGetOffer: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer, adaptLoadCreateTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction, adaptLoadGetTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction, adaptLoadCreateFixedTransaction: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction, adaptLoadCheckPayment: xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment }} */
xyz.swapee.wc.RecordIChangellyExchangeRadio

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.BoundIChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeRadio}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeRadioCaster}
 */
$xyz.swapee.wc.BoundIChangellyExchangeRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeRadio} */
xyz.swapee.wc.BoundIChangellyExchangeRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.BoundChangellyExchangeRadio filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeRadio}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeRadio = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeRadio} */
xyz.swapee.wc.BoundChangellyExchangeRadio

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetFixedOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetFixedOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetFixedOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetFixedOffer

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetOffer = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetOffer.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetOffer} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetOffer
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetOffer

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadGetTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadGetTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadGetTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateFixedTransaction = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCreateFixedTransaction.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCreateFixedTransaction
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCreateFixedTransaction

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 * @this {xyz.swapee.wc.IChangellyExchangeRadio}
 */
$xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCheckPayment = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} form
 * @param {IChangellyExchangeComputer.adaptLoadCheckPayment.Form} changes
 * @return {(undefined|!Promise<undefined>|!Promise<(xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return|void)>)}
 * @this {THIS}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeRadio._adaptLoadCheckPayment
/** @typedef {typeof $xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment} */
xyz.swapee.wc.IChangellyExchangeRadio.__adaptLoadCheckPayment

// nss:xyz.swapee.wc.IChangellyExchangeRadio,$xyz.swapee.wc.IChangellyExchangeRadio,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.FixedId}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFixedAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetFixedOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Ready_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.GetOffer_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.EstimatedFloatAmountTo}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MinAmount}
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.MaxAmount}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetOffer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoCore.Model.Tid_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Fixed}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.CurrencyTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountFrom}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.AmountTo}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NetworkFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.PartnerFee}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.VisibleAmount}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.NotFound}
 * @extends {xyz.swapee.wc.ITransactionInfoPort.Inputs.Rate}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadGetTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.AmountFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyFrom_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.CurrencyTo_Safe}
 * @extends {xyz.swapee.wc.IExchangeIntentCore.Model.Fixed_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Address_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.RefundAddress_Safe}
 * @extends {xyz.swapee.wc.IDealBrokerCore.Model.FixedId_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CreateTransaction_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IDealBrokerPort.Inputs.GetOffer}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Id}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreateTransactionError}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CreatedAt}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinAddress}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.PayinExtraId}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.KycRequired}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.ConfirmedAmountFrom}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCreateFixedTransaction
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.Id_Safe}
 * @extends {xyz.swapee.wc.IExchangeBrokerCore.Model.CheckPayment_Safe}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/160-IChangellyExchangeRadio.xml} xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return filter:!ControllerPlugin~props 06b4bc89ef37e3515dcc87f72abe8ec8 */
/**
 * @record
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.Status}
 * @extends {xyz.swapee.wc.IExchangeBrokerPort.Inputs.CheckPaymentError}
 */
$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return} */
xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeRadio.adaptLoadCheckPayment
/* @typal-end */