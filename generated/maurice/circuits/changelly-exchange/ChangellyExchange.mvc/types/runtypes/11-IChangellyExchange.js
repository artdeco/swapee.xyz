/** @const {?} */ $xyz.swapee.wc.IChangellyExchange
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeEnv filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @record */
$xyz.swapee.wc.ChangellyExchangeEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.IChangellyExchange} */
$xyz.swapee.wc.ChangellyExchangeEnv.prototype.changellyExchange
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ChangellyExchangeEnv}
 */
xyz.swapee.wc.ChangellyExchangeEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.Initialese filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer.Initialese}
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Initialese}
 */
$xyz.swapee.wc.IChangellyExchange.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchange.Initialese} */
xyz.swapee.wc.IChangellyExchange.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchange
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeFields filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchange.Pinout} */
$xyz.swapee.wc.IChangellyExchangeFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeFields}
 */
xyz.swapee.wc.IChangellyExchangeFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeCaster filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchange} */
$xyz.swapee.wc.IChangellyExchangeCaster.prototype.asIChangellyExchange
/** @type {!xyz.swapee.wc.BoundChangellyExchange} */
$xyz.swapee.wc.IChangellyExchangeCaster.prototype.superChangellyExchange
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeCaster}
 */
xyz.swapee.wc.IChangellyExchangeCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCaster}
 * @extends {xyz.swapee.wc.IChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.IChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.IChangellyExchangeController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.IChangellyExchange = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchange}
 */
xyz.swapee.wc.IChangellyExchange

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchange}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchange.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchange = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.ChangellyExchange
/** @type {function(new: xyz.swapee.wc.IChangellyExchange, ...!xyz.swapee.wc.IChangellyExchange.Initialese)} */
xyz.swapee.wc.ChangellyExchange.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.ChangellyExchange.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.AbstractChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchange}
 */
$xyz.swapee.wc.AbstractChangellyExchange = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchange.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchange)} */
xyz.swapee.wc.AbstractChangellyExchange.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchange.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchange|typeof xyz.swapee.wc.ChangellyExchange)|(!xyz.swapee.wc.IChangellyExchangeProcessor|typeof xyz.swapee.wc.ChangellyExchangeProcessor)|(!xyz.swapee.wc.IChangellyExchangeComputer|typeof xyz.swapee.wc.ChangellyExchangeComputer)|(!xyz.swapee.wc.IChangellyExchangeController|typeof xyz.swapee.wc.ChangellyExchangeController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchange}
 */
xyz.swapee.wc.AbstractChangellyExchange.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeConstructor filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchange, ...!xyz.swapee.wc.IChangellyExchange.Initialese)} */
xyz.swapee.wc.ChangellyExchangeConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.MVCOptions filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @record */
$xyz.swapee.wc.IChangellyExchange.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.IChangellyExchange.Pinout)|undefined} */
$xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.IChangellyExchange.Pinout)|undefined} */
$xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.IChangellyExchange.Pinout} */
$xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.ChangellyExchangeMemory)|undefined} */
$xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.ChangellyExchangeClasses)|undefined} */
$xyz.swapee.wc.IChangellyExchange.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.IChangellyExchange.MVCOptions} */
xyz.swapee.wc.IChangellyExchange.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchange
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.RecordIChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchange

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.BoundIChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchange}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeCaster}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeProcessor}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeComputer}
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.ChangellyExchangeMemory, !xyz.swapee.wc.IChangellyExchangeController.Inputs, !xyz.swapee.wc.ChangellyExchangeLand>}
 */
$xyz.swapee.wc.BoundIChangellyExchange = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchange} */
xyz.swapee.wc.BoundIChangellyExchange

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.BoundChangellyExchange filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchange}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchange = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchange} */
xyz.swapee.wc.BoundChangellyExchange

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchange.Pinout filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeController.Inputs}
 */
$xyz.swapee.wc.IChangellyExchange.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchange.Pinout} */
xyz.swapee.wc.IChangellyExchange.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchange
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.IChangellyExchangeBuffer filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.IChangellyExchangeController.Inputs>}
 */
$xyz.swapee.wc.IChangellyExchangeBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeBuffer}
 */
xyz.swapee.wc.IChangellyExchangeBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/11-IChangellyExchange.xml} xyz.swapee.wc.ChangellyExchangeBuffer filter:!ControllerPlugin~props 8a1ea67b8b358ce34d26d097ff2491d4 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.IChangellyExchangeBuffer}
 */
$xyz.swapee.wc.ChangellyExchangeBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeBuffer}
 */
xyz.swapee.wc.ChangellyExchangeBuffer
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeBuffer)} */
xyz.swapee.wc.ChangellyExchangeBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */