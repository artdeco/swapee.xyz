/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElementPort
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/** @const {?} */ $xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Initialese filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.IChangellyExchangeElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Initialese} */
xyz.swapee.wc.IChangellyExchangeElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPortFields filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeElementPortFields = function() {}
/** @type {!xyz.swapee.wc.IChangellyExchangeElementPort.Inputs} */
$xyz.swapee.wc.IChangellyExchangeElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.IChangellyExchangeElementPort.Inputs} */
$xyz.swapee.wc.IChangellyExchangeElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementPortFields}
 */
xyz.swapee.wc.IChangellyExchangeElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPortCaster filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @interface */
$xyz.swapee.wc.IChangellyExchangeElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundIChangellyExchangeElementPort} */
$xyz.swapee.wc.IChangellyExchangeElementPortCaster.prototype.asIChangellyExchangeElementPort
/** @type {!xyz.swapee.wc.BoundChangellyExchangeElementPort} */
$xyz.swapee.wc.IChangellyExchangeElementPortCaster.prototype.superChangellyExchangeElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementPortCaster}
 */
xyz.swapee.wc.IChangellyExchangeElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @interface
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.IChangellyExchangeElementPort.Inputs>}
 */
$xyz.swapee.wc.IChangellyExchangeElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementPort}
 */
xyz.swapee.wc.IChangellyExchangeElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.ChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese} init
 * @implements {xyz.swapee.wc.IChangellyExchangeElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese>}
 */
$xyz.swapee.wc.ChangellyExchangeElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.ChangellyExchangeElementPort}
 */
xyz.swapee.wc.ChangellyExchangeElementPort
/** @type {function(new: xyz.swapee.wc.IChangellyExchangeElementPort, ...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese)} */
xyz.swapee.wc.ChangellyExchangeElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElementPort}
 */
xyz.swapee.wc.ChangellyExchangeElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.AbstractChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese} init
 * @extends {xyz.swapee.wc.ChangellyExchangeElementPort}
 */
$xyz.swapee.wc.AbstractChangellyExchangeElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractChangellyExchangeElementPort}
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort
/** @type {function(new: xyz.swapee.wc.AbstractChangellyExchangeElementPort)} */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElementPort|typeof xyz.swapee.wc.ChangellyExchangeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractChangellyExchangeElementPort}
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElementPort}
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElementPort|typeof xyz.swapee.wc.ChangellyExchangeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElementPort}
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.IChangellyExchangeElementPort|typeof xyz.swapee.wc.ChangellyExchangeElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.ChangellyExchangeElementPort}
 */
xyz.swapee.wc.AbstractChangellyExchangeElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.ChangellyExchangeElementPortConstructor filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {function(new: xyz.swapee.wc.IChangellyExchangeElementPort, ...!xyz.swapee.wc.IChangellyExchangeElementPort.Initialese)} */
xyz.swapee.wc.ChangellyExchangeElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.RecordIChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordIChangellyExchangeElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.BoundIChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPortFields}
 * @extends {xyz.swapee.wc.RecordIChangellyExchangeElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.IChangellyExchangeElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundIChangellyExchangeElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundIChangellyExchangeElementPort} */
xyz.swapee.wc.BoundIChangellyExchangeElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.BoundChangellyExchangeElementPort filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundIChangellyExchangeElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundChangellyExchangeElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundChangellyExchangeElementPort} */
xyz.swapee.wc.BoundChangellyExchangeElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {boolean} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts.debugOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts.debugOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts.exchangeBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts.exchangeBrokerOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts.dealBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts.dealBrokerOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts.transactionInfoOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts.transactionInfoOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts.regionSelectorOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @typedef {!Object} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts.regionSelectorOpts

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts.prototype.debugOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts.prototype.exchangeBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts.prototype.dealBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts.prototype.transactionInfoOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts = function() {}
/** @type {(!Object)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts.prototype.regionSelectorOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts}
 */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs}
 */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts.prototype.debugOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts.prototype.exchangeBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts.prototype.dealBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts.prototype.transactionInfoOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts.prototype.regionSelectorOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/**
 * @record
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts}
 * @extends {xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts}
 */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs}
 */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts_Safe.prototype.debugOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DebugOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts_Safe.prototype.exchangeBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeBrokerOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts_Safe.prototype.dealBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.DealBrokerOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts_Safe.prototype.transactionInfoOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.TransactionInfoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts_Safe = function() {}
/** @type {!Object} */
$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts_Safe.prototype.regionSelectorOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.Inputs.RegionSelectorOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts_Safe.prototype.debugOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DebugOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts_Safe.prototype.exchangeBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeBrokerOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts_Safe.prototype.dealBrokerOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.DealBrokerOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts_Safe.prototype.exchangeIntentOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.ExchangeIntentOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts_Safe.prototype.transactionInfoOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.TransactionInfoOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/140-IChangellyExchangeElementPort.xml} xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts_Safe filter:!ControllerPlugin~props 06490ce2ad28151f454673e9dc8f6731 */
/** @record */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts_Safe.prototype.regionSelectorOpts
/** @typedef {$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts_Safe} */
xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs.RegionSelectorOpts_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.IChangellyExchangeElementPort.WeakInputs
/* @typal-end */