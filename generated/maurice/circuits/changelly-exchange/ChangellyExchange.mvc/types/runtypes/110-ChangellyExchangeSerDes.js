/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ChangellyExchangeMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeMemoryPQs.prototype.host
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeMemoryPQs}
 */
xyz.swapee.wc.ChangellyExchangeMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeMemoryQPs.prototype.g7b3d
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeMemoryQPs}
 */
xyz.swapee.wc.ChangellyExchangeMemoryQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeMemoryQPs)} */
xyz.swapee.wc.ChangellyExchangeMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ChangellyExchangeMemoryPQs}
 */
$xyz.swapee.wc.ChangellyExchangeInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeInputsPQs}
 */
xyz.swapee.wc.ChangellyExchangeInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.ChangellyExchangeMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeInputsQPs}
 */
xyz.swapee.wc.ChangellyExchangeInputsQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeInputsQPs)} */
xyz.swapee.wc.ChangellyExchangeInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeCachePQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ChangellyExchangeCachePQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.loadingCreateTransaction
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.hasMoreCreateTransaction
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCachePQs.prototype.loadCreateTransactionError
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeCachePQs}
 */
xyz.swapee.wc.ChangellyExchangeCachePQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeCacheQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeCacheQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.cee07
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.db41a
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.h6bbc
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeCacheQPs}
 */
xyz.swapee.wc.ChangellyExchangeCacheQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeCacheQPs)} */
xyz.swapee.wc.ChangellyExchangeCacheQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeQueriesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ChangellyExchangeQueriesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeQueriesPQs.prototype.exchangeBrokerSel
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeQueriesPQs}
 */
xyz.swapee.wc.ChangellyExchangeQueriesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeQueriesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeQueriesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeQueriesQPs.prototype.ecfe5
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeQueriesQPs}
 */
xyz.swapee.wc.ChangellyExchangeQueriesQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeQueriesQPs)} */
xyz.swapee.wc.ChangellyExchangeQueriesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ChangellyExchangeVdusPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeVdusPQs.prototype.ExchangeBroker
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeVdusPQs}
 */
xyz.swapee.wc.ChangellyExchangeVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeVdusQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeVdusQPs.prototype.j4a11
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeVdusQPs}
 */
xyz.swapee.wc.ChangellyExchangeVdusQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeVdusQPs)} */
xyz.swapee.wc.ChangellyExchangeVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeClassesPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.ChangellyExchangeClassesPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeClassesPQs.prototype._
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ChangellyExchangeClassesPQs}
 */
xyz.swapee.wc.ChangellyExchangeClassesPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/changelly-exchange/ChangellyExchange.mvc/design/110-ChangellyExchangeSerDes.xml} xyz.swapee.wc.ChangellyExchangeClassesQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.ChangellyExchangeClassesQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.ChangellyExchangeClassesQPs.prototype.b14a7
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.ChangellyExchangeClassesQPs}
 */
xyz.swapee.wc.ChangellyExchangeClassesQPs
/** @type {function(new: xyz.swapee.wc.ChangellyExchangeClassesQPs)} */
xyz.swapee.wc.ChangellyExchangeClassesQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */