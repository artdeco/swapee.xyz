import { ChangellyExchangeDisplay, ChangellyExchangeScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.ChangellyExchangeDisplay} */
export { ChangellyExchangeDisplay }
/** @lazy @api {xyz.swapee.wc.ChangellyExchangeScreen} */
export { ChangellyExchangeScreen }