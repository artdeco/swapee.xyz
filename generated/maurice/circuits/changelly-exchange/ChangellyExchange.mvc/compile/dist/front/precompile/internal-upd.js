import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {2013392380} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const m=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const n=m["37270038985"],p=m["372700389810"],q=m["372700389811"];function r(b,c,f,g){return m["372700389812"](b,c,f,g,!1,void 0)};
const t=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const u=t["61893096584"],v=t["61893096586"],B=t["618930965811"],C=t["618930965812"],D=t["618930965815"],E=t["618930965819"];function F(){}F.prototype={};function G(){this.l=this.m=this.j=this.h=this.i=this.g=null}class H{}class I extends r(H,201339238020,G,{C:1,ha:2}){}
I[q]=[F,u,{constructor(){n(this,()=>{const {queries:{I:b,H:c,K:f,ka:g,fa:k}}=this;this.scan({s:b,o:c,u:f,A:g,v:k})})},scan:function({s:b,o:c,u:f,A:g,v:k}){const {element:e,D:{vdusPQs:{g:T}},queries:{s:w,o:x,u:y,A:z,v:A}}=this,U=D(e);Object.assign(this,{g:U[T],i:(d=>{let a;d?a=e.closest(d):a=document;return w?a.querySelector(w):void 0})(b),h:(d=>{let a;d?a=e.closest(d):a=document;return x?a.querySelector(x):void 0})(c),j:(d=>{let a;d?a=e.closest(d):a=document;return y?a.querySelector(y):
void 0})(f),m:(d=>{let a;d?a=e.closest(d):a=document;return z?a.querySelector(z):void 0})(g),l:(d=>{let a;d?a=e.closest(d):a=document;return A?a.querySelector(A):void 0})(k)})}},{[p]:{g:1,i:1,h:1,j:1,m:1,l:1},initializer({g:b,i:c,h:f,j:g,m:k,l:e}){void 0!==b&&(this.g=b);void 0!==c&&(this.i=c);void 0!==f&&(this.h=f);void 0!==g&&(this.j=g);void 0!==k&&(this.m=k);void 0!==e&&(this.l=e)}}];var J=class extends I.implements(){};function K(){}K.prototype={};class L{}class M extends r(L,201339238029,null,{F:1,ga:2}){}M[q]=[K,B,{}];function N(){}N.prototype={};class O{}class P extends r(O,201339238032,null,{G:1,ja:2}){}P[q]=[N,C,{allocator(){this.methods={}}}];const Q={host:"67b3d",region:"960db"};const R={...Q};const S=Object.keys(Q).reduce((b,c)=>{b[Q[c]]=c;return b},{});const V={ba:"2ee07",O:"3b41a",W:"76bbc",$:"50a5a",L:"782f1",U:"fa964",da:"92f5e",R:"849f7",Y:"b5539",aa:"ede56",M:"c396c",V:"70ece",ca:"d97fa",P:"b420b",X:"dfce5",ea:"c1aba",T:"2099f",Z:"3e13c"};const W=Object.keys(V).reduce((b,c)=>{b[V[c]]=c;return b},{});function X(){}X.prototype={};class aa{}class Y extends r(aa,201339238030,null,{D:1,ia:2}){}function Z(){}Y[q]=[X,Z.prototype={deduceInputs(){const {C:{g:b}}=this;return{region:null==b?void 0:b.innerText}}},Z.prototype={inputsPQs:R,queriesPQs:{s:"ecfe5",v:"9592e",o:"f9c2e",u:"13da4",J:"02ed0",A:"63ad3"},memoryQPs:S,cacheQPs:W},v,E,P,Z.prototype={vdusPQs:{i:"j4a11",l:"j4a12",g:"j4a13",h:"j4a14",j:"j4a15",m:"j4a17"}}];var ba=class extends Y.implements(M,J,{get queries(){return this.settings}},{__$id:2013392380}){};module.exports["201339238041"]=J;module.exports["201339238071"]=ba;
/*! @embed-object-end {2013392380} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule