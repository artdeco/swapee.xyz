/**
 * Display for presenting information from the _IChangellyExchange_.
 * @extends {xyz.swapee.wc.ChangellyExchangeDisplay}
 */
class ChangellyExchangeDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ChangellyExchangeScreen}
 */
class ChangellyExchangeScreen extends (class {/* lazy-loaded */}) {}

module.exports.ChangellyExchangeDisplay = ChangellyExchangeDisplay
module.exports.ChangellyExchangeScreen = ChangellyExchangeScreen