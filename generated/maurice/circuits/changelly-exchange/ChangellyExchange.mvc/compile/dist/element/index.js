/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchange` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchange}
 */
class AbstractChangellyExchange extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IChangellyExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ChangellyExchangePort}
 */
class ChangellyExchangePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeController` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeController}
 */
class AbstractChangellyExchangeController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IChangellyExchange_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ChangellyExchangeElement}
 */
class ChangellyExchangeElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ChangellyExchangeBuffer}
 */
class ChangellyExchangeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
class AbstractChangellyExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ChangellyExchangeComputer}
 */
class ChangellyExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ChangellyExchangeController}
 */
class ChangellyExchangeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractChangellyExchange = AbstractChangellyExchange
module.exports.ChangellyExchangePort = ChangellyExchangePort
module.exports.AbstractChangellyExchangeController = AbstractChangellyExchangeController
module.exports.ChangellyExchangeElement = ChangellyExchangeElement
module.exports.ChangellyExchangeBuffer = ChangellyExchangeBuffer
module.exports.AbstractChangellyExchangeComputer = AbstractChangellyExchangeComputer
module.exports.ChangellyExchangeComputer = ChangellyExchangeComputer
module.exports.ChangellyExchangeController = ChangellyExchangeController

Object.defineProperties(module.exports, {
 'AbstractChangellyExchange': {get: () => require('./precompile/internal')[20133923801]},
 [20133923801]: {get: () => module.exports['AbstractChangellyExchange']},
 'ChangellyExchangePort': {get: () => require('./precompile/internal')[20133923803]},
 [20133923803]: {get: () => module.exports['ChangellyExchangePort']},
 'AbstractChangellyExchangeController': {get: () => require('./precompile/internal')[20133923804]},
 [20133923804]: {get: () => module.exports['AbstractChangellyExchangeController']},
 'ChangellyExchangeElement': {get: () => require('./precompile/internal')[20133923808]},
 [20133923808]: {get: () => module.exports['ChangellyExchangeElement']},
 'ChangellyExchangeBuffer': {get: () => require('./precompile/internal')[201339238011]},
 [201339238011]: {get: () => module.exports['ChangellyExchangeBuffer']},
 'AbstractChangellyExchangeComputer': {get: () => require('./precompile/internal')[201339238030]},
 [201339238030]: {get: () => module.exports['AbstractChangellyExchangeComputer']},
 'ChangellyExchangeComputer': {get: () => require('./precompile/internal')[201339238031]},
 [201339238031]: {get: () => module.exports['ChangellyExchangeComputer']},
 'ChangellyExchangeController': {get: () => require('./precompile/internal')[201339238061]},
 [201339238061]: {get: () => module.exports['ChangellyExchangeController']},
})