import Module from './browser'

/**@extends {xyz.swapee.wc.AbstractChangellyExchange}*/
export class AbstractChangellyExchange extends Module['20133923801'] {}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchange} */
AbstractChangellyExchange.class=function(){}
/** @type {typeof xyz.swapee.wc.ChangellyExchangePort} */
export const ChangellyExchangePort=Module['20133923803']
/**@extends {xyz.swapee.wc.AbstractChangellyExchangeController}*/
export class AbstractChangellyExchangeController extends Module['20133923804'] {}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeController} */
AbstractChangellyExchangeController.class=function(){}
/** @type {typeof xyz.swapee.wc.ChangellyExchangeHtmlComponent} */
export const ChangellyExchangeHtmlComponent=Module['201339238010']
/** @type {typeof xyz.swapee.wc.ChangellyExchangeBuffer} */
export const ChangellyExchangeBuffer=Module['201339238011']
/**@extends {xyz.swapee.wc.AbstractChangellyExchangeComputer}*/
export class AbstractChangellyExchangeComputer extends Module['201339238030'] {}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeComputer} */
AbstractChangellyExchangeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ChangellyExchangeComputer} */
export const ChangellyExchangeComputer=Module['201339238031']
/** @type {typeof xyz.swapee.wc.back.ChangellyExchangeController} */
export const ChangellyExchangeController=Module['201339238061']