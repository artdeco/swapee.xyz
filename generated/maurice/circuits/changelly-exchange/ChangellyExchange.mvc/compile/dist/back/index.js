/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchange` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchange}
 */
class AbstractChangellyExchange extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IChangellyExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ChangellyExchangePort}
 */
class ChangellyExchangePort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeController` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeController}
 */
class AbstractChangellyExchangeController extends (class {/* lazy-loaded */}) {}
/**
 * The _IChangellyExchange_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ChangellyExchangeHtmlComponent}
 */
class ChangellyExchangeHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ChangellyExchangeBuffer}
 */
class ChangellyExchangeBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IChangellyExchangeComputer` interface.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
class AbstractChangellyExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.ChangellyExchangeComputer}
 */
class ChangellyExchangeComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ChangellyExchangeController}
 */
class ChangellyExchangeController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractChangellyExchange = AbstractChangellyExchange
module.exports.ChangellyExchangePort = ChangellyExchangePort
module.exports.AbstractChangellyExchangeController = AbstractChangellyExchangeController
module.exports.ChangellyExchangeHtmlComponent = ChangellyExchangeHtmlComponent
module.exports.ChangellyExchangeBuffer = ChangellyExchangeBuffer
module.exports.AbstractChangellyExchangeComputer = AbstractChangellyExchangeComputer
module.exports.ChangellyExchangeComputer = ChangellyExchangeComputer
module.exports.ChangellyExchangeController = ChangellyExchangeController