import ChangellyExchangeSharedComputer from '../ChangellyExchangeSharedComputer'
import AbstractChangellyExchangeComputer from '../../gen/AbstractChangellyExchangeComputer'

/** @extends {xyz.swapee.wc.ChangellyExchangeComputer} */
export default class ChangellyExchangeHtmlComputer extends AbstractChangellyExchangeComputer.implements(
 ChangellyExchangeSharedComputer,
){}