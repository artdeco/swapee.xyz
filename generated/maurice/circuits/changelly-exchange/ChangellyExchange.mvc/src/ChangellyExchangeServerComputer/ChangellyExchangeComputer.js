import ChangellyExchangeSharedComputer from '../ChangellyExchangeSharedComputer'
import AbstractChangellyExchangeComputer from '../../gen/AbstractChangellyExchangeComputer'

/** @extends {xyz.swapee.wc.ChangellyExchangeComputer} */
export default class ChangellyExchangeServerComputer extends AbstractChangellyExchangeComputer.implements(
 ChangellyExchangeSharedComputer,
){}