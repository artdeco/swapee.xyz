import AbstractChangellyExchangeControllerAT from '../../gen/AbstractChangellyExchangeControllerAT'
import ChangellyExchangeDisplay from '../ChangellyExchangeDisplay'
import AbstractChangellyExchangeScreen from '../../gen/AbstractChangellyExchangeScreen'

/** @extends {xyz.swapee.wc.ChangellyExchangeScreen} */
export default class extends AbstractChangellyExchangeScreen.implements(
 AbstractChangellyExchangeControllerAT,
 ChangellyExchangeDisplay,
 /**@type {!xyz.swapee.wc.IChangellyExchangeScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IChangellyExchangeScreen} */ ({
  __$id:2013392380,
 }),
){}