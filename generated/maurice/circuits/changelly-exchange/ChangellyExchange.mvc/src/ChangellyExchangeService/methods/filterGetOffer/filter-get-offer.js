import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterGetOffer} */
export default async function filterGetOffer({
 amountFrom:amountFrom,currencyFrom:currencyFrom,currencyTo:currencyTo,
}){
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{
  GetExtendedExchangeAmount:GetExtendedExchangeAmount,
  GetExchangeAmount:GetExchangeAmount,
 }}=changelly

 let R
 try {
  R=await GetExtendedExchangeAmount({
   amount:amountFrom,
   from:currencyFrom,
   to:currencyTo,
  })
 }catch(err) {
  // Error: Not enough liquidity in pair btc->matic. Max amount is 3.2664419999999996 btc.// -32602
  // if(err['co de']==-32600) {
  // Invalid amount: minimal amount for matic->bat is 33.813
  // Invalid amount: maximal amount for eth->bat is 3.34930326688981605214
  if(/max.*? amount/i.test(err.message)) {
   const[,max]=/(\d+(\.\d+)?)/.exec(err.message)||[]
   return{
    maxAmount:max,
   }
  }
  // const[min]=/[\d.]+/.exec(err.message)||[]
  // return {
  //  minAmount:min,
  // }
  // }else
  if(err['code']) {
   throw new Error(`!${err.message}`)
  }
  throw err
 }
 // debugger
 if(!R) {
  try {
   const rr=await GetExchangeAmount({
    amount:amountFrom,
    from:currencyFrom,
    to:currencyTo,
   })
   return{
    estimatedFloatAmountTo:rr,
   }
   // debugger
  }catch(err) {
   if(err['code']==-32600) {
    // Invalid amount: minimal amount for matic->bat is 33.813
    if(/maximal/.test(err.message)) {
     const[max]=/[\d.]+$/.exec(err.message)||[]
     return {
      maxAmount:max,
     }
    }
    const[min]=/[\d.]+$/.exec(err.message)||[]
    return {
     minAmount:min,
    }
   }else if(err['code']) {
    throw new Error(`!${err.message}`)
   }
   throw err
  }
 }
 const precisionMap=new Map([
  ['eth',8],
 ])
 const precision=precisionMap.get(currencyTo.toLowerCase())
 let{
  result:result,fee:fee,networkFee:networkFee,visibleAmount:visibleAmount,
  rate:rate,
 }=R
 networkFee=Number(networkFee)
 const r=Number(result)-networkFee
 if(precision) {
  result=r.toFixed(precision).replace(/(\.\d*?)0+$/,'$1')
  rate=Number(rate).toFixed(precision).replace(/(\.\d*?)0+$/,'$1')
 }
 else result=r
 networkFee=networkFee+''

 const partnerFee=(Number(fee)/(Number(rate)*Number(amountFrom))*100).toFixed(2).replace(/(\.\d*?)0+$/,'$1')
 return{
  estimatedFloatAmountTo:result,partnerFee:partnerFee,networkFee:networkFee,
  visibleAmount:visibleAmount,rate,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBeVVHLE1BQU0sU0FBUyxjQUFjO0NBQzVCLHFCQUFxQixDQUFDLHlCQUF5QixDQUFDLHFCQUFxQjtBQUN0RSxDQUFDO0NBQ0EsTUFBTSxjQUFjLFNBQVMsR0FBRztDQUNoQyxNQUFNO0VBQ0wsbURBQW1EO0VBQ25ELG1DQUFtQztBQUNyQyxJQUFJOztDQUVILElBQUk7Q0FDSjtFQUNDLENBQUMsQ0FBQyxNQUFNLHlCQUF5QjtHQUNoQyxpQkFBaUI7R0FDakIsaUJBQWlCO0dBQ2pCLGFBQWE7QUFDaEIsR0FBRztBQUNILEVBQUUsS0FBSyxDQUFDO0VBQ04sR0FBRyxPQUFPLElBQUksT0FBTyxVQUFVLEdBQUcsS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLEdBQUc7RUFDM0YsR0FBRyxFQUFFLENBQUMsUUFBUSxJQUFJLEVBQUU7RUFDcEIsR0FBRyxRQUFRLFFBQVEsUUFBUSxPQUFPLElBQUksTUFBTSxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7RUFDdkQsR0FBRyxRQUFRLFFBQVEsUUFBUSxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7RUFDcEQsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0dBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7R0FDckM7SUFDQyxhQUFhO0FBQ2pCO0FBQ0E7RUFDRSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7RUFDaEMsR0FBRztFQUNILElBQUksYUFBYTtFQUNqQixFQUFFO0VBQ0YsRUFBRSxFQUFFO0VBQ0osRUFBRSxDQUFDO0dBQ0YsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUM7QUFDbkM7RUFDRSxNQUFNO0FBQ1I7Q0FDQyxHQUFHO0NBQ0gsRUFBRSxDQUFDO0VBQ0Y7R0FDQyxNQUFNLEVBQUUsQ0FBQyxNQUFNLGlCQUFpQjtJQUMvQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLGFBQWE7QUFDakIsSUFBSTtHQUNEO0lBQ0MseUJBQXlCO0FBQzdCO0dBQ0csR0FBRztBQUNOLEdBQUcsS0FBSyxDQUFDO0dBQ04sRUFBRSxDQUFDLFdBQVcsRUFBRTtJQUNmLEdBQUcsUUFBUSxRQUFRLFFBQVEsT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO0lBQ3ZELEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztLQUNyQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO0tBQzlCO01BQ0MsYUFBYTtBQUNuQjtBQUNBO0lBQ0ksVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUM5QjtLQUNDLGFBQWE7QUFDbEI7QUFDQSxJQUFJLEtBQUssRUFBRSxDQUFDO0lBQ1IsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUM7QUFDcEM7R0FDRyxNQUFNO0FBQ1Q7QUFDQTtDQUNDLE1BQU0sWUFBWSxDQUFDLElBQUksR0FBRyxDQUFDO0VBQzFCLE1BQU0sQ0FBQyxFQUFFO0NBQ1Y7Q0FDQSxNQUFNLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7Q0FDeEQ7RUFDQyxhQUFhLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLDJCQUEyQjtFQUN2RSxTQUFTO0dBQ1I7Q0FDRixVQUFVLENBQUMsTUFBTSxDQUFDO0NBQ2xCLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQztDQUNmLEVBQUUsQ0FBQztFQUNGLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7RUFDbEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7QUFDN0Q7Q0FDQyxLQUFLLE1BQU0sQ0FBQztDQUNaLFVBQVUsQ0FBQzs7Q0FFWCxNQUFNLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztDQUN0RztFQUNDLDZCQUE2QixDQUFDLHFCQUFxQixDQUFDLHFCQUFxQjtFQUN6RSwyQkFBMkIsQ0FBQyxJQUFJO0FBQ2xDO0FBQ0EsQ0FBRiJ9