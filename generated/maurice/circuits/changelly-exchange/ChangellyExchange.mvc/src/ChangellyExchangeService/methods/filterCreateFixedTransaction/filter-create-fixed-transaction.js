import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterCreateFixedTransaction} */
export default async function filterCreateFixedTransaction({
 amountFrom:amountFrom,currencyTo:currencyTo,currencyFrom:currencyFrom,
 address:address,refundAddress:refundAddress,fixedId,
}) {
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{CreateFixTransaction:CreateFixTransaction}}=changelly
 try{
  const transaction=await CreateFixTransaction({
   address:      address,
   from:         currencyFrom,
   to:           currencyTo,
   refundAddress:refundAddress,
   rateId:       fixedId,
   amountFrom:   amountFrom,
  })
  const{
   id,createdAt,payinAddress,status,kycRequired,amountExpectedFrom,
   payinExtraId,changellyFee,
  }=transaction

  return{
   partnerFee:changellyFee,
   id:`chngl-${id}`,
   createdAt:createdAt,
   payinAddress:payinAddress,
   payinExtraId:payinExtraId,
   status:status,
   kycRequired:kycRequired,
   confirmedAmountFrom:amountExpectedFrom,
  }
 }catch(err){
  if(/expired/.test(err)){
   return {
    getOffer:true,
   }
  }
  if(err.code) {
   throw new Error(`!${err.message}`)
  }
  throw err
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBd2dCRyxNQUFNLFNBQVMsNEJBQTRCO0NBQzFDLHFCQUFxQixDQUFDLHFCQUFxQixDQUFDLHlCQUF5QjtDQUNyRSxlQUFlLENBQUMsMkJBQTJCLENBQUMsT0FBTztBQUNwRCxDQUFDO0NBQ0EsTUFBTSxjQUFjLFNBQVMsR0FBRztDQUNoQyxNQUFNLGNBQWMseUNBQXlDLEdBQUc7Q0FDaEU7RUFDQyxNQUFNLFdBQVcsQ0FBQyxNQUFNLG9CQUFvQjtHQUMzQyxjQUFjLE9BQU87R0FDckIsY0FBYyxZQUFZO0dBQzFCLGNBQWMsVUFBVTtHQUN4QiwyQkFBMkI7R0FDM0IsY0FBYyxPQUFPO0dBQ3JCLGNBQWMsVUFBVTtBQUMzQixHQUFHO0VBQ0Q7R0FDQyxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtHQUMvRCxZQUFZLENBQUMsWUFBWTtJQUN4Qjs7RUFFRjtHQUNDLHVCQUF1QjtHQUN2QixZQUFZLEVBQUUsQ0FBQyxDQUFDO0dBQ2hCLG1CQUFtQjtHQUNuQix5QkFBeUI7R0FDekIseUJBQXlCO0dBQ3pCLGFBQWE7R0FDYix1QkFBdUI7R0FDdkIsc0NBQXNDO0FBQ3pDO0FBQ0EsRUFBRSxLQUFLLENBQUM7RUFDTixFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztHQUNqQjtJQUNDLGFBQWE7QUFDakI7QUFDQTtFQUNFLEVBQUUsQ0FBQyxHQUFHLENBQUM7R0FDTixNQUFNLElBQUksS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQztBQUNuQztFQUNFLE1BQU07QUFDUjtBQUNBLENBQUYifQ==