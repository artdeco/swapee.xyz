import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterGetFixedOffer} */
export default async function filterGetFixedOffer({
 amountFrom:amountFrom,currencyFrom:currencyFrom,currencyTo:currencyTo,
}){
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{GetExtendedFixRateForAmount:GetExtendedFixRateForAmount}}=changelly

 try {
  const R=await GetExtendedFixRateForAmount({
   amountFrom:amountFrom,
   from:currencyFrom,
   to:currencyTo,
  })
  // debugger // result:result,
  const{id:id,amountTo:amountTo,rate}=R
  // result is rate
  return{
   estimatedFixedAmountTo:amountTo,fixedId:id,rate:rate,
  }
 }catch(err) {
  if(err['code']==-32600) {
   // Invalid amount: minimal amount for matic->bat is 33.813
   if(/maximal/.test(err.message)) {
    const[max]=/[\d.]+$/.exec(err.message)||[]
    return {
     maxAmount:max,
    }
   }
   const[min]=/[\d.]+$/.exec(err.message)||[]
   return {
    minAmount:min,
   }
  }else if(err['code']) {
   throw new Error(`!${err.message}`)
  }
  throw err
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBb1NHLE1BQU0sU0FBUyxtQkFBbUI7Q0FDakMscUJBQXFCLENBQUMseUJBQXlCLENBQUMscUJBQXFCO0FBQ3RFLENBQUM7Q0FDQSxNQUFNLGNBQWMsU0FBUyxHQUFHO0NBQ2hDLE1BQU0sY0FBYyx1REFBdUQsR0FBRzs7Q0FFOUU7RUFDQyxNQUFNLENBQUMsQ0FBQyxNQUFNLDJCQUEyQjtHQUN4QyxxQkFBcUI7R0FDckIsaUJBQWlCO0dBQ2pCLGFBQWE7QUFDaEIsR0FBRztFQUNELEdBQUcsU0FBUyxHQUFHLGFBQWE7RUFDNUIsTUFBTSxLQUFLLENBQUMsaUJBQWlCLENBQUMsTUFBTTtFQUNwQyxHQUFHLE9BQU8sR0FBRztFQUNiO0dBQ0MsK0JBQStCLENBQUMsVUFBVSxDQUFDLFNBQVM7QUFDdkQ7QUFDQSxFQUFFLEtBQUssQ0FBQztFQUNOLEVBQUUsQ0FBQyxXQUFXLEVBQUU7R0FDZixHQUFHLFFBQVEsUUFBUSxRQUFRLE9BQU8sSUFBSSxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztHQUN2RCxFQUFFLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDckIsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUM5QjtLQUNDLGFBQWE7QUFDbEI7QUFDQTtHQUNHLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7R0FDOUI7SUFDQyxhQUFhO0FBQ2pCO0FBQ0EsR0FBRyxLQUFLLEVBQUUsQ0FBQztHQUNSLE1BQU0sSUFBSSxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDO0FBQ25DO0VBQ0UsTUFBTTtBQUNSO0FBQ0EsQ0FBRiJ9