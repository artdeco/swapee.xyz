import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterGetTransaction} */
export default async function filterGetTransaction({
 tid:tid,
}){
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{ListTransactions:ListTransactions,GetExtendedExchangeAmount}}=changelly
 const id=tid.replace(/^chngl-/,'')
 const[transaction]=await ListTransactions({id:id})
 if(!transaction) {
  return{
   notFound:true,
  }
 }
 let networkFee='0'

 const{
  createdAt:createdAt,payinAddress:payinAddress,status:status,
  kycRequired:kycRequired,type:type,currencyFrom,currencyTo,
  amountExpectedFrom:amountExpectedFrom,amountExpectedTo:amountExpectedTo,
  payinExtraId:payinExtraId,changellyFee:changellyFee,rate:rate,
 }=transaction // 1.874518865 1.86702079

 const isFixed=type=='fixed'

 if(!isFixed) try {
  const data=await GetExtendedExchangeAmount({
   amount:transaction.amountExpectedFrom,
   from:transaction.currencyFrom,
   to:transaction.currencyTo,
  }) // extract network fee from here
  networkFee=data.networkFee
  networkFee=networkFee.replace(/(\.\d*?)0+$/,'$1')
 }catch(err){}

 let result=Number(amountExpectedTo)-Number(networkFee)

 const getLongestDecimalLength=(...numbers)=>numbers.reduce((previousLength,number)=>{
  let[,afterDot]=number.split('.')
  if(!afterDot) return previousLength
  // afterDot=afterDot.replace(/0+$/,'')
  return afterDot.length>previousLength?afterDot.length:previousLength
 },0)
 const decimalLength=getLongestDecimalLength(amountExpectedTo,networkFee)
 result=result.toFixed(decimalLength).replace(/(\.\d*?)0+$/,'$1')

 return{
  fixed        :isFixed,
  currencyFrom :currencyFrom.toUpperCase(),
  currencyTo   :currencyTo.toUpperCase(),
  amountFrom   :amountExpectedFrom,
  amountTo     :result,
  rate         :rate,
  visibleAmount:amountExpectedTo,
  networkFee   :networkFee,
  partnerFee   :changellyFee,

  // IExchangeBrokerPort
  id:tid,
  createdAt:createdAt,
  payinAddress:payinAddress,
  payinExtraId:payinExtraId,
  status:status,
  confirmedAmountFrom:amountExpectedFrom,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBd2NHLE1BQU0sU0FBUyxvQkFBb0I7Q0FDbEMsT0FBTztBQUNSLENBQUM7Q0FDQSxNQUFNLGNBQWMsU0FBUyxHQUFHO0NBQ2hDLE1BQU0sY0FBYyxpQ0FBaUMsQ0FBQyx5QkFBeUIsR0FBRztDQUNsRixNQUFNLEVBQUUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQztDQUMvQixrQkFBa0IsQ0FBQyxNQUFNLGdCQUFnQixFQUFFLEtBQUssQ0FBQztDQUNqRCxFQUFFLENBQUM7RUFDRjtHQUNDLGFBQWE7QUFDaEI7QUFDQTtDQUNDLElBQUksVUFBVSxDQUFDOztDQUVmO0VBQ0MsbUJBQW1CLENBQUMseUJBQXlCLENBQUMsYUFBYTtFQUMzRCx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFVBQVU7RUFDekQscUNBQXFDLENBQUMsaUNBQWlDO0VBQ3ZFLHlCQUF5QixDQUFDLHlCQUF5QixDQUFDLFNBQVM7R0FDNUQsWUFBWSxHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQzs7Q0FFL0IsTUFBTSxPQUFPLENBQUMsSUFBSSxFQUFFOztDQUVwQixFQUFFLENBQUMsVUFBVTtFQUNaLE1BQU0sSUFBSSxDQUFDLE1BQU0seUJBQXlCO0dBQ3pDLGtCQUFrQixDQUFDLGtCQUFrQjtHQUNyQyxnQkFBZ0IsQ0FBQyxZQUFZO0dBQzdCLGNBQWMsQ0FBQyxVQUFVO0FBQzVCLEdBQUcsRUFBRSxHQUFHLFFBQVEsUUFBUSxJQUFJLEtBQUs7RUFDL0IsVUFBVSxDQUFDLElBQUksQ0FBQztFQUNoQixVQUFVLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztBQUM5QyxFQUFFLEtBQUssQ0FBQyxJQUFJOztDQUVYLElBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQzs7Q0FFM0MsTUFBTSx1QkFBdUIsQ0FBQyxDQUFDLEdBQUcsVUFBVSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsY0FBYyxDQUFDO0VBQzFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO0VBQzlCLEVBQUUsQ0FBQyxXQUFXLE9BQU87RUFDckIsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7RUFDbkMsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDO0FBQ2pELEdBQUc7Q0FDRixNQUFNLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxnQkFBZ0IsQ0FBQztDQUM3RCxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDOztDQUUzRDtFQUNDLGFBQWEsUUFBUTtFQUNyQixhQUFhLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztFQUN4QyxhQUFhLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztFQUN0QyxhQUFhLG1CQUFtQjtFQUNoQyxhQUFhLE9BQU87RUFDcEIsYUFBYSxLQUFLO0VBQ2xCLDhCQUE4QjtFQUM5QixhQUFhLFdBQVc7RUFDeEIsYUFBYSxhQUFhOztFQUUxQixHQUFHO0VBQ0gsTUFBTTtFQUNOLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIseUJBQXlCO0VBQ3pCLGFBQWE7RUFDYixzQ0FBc0M7QUFDeEM7QUFDQSxDQUFGIn0=