import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._fiterGetTransaction} */
export default async function fiterGetTransaction({
 id,
}){
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{ListTransactions:ListTransactions}}=changelly
 const[transaction]=await ListTransactions({id:id})

 const{
  createdAt,payinAddress,status,kycRequired,amountExpectedFrom,amountExpectedTo,
  payinExtraId,changellyFee,networkFee,type:type,
 }=transaction
 const result=new Number(amountExpectedTo)-new Number(networkFee)

 const isFixed=type=='fixed'
 debugger
 // maybe install on the deal broker too

 return{
  // id:`chngn-${id}`,
  networkFee:networkFee,
  partnerFee:changellyFee,
  id:`chngl-${id}`,
  createdAt:createdAt,
  payinAddress:payinAddress,
  payinExtraId:payinExtraId,
  status:status,
  kycRequired:kycRequired,
  confirmedAmountFrom:amountExpectedFrom,
  ...(isFixed?{fixedAmountTo:result}:{expectedAmountTo:result}),
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBNGJHLE1BQU0sU0FBUyxtQkFBbUI7Q0FDakMsRUFBRTtBQUNILENBQUM7Q0FDQSxNQUFNLGNBQWMsU0FBUyxHQUFHO0NBQ2hDLE1BQU0sY0FBYyxpQ0FBaUMsR0FBRztDQUN4RCxrQkFBa0IsQ0FBQyxNQUFNLGdCQUFnQixFQUFFLEtBQUssQ0FBQzs7Q0FFakQ7RUFDQyxTQUFTLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUMsZ0JBQWdCO0VBQzdFLFlBQVksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVM7R0FDN0M7Q0FDRixNQUFNLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxzQkFBc0IsTUFBTSxDQUFDOztDQUVyRCxNQUFNLE9BQU8sQ0FBQyxJQUFJLEVBQUU7Q0FDcEI7Q0FDQSxHQUFHLE1BQU0sUUFBUSxHQUFHLElBQUksS0FBSyxPQUFPOztDQUVwQztFQUNDLEdBQUcsWUFBWSxFQUFFLENBQUMsQ0FBQztFQUNuQixxQkFBcUI7RUFDckIsdUJBQXVCO0VBQ3ZCLFlBQVksRUFBRSxDQUFDLENBQUM7RUFDaEIsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6Qix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixzQ0FBc0M7QUFDeEMsS0FBSyxDQUFDLFNBQVMsb0JBQW9CLENBQUMsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO0FBQy9EO0FBQ0EsQ0FBRiJ9