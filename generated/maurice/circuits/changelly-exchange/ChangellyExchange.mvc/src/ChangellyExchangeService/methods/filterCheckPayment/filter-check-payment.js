import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterCheckPayment} */
export default async function filterCheckPayment({
 id:id,
}) {
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{GetStatus:GetStatus}}=changelly

 id=id.replace(/^chngl-/,'')
 try {
  const status=await GetStatus({
   id:id,
  })
  return{
   status:status,
  }
 }catch(err) {
  return {
   checkPaymentError:err.message,
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBZ1JHLE1BQU0sU0FBUyxrQkFBa0I7Q0FDaEMsS0FBSztBQUNOLENBQUM7Q0FDQSxNQUFNLGNBQWMsU0FBUyxHQUFHO0NBQ2hDLE1BQU0sY0FBYyxtQkFBbUIsR0FBRzs7Q0FFMUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO0NBQ3hCO0VBQ0MsTUFBTSxNQUFNLENBQUMsTUFBTSxTQUFTO0dBQzNCLEtBQUs7QUFDUixHQUFHO0VBQ0Q7R0FDQyxhQUFhO0FBQ2hCO0FBQ0EsRUFBRSxLQUFLLENBQUM7RUFDTjtHQUNDLHFCQUFxQixDQUFDLE9BQU87QUFDaEM7QUFDQTtBQUNBLENBQUYifQ==