import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IChangellyExchangeService._filterCreateTransaction} */
export default async function filterCreateTransaction({
 amountFrom:amountFrom,currencyTo:currencyTo,currencyFrom:currencyFrom,
 address:address,refundAddress:refundAddress,
}) {
 const{asUChangelly:{changelly}}=this
 const{asIChangelly:{CreateTransaction:CreateTransaction}}=changelly
 try{
  const transaction=await CreateTransaction({
   address:address,
   from:currencyFrom,
   to:currencyTo,
   refundAddress:refundAddress,
   amount:amountFrom,
  })
  const{
   id,createdAt,payinAddress,status,kycRequired,amountExpectedFrom,
   payinExtraId:payinExtraId,
  }=transaction

  // const result=new Number(amountExpectedTo)-new Number(networkFee)
  return {
   id:`chngl-${id}`,
   createdAt:createdAt,
   payinAddress:payinAddress,
   payinExtraId:payinExtraId,
   status:status,
   kycRequired:kycRequired,
   confirmedAmountFrom:amountExpectedFrom,
  }
 }catch(err){
  return{
   createTransactionError:err.message,
   // changellyFloatingError:1
  }
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBb2FHLE1BQU0sU0FBUyx1QkFBdUI7Q0FDckMscUJBQXFCLENBQUMscUJBQXFCLENBQUMseUJBQXlCO0NBQ3JFLGVBQWUsQ0FBQywyQkFBMkI7QUFDNUMsQ0FBQztDQUNBLE1BQU0sY0FBYyxTQUFTLEdBQUc7Q0FDaEMsTUFBTSxjQUFjLG1DQUFtQyxHQUFHO0NBQzFEO0VBQ0MsTUFBTSxXQUFXLENBQUMsTUFBTSxpQkFBaUI7R0FDeEMsZUFBZTtHQUNmLGlCQUFpQjtHQUNqQixhQUFhO0dBQ2IsMkJBQTJCO0dBQzNCLGlCQUFpQjtBQUNwQixHQUFHO0VBQ0Q7R0FDQyxFQUFFLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtHQUMvRCx5QkFBeUI7SUFDeEI7O0VBRUYsR0FBRyxNQUFNLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxzQkFBc0IsTUFBTSxDQUFDO0VBQ3hEO0dBQ0MsWUFBWSxFQUFFLENBQUMsQ0FBQztHQUNoQixtQkFBbUI7R0FDbkIseUJBQXlCO0dBQ3pCLHlCQUF5QjtHQUN6QixhQUFhO0dBQ2IsdUJBQXVCO0dBQ3ZCLHNDQUFzQztBQUN6QztBQUNBLEVBQUUsS0FBSyxDQUFDO0VBQ047R0FDQywwQkFBMEIsQ0FBQyxPQUFPO0dBQ2xDLEdBQUc7QUFDTjtBQUNBO0FBQ0EsQ0FBRiJ9