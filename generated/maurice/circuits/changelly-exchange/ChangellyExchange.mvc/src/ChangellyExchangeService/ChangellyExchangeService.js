import AbstractChangellyExchangeService from '../../gen/AbstractChangellyExchangeService'
import filterCheckPayment from './methods/filterCheckPayment/filter-check-payment'
import filterGetFixedOffer from './methods/filterGetFixedOffer/filter-get-fixed-offer'
import filterGetOffer from './methods/filterGetOffer/filter-get-offer'
import filterCreateTransaction from './methods/filterCreateTransaction/filter-create-transaction'
import filterGetTransaction from './methods/filterGetTransaction/filter-get-transaction'
import filterCreateFixedTransaction from './methods/filterCreateFixedTransaction/filter-create-fixed-transaction'
import {ChangellyUniversal} from '@type.community/changelly.com'
import {ChangeNowUniversal} from '@swapee.xyz/swapee.xyz'
import {LetsExchangeUniversal} from '@swapee.xyz/swapee.xyz'

/**@extends {xyz.swapee.wc.ChangellyExchangeService} */
export default class extends AbstractChangellyExchangeService.implements(
 ChangellyUniversal,
 ChangeNowUniversal,
 LetsExchangeUniversal,
 AbstractChangellyExchangeService.class.prototype=/**@type {!xyz.swapee.wc.ChangellyExchangeService}*/({
  filterCheckPayment:filterCheckPayment,
  filterGetFixedOffer:filterGetFixedOffer,
  filterGetOffer:filterGetOffer,
  filterCreateTransaction:filterCreateTransaction,
  filterGetTransaction:filterGetTransaction,
  filterCreateFixedTransaction:filterCreateFixedTransaction,
 }),
){}