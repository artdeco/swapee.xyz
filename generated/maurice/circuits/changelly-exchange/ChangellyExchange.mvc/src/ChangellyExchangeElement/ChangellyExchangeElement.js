import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ChangellyExchangeServerController from '../ChangellyExchangeServerController'
import ChangellyExchangeServerComputer from '../ChangellyExchangeServerComputer'
import ChangellyExchangeService from '../../src/ChangellyExchangeService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractChangellyExchangeElement from '../../gen/AbstractChangellyExchangeElement'

/** @extends {xyz.swapee.wc.ChangellyExchangeElement} */
export default class ChangellyExchangeElement extends AbstractChangellyExchangeElement.implements(
 ChangellyExchangeServerController,
 ChangellyExchangeServerComputer,
 ChangellyExchangeService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IChangellyExchangeElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
   classesMap: true,
   rootSelector:     `.ChangellyExchange`,
   stylesheet:       'html/styles/ChangellyExchange.css',
   blockName:        'html/ChangellyExchangeBlock.html',
  }),
  /**@type {xyz.swapee.wc.IChangellyExchangeDesigner}*/({
  }),
){}

// thank you for using web circuits
