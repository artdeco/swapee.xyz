/** @type {xyz.swapee.wc.IChangellyExchangeElement._short} */
export default function short({
 loadingCheckPayment,loadCheckPaymentError,
 loadingGetTransaction:loadingGetTransaction,
 loadGetTransactionError:loadGetTransactionError,
 // loadingCheckingPayment,loadCheckPaymentError,
},{ExchangeBroker:ExchangeBroker,DealBroker:DealBroker,TransactionInfo},{ExchangeIntent:{ready:ready}}){
 return <>
  {/* once when connected */}
  <DealBroker getOffer={ready} />
  <TransactionInfo gettingTransaction={loadingGetTransaction} getTransactionError={loadGetTransactionError} />
  <ExchangeBroker
   checkingStatus={loadingCheckPayment}
   checkStatusError={loadCheckPaymentError}
   checkPaymentError={loadCheckPaymentError}
  />
 </>
}