/** @type {xyz.swapee.wc.IChangellyExchangeElement._relay} */
export default function relay({
 This:This,
 ExchangeBroker:_ChangellyExchange,
 DealBroker:DealBroker,
 ExchangeIntent:ExchangeIntent,
 TransactionInfo:TransactionInfo,
}){
 return <>
  <TransactionInfo onGetTransactionHigh={This.loadGetTransaction()} />
  <ExchangeIntent onCurrencyFromHigh={
   DealBroker.pulseGetOffer()
  } onCurrencyToHigh={
   DealBroker.pulseGetOffer()
  } onAmountFromHigh={
   DealBroker.pulseGetOffer()
  } onFixedHigh={
   DealBroker.pulseGetOffer()
  } />
  <This
   onLoadingCreateTransactionHigh={_ChangellyExchange.unsetCreateTransactionError()}
   onLoadingCheckPaymentHigh={_ChangellyExchange.unsetCheckPaymentError()}
  />
 </>
}