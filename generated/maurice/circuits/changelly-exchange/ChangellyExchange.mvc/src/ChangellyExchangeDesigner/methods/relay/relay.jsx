
/**@type {xyz.swapee.wc.IChangellyExchangeDesigner._relay} */
export default function relay({
 This:This,
 ExchangeBroker:_ChangellyExchange,
 DealBroker:DealBroker,
 ExchangeIntent:ExchangeIntent,
 TransactionInfo:TransactionInfo,
}){
 return h(Fragment,{},
  h(TransactionInfo,{ onGetTransactionHigh:This.loadGetTransaction() }),
  h(ExchangeIntent,{ onCurrencyFromHigh:
   DealBroker.pulseGetOffer()
  , onCurrencyToHigh:
   DealBroker.pulseGetOffer()
  , onAmountFromHigh:
   DealBroker.pulseGetOffer()
  , onFixedHigh:
   DealBroker.pulseGetOffer()
   }),
  h(This,{
   onLoadingCreateTransactionHigh:_ChangellyExchange.unsetCreateTransactionError(),
   onLoadingCheckPaymentHigh:_ChangellyExchange.unsetCheckPaymentError()
  })
 )
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvY2hhbmdlbGx5LWV4Y2hhbmdlL2NoYW5nZWxseS1leGNoYW5nZS53Y2t0Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBc1BHLFNBQVMsS0FBSztDQUNiLFNBQVM7Q0FDVCxpQ0FBaUM7Q0FDakMscUJBQXFCO0NBQ3JCLDZCQUE2QjtDQUM3QiwrQkFBK0I7QUFDaEMsQ0FBQztDQUNBLE9BQU8sWUFBQztFQUNQLG9CQUFpQixxQkFBc0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUU7RUFDakUsbUJBQWdCO0dBQ2YsVUFBVSxDQUFDLGFBQWEsQ0FBQztFQUN6QixFQUFDO0dBQ0QsVUFBVSxDQUFDLGFBQWEsQ0FBQztFQUN6QixFQUFDO0dBQ0QsVUFBVSxDQUFDLGFBQWEsQ0FBQztFQUN6QixFQUFDO0dBQ0QsVUFBVSxDQUFDLGFBQWEsQ0FBQztFQUN6QjtFQUNEO0dBQ0MsK0JBQWdDLGtCQUFrQixDQUFDLDJCQUEyQixDQUFDLENBQUU7R0FDakYsMEJBQTJCLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDO0FBQ3hFO0NBQ0M7QUFDRCxDQUFGIn0=