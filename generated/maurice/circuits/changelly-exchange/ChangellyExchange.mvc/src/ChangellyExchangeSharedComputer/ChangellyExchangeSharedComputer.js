import adaptCreatingTransaction from './methods/adapt-creating-transaction'
import adaptCreateTransactionError from './methods/adapt-create-transaction-error'
import adaptGetOfferError from './methods/adapt-get-offer-error'
import adaptGettingOffer from './methods/adapt-getting-offer'
import adaptRegion from './methods/adapt-region'
import {preadaptCreatingTransaction,preadaptCreateTransactionError,preadaptGetOfferError,preadaptGettingOffer,preadaptRegion} from '../../gen/AbstractChangellyExchangeComputer/preadapters'
import AbstractChangellyExchangeComputer from '../../gen/AbstractChangellyExchangeComputer'

const ChangellyExchangeSharedComputer=AbstractChangellyExchangeComputer.__trait(
 /** @type {!xyz.swapee.wc.IChangellyExchangeComputer} */ ({
  adaptCreatingTransaction:adaptCreatingTransaction,
  adaptCreateTransactionError:adaptCreateTransactionError,
  adaptGetOfferError:adaptGetOfferError,
  adaptGettingOffer:adaptGettingOffer,
  adaptRegion:adaptRegion,
  adapt:[preadaptCreatingTransaction,preadaptCreateTransactionError,preadaptGetOfferError,preadaptGettingOffer,preadaptRegion],
 }),
)
export default ChangellyExchangeSharedComputer