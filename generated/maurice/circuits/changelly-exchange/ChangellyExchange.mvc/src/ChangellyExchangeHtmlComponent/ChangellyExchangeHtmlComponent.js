import ChangellyExchangeHtmlController from '../ChangellyExchangeHtmlController'
import ChangellyExchangeHtmlComputer from '../ChangellyExchangeHtmlComputer'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractChangellyExchangeHtmlComponent} from '../../gen/AbstractChangellyExchangeHtmlComponent'

/** @extends {xyz.swapee.wc.ChangellyExchangeHtmlComponent} */
export default class extends AbstractChangellyExchangeHtmlComponent.implements(
 ChangellyExchangeHtmlController,
 ChangellyExchangeHtmlComputer,
 IntegratedComponentInitialiser,
){}