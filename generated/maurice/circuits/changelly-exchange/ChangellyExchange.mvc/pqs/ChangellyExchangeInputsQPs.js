import {ChangellyExchangeInputsPQs} from './ChangellyExchangeInputsPQs'
export const ChangellyExchangeInputsQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeInputsQPs}*/(Object.keys(ChangellyExchangeInputsPQs)
 .reduce((a,k)=>{a[ChangellyExchangeInputsPQs[k]]=k;return a},{}))