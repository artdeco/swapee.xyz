import {ChangellyExchangeMemoryPQs} from './ChangellyExchangeMemoryPQs'
export const ChangellyExchangeInputsPQs=/**@type {!xyz.swapee.wc.ChangellyExchangeInputsQPs}*/({
 ...ChangellyExchangeMemoryPQs,
})