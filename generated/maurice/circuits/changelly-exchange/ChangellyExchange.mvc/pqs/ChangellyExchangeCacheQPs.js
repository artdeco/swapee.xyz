import {ChangellyExchangeCachePQs} from './ChangellyExchangeCachePQs'
export const ChangellyExchangeCacheQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeCacheQPs}*/(Object.keys(ChangellyExchangeCachePQs)
 .reduce((a,k)=>{a[ChangellyExchangeCachePQs[k]]=k;return a},{}))