import {ChangellyExchangeVdusPQs} from './ChangellyExchangeVdusPQs'
export const ChangellyExchangeVdusQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeVdusQPs}*/(Object.keys(ChangellyExchangeVdusPQs)
 .reduce((a,k)=>{a[ChangellyExchangeVdusPQs[k]]=k;return a},{}))