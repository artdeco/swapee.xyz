import {ChangellyExchangeQueriesPQs} from './ChangellyExchangeQueriesPQs'
export const ChangellyExchangeQueriesQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeQueriesQPs}*/(Object.keys(ChangellyExchangeQueriesPQs)
 .reduce((a,k)=>{a[ChangellyExchangeQueriesPQs[k]]=k;return a},{}))