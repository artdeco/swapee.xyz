import {ChangellyExchangeMemoryPQs} from './ChangellyExchangeMemoryPQs'
export const ChangellyExchangeMemoryQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeMemoryQPs}*/(Object.keys(ChangellyExchangeMemoryPQs)
 .reduce((a,k)=>{a[ChangellyExchangeMemoryPQs[k]]=k;return a},{}))