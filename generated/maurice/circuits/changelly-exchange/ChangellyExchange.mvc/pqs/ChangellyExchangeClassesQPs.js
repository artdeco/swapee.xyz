import ChangellyExchangeClassesPQs from './ChangellyExchangeClassesPQs'
export const ChangellyExchangeClassesQPs=/**@type {!xyz.swapee.wc.ChangellyExchangeClassesQPs}*/(Object.keys(ChangellyExchangeClassesPQs)
 .reduce((a,k)=>{a[ChangellyExchangeClassesPQs[k]]=k;return a},{}))