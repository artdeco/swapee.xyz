import ChangellyExchangeBuffer from '../ChangellyExchangeBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ChangellyExchangePortConnector} from '../ChangellyExchangePort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeController}
 */
function __AbstractChangellyExchangeController() {}
__AbstractChangellyExchangeController.prototype = /** @type {!_AbstractChangellyExchangeController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeController}
 */
class _AbstractChangellyExchangeController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeController} ‎
 */
export class AbstractChangellyExchangeController extends newAbstract(
 _AbstractChangellyExchangeController,201339238025,null,{
  asIChangellyExchangeController:1,
  superChangellyExchangeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeController} */
AbstractChangellyExchangeController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeController} */
function AbstractChangellyExchangeControllerClass(){}


AbstractChangellyExchangeController[$implementations]=[
 AbstractChangellyExchangeControllerClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IChangellyExchangePort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractChangellyExchangeController,
 ChangellyExchangeBuffer,
 IntegratedController,
 /**@type {!AbstractChangellyExchangeController}*/(ChangellyExchangePortConnector),
]


export default AbstractChangellyExchangeController