
/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptGetFixedOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}=this.land
 if(!ExchangeIntent) return
 if(!DealBroker) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetFixedOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
  fixed:ExchangeIntent.model['cec31'],
  ready:ExchangeIntent.model['b2fda'],
  getOffer:DealBroker.model['b370a'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)||(!__inputs.fixed)||(!__inputs.ready)||(!__inputs.getOffer)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.fixed=changes['cec31']
 changes.ready=changes['b2fda']
 changes.getOffer=changes['b370a']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptGetFixedOffer(__inputs,__changes)
 return RET.then(R=>{
  const{rate:rate,fixedId:fixedId,estimatedFixedAmountTo:estimatedFixedAmountTo,minAmount:minAmount,maxAmount:maxAmount,...REST}=R
  if(DealBroker) DealBroker.port.inputs['67942']=rate
  if(DealBroker) DealBroker.port.inputs['2568a']=fixedId
  if(DealBroker) DealBroker.port.inputs['ea0a0']=estimatedFixedAmountTo
  if(DealBroker) DealBroker.port.inputs['ae0d3']=minAmount
  if(DealBroker) DealBroker.port.inputs['d0b0c']=maxAmount
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptGetOffer(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}=this.land
 if(!ExchangeIntent) return
 if(!DealBroker) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOffer.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
  fixed:ExchangeIntent.model['cec31'],
  ready:ExchangeIntent.model['b2fda'],
  getOffer:DealBroker.model['b370a'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)||(!__inputs.ready)||(!__inputs.getOffer)||(__inputs.fixed)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.fixed=changes['cec31']
 changes.ready=changes['b2fda']
 changes.getOffer=changes['b370a']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptGetOffer(__inputs,__changes)
 return RET.then(R=>{
  const{rate:rate,estimatedFloatAmountTo:estimatedFloatAmountTo,networkFee:networkFee,partnerFee:partnerFee,visibleAmount:visibleAmount,minAmount:minAmount,maxAmount:maxAmount,...REST}=R
  if(DealBroker) DealBroker.port.inputs['67942']=rate
  if(DealBroker) DealBroker.port.inputs['8ca05']=estimatedFloatAmountTo
  if(DealBroker) DealBroker.port.inputs['5fd9c']=networkFee
  if(DealBroker) DealBroker.port.inputs['96f44']=partnerFee
  if(DealBroker) DealBroker.port.inputs['4685c']=visibleAmount
  if(DealBroker) DealBroker.port.inputs['ae0d3']=minAmount
  if(DealBroker) DealBroker.port.inputs['d0b0c']=maxAmount
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptCreateTransaction(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent,ExchangeBroker:ExchangeBroker}=this.land
 if(!ExchangeIntent) return
 if(!ExchangeBroker) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransaction.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
  fixed:ExchangeIntent.model['cec31'],
  address:ExchangeBroker.model['884d9'],
  refundAddress:ExchangeBroker.model['be03a'],
  createTransaction:ExchangeBroker.model['cbfb1'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)||(!__inputs.address)||(!__inputs.createTransaction)||(__inputs.fixed)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.fixed=changes['cec31']
 changes.address=changes['884d9']
 changes.refundAddress=changes['be03a']
 changes.createTransaction=changes['cbfb1']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCreateTransaction(__inputs,__changes)
 return RET.then(R=>{
  const{id:id,createTransactionError:createTransactionError,createdAt:createdAt,payinAddress:payinAddress,payinExtraId:payinExtraId,status:status,kycRequired:kycRequired,confirmedAmountFrom:confirmedAmountFrom,...REST}=R
  if(ExchangeBroker) ExchangeBroker.port.inputs['b80bb']=id
  if(ExchangeBroker) ExchangeBroker.port.inputs['601f8']=createTransactionError
  if(ExchangeBroker) ExchangeBroker.port.inputs['97def']=createdAt
  if(ExchangeBroker) ExchangeBroker.port.inputs['59af8']=payinAddress
  if(ExchangeBroker) ExchangeBroker.port.inputs['7a19b']=payinExtraId
  if(ExchangeBroker) ExchangeBroker.port.inputs['9acb4']=status
  if(ExchangeBroker) ExchangeBroker.port.inputs['2b803']=kycRequired
  if(ExchangeBroker) ExchangeBroker.port.inputs['6103f']=confirmedAmountFrom
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptGetTransaction(inputs,changes,mapForm) {
 const{TransactionInfo:TransactionInfo}=this.land
 if(!TransactionInfo) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetTransaction.Form}*/
 const _inputs={
  tid:TransactionInfo.model['97bea'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.tid) return
 changes.tid=changes['97bea']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptGetTransaction(__inputs,__changes)
 return RET.then(R=>{
  const{fixed:fixed,currencyFrom:currencyFrom,currencyTo:currencyTo,amountFrom:amountFrom,amountTo:amountTo,networkFee:networkFee,partnerFee:partnerFee,visibleAmount:visibleAmount,notFound:notFound,rate:rate,id:id,createdAt:createdAt,payinAddress:payinAddress,payinExtraId:payinExtraId,status:status,confirmedAmountFrom:confirmedAmountFrom,...REST}=R
  const{land:{ExchangeBroker:ExchangeBroker}}=this
  if(TransactionInfo) TransactionInfo.port.inputs['cec31']=fixed
  if(TransactionInfo) TransactionInfo.port.inputs['96c88']=currencyFrom
  if(TransactionInfo) TransactionInfo.port.inputs['c23cd']=currencyTo
  if(TransactionInfo) TransactionInfo.port.inputs['748e6']=amountFrom
  if(TransactionInfo) TransactionInfo.port.inputs['9dc9f']=amountTo
  if(TransactionInfo) TransactionInfo.port.inputs['5fd9c']=networkFee
  if(TransactionInfo) TransactionInfo.port.inputs['96f44']=partnerFee
  if(TransactionInfo) TransactionInfo.port.inputs['4685c']=visibleAmount
  if(TransactionInfo) TransactionInfo.port.inputs['5ccf4']=notFound
  if(TransactionInfo) TransactionInfo.port.inputs['67942']=rate
  if(ExchangeBroker) ExchangeBroker.port.inputs['b80bb']=id
  if(ExchangeBroker) ExchangeBroker.port.inputs['97def']=createdAt
  if(ExchangeBroker) ExchangeBroker.port.inputs['59af8']=payinAddress
  if(ExchangeBroker) ExchangeBroker.port.inputs['7a19b']=payinExtraId
  if(ExchangeBroker) ExchangeBroker.port.inputs['9acb4']=status
  if(ExchangeBroker) ExchangeBroker.port.inputs['6103f']=confirmedAmountFrom
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptCreateFixedTransaction(inputs,changes,mapForm) {
 const{ExchangeIntent:ExchangeIntent,ExchangeBroker:ExchangeBroker,DealBroker:DealBroker}=this.land
 if(!ExchangeIntent) return
 if(!ExchangeBroker) return
 if(!DealBroker) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateFixedTransaction.Form}*/
 const _inputs={
  amountFrom:ExchangeIntent.model['748e6'],
  currencyFrom:ExchangeIntent.model['96c88'],
  currencyTo:ExchangeIntent.model['c23cd'],
  fixed:ExchangeIntent.model['cec31'],
  address:ExchangeBroker.model['884d9'],
  refundAddress:ExchangeBroker.model['be03a'],
  fixedId:DealBroker.model['2568a'],
  createTransaction:ExchangeBroker.model['cbfb1'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if((!__inputs.amountFrom)||(!__inputs.currencyFrom)||(!__inputs.currencyTo)||(!__inputs.fixed)||(!__inputs.address)||(!__inputs.refundAddress)||(!__inputs.fixedId)||(!__inputs.createTransaction)) return
 changes.amountFrom=changes['748e6']
 changes.currencyFrom=changes['96c88']
 changes.currencyTo=changes['c23cd']
 changes.fixed=changes['cec31']
 changes.address=changes['884d9']
 changes.refundAddress=changes['be03a']
 changes.fixedId=changes['2568a']
 changes.createTransaction=changes['cbfb1']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCreateFixedTransaction(__inputs,__changes)
 return RET.then(R=>{
  const{getOffer:getOffer,id:id,createTransactionError:createTransactionError,createdAt:createdAt,payinAddress:payinAddress,payinExtraId:payinExtraId,status:status,kycRequired:kycRequired,confirmedAmountFrom:confirmedAmountFrom,...REST}=R
  if(DealBroker) DealBroker.port.inputs['b370a']=getOffer
  if(ExchangeBroker) ExchangeBroker.port.inputs['b80bb']=id
  if(ExchangeBroker) ExchangeBroker.port.inputs['601f8']=createTransactionError
  if(ExchangeBroker) ExchangeBroker.port.inputs['97def']=createdAt
  if(ExchangeBroker) ExchangeBroker.port.inputs['59af8']=payinAddress
  if(ExchangeBroker) ExchangeBroker.port.inputs['7a19b']=payinExtraId
  if(ExchangeBroker) ExchangeBroker.port.inputs['9acb4']=status
  if(ExchangeBroker) ExchangeBroker.port.inputs['2b803']=kycRequired
  if(ExchangeBroker) ExchangeBroker.port.inputs['6103f']=confirmedAmountFrom
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptCheckPayment(inputs,changes,mapForm) {
 const{ExchangeBroker:ExchangeBroker}=this.land
 if(!ExchangeBroker) return
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCheckPayment.Form}*/
 const _inputs={
  id:ExchangeBroker.model['b80bb'],
  checkPayment:ExchangeBroker.model['c9266'],
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.checkPayment) return
 changes.id=changes['b80bb']
 changes.checkPayment=changes['c9266']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCheckPayment(__inputs,__changes)
 return RET.then(R=>{
  const{status:status,checkPaymentError:checkPaymentError,...REST}=R
  if(ExchangeBroker) ExchangeBroker.port.inputs['9acb4']=status
  if(ExchangeBroker) ExchangeBroker.port.inputs['afbad']=checkPaymentError
  return REST
 })
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptRegion(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptRegion.Form}*/
 const _inputs={
  region:this.land.RegionSelector?this.land.RegionSelector.model['960db']:void 0,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if([null,void 0].includes(__inputs.region)) return
 changes.region=changes['960db']
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptRegion(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptGettingOffer(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGettingOffer.Form}*/
 const _inputs={
  loadingGetOffer:inputs.loadingGetOffer,
  loadingGetFixedOffer:inputs.loadingGetFixedOffer,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptGettingOffer(__inputs,__changes)
 const{gettingOffer:gettingOffer,...REST}=RET
 const{land:{DealBroker:DealBroker}}=this
 if(DealBroker) DealBroker.port.inputs['341da']=gettingOffer
 return REST
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptGetOfferError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptGetOfferError.Form}*/
 const _inputs={
  loadGetOfferError:inputs.loadGetOfferError,
  loadGetFixedOfferError:inputs.loadGetFixedOfferError,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptGetOfferError(__inputs,__changes)
 const{getOfferError:getOfferError,...REST}=RET
 const{land:{DealBroker:DealBroker}}=this
 if(DealBroker) DealBroker.port.inputs['5f4a3']=getOfferError
 return REST
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptCreatingTransaction(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreatingTransaction.Form}*/
 const _inputs={
  loadingCreateTransaction:inputs.loadingCreateTransaction,
  loadingCreateFixedTransaction:inputs.loadingCreateFixedTransaction,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCreatingTransaction(__inputs,__changes)
 const{creatingTransaction:creatingTransaction,...REST}=RET
 const{land:{ExchangeBroker:ExchangeBroker}}=this
 if(ExchangeBroker) ExchangeBroker.port.inputs['89fc6']=creatingTransaction
 return REST
}

/**@this {xyz.swapee.wc.IChangellyExchangeComputer}*/
export function preadaptCreateTransactionError(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IChangellyExchangeComputer.adaptCreateTransactionError.Form}*/
 const _inputs={
  loadCreateTransactionError:inputs.loadCreateTransactionError,
  loadCreateFixedTransactionError:inputs.loadCreateFixedTransactionError,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptCreateTransactionError(__inputs,__changes)
 const{createTransactionError:createTransactionError,...REST}=RET
 const{land:{ExchangeBroker:ExchangeBroker}}=this
 if(ExchangeBroker) ExchangeBroker.port.inputs['601f8']=createTransactionError
 return REST
}