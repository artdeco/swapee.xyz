import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeComputer}
 */
function __AbstractChangellyExchangeComputer() {}
__AbstractChangellyExchangeComputer.prototype = /** @type {!_AbstractChangellyExchangeComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeComputer}
 */
class _AbstractChangellyExchangeComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeComputer} ‎
 */
export class AbstractChangellyExchangeComputer extends newAbstract(
 _AbstractChangellyExchangeComputer,20133923801,null,{
  asIChangellyExchangeComputer:1,
  superChangellyExchangeComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeComputer} */
AbstractChangellyExchangeComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeComputer} */
function AbstractChangellyExchangeComputerClass(){}


AbstractChangellyExchangeComputer[$implementations]=[
 __AbstractChangellyExchangeComputer,
 Adapter,
]


export default AbstractChangellyExchangeComputer