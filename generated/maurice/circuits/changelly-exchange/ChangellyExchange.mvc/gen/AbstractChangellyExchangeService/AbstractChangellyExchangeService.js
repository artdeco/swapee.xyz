import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeService}
 */
function __AbstractChangellyExchangeService() {}
__AbstractChangellyExchangeService.prototype = /** @type {!_AbstractChangellyExchangeService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeService}
 */
class _AbstractChangellyExchangeService { }
/**
 * A service for the IChangellyExchange.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeService} ‎
 */
class AbstractChangellyExchangeService extends newAbstract(
 _AbstractChangellyExchangeService,201339238018,null,{
  asIChangellyExchangeService:1,
  superChangellyExchangeService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeService} */
AbstractChangellyExchangeService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeService} */
function AbstractChangellyExchangeServiceClass(){}

export default AbstractChangellyExchangeService


AbstractChangellyExchangeService[$implementations]=[
 __AbstractChangellyExchangeService,
]

export {AbstractChangellyExchangeService}