
import AbstractChangellyExchange from '../AbstractChangellyExchange'

/** @abstract {xyz.swapee.wc.IChangellyExchangeElement} */
export default class AbstractChangellyExchangeElement { }



AbstractChangellyExchangeElement[$implementations]=[AbstractChangellyExchange,
 /** @type {!AbstractChangellyExchangeElement} */ ({
  rootId:'ChangellyExchange',
  __$id:2013392380,
  fqn:'xyz.swapee.wc.IChangellyExchange',
  maurice_element_v3:true,
 }),
]