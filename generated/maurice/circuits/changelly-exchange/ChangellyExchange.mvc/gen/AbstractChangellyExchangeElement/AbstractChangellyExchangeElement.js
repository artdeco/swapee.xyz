import ChangellyExchangeRenderVdus from './methods/render-vdus'
import ChangellyExchangeElementPort from '../ChangellyExchangeElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {ChangellyExchangeInputsPQs} from '../../pqs/ChangellyExchangeInputsPQs'
import {ChangellyExchangeQueriesPQs} from '../../pqs/ChangellyExchangeQueriesPQs'
import {ChangellyExchangeCachePQs} from '../../pqs/ChangellyExchangeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractChangellyExchange from '../AbstractChangellyExchange'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeElement}
 */
function __AbstractChangellyExchangeElement() {}
__AbstractChangellyExchangeElement.prototype = /** @type {!_AbstractChangellyExchangeElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeElement}
 */
class _AbstractChangellyExchangeElement { }
/**
 * A component description.
 *
 * The _IChangellyExchange_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeElement} ‎
 */
class AbstractChangellyExchangeElement extends newAbstract(
 _AbstractChangellyExchangeElement,201339238014,null,{
  asIChangellyExchangeElement:1,
  superChangellyExchangeElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeElement} */
AbstractChangellyExchangeElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeElement} */
function AbstractChangellyExchangeElementClass(){}

export default AbstractChangellyExchangeElement


AbstractChangellyExchangeElement[$implementations]=[
 __AbstractChangellyExchangeElement,
 ElementBase,
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':host':hostColAttr,
   ':region':regionColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'host':hostAttr,
    'region':regionAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(regionAttr===undefined?{'region':regionColAttr}:{}),
   }
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'host':hostAttr,
   'region':regionAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    host:hostAttr,
    region:regionAttr,
   }
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  render:ChangellyExchangeRenderVdus,
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  classes:{
   '_': 'b14a7',
  },
  inputsPQs:ChangellyExchangeInputsPQs,
  queriesPQs:ChangellyExchangeQueriesPQs,
  cachePQs:ChangellyExchangeCachePQs,
  vdus:{
   'ExchangeBroker': 'j4a11',
   'RegionSelector': 'j4a12',
   'Debug': 'j4a13',
   'DealBroker': 'j4a14',
   'ExchangeIntent': 'j4a15',
   'TransactionInfo': 'j4a17',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','host','region','query:exchange-broker','query:deal-broker','query:exchange-intent','query:transaction-info','query:region-selector','no-solder',':no-solder',':host',':region','fe646','05bd2','38bd9','c9e36','c3b6b','6b245','aa2f5','67b3d','960db','children']),
   })
  },
  get Port(){
   return ChangellyExchangeElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:exchange-broker':exchangeBrokerSel,'query:deal-broker':dealBrokerSel,'query:exchange-intent':exchangeIntentSel,'query:transaction-info':transactionInfoSel,'query:region-selector':regionSelectorSel}){
   const _ret={}
   if(exchangeBrokerSel) _ret.exchangeBrokerSel=exchangeBrokerSel
   if(dealBrokerSel) _ret.dealBrokerSel=dealBrokerSel
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   if(transactionInfoSel) _ret.transactionInfoSel=transactionInfoSel
   if(regionSelectorSel) _ret.regionSelectorSel=regionSelectorSel
   return _ret
  },
 }),
 Landed,
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  constructor(){
   this.land={
    ExchangeBroker:null,
    DealBroker:null,
    ExchangeIntent:null,
    TransactionInfo:null,
    RegionSelector:null,
   }
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:async function awaitOnExchangeBroker({exchangeBrokerSel:exchangeBrokerSel}){
   if(!exchangeBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeBroker=await milleu(exchangeBrokerSel)
   if(!ExchangeBroker) {
    console.warn('❗️ exchangeBrokerSel %s must be present on the page for %s to work',exchangeBrokerSel,fqn)
    return{}
   }
   land.ExchangeBroker=ExchangeBroker
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:async function awaitOnDealBroker({dealBrokerSel:dealBrokerSel}){
   if(!dealBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const DealBroker=await milleu(dealBrokerSel)
   if(!DealBroker) {
    console.warn('❗️ dealBrokerSel %s must be present on the page for %s to work',dealBrokerSel,fqn)
    return{}
   }
   land.DealBroker=DealBroker
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:async function awaitOnTransactionInfo({transactionInfoSel:transactionInfoSel}){
   if(!transactionInfoSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const TransactionInfo=await milleu(transactionInfoSel)
   if(!TransactionInfo) {
    console.warn('❗️ transactionInfoSel %s must be present on the page for %s to work',transactionInfoSel,fqn)
    return{}
   }
   land.TransactionInfo=TransactionInfo
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  calibrate:async function awaitOnRegionSelector({regionSelectorSel:regionSelectorSel}){
   if(!regionSelectorSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const RegionSelector=await milleu(regionSelectorSel)
   if(!RegionSelector) {
    console.warn('❗️ regionSelectorSel %s must be present on the page for %s to work',regionSelectorSel,fqn)
    return{}
   }
   land.RegionSelector=RegionSelector
  },
 }),
 AbstractChangellyExchangeElementClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElement}*/({
  solder:(_,{
   exchangeBrokerSel:exchangeBrokerSel,
   dealBrokerSel:dealBrokerSel,
   exchangeIntentSel:exchangeIntentSel,
   transactionInfoSel:transactionInfoSel,
   regionSelectorSel:regionSelectorSel,
  })=>{
   return{
    exchangeBrokerSel:exchangeBrokerSel,
    dealBrokerSel:dealBrokerSel,
    exchangeIntentSel:exchangeIntentSel,
    transactionInfoSel:transactionInfoSel,
    regionSelectorSel:regionSelectorSel,
   }
  },
 }),
]



AbstractChangellyExchangeElement[$implementations]=[AbstractChangellyExchange,
 /** @type {!AbstractChangellyExchangeElement} */ ({
  rootId:'ChangellyExchange',
  __$id:2013392380,
  fqn:'xyz.swapee.wc.IChangellyExchange',
  maurice_element_v3:true,
 }),
]