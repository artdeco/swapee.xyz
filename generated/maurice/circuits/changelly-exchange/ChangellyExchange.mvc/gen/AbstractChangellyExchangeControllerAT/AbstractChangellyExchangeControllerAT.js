import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeControllerAT}
 */
function __AbstractChangellyExchangeControllerAT() {}
__AbstractChangellyExchangeControllerAT.prototype = /** @type {!_AbstractChangellyExchangeControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT}
 */
class _AbstractChangellyExchangeControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IChangellyExchangeControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT} ‎
 */
class AbstractChangellyExchangeControllerAT extends newAbstract(
 _AbstractChangellyExchangeControllerAT,201339238029,null,{
  asIChangellyExchangeControllerAT:1,
  superChangellyExchangeControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT} */
AbstractChangellyExchangeControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractChangellyExchangeControllerAT} */
function AbstractChangellyExchangeControllerATClass(){}

export default AbstractChangellyExchangeControllerAT


AbstractChangellyExchangeControllerAT[$implementations]=[
 __AbstractChangellyExchangeControllerAT,
 UartUniversal,
 AbstractChangellyExchangeControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IChangellyExchangeControllerAT}*/({
  get asIChangellyExchangeController(){
   return this
  },
  loadGetFixedOffer(){
   this.uart.t("inv",{mid:'eb5ae'})
  },
  loadGetOffer(){
   this.uart.t("inv",{mid:'51084'})
  },
  loadCreateTransaction(){
   this.uart.t("inv",{mid:'8c262'})
  },
  loadGetTransaction(){
   this.uart.t("inv",{mid:'4ff1f'})
  },
  loadCreateFixedTransaction(){
   this.uart.t("inv",{mid:'55209'})
  },
  loadCheckPayment(){
   this.uart.t("inv",{mid:'78fd2'})
  },
 }),
]