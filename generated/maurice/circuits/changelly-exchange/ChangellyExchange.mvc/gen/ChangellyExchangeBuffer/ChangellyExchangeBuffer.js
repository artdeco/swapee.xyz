import {makeBuffers} from '@webcircuits/webcircuits'

export const ChangellyExchangeBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  host:String,
  region:String,
 }),
})

export default ChangellyExchangeBuffer