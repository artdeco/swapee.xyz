import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeScreenAT}
 */
function __AbstractChangellyExchangeScreenAT() {}
__AbstractChangellyExchangeScreenAT.prototype = /** @type {!_AbstractChangellyExchangeScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT}
 */
class _AbstractChangellyExchangeScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IChangellyExchangeScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT} ‎
 */
class AbstractChangellyExchangeScreenAT extends newAbstract(
 _AbstractChangellyExchangeScreenAT,201339238033,null,{
  asIChangellyExchangeScreenAT:1,
  superChangellyExchangeScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT} */
AbstractChangellyExchangeScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreenAT} */
function AbstractChangellyExchangeScreenATClass(){}

export default AbstractChangellyExchangeScreenAT


AbstractChangellyExchangeScreenAT[$implementations]=[
 __AbstractChangellyExchangeScreenAT,
 UartUniversal,
]