import AbstractChangellyExchangeDisplay from '../AbstractChangellyExchangeDisplayBack'
import ChangellyExchangeClassesPQs from '../../pqs/ChangellyExchangeClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ChangellyExchangeClassesQPs} from '../../pqs/ChangellyExchangeClassesQPs'
import {ChangellyExchangeVdusPQs} from '../../pqs/ChangellyExchangeVdusPQs'
import {ChangellyExchangeVdusQPs} from '../../pqs/ChangellyExchangeVdusQPs'
import {ChangellyExchangeMemoryPQs} from '../../pqs/ChangellyExchangeMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeGPU}
 */
function __AbstractChangellyExchangeGPU() {}
__AbstractChangellyExchangeGPU.prototype = /** @type {!_AbstractChangellyExchangeGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeGPU}
 */
class _AbstractChangellyExchangeGPU { }
/**
 * Handles the periphery of the _IChangellyExchangeDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeGPU} ‎
 */
class AbstractChangellyExchangeGPU extends newAbstract(
 _AbstractChangellyExchangeGPU,201339238019,null,{
  asIChangellyExchangeGPU:1,
  superChangellyExchangeGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeGPU} */
AbstractChangellyExchangeGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeGPU} */
function AbstractChangellyExchangeGPUClass(){}

export default AbstractChangellyExchangeGPU


AbstractChangellyExchangeGPU[$implementations]=[
 __AbstractChangellyExchangeGPU,
 AbstractChangellyExchangeGPUClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeGPU}*/({
  classesQPs:ChangellyExchangeClassesQPs,
  vdusPQs:ChangellyExchangeVdusPQs,
  vdusQPs:ChangellyExchangeVdusQPs,
  memoryPQs:ChangellyExchangeMemoryPQs,
 }),
 AbstractChangellyExchangeDisplay,
 BrowserView,
 AbstractChangellyExchangeGPUClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeGPU}*/({
  allocator(){
   pressFit(this.classes,'',ChangellyExchangeClassesPQs)
  },
 }),
]