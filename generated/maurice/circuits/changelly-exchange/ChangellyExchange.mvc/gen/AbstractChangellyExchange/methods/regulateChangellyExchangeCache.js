import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateChangellyExchangeCache=makeBuffers({
 loadingGetFixedOffer:Boolean,
 hasMoreGetFixedOffer:Boolean,
 loadGetFixedOfferError:5,
 loadingGetOffer:Boolean,
 hasMoreGetOffer:Boolean,
 loadGetOfferError:5,
 loadingCreateTransaction:Boolean,
 hasMoreCreateTransaction:Boolean,
 loadCreateTransactionError:5,
 loadingGetTransaction:Boolean,
 hasMoreGetTransaction:Boolean,
 loadGetTransactionError:5,
 loadingCreateFixedTransaction:Boolean,
 hasMoreCreateFixedTransaction:Boolean,
 loadCreateFixedTransactionError:5,
 loadingCheckPayment:Boolean,
 hasMoreCheckPayment:Boolean,
 loadCheckPaymentError:5,
},{silent:true})