import AbstractChangellyExchangeProcessor from '../AbstractChangellyExchangeProcessor'
import {ChangellyExchangeCore} from '../ChangellyExchangeCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractChangellyExchangeComputer} from '../AbstractChangellyExchangeComputer'
import {AbstractChangellyExchangeController} from '../AbstractChangellyExchangeController'
import {regulateChangellyExchangeCache} from './methods/regulateChangellyExchangeCache'
import {ChangellyExchangeCacheQPs} from '../../pqs/ChangellyExchangeCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchange}
 */
function __AbstractChangellyExchange() {}
__AbstractChangellyExchange.prototype = /** @type {!_AbstractChangellyExchange} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchange}
 */
class _AbstractChangellyExchange { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractChangellyExchange} ‎
 */
class AbstractChangellyExchange extends newAbstract(
 _AbstractChangellyExchange,201339238010,null,{
  asIChangellyExchange:1,
  superChangellyExchange:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchange} */
AbstractChangellyExchange.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchange} */
function AbstractChangellyExchangeClass(){}

export default AbstractChangellyExchange


AbstractChangellyExchange[$implementations]=[
 __AbstractChangellyExchange,
 ChangellyExchangeCore,
 AbstractChangellyExchangeProcessor,
 IntegratedComponent,
 AbstractChangellyExchangeComputer,
 AbstractChangellyExchangeController,
 AbstractChangellyExchangeClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchange}*/({
  regulateState:regulateChangellyExchangeCache,
  stateQPs:ChangellyExchangeCacheQPs,
 }),
]


export {AbstractChangellyExchange}