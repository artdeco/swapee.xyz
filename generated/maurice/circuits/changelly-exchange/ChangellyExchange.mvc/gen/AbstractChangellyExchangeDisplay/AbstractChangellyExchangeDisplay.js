import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeDisplay}
 */
function __AbstractChangellyExchangeDisplay() {}
__AbstractChangellyExchangeDisplay.prototype = /** @type {!_AbstractChangellyExchangeDisplay} */ ({ })
/** @this {xyz.swapee.wc.ChangellyExchangeDisplay} */ function ChangellyExchangeDisplayConstructor() {
  /** @type {HTMLPreElement} */ this.Debug=null
  /** @type {HTMLElement} */ this.ExchangeBroker=null
  /** @type {HTMLElement} */ this.DealBroker=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
  /** @type {HTMLElement} */ this.TransactionInfo=null
  /** @type {HTMLElement} */ this.RegionSelector=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeDisplay}
 */
class _AbstractChangellyExchangeDisplay { }
/**
 * Display for presenting information from the _IChangellyExchange_.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeDisplay} ‎
 */
class AbstractChangellyExchangeDisplay extends newAbstract(
 _AbstractChangellyExchangeDisplay,201339238020,ChangellyExchangeDisplayConstructor,{
  asIChangellyExchangeDisplay:1,
  superChangellyExchangeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeDisplay} */
AbstractChangellyExchangeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeDisplay} */
function AbstractChangellyExchangeDisplayClass(){}

export default AbstractChangellyExchangeDisplay


AbstractChangellyExchangeDisplay[$implementations]=[
 __AbstractChangellyExchangeDisplay,
 Display,
 AbstractChangellyExchangeDisplayClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{exchangeBrokerScopeSel:exchangeBrokerScopeSel,dealBrokerScopeSel:dealBrokerScopeSel,exchangeIntentScopeSel:exchangeIntentScopeSel,transactionInfoScopeSel:transactionInfoScopeSel,regionSelectorScopeSel:regionSelectorScopeSel}}=this
    this.scan({
     exchangeBrokerSel:exchangeBrokerScopeSel,
     dealBrokerSel:dealBrokerScopeSel,
     exchangeIntentSel:exchangeIntentScopeSel,
     transactionInfoSel:transactionInfoScopeSel,
     regionSelectorSel:regionSelectorScopeSel,
    })
   })
  },
  scan:function vduScan({exchangeBrokerSel:exchangeBrokerSelScope,dealBrokerSel:dealBrokerSelScope,exchangeIntentSel:exchangeIntentSelScope,transactionInfoSel:transactionInfoSelScope,regionSelectorSel:regionSelectorSelScope}){
   const{element:element,asIChangellyExchangeScreen:{vdusPQs:{
    Debug:Debug,
   }},queries:{exchangeBrokerSel,dealBrokerSel,exchangeIntentSel,transactionInfoSel,regionSelectorSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const ExchangeBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeBroker=exchangeBrokerSel?root.querySelector(exchangeBrokerSel):void 0
    return _ExchangeBroker
   })(exchangeBrokerSelScope)
   /**@type {HTMLElement}*/
   const DealBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _DealBroker=dealBrokerSel?root.querySelector(dealBrokerSel):void 0
    return _DealBroker
   })(dealBrokerSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   /**@type {HTMLElement}*/
   const TransactionInfo=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _TransactionInfo=transactionInfoSel?root.querySelector(transactionInfoSel):void 0
    return _TransactionInfo
   })(transactionInfoSelScope)
   /**@type {HTMLElement}*/
   const RegionSelector=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _RegionSelector=regionSelectorSel?root.querySelector(regionSelectorSel):void 0
    return _RegionSelector
   })(regionSelectorSelScope)
   Object.assign(this,{
    Debug:/**@type {HTMLPreElement}*/(children[Debug]),
    ExchangeBroker:ExchangeBroker,
    DealBroker:DealBroker,
    ExchangeIntent:ExchangeIntent,
    TransactionInfo:TransactionInfo,
    RegionSelector:RegionSelector,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ChangellyExchangeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IChangellyExchangeDisplay.Initialese}*/({
   Debug:1,
   ExchangeBroker:1,
   DealBroker:1,
   ExchangeIntent:1,
   TransactionInfo:1,
   RegionSelector:1,
  }),
  initializer({
   Debug:_Debug,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
   TransactionInfo:_TransactionInfo,
   RegionSelector:_RegionSelector,
  }) {
   if(_Debug!==undefined) this.Debug=_Debug
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_RegionSelector!==undefined) this.RegionSelector=_RegionSelector
  },
 }),
]