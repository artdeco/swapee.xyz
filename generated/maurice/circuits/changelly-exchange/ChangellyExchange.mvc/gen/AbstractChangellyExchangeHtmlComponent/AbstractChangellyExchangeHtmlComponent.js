import AbstractChangellyExchangeGPU from '../AbstractChangellyExchangeGPU'
import AbstractChangellyExchangeScreenBack from '../AbstractChangellyExchangeScreenBack'
import AbstractChangellyExchangeRadio from '../AbstractChangellyExchangeRadio'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {ChangellyExchangeInputsQPs} from '../../pqs/ChangellyExchangeInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractChangellyExchange from '../AbstractChangellyExchange'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeHtmlComponent}
 */
function __AbstractChangellyExchangeHtmlComponent() {}
__AbstractChangellyExchangeHtmlComponent.prototype = /** @type {!_AbstractChangellyExchangeHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent}
 */
class _AbstractChangellyExchangeHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ChangellyExchangeHtmlComponent} */ (res)
  }
}
/**
 * The _IChangellyExchange_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent} ‎
 */
export class AbstractChangellyExchangeHtmlComponent extends newAbstract(
 _AbstractChangellyExchangeHtmlComponent,201339238013,null,{
  asIChangellyExchangeHtmlComponent:1,
  superChangellyExchangeHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent} */
AbstractChangellyExchangeHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeHtmlComponent} */
function AbstractChangellyExchangeHtmlComponentClass(){}


AbstractChangellyExchangeHtmlComponent[$implementations]=[
 __AbstractChangellyExchangeHtmlComponent,
 HtmlComponent,
 AbstractChangellyExchange,
 AbstractChangellyExchangeGPU,
 AbstractChangellyExchangeScreenBack,
 AbstractChangellyExchangeRadio,
 Landed,
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  constructor(){
   this.land={
    ExchangeBroker:null,
    DealBroker:null,
    ExchangeIntent:null,
    TransactionInfo:null,
    RegionSelector:null,
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  inputsQPs:ChangellyExchangeInputsQPs,
 }),

/** @type {!AbstractChangellyExchangeHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/function paintDebug() {
   this.Debug.setText(this.model.region)
  }
 ] }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIChangellyExchangeGPU:{
     ExchangeBroker:ExchangeBroker,
     DealBroker:DealBroker,
     ExchangeIntent:ExchangeIntent,
     TransactionInfo:TransactionInfo,
     RegionSelector:RegionSelector,
    },
   }=this
   complete(1683059665,{ExchangeBroker:ExchangeBroker},{
    loadingCheckPayment:'adfbe', // -> checkingStatus
    loadCheckPaymentError:['64a41','afbad'], // -> checkStatusError,checkPaymentError
   })
   complete(3427226314,{DealBroker:DealBroker},{
    /**@this {xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/
    get['b370a'](){ // -> getOffer
     const{land:{ExchangeIntent:_ExchangeIntent}}=this
     if(!_ExchangeIntent) return void 0
     const ready=_ExchangeIntent.model['b2fda'] // <- ready
     return ready
    },
   })
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
   complete(1988051819,{TransactionInfo:TransactionInfo},{
    loadingGetTransaction:'7c389', // -> gettingTransaction
    loadGetTransactionError:'43e5c', // -> getTransactionError
   })
   complete(9509928906,{RegionSelector:RegionSelector})
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint(_,{TransactionInfo:TransactionInfo}){
   if(!TransactionInfo) return
   const{asIChangellyExchangeController:{
    loadGetTransaction:loadGetTransaction,
   }}=this
   const getTransaction=TransactionInfo.model['5a67c']
   if(getTransaction) {
    loadGetTransaction()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const currencyFrom=ExchangeIntent.model['96c88']
   if(currencyFrom) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const currencyTo=ExchangeIntent.model['c23cd']
   if(currencyTo) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const amountFrom=ExchangeIntent.model['748e6']
   if(amountFrom) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint(_,{ExchangeIntent:ExchangeIntent,DealBroker:DealBroker}){
   if(!ExchangeIntent||!DealBroker) return
   const fixed=ExchangeIntent.model['cec31']
   if(fixed) {
    DealBroker['e0519']() // pulseGetOffer()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint({loadingCreateTransaction:loadingCreateTransaction},{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   if(loadingCreateTransaction) {
    ExchangeBroker['ed2de']() // unsetCreateTransactionError()
   }
  },
 }),
 AbstractChangellyExchangeHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeHtmlComponent}*/({
  paint({loadingCheckPayment:loadingCheckPayment},{ExchangeBroker:ExchangeBroker}){
   if(!ExchangeBroker) return
   if(loadingCheckPayment) {
    ExchangeBroker['d553e']() // unsetCheckPaymentError()
   }
  },
 }),
]