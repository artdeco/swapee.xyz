import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeScreenAR}
 */
function __AbstractChangellyExchangeScreenAR() {}
__AbstractChangellyExchangeScreenAR.prototype = /** @type {!_AbstractChangellyExchangeScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR}
 */
class _AbstractChangellyExchangeScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IChangellyExchangeScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR} ‎
 */
class AbstractChangellyExchangeScreenAR extends newAbstract(
 _AbstractChangellyExchangeScreenAR,201339238032,null,{
  asIChangellyExchangeScreenAR:1,
  superChangellyExchangeScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR} */
AbstractChangellyExchangeScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractChangellyExchangeScreenAR} */
function AbstractChangellyExchangeScreenARClass(){}

export default AbstractChangellyExchangeScreenAR


AbstractChangellyExchangeScreenAR[$implementations]=[
 __AbstractChangellyExchangeScreenAR,
 AR,
 AbstractChangellyExchangeScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IChangellyExchangeScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractChangellyExchangeScreenAR}