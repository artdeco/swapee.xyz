import {preadaptLoadGetFixedOffer,preadaptLoadGetOffer,preadaptLoadCreateTransaction,preadaptLoadGetTransaction,preadaptLoadCreateFixedTransaction,preadaptLoadCheckPayment} from '../AbstractChangellyExchangeRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeProcessor}
 */
function __AbstractChangellyExchangeProcessor() {}
__AbstractChangellyExchangeProcessor.prototype = /** @type {!_AbstractChangellyExchangeProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeProcessor}
 */
class _AbstractChangellyExchangeProcessor { }
/**
 * The processor to compute changes to the memory for the _IChangellyExchange_.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeProcessor} ‎
 */
class AbstractChangellyExchangeProcessor extends newAbstract(
 _AbstractChangellyExchangeProcessor,20133923809,null,{
  asIChangellyExchangeProcessor:1,
  superChangellyExchangeProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeProcessor} */
AbstractChangellyExchangeProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeProcessor} */
function AbstractChangellyExchangeProcessorClass(){}

export default AbstractChangellyExchangeProcessor


AbstractChangellyExchangeProcessor[$implementations]=[
 __AbstractChangellyExchangeProcessor,
 AbstractChangellyExchangeProcessorClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeProcessor}*/({
  loadGetFixedOffer(){
   return preadaptLoadGetFixedOffer.call(this,this.model).then(this.setInfo)
  },
  loadGetOffer(){
   return preadaptLoadGetOffer.call(this,this.model).then(this.setInfo)
  },
  loadCreateTransaction(){
   return preadaptLoadCreateTransaction.call(this,this.model).then(this.setInfo)
  },
  loadGetTransaction(){
   return preadaptLoadGetTransaction.call(this,this.model).then(this.setInfo)
  },
  loadCreateFixedTransaction(){
   return preadaptLoadCreateFixedTransaction.call(this,this.model).then(this.setInfo)
  },
  loadCheckPayment(){
   return preadaptLoadCheckPayment.call(this,this.model).then(this.setInfo)
  },
 }),
]