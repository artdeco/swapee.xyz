import ChangellyExchangeClassesPQs from '../../pqs/ChangellyExchangeClassesPQs'
import AbstractChangellyExchangeScreenAR from '../AbstractChangellyExchangeScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ChangellyExchangeInputsPQs} from '../../pqs/ChangellyExchangeInputsPQs'
import {ChangellyExchangeQueriesPQs} from '../../pqs/ChangellyExchangeQueriesPQs'
import {ChangellyExchangeMemoryQPs} from '../../pqs/ChangellyExchangeMemoryQPs'
import {ChangellyExchangeCacheQPs} from '../../pqs/ChangellyExchangeCacheQPs'
import {ChangellyExchangeVdusPQs} from '../../pqs/ChangellyExchangeVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeScreen}
 */
function __AbstractChangellyExchangeScreen() {}
__AbstractChangellyExchangeScreen.prototype = /** @type {!_AbstractChangellyExchangeScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeScreen}
 */
class _AbstractChangellyExchangeScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeScreen} ‎
 */
class AbstractChangellyExchangeScreen extends newAbstract(
 _AbstractChangellyExchangeScreen,201339238030,null,{
  asIChangellyExchangeScreen:1,
  superChangellyExchangeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeScreen} */
AbstractChangellyExchangeScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeScreen} */
function AbstractChangellyExchangeScreenClass(){}

export default AbstractChangellyExchangeScreen


AbstractChangellyExchangeScreen[$implementations]=[
 __AbstractChangellyExchangeScreen,
 AbstractChangellyExchangeScreenClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeScreen}*/({
  deduceInputs(){
   const{asIChangellyExchangeDisplay:{
    Debug:Debug,
   }}=this
   return{
    region:Debug?.innerText,
   }
  },
 }),
 AbstractChangellyExchangeScreenClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeScreen}*/({
  inputsPQs:ChangellyExchangeInputsPQs,
  classesPQs:ChangellyExchangeClassesPQs,
  queriesPQs:ChangellyExchangeQueriesPQs,
  memoryQPs:ChangellyExchangeMemoryQPs,
  cacheQPs:ChangellyExchangeCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractChangellyExchangeScreenAR,
 AbstractChangellyExchangeScreenClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeScreen}*/({
  vdusPQs:ChangellyExchangeVdusPQs,
 }),
]