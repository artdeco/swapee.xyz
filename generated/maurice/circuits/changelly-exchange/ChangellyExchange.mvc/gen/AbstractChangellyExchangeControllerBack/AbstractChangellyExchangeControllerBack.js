import AbstractChangellyExchangeControllerAR from '../AbstractChangellyExchangeControllerAR'
import {AbstractChangellyExchangeController} from '../AbstractChangellyExchangeController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeControllerBack}
 */
function __AbstractChangellyExchangeControllerBack() {}
__AbstractChangellyExchangeControllerBack.prototype = /** @type {!_AbstractChangellyExchangeControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeController}
 */
class _AbstractChangellyExchangeControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeController} ‎
 */
class AbstractChangellyExchangeControllerBack extends newAbstract(
 _AbstractChangellyExchangeControllerBack,201339238027,null,{
  asIChangellyExchangeController:1,
  superChangellyExchangeController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeController} */
AbstractChangellyExchangeControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeController} */
function AbstractChangellyExchangeControllerBackClass(){}

export default AbstractChangellyExchangeControllerBack


AbstractChangellyExchangeControllerBack[$implementations]=[
 __AbstractChangellyExchangeControllerBack,
 AbstractChangellyExchangeController,
 AbstractChangellyExchangeControllerAR,
 DriverBack,
]