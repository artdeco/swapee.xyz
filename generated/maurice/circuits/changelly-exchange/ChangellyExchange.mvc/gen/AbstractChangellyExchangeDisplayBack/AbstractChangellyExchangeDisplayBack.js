import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeDisplay}
 */
function __AbstractChangellyExchangeDisplay() {}
__AbstractChangellyExchangeDisplay.prototype = /** @type {!_AbstractChangellyExchangeDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeDisplay}
 */
class _AbstractChangellyExchangeDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeDisplay} ‎
 */
class AbstractChangellyExchangeDisplay extends newAbstract(
 _AbstractChangellyExchangeDisplay,201339238023,null,{
  asIChangellyExchangeDisplay:1,
  superChangellyExchangeDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeDisplay} */
AbstractChangellyExchangeDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeDisplay} */
function AbstractChangellyExchangeDisplayClass(){}

export default AbstractChangellyExchangeDisplay


AbstractChangellyExchangeDisplay[$implementations]=[
 __AbstractChangellyExchangeDisplay,
 GraphicsDriverBack,
 AbstractChangellyExchangeDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IChangellyExchangeDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IChangellyExchangeDisplay}*/({
    Debug:twinMock,
    ExchangeBroker:twinMock,
    DealBroker:twinMock,
    ExchangeIntent:twinMock,
    TransactionInfo:twinMock,
    RegionSelector:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ChangellyExchangeDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IChangellyExchangeDisplay.Initialese}*/({
   Debug:1,
   ExchangeBroker:1,
   DealBroker:1,
   ExchangeIntent:1,
   TransactionInfo:1,
   RegionSelector:1,
  }),
  initializer({
   Debug:_Debug,
   ExchangeBroker:_ExchangeBroker,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
   TransactionInfo:_TransactionInfo,
   RegionSelector:_RegionSelector,
  }) {
   if(_Debug!==undefined) this.Debug=_Debug
   if(_ExchangeBroker!==undefined) this.ExchangeBroker=_ExchangeBroker
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
   if(_TransactionInfo!==undefined) this.TransactionInfo=_TransactionInfo
   if(_RegionSelector!==undefined) this.RegionSelector=_RegionSelector
  },
 }),
]