import {preadaptLoadGetFixedOffer,preadaptLoadGetOffer,preadaptLoadCreateTransaction,preadaptLoadGetTransaction,preadaptLoadCreateFixedTransaction,preadaptLoadCheckPayment} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations, create } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeRadio}
 */
function __AbstractChangellyExchangeRadio() {}
__AbstractChangellyExchangeRadio.prototype = /** @type {!_AbstractChangellyExchangeRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeRadio}
 */
class _AbstractChangellyExchangeRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeRadio} ‎
 */
class AbstractChangellyExchangeRadio extends newAbstract(
 _AbstractChangellyExchangeRadio,201339238016,null,{
  asIChangellyExchangeRadio:1,
  superChangellyExchangeRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeRadio} */
AbstractChangellyExchangeRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeRadio} */
function AbstractChangellyExchangeRadioClass(){}

export default AbstractChangellyExchangeRadio


AbstractChangellyExchangeRadio[$implementations]=[
 __AbstractChangellyExchangeRadio,
 makeLoaders(2013392380,{
  adaptLoadGetFixedOffer:[
   '22dec',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
    fixed:'cec31',
    ready:'b2fda',
   },
   {
    rate:'67942',
    fixedId:'2568a',
    estimatedFixedAmountTo:'ea0a0',
    minAmount:'ae0d3',
    maxAmount:'d0b0c',
   },
   {
    loadingGetFixedOffer:1,
    loadGetFixedOfferError:2,
   },
  ],
  adaptLoadGetOffer:[
   'd8a4f',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
    fixed:'cec31',
    ready:'b2fda',
   },
   {
    rate:'67942',
    estimatedFloatAmountTo:'8ca05',
    networkFee:'5fd9c',
    partnerFee:'96f44',
    visibleAmount:'4685c',
    minAmount:'ae0d3',
    maxAmount:'d0b0c',
   },
   {
    loadingGetOffer:1,
    loadGetOfferError:2,
   },
  ],
  adaptLoadCreateTransaction:[
   '321e2',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
    fixed:'cec31',
    address:'884d9',
    refundAddress:'be03a',
   },
   {
    id:'b80bb',
    createTransactionError:'601f8',
    createdAt:'97def',
    payinAddress:'59af8',
    payinExtraId:'7a19b',
    status:'9acb4',
    kycRequired:'2b803',
    confirmedAmountFrom:'6103f',
   },
   {
    loadingCreateTransaction:1,
    loadCreateTransactionError:2,
   },
  ],
  adaptLoadGetTransaction:[
   '6a2c5',
   {
    tid:'97bea',
   },
   {
    fixed:'cec31',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
    amountFrom:'748e6',
    amountTo:'9dc9f',
    networkFee:'5fd9c',
    partnerFee:'96f44',
    visibleAmount:'4685c',
    notFound:'5ccf4',
    rate:'67942',
    id:'b80bb',
    createdAt:'97def',
    payinAddress:'59af8',
    payinExtraId:'7a19b',
    status:'9acb4',
    confirmedAmountFrom:'6103f',
   },
   {
    loadingGetTransaction:1,
    loadGetTransactionError:2,
   },
  ],
  adaptLoadCreateFixedTransaction:[
   '1344b',
   {
    amountFrom:'748e6',
    currencyFrom:'96c88',
    currencyTo:'c23cd',
    fixed:'cec31',
    address:'884d9',
    refundAddress:'be03a',
    fixedId:'2568a',
   },
   {
    getOffer:'b370a',
    id:'b80bb',
    createTransactionError:'601f8',
    createdAt:'97def',
    payinAddress:'59af8',
    payinExtraId:'7a19b',
    status:'9acb4',
    kycRequired:'2b803',
    confirmedAmountFrom:'6103f',
   },
   {
    loadingCreateFixedTransaction:1,
    loadCreateFixedTransactionError:2,
   },
  ],
  adaptLoadCheckPayment:[
   'c3eae',
   {
    id:'b80bb',
   },
   {
    status:'9acb4',
    checkPaymentError:'afbad',
   },
   {
    loadingCheckPayment:1,
    loadCheckPaymentError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadGetFixedOffer,preadaptLoadGetOffer,preadaptLoadCreateTransaction,preadaptLoadGetTransaction,preadaptLoadCreateFixedTransaction,preadaptLoadCheckPayment]},
]