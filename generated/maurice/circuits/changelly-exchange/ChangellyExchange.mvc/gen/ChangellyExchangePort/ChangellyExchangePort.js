import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {ChangellyExchangeInputsPQs} from '../../pqs/ChangellyExchangeInputsPQs'
import {ChangellyExchangeOuterCoreConstructor} from '../ChangellyExchangeCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangePort}
 */
function __ChangellyExchangePort() {}
__ChangellyExchangePort.prototype = /** @type {!_ChangellyExchangePort} */ ({ })
/** @this {xyz.swapee.wc.ChangellyExchangePort} */ function ChangellyExchangePortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ChangellyExchangeOuterCore} */ ({model:null})
  ChangellyExchangeOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangePort}
 */
class _ChangellyExchangePort { }
/**
 * The port that serves as an interface to the _IChangellyExchange_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangePort} ‎
 */
export class ChangellyExchangePort extends newAbstract(
 _ChangellyExchangePort,20133923805,ChangellyExchangePortConstructor,{
  asIChangellyExchangePort:1,
  superChangellyExchangePort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangePort} */
ChangellyExchangePort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangePort} */
function ChangellyExchangePortClass(){}

export const ChangellyExchangePortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IChangellyExchange.Pinout>}*/({
 get Port() { return ChangellyExchangePort },
})

ChangellyExchangePort[$implementations]=[
 ChangellyExchangePortClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangePort}*/({
  resetPort(){
   this.resetChangellyExchangePort()
  },
  resetChangellyExchangePort(){
   ChangellyExchangePortConstructor.call(this)
  },
 }),
 __ChangellyExchangePort,
 Parametric,
 ChangellyExchangePortClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangePort}*/({
  constructor(){
   mountPins(this.inputs,ChangellyExchangeInputsPQs)
  },
 }),
]


export default ChangellyExchangePort