import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangeElementPort}
 */
function __ChangellyExchangeElementPort() {}
__ChangellyExchangeElementPort.prototype = /** @type {!_ChangellyExchangeElementPort} */ ({ })
/** @this {xyz.swapee.wc.ChangellyExchangeElementPort} */ function ChangellyExchangeElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IChangellyExchangeElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    debugOpts: {},
    exchangeBrokerOpts: {},
    dealBrokerOpts: {},
    exchangeIntentOpts: {},
    transactionInfoOpts: {},
    regionSelectorOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeElementPort}
 */
class _ChangellyExchangeElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeElementPort} ‎
 */
class ChangellyExchangeElementPort extends newAbstract(
 _ChangellyExchangeElementPort,201339238015,ChangellyExchangeElementPortConstructor,{
  asIChangellyExchangeElementPort:1,
  superChangellyExchangeElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeElementPort} */
ChangellyExchangeElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeElementPort} */
function ChangellyExchangeElementPortClass(){}

export default ChangellyExchangeElementPort


ChangellyExchangeElementPort[$implementations]=[
 __ChangellyExchangeElementPort,
 ChangellyExchangeElementPortClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'debug-opts':undefined,
    'exchange-broker-opts':undefined,
    'deal-broker-opts':undefined,
    'exchange-intent-opts':undefined,
    'transaction-info-opts':undefined,
    'region-selector-opts':undefined,
   })
  },
 }),
]