import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeControllerAR}
 */
function __AbstractChangellyExchangeControllerAR() {}
__AbstractChangellyExchangeControllerAR.prototype = /** @type {!_AbstractChangellyExchangeControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR}
 */
class _AbstractChangellyExchangeControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IChangellyExchangeControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR} ‎
 */
class AbstractChangellyExchangeControllerAR extends newAbstract(
 _AbstractChangellyExchangeControllerAR,201339238028,null,{
  asIChangellyExchangeControllerAR:1,
  superChangellyExchangeControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR} */
AbstractChangellyExchangeControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeControllerAR} */
function AbstractChangellyExchangeControllerARClass(){}

export default AbstractChangellyExchangeControllerAR


AbstractChangellyExchangeControllerAR[$implementations]=[
 __AbstractChangellyExchangeControllerAR,
 AR,
 AbstractChangellyExchangeControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IChangellyExchangeControllerAR}*/({
  allocator(){
   this.methods={
    loadGetFixedOffer:'eb5ae',
    loadGetOffer:'51084',
    loadCreateTransaction:'8c262',
    loadGetTransaction:'4ff1f',
    loadCreateFixedTransaction:'55209',
    loadCheckPayment:'78fd2',
   }
  },
 }),
]