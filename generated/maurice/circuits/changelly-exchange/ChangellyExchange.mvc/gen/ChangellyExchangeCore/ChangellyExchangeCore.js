import {mountPins} from '@type.engineering/seers'
import {ChangellyExchangeMemoryPQs} from '../../pqs/ChangellyExchangeMemoryPQs'
import {ChangellyExchangeCachePQs} from '../../pqs/ChangellyExchangeCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangeCore}
 */
function __ChangellyExchangeCore() {}
__ChangellyExchangeCore.prototype = /** @type {!_ChangellyExchangeCore} */ ({ })
/** @this {xyz.swapee.wc.ChangellyExchangeCore} */ function ChangellyExchangeCoreConstructor() {
  /**@type {!xyz.swapee.wc.IChangellyExchangeCore.Model}*/
  this.model={
    loadingGetFixedOffer: false,
    hasMoreGetFixedOffer: null,
    loadGetFixedOfferError: null,
    loadingGetOffer: false,
    hasMoreGetOffer: null,
    loadGetOfferError: null,
    loadingCreateTransaction: false,
    hasMoreCreateTransaction: null,
    loadCreateTransactionError: null,
    loadingGetTransaction: false,
    hasMoreGetTransaction: null,
    loadGetTransactionError: null,
    loadingCreateFixedTransaction: false,
    hasMoreCreateFixedTransaction: null,
    loadCreateFixedTransactionError: null,
    loadingCheckPayment: false,
    hasMoreCheckPayment: null,
    loadCheckPaymentError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeCore}
 */
class _ChangellyExchangeCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeCore} ‎
 */
class ChangellyExchangeCore extends newAbstract(
 _ChangellyExchangeCore,20133923808,ChangellyExchangeCoreConstructor,{
  asIChangellyExchangeCore:1,
  superChangellyExchangeCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeCore} */
ChangellyExchangeCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeCore} */
function ChangellyExchangeCoreClass(){}

export default ChangellyExchangeCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ChangellyExchangeOuterCore}
 */
function __ChangellyExchangeOuterCore() {}
__ChangellyExchangeOuterCore.prototype = /** @type {!_ChangellyExchangeOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ChangellyExchangeOuterCore} */
export function ChangellyExchangeOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IChangellyExchangeOuterCore.Model}*/
  this.model={
    host: '',
    region: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeOuterCore}
 */
class _ChangellyExchangeOuterCore { }
/**
 * The _IChangellyExchange_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractChangellyExchangeOuterCore} ‎
 */
export class ChangellyExchangeOuterCore extends newAbstract(
 _ChangellyExchangeOuterCore,20133923803,ChangellyExchangeOuterCoreConstructor,{
  asIChangellyExchangeOuterCore:1,
  superChangellyExchangeOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeOuterCore} */
ChangellyExchangeOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractChangellyExchangeOuterCore} */
function ChangellyExchangeOuterCoreClass(){}


ChangellyExchangeOuterCore[$implementations]=[
 __ChangellyExchangeOuterCore,
 ChangellyExchangeOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeOuterCore}*/({
  constructor(){
   mountPins(this.model,ChangellyExchangeMemoryPQs)
   mountPins(this.model,ChangellyExchangeCachePQs)
  },
 }),
]

ChangellyExchangeCore[$implementations]=[
 ChangellyExchangeCoreClass.prototype=/**@type {!xyz.swapee.wc.IChangellyExchangeCore}*/({
  resetCore(){
   this.resetChangellyExchangeCore()
  },
  resetChangellyExchangeCore(){
   ChangellyExchangeCoreConstructor.call(
    /**@type {xyz.swapee.wc.ChangellyExchangeCore}*/(this),
   )
   ChangellyExchangeOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ChangellyExchangeOuterCore}*/(
     /**@type {!xyz.swapee.wc.IChangellyExchangeOuterCore}*/(this)),
   )
  },
 }),
 __ChangellyExchangeCore,
 ChangellyExchangeOuterCore,
]

export {ChangellyExchangeCore}