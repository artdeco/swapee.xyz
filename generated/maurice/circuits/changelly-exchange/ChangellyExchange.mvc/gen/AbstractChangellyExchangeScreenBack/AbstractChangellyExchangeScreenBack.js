import AbstractChangellyExchangeScreenAT from '../AbstractChangellyExchangeScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractChangellyExchangeScreenBack}
 */
function __AbstractChangellyExchangeScreenBack() {}
__AbstractChangellyExchangeScreenBack.prototype = /** @type {!_AbstractChangellyExchangeScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeScreen}
 */
class _AbstractChangellyExchangeScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractChangellyExchangeScreen} ‎
 */
class AbstractChangellyExchangeScreenBack extends newAbstract(
 _AbstractChangellyExchangeScreenBack,201339238031,null,{
  asIChangellyExchangeScreen:1,
  superChangellyExchangeScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreen} */
AbstractChangellyExchangeScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractChangellyExchangeScreen} */
function AbstractChangellyExchangeScreenBackClass(){}

export default AbstractChangellyExchangeScreenBack


AbstractChangellyExchangeScreenBack[$implementations]=[
 __AbstractChangellyExchangeScreenBack,
 AbstractChangellyExchangeScreenAT,
]