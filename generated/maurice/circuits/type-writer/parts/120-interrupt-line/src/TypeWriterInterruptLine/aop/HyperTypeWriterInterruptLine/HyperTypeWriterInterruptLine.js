import AbstractHyperTypeWriterInterruptLine from '../../../../gen/AbstractTypeWriterInterruptLine/hyper/AbstractHyperTypeWriterInterruptLine'
import TypeWriterInterruptLine from '../../TypeWriterInterruptLine'
import TypeWriterInterruptLineGeneralAspects from '../TypeWriterInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperTypeWriterInterruptLine} */
export default class extends AbstractHyperTypeWriterInterruptLine
 .consults(
  TypeWriterInterruptLineGeneralAspects,
 )
 .implements(
  TypeWriterInterruptLine,
 )
{}