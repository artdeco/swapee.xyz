/**@type {xyz.swapee.wc.ITypeWriterDisplay._paint}*/
export default
function paint({
 nextPhrase,enterDelay,eraseDelay,hold,holdClear:holdClear,
}) {
 const{
  asITypeWriterDisplay:{element:element},
  asITypeWriterScreen:{next:next},
 }=this
 // const symbols=next.split('')
 const currentPhrase=element.innerText
 let i=currentPhrase.length-1
 const int=setInterval(()=>{
  if(i==-1) {
   clearInterval(int)
   setTimeout(()=>{
    const j=nextPhrase.length
    i=0
    const j2=setInterval(()=>{
     if(i<j){
      element.innerText+=nextPhrase[i]
      i++
     }else{
      clearInterval(j2)
      setTimeout(()=>{
       next()
      },hold)
     }
    },enterDelay)
   },holdClear)
  }else{
   element.innerText=element.innerText.slice(0,i)
   i--
  }
 },eraseDelay)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvdHlwZS13cml0ZXIvdHlwZS13cml0ZXIud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTJFRyxTQUFTLEtBQUs7Q0FDYixVQUFVLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CO0FBQzFELENBQUM7Q0FDQTtFQUNDLHNCQUFzQixlQUFlO0VBQ3JDLHFCQUFxQixTQUFTO0dBQzdCO0NBQ0YsR0FBRyxNQUFNLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0NBQzVCLE1BQU0sYUFBYSxDQUFDLE9BQU8sQ0FBQztDQUM1QixJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUM7Q0FDcEIsTUFBTSxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7RUFDdEIsRUFBRSxDQUFDLENBQUMsRUFBRTtHQUNMLGFBQWEsQ0FBQztHQUNkLFVBQVUsQ0FBQyxDQUFDO0lBQ1gsTUFBTSxDQUFDLENBQUMsVUFBVSxDQUFDO0lBQ25CLENBQUMsQ0FBQztJQUNGLE1BQU0sRUFBRSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ3JCLEVBQUUsQ0FBQztNQUNGLE9BQU8sQ0FBQyxVQUFVLENBQUM7TUFDbkI7QUFDTixNQUFNO01BQ0EsYUFBYSxDQUFDO01BQ2QsVUFBVSxDQUFDLENBQUM7T0FDWCxJQUFJLENBQUM7QUFDWixRQUFRO0FBQ1I7QUFDQSxNQUFNO0FBQ04sS0FBSztBQUNMLEdBQUc7R0FDQSxPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztHQUM1QztBQUNIO0FBQ0EsR0FBRztBQUNILENBQUYifQ==