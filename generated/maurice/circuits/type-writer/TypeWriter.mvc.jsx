/** @extends {xyz.swapee.wc.AbstractTypeWriter} */
export default class AbstractTypeWriter extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractTypeWriterComputer} */
export class AbstractTypeWriterComputer extends (<computer>
   <adapter name="adaptNextPhrase">
    <xyz.swapee.wc.ITypeWriterCore nextIndex phrases />
    <outputs>
     <xyz.swapee.wc.ITypeWriterCore nextPhrase />
    </outputs>
   </adapter>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTypeWriterController} */
export class AbstractTypeWriterController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTypeWriterPort} */
export class TypeWriterPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractTypeWriterView} */
export class AbstractTypeWriterView extends (<view>
  <classes>

  </classes>

  <children>

  </children>
</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTypeWriterElement} */
export class AbstractTypeWriterElement extends (<element v3 html mv>
 <block src="./TypeWriter.mvc/src/TypeWriterElement/methods/render.jsx" />
 <inducer src="./TypeWriter.mvc/src/TypeWriterElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractTypeWriterHtmlComponent} */
export class AbstractTypeWriterHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>