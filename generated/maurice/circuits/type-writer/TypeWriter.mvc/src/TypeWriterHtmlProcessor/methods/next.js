/** @type {xyz.swapee.wc.ITypeWriterProcessor._next} */
export default function next(){
 const{
  asITypeWriterController:{setInputs:setInputs},
  asITypeWriterCore:{model:{phrases:phrases,nextIndex:nextIndex}},
 }=/**@type {!xyz.swapee.wc.ITypeWriterProcessor}*/(this)
 let i=(nextIndex+1)%phrases.length
 setInputs({nextIndex:i})
}