import next from './methods/next'
import AbstractTypeWriterProcessor from '../../gen/AbstractTypeWriterProcessor'

/** @extends {xyz.swapee.wc.TypeWriterProcessor} */
export default class extends AbstractTypeWriterProcessor.implements(
 /** @type {!xyz.swapee.wc.ITypeWriterProcessor} */ ({
  next:next,
 }),
){}