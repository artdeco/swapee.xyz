import paint from './methods/paint'
import AbstractTypeWriterDisplay from '../../gen/AbstractTypeWriterDisplay'

/** @extends {xyz.swapee.wc.TypeWriterDisplay} */
export default class extends AbstractTypeWriterDisplay.implements(
 /** @type {!xyz.swapee.wc.ITypeWriterDisplay} */ ({
  paint:paint,
 }),
){}