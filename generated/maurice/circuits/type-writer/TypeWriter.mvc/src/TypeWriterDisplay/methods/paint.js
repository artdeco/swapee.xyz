/** @type {!xyz.swapee.wc.ITypeWriterDisplay._paint} */
export default function paint({
 nextPhrase,enterDelay,eraseDelay,hold,holdClear:holdClear,
}) {
 const{
  asITypeWriterDisplay:{element:element},
  asITypeWriterScreen:{next:next},
 }=/**@type {!xyz.swapee.wc.ITypeWriterDisplay}*/(this)
 // const symbols=next.split('')
 const currentPhrase=element.innerText
 let i=currentPhrase.length-1
 const int=setInterval(()=>{
  if(i==-1) {
   clearInterval(int)
   setTimeout(()=>{
    const j=nextPhrase.length
    i=0
    const j2=setInterval(()=>{
     if(i<j){
      element.innerText+=nextPhrase[i]
      i++
     }else{
      clearInterval(j2)
      setTimeout(()=>{
       next()
      },hold)
     }
    },enterDelay)
   },holdClear)
  }else{
   element.innerText=element.innerText.slice(0,i)
   i--
  }
 },eraseDelay)
}
paint['_id']='7d382a8'