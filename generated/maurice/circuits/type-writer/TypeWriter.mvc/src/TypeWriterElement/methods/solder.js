/** @type {xyz.swapee.wc.ITypeWriterElement._solder} */
export default function solder({
 phrases:phrases,hold:hold,enterDelay:enterDelay,eraseDelay:eraseDelay,
 holdClear:holdClear,
}) {
 return{
  phrases:phrases.join(','),
  hold:hold,
  holdClear:holdClear,
  enterDelay:enterDelay,
  eraseDelay:eraseDelay,
 }
}