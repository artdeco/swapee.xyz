import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import TypeWriterServerController from '../TypeWriterServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractTypeWriterElement from '../../gen/AbstractTypeWriterElement'

/** @extends {xyz.swapee.wc.TypeWriterElement} */
export default class TypeWriterElement extends AbstractTypeWriterElement.implements(
 TypeWriterServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.ITypeWriterElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
   classesMap: true, //
   rootSelector:     `.TypeWriter`,
   stylesheet:       'html/styles/TypeWriter.css',
   blockName:        'html/TypeWriterBlock.html',
  }),
){}

// thank you for using web circuits
