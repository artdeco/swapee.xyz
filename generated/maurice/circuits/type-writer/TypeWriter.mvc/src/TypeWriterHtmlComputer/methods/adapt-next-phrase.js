/** @type {xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase} */
export default function adaptNextPhrase({nextIndex:nextIndex,phrases:phrases}) {
 const nextPhrase=phrases[nextIndex]
 return{nextPhrase:nextPhrase}
}