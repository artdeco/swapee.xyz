import adaptNextPhrase from './methods/adapt-next-phrase'
import {preadaptNextPhrase} from '../../gen/AbstractTypeWriterComputer/preadapters'
import AbstractTypeWriterComputer from '../../gen/AbstractTypeWriterComputer'

/** @extends {xyz.swapee.wc.TypeWriterComputer} */
export default class TypeWriterHtmlComputer extends AbstractTypeWriterComputer.implements(
 /** @type {!xyz.swapee.wc.ITypeWriterComputer} */ ({
  adaptNextPhrase:adaptNextPhrase,
  adapt:[preadaptNextPhrase],
 }),
){}