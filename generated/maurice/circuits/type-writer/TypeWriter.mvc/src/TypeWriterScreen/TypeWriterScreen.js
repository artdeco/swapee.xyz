import AbstractTypeWriterControllerAT from '../../gen/AbstractTypeWriterControllerAT'
import TypeWriterDisplay from '../TypeWriterDisplay'
import AbstractTypeWriterScreen from '../../gen/AbstractTypeWriterScreen'

/** @extends {xyz.swapee.wc.TypeWriterScreen} */
export default class extends AbstractTypeWriterScreen.implements(
 /**@type {!xyz.swapee.wc.ITypeWriterScreen}*/({get queries(){return this.settings}}),
 AbstractTypeWriterControllerAT,
 TypeWriterDisplay,
 /** @type {!xyz.swapee.wc.ITypeWriterScreen} */ ({
  __$id:6053195070,
 }),
/**@type {!xyz.swapee.wc.ITypeWriterScreen}*/({
   // deduceInputs(el) {},
   // deduceCache(el){
   //  const{innerText:innerText}=el
   //  return{
   //   currentPhrase:innerText,
   //  }
   // },
  }),
){}