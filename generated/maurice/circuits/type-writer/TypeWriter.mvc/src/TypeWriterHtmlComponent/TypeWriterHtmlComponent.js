import TypeWriterHtmlController from '../TypeWriterHtmlController'
import TypeWriterHtmlComputer from '../TypeWriterHtmlComputer'
import TypeWriterHtmlProcessor from '../TypeWriterHtmlProcessor'
import TypeWriterVirtualDisplay from '../TypeWriterVirtualDisplay'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractTypeWriterHtmlComponent} from '../../gen/AbstractTypeWriterHtmlComponent'

/** @extends {xyz.swapee.wc.TypeWriterHtmlComponent} */
export default class extends AbstractTypeWriterHtmlComponent.implements(
 TypeWriterHtmlController,
 TypeWriterHtmlComputer,
 TypeWriterHtmlProcessor,
 TypeWriterVirtualDisplay,
 IntegratedComponentInitialiser,
/**@type {!xyz.swapee.wc.ITypeWriterHtmlComponent}*/({
   // __$constructor() {},
  }),
){}