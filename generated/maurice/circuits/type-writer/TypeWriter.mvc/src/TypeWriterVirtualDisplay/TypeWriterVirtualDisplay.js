import paint from './methods/paint'
import {Painter} from '@webcircuits/webcircuits'

const TypeWriterVirtualDisplay=Painter.__trait(
 /** @type {!com.webcircuits.IPainter} */ ({
  paint:paint,
 }),
)
export default TypeWriterVirtualDisplay