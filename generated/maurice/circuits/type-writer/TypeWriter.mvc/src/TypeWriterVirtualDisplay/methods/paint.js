/** @type {xyz.swapee.wc.back.ITypeWriterDisplay._paint} */
export default function paint({nextPhrase:nextPhrase,enterDelay:enterDelay,eraseDelay:eraseDelay,hold:hold,holdClear:holdClear}){
  const{
   asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
  }=/**@type {!xyz.swapee.wc.back.ITypeWriterDisplay}*/(this)
  const _mem=serMemory({nextPhrase:nextPhrase,enterDelay:enterDelay,eraseDelay:eraseDelay,hold:hold,holdClear:holdClear})
  t_pa({
   pid:'7d382a8',
   mem:_mem,
  })
 }