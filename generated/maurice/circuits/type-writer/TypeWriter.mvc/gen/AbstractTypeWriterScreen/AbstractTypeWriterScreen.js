import AbstractTypeWriterScreenAR from '../AbstractTypeWriterScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {TypeWriterInputsPQs} from '../../pqs/TypeWriterInputsPQs'
import {TypeWriterMemoryQPs} from '../../pqs/TypeWriterMemoryQPs'
import {TypeWriterVdusPQs} from '../../pqs/TypeWriterVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterScreen}
 */
function __AbstractTypeWriterScreen() {}
__AbstractTypeWriterScreen.prototype = /** @type {!_AbstractTypeWriterScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterScreen}
 */
class _AbstractTypeWriterScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractTypeWriterScreen} ‎
 */
class AbstractTypeWriterScreen extends newAbstract(
 _AbstractTypeWriterScreen,605319507023,null,{
  asITypeWriterScreen:1,
  superTypeWriterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterScreen} */
AbstractTypeWriterScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterScreen} */
function AbstractTypeWriterScreenClass(){}

export default AbstractTypeWriterScreen


AbstractTypeWriterScreen[$implementations]=[
 __AbstractTypeWriterScreen,
 AbstractTypeWriterScreenClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterScreen}*/({
  inputsPQs:TypeWriterInputsPQs,
  memoryQPs:TypeWriterMemoryQPs,
 }),
 Screen,
 AbstractTypeWriterScreenAR,
 AbstractTypeWriterScreenClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterScreen}*/({
  vdusPQs:TypeWriterVdusPQs,
 }),
]