import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {TypeWriterInputsPQs} from '../../pqs/TypeWriterInputsPQs'
import {TypeWriterOuterCoreConstructor} from '../TypeWriterCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TypeWriterPort}
 */
function __TypeWriterPort() {}
__TypeWriterPort.prototype = /** @type {!_TypeWriterPort} */ ({ })
/** @this {xyz.swapee.wc.TypeWriterPort} */ function TypeWriterPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.TypeWriterOuterCore} */ ({model:null})
  TypeWriterOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterPort}
 */
class _TypeWriterPort { }
/**
 * The port that serves as an interface to the _ITypeWriter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractTypeWriterPort} ‎
 */
export class TypeWriterPort extends newAbstract(
 _TypeWriterPort,60531950705,TypeWriterPortConstructor,{
  asITypeWriterPort:1,
  superTypeWriterPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterPort} */
TypeWriterPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterPort} */
function TypeWriterPortClass(){}

export const TypeWriterPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.ITypeWriter.Pinout>}*/({
 get Port() { return TypeWriterPort },
})

TypeWriterPort[$implementations]=[
 TypeWriterPortClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterPort}*/({
  resetPort(){
   this.resetTypeWriterPort()
  },
  resetTypeWriterPort(){
   TypeWriterPortConstructor.call(this)
  },
 }),
 __TypeWriterPort,
 Parametric,
 TypeWriterPortClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterPort}*/({
  constructor(){
   mountPins(this.inputs,'',TypeWriterInputsPQs)
  },
 }),
]


export default TypeWriterPort