import {mountPins} from '@webcircuits/webcircuits'
import {TypeWriterMemoryPQs} from '../../pqs/TypeWriterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TypeWriterCore}
 */
function __TypeWriterCore() {}
__TypeWriterCore.prototype = /** @type {!_TypeWriterCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterCore}
 */
class _TypeWriterCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractTypeWriterCore} ‎
 */
class TypeWriterCore extends newAbstract(
 _TypeWriterCore,60531950706,null,{
  asITypeWriterCore:1,
  superTypeWriterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterCore} */
TypeWriterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterCore} */
function TypeWriterCoreClass(){}

export default TypeWriterCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TypeWriterOuterCore}
 */
function __TypeWriterOuterCore() {}
__TypeWriterOuterCore.prototype = /** @type {!_TypeWriterOuterCore} */ ({ })
/** @this {xyz.swapee.wc.TypeWriterOuterCore} */
export function TypeWriterOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.ITypeWriterOuterCore.Model}*/
  this.model={
    phrases: [],
    hold: 1250,
    holdClear: 100,
    enterDelay: 100,
    eraseDelay: 100,
    currentPhrase: '',
    nextPhrase: '',
    nextIndex: 1,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
class _TypeWriterOuterCore { }
/**
 * The _ITypeWriter_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractTypeWriterOuterCore} ‎
 */
export class TypeWriterOuterCore extends newAbstract(
 _TypeWriterOuterCore,60531950703,TypeWriterOuterCoreConstructor,{
  asITypeWriterOuterCore:1,
  superTypeWriterOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore} */
TypeWriterOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore} */
function TypeWriterOuterCoreClass(){}


TypeWriterOuterCore[$implementations]=[
 __TypeWriterOuterCore,
 TypeWriterOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterOuterCore}*/({
  constructor(){
   mountPins(this.model,'',TypeWriterMemoryPQs)

  },
 }),
]

TypeWriterCore[$implementations]=[
 TypeWriterCoreClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterCore}*/({
  resetCore(){
   this.resetTypeWriterCore()
  },
  resetTypeWriterCore(){
   TypeWriterOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.TypeWriterOuterCore}*/(
     /**@type {!xyz.swapee.wc.ITypeWriterOuterCore}*/(this)),
   )
  },
 }),
 __TypeWriterCore,
 TypeWriterOuterCore,
]

export {TypeWriterCore}