import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterComputer}
 */
function __AbstractTypeWriterComputer() {}
__AbstractTypeWriterComputer.prototype = /** @type {!_AbstractTypeWriterComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterComputer}
 */
class _AbstractTypeWriterComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractTypeWriterComputer} ‎
 */
export class AbstractTypeWriterComputer extends newAbstract(
 _AbstractTypeWriterComputer,60531950701,null,{
  asITypeWriterComputer:1,
  superTypeWriterComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterComputer} */
AbstractTypeWriterComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterComputer} */
function AbstractTypeWriterComputerClass(){}


AbstractTypeWriterComputer[$implementations]=[
 __AbstractTypeWriterComputer,
 Adapter,
]


export default AbstractTypeWriterComputer