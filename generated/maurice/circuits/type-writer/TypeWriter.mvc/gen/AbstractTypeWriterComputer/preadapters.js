
/**@this {xyz.swapee.wc.ITypeWriterComputer}*/
export function preadaptNextPhrase(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form}*/
 const _inputs={
  nextIndex:inputs.nextIndex,
  phrases:inputs.phrases,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptNextPhrase(__inputs,__changes)
 return RET
}