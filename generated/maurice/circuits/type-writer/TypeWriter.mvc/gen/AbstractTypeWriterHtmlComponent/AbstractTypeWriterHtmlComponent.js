import AbstractTypeWriterGPU from '../AbstractTypeWriterGPU'
import AbstractTypeWriterScreenBack from '../AbstractTypeWriterScreenBack'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {TypeWriterInputsQPs} from '../../pqs/TypeWriterInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTypeWriter from '../AbstractTypeWriter'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterHtmlComponent}
 */
function __AbstractTypeWriterHtmlComponent() {}
__AbstractTypeWriterHtmlComponent.prototype = /** @type {!_AbstractTypeWriterHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
class _AbstractTypeWriterHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.TypeWriterHtmlComponent} */ (res)
  }
}
/**
 * The _ITypeWriter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractTypeWriterHtmlComponent} ‎
 */
export class AbstractTypeWriterHtmlComponent extends newAbstract(
 _AbstractTypeWriterHtmlComponent,605319507010,null,{
  asITypeWriterHtmlComponent:1,
  superTypeWriterHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent} */
AbstractTypeWriterHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent} */
function AbstractTypeWriterHtmlComponentClass(){}


AbstractTypeWriterHtmlComponent[$implementations]=[
 __AbstractTypeWriterHtmlComponent,
 HtmlComponent,
 AbstractTypeWriter,
 AbstractTypeWriterGPU,
 AbstractTypeWriterScreenBack,
 AbstractTypeWriterHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterHtmlComponent}*/({
  inputsQPs:TypeWriterInputsQPs,
 }),
]