import {makeBuffers} from '@webcircuits/webcircuits'

export const TypeWriterBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  phrases:[String],
  hold:Number,
  holdClear:Number,
  enterDelay:Number,
  eraseDelay:Number,
  currentPhrase:String,
  nextPhrase:String,
  nextIndex:Number,
 }),
})

export default TypeWriterBuffer