import TypeWriterBuffer from '../TypeWriterBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {TypeWriterPortConnector} from '../TypeWriterPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterController}
 */
function __AbstractTypeWriterController() {}
__AbstractTypeWriterController.prototype = /** @type {!_AbstractTypeWriterController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterController}
 */
class _AbstractTypeWriterController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractTypeWriterController} ‎
 */
export class AbstractTypeWriterController extends newAbstract(
 _AbstractTypeWriterController,605319507018,null,{
  asITypeWriterController:1,
  superTypeWriterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterController} */
AbstractTypeWriterController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterController} */
function AbstractTypeWriterControllerClass(){}


AbstractTypeWriterController[$implementations]=[
 AbstractTypeWriterControllerClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.ITypeWriterPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractTypeWriterController,
 TypeWriterBuffer,
 IntegratedController,
 /**@type {!AbstractTypeWriterController}*/(TypeWriterPortConnector),
]


export default AbstractTypeWriterController