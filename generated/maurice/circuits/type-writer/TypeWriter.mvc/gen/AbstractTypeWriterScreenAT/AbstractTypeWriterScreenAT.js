import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterScreenAT}
 */
function __AbstractTypeWriterScreenAT() {}
__AbstractTypeWriterScreenAT.prototype = /** @type {!_AbstractTypeWriterScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
class _AbstractTypeWriterScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITypeWriterScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterScreenAT} ‎
 */
class AbstractTypeWriterScreenAT extends newAbstract(
 _AbstractTypeWriterScreenAT,605319507026,null,{
  asITypeWriterScreenAT:1,
  superTypeWriterScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT} */
AbstractTypeWriterScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT} */
function AbstractTypeWriterScreenATClass(){}

export default AbstractTypeWriterScreenAT


AbstractTypeWriterScreenAT[$implementations]=[
 __AbstractTypeWriterScreenAT,
 UartUniversal,
]