import AbstractTypeWriterDisplay from '../AbstractTypeWriterDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {TypeWriterVdusPQs} from '../../pqs/TypeWriterVdusPQs'
import {TypeWriterVdusQPs} from '../../pqs/TypeWriterVdusQPs'
import {TypeWriterMemoryPQs} from '../../pqs/TypeWriterMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterGPU}
 */
function __AbstractTypeWriterGPU() {}
__AbstractTypeWriterGPU.prototype = /** @type {!_AbstractTypeWriterGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterGPU}
 */
class _AbstractTypeWriterGPU { }
/**
 * Handles the periphery of the _ITypeWriterDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractTypeWriterGPU} ‎
 */
class AbstractTypeWriterGPU extends newAbstract(
 _AbstractTypeWriterGPU,605319507014,null,{
  asITypeWriterGPU:1,
  superTypeWriterGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterGPU} */
AbstractTypeWriterGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterGPU} */
function AbstractTypeWriterGPUClass(){}

export default AbstractTypeWriterGPU


AbstractTypeWriterGPU[$implementations]=[
 __AbstractTypeWriterGPU,
 AbstractTypeWriterGPUClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterGPU}*/({
  vdusPQs:TypeWriterVdusPQs,
  vdusQPs:TypeWriterVdusQPs,
  memoryPQs:TypeWriterMemoryPQs,
 }),
 AbstractTypeWriterDisplay,
 BrowserView,
]