import AbstractTypeWriterProcessor from '../AbstractTypeWriterProcessor'
import {TypeWriterCore} from '../TypeWriterCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractTypeWriterComputer} from '../AbstractTypeWriterComputer'
import {AbstractTypeWriterController} from '../AbstractTypeWriterController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriter}
 */
function __AbstractTypeWriter() {}
__AbstractTypeWriter.prototype = /** @type {!_AbstractTypeWriter} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriter}
 */
class _AbstractTypeWriter { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractTypeWriter} ‎
 */
class AbstractTypeWriter extends newAbstract(
 _AbstractTypeWriter,60531950708,null,{
  asITypeWriter:1,
  superTypeWriter:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriter} */
AbstractTypeWriter.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriter} */
function AbstractTypeWriterClass(){}

export default AbstractTypeWriter


AbstractTypeWriter[$implementations]=[
 __AbstractTypeWriter,
 TypeWriterCore,
 AbstractTypeWriterProcessor,
 IntegratedComponent,
 AbstractTypeWriterComputer,
 AbstractTypeWriterController,
]


export {AbstractTypeWriter}