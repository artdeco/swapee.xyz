import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterDisplay}
 */
function __AbstractTypeWriterDisplay() {}
__AbstractTypeWriterDisplay.prototype = /** @type {!_AbstractTypeWriterDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
class _AbstractTypeWriterDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterDisplay} ‎
 */
class AbstractTypeWriterDisplay extends newAbstract(
 _AbstractTypeWriterDisplay,605319507017,null,{
  asITypeWriterDisplay:1,
  superTypeWriterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay} */
AbstractTypeWriterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay} */
function AbstractTypeWriterDisplayClass(){}

export default AbstractTypeWriterDisplay


AbstractTypeWriterDisplay[$implementations]=[
 __AbstractTypeWriterDisplay,
 GraphicsDriverBack,
]