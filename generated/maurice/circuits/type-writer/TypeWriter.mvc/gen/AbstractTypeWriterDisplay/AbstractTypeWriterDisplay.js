import {Display} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterDisplay}
 */
function __AbstractTypeWriterDisplay() {}
__AbstractTypeWriterDisplay.prototype = /** @type {!_AbstractTypeWriterDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
class _AbstractTypeWriterDisplay { }
/**
 * Display for presenting information from the _ITypeWriter_.
 * @extends {xyz.swapee.wc.AbstractTypeWriterDisplay} ‎
 */
class AbstractTypeWriterDisplay extends newAbstract(
 _AbstractTypeWriterDisplay,605319507015,null,{
  asITypeWriterDisplay:1,
  superTypeWriterDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterDisplay} */
AbstractTypeWriterDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterDisplay} */
function AbstractTypeWriterDisplayClass(){}

export default AbstractTypeWriterDisplay


AbstractTypeWriterDisplay[$implementations]=[
 __AbstractTypeWriterDisplay,
 Display,
]