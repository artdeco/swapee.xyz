import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterScreenAR}
 */
function __AbstractTypeWriterScreenAR() {}
__AbstractTypeWriterScreenAR.prototype = /** @type {!_AbstractTypeWriterScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
class _AbstractTypeWriterScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITypeWriterScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractTypeWriterScreenAR} ‎
 */
class AbstractTypeWriterScreenAR extends newAbstract(
 _AbstractTypeWriterScreenAR,605319507025,null,{
  asITypeWriterScreenAR:1,
  superTypeWriterScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR} */
AbstractTypeWriterScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR} */
function AbstractTypeWriterScreenARClass(){}

export default AbstractTypeWriterScreenAR


AbstractTypeWriterScreenAR[$implementations]=[
 __AbstractTypeWriterScreenAR,
 AR,
 AbstractTypeWriterScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.ITypeWriterScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractTypeWriterScreenAR}