import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_TypeWriterElementPort}
 */
function __TypeWriterElementPort() {}
__TypeWriterElementPort.prototype = /** @type {!_TypeWriterElementPort} */ ({ })
/** @this {xyz.swapee.wc.TypeWriterElementPort} */ function TypeWriterElementPortConstructor() {
  /**@type {!xyz.swapee.wc.ITypeWriterElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterElementPort}
 */
class _TypeWriterElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractTypeWriterElementPort} ‎
 */
class TypeWriterElementPort extends newAbstract(
 _TypeWriterElementPort,605319507012,TypeWriterElementPortConstructor,{
  asITypeWriterElementPort:1,
  superTypeWriterElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterElementPort} */
TypeWriterElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterElementPort} */
function TypeWriterElementPortClass(){}

export default TypeWriterElementPort


TypeWriterElementPort[$implementations]=[
 __TypeWriterElementPort,
 TypeWriterElementPortClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'hold-clear':undefined,
    'enter-delay':undefined,
    'erase-delay':undefined,
    'current-phrase':undefined,
    'next-phrase':undefined,
    'next-index':undefined,
   })
  },
 }),
]