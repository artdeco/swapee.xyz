import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterControllerAR}
 */
function __AbstractTypeWriterControllerAR() {}
__AbstractTypeWriterControllerAR.prototype = /** @type {!_AbstractTypeWriterControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
class _AbstractTypeWriterControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITypeWriterControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterControllerAR} ‎
 */
class AbstractTypeWriterControllerAR extends newAbstract(
 _AbstractTypeWriterControllerAR,605319507021,null,{
  asITypeWriterControllerAR:1,
  superTypeWriterControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR} */
AbstractTypeWriterControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR} */
function AbstractTypeWriterControllerARClass(){}

export default AbstractTypeWriterControllerAR


AbstractTypeWriterControllerAR[$implementations]=[
 __AbstractTypeWriterControllerAR,
 AR,
 AbstractTypeWriterControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.ITypeWriterControllerAR}*/({
  allocator(){
   this.methods={
    next:'684d2',
   }
  },
 }),
]