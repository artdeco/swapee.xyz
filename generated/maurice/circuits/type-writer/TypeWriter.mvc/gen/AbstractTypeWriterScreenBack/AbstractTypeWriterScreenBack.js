import AbstractTypeWriterScreenAT from '../AbstractTypeWriterScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterScreenBack}
 */
function __AbstractTypeWriterScreenBack() {}
__AbstractTypeWriterScreenBack.prototype = /** @type {!_AbstractTypeWriterScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
class _AbstractTypeWriterScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterScreen} ‎
 */
class AbstractTypeWriterScreenBack extends newAbstract(
 _AbstractTypeWriterScreenBack,605319507024,null,{
  asITypeWriterScreen:1,
  superTypeWriterScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen} */
AbstractTypeWriterScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen} */
function AbstractTypeWriterScreenBackClass(){}

export default AbstractTypeWriterScreenBack


AbstractTypeWriterScreenBack[$implementations]=[
 __AbstractTypeWriterScreenBack,
 AbstractTypeWriterScreenAT,
]