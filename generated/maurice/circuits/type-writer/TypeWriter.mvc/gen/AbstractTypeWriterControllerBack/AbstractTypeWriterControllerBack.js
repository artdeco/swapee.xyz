import AbstractTypeWriterControllerAR from '../AbstractTypeWriterControllerAR'
import {AbstractTypeWriterController} from '../AbstractTypeWriterController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterControllerBack}
 */
function __AbstractTypeWriterControllerBack() {}
__AbstractTypeWriterControllerBack.prototype = /** @type {!_AbstractTypeWriterControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterController}
 */
class _AbstractTypeWriterControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractTypeWriterController} ‎
 */
class AbstractTypeWriterControllerBack extends newAbstract(
 _AbstractTypeWriterControllerBack,605319507020,null,{
  asITypeWriterController:1,
  superTypeWriterController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterController} */
AbstractTypeWriterControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterController} */
function AbstractTypeWriterControllerBackClass(){}

export default AbstractTypeWriterControllerBack


AbstractTypeWriterControllerBack[$implementations]=[
 __AbstractTypeWriterControllerBack,
 AbstractTypeWriterController,
 AbstractTypeWriterControllerAR,
 DriverBack,
]