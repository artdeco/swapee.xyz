
import AbstractTypeWriter from '../AbstractTypeWriter'

/** @abstract {xyz.swapee.wc.ITypeWriterElement} */
export default class AbstractTypeWriterElement { }



AbstractTypeWriterElement[$implementations]=[AbstractTypeWriter,
 /** @type {!AbstractTypeWriterElement} */ ({
  rootId:'TypeWriter',
  __$id:6053195070,
  fqn:'xyz.swapee.wc.ITypeWriter',
  maurice_element_v3:true,
 }),
]