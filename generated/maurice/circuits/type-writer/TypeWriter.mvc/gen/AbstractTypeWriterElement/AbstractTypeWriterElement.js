import TypeWriterElementPort from '../TypeWriterElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {TypeWriterInputsPQs} from '../../pqs/TypeWriterInputsPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractTypeWriter from '../AbstractTypeWriter'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterElement}
 */
function __AbstractTypeWriterElement() {}
__AbstractTypeWriterElement.prototype = /** @type {!_AbstractTypeWriterElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterElement}
 */
class _AbstractTypeWriterElement { }
/**
 * A component description.
 *
 * The _ITypeWriter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractTypeWriterElement} ‎
 */
class AbstractTypeWriterElement extends newAbstract(
 _AbstractTypeWriterElement,605319507011,null,{
  asITypeWriterElement:1,
  superTypeWriterElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterElement} */
AbstractTypeWriterElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterElement} */
function AbstractTypeWriterElementClass(){}

export default AbstractTypeWriterElement


AbstractTypeWriterElement[$implementations]=[
 __AbstractTypeWriterElement,
 ElementBase,
 AbstractTypeWriterElementClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':phrases':phrasesColAttr,
   ':hold':holdColAttr,
   ':hold-clear':holdClearColAttr,
   ':enter-delay':enterDelayColAttr,
   ':erase-delay':eraseDelayColAttr,
   ':current-phrase':currentPhraseColAttr,
   ':next-phrase':nextPhraseColAttr,
   ':next-index':nextIndexColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'phrases':phrasesAttr,
    'hold':holdAttr,
    'hold-clear':holdClearAttr,
    'enter-delay':enterDelayAttr,
    'erase-delay':eraseDelayAttr,
    'current-phrase':currentPhraseAttr,
    'next-phrase':nextPhraseAttr,
    'next-index':nextIndexAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(phrasesAttr===undefined?{'phrases':phrasesColAttr}:{}),
    ...(holdAttr===undefined?{'hold':holdColAttr}:{}),
    ...(holdClearAttr===undefined?{'hold-clear':holdClearColAttr}:{}),
    ...(enterDelayAttr===undefined?{'enter-delay':enterDelayColAttr}:{}),
    ...(eraseDelayAttr===undefined?{'erase-delay':eraseDelayColAttr}:{}),
    ...(currentPhraseAttr===undefined?{'current-phrase':currentPhraseColAttr}:{}),
    ...(nextPhraseAttr===undefined?{'next-phrase':nextPhraseColAttr}:{}),
    ...(nextIndexAttr===undefined?{'next-index':nextIndexColAttr}:{}),
   }
  },
 }),
 AbstractTypeWriterElementClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'phrases':phrasesAttr,
   'hold':holdAttr,
   'hold-clear':holdClearAttr,
   'enter-delay':enterDelayAttr,
   'erase-delay':eraseDelayAttr,
   'current-phrase':currentPhraseAttr,
   'next-phrase':nextPhraseAttr,
   'next-index':nextIndexAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    phrases:phrasesAttr,
    hold:holdAttr,
    holdClear:holdClearAttr,
    enterDelay:enterDelayAttr,
    eraseDelay:eraseDelayAttr,
    currentPhrase:currentPhraseAttr,
    nextPhrase:nextPhraseAttr,
    nextIndex:nextIndexAttr,
   }
  },
 }),
 AbstractTypeWriterElementClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractTypeWriterElementClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
  inputsPQs:TypeWriterInputsPQs,
  vdus:{},
 }),
 IntegratedComponent,
 AbstractTypeWriterElementClass.prototype=/**@type {!xyz.swapee.wc.ITypeWriterElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','phrases','hold','holdClear','enterDelay','eraseDelay','currentPhrase','nextPhrase','nextIndex','no-solder',':no-solder',':phrases',':hold','hold-clear',':hold-clear','enter-delay',':enter-delay','erase-delay',':erase-delay','current-phrase',':current-phrase','next-phrase',':next-phrase','next-index',':next-index','fe646','5a196','af1d8','57bdd','3b40c','26e0c','53af0','25444','b6f75','children']),
   })
  },
  get Port(){
   return TypeWriterElementPort
  },
 }),
]



AbstractTypeWriterElement[$implementations]=[AbstractTypeWriter,
 /** @type {!AbstractTypeWriterElement} */ ({
  rootId:'TypeWriter',
  __$id:6053195070,
  fqn:'xyz.swapee.wc.ITypeWriter',
  maurice_element_v3:true,
 }),
]