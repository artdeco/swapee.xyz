import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterProcessor}
 */
function __AbstractTypeWriterProcessor() {}
__AbstractTypeWriterProcessor.prototype = /** @type {!_AbstractTypeWriterProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
class _AbstractTypeWriterProcessor { }
/**
 * The processor to compute changes to the memory for the _ITypeWriter_.
 * @extends {xyz.swapee.wc.AbstractTypeWriterProcessor} ‎
 */
class AbstractTypeWriterProcessor extends newAbstract(
 _AbstractTypeWriterProcessor,60531950707,null,{
  asITypeWriterProcessor:1,
  superTypeWriterProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractTypeWriterProcessor} */
AbstractTypeWriterProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterProcessor} */
function AbstractTypeWriterProcessorClass(){}

export default AbstractTypeWriterProcessor


AbstractTypeWriterProcessor[$implementations]=[
 __AbstractTypeWriterProcessor,
]