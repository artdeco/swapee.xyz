import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractTypeWriterControllerAT}
 */
function __AbstractTypeWriterControllerAT() {}
__AbstractTypeWriterControllerAT.prototype = /** @type {!_AbstractTypeWriterControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
class _AbstractTypeWriterControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITypeWriterControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractTypeWriterControllerAT} ‎
 */
class AbstractTypeWriterControllerAT extends newAbstract(
 _AbstractTypeWriterControllerAT,605319507022,null,{
  asITypeWriterControllerAT:1,
  superTypeWriterControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT} */
AbstractTypeWriterControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT} */
function AbstractTypeWriterControllerATClass(){}

export default AbstractTypeWriterControllerAT


AbstractTypeWriterControllerAT[$implementations]=[
 __AbstractTypeWriterControllerAT,
 UartUniversal,
 AbstractTypeWriterControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.ITypeWriterControllerAT}*/({
  get asITypeWriterController(){
   return this
  },
  next(){
   this.uart.t("inv",{mid:'684d2'})
  },
 }),
]