/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPU.Initialese filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplay.Initialese}
 */
$xyz.swapee.wc.ITypeWriterGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterGPU.Initialese} */
xyz.swapee.wc.ITypeWriterGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPUFields filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITypeWriterGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.ITypeWriterGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPUFields}
 */
xyz.swapee.wc.ITypeWriterGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPUCaster filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITypeWriterGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterGPU} */
$xyz.swapee.wc.ITypeWriterGPUCaster.prototype.asITypeWriterGPU
/** @type {!xyz.swapee.wc.BoundTypeWriterGPU} */
$xyz.swapee.wc.ITypeWriterGPUCaster.prototype.superTypeWriterGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPUCaster}
 */
xyz.swapee.wc.ITypeWriterGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!TypeWriterMemory,>}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplay}
 */
$xyz.swapee.wc.ITypeWriterGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPU}
 */
xyz.swapee.wc.ITypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.TypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterGPU.Initialese>}
 */
$xyz.swapee.wc.TypeWriterGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.TypeWriterGPU
/** @type {function(new: xyz.swapee.wc.ITypeWriterGPU, ...!xyz.swapee.wc.ITypeWriterGPU.Initialese)} */
xyz.swapee.wc.TypeWriterGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.TypeWriterGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.AbstractTypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterGPU}
 */
$xyz.swapee.wc.AbstractTypeWriterGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterGPU)} */
xyz.swapee.wc.AbstractTypeWriterGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.TypeWriterGPUConstructor filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterGPU, ...!xyz.swapee.wc.ITypeWriterGPU.Initialese)} */
xyz.swapee.wc.TypeWriterGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.RecordITypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.BoundITypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterGPUFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!TypeWriterMemory,>}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterDisplay}
 */
$xyz.swapee.wc.BoundITypeWriterGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterGPU} */
xyz.swapee.wc.BoundITypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.BoundTypeWriterGPU filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterGPU} */
xyz.swapee.wc.BoundTypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */