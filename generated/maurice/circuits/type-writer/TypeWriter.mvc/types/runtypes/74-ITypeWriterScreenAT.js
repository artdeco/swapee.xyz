/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} */
xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenATCaster filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterScreenAT} */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.asITypeWriterScreenAT
/** @type {!xyz.swapee.wc.back.BoundTypeWriterScreenAT} */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.superTypeWriterScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 */
xyz.swapee.wc.back.ITypeWriterScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.ITypeWriterScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenAT}
 */
xyz.swapee.wc.back.ITypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.TypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.TypeWriterScreenAT
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterScreenAT, ...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.TypeWriterScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.AbstractTypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterScreenAT}
 */
$xyz.swapee.wc.back.AbstractTypeWriterScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterScreenAT)} */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.TypeWriterScreenATConstructor filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreenAT, ...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.RecordITypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.BoundITypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundITypeWriterScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterScreenAT} */
xyz.swapee.wc.back.BoundITypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.BoundTypeWriterScreenAT filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterScreenAT} */
xyz.swapee.wc.back.BoundTypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */