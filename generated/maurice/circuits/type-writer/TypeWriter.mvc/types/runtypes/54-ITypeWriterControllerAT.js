/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} */
xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerATCaster filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterControllerAT} */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.asITypeWriterControllerAT
/** @type {!xyz.swapee.wc.front.BoundTypeWriterControllerAT} */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.superTypeWriterControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 */
xyz.swapee.wc.front.ITypeWriterControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.ITypeWriterControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerAT}
 */
xyz.swapee.wc.front.ITypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.TypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.TypeWriterControllerAT
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterControllerAT, ...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.TypeWriterControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.AbstractTypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterControllerAT}
 */
$xyz.swapee.wc.front.AbstractTypeWriterControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterControllerAT)} */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.TypeWriterControllerATConstructor filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterControllerAT, ...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.RecordITypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITypeWriterControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.BoundITypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundITypeWriterControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterControllerAT} */
xyz.swapee.wc.front.BoundITypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.BoundTypeWriterControllerAT filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterControllerAT} */
xyz.swapee.wc.front.BoundTypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */