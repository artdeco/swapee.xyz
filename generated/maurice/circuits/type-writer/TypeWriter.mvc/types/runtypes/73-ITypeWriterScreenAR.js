/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterScreen.Initialese}
 */
$xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} */
xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenARCaster filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterScreenAR} */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.asITypeWriterScreenAR
/** @type {!xyz.swapee.wc.front.BoundTypeWriterScreenAR} */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.superTypeWriterScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 */
xyz.swapee.wc.front.ITypeWriterScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITypeWriterScreen}
 */
$xyz.swapee.wc.front.ITypeWriterScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterScreenAR}
 */
xyz.swapee.wc.front.ITypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.TypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.TypeWriterScreenAR
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterScreenAR, ...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese)} */
xyz.swapee.wc.front.TypeWriterScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.TypeWriterScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.AbstractTypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterScreenAR}
 */
$xyz.swapee.wc.front.AbstractTypeWriterScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterScreenAR)} */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.TypeWriterScreenARConstructor filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterScreenAR, ...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese)} */
xyz.swapee.wc.front.TypeWriterScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.RecordITypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITypeWriterScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.BoundITypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITypeWriterScreen}
 */
$xyz.swapee.wc.front.BoundITypeWriterScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterScreenAR} */
xyz.swapee.wc.front.BoundITypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.BoundTypeWriterScreenAR filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterScreenAR} */
xyz.swapee.wc.front.BoundTypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */