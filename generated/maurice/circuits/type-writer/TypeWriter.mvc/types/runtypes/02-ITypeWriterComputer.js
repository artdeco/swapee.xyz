/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.Initialese filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.ITypeWriterComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.Initialese} */
xyz.swapee.wc.ITypeWriterComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputerCaster filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @interface */
$xyz.swapee.wc.ITypeWriterComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterComputer} */
$xyz.swapee.wc.ITypeWriterComputerCaster.prototype.asITypeWriterComputer
/** @type {!xyz.swapee.wc.BoundTypeWriterComputer} */
$xyz.swapee.wc.ITypeWriterComputerCaster.prototype.superTypeWriterComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterComputerCaster}
 */
xyz.swapee.wc.ITypeWriterComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.TypeWriterMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.ITypeWriterComputer = function() {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 */
$xyz.swapee.wc.ITypeWriterComputer.prototype.adaptNextPhrase = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.TypeWriterMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterComputer}
 */
xyz.swapee.wc.ITypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.TypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterComputer.Initialese>}
 */
$xyz.swapee.wc.TypeWriterComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.TypeWriterComputer
/** @type {function(new: xyz.swapee.wc.ITypeWriterComputer, ...!xyz.swapee.wc.ITypeWriterComputer.Initialese)} */
xyz.swapee.wc.TypeWriterComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.TypeWriterComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.AbstractTypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterComputer}
 */
$xyz.swapee.wc.AbstractTypeWriterComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterComputer)} */
xyz.swapee.wc.AbstractTypeWriterComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.TypeWriterComputerConstructor filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterComputer, ...!xyz.swapee.wc.ITypeWriterComputer.Initialese)} */
xyz.swapee.wc.TypeWriterComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.RecordITypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @typedef {{ adaptNextPhrase: xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase, compute: xyz.swapee.wc.ITypeWriterComputer.compute }} */
xyz.swapee.wc.RecordITypeWriterComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.BoundITypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.TypeWriterMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundITypeWriterComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterComputer} */
xyz.swapee.wc.BoundITypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.BoundTypeWriterComputer filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterComputer} */
xyz.swapee.wc.BoundTypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 * @this {xyz.swapee.wc.ITypeWriterComputer}
 */
$xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase

// nss:xyz.swapee.wc.ITypeWriterComputer,$xyz.swapee.wc.ITypeWriterComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.compute filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.TypeWriterMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.TypeWriterMemory): void} */
xyz.swapee.wc.ITypeWriterComputer.compute
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterComputer, xyz.swapee.wc.TypeWriterMemory): void} */
xyz.swapee.wc.ITypeWriterComputer._compute
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.__compute} */
xyz.swapee.wc.ITypeWriterComputer.__compute

// nss:xyz.swapee.wc.ITypeWriterComputer,$xyz.swapee.wc.ITypeWriterComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe}
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/* @typal-end */