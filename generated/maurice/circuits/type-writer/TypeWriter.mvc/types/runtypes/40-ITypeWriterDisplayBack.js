/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay.Initialese filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TypeWriterClasses>}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterDisplay.Initialese} */
xyz.swapee.wc.back.ITypeWriterDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplayCaster filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterDisplay} */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/** @type {!xyz.swapee.wc.back.BoundTypeWriterDisplay} */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 */
xyz.swapee.wc.back.ITypeWriterDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.TypeWriterClasses, null>}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterDisplay}
 */
xyz.swapee.wc.back.ITypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.TypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ITypeWriterDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterDisplay.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.TypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterDisplay)} */
xyz.swapee.wc.back.TypeWriterDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.TypeWriterDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.AbstractTypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.TypeWriterDisplay}
 */
$xyz.swapee.wc.back.AbstractTypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterDisplay)} */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.RecordITypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/** @typedef {{ paint: xyz.swapee.wc.back.ITypeWriterDisplay.paint }} */
xyz.swapee.wc.back.RecordITypeWriterDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.BoundITypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.TypeWriterClasses, null>}
 */
$xyz.swapee.wc.back.BoundITypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterDisplay} */
xyz.swapee.wc.back.BoundITypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.BoundTypeWriterDisplay filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterDisplay} */
xyz.swapee.wc.back.BoundTypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay.paint filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory=, null=): void} */
xyz.swapee.wc.back.ITypeWriterDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ITypeWriterDisplay, !xyz.swapee.wc.TypeWriterMemory=, null=): void} */
xyz.swapee.wc.back.ITypeWriterDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.ITypeWriterDisplay.__paint} */
xyz.swapee.wc.back.ITypeWriterDisplay.__paint

// nss:xyz.swapee.wc.back.ITypeWriterDisplay,$xyz.swapee.wc.back.ITypeWriterDisplay,xyz.swapee.wc.back
/* @typal-end */