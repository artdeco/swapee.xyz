/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.Initialese filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @record */
$xyz.swapee.wc.ITypeWriterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Initialese} */
xyz.swapee.wc.ITypeWriterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCoreFields filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @interface */
$xyz.swapee.wc.ITypeWriterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterCore.Model} */
$xyz.swapee.wc.ITypeWriterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCoreFields}
 */
xyz.swapee.wc.ITypeWriterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCoreCaster filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @interface */
$xyz.swapee.wc.ITypeWriterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterCore} */
$xyz.swapee.wc.ITypeWriterCoreCaster.prototype.asITypeWriterCore
/** @type {!xyz.swapee.wc.BoundTypeWriterCore} */
$xyz.swapee.wc.ITypeWriterCoreCaster.prototype.superTypeWriterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCoreCaster}
 */
xyz.swapee.wc.ITypeWriterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCoreCaster}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore}
 */
$xyz.swapee.wc.ITypeWriterCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterCore.prototype.resetTypeWriterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCore}
 */
xyz.swapee.wc.ITypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.TypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterCore.Initialese>}
 */
$xyz.swapee.wc.TypeWriterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.TypeWriterCore
/** @type {function(new: xyz.swapee.wc.ITypeWriterCore)} */
xyz.swapee.wc.TypeWriterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.TypeWriterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.AbstractTypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterCore}
 */
$xyz.swapee.wc.AbstractTypeWriterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterCore)} */
xyz.swapee.wc.AbstractTypeWriterCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.RecordITypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @typedef {{ resetCore: xyz.swapee.wc.ITypeWriterCore.resetCore, resetTypeWriterCore: xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore }} */
xyz.swapee.wc.RecordITypeWriterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.BoundITypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCoreFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCoreCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterOuterCore}
 */
$xyz.swapee.wc.BoundITypeWriterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterCore} */
xyz.swapee.wc.BoundITypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.BoundTypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterCore} */
xyz.swapee.wc.BoundTypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.resetCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterCore): void} */
xyz.swapee.wc.ITypeWriterCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterCore.__resetCore} */
xyz.swapee.wc.ITypeWriterCore.__resetCore

// nss:xyz.swapee.wc.ITypeWriterCore,$xyz.swapee.wc.ITypeWriterCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterCore): void} */
xyz.swapee.wc.ITypeWriterCore._resetTypeWriterCore
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore} */
xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore

// nss:xyz.swapee.wc.ITypeWriterCore,$xyz.swapee.wc.ITypeWriterCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model}
 */
$xyz.swapee.wc.ITypeWriterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model} */
xyz.swapee.wc.ITypeWriterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore
/* @typal-end */