/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessor.Initialese filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.ITypeWriterProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterProcessor.Initialese} */
xyz.swapee.wc.ITypeWriterProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessorCaster filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @interface */
$xyz.swapee.wc.ITypeWriterProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.asITypeWriterProcessor
/** @type {!xyz.swapee.wc.BoundTypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.superTypeWriterProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterProcessorCaster}
 */
xyz.swapee.wc.ITypeWriterProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterProcessorCaster}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {xyz.swapee.wc.ITypeWriterCore}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 */
$xyz.swapee.wc.ITypeWriterProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterProcessor}
 */
xyz.swapee.wc.ITypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.TypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterProcessor.Initialese>}
 */
$xyz.swapee.wc.TypeWriterProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.TypeWriterProcessor
/** @type {function(new: xyz.swapee.wc.ITypeWriterProcessor, ...!xyz.swapee.wc.ITypeWriterProcessor.Initialese)} */
xyz.swapee.wc.TypeWriterProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.TypeWriterProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.AbstractTypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterProcessor}
 */
$xyz.swapee.wc.AbstractTypeWriterProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterProcessor)} */
xyz.swapee.wc.AbstractTypeWriterProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.TypeWriterProcessorConstructor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterProcessor, ...!xyz.swapee.wc.ITypeWriterProcessor.Initialese)} */
xyz.swapee.wc.TypeWriterProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.RecordITypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.BoundITypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterProcessorCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {xyz.swapee.wc.BoundITypeWriterCore}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 */
$xyz.swapee.wc.BoundITypeWriterProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterProcessor} */
xyz.swapee.wc.BoundITypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.BoundTypeWriterProcessor filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterProcessor} */
xyz.swapee.wc.BoundTypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */