/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.Initialese filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 */
$xyz.swapee.wc.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.Initialese} */
xyz.swapee.wc.ITypeWriterController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterControllerFields filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @interface */
$xyz.swapee.wc.ITypeWriterControllerFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterController.Inputs} */
$xyz.swapee.wc.ITypeWriterControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterControllerFields}
 */
xyz.swapee.wc.ITypeWriterControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterControllerCaster filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @interface */
$xyz.swapee.wc.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterController} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.BoundITypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterProcessor
/** @type {!xyz.swapee.wc.BoundTypeWriterController} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.ITypeWriterControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.TypeWriterMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterController = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterController.prototype.resetPort = function() {}
/** @return {?} */
$xyz.swapee.wc.ITypeWriterController.prototype.next = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterController}
 */
xyz.swapee.wc.ITypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.TypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.TypeWriterController
/** @type {function(new: xyz.swapee.wc.ITypeWriterController, ...!xyz.swapee.wc.ITypeWriterController.Initialese)} */
xyz.swapee.wc.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.TypeWriterController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.AbstractTypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterController}
 */
$xyz.swapee.wc.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterController)} */
xyz.swapee.wc.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.TypeWriterControllerConstructor filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterController, ...!xyz.swapee.wc.ITypeWriterController.Initialese)} */
xyz.swapee.wc.TypeWriterControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.RecordITypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITypeWriterController.resetPort, next: xyz.swapee.wc.ITypeWriterController.next }} */
xyz.swapee.wc.RecordITypeWriterController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.BoundITypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterControllerFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.TypeWriterMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterController} */
xyz.swapee.wc.BoundITypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.BoundTypeWriterController filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterController} */
xyz.swapee.wc.BoundTypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.resetPort filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterController): void} */
xyz.swapee.wc.ITypeWriterController._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterController.__resetPort} */
xyz.swapee.wc.ITypeWriterController.__resetPort

// nss:xyz.swapee.wc.ITypeWriterController,$xyz.swapee.wc.ITypeWriterController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.next filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.ITypeWriterController.__next = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITypeWriterController.next
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterController)} */
xyz.swapee.wc.ITypeWriterController._next
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterController.__next} */
xyz.swapee.wc.ITypeWriterController.__next

// nss:xyz.swapee.wc.ITypeWriterController,$xyz.swapee.wc.ITypeWriterController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.Inputs filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPort.Inputs}
 */
$xyz.swapee.wc.ITypeWriterController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.Inputs} */
xyz.swapee.wc.ITypeWriterController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.WeakInputs filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPort.WeakInputs}
 */
$xyz.swapee.wc.ITypeWriterController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.WeakInputs} */
xyz.swapee.wc.ITypeWriterController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */