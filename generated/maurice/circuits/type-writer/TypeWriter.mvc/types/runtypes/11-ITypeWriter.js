/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterEnv filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @record */
$xyz.swapee.wc.TypeWriterEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.ITypeWriter} */
$xyz.swapee.wc.TypeWriterEnv.prototype.typeWriter
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TypeWriterEnv}
 */
xyz.swapee.wc.TypeWriterEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.Initialese filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.ITypeWriter.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriter.Initialese} */
xyz.swapee.wc.ITypeWriter.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterFields filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @interface */
$xyz.swapee.wc.ITypeWriterFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriter.Pinout} */
$xyz.swapee.wc.ITypeWriterFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterFields}
 */
xyz.swapee.wc.ITypeWriterFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterCaster filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @interface */
$xyz.swapee.wc.ITypeWriterCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriter} */
$xyz.swapee.wc.ITypeWriterCaster.prototype.asITypeWriter
/** @type {!xyz.swapee.wc.BoundTypeWriter} */
$xyz.swapee.wc.ITypeWriterCaster.prototype.superTypeWriter
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCaster}
 */
xyz.swapee.wc.ITypeWriterCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCaster}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, null>}
 */
$xyz.swapee.wc.ITypeWriter = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriter}
 */
xyz.swapee.wc.ITypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriter}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriter.Initialese>}
 */
$xyz.swapee.wc.TypeWriter = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.TypeWriter
/** @type {function(new: xyz.swapee.wc.ITypeWriter, ...!xyz.swapee.wc.ITypeWriter.Initialese)} */
xyz.swapee.wc.TypeWriter.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.TypeWriter.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.AbstractTypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriter}
 */
$xyz.swapee.wc.AbstractTypeWriter = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriter)} */
xyz.swapee.wc.AbstractTypeWriter.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriter.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterConstructor filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriter, ...!xyz.swapee.wc.ITypeWriter.Initialese)} */
xyz.swapee.wc.TypeWriterConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.MVCOptions filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @record */
$xyz.swapee.wc.ITypeWriter.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ITypeWriter.Pinout)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ITypeWriter.Pinout)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ITypeWriter.Pinout} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.TypeWriterMemory)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.TypeWriterClasses)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.ITypeWriter.MVCOptions} */
xyz.swapee.wc.ITypeWriter.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.RecordITypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriter

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.BoundITypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterFields}
 * @extends {xyz.swapee.wc.RecordITypeWriter}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, null>}
 */
$xyz.swapee.wc.BoundITypeWriter = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriter} */
xyz.swapee.wc.BoundITypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.BoundTypeWriter filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriter}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriter = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriter} */
xyz.swapee.wc.BoundTypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.Pinout filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterController.Inputs}
 */
$xyz.swapee.wc.ITypeWriter.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriter.Pinout} */
xyz.swapee.wc.ITypeWriter.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterBuffer filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterBuffer}
 */
xyz.swapee.wc.ITypeWriterBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterBuffer filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterBuffer}
 */
$xyz.swapee.wc.TypeWriterBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterBuffer}
 */
xyz.swapee.wc.TypeWriterBuffer
/** @type {function(new: xyz.swapee.wc.ITypeWriterBuffer)} */
xyz.swapee.wc.TypeWriterBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */