/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Initialese filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings>}
 */
$xyz.swapee.wc.ITypeWriterDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Initialese} */
xyz.swapee.wc.ITypeWriterDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplayFields filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @interface */
$xyz.swapee.wc.ITypeWriterDisplayFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterDisplay.Settings} */
$xyz.swapee.wc.ITypeWriterDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ITypeWriterDisplay.Queries} */
$xyz.swapee.wc.ITypeWriterDisplayFields.prototype.queries
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplayFields}
 */
xyz.swapee.wc.ITypeWriterDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplayCaster filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @interface */
$xyz.swapee.wc.ITypeWriterDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterDisplay} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/** @type {!xyz.swapee.wc.BoundITypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.BoundTypeWriterDisplay} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplayCaster}
 */
xyz.swapee.wc.ITypeWriterDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.TypeWriterMemory, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 */
$xyz.swapee.wc.ITypeWriterDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplay}
 */
xyz.swapee.wc.ITypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.TypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterDisplay.Initialese>}
 */
$xyz.swapee.wc.TypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.TypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.ITypeWriterDisplay, ...!xyz.swapee.wc.ITypeWriterDisplay.Initialese)} */
xyz.swapee.wc.TypeWriterDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.TypeWriterDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.AbstractTypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterDisplay}
 */
$xyz.swapee.wc.AbstractTypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterDisplay)} */
xyz.swapee.wc.AbstractTypeWriterDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.TypeWriterDisplayConstructor filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterDisplay, ...!xyz.swapee.wc.ITypeWriterDisplay.Initialese)} */
xyz.swapee.wc.TypeWriterDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.RecordITypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @typedef {{ paint: xyz.swapee.wc.ITypeWriterDisplay.paint }} */
xyz.swapee.wc.RecordITypeWriterDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.BoundITypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterDisplayFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.TypeWriterMemory, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundITypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterDisplay} */
xyz.swapee.wc.BoundITypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.BoundTypeWriterDisplay filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterDisplay} */
xyz.swapee.wc.BoundTypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.paint filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory, null): void} */
xyz.swapee.wc.ITypeWriterDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterDisplay, !xyz.swapee.wc.TypeWriterMemory, null): void} */
xyz.swapee.wc.ITypeWriterDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterDisplay.__paint} */
xyz.swapee.wc.ITypeWriterDisplay.__paint

// nss:xyz.swapee.wc.ITypeWriterDisplay,$xyz.swapee.wc.ITypeWriterDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Queries filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @record */
$xyz.swapee.wc.ITypeWriterDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Queries} */
xyz.swapee.wc.ITypeWriterDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Settings filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterDisplay.Queries}
 */
$xyz.swapee.wc.ITypeWriterDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Settings} */
xyz.swapee.wc.ITypeWriterDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */