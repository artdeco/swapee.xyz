/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.Initialese filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ITypeWriterPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Initialese} */
xyz.swapee.wc.ITypeWriterPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPortFields filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @interface */
$xyz.swapee.wc.ITypeWriterPortFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */
$xyz.swapee.wc.ITypeWriterPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */
$xyz.swapee.wc.ITypeWriterPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPortFields}
 */
xyz.swapee.wc.ITypeWriterPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPortCaster filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @interface */
$xyz.swapee.wc.ITypeWriterPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterPort} */
$xyz.swapee.wc.ITypeWriterPortCaster.prototype.asITypeWriterPort
/** @type {!xyz.swapee.wc.BoundTypeWriterPort} */
$xyz.swapee.wc.ITypeWriterPortCaster.prototype.superTypeWriterPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPortCaster}
 */
xyz.swapee.wc.ITypeWriterPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITypeWriterPort.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterPort.prototype.resetTypeWriterPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPort}
 */
xyz.swapee.wc.ITypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.TypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterPort.Initialese>}
 */
$xyz.swapee.wc.TypeWriterPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.TypeWriterPort
/** @type {function(new: xyz.swapee.wc.ITypeWriterPort, ...!xyz.swapee.wc.ITypeWriterPort.Initialese)} */
xyz.swapee.wc.TypeWriterPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.TypeWriterPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.AbstractTypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterPort}
 */
$xyz.swapee.wc.AbstractTypeWriterPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterPort)} */
xyz.swapee.wc.AbstractTypeWriterPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.TypeWriterPortConstructor filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterPort, ...!xyz.swapee.wc.ITypeWriterPort.Initialese)} */
xyz.swapee.wc.TypeWriterPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.RecordITypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITypeWriterPort.resetPort, resetTypeWriterPort: xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort }} */
xyz.swapee.wc.RecordITypeWriterPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.BoundITypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPortFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITypeWriterPort.Inputs>}
 */
$xyz.swapee.wc.BoundITypeWriterPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterPort} */
xyz.swapee.wc.BoundITypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.BoundTypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterPort} */
xyz.swapee.wc.BoundTypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.resetPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterPort): void} */
xyz.swapee.wc.ITypeWriterPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterPort.__resetPort} */
xyz.swapee.wc.ITypeWriterPort.__resetPort

// nss:xyz.swapee.wc.ITypeWriterPort,$xyz.swapee.wc.ITypeWriterPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterPort): void} */
xyz.swapee.wc.ITypeWriterPort._resetTypeWriterPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort} */
xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort

// nss:xyz.swapee.wc.ITypeWriterPort,$xyz.swapee.wc.ITypeWriterPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.Inputs filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterPort.Inputs}
 */
xyz.swapee.wc.ITypeWriterPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterPort.WeakInputs}
 */
xyz.swapee.wc.ITypeWriterPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */