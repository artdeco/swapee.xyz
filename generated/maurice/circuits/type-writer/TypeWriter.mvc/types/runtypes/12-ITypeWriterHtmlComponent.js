/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterController.Initialese}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreen.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriter.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {ITypeWriterGenerator.Initialese}
 */
$xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} */
xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponentCaster filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @interface */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterHtmlComponent} */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.asITypeWriterHtmlComponent
/** @type {!xyz.swapee.wc.BoundTypeWriterHtmlComponent} */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.superTypeWriterHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 */
xyz.swapee.wc.ITypeWriterHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ITypeWriterController}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreen}
 * @extends {xyz.swapee.wc.ITypeWriter}
 * @extends {xyz.swapee.wc.ITypeWriterGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {ITypeWriterGenerator}
 */
$xyz.swapee.wc.ITypeWriterHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterHtmlComponent}
 */
xyz.swapee.wc.ITypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.TypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.TypeWriterHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.TypeWriterHtmlComponent
/** @type {function(new: xyz.swapee.wc.ITypeWriterHtmlComponent, ...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese)} */
xyz.swapee.wc.TypeWriterHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.TypeWriterHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.AbstractTypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterHtmlComponent}
 */
$xyz.swapee.wc.AbstractTypeWriterHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterHtmlComponent)} */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.TypeWriterHtmlComponentConstructor filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterHtmlComponent, ...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese)} */
xyz.swapee.wc.TypeWriterHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.RecordITypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.BoundITypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterController}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreen}
 * @extends {xyz.swapee.wc.BoundITypeWriter}
 * @extends {xyz.swapee.wc.BoundITypeWriterGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {BoundITypeWriterGenerator}
 */
$xyz.swapee.wc.BoundITypeWriterHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterHtmlComponent} */
xyz.swapee.wc.BoundITypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.BoundTypeWriterHtmlComponent filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterHtmlComponent} */
xyz.swapee.wc.BoundTypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */