/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} */
xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerARCaster filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterControllerAR} */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.asITypeWriterControllerAR
/** @type {!xyz.swapee.wc.back.BoundTypeWriterControllerAR} */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.superTypeWriterControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 */
xyz.swapee.wc.back.ITypeWriterControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 */
$xyz.swapee.wc.back.ITypeWriterControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerAR}
 */
xyz.swapee.wc.back.ITypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.TypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.TypeWriterControllerAR
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterControllerAR, ...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.TypeWriterControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.AbstractTypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterControllerAR}
 */
$xyz.swapee.wc.back.AbstractTypeWriterControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterControllerAR)} */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.TypeWriterControllerARConstructor filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterControllerAR, ...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.RecordITypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.BoundITypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 */
$xyz.swapee.wc.back.BoundITypeWriterControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterControllerAR} */
xyz.swapee.wc.back.BoundITypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.BoundTypeWriterControllerAR filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterControllerAR} */
xyz.swapee.wc.back.BoundTypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */