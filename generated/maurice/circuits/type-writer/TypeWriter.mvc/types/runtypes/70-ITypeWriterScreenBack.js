/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreen.Initialese filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterScreen.Initialese} */
xyz.swapee.wc.back.ITypeWriterScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreenCaster filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterScreen} */
$xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.back.BoundTypeWriterScreen} */
$xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.superTypeWriterScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenCaster}
 */
xyz.swapee.wc.back.ITypeWriterScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenCaster}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenAT}
 */
$xyz.swapee.wc.back.ITypeWriterScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreen}
 */
xyz.swapee.wc.back.ITypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.TypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreen.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.TypeWriterScreen
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterScreen, ...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.TypeWriterScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.AbstractTypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterScreen}
 */
$xyz.swapee.wc.back.AbstractTypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterScreen)} */
xyz.swapee.wc.back.AbstractTypeWriterScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.TypeWriterScreenConstructor filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreen, ...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.RecordITypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.BoundITypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreenAT}
 */
$xyz.swapee.wc.back.BoundITypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterScreen} */
xyz.swapee.wc.back.BoundITypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.BoundTypeWriterScreen filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterScreen} */
xyz.swapee.wc.back.BoundTypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */