/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.Initialese filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ITypeWriterElementPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterElementPort.Initialese} */
xyz.swapee.wc.ITypeWriterElementPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPortFields filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @interface */
$xyz.swapee.wc.ITypeWriterElementPortFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterElementPort.Inputs} */
$xyz.swapee.wc.ITypeWriterElementPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITypeWriterElementPort.Inputs} */
$xyz.swapee.wc.ITypeWriterElementPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterElementPortFields}
 */
xyz.swapee.wc.ITypeWriterElementPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPortCaster filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @interface */
$xyz.swapee.wc.ITypeWriterElementPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterElementPort} */
$xyz.swapee.wc.ITypeWriterElementPortCaster.prototype.asITypeWriterElementPort
/** @type {!xyz.swapee.wc.BoundTypeWriterElementPort} */
$xyz.swapee.wc.ITypeWriterElementPortCaster.prototype.superTypeWriterElementPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterElementPortCaster}
 */
xyz.swapee.wc.ITypeWriterElementPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterElementPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterElementPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITypeWriterElementPort.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterElementPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterElementPort}
 */
xyz.swapee.wc.ITypeWriterElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.TypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterElementPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElementPort.Initialese>}
 */
$xyz.swapee.wc.TypeWriterElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.TypeWriterElementPort
/** @type {function(new: xyz.swapee.wc.ITypeWriterElementPort, ...!xyz.swapee.wc.ITypeWriterElementPort.Initialese)} */
xyz.swapee.wc.TypeWriterElementPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.TypeWriterElementPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.AbstractTypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterElementPort}
 */
$xyz.swapee.wc.AbstractTypeWriterElementPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterElementPort)} */
xyz.swapee.wc.AbstractTypeWriterElementPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.TypeWriterElementPortConstructor filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementPort, ...!xyz.swapee.wc.ITypeWriterElementPort.Initialese)} */
xyz.swapee.wc.TypeWriterElementPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.RecordITypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterElementPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.BoundITypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterElementPortFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterElementPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterElementPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITypeWriterElementPort.Inputs>}
 */
$xyz.swapee.wc.BoundITypeWriterElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterElementPort} */
xyz.swapee.wc.BoundITypeWriterElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.BoundTypeWriterElementPort filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterElementPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterElementPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterElementPort} */
xyz.swapee.wc.BoundTypeWriterElementPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.noSolder filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @typedef {boolean} */
xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.noSolder

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @record */
$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder = function() {}
/** @type {boolean|undefined} */
$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder} */
xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.Inputs filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder}
 */
$xyz.swapee.wc.ITypeWriterElementPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterElementPort.Inputs}
 */
xyz.swapee.wc.ITypeWriterElementPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @record */
$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder} */
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder}
 */
$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs}
 */
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @record */
$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe = function() {}
/** @type {boolean} */
$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe} */
xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe filter:!ControllerPlugin~props e8387990252964cfa0b385636dd9b2bc */
/** @record */
$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe.prototype.noSolder
/** @typedef {$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe} */
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterElementPort.WeakInputs
/* @typal-end */