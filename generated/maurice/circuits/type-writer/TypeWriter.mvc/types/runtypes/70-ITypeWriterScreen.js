/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreen.Initialese filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.ITypeWriterDisplay.Initialese}
 */
$xyz.swapee.wc.ITypeWriterScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterScreen.Initialese} */
xyz.swapee.wc.ITypeWriterScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreenCaster filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @interface */
$xyz.swapee.wc.ITypeWriterScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.BoundTypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterScreenCaster.prototype.superTypeWriterScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterScreenCaster}
 */
xyz.swapee.wc.ITypeWriterScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.ITypeWriterController}
 * @extends {xyz.swapee.wc.ITypeWriterDisplay}
 */
$xyz.swapee.wc.ITypeWriterScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterScreen}
 */
xyz.swapee.wc.ITypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.TypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterScreen.Initialese>}
 */
$xyz.swapee.wc.TypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.TypeWriterScreen
/** @type {function(new: xyz.swapee.wc.ITypeWriterScreen, ...!xyz.swapee.wc.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.TypeWriterScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.TypeWriterScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.AbstractTypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterScreen}
 */
$xyz.swapee.wc.AbstractTypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterScreen)} */
xyz.swapee.wc.AbstractTypeWriterScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.TypeWriterScreenConstructor filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterScreen, ...!xyz.swapee.wc.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.TypeWriterScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.RecordITypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.BoundITypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundITypeWriterController}
 * @extends {xyz.swapee.wc.BoundITypeWriterDisplay}
 */
$xyz.swapee.wc.BoundITypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterScreen} */
xyz.swapee.wc.BoundITypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.BoundTypeWriterScreen filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterScreen} */
xyz.swapee.wc.BoundTypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */