/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController.Initialese filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @record */
$xyz.swapee.wc.front.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterController.Initialese} */
xyz.swapee.wc.front.ITypeWriterController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterControllerCaster filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterController} */
$xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.front.BoundTypeWriterController} */
$xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.front.ITypeWriterControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerAT}
 */
$xyz.swapee.wc.front.ITypeWriterController = function() {}
/** @return {?} */
$xyz.swapee.wc.front.ITypeWriterController.prototype.next = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterController}
 */
xyz.swapee.wc.front.ITypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.TypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.TypeWriterController
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterController, ...!xyz.swapee.wc.front.ITypeWriterController.Initialese)} */
xyz.swapee.wc.front.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.TypeWriterController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.AbstractTypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterController}
 */
$xyz.swapee.wc.front.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterController)} */
xyz.swapee.wc.front.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.TypeWriterControllerConstructor filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterController, ...!xyz.swapee.wc.front.ITypeWriterController.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.RecordITypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @typedef {{ next: xyz.swapee.wc.front.ITypeWriterController.next }} */
xyz.swapee.wc.front.RecordITypeWriterController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.BoundITypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundITypeWriterControllerAT}
 */
$xyz.swapee.wc.front.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterController} */
xyz.swapee.wc.front.BoundITypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.BoundTypeWriterController filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterController} */
xyz.swapee.wc.front.BoundTypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController.next filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.ITypeWriterController.__next = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITypeWriterController.next
/** @typedef {function(this: xyz.swapee.wc.front.ITypeWriterController)} */
xyz.swapee.wc.front.ITypeWriterController._next
/** @typedef {typeof $xyz.swapee.wc.front.ITypeWriterController.__next} */
xyz.swapee.wc.front.ITypeWriterController.__next

// nss:xyz.swapee.wc.front.ITypeWriterController,$xyz.swapee.wc.front.ITypeWriterController,xyz.swapee.wc.front
/* @typal-end */