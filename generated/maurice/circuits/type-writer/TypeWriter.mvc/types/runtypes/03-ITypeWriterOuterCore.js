/** @const {?} */ $xyz.swapee.wc.ITypeWriterPort
/** @const {?} */ $xyz.swapee.wc.ITypeWriterPort.Inputs
/** @const {?} */ $xyz.swapee.wc.ITypeWriterPort.WeakInputs
/** @const {?} */ $xyz.swapee.wc.ITypeWriterCore
/** @const {?} */ $xyz.swapee.wc.ITypeWriterCore.Model
/** @const {?} */ xyz.swapee.wc.ITypeWriterPort
/** @const {?} */ xyz.swapee.wc.ITypeWriterPort.Inputs
/** @const {?} */ xyz.swapee.wc.ITypeWriterPort.WeakInputs
/** @const {?} */ xyz.swapee.wc.ITypeWriterCore
/** @const {?} */ xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Initialese filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Initialese} */
xyz.swapee.wc.ITypeWriterOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCoreFields filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @interface */
$xyz.swapee.wc.ITypeWriterOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterOuterCore.Model} */
$xyz.swapee.wc.ITypeWriterOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCoreFields}
 */
xyz.swapee.wc.ITypeWriterOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCoreCaster filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @interface */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterOuterCore} */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.asITypeWriterOuterCore
/** @type {!xyz.swapee.wc.BoundTypeWriterOuterCore} */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.superTypeWriterOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
xyz.swapee.wc.ITypeWriterOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
$xyz.swapee.wc.ITypeWriterOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCore}
 */
xyz.swapee.wc.ITypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.TypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterOuterCore.Initialese>}
 */
$xyz.swapee.wc.TypeWriterOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.TypeWriterOuterCore
/** @type {function(new: xyz.swapee.wc.ITypeWriterOuterCore)} */
xyz.swapee.wc.TypeWriterOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.TypeWriterOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.AbstractTypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterOuterCore}
 */
$xyz.swapee.wc.AbstractTypeWriterOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterOuterCore)} */
xyz.swapee.wc.AbstractTypeWriterOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.RecordITypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.BoundITypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
$xyz.swapee.wc.BoundITypeWriterOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterOuterCore} */
xyz.swapee.wc.BoundITypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.BoundTypeWriterOuterCore filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterOuterCore} */
xyz.swapee.wc.BoundTypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {!Array<string>} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.phrases

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.hold

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.holdClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.holdClear

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.enterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.enterDelay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.eraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.eraseDelay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.currentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {string} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.currentPhrase

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.nextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {string} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.nextPhrase

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.nextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.nextIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases = function() {}
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model} */
xyz.swapee.wc.ITypeWriterOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe = function() {}
/** @type {!Array<string>} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear} */
xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Phrases filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Phrases} */
xyz.swapee.wc.ITypeWriterCore.Model.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Hold filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Hold} */
xyz.swapee.wc.ITypeWriterCore.Model.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear} */
xyz.swapee.wc.ITypeWriterCore.Model.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay} */
xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay} */
xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase} */
xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex} */
xyz.swapee.wc.ITypeWriterCore.Model.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */