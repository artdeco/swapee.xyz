/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/102-TypeWriterInputs.xml} xyz.swapee.wc.front.TypeWriterInputs filter:!ControllerPlugin~props 0fd59a7318f455dbb1981204b63e7e44 */
/** @record */
$xyz.swapee.wc.front.TypeWriterInputs = __$te_Mixin()
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.phrases
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.hold
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.holdClear
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.enterDelay
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.eraseDelay
/** @type {string|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.currentPhrase
/** @type {string|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.nextPhrase
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.TypeWriterInputs}
 */
xyz.swapee.wc.front.TypeWriterInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */