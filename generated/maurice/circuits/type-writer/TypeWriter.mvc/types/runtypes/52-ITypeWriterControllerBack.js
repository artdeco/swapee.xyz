/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterController.Initialese filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterController.Initialese} */
xyz.swapee.wc.back.ITypeWriterController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterControllerCaster filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterController} */
$xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.back.BoundTypeWriterController} */
$xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.back.ITypeWriterControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.back.ITypeWriterController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterController}
 */
xyz.swapee.wc.back.ITypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.TypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.TypeWriterController
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterController, ...!xyz.swapee.wc.back.ITypeWriterController.Initialese)} */
xyz.swapee.wc.back.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.TypeWriterController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.AbstractTypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterController}
 */
$xyz.swapee.wc.back.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterController)} */
xyz.swapee.wc.back.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.TypeWriterControllerConstructor filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterController, ...!xyz.swapee.wc.back.ITypeWriterController.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.RecordITypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.BoundITypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.back.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterController} */
xyz.swapee.wc.back.BoundITypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.BoundTypeWriterController filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterController} */
xyz.swapee.wc.back.BoundTypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */