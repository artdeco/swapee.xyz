/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/100-TypeWriterMemory.xml} xyz.swapee.wc.TypeWriterMemory filter:!ControllerPlugin~props 0af58cb1ef41987cf32400f70830dc92 */
/** @record */
$xyz.swapee.wc.TypeWriterMemory = __$te_Mixin()
/** @type {!Array<string>} */
$xyz.swapee.wc.TypeWriterMemory.prototype.phrases
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.hold
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.holdClear
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.enterDelay
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.eraseDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemory.prototype.currentPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemory.prototype.nextPhrase
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TypeWriterMemory}
 */
xyz.swapee.wc.TypeWriterMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */