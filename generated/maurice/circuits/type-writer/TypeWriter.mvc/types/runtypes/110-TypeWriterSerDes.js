/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterMemoryPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TypeWriterMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.phrases
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.hold
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.holdClear
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.enterDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.eraseDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.currentPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.nextPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterMemoryPQs}
 */
xyz.swapee.wc.TypeWriterMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterMemoryQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TypeWriterMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.fa196
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.af1d8
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f7bdd
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.db40c
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c6e0c
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f3af0
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c5444
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.b6f75
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterMemoryQPs}
 */
xyz.swapee.wc.TypeWriterMemoryQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterMemoryQPs)} */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterInputsPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.TypeWriterMemoryPQs}
 */
$xyz.swapee.wc.TypeWriterInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterInputsPQs}
 */
xyz.swapee.wc.TypeWriterInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterInputsQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.TypeWriterInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterInputsQPs}
 */
xyz.swapee.wc.TypeWriterInputsQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterInputsQPs)} */
xyz.swapee.wc.TypeWriterInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterVdusPQs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TypeWriterVdusPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterVdusPQs}
 */
xyz.swapee.wc.TypeWriterVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterVdusQPs filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TypeWriterVdusQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterVdusQPs}
 */
xyz.swapee.wc.TypeWriterVdusQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterVdusQPs)} */
xyz.swapee.wc.TypeWriterVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */