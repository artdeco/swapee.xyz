/**
 * @fileoverview
 * @externs
 */

/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.Initialese  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs>}
 * @extends {_findesiècle.IHTMLBlocker.Initialese}
 * @extends {guest.maurice.IGuest.Initialese}
 */
xyz.swapee.wc.ITypeWriterElement.Initialese = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElementFields  ea61de72b575a3480a63dc967b68c9d6 */
/** @interface */
xyz.swapee.wc.ITypeWriterElementFields
/** @type {!xyz.swapee.wc.ITypeWriterElement.Inputs} */
xyz.swapee.wc.ITypeWriterElementFields.prototype.inputs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElementCaster  ea61de72b575a3480a63dc967b68c9d6 */
/** @interface */
xyz.swapee.wc.ITypeWriterElementCaster
/** @type {!xyz.swapee.wc.BoundITypeWriterElement} */
xyz.swapee.wc.ITypeWriterElementCaster.prototype.asITypeWriterElement
/** @type {!xyz.swapee.wc.BoundTypeWriterElement} */
xyz.swapee.wc.ITypeWriterElementCaster.prototype.superTypeWriterElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterElementFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterElementCaster}
 * @extends {_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs>}
 * @extends {guest.maurice.IGuest}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs, null>}
 */
xyz.swapee.wc.ITypeWriterElement = function() {}
/** @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init */
xyz.swapee.wc.ITypeWriterElement.prototype.constructor = function(...init) {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} model
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} props
 * @return {Object<string, *>}
 */
xyz.swapee.wc.ITypeWriterElement.prototype.solder = function(model, props) {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITypeWriterElement.prototype.render = function(model, instance) {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
xyz.swapee.wc.ITypeWriterElement.prototype.server = function(memory, inputs) {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model]
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} [port]
 * @return {?}
 */
xyz.swapee.wc.ITypeWriterElement.prototype.inducer = function(model, port) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.TypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterElement}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElement.Initialese>}
 */
xyz.swapee.wc.TypeWriterElement = function(...init) {}
/** @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init */
xyz.swapee.wc.TypeWriterElement.prototype.constructor = function(...init) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.TypeWriterElement.__extend = function(...Extensions) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.AbstractTypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement = function(...init) {}
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterElement.implements = function(...Implementations) {}
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.clone = function(data) {}
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.__extend = function(...Extensions) {}
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.continues = function(...Implementations) {}
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.__trait = function(...Implementations) {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.TypeWriterElementConstructor  ea61de72b575a3480a63dc967b68c9d6 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElement, ...!xyz.swapee.wc.ITypeWriterElement.Initialese)} */
xyz.swapee.wc.TypeWriterElementConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.RecordITypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/** @typedef {{ solder: xyz.swapee.wc.ITypeWriterElement.solder, render: xyz.swapee.wc.ITypeWriterElement.render, server: xyz.swapee.wc.ITypeWriterElement.server, inducer: xyz.swapee.wc.ITypeWriterElement.inducer }} */
xyz.swapee.wc.RecordITypeWriterElement

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.BoundITypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterElementFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterElement}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterElementCaster}
 * @extends {_findesiècle.BoundIHTMLBlocker<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs>}
 * @extends {guest.maurice.BoundIGuest}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs, null>}
 */
xyz.swapee.wc.BoundITypeWriterElement = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.BoundTypeWriterElement  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterElement}
 * @extends {engineering.type.BoundIInitialiser}
 */
xyz.swapee.wc.BoundTypeWriterElement = function() {}

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.solder  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} model
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} props
 * @return {Object<string, *>}
 */
$$xyz.swapee.wc.ITypeWriterElement.__solder = function(model, props) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs): Object<string, *>} */
xyz.swapee.wc.ITypeWriterElement.solder
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterElement, !xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs): Object<string, *>} */
xyz.swapee.wc.ITypeWriterElement._solder
/** @typedef {typeof $$xyz.swapee.wc.ITypeWriterElement.__solder} */
xyz.swapee.wc.ITypeWriterElement.__solder

// nss:xyz.swapee.wc.ITypeWriterElement,$$xyz.swapee.wc.ITypeWriterElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.render  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model]
 * @param {!Object<string, !Function>} [instance]
 * @return {!engineering.type.VNode}
 */
$$xyz.swapee.wc.ITypeWriterElement.__render = function(model, instance) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITypeWriterElement.render
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterElement, !xyz.swapee.wc.TypeWriterMemory=, !Object<string, !Function>=): !engineering.type.VNode} */
xyz.swapee.wc.ITypeWriterElement._render
/** @typedef {typeof $$xyz.swapee.wc.ITypeWriterElement.__render} */
xyz.swapee.wc.ITypeWriterElement.__render

// nss:xyz.swapee.wc.ITypeWriterElement,$$xyz.swapee.wc.ITypeWriterElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.server  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} inputs
 * @return {!engineering.type.VNode}
 */
$$xyz.swapee.wc.ITypeWriterElement.__server = function(memory, inputs) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs): !engineering.type.VNode} */
xyz.swapee.wc.ITypeWriterElement.server
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterElement, !xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs): !engineering.type.VNode} */
xyz.swapee.wc.ITypeWriterElement._server
/** @typedef {typeof $$xyz.swapee.wc.ITypeWriterElement.__server} */
xyz.swapee.wc.ITypeWriterElement.__server

// nss:xyz.swapee.wc.ITypeWriterElement,$$xyz.swapee.wc.ITypeWriterElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.inducer  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model]
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} [port]
 */
$$xyz.swapee.wc.ITypeWriterElement.__inducer = function(model, port) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory=, !xyz.swapee.wc.ITypeWriterElement.Inputs=)} */
xyz.swapee.wc.ITypeWriterElement.inducer
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterElement, !xyz.swapee.wc.TypeWriterMemory=, !xyz.swapee.wc.ITypeWriterElement.Inputs=)} */
xyz.swapee.wc.ITypeWriterElement._inducer
/** @typedef {typeof $$xyz.swapee.wc.ITypeWriterElement.__inducer} */
xyz.swapee.wc.ITypeWriterElement.__inducer

// nss:xyz.swapee.wc.ITypeWriterElement,$$xyz.swapee.wc.ITypeWriterElement,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml} xyz.swapee.wc.ITypeWriterElement.Inputs  ea61de72b575a3480a63dc967b68c9d6 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPort.Inputs}
 * @extends {xyz.swapee.wc.ITypeWriterDisplay.Queries}
 * @extends {xyz.swapee.wc.ITypeWriterController.Inputs}
 * @extends {guest.maurice.IGuestPort.Inputs}
 * @extends {xyz.swapee.wc.ITypeWriterElementPort.Inputs}
 */
xyz.swapee.wc.ITypeWriterElement.Inputs = function() {}

// nss:xyz.swapee.wc
/* @typal-end */