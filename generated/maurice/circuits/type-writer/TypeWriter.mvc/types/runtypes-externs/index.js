/** @const {?} */ $xyz.swapee.wc.ITypeWriterDesigner.communicator
/** @const {?} */ $xyz.swapee.wc.ITypeWriterDesigner.relay
/** @const {?} */ xyz.swapee.wc.ITypeWriterDesigner.communicator
/** @const {?} */ xyz.swapee.wc.ITypeWriterDesigner.relay
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {com.webcircuits.IAdapter.Initialese}
 */
$xyz.swapee.wc.ITypeWriterComputer.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.Initialese} */
xyz.swapee.wc.ITypeWriterComputer.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @interface */
$xyz.swapee.wc.ITypeWriterComputerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterComputer} */
$xyz.swapee.wc.ITypeWriterComputerCaster.prototype.asITypeWriterComputer
/** @type {!xyz.swapee.wc.BoundTypeWriterComputer} */
$xyz.swapee.wc.ITypeWriterComputerCaster.prototype.superTypeWriterComputer
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterComputerCaster}
 */
xyz.swapee.wc.ITypeWriterComputerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterComputerCaster}
 * @extends {com.webcircuits.IAdapter<!xyz.swapee.wc.TypeWriterMemory>}
 * @extends {com.webcircuits.ILanded<null>}
 */
$xyz.swapee.wc.ITypeWriterComputer = function() {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 */
$xyz.swapee.wc.ITypeWriterComputer.prototype.adaptNextPhrase = function(form, changes) {}
/**
 * @param {xyz.swapee.wc.TypeWriterMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterComputer.prototype.compute = function(mem) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterComputer}
 */
xyz.swapee.wc.ITypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.TypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterComputer}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterComputer.Initialese>}
 */
$xyz.swapee.wc.TypeWriterComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.TypeWriterComputer
/** @type {function(new: xyz.swapee.wc.ITypeWriterComputer, ...!xyz.swapee.wc.ITypeWriterComputer.Initialese)} */
xyz.swapee.wc.TypeWriterComputer.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.TypeWriterComputer.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.AbstractTypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterComputer}
 */
$xyz.swapee.wc.AbstractTypeWriterComputer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterComputer)} */
xyz.swapee.wc.AbstractTypeWriterComputer.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterComputer.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.TypeWriterComputerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterComputer, ...!xyz.swapee.wc.ITypeWriterComputer.Initialese)} */
xyz.swapee.wc.TypeWriterComputerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.RecordITypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/** @typedef {{ adaptNextPhrase: xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase, compute: xyz.swapee.wc.ITypeWriterComputer.compute }} */
xyz.swapee.wc.RecordITypeWriterComputer

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.BoundITypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterComputer}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterComputerCaster}
 * @extends {com.webcircuits.BoundIAdapter<!xyz.swapee.wc.TypeWriterMemory>}
 * @extends {com.webcircuits.BoundILanded<null>}
 */
$xyz.swapee.wc.BoundITypeWriterComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterComputer} */
xyz.swapee.wc.BoundITypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.BoundTypeWriterComputer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterComputer = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterComputer} */
xyz.swapee.wc.BoundTypeWriterComputer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase = function(form, changes) {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 * @this {xyz.swapee.wc.ITypeWriterComputer}
 */
$xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase = function(form, changes) {}
/**
 * @template THIS
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes
 * @return {(undefined|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)}
 * @this {THIS}
 */
$xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase = function(form, changes) {}
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase

// nss:xyz.swapee.wc.ITypeWriterComputer,$xyz.swapee.wc.ITypeWriterComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.compute exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @this {THIS}
 * @template THIS
 * @param {xyz.swapee.wc.TypeWriterMemory} mem
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterComputer.__compute = function(mem) {}
/** @typedef {function(xyz.swapee.wc.TypeWriterMemory): void} */
xyz.swapee.wc.ITypeWriterComputer.compute
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterComputer, xyz.swapee.wc.TypeWriterMemory): void} */
xyz.swapee.wc.ITypeWriterComputer._compute
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterComputer.__compute} */
xyz.swapee.wc.ITypeWriterComputer.__compute

// nss:xyz.swapee.wc.ITypeWriterComputer,$xyz.swapee.wc.ITypeWriterComputer,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe = function() {}
/** @type {!Array<string>} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe}
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase} */
xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 874baad0ed61be36f507fcce89d5bb15 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return} */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Initialese} */
xyz.swapee.wc.ITypeWriterOuterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @interface */
$xyz.swapee.wc.ITypeWriterOuterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterOuterCore.Model} */
$xyz.swapee.wc.ITypeWriterOuterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCoreFields}
 */
xyz.swapee.wc.ITypeWriterOuterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @interface */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterOuterCore} */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.asITypeWriterOuterCore
/** @type {!xyz.swapee.wc.BoundTypeWriterOuterCore} */
$xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.superTypeWriterOuterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
xyz.swapee.wc.ITypeWriterOuterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
$xyz.swapee.wc.ITypeWriterOuterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterOuterCore}
 */
xyz.swapee.wc.ITypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.TypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterOuterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterOuterCore.Initialese>}
 */
$xyz.swapee.wc.TypeWriterOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.TypeWriterOuterCore
/** @type {function(new: xyz.swapee.wc.ITypeWriterOuterCore)} */
xyz.swapee.wc.TypeWriterOuterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.TypeWriterOuterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.AbstractTypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterOuterCore}
 */
$xyz.swapee.wc.AbstractTypeWriterOuterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterOuterCore)} */
xyz.swapee.wc.AbstractTypeWriterOuterCore.prototype.constructor
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__extend
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.continues
/**
 * @param {...(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterMemoryPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TypeWriterMemoryPQs = function() {}
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.phrases
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.hold
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.holdClear
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.enterDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.eraseDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.currentPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.nextPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryPQs.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterMemoryPQs}
 */
xyz.swapee.wc.TypeWriterMemoryPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterMemoryQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TypeWriterMemoryQPs = __$te_Mixin()
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.fa196
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.af1d8
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f7bdd
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.db40c
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c6e0c
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f3af0
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c5444
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemoryQPs.prototype.b6f75
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterMemoryQPs}
 */
xyz.swapee.wc.TypeWriterMemoryQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterMemoryQPs)} */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.RecordITypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterOuterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.BoundITypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterOuterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCoreCaster}
 */
$xyz.swapee.wc.BoundITypeWriterOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterOuterCore} */
xyz.swapee.wc.BoundITypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.BoundTypeWriterOuterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterOuterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterOuterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterOuterCore} */
xyz.swapee.wc.BoundTypeWriterOuterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {!Array<string>} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.phrases

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.hold

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.holdClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.holdClear

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.enterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.enterDelay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.eraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.eraseDelay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.currentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {string} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.currentPhrase

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.nextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {string} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.nextPhrase

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.nextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {number} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.nextIndex

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases = function() {}
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase = function() {}
/** @type {string|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex = function() {}
/** @type {number|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterOuterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model} */
xyz.swapee.wc.ITypeWriterOuterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex = function() {}
/** @type {(*)|undefined} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {engineering.type.mvc.IParametric.Initialese}
 */
$xyz.swapee.wc.ITypeWriterPort.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Initialese} */
xyz.swapee.wc.ITypeWriterPort.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPortFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @interface */
$xyz.swapee.wc.ITypeWriterPortFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */
$xyz.swapee.wc.ITypeWriterPortFields.prototype.inputs
/** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */
$xyz.swapee.wc.ITypeWriterPortFields.prototype.props
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPortFields}
 */
xyz.swapee.wc.ITypeWriterPortFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPortCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @interface */
$xyz.swapee.wc.ITypeWriterPortCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterPort} */
$xyz.swapee.wc.ITypeWriterPortCaster.prototype.asITypeWriterPort
/** @type {!xyz.swapee.wc.BoundTypeWriterPort} */
$xyz.swapee.wc.ITypeWriterPortCaster.prototype.superTypeWriterPort
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPortCaster}
 */
xyz.swapee.wc.ITypeWriterPortCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterPortFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterPortCaster}
 * @extends {engineering.type.mvc.IParametric<!xyz.swapee.wc.ITypeWriterPort.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterPort.prototype.resetPort = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterPort.prototype.resetTypeWriterPort = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterPort}
 */
xyz.swapee.wc.ITypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.TypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterPort}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterPort.Initialese>}
 */
$xyz.swapee.wc.TypeWriterPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.TypeWriterPort
/** @type {function(new: xyz.swapee.wc.ITypeWriterPort, ...!xyz.swapee.wc.ITypeWriterPort.Initialese)} */
xyz.swapee.wc.TypeWriterPort.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.TypeWriterPort.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.AbstractTypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterPort}
 */
$xyz.swapee.wc.AbstractTypeWriterPort = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterPort)} */
xyz.swapee.wc.AbstractTypeWriterPort.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterPort.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.TypeWriterPortConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterPort, ...!xyz.swapee.wc.ITypeWriterPort.Initialese)} */
xyz.swapee.wc.TypeWriterPortConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterInputsPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @interface
 * @extends {xyz.swapee.wc.TypeWriterMemoryPQs}
 */
$xyz.swapee.wc.TypeWriterInputsPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterInputsPQs}
 */
xyz.swapee.wc.TypeWriterInputsPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterInputsQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterMemoryPQs}
 * @dict
 */
$xyz.swapee.wc.TypeWriterInputsQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterInputsQPs}
 */
xyz.swapee.wc.TypeWriterInputsQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterInputsQPs)} */
xyz.swapee.wc.TypeWriterInputsQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.RecordITypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITypeWriterPort.resetPort, resetTypeWriterPort: xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort }} */
xyz.swapee.wc.RecordITypeWriterPort

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.BoundITypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPortFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterPort}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterPortCaster}
 * @extends {engineering.type.mvc.BoundIParametric<!xyz.swapee.wc.ITypeWriterPort.Inputs>}
 */
$xyz.swapee.wc.BoundITypeWriterPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterPort} */
xyz.swapee.wc.BoundITypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.BoundTypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterPort}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterPort = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterPort} */
xyz.swapee.wc.BoundTypeWriterPort

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterPort.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterPort.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterPort): void} */
xyz.swapee.wc.ITypeWriterPort._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterPort.__resetPort} */
xyz.swapee.wc.ITypeWriterPort.__resetPort

// nss:xyz.swapee.wc.ITypeWriterPort,$xyz.swapee.wc.ITypeWriterPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterPort): void} */
xyz.swapee.wc.ITypeWriterPort._resetTypeWriterPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort} */
xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort

// nss:xyz.swapee.wc.ITypeWriterPort,$xyz.swapee.wc.ITypeWriterPort,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterPort.Inputs}
 */
xyz.swapee.wc.ITypeWriterPort.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 87fa25fe477929cc9e2a3834cbe15081 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.ITypeWriterPort.WeakInputs}
 */
xyz.swapee.wc.ITypeWriterPort.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @record */
$xyz.swapee.wc.ITypeWriterCore.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Initialese} */
xyz.swapee.wc.ITypeWriterCore.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCoreFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @interface */
$xyz.swapee.wc.ITypeWriterCoreFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterCore.Model} */
$xyz.swapee.wc.ITypeWriterCoreFields.prototype.model
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCoreFields}
 */
xyz.swapee.wc.ITypeWriterCoreFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCoreCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @interface */
$xyz.swapee.wc.ITypeWriterCoreCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterCore} */
$xyz.swapee.wc.ITypeWriterCoreCaster.prototype.asITypeWriterCore
/** @type {!xyz.swapee.wc.BoundTypeWriterCore} */
$xyz.swapee.wc.ITypeWriterCoreCaster.prototype.superTypeWriterCore
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCoreCaster}
 */
xyz.swapee.wc.ITypeWriterCoreCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterCoreFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCoreCaster}
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore}
 */
$xyz.swapee.wc.ITypeWriterCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterCore.prototype.resetCore = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterCore.prototype.resetTypeWriterCore = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCore}
 */
xyz.swapee.wc.ITypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.TypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterCore}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterCore.Initialese>}
 */
$xyz.swapee.wc.TypeWriterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.TypeWriterCore
/** @type {function(new: xyz.swapee.wc.ITypeWriterCore)} */
xyz.swapee.wc.TypeWriterCore.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.TypeWriterCore.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.AbstractTypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @constructor
 * @extends {xyz.swapee.wc.TypeWriterCore}
 */
$xyz.swapee.wc.AbstractTypeWriterCore = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.AbstractTypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterCore)} */
xyz.swapee.wc.AbstractTypeWriterCore.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterCore.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.RecordITypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/** @typedef {{ resetCore: xyz.swapee.wc.ITypeWriterCore.resetCore, resetTypeWriterCore: xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore }} */
xyz.swapee.wc.RecordITypeWriterCore

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.BoundITypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterCoreFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterCore}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCoreCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterOuterCore}
 */
$xyz.swapee.wc.BoundITypeWriterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterCore} */
xyz.swapee.wc.BoundITypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.BoundTypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterCore}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterCore = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterCore} */
xyz.swapee.wc.BoundTypeWriterCore

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.resetCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterCore.__resetCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterCore.resetCore
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterCore): void} */
xyz.swapee.wc.ITypeWriterCore._resetCore
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterCore.__resetCore} */
xyz.swapee.wc.ITypeWriterCore.__resetCore

// nss:xyz.swapee.wc.ITypeWriterCore,$xyz.swapee.wc.ITypeWriterCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterCore): void} */
xyz.swapee.wc.ITypeWriterCore._resetTypeWriterCore
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore} */
xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore

// nss:xyz.swapee.wc.ITypeWriterCore,$xyz.swapee.wc.ITypeWriterCore,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 26a0f3819250330848b27b8be65e84cd */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model}
 */
$xyz.swapee.wc.ITypeWriterCore.Model = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model} */
xyz.swapee.wc.ITypeWriterCore.Model

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 */
$xyz.swapee.wc.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.Initialese} */
xyz.swapee.wc.ITypeWriterController.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessor.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.ITypeWriterProcessor.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterProcessor.Initialese} */
xyz.swapee.wc.ITypeWriterProcessor.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterProcessor
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessorCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @interface */
$xyz.swapee.wc.ITypeWriterProcessorCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.asITypeWriterProcessor
/** @type {!xyz.swapee.wc.BoundTypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.superTypeWriterProcessor
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterProcessorCaster}
 */
xyz.swapee.wc.ITypeWriterProcessorCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterControllerFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @interface */
$xyz.swapee.wc.ITypeWriterControllerFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterController.Inputs} */
$xyz.swapee.wc.ITypeWriterControllerFields.prototype.inputs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterControllerFields}
 */
xyz.swapee.wc.ITypeWriterControllerFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @interface */
$xyz.swapee.wc.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterController} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.BoundITypeWriterProcessor} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterProcessor
/** @type {!xyz.swapee.wc.BoundTypeWriterController} */
$xyz.swapee.wc.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.ITypeWriterControllerCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterControllerFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterControllerCaster}
 * @extends {com.webcircuits.IPort<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.IBuffer<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterController.WeakInputs>}
 * @extends {engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.TypeWriterMemory>}
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterController = function() {}
/** @return {void} */
$xyz.swapee.wc.ITypeWriterController.prototype.resetPort = function() {}
/** @return {?} */
$xyz.swapee.wc.ITypeWriterController.prototype.next = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterController}
 */
xyz.swapee.wc.ITypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.ITypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterProcessorCaster}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {xyz.swapee.wc.ITypeWriterCore}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 */
$xyz.swapee.wc.ITypeWriterProcessor = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterProcessor}
 */
xyz.swapee.wc.ITypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.TypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterProcessor}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterProcessor.Initialese>}
 */
$xyz.swapee.wc.TypeWriterProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.TypeWriterProcessor
/** @type {function(new: xyz.swapee.wc.ITypeWriterProcessor, ...!xyz.swapee.wc.ITypeWriterProcessor.Initialese)} */
xyz.swapee.wc.TypeWriterProcessor.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.TypeWriterProcessor.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.AbstractTypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterProcessor}
 */
$xyz.swapee.wc.AbstractTypeWriterProcessor = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterProcessor)} */
xyz.swapee.wc.AbstractTypeWriterProcessor.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.TypeWriterProcessorConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterProcessor, ...!xyz.swapee.wc.ITypeWriterProcessor.Initialese)} */
xyz.swapee.wc.TypeWriterProcessorConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.RecordITypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.RecordITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @typedef {{ resetPort: xyz.swapee.wc.ITypeWriterController.resetPort, next: xyz.swapee.wc.ITypeWriterController.next }} */
xyz.swapee.wc.RecordITypeWriterController

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.BoundITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterControllerFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterControllerCaster}
 * @extends {com.webcircuits.BoundIPort<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>}
 * @extends {com.webcircuits.BoundIBuffer<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>}
 * @extends {engineering.type.mvc.BoundITransformer<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterController.WeakInputs>}
 * @extends {engineering.type.mvc.BoundIIntegratedController<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.TypeWriterMemory>}
 * @extends {engineering.type.mvc.BoundIRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {engineering.type.mvc.BoundICalibrator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterController} */
xyz.swapee.wc.BoundITypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.BoundITypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterProcessor}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterProcessorCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {xyz.swapee.wc.BoundITypeWriterCore}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 */
$xyz.swapee.wc.BoundITypeWriterProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterProcessor} */
xyz.swapee.wc.BoundITypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml} xyz.swapee.wc.BoundTypeWriterProcessor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props f4cd0e214e33419ef02bdb94ba8c7388 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterProcessor = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterProcessor} */
xyz.swapee.wc.BoundTypeWriterProcessor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/100-TypeWriterMemory.xml} xyz.swapee.wc.TypeWriterMemory exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0af58cb1ef41987cf32400f70830dc92 */
/** @record */
$xyz.swapee.wc.TypeWriterMemory = __$te_Mixin()
/** @type {!Array<string>} */
$xyz.swapee.wc.TypeWriterMemory.prototype.phrases
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.hold
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.holdClear
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.enterDelay
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.eraseDelay
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemory.prototype.currentPhrase
/** @type {string} */
$xyz.swapee.wc.TypeWriterMemory.prototype.nextPhrase
/** @type {number} */
$xyz.swapee.wc.TypeWriterMemory.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TypeWriterMemory}
 */
xyz.swapee.wc.TypeWriterMemory

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/102-TypeWriterInputs.xml} xyz.swapee.wc.front.TypeWriterInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 0fd59a7318f455dbb1981204b63e7e44 */
/** @record */
$xyz.swapee.wc.front.TypeWriterInputs = __$te_Mixin()
/** @type {(!Array<string>)|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.phrases
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.hold
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.holdClear
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.enterDelay
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.eraseDelay
/** @type {string|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.currentPhrase
/** @type {string|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.nextPhrase
/** @type {number|undefined} */
$xyz.swapee.wc.front.TypeWriterInputs.prototype.nextIndex
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.front.TypeWriterInputs}
 */
xyz.swapee.wc.front.TypeWriterInputs

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterEnv exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @record */
$xyz.swapee.wc.TypeWriterEnv = __$te_Mixin()
/** @type {xyz.swapee.wc.ITypeWriter} */
$xyz.swapee.wc.TypeWriterEnv.prototype.typeWriter
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TypeWriterEnv}
 */
xyz.swapee.wc.TypeWriterEnv

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.ITypeWriter.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriter.Initialese} */
xyz.swapee.wc.ITypeWriter.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @interface */
$xyz.swapee.wc.ITypeWriterFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriter.Pinout} */
$xyz.swapee.wc.ITypeWriterFields.prototype.pinout
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterFields}
 */
xyz.swapee.wc.ITypeWriterFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @interface */
$xyz.swapee.wc.ITypeWriterCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriter} */
$xyz.swapee.wc.ITypeWriterCaster.prototype.asITypeWriter
/** @type {!xyz.swapee.wc.BoundTypeWriter} */
$xyz.swapee.wc.ITypeWriterCaster.prototype.superTypeWriter
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterCaster}
 */
xyz.swapee.wc.ITypeWriterCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCaster}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 * @extends {guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, null>}
 */
$xyz.swapee.wc.ITypeWriter = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriter}
 */
xyz.swapee.wc.ITypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriter}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriter.Initialese>}
 */
$xyz.swapee.wc.TypeWriter = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.TypeWriter
/** @type {function(new: xyz.swapee.wc.ITypeWriter, ...!xyz.swapee.wc.ITypeWriter.Initialese)} */
xyz.swapee.wc.TypeWriter.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.TypeWriter.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.AbstractTypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriter}
 */
$xyz.swapee.wc.AbstractTypeWriter = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriter)} */
xyz.swapee.wc.AbstractTypeWriter.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriter.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriter, ...!xyz.swapee.wc.ITypeWriter.Initialese)} */
xyz.swapee.wc.TypeWriterConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.MVCOptions exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @record */
$xyz.swapee.wc.ITypeWriter.MVCOptions = function() {}
/** @type {(!xyz.swapee.wc.ITypeWriter.Pinout)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.props
/** @type {(!xyz.swapee.wc.ITypeWriter.Pinout)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.derivedProps
/** @type {!xyz.swapee.wc.ITypeWriter.Pinout} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.circuits
/** @type {(!xyz.swapee.wc.TypeWriterMemory)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.state
/** @type {(!xyz.swapee.wc.TypeWriterClasses)|undefined} */
$xyz.swapee.wc.ITypeWriter.MVCOptions.prototype.classes
/** @typedef {$xyz.swapee.wc.ITypeWriter.MVCOptions} */
xyz.swapee.wc.ITypeWriter.MVCOptions

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.RecordITypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriter

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.BoundITypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterFields}
 * @extends {xyz.swapee.wc.RecordITypeWriter}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {guest.maurice.BoundIIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, null>}
 */
$xyz.swapee.wc.BoundITypeWriter = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriter} */
xyz.swapee.wc.BoundITypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.BoundTypeWriter exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriter}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriter = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriter} */
xyz.swapee.wc.BoundTypeWriter

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.Inputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPort.Inputs}
 */
$xyz.swapee.wc.ITypeWriterController.Inputs = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.Inputs} */
xyz.swapee.wc.ITypeWriterController.Inputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriter.Pinout exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterController.Inputs}
 */
$xyz.swapee.wc.ITypeWriter.Pinout = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriter.Pinout} */
xyz.swapee.wc.ITypeWriter.Pinout

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriter
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.ITypeWriterBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @interface
 * @extends {engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.ITypeWriterBuffer = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterBuffer}
 */
xyz.swapee.wc.ITypeWriterBuffer

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml} xyz.swapee.wc.TypeWriterBuffer exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterBuffer}
 */
$xyz.swapee.wc.TypeWriterBuffer = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterBuffer}
 */
xyz.swapee.wc.TypeWriterBuffer
/** @type {function(new: xyz.swapee.wc.ITypeWriterBuffer)} */
xyz.swapee.wc.TypeWriterBuffer.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPU.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplay.Initialese}
 */
$xyz.swapee.wc.ITypeWriterGPU.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterGPU.Initialese} */
xyz.swapee.wc.ITypeWriterGPU.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterGPU
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterController.Initialese}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreen.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriter.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterGPU.Initialese}
 * @extends {com.webcircuits.IHtmlComponent.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterComputer.Initialese}
 * @extends {ITypeWriterGenerator.Initialese}
 */
$xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} */
xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterHtmlComponent
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponentCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @interface */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterHtmlComponent} */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.asITypeWriterHtmlComponent
/** @type {!xyz.swapee.wc.BoundTypeWriterHtmlComponent} */
$xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.superTypeWriterHtmlComponent
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 */
xyz.swapee.wc.ITypeWriterHtmlComponentCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPUFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITypeWriterGPUFields = function() {}
/** @type {!Object<string, string>} */
$xyz.swapee.wc.ITypeWriterGPUFields.prototype.vdusPQs
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPUFields}
 */
xyz.swapee.wc.ITypeWriterGPUFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPUCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @interface */
$xyz.swapee.wc.ITypeWriterGPUCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterGPU} */
$xyz.swapee.wc.ITypeWriterGPUCaster.prototype.asITypeWriterGPU
/** @type {!xyz.swapee.wc.BoundTypeWriterGPU} */
$xyz.swapee.wc.ITypeWriterGPUCaster.prototype.superTypeWriterGPU
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPUCaster}
 */
xyz.swapee.wc.ITypeWriterGPUCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.ITypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterGPUFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterGPUCaster}
 * @extends {com.webcircuits.IBrowserView<.!TypeWriterMemory,>}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplay}
 */
$xyz.swapee.wc.ITypeWriterGPU = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterGPU}
 */
xyz.swapee.wc.ITypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.ITypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.ITypeWriterController}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreen}
 * @extends {xyz.swapee.wc.ITypeWriter}
 * @extends {xyz.swapee.wc.ITypeWriterGPU}
 * @extends {com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.ITypeWriterProcessor}
 * @extends {xyz.swapee.wc.ITypeWriterComputer}
 * @extends {ITypeWriterGenerator}
 */
$xyz.swapee.wc.ITypeWriterHtmlComponent = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterHtmlComponent}
 */
xyz.swapee.wc.ITypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.TypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterHtmlComponent}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese>}
 */
$xyz.swapee.wc.TypeWriterHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.TypeWriterHtmlComponent
/** @type {function(new: xyz.swapee.wc.ITypeWriterHtmlComponent, ...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese)} */
xyz.swapee.wc.TypeWriterHtmlComponent.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.TypeWriterHtmlComponent.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.AbstractTypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterHtmlComponent}
 */
$xyz.swapee.wc.AbstractTypeWriterHtmlComponent = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterHtmlComponent)} */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.TypeWriterHtmlComponentConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterHtmlComponent, ...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese)} */
xyz.swapee.wc.TypeWriterHtmlComponentConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.RecordITypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.RecordITypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterGPU

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.BoundITypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterGPUFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterGPU}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterGPUCaster}
 * @extends {com.webcircuits.BoundIBrowserView<.!TypeWriterMemory,>}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterDisplay}
 */
$xyz.swapee.wc.BoundITypeWriterGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterGPU} */
xyz.swapee.wc.BoundITypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.BoundITypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterHtmlComponent}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterHtmlComponentCaster}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterController}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreen}
 * @extends {xyz.swapee.wc.BoundITypeWriter}
 * @extends {xyz.swapee.wc.BoundITypeWriterGPU}
 * @extends {com.webcircuits.BoundIHtmlComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, !HTMLDivElement, null>}
 * @extends {xyz.swapee.wc.BoundITypeWriterProcessor}
 * @extends {xyz.swapee.wc.BoundITypeWriterComputer}
 * @extends {BoundITypeWriterGenerator}
 */
$xyz.swapee.wc.BoundITypeWriterHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterHtmlComponent} */
xyz.swapee.wc.BoundITypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml} xyz.swapee.wc.BoundTypeWriterHtmlComponent exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fe7841b5a15614646529e0db6279ff50 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterHtmlComponent}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterHtmlComponent = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterHtmlComponent} */
xyz.swapee.wc.BoundTypeWriterHtmlComponent

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml} xyz.swapee.wc.ITypeWriterDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5fb3afc6b796b5cbb194e5b6deb8fc11 */
/** @interface */
$xyz.swapee.wc.ITypeWriterDesigner = function() {}
/**
 * @param {xyz.swapee.wc.TypeWriterClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITypeWriterDesigner.prototype.borrowClasses = function(classes) {}
/**
 * @param {xyz.swapee.wc.TypeWriterClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITypeWriterDesigner.prototype.classes = function(classes) {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh} mesh
 * @return {?}
 */
$xyz.swapee.wc.ITypeWriterDesigner.prototype.communicator = function(mesh) {}
/**
 * @param {!xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh} mesh
 * @param {!xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool} memPool
 * @return {?}
 */
$xyz.swapee.wc.ITypeWriterDesigner.prototype.relay = function(mesh, memPool) {}
/**
 * @param {xyz.swapee.wc.TypeWriterClasses} classes
 * @return {!engineering.type.VNode}
 */
$xyz.swapee.wc.ITypeWriterDesigner.prototype.lendClasses = function(classes) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDesigner}
 */
xyz.swapee.wc.ITypeWriterDesigner

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml} xyz.swapee.wc.TypeWriterDesigner exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5fb3afc6b796b5cbb194e5b6deb8fc11 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.ITypeWriterDesigner}
 */
$xyz.swapee.wc.TypeWriterDesigner = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterDesigner}
 */
xyz.swapee.wc.TypeWriterDesigner
/** @type {function(new: xyz.swapee.wc.ITypeWriterDesigner)} */
xyz.swapee.wc.TypeWriterDesigner.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml} xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5fb3afc6b796b5cbb194e5b6deb8fc11 */
/** @record */
$xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITypeWriterController} */
$xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh.prototype.TypeWriter
/** @typedef {$xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh} */
xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDesigner.communicator
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml} xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5fb3afc6b796b5cbb194e5b6deb8fc11 */
/** @record */
$xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh = function() {}
/** @type {typeof xyz.swapee.wc.ITypeWriterController} */
$xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh.prototype.TypeWriter
/** @type {typeof xyz.swapee.wc.ITypeWriterController} */
$xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh.prototype.This
/** @typedef {$xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh} */
xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml} xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 5fb3afc6b796b5cbb194e5b6deb8fc11 */
/** @record */
$xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool = function() {}
/** @type {!xyz.swapee.wc.TypeWriterMemory} */
$xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool.prototype.TypeWriter
/** @type {!xyz.swapee.wc.TypeWriterMemory} */
$xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool.prototype.This
/** @typedef {$xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool} */
xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDesigner.relay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings>}
 */
$xyz.swapee.wc.ITypeWriterDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Initialese} */
xyz.swapee.wc.ITypeWriterDisplay.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplayFields exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @interface */
$xyz.swapee.wc.ITypeWriterDisplayFields = function() {}
/** @type {!xyz.swapee.wc.ITypeWriterDisplay.Settings} */
$xyz.swapee.wc.ITypeWriterDisplayFields.prototype.settings
/** @type {!xyz.swapee.wc.ITypeWriterDisplay.Queries} */
$xyz.swapee.wc.ITypeWriterDisplayFields.prototype.queries
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplayFields}
 */
xyz.swapee.wc.ITypeWriterDisplayFields

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @interface */
$xyz.swapee.wc.ITypeWriterDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterDisplay} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/** @type {!xyz.swapee.wc.BoundITypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.BoundTypeWriterDisplay} */
$xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplayCaster}
 */
xyz.swapee.wc.ITypeWriterDisplayCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @interface
 * @extends {xyz.swapee.wc.ITypeWriterDisplayFields}
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.IDisplay<!xyz.swapee.wc.TypeWriterMemory, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 */
$xyz.swapee.wc.ITypeWriterDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterDisplay}
 */
xyz.swapee.wc.ITypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.TypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterDisplay.Initialese>}
 */
$xyz.swapee.wc.TypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.TypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.ITypeWriterDisplay, ...!xyz.swapee.wc.ITypeWriterDisplay.Initialese)} */
xyz.swapee.wc.TypeWriterDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.TypeWriterDisplay.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.AbstractTypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterDisplay}
 */
$xyz.swapee.wc.AbstractTypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterDisplay)} */
xyz.swapee.wc.AbstractTypeWriterDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.TypeWriterDisplayConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterDisplay, ...!xyz.swapee.wc.ITypeWriterDisplay.Initialese)} */
xyz.swapee.wc.TypeWriterDisplayConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.TypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterGPU}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterGPU.Initialese>}
 */
$xyz.swapee.wc.TypeWriterGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.TypeWriterGPU
/** @type {function(new: xyz.swapee.wc.ITypeWriterGPU, ...!xyz.swapee.wc.ITypeWriterGPU.Initialese)} */
xyz.swapee.wc.TypeWriterGPU.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.TypeWriterGPU.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.AbstractTypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterGPU}
 */
$xyz.swapee.wc.AbstractTypeWriterGPU = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterGPU)} */
xyz.swapee.wc.AbstractTypeWriterGPU.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterGPU.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.TypeWriterGPUConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterGPU, ...!xyz.swapee.wc.ITypeWriterGPU.Initialese)} */
xyz.swapee.wc.TypeWriterGPUConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml} xyz.swapee.wc.BoundTypeWriterGPU exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props c88ba726d320e23b2553b67f4b1f92fc */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterGPU}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterGPU = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterGPU} */
xyz.swapee.wc.BoundTypeWriterGPU

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.RecordITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @typedef {{ paint: xyz.swapee.wc.ITypeWriterDisplay.paint }} */
xyz.swapee.wc.RecordITypeWriterDisplay

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.BoundITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterDisplayFields}
 * @extends {xyz.swapee.wc.RecordITypeWriterDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.BoundIDisplay<!xyz.swapee.wc.TypeWriterMemory, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 */
$xyz.swapee.wc.BoundITypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterDisplay} */
xyz.swapee.wc.BoundITypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.BoundTypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterDisplay} */
xyz.swapee.wc.BoundTypeWriterDisplay

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory
 * @param {null} land
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory, null): void} */
xyz.swapee.wc.ITypeWriterDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterDisplay, !xyz.swapee.wc.TypeWriterMemory, null): void} */
xyz.swapee.wc.ITypeWriterDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterDisplay.__paint} */
xyz.swapee.wc.ITypeWriterDisplay.__paint

// nss:xyz.swapee.wc.ITypeWriterDisplay,$xyz.swapee.wc.ITypeWriterDisplay,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Queries exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/** @record */
$xyz.swapee.wc.ITypeWriterDisplay.Queries = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Queries} */
xyz.swapee.wc.ITypeWriterDisplay.Queries

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml} xyz.swapee.wc.ITypeWriterDisplay.Settings exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 40bd1afc1fdbac94736e0f97e2657222 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterDisplay.Queries}
 */
$xyz.swapee.wc.ITypeWriterDisplay.Settings = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterDisplay.Settings} */
xyz.swapee.wc.ITypeWriterDisplay.Settings

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TypeWriterClasses>}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterDisplay.Initialese} */
xyz.swapee.wc.back.ITypeWriterDisplay.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterDisplay
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplayCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterDisplay} */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/** @type {!xyz.swapee.wc.back.BoundTypeWriterDisplay} */
$xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 */
xyz.swapee.wc.back.ITypeWriterDisplayCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.TypeWriterClasses, null>}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay = function() {}
/**
 * @param {!xyz.swapee.wc.TypeWriterMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.prototype.paint = function(memory, land) {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterDisplay}
 */
xyz.swapee.wc.back.ITypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.TypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @constructor
 * @implements {xyz.swapee.wc.back.ITypeWriterDisplay}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterDisplay.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.TypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterDisplay)} */
xyz.swapee.wc.back.TypeWriterDisplay.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.TypeWriterDisplay.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.AbstractTypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @constructor
 * @extends {xyz.swapee.wc.back.TypeWriterDisplay}
 */
$xyz.swapee.wc.back.AbstractTypeWriterDisplay = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterDisplay)} */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterVdusPQs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/** @interface */
$xyz.swapee.wc.TypeWriterVdusPQs = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.TypeWriterVdusPQs}
 */
xyz.swapee.wc.TypeWriterVdusPQs

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml} xyz.swapee.wc.TypeWriterVdusQPs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * @constructor
 * @dict
 */
$xyz.swapee.wc.TypeWriterVdusQPs = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {$xyz.swapee.wc.TypeWriterVdusQPs}
 */
xyz.swapee.wc.TypeWriterVdusQPs
/** @type {function(new: xyz.swapee.wc.TypeWriterVdusQPs)} */
xyz.swapee.wc.TypeWriterVdusQPs.prototype.constructor

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.RecordITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/** @typedef {{ paint: xyz.swapee.wc.back.ITypeWriterDisplay.paint }} */
xyz.swapee.wc.back.RecordITypeWriterDisplay

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.BoundITypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterDisplay}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterDisplayCaster}
 * @extends {com.webcircuits.BoundIGraphicsDriverBack<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.TypeWriterClasses, null>}
 */
$xyz.swapee.wc.back.BoundITypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterDisplay} */
xyz.swapee.wc.back.BoundITypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.BoundTypeWriterDisplay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterDisplay}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterDisplay = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterDisplay} */
xyz.swapee.wc.back.BoundTypeWriterDisplay

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml} xyz.swapee.wc.back.ITypeWriterDisplay.paint exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props a1bee4e620d54cbc8498a16ed3579499 */
/**
 * @this {THIS}
 * @template THIS
 * @param {!xyz.swapee.wc.TypeWriterMemory} [memory]
 * @param {null} [land]
 * @return {void}
 */
$xyz.swapee.wc.back.ITypeWriterDisplay.__paint = function(memory, land) {}
/** @typedef {function(!xyz.swapee.wc.TypeWriterMemory=, null=): void} */
xyz.swapee.wc.back.ITypeWriterDisplay.paint
/** @typedef {function(this: xyz.swapee.wc.back.ITypeWriterDisplay, !xyz.swapee.wc.TypeWriterMemory=, null=): void} */
xyz.swapee.wc.back.ITypeWriterDisplay._paint
/** @typedef {typeof $xyz.swapee.wc.back.ITypeWriterDisplay.__paint} */
xyz.swapee.wc.back.ITypeWriterDisplay.__paint

// nss:xyz.swapee.wc.back.ITypeWriterDisplay,$xyz.swapee.wc.back.ITypeWriterDisplay,xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/41-TypeWriterClasses.xml} xyz.swapee.wc.TypeWriterClasses exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props adf81165d8f25289620940b0c1efa27e */
/** @record */
$xyz.swapee.wc.TypeWriterClasses = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @record
 * @extends {$xyz.swapee.wc.TypeWriterClasses}
 */
xyz.swapee.wc.TypeWriterClasses

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.TypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.TypeWriterController
/** @type {function(new: xyz.swapee.wc.ITypeWriterController, ...!xyz.swapee.wc.ITypeWriterController.Initialese)} */
xyz.swapee.wc.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.TypeWriterController.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.AbstractTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterController}
 */
$xyz.swapee.wc.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterController)} */
xyz.swapee.wc.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.TypeWriterControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterController, ...!xyz.swapee.wc.ITypeWriterController.Initialese)} */
xyz.swapee.wc.TypeWriterControllerConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.BoundTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterController} */
xyz.swapee.wc.BoundTypeWriterController

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.resetPort exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @this {THIS}
 * @template THIS
 * @return {void}
 */
$xyz.swapee.wc.ITypeWriterController.__resetPort = function() {}
/** @typedef {function(): void} */
xyz.swapee.wc.ITypeWriterController.resetPort
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterController): void} */
xyz.swapee.wc.ITypeWriterController._resetPort
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterController.__resetPort} */
xyz.swapee.wc.ITypeWriterController.__resetPort

// nss:xyz.swapee.wc.ITypeWriterController,$xyz.swapee.wc.ITypeWriterController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.next exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.ITypeWriterController.__next = function() {}
/** @typedef {function()} */
xyz.swapee.wc.ITypeWriterController.next
/** @typedef {function(this: xyz.swapee.wc.ITypeWriterController)} */
xyz.swapee.wc.ITypeWriterController._next
/** @typedef {typeof $xyz.swapee.wc.ITypeWriterController.__next} */
xyz.swapee.wc.ITypeWriterController.__next

// nss:xyz.swapee.wc.ITypeWriterController,$xyz.swapee.wc.ITypeWriterController,xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml} xyz.swapee.wc.ITypeWriterController.WeakInputs exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 6113dd8a95fc3d2ed506eded47f91c46 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterPort.WeakInputs}
 */
$xyz.swapee.wc.ITypeWriterController.WeakInputs = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterController.WeakInputs} */
xyz.swapee.wc.ITypeWriterController.WeakInputs

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @record */
$xyz.swapee.wc.front.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterController.Initialese} */
xyz.swapee.wc.front.ITypeWriterController.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterController} */
$xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.front.BoundTypeWriterController} */
$xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.front.ITypeWriterControllerCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterControllerAT} */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.asITypeWriterControllerAT
/** @type {!xyz.swapee.wc.front.BoundTypeWriterControllerAT} */
$xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.superTypeWriterControllerAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 */
xyz.swapee.wc.front.ITypeWriterControllerATCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.front.ITypeWriterControllerAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterControllerAT}
 */
xyz.swapee.wc.front.ITypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerAT}
 */
$xyz.swapee.wc.front.ITypeWriterController = function() {}
/** @return {?} */
$xyz.swapee.wc.front.ITypeWriterController.prototype.next = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterController}
 */
xyz.swapee.wc.front.ITypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.TypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.TypeWriterController
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterController, ...!xyz.swapee.wc.front.ITypeWriterController.Initialese)} */
xyz.swapee.wc.front.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.TypeWriterController.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.AbstractTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterController}
 */
$xyz.swapee.wc.front.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterController)} */
xyz.swapee.wc.front.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.TypeWriterControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterController, ...!xyz.swapee.wc.front.ITypeWriterController.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.RecordITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/** @typedef {{ next: xyz.swapee.wc.front.ITypeWriterController.next }} */
xyz.swapee.wc.front.RecordITypeWriterController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.RecordITypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITypeWriterControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.BoundITypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterControllerAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.front.BoundITypeWriterControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterControllerAT} */
xyz.swapee.wc.front.BoundITypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.BoundITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.front.BoundITypeWriterControllerAT}
 */
$xyz.swapee.wc.front.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterController} */
xyz.swapee.wc.front.BoundITypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.BoundTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterController} */
xyz.swapee.wc.front.BoundTypeWriterController

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml} xyz.swapee.wc.front.ITypeWriterController.next exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 074c9fe7b0fc3a231bca6737670243a9 */
/**
 * @this {THIS}
 * @template THIS
 */
$xyz.swapee.wc.front.ITypeWriterController.__next = function() {}
/** @typedef {function()} */
xyz.swapee.wc.front.ITypeWriterController.next
/** @typedef {function(this: xyz.swapee.wc.front.ITypeWriterController)} */
xyz.swapee.wc.front.ITypeWriterController._next
/** @typedef {typeof $xyz.swapee.wc.front.ITypeWriterController.__next} */
xyz.swapee.wc.front.ITypeWriterController.__next

// nss:xyz.swapee.wc.front.ITypeWriterController,$xyz.swapee.wc.front.ITypeWriterController,xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterController.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterController.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterController.Initialese} */
xyz.swapee.wc.back.ITypeWriterController.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterController
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterControllerCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterControllerCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterController} */
$xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.asITypeWriterController
/** @type {!xyz.swapee.wc.back.BoundTypeWriterController} */
$xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.superTypeWriterController
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerCaster}
 */
xyz.swapee.wc.back.ITypeWriterControllerCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.ITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 * @extends {com.webcircuits.IDriverBack<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.back.ITypeWriterController = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterController}
 */
xyz.swapee.wc.back.ITypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.TypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterController}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterController.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.TypeWriterController
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterController, ...!xyz.swapee.wc.back.ITypeWriterController.Initialese)} */
xyz.swapee.wc.back.TypeWriterController.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.TypeWriterController.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.AbstractTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterController}
 */
$xyz.swapee.wc.back.AbstractTypeWriterController = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterController)} */
xyz.swapee.wc.back.AbstractTypeWriterController.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterController.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.TypeWriterControllerConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterController, ...!xyz.swapee.wc.back.ITypeWriterController.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.RecordITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.BoundITypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterController}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerCaster}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 * @extends {com.webcircuits.BoundIDriverBack<!xyz.swapee.wc.ITypeWriterController.Inputs>}
 */
$xyz.swapee.wc.back.BoundITypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterController} */
xyz.swapee.wc.back.BoundITypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml} xyz.swapee.wc.back.BoundTypeWriterController exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 408c1b42749abcf20ee27a918ea72768 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterController}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterController = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterController} */
xyz.swapee.wc.back.BoundTypeWriterController

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterController.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} */
xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterControllerAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterControllerAR} */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.asITypeWriterControllerAR
/** @type {!xyz.swapee.wc.back.BoundTypeWriterControllerAR} */
$xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.superTypeWriterControllerAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 */
xyz.swapee.wc.back.ITypeWriterControllerARCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.ITypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITypeWriterController}
 */
$xyz.swapee.wc.back.ITypeWriterControllerAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterControllerAR}
 */
xyz.swapee.wc.back.ITypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.TypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterControllerAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.TypeWriterControllerAR
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterControllerAR, ...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.TypeWriterControllerAR.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.AbstractTypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterControllerAR}
 */
$xyz.swapee.wc.back.AbstractTypeWriterControllerAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterControllerAR)} */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.TypeWriterControllerARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterControllerAR, ...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese)} */
xyz.swapee.wc.back.TypeWriterControllerARConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.RecordITypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.BoundITypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterControllerAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterControllerARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITypeWriterController}
 */
$xyz.swapee.wc.back.BoundITypeWriterControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterControllerAR} */
xyz.swapee.wc.back.BoundITypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml} xyz.swapee.wc.back.BoundTypeWriterControllerAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 946702a0b145eac225b3fb0e0b8c0dae */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterControllerAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterControllerAR = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterControllerAR} */
xyz.swapee.wc.back.BoundTypeWriterControllerAR

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} */
xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterControllerAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.TypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterControllerAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.TypeWriterControllerAT
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterControllerAT, ...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.TypeWriterControllerAT.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.AbstractTypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterControllerAT}
 */
$xyz.swapee.wc.front.AbstractTypeWriterControllerAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterControllerAT)} */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.TypeWriterControllerATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterControllerAT, ...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese)} */
xyz.swapee.wc.front.TypeWriterControllerATConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml} xyz.swapee.wc.front.BoundTypeWriterControllerAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d6e9748744c7dda41cb1aa197b458ce7 */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterControllerAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterControllerAT = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterControllerAT} */
xyz.swapee.wc.front.BoundTypeWriterControllerAT

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null>}
 * @extends {xyz.swapee.wc.ITypeWriterDisplay.Initialese}
 */
$xyz.swapee.wc.ITypeWriterScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterScreen.Initialese} */
xyz.swapee.wc.ITypeWriterScreen.Initialese

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @interface */
$xyz.swapee.wc.ITypeWriterScreenCaster = function() {}
/** @type {!xyz.swapee.wc.BoundITypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.BoundTypeWriterScreen} */
$xyz.swapee.wc.ITypeWriterScreenCaster.prototype.superTypeWriterScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterScreenCaster}
 */
xyz.swapee.wc.ITypeWriterScreenCaster

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.ITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterScreenCaster}
 * @extends {com.webcircuits.IScreen<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.ITypeWriterController}
 * @extends {xyz.swapee.wc.ITypeWriterDisplay}
 */
$xyz.swapee.wc.ITypeWriterScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.ITypeWriterScreen}
 */
xyz.swapee.wc.ITypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.TypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init
 * @implements {xyz.swapee.wc.ITypeWriterScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterScreen.Initialese>}
 */
$xyz.swapee.wc.TypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.TypeWriterScreen
/** @type {function(new: xyz.swapee.wc.ITypeWriterScreen, ...!xyz.swapee.wc.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.TypeWriterScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.TypeWriterScreen.__extend

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.AbstractTypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init
 * @extends {xyz.swapee.wc.TypeWriterScreen}
 */
$xyz.swapee.wc.AbstractTypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen
/** @type {function(new: xyz.swapee.wc.AbstractTypeWriterScreen)} */
xyz.swapee.wc.AbstractTypeWriterScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.continues
/**
 * @param {...((!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay))} Implementations
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__trait

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.TypeWriterScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @typedef {function(new: xyz.swapee.wc.ITypeWriterScreen, ...!xyz.swapee.wc.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.TypeWriterScreenConstructor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.RecordITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.RecordITypeWriterScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.BoundITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {xyz.swapee.wc.RecordITypeWriterScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.ITypeWriterScreenCaster}
 * @extends {com.webcircuits.BoundIScreen<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null, null>}
 * @extends {xyz.swapee.wc.front.BoundITypeWriterController}
 * @extends {xyz.swapee.wc.BoundITypeWriterDisplay}
 */
$xyz.swapee.wc.BoundITypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundITypeWriterScreen} */
xyz.swapee.wc.BoundITypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml} xyz.swapee.wc.BoundTypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props 14227cdb0561038ce2ee6c141192529b */
/**
 * @record
 * @extends {xyz.swapee.wc.BoundITypeWriterScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.BoundTypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.BoundTypeWriterScreen} */
xyz.swapee.wc.BoundTypeWriterScreen

// nss:xyz.swapee.wc,$xyz.swapee.wc
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {com.webcircuits.IAT.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} */
xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterScreenAT
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreen.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese}
 */
$xyz.swapee.wc.back.ITypeWriterScreen.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.back.ITypeWriterScreen.Initialese} */
xyz.swapee.wc.back.ITypeWriterScreen.Initialese

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back.ITypeWriterScreen
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreenCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterScreenCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterScreen} */
$xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/** @type {!xyz.swapee.wc.back.BoundTypeWriterScreen} */
$xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.superTypeWriterScreen
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenCaster}
 */
xyz.swapee.wc.back.ITypeWriterScreenCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenATCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @interface */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster = function() {}
/** @type {!xyz.swapee.wc.back.BoundITypeWriterScreenAT} */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.asITypeWriterScreenAT
/** @type {!xyz.swapee.wc.back.BoundTypeWriterScreenAT} */
$xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.superTypeWriterScreenAT
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 */
xyz.swapee.wc.back.ITypeWriterScreenATCaster

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.ITypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 * @extends {com.webcircuits.IAT}
 */
$xyz.swapee.wc.back.ITypeWriterScreenAT = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreenAT}
 */
xyz.swapee.wc.back.ITypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.ITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenCaster}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenAT}
 */
$xyz.swapee.wc.back.ITypeWriterScreen = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.back.ITypeWriterScreen}
 */
xyz.swapee.wc.back.ITypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.TypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterScreen}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreen.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.TypeWriterScreen
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterScreen, ...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreen.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.TypeWriterScreen.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.AbstractTypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterScreen}
 */
$xyz.swapee.wc.back.AbstractTypeWriterScreen = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterScreen)} */
xyz.swapee.wc.back.AbstractTypeWriterScreen.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.TypeWriterScreenConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreen, ...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.RecordITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.RecordITypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.back.RecordITypeWriterScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.BoundITypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterScreenAT}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenATCaster}
 * @extends {com.webcircuits.BoundIAT}
 */
$xyz.swapee.wc.back.BoundITypeWriterScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterScreenAT} */
xyz.swapee.wc.back.BoundITypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.BoundITypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.RecordITypeWriterScreen}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.back.ITypeWriterScreenCaster}
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreenAT}
 */
$xyz.swapee.wc.back.BoundITypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundITypeWriterScreen} */
xyz.swapee.wc.back.BoundITypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml} xyz.swapee.wc.back.BoundTypeWriterScreen exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props ab35938b8e9911e6e67c3e1677580cbc */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreen}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterScreen = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterScreen} */
xyz.swapee.wc.back.BoundTypeWriterScreen

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {com.webcircuits.IAR.Initialese}
 * @extends {xyz.swapee.wc.ITypeWriterScreen.Initialese}
 */
$xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese = function() {}
/** @typedef {$xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} */
xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front.ITypeWriterScreenAR
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenARCaster exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @interface */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster = function() {}
/** @type {!xyz.swapee.wc.front.BoundITypeWriterScreenAR} */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.asITypeWriterScreenAR
/** @type {!xyz.swapee.wc.front.BoundTypeWriterScreenAR} */
$xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.superTypeWriterScreenAR
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 */
xyz.swapee.wc.front.ITypeWriterScreenARCaster

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.ITypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @interface
 * @extends {engineering.type.IEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 * @extends {com.webcircuits.IAR}
 * @extends {xyz.swapee.wc.ITypeWriterScreen}
 */
$xyz.swapee.wc.front.ITypeWriterScreenAR = function() {}
/**
 * @suppress {checkTypes}
 * @interface
 * @extends {$xyz.swapee.wc.front.ITypeWriterScreenAR}
 */
xyz.swapee.wc.front.ITypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.TypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init
 * @implements {xyz.swapee.wc.front.ITypeWriterScreenAR}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese>}
 */
$xyz.swapee.wc.front.TypeWriterScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.TypeWriterScreenAR
/** @type {function(new: xyz.swapee.wc.front.ITypeWriterScreenAR, ...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese)} */
xyz.swapee.wc.front.TypeWriterScreenAR.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.TypeWriterScreenAR.__extend

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.AbstractTypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init
 * @extends {xyz.swapee.wc.front.TypeWriterScreenAR}
 */
$xyz.swapee.wc.front.AbstractTypeWriterScreenAR = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR
/** @type {function(new: xyz.swapee.wc.front.AbstractTypeWriterScreenAR)} */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__extend
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.continues
/**
 * @param {...((!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen))} Implementations
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__trait

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.TypeWriterScreenARConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterScreenAR, ...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese)} */
xyz.swapee.wc.front.TypeWriterScreenARConstructor

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.RecordITypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/** @typedef {typeof __$te_plain} */
xyz.swapee.wc.front.RecordITypeWriterScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.BoundITypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {xyz.swapee.wc.front.RecordITypeWriterScreenAR}
 * @extends {engineering.type.BoundIEngineer}
 * @extends {xyz.swapee.wc.front.ITypeWriterScreenARCaster}
 * @extends {com.webcircuits.BoundIAR}
 * @extends {xyz.swapee.wc.BoundITypeWriterScreen}
 */
$xyz.swapee.wc.front.BoundITypeWriterScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundITypeWriterScreenAR} */
xyz.swapee.wc.front.BoundITypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml} xyz.swapee.wc.front.BoundTypeWriterScreenAR exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props fd9013389e7824cef26bde83de80dd3e */
/**
 * @record
 * @extends {xyz.swapee.wc.front.BoundITypeWriterScreenAR}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.front.BoundTypeWriterScreenAR = function() {}
/** @typedef {$xyz.swapee.wc.front.BoundTypeWriterScreenAR} */
xyz.swapee.wc.front.BoundTypeWriterScreenAR

// nss:xyz.swapee.wc.front,$xyz.swapee.wc.front
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.TypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init
 * @implements {xyz.swapee.wc.back.ITypeWriterScreenAT}
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese>}
 */
$xyz.swapee.wc.back.TypeWriterScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.TypeWriterScreenAT
/** @type {function(new: xyz.swapee.wc.back.ITypeWriterScreenAT, ...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenAT.prototype.constructor
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.TypeWriterScreenAT.__extend

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.AbstractTypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init
 * @extends {xyz.swapee.wc.back.TypeWriterScreenAT}
 */
$xyz.swapee.wc.back.AbstractTypeWriterScreenAT = __$te_Mixin()
/**
 * @suppress {checkTypes}
 * @constructor
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 * @extends {$xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT
/** @type {function(new: xyz.swapee.wc.back.AbstractTypeWriterScreenAT)} */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.prototype.constructor
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.implements
/**
 * @param {{ aspectsInstaller: (!Function|undefined), aspectsInstallers: (!Array|undefined) }} [data]
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.clone
/**
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__extend
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.continues
/**
 * @param {...((!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT))} Implementations
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__trait

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.TypeWriterScreenATConstructor exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreenAT, ...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese)} */
xyz.swapee.wc.back.TypeWriterScreenATConstructor

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml} xyz.swapee.wc.back.BoundTypeWriterScreenAT exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props d224c62115749f710a925a46a2fc6402 */
/**
 * @record
 * @extends {xyz.swapee.wc.back.BoundITypeWriterScreenAT}
 * @extends {engineering.type.BoundIInitialiser}
 */
$xyz.swapee.wc.back.BoundTypeWriterScreenAT = function() {}
/** @typedef {$xyz.swapee.wc.back.BoundTypeWriterScreenAT} */
xyz.swapee.wc.back.BoundTypeWriterScreenAT

// nss:xyz.swapee.wc.back,$xyz.swapee.wc.back
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe = function() {}
/** @type {number} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe = function() {}
/** @type {string} */
$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe.prototype.phrases
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe.prototype.hold
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe.prototype.holdClear
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe.prototype.enterDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe.prototype.eraseDelay
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe.prototype.currentPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe.prototype.nextPhrase
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @record */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe = function() {}
/** @type {*} */
$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe.prototype.nextIndex
/** @typedef {$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterOuterCore.WeakModel
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear} */
xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.Inputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe}
 */
$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe} */
xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterPort.WeakInputs
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Phrases exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Phrases = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Phrases} */
xyz.swapee.wc.ITypeWriterCore.Model.Phrases

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Hold exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Hold = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Hold} */
xyz.swapee.wc.ITypeWriterCore.Model.Hold

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear} */
xyz.swapee.wc.ITypeWriterCore.Model.HoldClear

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay} */
xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay} */
xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase} */
xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe} */
xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */
/* @typal-type {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex exclude:*Element.xml,*ElementPort.xml filter:!ControllerPlugin~props b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/**
 * @record
 * @extends {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex}
 */
$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex = function() {}
/** @typedef {$xyz.swapee.wc.ITypeWriterCore.Model.NextIndex} */
xyz.swapee.wc.ITypeWriterCore.Model.NextIndex

// nss:xyz.swapee.wc,$xyz.swapee.wc.ITypeWriterCore.Model
/* @typal-end */