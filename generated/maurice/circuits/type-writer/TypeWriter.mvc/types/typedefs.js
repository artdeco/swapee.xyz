/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.ITypeWriterComputer={}
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase={}
xyz.swapee.wc.ITypeWriterOuterCore={}
xyz.swapee.wc.ITypeWriterOuterCore.Model={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase={}
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex={}
xyz.swapee.wc.ITypeWriterOuterCore.WeakModel={}
xyz.swapee.wc.ITypeWriterPort={}
xyz.swapee.wc.ITypeWriterPort.Inputs={}
xyz.swapee.wc.ITypeWriterPort.WeakInputs={}
xyz.swapee.wc.ITypeWriterCore={}
xyz.swapee.wc.ITypeWriterCore.Model={}
xyz.swapee.wc.ITypeWriterProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.ITypeWriterController={}
xyz.swapee.wc.front.ITypeWriterControllerAT={}
xyz.swapee.wc.front.ITypeWriterScreenAR={}
xyz.swapee.wc.ITypeWriter={}
xyz.swapee.wc.ITypeWriterHtmlComponent={}
xyz.swapee.wc.ITypeWriterElement={}
xyz.swapee.wc.ITypeWriterElementPort={}
xyz.swapee.wc.ITypeWriterElementPort.Inputs={}
xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder={}
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs={}
xyz.swapee.wc.ITypeWriterDesigner={}
xyz.swapee.wc.ITypeWriterDesigner.communicator={}
xyz.swapee.wc.ITypeWriterDesigner.relay={}
xyz.swapee.wc.ITypeWriterDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.ITypeWriterDisplay={}
xyz.swapee.wc.back.ITypeWriterController={}
xyz.swapee.wc.back.ITypeWriterControllerAR={}
xyz.swapee.wc.back.ITypeWriterScreen={}
xyz.swapee.wc.back.ITypeWriterScreenAT={}
xyz.swapee.wc.ITypeWriterController={}
xyz.swapee.wc.ITypeWriterScreen={}
xyz.swapee.wc.ITypeWriterGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/02-ITypeWriterComputer.xml}  874baad0ed61be36f507fcce89d5bb15 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.ITypeWriterComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterComputer)} xyz.swapee.wc.AbstractTypeWriterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterComputer} xyz.swapee.wc.TypeWriterComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterComputer` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterComputer
 */
xyz.swapee.wc.AbstractTypeWriterComputer = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterComputer.constructor&xyz.swapee.wc.TypeWriterComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterComputer.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterComputer.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.AbstractTypeWriterComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterComputer.Initialese[]) => xyz.swapee.wc.ITypeWriterComputer} xyz.swapee.wc.TypeWriterComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.TypeWriterMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.ITypeWriterComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.ITypeWriterComputer
 */
xyz.swapee.wc.ITypeWriterComputer = class extends /** @type {xyz.swapee.wc.ITypeWriterComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase} */
xyz.swapee.wc.ITypeWriterComputer.prototype.adaptNextPhrase = function() {}
/** @type {xyz.swapee.wc.ITypeWriterComputer.compute} */
xyz.swapee.wc.ITypeWriterComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterComputer&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterComputer.Initialese>)} xyz.swapee.wc.TypeWriterComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterComputer} xyz.swapee.wc.ITypeWriterComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _ITypeWriterComputer_ instances.
 * @constructor xyz.swapee.wc.TypeWriterComputer
 * @implements {xyz.swapee.wc.ITypeWriterComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterComputer.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterComputer = class extends /** @type {xyz.swapee.wc.TypeWriterComputer.constructor&xyz.swapee.wc.ITypeWriterComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterComputer}
 */
xyz.swapee.wc.TypeWriterComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITypeWriterComputer} */
xyz.swapee.wc.RecordITypeWriterComputer

/** @typedef {xyz.swapee.wc.ITypeWriterComputer} xyz.swapee.wc.BoundITypeWriterComputer */

/** @typedef {xyz.swapee.wc.TypeWriterComputer} xyz.swapee.wc.BoundTypeWriterComputer */

/**
 * Contains getters to cast the _ITypeWriterComputer_ interface.
 * @interface xyz.swapee.wc.ITypeWriterComputerCaster
 */
xyz.swapee.wc.ITypeWriterComputerCaster = class { }
/**
 * Cast the _ITypeWriterComputer_ instance into the _BoundITypeWriterComputer_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterComputer}
 */
xyz.swapee.wc.ITypeWriterComputerCaster.prototype.asITypeWriterComputer
/**
 * Access the _TypeWriterComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterComputer}
 */
xyz.swapee.wc.ITypeWriterComputerCaster.prototype.superTypeWriterComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form, changes: xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form) => (void|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return)} xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterComputer.__adaptNextPhrase<!xyz.swapee.wc.ITypeWriterComputer>} xyz.swapee.wc.ITypeWriterComputer._adaptNextPhrase */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase} */
/**
 * @param {!xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} form The form with inputs.
 * - `nextIndex` _number_ The index of the currently selected phrase. ⤴ *ITypeWriterOuterCore.Model.NextIndex_Safe*
 * - `phrases` _!Array&lt;string&gt;_ The phrases. ⤴ *ITypeWriterOuterCore.Model.Phrases_Safe*
 * @param {xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form} changes The previous values of the form.
 * - `nextIndex` _number_ The index of the currently selected phrase. ⤴ *ITypeWriterOuterCore.Model.NextIndex_Safe*
 * - `phrases` _!Array&lt;string&gt;_ The phrases. ⤴ *ITypeWriterOuterCore.Model.Phrases_Safe*
 * @return {void|xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return} The form with outputs.
 */
xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase = function(form, changes) {}

/** @typedef {xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe&xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase} xyz.swapee.wc.ITypeWriterComputer.adaptNextPhrase.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.TypeWriterMemory) => void} xyz.swapee.wc.ITypeWriterComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterComputer.__compute<!xyz.swapee.wc.ITypeWriterComputer>} xyz.swapee.wc.ITypeWriterComputer._compute */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.TypeWriterMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/03-ITypeWriterOuterCore.xml}  b52f6f6f71f2e3372d7b4dd7ad28b7c7 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITypeWriterOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterOuterCore)} xyz.swapee.wc.AbstractTypeWriterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterOuterCore} xyz.swapee.wc.TypeWriterOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterOuterCore
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterOuterCore.constructor&xyz.swapee.wc.TypeWriterOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterOuterCore.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.AbstractTypeWriterOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterOuterCoreCaster)} xyz.swapee.wc.ITypeWriterOuterCore.constructor */
/**
 * The _ITypeWriter_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.ITypeWriterOuterCore
 */
xyz.swapee.wc.ITypeWriterOuterCore = class extends /** @type {xyz.swapee.wc.ITypeWriterOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterOuterCore.prototype.constructor = xyz.swapee.wc.ITypeWriterOuterCore

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterOuterCore.Initialese>)} xyz.swapee.wc.TypeWriterOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterOuterCore} xyz.swapee.wc.ITypeWriterOuterCore.typeof */
/**
 * A concrete class of _ITypeWriterOuterCore_ instances.
 * @constructor xyz.swapee.wc.TypeWriterOuterCore
 * @implements {xyz.swapee.wc.ITypeWriterOuterCore} The _ITypeWriter_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterOuterCore = class extends /** @type {xyz.swapee.wc.TypeWriterOuterCore.constructor&xyz.swapee.wc.ITypeWriterOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TypeWriterOuterCore.prototype.constructor = xyz.swapee.wc.TypeWriterOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterOuterCore}
 */
xyz.swapee.wc.TypeWriterOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterOuterCore.
 * @interface xyz.swapee.wc.ITypeWriterOuterCoreFields
 */
xyz.swapee.wc.ITypeWriterOuterCoreFields = class { }
/**
 * The _ITypeWriter_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.ITypeWriterOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITypeWriterOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore} */
xyz.swapee.wc.RecordITypeWriterOuterCore

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore} xyz.swapee.wc.BoundITypeWriterOuterCore */

/** @typedef {xyz.swapee.wc.TypeWriterOuterCore} xyz.swapee.wc.BoundTypeWriterOuterCore */

/**
 * The phrases.
 * @typedef {!Array<string>}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases.phrases

/**
 * How long to keep the phrase appearing, in ms.
 * @typedef {number}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold.hold

/**
 * How long to keep the phrase erased before the next one is entered.
 * @typedef {number}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear.holdClear

/**
 * The interval between entering letters in ms.
 * @typedef {number}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay.enterDelay

/**
 * The interval between erasing letters in ms.
 * @typedef {number}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay.eraseDelay

/**
 * The currently selected phrase.
 * @typedef {string}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase.currentPhrase

/**
 * The phrase to be selected next.
 * @typedef {string}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase.nextPhrase

/**
 * The index of the currently selected phrase.
 * @typedef {number}
 */
xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex.nextIndex

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases&xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold&xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear&xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay&xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay&xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase&xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase&xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex} xyz.swapee.wc.ITypeWriterOuterCore.Model The _ITypeWriter_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel The _ITypeWriter_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _ITypeWriterOuterCore_ interface.
 * @interface xyz.swapee.wc.ITypeWriterOuterCoreCaster
 */
xyz.swapee.wc.ITypeWriterOuterCoreCaster = class { }
/**
 * Cast the _ITypeWriterOuterCore_ instance into the _BoundITypeWriterOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterOuterCore}
 */
xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.asITypeWriterOuterCore
/**
 * Access the _TypeWriterOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterOuterCore}
 */
xyz.swapee.wc.ITypeWriterOuterCoreCaster.prototype.superTypeWriterOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases The phrases (optional overlay).
 * @prop {!Array<string>} [phrases] The phrases. Default `[]`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe The phrases (required overlay).
 * @prop {!Array<string>} phrases The phrases.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold How long to keep the phrase appearing, in ms (optional overlay).
 * @prop {number} [hold=1250] How long to keep the phrase appearing, in ms. Default `1250`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe How long to keep the phrase appearing, in ms (required overlay).
 * @prop {number} hold How long to keep the phrase appearing, in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear How long to keep the phrase erased before the next one is entered (optional overlay).
 * @prop {number} [holdClear=100] How long to keep the phrase erased before the next one is entered. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe How long to keep the phrase erased before the next one is entered (required overlay).
 * @prop {number} holdClear How long to keep the phrase erased before the next one is entered.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay The interval between entering letters in ms (optional overlay).
 * @prop {number} [enterDelay=100] The interval between entering letters in ms. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe The interval between entering letters in ms (required overlay).
 * @prop {number} enterDelay The interval between entering letters in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay The interval between erasing letters in ms (optional overlay).
 * @prop {number} [eraseDelay=100] The interval between erasing letters in ms. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe The interval between erasing letters in ms (required overlay).
 * @prop {number} eraseDelay The interval between erasing letters in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase The currently selected phrase (optional overlay).
 * @prop {string} [currentPhrase=""] The currently selected phrase. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe The currently selected phrase (required overlay).
 * @prop {string} currentPhrase The currently selected phrase.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase The phrase to be selected next (optional overlay).
 * @prop {string} [nextPhrase=""] The phrase to be selected next. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe The phrase to be selected next (required overlay).
 * @prop {string} nextPhrase The phrase to be selected next.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex The index of the currently selected phrase (optional overlay).
 * @prop {number} [nextIndex=1] The index of the currently selected phrase. Default `1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe The index of the currently selected phrase (required overlay).
 * @prop {number} nextIndex The index of the currently selected phrase.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases The phrases (optional overlay).
 * @prop {*} [phrases=null] The phrases. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe The phrases (required overlay).
 * @prop {*} phrases The phrases.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold How long to keep the phrase appearing, in ms (optional overlay).
 * @prop {*} [hold=1250] How long to keep the phrase appearing, in ms. Default `1250`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe How long to keep the phrase appearing, in ms (required overlay).
 * @prop {*} hold How long to keep the phrase appearing, in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear How long to keep the phrase erased before the next one is entered (optional overlay).
 * @prop {*} [holdClear=100] How long to keep the phrase erased before the next one is entered. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe How long to keep the phrase erased before the next one is entered (required overlay).
 * @prop {*} holdClear How long to keep the phrase erased before the next one is entered.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay The interval between entering letters in ms (optional overlay).
 * @prop {*} [enterDelay=100] The interval between entering letters in ms. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe The interval between entering letters in ms (required overlay).
 * @prop {*} enterDelay The interval between entering letters in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay The interval between erasing letters in ms (optional overlay).
 * @prop {*} [eraseDelay=100] The interval between erasing letters in ms. Default `100`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe The interval between erasing letters in ms (required overlay).
 * @prop {*} eraseDelay The interval between erasing letters in ms.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase The currently selected phrase (optional overlay).
 * @prop {*} [currentPhrase=null] The currently selected phrase. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe The currently selected phrase (required overlay).
 * @prop {*} currentPhrase The currently selected phrase.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase The phrase to be selected next (optional overlay).
 * @prop {*} [nextPhrase=null] The phrase to be selected next. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe The phrase to be selected next (required overlay).
 * @prop {*} nextPhrase The phrase to be selected next.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex The index of the currently selected phrase (optional overlay).
 * @prop {*} [nextIndex=1] The index of the currently selected phrase. Default `1`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe The index of the currently selected phrase (required overlay).
 * @prop {*} nextIndex The index of the currently selected phrase.
 */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases The phrases (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.Phrases_Safe The phrases (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold How long to keep the phrase appearing, in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.Hold_Safe How long to keep the phrase appearing, in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear How long to keep the phrase erased before the next one is entered (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.HoldClear_Safe How long to keep the phrase erased before the next one is entered (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay The interval between entering letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.EnterDelay_Safe The interval between entering letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay The interval between erasing letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.EraseDelay_Safe The interval between erasing letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase The currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.CurrentPhrase_Safe The currently selected phrase (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase The phrase to be selected next (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.NextPhrase_Safe The phrase to be selected next (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex The index of the currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe} xyz.swapee.wc.ITypeWriterPort.Inputs.NextIndex_Safe The index of the currently selected phrase (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases The phrases (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Phrases_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Phrases_Safe The phrases (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold How long to keep the phrase appearing, in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.Hold_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.Hold_Safe How long to keep the phrase appearing, in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear How long to keep the phrase erased before the next one is entered (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.HoldClear_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.HoldClear_Safe How long to keep the phrase erased before the next one is entered (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay The interval between entering letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EnterDelay_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EnterDelay_Safe The interval between entering letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay The interval between erasing letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.EraseDelay_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.EraseDelay_Safe The interval between erasing letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase The currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.CurrentPhrase_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.CurrentPhrase_Safe The currently selected phrase (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase The phrase to be selected next (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextPhrase_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextPhrase_Safe The phrase to be selected next (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex The index of the currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.NextIndex_Safe} xyz.swapee.wc.ITypeWriterPort.WeakInputs.NextIndex_Safe The index of the currently selected phrase (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases} xyz.swapee.wc.ITypeWriterCore.Model.Phrases The phrases (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.Phrases_Safe} xyz.swapee.wc.ITypeWriterCore.Model.Phrases_Safe The phrases (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold} xyz.swapee.wc.ITypeWriterCore.Model.Hold How long to keep the phrase appearing, in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.Hold_Safe} xyz.swapee.wc.ITypeWriterCore.Model.Hold_Safe How long to keep the phrase appearing, in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear How long to keep the phrase erased before the next one is entered (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.HoldClear_Safe} xyz.swapee.wc.ITypeWriterCore.Model.HoldClear_Safe How long to keep the phrase erased before the next one is entered (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay The interval between entering letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.EnterDelay_Safe} xyz.swapee.wc.ITypeWriterCore.Model.EnterDelay_Safe The interval between entering letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay The interval between erasing letters in ms (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.EraseDelay_Safe} xyz.swapee.wc.ITypeWriterCore.Model.EraseDelay_Safe The interval between erasing letters in ms (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase The currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.CurrentPhrase_Safe} xyz.swapee.wc.ITypeWriterCore.Model.CurrentPhrase_Safe The currently selected phrase (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase The phrase to be selected next (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextPhrase_Safe} xyz.swapee.wc.ITypeWriterCore.Model.NextPhrase_Safe The phrase to be selected next (required overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex The index of the currently selected phrase (optional overlay). */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model.NextIndex_Safe} xyz.swapee.wc.ITypeWriterCore.Model.NextIndex_Safe The index of the currently selected phrase (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/04-ITypeWriterPort.xml}  87fa25fe477929cc9e2a3834cbe15081 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITypeWriterPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterPort)} xyz.swapee.wc.AbstractTypeWriterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterPort} xyz.swapee.wc.TypeWriterPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterPort` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterPort
 */
xyz.swapee.wc.AbstractTypeWriterPort = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterPort.constructor&xyz.swapee.wc.TypeWriterPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterPort.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterPort.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterPort|typeof xyz.swapee.wc.TypeWriterPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.AbstractTypeWriterPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterPort.Initialese[]) => xyz.swapee.wc.ITypeWriterPort} xyz.swapee.wc.TypeWriterPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITypeWriterPort.Inputs>)} xyz.swapee.wc.ITypeWriterPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _ITypeWriter_, providing input
 * pins.
 * @interface xyz.swapee.wc.ITypeWriterPort
 */
xyz.swapee.wc.ITypeWriterPort = class extends /** @type {xyz.swapee.wc.ITypeWriterPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITypeWriterPort.resetPort} */
xyz.swapee.wc.ITypeWriterPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort} */
xyz.swapee.wc.ITypeWriterPort.prototype.resetTypeWriterPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterPort.Initialese>)} xyz.swapee.wc.TypeWriterPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterPort} xyz.swapee.wc.ITypeWriterPort.typeof */
/**
 * A concrete class of _ITypeWriterPort_ instances.
 * @constructor xyz.swapee.wc.TypeWriterPort
 * @implements {xyz.swapee.wc.ITypeWriterPort} The port that serves as an interface to the _ITypeWriter_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterPort.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterPort = class extends /** @type {xyz.swapee.wc.TypeWriterPort.constructor&xyz.swapee.wc.ITypeWriterPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterPort}
 */
xyz.swapee.wc.TypeWriterPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterPort.
 * @interface xyz.swapee.wc.ITypeWriterPortFields
 */
xyz.swapee.wc.ITypeWriterPortFields = class { }
/**
 * The inputs to the _ITypeWriter_'s controller via its port.
 */
xyz.swapee.wc.ITypeWriterPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITypeWriterPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITypeWriterPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterPort} */
xyz.swapee.wc.RecordITypeWriterPort

/** @typedef {xyz.swapee.wc.ITypeWriterPort} xyz.swapee.wc.BoundITypeWriterPort */

/** @typedef {xyz.swapee.wc.TypeWriterPort} xyz.swapee.wc.BoundTypeWriterPort */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterOuterCore.WeakModel)} xyz.swapee.wc.ITypeWriterPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterOuterCore.WeakModel} xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.typeof */
/**
 * The inputs to the _ITypeWriter_'s controller via its port.
 * @record xyz.swapee.wc.ITypeWriterPort.Inputs
 */
xyz.swapee.wc.ITypeWriterPort.Inputs = class extends /** @type {xyz.swapee.wc.ITypeWriterPort.Inputs.constructor&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterPort.Inputs.prototype.constructor = xyz.swapee.wc.ITypeWriterPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterOuterCore.WeakModel)} xyz.swapee.wc.ITypeWriterPort.WeakInputs.constructor */
/**
 * The inputs to the _ITypeWriter_'s controller via its port.
 * @record xyz.swapee.wc.ITypeWriterPort.WeakInputs
 */
xyz.swapee.wc.ITypeWriterPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITypeWriterPort.WeakInputs.constructor&xyz.swapee.wc.ITypeWriterOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITypeWriterPort.WeakInputs

/**
 * Contains getters to cast the _ITypeWriterPort_ interface.
 * @interface xyz.swapee.wc.ITypeWriterPortCaster
 */
xyz.swapee.wc.ITypeWriterPortCaster = class { }
/**
 * Cast the _ITypeWriterPort_ instance into the _BoundITypeWriterPort_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterPort}
 */
xyz.swapee.wc.ITypeWriterPortCaster.prototype.asITypeWriterPort
/**
 * Access the _TypeWriterPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterPort}
 */
xyz.swapee.wc.ITypeWriterPortCaster.prototype.superTypeWriterPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITypeWriterPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterPort.__resetPort<!xyz.swapee.wc.ITypeWriterPort>} xyz.swapee.wc.ITypeWriterPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterPort.resetPort} */
/**
 * Resets the _ITypeWriter_ port.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterPort.__resetTypeWriterPort<!xyz.swapee.wc.ITypeWriterPort>} xyz.swapee.wc.ITypeWriterPort._resetTypeWriterPort */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterPort.resetTypeWriterPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/09-ITypeWriterCore.xml}  26a0f3819250330848b27b8be65e84cd */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITypeWriterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterCore)} xyz.swapee.wc.AbstractTypeWriterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterCore} xyz.swapee.wc.TypeWriterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterCore` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterCore
 */
xyz.swapee.wc.AbstractTypeWriterCore = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterCore.constructor&xyz.swapee.wc.TypeWriterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterCore.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterCore.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterOuterCore|typeof xyz.swapee.wc.TypeWriterOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.AbstractTypeWriterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterCoreCaster&xyz.swapee.wc.ITypeWriterOuterCore)} xyz.swapee.wc.ITypeWriterCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.ITypeWriterCore
 */
xyz.swapee.wc.ITypeWriterCore = class extends /** @type {xyz.swapee.wc.ITypeWriterCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITypeWriterOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.ITypeWriterCore.resetCore} */
xyz.swapee.wc.ITypeWriterCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore} */
xyz.swapee.wc.ITypeWriterCore.prototype.resetTypeWriterCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterCore&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterCore.Initialese>)} xyz.swapee.wc.TypeWriterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterCore} xyz.swapee.wc.ITypeWriterCore.typeof */
/**
 * A concrete class of _ITypeWriterCore_ instances.
 * @constructor xyz.swapee.wc.TypeWriterCore
 * @implements {xyz.swapee.wc.ITypeWriterCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterCore.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterCore = class extends /** @type {xyz.swapee.wc.TypeWriterCore.constructor&xyz.swapee.wc.ITypeWriterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.TypeWriterCore.prototype.constructor = xyz.swapee.wc.TypeWriterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterCore}
 */
xyz.swapee.wc.TypeWriterCore.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterCore.
 * @interface xyz.swapee.wc.ITypeWriterCoreFields
 */
xyz.swapee.wc.ITypeWriterCoreFields = class { }
/**
 * The _ITypeWriter_'s memory.
 */
xyz.swapee.wc.ITypeWriterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.ITypeWriterCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.ITypeWriterCoreFields.prototype.props = /** @type {xyz.swapee.wc.ITypeWriterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterCore} */
xyz.swapee.wc.RecordITypeWriterCore

/** @typedef {xyz.swapee.wc.ITypeWriterCore} xyz.swapee.wc.BoundITypeWriterCore */

/** @typedef {xyz.swapee.wc.TypeWriterCore} xyz.swapee.wc.BoundTypeWriterCore */

/** @typedef {xyz.swapee.wc.ITypeWriterOuterCore.Model} xyz.swapee.wc.ITypeWriterCore.Model The _ITypeWriter_'s memory. */

/**
 * Contains getters to cast the _ITypeWriterCore_ interface.
 * @interface xyz.swapee.wc.ITypeWriterCoreCaster
 */
xyz.swapee.wc.ITypeWriterCoreCaster = class { }
/**
 * Cast the _ITypeWriterCore_ instance into the _BoundITypeWriterCore_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterCore}
 */
xyz.swapee.wc.ITypeWriterCoreCaster.prototype.asITypeWriterCore
/**
 * Access the _TypeWriterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterCore}
 */
xyz.swapee.wc.ITypeWriterCoreCaster.prototype.superTypeWriterCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITypeWriterCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterCore.__resetCore<!xyz.swapee.wc.ITypeWriterCore>} xyz.swapee.wc.ITypeWriterCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterCore.resetCore} */
/**
 * Resets the _ITypeWriter_ core.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterCore.__resetTypeWriterCore<!xyz.swapee.wc.ITypeWriterCore>} xyz.swapee.wc.ITypeWriterCore._resetTypeWriterCore */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterCore.resetTypeWriterCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/10-ITypeWriterProcessor.xml}  f4cd0e214e33419ef02bdb94ba8c7388 */
/** @typedef {xyz.swapee.wc.ITypeWriterComputer.Initialese&xyz.swapee.wc.ITypeWriterController.Initialese} xyz.swapee.wc.ITypeWriterProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterProcessor)} xyz.swapee.wc.AbstractTypeWriterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterProcessor} xyz.swapee.wc.TypeWriterProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterProcessor
 */
xyz.swapee.wc.AbstractTypeWriterProcessor = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterProcessor.constructor&xyz.swapee.wc.TypeWriterProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterProcessor.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterCore|typeof xyz.swapee.wc.TypeWriterCore)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.AbstractTypeWriterProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterProcessor.Initialese[]) => xyz.swapee.wc.ITypeWriterProcessor} xyz.swapee.wc.TypeWriterProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterProcessorCaster&xyz.swapee.wc.ITypeWriterComputer&xyz.swapee.wc.ITypeWriterCore&xyz.swapee.wc.ITypeWriterController)} xyz.swapee.wc.ITypeWriterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterController} xyz.swapee.wc.ITypeWriterController.typeof */
/**
 * The processor to compute changes to the memory for the _ITypeWriter_.
 * @interface xyz.swapee.wc.ITypeWriterProcessor
 */
xyz.swapee.wc.ITypeWriterProcessor = class extends /** @type {xyz.swapee.wc.ITypeWriterProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITypeWriterComputer.typeof&xyz.swapee.wc.ITypeWriterCore.typeof&xyz.swapee.wc.ITypeWriterController.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterProcessor.Initialese>)} xyz.swapee.wc.TypeWriterProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterProcessor} xyz.swapee.wc.ITypeWriterProcessor.typeof */
/**
 * A concrete class of _ITypeWriterProcessor_ instances.
 * @constructor xyz.swapee.wc.TypeWriterProcessor
 * @implements {xyz.swapee.wc.ITypeWriterProcessor} The processor to compute changes to the memory for the _ITypeWriter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterProcessor.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterProcessor = class extends /** @type {xyz.swapee.wc.TypeWriterProcessor.constructor&xyz.swapee.wc.ITypeWriterProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterProcessor}
 */
xyz.swapee.wc.TypeWriterProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITypeWriterProcessor} */
xyz.swapee.wc.RecordITypeWriterProcessor

/** @typedef {xyz.swapee.wc.ITypeWriterProcessor} xyz.swapee.wc.BoundITypeWriterProcessor */

/** @typedef {xyz.swapee.wc.TypeWriterProcessor} xyz.swapee.wc.BoundTypeWriterProcessor */

/**
 * Contains getters to cast the _ITypeWriterProcessor_ interface.
 * @interface xyz.swapee.wc.ITypeWriterProcessorCaster
 */
xyz.swapee.wc.ITypeWriterProcessorCaster = class { }
/**
 * Cast the _ITypeWriterProcessor_ instance into the _BoundITypeWriterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterProcessor}
 */
xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.asITypeWriterProcessor
/**
 * Access the _TypeWriterProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterProcessor}
 */
xyz.swapee.wc.ITypeWriterProcessorCaster.prototype.superTypeWriterProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/100-TypeWriterMemory.xml}  0af58cb1ef41987cf32400f70830dc92 */
/**
 * The memory of the _ITypeWriter_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.TypeWriterMemory
 */
xyz.swapee.wc.TypeWriterMemory = class { }
/**
 * The phrases. Default `[]`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.phrases = /** @type {!Array<string>} */ (void 0)
/**
 * How long to keep the phrase appearing, in ms. Default `0`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.hold = /** @type {number} */ (void 0)
/**
 * How long to keep the phrase erased before the next one is entered. Default `0`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.holdClear = /** @type {number} */ (void 0)
/**
 * The interval between entering letters in ms. Default `0`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.enterDelay = /** @type {number} */ (void 0)
/**
 * The interval between erasing letters in ms. Default `0`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.eraseDelay = /** @type {number} */ (void 0)
/**
 * The currently selected phrase. Default empty string.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.currentPhrase = /** @type {string} */ (void 0)
/**
 * The phrase to be selected next. Default empty string.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.nextPhrase = /** @type {string} */ (void 0)
/**
 * The index of the currently selected phrase. Default `0`.
 */
xyz.swapee.wc.TypeWriterMemory.prototype.nextIndex = /** @type {number} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/102-TypeWriterInputs.xml}  0fd59a7318f455dbb1981204b63e7e44 */
/**
 * The inputs of the _ITypeWriter_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.TypeWriterInputs
 */
xyz.swapee.wc.front.TypeWriterInputs = class { }
/**
 * The phrases. Default `[]`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.phrases = /** @type {(!Array<string>)|undefined} */ (void 0)
/**
 * How long to keep the phrase appearing, in ms. Default `1250`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.hold = /** @type {number|undefined} */ (void 0)
/**
 * How long to keep the phrase erased before the next one is entered. Default `100`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.holdClear = /** @type {number|undefined} */ (void 0)
/**
 * The interval between entering letters in ms. Default `100`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.enterDelay = /** @type {number|undefined} */ (void 0)
/**
 * The interval between erasing letters in ms. Default `100`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.eraseDelay = /** @type {number|undefined} */ (void 0)
/**
 * The currently selected phrase. Default empty string.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.currentPhrase = /** @type {string|undefined} */ (void 0)
/**
 * The phrase to be selected next. Default empty string.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.nextPhrase = /** @type {string|undefined} */ (void 0)
/**
 * The index of the currently selected phrase. Default `1`.
 */
xyz.swapee.wc.front.TypeWriterInputs.prototype.nextIndex = /** @type {number|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/11-ITypeWriter.xml}  e4315d38eb8376426a5543b7bc7a7ca3 */
/**
 * An atomic wrapper for the _ITypeWriter_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.TypeWriterEnv
 */
xyz.swapee.wc.TypeWriterEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.TypeWriterEnv.prototype.typeWriter = /** @type {xyz.swapee.wc.ITypeWriter} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs>&xyz.swapee.wc.ITypeWriterProcessor.Initialese&xyz.swapee.wc.ITypeWriterComputer.Initialese&xyz.swapee.wc.ITypeWriterController.Initialese} xyz.swapee.wc.ITypeWriter.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TypeWriter)} xyz.swapee.wc.AbstractTypeWriter.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriter} xyz.swapee.wc.TypeWriter.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriter` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriter
 */
xyz.swapee.wc.AbstractTypeWriter = class extends /** @type {xyz.swapee.wc.AbstractTypeWriter.constructor&xyz.swapee.wc.TypeWriter.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriter.prototype.constructor = xyz.swapee.wc.AbstractTypeWriter
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriter.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriter} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriter}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriter.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.AbstractTypeWriter.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriter.Initialese[]) => xyz.swapee.wc.ITypeWriter} xyz.swapee.wc.TypeWriterConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriter.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.ITypeWriter.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.ITypeWriter.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.ITypeWriter.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.TypeWriterMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.TypeWriterClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterCaster&xyz.swapee.wc.ITypeWriterProcessor&xyz.swapee.wc.ITypeWriterComputer&xyz.swapee.wc.ITypeWriterController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, null>)} xyz.swapee.wc.ITypeWriter.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.ITypeWriter
 */
xyz.swapee.wc.ITypeWriter = class extends /** @type {xyz.swapee.wc.ITypeWriter.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITypeWriterProcessor.typeof&xyz.swapee.wc.ITypeWriterComputer.typeof&xyz.swapee.wc.ITypeWriterController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriter.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriter&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriter.Initialese>)} xyz.swapee.wc.TypeWriter.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriter} xyz.swapee.wc.ITypeWriter.typeof */
/**
 * A concrete class of _ITypeWriter_ instances.
 * @constructor xyz.swapee.wc.TypeWriter
 * @implements {xyz.swapee.wc.ITypeWriter} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriter.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriter = class extends /** @type {xyz.swapee.wc.TypeWriter.constructor&xyz.swapee.wc.ITypeWriter.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriter* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriter* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriter.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriter.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriter}
 */
xyz.swapee.wc.TypeWriter.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriter.
 * @interface xyz.swapee.wc.ITypeWriterFields
 */
xyz.swapee.wc.ITypeWriterFields = class { }
/**
 * The input pins of the _ITypeWriter_ port.
 */
xyz.swapee.wc.ITypeWriterFields.prototype.pinout = /** @type {!xyz.swapee.wc.ITypeWriter.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriter} */
xyz.swapee.wc.RecordITypeWriter

/** @typedef {xyz.swapee.wc.ITypeWriter} xyz.swapee.wc.BoundITypeWriter */

/** @typedef {xyz.swapee.wc.TypeWriter} xyz.swapee.wc.BoundTypeWriter */

/** @typedef {xyz.swapee.wc.ITypeWriterController.Inputs} xyz.swapee.wc.ITypeWriter.Pinout The input pins of the _ITypeWriter_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>)} xyz.swapee.wc.ITypeWriterBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.ITypeWriterBuffer
 */
xyz.swapee.wc.ITypeWriterBuffer = class extends /** @type {xyz.swapee.wc.ITypeWriterBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterBuffer.prototype.constructor = xyz.swapee.wc.ITypeWriterBuffer

/**
 * A concrete class of _ITypeWriterBuffer_ instances.
 * @constructor xyz.swapee.wc.TypeWriterBuffer
 * @implements {xyz.swapee.wc.ITypeWriterBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.TypeWriterBuffer = class extends xyz.swapee.wc.ITypeWriterBuffer { }
xyz.swapee.wc.TypeWriterBuffer.prototype.constructor = xyz.swapee.wc.TypeWriterBuffer

/**
 * Contains getters to cast the _ITypeWriter_ interface.
 * @interface xyz.swapee.wc.ITypeWriterCaster
 */
xyz.swapee.wc.ITypeWriterCaster = class { }
/**
 * Cast the _ITypeWriter_ instance into the _BoundITypeWriter_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriter}
 */
xyz.swapee.wc.ITypeWriterCaster.prototype.asITypeWriter
/**
 * Access the _TypeWriter_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriter}
 */
xyz.swapee.wc.ITypeWriterCaster.prototype.superTypeWriter

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/110-TypeWriterSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TypeWriterMemoryPQs
 */
xyz.swapee.wc.TypeWriterMemoryPQs = class {
  constructor() {
    /**
     * `fa196`
     */
    this.phrases=/** @type {string} */ (void 0)
    /**
     * `af1d8`
     */
    this.hold=/** @type {string} */ (void 0)
    /**
     * `f7bdd`
     */
    this.holdClear=/** @type {string} */ (void 0)
    /**
     * `db40c`
     */
    this.enterDelay=/** @type {string} */ (void 0)
    /**
     * `c6e0c`
     */
    this.eraseDelay=/** @type {string} */ (void 0)
    /**
     * `f3af0`
     */
    this.currentPhrase=/** @type {string} */ (void 0)
    /**
     * `c5444`
     */
    this.nextPhrase=/** @type {string} */ (void 0)
    /**
     * `b6f75`
     */
    this.nextIndex=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.TypeWriterMemoryPQs.prototype.constructor = xyz.swapee.wc.TypeWriterMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TypeWriterMemoryQPs
 * @dict
 */
xyz.swapee.wc.TypeWriterMemoryQPs = class { }
/**
 * `phrases`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.fa196 = /** @type {string} */ (void 0)
/**
 * `hold`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.af1d8 = /** @type {string} */ (void 0)
/**
 * `holdClear`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f7bdd = /** @type {string} */ (void 0)
/**
 * `enterDelay`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.db40c = /** @type {string} */ (void 0)
/**
 * `eraseDelay`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c6e0c = /** @type {string} */ (void 0)
/**
 * `currentPhrase`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.f3af0 = /** @type {string} */ (void 0)
/**
 * `nextPhrase`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.c5444 = /** @type {string} */ (void 0)
/**
 * `nextIndex`
 */
xyz.swapee.wc.TypeWriterMemoryQPs.prototype.b6f75 = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.TypeWriterMemoryPQs)} xyz.swapee.wc.TypeWriterInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterMemoryPQs} xyz.swapee.wc.TypeWriterMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TypeWriterInputsPQs
 */
xyz.swapee.wc.TypeWriterInputsPQs = class extends /** @type {xyz.swapee.wc.TypeWriterInputsPQs.constructor&xyz.swapee.wc.TypeWriterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TypeWriterInputsPQs.prototype.constructor = xyz.swapee.wc.TypeWriterInputsPQs

/** @typedef {function(new: xyz.swapee.wc.TypeWriterMemoryPQs)} xyz.swapee.wc.TypeWriterInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TypeWriterInputsQPs
 * @dict
 */
xyz.swapee.wc.TypeWriterInputsQPs = class extends /** @type {xyz.swapee.wc.TypeWriterInputsQPs.constructor&xyz.swapee.wc.TypeWriterMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.TypeWriterInputsQPs.prototype.constructor = xyz.swapee.wc.TypeWriterInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.TypeWriterVdusPQs
 */
xyz.swapee.wc.TypeWriterVdusPQs = class { }
xyz.swapee.wc.TypeWriterVdusPQs.prototype.constructor = xyz.swapee.wc.TypeWriterVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.TypeWriterVdusQPs
 * @dict
 */
xyz.swapee.wc.TypeWriterVdusQPs = class { }
xyz.swapee.wc.TypeWriterVdusQPs.prototype.constructor = xyz.swapee.wc.TypeWriterVdusQPs

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/12-ITypeWriterHtmlComponent.xml}  fe7841b5a15614646529e0db6279ff50 */
/** @typedef {xyz.swapee.wc.back.ITypeWriterController.Initialese&xyz.swapee.wc.back.ITypeWriterScreen.Initialese&xyz.swapee.wc.ITypeWriter.Initialese&xyz.swapee.wc.ITypeWriterGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.ITypeWriterProcessor.Initialese&xyz.swapee.wc.ITypeWriterComputer.Initialese&ITypeWriterGenerator.Initialese} xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterHtmlComponent)} xyz.swapee.wc.AbstractTypeWriterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterHtmlComponent} xyz.swapee.wc.TypeWriterHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterHtmlComponent
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterHtmlComponent.constructor&xyz.swapee.wc.TypeWriterHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterHtmlComponent|typeof xyz.swapee.wc.TypeWriterHtmlComponent)|(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.ITypeWriter|typeof xyz.swapee.wc.TypeWriter)|(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.ITypeWriterProcessor|typeof xyz.swapee.wc.TypeWriterProcessor)|(!xyz.swapee.wc.ITypeWriterComputer|typeof xyz.swapee.wc.TypeWriterComputer)|(!ITypeWriterGenerator|typeof TypeWriterGenerator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.AbstractTypeWriterHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese[]) => xyz.swapee.wc.ITypeWriterHtmlComponent} xyz.swapee.wc.TypeWriterHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterHtmlComponentCaster&xyz.swapee.wc.back.ITypeWriterController&xyz.swapee.wc.back.ITypeWriterScreen&xyz.swapee.wc.ITypeWriter&xyz.swapee.wc.ITypeWriterGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.ITypeWriterProcessor&xyz.swapee.wc.ITypeWriterComputer&ITypeWriterGenerator)} xyz.swapee.wc.ITypeWriterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterController} xyz.swapee.wc.back.ITypeWriterController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterScreen} xyz.swapee.wc.back.ITypeWriterScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterGPU} xyz.swapee.wc.ITypeWriterGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof ITypeWriterGenerator} ITypeWriterGenerator.typeof */
/**
 * The _ITypeWriter_'s HTML component for the web page.
 * @interface xyz.swapee.wc.ITypeWriterHtmlComponent
 */
xyz.swapee.wc.ITypeWriterHtmlComponent = class extends /** @type {xyz.swapee.wc.ITypeWriterHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITypeWriterController.typeof&xyz.swapee.wc.back.ITypeWriterScreen.typeof&xyz.swapee.wc.ITypeWriter.typeof&xyz.swapee.wc.ITypeWriterGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.ITypeWriterProcessor.typeof&xyz.swapee.wc.ITypeWriterComputer.typeof&ITypeWriterGenerator.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese>)} xyz.swapee.wc.TypeWriterHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterHtmlComponent} xyz.swapee.wc.ITypeWriterHtmlComponent.typeof */
/**
 * A concrete class of _ITypeWriterHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.TypeWriterHtmlComponent
 * @implements {xyz.swapee.wc.ITypeWriterHtmlComponent} The _ITypeWriter_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterHtmlComponent = class extends /** @type {xyz.swapee.wc.TypeWriterHtmlComponent.constructor&xyz.swapee.wc.ITypeWriterHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterHtmlComponent}
 */
xyz.swapee.wc.TypeWriterHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITypeWriterHtmlComponent} */
xyz.swapee.wc.RecordITypeWriterHtmlComponent

/** @typedef {xyz.swapee.wc.ITypeWriterHtmlComponent} xyz.swapee.wc.BoundITypeWriterHtmlComponent */

/** @typedef {xyz.swapee.wc.TypeWriterHtmlComponent} xyz.swapee.wc.BoundTypeWriterHtmlComponent */

/**
 * Contains getters to cast the _ITypeWriterHtmlComponent_ interface.
 * @interface xyz.swapee.wc.ITypeWriterHtmlComponentCaster
 */
xyz.swapee.wc.ITypeWriterHtmlComponentCaster = class { }
/**
 * Cast the _ITypeWriterHtmlComponent_ instance into the _BoundITypeWriterHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterHtmlComponent}
 */
xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.asITypeWriterHtmlComponent
/**
 * Access the _TypeWriterHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterHtmlComponent}
 */
xyz.swapee.wc.ITypeWriterHtmlComponentCaster.prototype.superTypeWriterHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/130-ITypeWriterElement.xml}  ea61de72b575a3480a63dc967b68c9d6 */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.ITypeWriterElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterElement)} xyz.swapee.wc.AbstractTypeWriterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterElement} xyz.swapee.wc.TypeWriterElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterElement` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterElement
 */
xyz.swapee.wc.AbstractTypeWriterElement = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterElement.constructor&xyz.swapee.wc.TypeWriterElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterElement.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterElement.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterElement|typeof xyz.swapee.wc.TypeWriterElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.AbstractTypeWriterElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterElement.Initialese[]) => xyz.swapee.wc.ITypeWriterElement} xyz.swapee.wc.TypeWriterElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.ITypeWriterElement.Inputs, null>)} xyz.swapee.wc.ITypeWriterElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _ITypeWriter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.ITypeWriterElement
 */
xyz.swapee.wc.ITypeWriterElement = class extends /** @type {xyz.swapee.wc.ITypeWriterElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITypeWriterElement.solder} */
xyz.swapee.wc.ITypeWriterElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.ITypeWriterElement.render} */
xyz.swapee.wc.ITypeWriterElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.ITypeWriterElement.server} */
xyz.swapee.wc.ITypeWriterElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.ITypeWriterElement.inducer} */
xyz.swapee.wc.ITypeWriterElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElement&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElement.Initialese>)} xyz.swapee.wc.TypeWriterElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElement} xyz.swapee.wc.ITypeWriterElement.typeof */
/**
 * A concrete class of _ITypeWriterElement_ instances.
 * @constructor xyz.swapee.wc.TypeWriterElement
 * @implements {xyz.swapee.wc.ITypeWriterElement} A component description.
 *
 * The _ITypeWriter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElement.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterElement = class extends /** @type {xyz.swapee.wc.TypeWriterElement.constructor&xyz.swapee.wc.ITypeWriterElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElement}
 */
xyz.swapee.wc.TypeWriterElement.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterElement.
 * @interface xyz.swapee.wc.ITypeWriterElementFields
 */
xyz.swapee.wc.ITypeWriterElementFields = class { }
/**
 * The element-specific inputs to the _ITypeWriter_ component.
 */
xyz.swapee.wc.ITypeWriterElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITypeWriterElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterElement} */
xyz.swapee.wc.RecordITypeWriterElement

/** @typedef {xyz.swapee.wc.ITypeWriterElement} xyz.swapee.wc.BoundITypeWriterElement */

/** @typedef {xyz.swapee.wc.TypeWriterElement} xyz.swapee.wc.BoundTypeWriterElement */

/** @typedef {xyz.swapee.wc.ITypeWriterPort.Inputs&xyz.swapee.wc.ITypeWriterDisplay.Queries&xyz.swapee.wc.ITypeWriterController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.ITypeWriterElementPort.Inputs} xyz.swapee.wc.ITypeWriterElement.Inputs The element-specific inputs to the _ITypeWriter_ component. */

/**
 * Contains getters to cast the _ITypeWriterElement_ interface.
 * @interface xyz.swapee.wc.ITypeWriterElementCaster
 */
xyz.swapee.wc.ITypeWriterElementCaster = class { }
/**
 * Cast the _ITypeWriterElement_ instance into the _BoundITypeWriterElement_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterElement}
 */
xyz.swapee.wc.ITypeWriterElementCaster.prototype.asITypeWriterElement
/**
 * Access the _TypeWriterElement_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterElement}
 */
xyz.swapee.wc.ITypeWriterElementCaster.prototype.superTypeWriterElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.TypeWriterMemory, props: !xyz.swapee.wc.ITypeWriterElement.Inputs) => Object<string, *>} xyz.swapee.wc.ITypeWriterElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterElement.__solder<!xyz.swapee.wc.ITypeWriterElement>} xyz.swapee.wc.ITypeWriterElement._solder */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.TypeWriterMemory} model The model.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} props The element props.
 * - `[phrases=null]` _&#42;?_ The phrases. ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* Default `null`.
 * - `[hold=1250]` _&#42;?_ How long to keep the phrase appearing, in ms. ⤴ *ITypeWriterOuterCore.WeakModel.Hold* ⤴ *ITypeWriterOuterCore.WeakModel.Hold* Default `1250`.
 * - `[holdClear=100]` _&#42;?_ How long to keep the phrase erased before the next one is entered. ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* Default `100`.
 * - `[enterDelay=100]` _&#42;?_ The interval between entering letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* Default `100`.
 * - `[eraseDelay=100]` _&#42;?_ The interval between erasing letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* Default `100`.
 * - `[currentPhrase=null]` _&#42;?_ The currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* Default `null`.
 * - `[nextPhrase=null]` _&#42;?_ The phrase to be selected next. ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* Default `null`.
 * - `[nextIndex=1]` _&#42;?_ The index of the currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* Default `1`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITypeWriterElementPort.Inputs.NoSolder* Default `false`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.ITypeWriterElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TypeWriterMemory, instance?: !xyz.swapee.wc.ITypeWriterScreen&xyz.swapee.wc.ITypeWriterController) => !engineering.type.VNode} xyz.swapee.wc.ITypeWriterElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterElement.__render<!xyz.swapee.wc.ITypeWriterElement>} xyz.swapee.wc.ITypeWriterElement._render */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model] The model for the view.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {!xyz.swapee.wc.ITypeWriterScreen&xyz.swapee.wc.ITypeWriterController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITypeWriterElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TypeWriterMemory, inputs: !xyz.swapee.wc.ITypeWriterElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.ITypeWriterElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterElement.__server<!xyz.swapee.wc.ITypeWriterElement>} xyz.swapee.wc.ITypeWriterElement._server */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory The memory registers.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} inputs The inputs to the port.
 * - `[phrases=null]` _&#42;?_ The phrases. ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* Default `null`.
 * - `[hold=1250]` _&#42;?_ How long to keep the phrase appearing, in ms. ⤴ *ITypeWriterOuterCore.WeakModel.Hold* ⤴ *ITypeWriterOuterCore.WeakModel.Hold* Default `1250`.
 * - `[holdClear=100]` _&#42;?_ How long to keep the phrase erased before the next one is entered. ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* Default `100`.
 * - `[enterDelay=100]` _&#42;?_ The interval between entering letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* Default `100`.
 * - `[eraseDelay=100]` _&#42;?_ The interval between erasing letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* Default `100`.
 * - `[currentPhrase=null]` _&#42;?_ The currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* Default `null`.
 * - `[nextPhrase=null]` _&#42;?_ The phrase to be selected next. ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* Default `null`.
 * - `[nextIndex=1]` _&#42;?_ The index of the currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* Default `1`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITypeWriterElementPort.Inputs.NoSolder* Default `false`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.ITypeWriterElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.TypeWriterMemory, port?: !xyz.swapee.wc.ITypeWriterElement.Inputs) => ?} xyz.swapee.wc.ITypeWriterElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterElement.__inducer<!xyz.swapee.wc.ITypeWriterElement>} xyz.swapee.wc.ITypeWriterElement._inducer */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.TypeWriterMemory} [model] The model of the component into which to induce the state.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {!xyz.swapee.wc.ITypeWriterElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[phrases=null]` _&#42;?_ The phrases. ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* ⤴ *ITypeWriterOuterCore.WeakModel.Phrases* Default `null`.
 * - `[hold=1250]` _&#42;?_ How long to keep the phrase appearing, in ms. ⤴ *ITypeWriterOuterCore.WeakModel.Hold* ⤴ *ITypeWriterOuterCore.WeakModel.Hold* Default `1250`.
 * - `[holdClear=100]` _&#42;?_ How long to keep the phrase erased before the next one is entered. ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* ⤴ *ITypeWriterOuterCore.WeakModel.HoldClear* Default `100`.
 * - `[enterDelay=100]` _&#42;?_ The interval between entering letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EnterDelay* Default `100`.
 * - `[eraseDelay=100]` _&#42;?_ The interval between erasing letters in ms. ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* ⤴ *ITypeWriterOuterCore.WeakModel.EraseDelay* Default `100`.
 * - `[currentPhrase=null]` _&#42;?_ The currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.CurrentPhrase* Default `null`.
 * - `[nextPhrase=null]` _&#42;?_ The phrase to be selected next. ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* ⤴ *ITypeWriterOuterCore.WeakModel.NextPhrase* Default `null`.
 * - `[nextIndex=1]` _&#42;?_ The index of the currently selected phrase. ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* ⤴ *ITypeWriterOuterCore.WeakModel.NextIndex* Default `1`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *ITypeWriterElementPort.Inputs.NoSolder* Default `false`.
 * @return {?}
 */
xyz.swapee.wc.ITypeWriterElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/140-ITypeWriterElementPort.xml}  e8387990252964cfa0b385636dd9b2bc */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.ITypeWriterElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterElementPort)} xyz.swapee.wc.AbstractTypeWriterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterElementPort} xyz.swapee.wc.TypeWriterElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterElementPort
 */
xyz.swapee.wc.AbstractTypeWriterElementPort = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterElementPort.constructor&xyz.swapee.wc.TypeWriterElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterElementPort.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterElementPort|typeof xyz.swapee.wc.TypeWriterElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.AbstractTypeWriterElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterElementPort.Initialese[]) => xyz.swapee.wc.ITypeWriterElementPort} xyz.swapee.wc.TypeWriterElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.ITypeWriterElementPort.Inputs>)} xyz.swapee.wc.ITypeWriterElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.ITypeWriterElementPort
 */
xyz.swapee.wc.ITypeWriterElementPort = class extends /** @type {xyz.swapee.wc.ITypeWriterElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElementPort.Initialese>)} xyz.swapee.wc.TypeWriterElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElementPort} xyz.swapee.wc.ITypeWriterElementPort.typeof */
/**
 * A concrete class of _ITypeWriterElementPort_ instances.
 * @constructor xyz.swapee.wc.TypeWriterElementPort
 * @implements {xyz.swapee.wc.ITypeWriterElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterElementPort.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterElementPort = class extends /** @type {xyz.swapee.wc.TypeWriterElementPort.constructor&xyz.swapee.wc.ITypeWriterElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterElementPort}
 */
xyz.swapee.wc.TypeWriterElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterElementPort.
 * @interface xyz.swapee.wc.ITypeWriterElementPortFields
 */
xyz.swapee.wc.ITypeWriterElementPortFields = class { }
/**
 * The inputs to the _ITypeWriterElement_'s controller via its element port.
 */
xyz.swapee.wc.ITypeWriterElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITypeWriterElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.ITypeWriterElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.ITypeWriterElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterElementPort} */
xyz.swapee.wc.RecordITypeWriterElementPort

/** @typedef {xyz.swapee.wc.ITypeWriterElementPort} xyz.swapee.wc.BoundITypeWriterElementPort */

/** @typedef {xyz.swapee.wc.TypeWriterElementPort} xyz.swapee.wc.BoundTypeWriterElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.noSolder

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder)} xyz.swapee.wc.ITypeWriterElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.typeof */
/**
 * The inputs to the _ITypeWriterElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITypeWriterElementPort.Inputs
 */
xyz.swapee.wc.ITypeWriterElementPort.Inputs = class extends /** @type {xyz.swapee.wc.ITypeWriterElementPort.Inputs.constructor&xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterElementPort.Inputs.prototype.constructor = xyz.swapee.wc.ITypeWriterElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder)} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder.typeof */
/**
 * The inputs to the _ITypeWriterElement_'s controller via its element port.
 * @record xyz.swapee.wc.ITypeWriterElementPort.WeakInputs
 */
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.constructor&xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder.typeof} */ (class {}) { }
xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.ITypeWriterElementPort.WeakInputs

/**
 * Contains getters to cast the _ITypeWriterElementPort_ interface.
 * @interface xyz.swapee.wc.ITypeWriterElementPortCaster
 */
xyz.swapee.wc.ITypeWriterElementPortCaster = class { }
/**
 * Cast the _ITypeWriterElementPort_ instance into the _BoundITypeWriterElementPort_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterElementPort}
 */
xyz.swapee.wc.ITypeWriterElementPortCaster.prototype.asITypeWriterElementPort
/**
 * Access the _TypeWriterElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterElementPort}
 */
xyz.swapee.wc.ITypeWriterElementPortCaster.prototype.superTypeWriterElementPort

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/170-ITypeWriterDesigner.xml}  5fb3afc6b796b5cbb194e5b6deb8fc11 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.ITypeWriterDesigner
 */
xyz.swapee.wc.ITypeWriterDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.TypeWriterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITypeWriter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TypeWriterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.ITypeWriter />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.TypeWriterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TypeWriter` _typeof ITypeWriterController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `TypeWriter` _typeof ITypeWriterController_
   * - `This` _typeof ITypeWriterController_
   * @param {!xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `TypeWriter` _!TypeWriterMemory_
   * - `This` _!TypeWriterMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TypeWriterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.TypeWriterClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.ITypeWriterDesigner.prototype.constructor = xyz.swapee.wc.ITypeWriterDesigner

/**
 * A concrete class of _ITypeWriterDesigner_ instances.
 * @constructor xyz.swapee.wc.TypeWriterDesigner
 * @implements {xyz.swapee.wc.ITypeWriterDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.TypeWriterDesigner = class extends xyz.swapee.wc.ITypeWriterDesigner { }
xyz.swapee.wc.TypeWriterDesigner.prototype.constructor = xyz.swapee.wc.TypeWriterDesigner

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITypeWriterController} TypeWriter
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.ITypeWriterController} TypeWriter
 * @prop {typeof xyz.swapee.wc.ITypeWriterController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.ITypeWriterDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.TypeWriterMemory} TypeWriter
 * @prop {!xyz.swapee.wc.TypeWriterMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplay.xml}  40bd1afc1fdbac94736e0f97e2657222 */
/** @typedef {com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings>} xyz.swapee.wc.ITypeWriterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterDisplay)} xyz.swapee.wc.AbstractTypeWriterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterDisplay} xyz.swapee.wc.TypeWriterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterDisplay
 */
xyz.swapee.wc.AbstractTypeWriterDisplay = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterDisplay.constructor&xyz.swapee.wc.TypeWriterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterDisplay.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.AbstractTypeWriterDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterDisplay.Initialese[]) => xyz.swapee.wc.ITypeWriterDisplay} xyz.swapee.wc.TypeWriterDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.TypeWriterMemory, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, xyz.swapee.wc.ITypeWriterDisplay.Queries, null>)} xyz.swapee.wc.ITypeWriterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _ITypeWriter_.
 * @interface xyz.swapee.wc.ITypeWriterDisplay
 */
xyz.swapee.wc.ITypeWriterDisplay = class extends /** @type {xyz.swapee.wc.ITypeWriterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITypeWriterDisplay.paint} */
xyz.swapee.wc.ITypeWriterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterDisplay.Initialese>)} xyz.swapee.wc.TypeWriterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterDisplay} xyz.swapee.wc.ITypeWriterDisplay.typeof */
/**
 * A concrete class of _ITypeWriterDisplay_ instances.
 * @constructor xyz.swapee.wc.TypeWriterDisplay
 * @implements {xyz.swapee.wc.ITypeWriterDisplay} Display for presenting information from the _ITypeWriter_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterDisplay = class extends /** @type {xyz.swapee.wc.TypeWriterDisplay.constructor&xyz.swapee.wc.ITypeWriterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterDisplay}
 */
xyz.swapee.wc.TypeWriterDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterDisplay.
 * @interface xyz.swapee.wc.ITypeWriterDisplayFields
 */
xyz.swapee.wc.ITypeWriterDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.ITypeWriterDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.ITypeWriterDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.ITypeWriterDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.ITypeWriterDisplay.Queries} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterDisplay} */
xyz.swapee.wc.RecordITypeWriterDisplay

/** @typedef {xyz.swapee.wc.ITypeWriterDisplay} xyz.swapee.wc.BoundITypeWriterDisplay */

/** @typedef {xyz.swapee.wc.TypeWriterDisplay} xyz.swapee.wc.BoundTypeWriterDisplay */

/** @typedef {xyz.swapee.wc.ITypeWriterDisplay.Queries} xyz.swapee.wc.ITypeWriterDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.ITypeWriterDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _ITypeWriterDisplay_ interface.
 * @interface xyz.swapee.wc.ITypeWriterDisplayCaster
 */
xyz.swapee.wc.ITypeWriterDisplayCaster = class { }
/**
 * Cast the _ITypeWriterDisplay_ instance into the _BoundITypeWriterDisplay_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterDisplay}
 */
xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/**
 * Cast the _ITypeWriterDisplay_ instance into the _BoundITypeWriterScreen_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterScreen}
 */
xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.asITypeWriterScreen
/**
 * Access the _TypeWriterDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterDisplay}
 */
xyz.swapee.wc.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.TypeWriterMemory, land: null) => void} xyz.swapee.wc.ITypeWriterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterDisplay.__paint<!xyz.swapee.wc.ITypeWriterDisplay>} xyz.swapee.wc.ITypeWriterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TypeWriterMemory} memory The display data.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/40-ITypeWriterDisplayBack.xml}  a1bee4e620d54cbc8498a16ed3579499 */
/** @typedef {com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.TypeWriterClasses>} xyz.swapee.wc.back.ITypeWriterDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TypeWriterDisplay)} xyz.swapee.wc.back.AbstractTypeWriterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TypeWriterDisplay} xyz.swapee.wc.back.TypeWriterDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITypeWriterDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractTypeWriterDisplay
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractTypeWriterDisplay.constructor&xyz.swapee.wc.back.TypeWriterDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTypeWriterDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractTypeWriterDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.AbstractTypeWriterDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITypeWriterDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.TypeWriterClasses, null>)} xyz.swapee.wc.back.ITypeWriterDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.ITypeWriterDisplay
 */
xyz.swapee.wc.back.ITypeWriterDisplay = class extends /** @type {xyz.swapee.wc.back.ITypeWriterDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.ITypeWriterDisplay.paint} */
xyz.swapee.wc.back.ITypeWriterDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterDisplay.Initialese>)} xyz.swapee.wc.back.TypeWriterDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterDisplay} xyz.swapee.wc.back.ITypeWriterDisplay.typeof */
/**
 * A concrete class of _ITypeWriterDisplay_ instances.
 * @constructor xyz.swapee.wc.back.TypeWriterDisplay
 * @implements {xyz.swapee.wc.back.ITypeWriterDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.TypeWriterDisplay = class extends /** @type {xyz.swapee.wc.back.TypeWriterDisplay.constructor&xyz.swapee.wc.back.ITypeWriterDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.TypeWriterDisplay.prototype.constructor = xyz.swapee.wc.back.TypeWriterDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterDisplay}
 */
xyz.swapee.wc.back.TypeWriterDisplay.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITypeWriterDisplay} */
xyz.swapee.wc.back.RecordITypeWriterDisplay

/** @typedef {xyz.swapee.wc.back.ITypeWriterDisplay} xyz.swapee.wc.back.BoundITypeWriterDisplay */

/** @typedef {xyz.swapee.wc.back.TypeWriterDisplay} xyz.swapee.wc.back.BoundTypeWriterDisplay */

/**
 * Contains getters to cast the _ITypeWriterDisplay_ interface.
 * @interface xyz.swapee.wc.back.ITypeWriterDisplayCaster
 */
xyz.swapee.wc.back.ITypeWriterDisplayCaster = class { }
/**
 * Cast the _ITypeWriterDisplay_ instance into the _BoundITypeWriterDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundITypeWriterDisplay}
 */
xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.asITypeWriterDisplay
/**
 * Access the _TypeWriterDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTypeWriterDisplay}
 */
xyz.swapee.wc.back.ITypeWriterDisplayCaster.prototype.superTypeWriterDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.TypeWriterMemory, land?: null) => void} xyz.swapee.wc.back.ITypeWriterDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.ITypeWriterDisplay.__paint<!xyz.swapee.wc.back.ITypeWriterDisplay>} xyz.swapee.wc.back.ITypeWriterDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.TypeWriterMemory} [memory] The display data.
 * - `phrases` _!Array&lt;string&gt;_ The phrases. Default `[]`.
 * - `hold` _number_ How long to keep the phrase appearing, in ms. Default `0`.
 * - `holdClear` _number_ How long to keep the phrase erased before the next one is entered. Default `0`.
 * - `enterDelay` _number_ The interval between entering letters in ms. Default `0`.
 * - `eraseDelay` _number_ The interval between erasing letters in ms. Default `0`.
 * - `currentPhrase` _string_ The currently selected phrase. Default empty string.
 * - `nextPhrase` _string_ The phrase to be selected next. Default empty string.
 * - `nextIndex` _number_ The index of the currently selected phrase. Default `0`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.ITypeWriterDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.ITypeWriterDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/41-TypeWriterClasses.xml}  adf81165d8f25289620940b0c1efa27e */
/**
 * The classes of the _ITypeWriterDisplay_.
 * @record xyz.swapee.wc.TypeWriterClasses
 */
xyz.swapee.wc.TypeWriterClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.TypeWriterClasses.prototype.props = /** @type {xyz.swapee.wc.TypeWriterClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/50-ITypeWriterController.xml}  6113dd8a95fc3d2ed506eded47f91c46 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>} xyz.swapee.wc.ITypeWriterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterController)} xyz.swapee.wc.AbstractTypeWriterController.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterController} xyz.swapee.wc.TypeWriterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterController` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterController
 */
xyz.swapee.wc.AbstractTypeWriterController = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterController.constructor&xyz.swapee.wc.TypeWriterController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterController.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterController.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.AbstractTypeWriterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterController.Initialese[]) => xyz.swapee.wc.ITypeWriterController} xyz.swapee.wc.TypeWriterControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterControllerFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.ITypeWriterOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.ITypeWriterController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.ITypeWriterController.Inputs, !xyz.swapee.wc.TypeWriterMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.ITypeWriterController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.ITypeWriterController.Inputs>)} xyz.swapee.wc.ITypeWriterController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.ITypeWriterController
 */
xyz.swapee.wc.ITypeWriterController = class extends /** @type {xyz.swapee.wc.ITypeWriterController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.ITypeWriterController.resetPort} */
xyz.swapee.wc.ITypeWriterController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.ITypeWriterController.next} */
xyz.swapee.wc.ITypeWriterController.prototype.next = function() {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterController&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterController.Initialese>)} xyz.swapee.wc.TypeWriterController.constructor */
/**
 * A concrete class of _ITypeWriterController_ instances.
 * @constructor xyz.swapee.wc.TypeWriterController
 * @implements {xyz.swapee.wc.ITypeWriterController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterController.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterController = class extends /** @type {xyz.swapee.wc.TypeWriterController.constructor&xyz.swapee.wc.ITypeWriterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterController}
 */
xyz.swapee.wc.TypeWriterController.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterController.
 * @interface xyz.swapee.wc.ITypeWriterControllerFields
 */
xyz.swapee.wc.ITypeWriterControllerFields = class { }
/**
 * The inputs to the _ITypeWriter_'s controller.
 */
xyz.swapee.wc.ITypeWriterControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.ITypeWriterController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ITypeWriterControllerFields.prototype.props = /** @type {xyz.swapee.wc.ITypeWriterController} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterController} */
xyz.swapee.wc.RecordITypeWriterController

/** @typedef {xyz.swapee.wc.ITypeWriterController} xyz.swapee.wc.BoundITypeWriterController */

/** @typedef {xyz.swapee.wc.TypeWriterController} xyz.swapee.wc.BoundTypeWriterController */

/** @typedef {xyz.swapee.wc.ITypeWriterPort.Inputs} xyz.swapee.wc.ITypeWriterController.Inputs The inputs to the _ITypeWriter_'s controller. */

/** @typedef {xyz.swapee.wc.ITypeWriterPort.WeakInputs} xyz.swapee.wc.ITypeWriterController.WeakInputs The inputs to the _ITypeWriter_'s controller. */

/**
 * Contains getters to cast the _ITypeWriterController_ interface.
 * @interface xyz.swapee.wc.ITypeWriterControllerCaster
 */
xyz.swapee.wc.ITypeWriterControllerCaster = class { }
/**
 * Cast the _ITypeWriterController_ instance into the _BoundITypeWriterController_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterController}
 */
xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterController
/**
 * Cast the _ITypeWriterController_ instance into the _BoundITypeWriterProcessor_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterProcessor}
 */
xyz.swapee.wc.ITypeWriterControllerCaster.prototype.asITypeWriterProcessor
/**
 * Access the _TypeWriterController_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterController}
 */
xyz.swapee.wc.ITypeWriterControllerCaster.prototype.superTypeWriterController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.ITypeWriterController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterController.__resetPort<!xyz.swapee.wc.ITypeWriterController>} xyz.swapee.wc.ITypeWriterController._resetPort */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.ITypeWriterController.resetPort = function() {}

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.ITypeWriterController.__next
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.ITypeWriterController.__next<!xyz.swapee.wc.ITypeWriterController>} xyz.swapee.wc.ITypeWriterController._next */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterController.next} */
/**
 * After the phrase has been entered and erased, the screen will ask the
 * controller to select the next phrase.
 * @return {?}
 */
xyz.swapee.wc.ITypeWriterController.next = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.ITypeWriterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/51-ITypeWriterControllerFront.xml}  074c9fe7b0fc3a231bca6737670243a9 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.ITypeWriterController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TypeWriterController)} xyz.swapee.wc.front.AbstractTypeWriterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TypeWriterController} xyz.swapee.wc.front.TypeWriterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITypeWriterController` interface.
 * @constructor xyz.swapee.wc.front.AbstractTypeWriterController
 */
xyz.swapee.wc.front.AbstractTypeWriterController = class extends /** @type {xyz.swapee.wc.front.AbstractTypeWriterController.constructor&xyz.swapee.wc.front.TypeWriterController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTypeWriterController.prototype.constructor = xyz.swapee.wc.front.AbstractTypeWriterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTypeWriterController.class = /** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.AbstractTypeWriterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITypeWriterController.Initialese[]) => xyz.swapee.wc.front.ITypeWriterController} xyz.swapee.wc.front.TypeWriterControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITypeWriterControllerCaster&xyz.swapee.wc.front.ITypeWriterControllerAT)} xyz.swapee.wc.front.ITypeWriterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITypeWriterControllerAT} xyz.swapee.wc.front.ITypeWriterControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.ITypeWriterController
 */
xyz.swapee.wc.front.ITypeWriterController = class extends /** @type {xyz.swapee.wc.front.ITypeWriterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.ITypeWriterControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITypeWriterController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.ITypeWriterController.next} */
xyz.swapee.wc.front.ITypeWriterController.prototype.next = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterController&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterController.Initialese>)} xyz.swapee.wc.front.TypeWriterController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITypeWriterController} xyz.swapee.wc.front.ITypeWriterController.typeof */
/**
 * A concrete class of _ITypeWriterController_ instances.
 * @constructor xyz.swapee.wc.front.TypeWriterController
 * @implements {xyz.swapee.wc.front.ITypeWriterController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterController.Initialese>} ‎
 */
xyz.swapee.wc.front.TypeWriterController = class extends /** @type {xyz.swapee.wc.front.TypeWriterController.constructor&xyz.swapee.wc.front.ITypeWriterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TypeWriterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterController}
 */
xyz.swapee.wc.front.TypeWriterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITypeWriterController} */
xyz.swapee.wc.front.RecordITypeWriterController

/** @typedef {xyz.swapee.wc.front.ITypeWriterController} xyz.swapee.wc.front.BoundITypeWriterController */

/** @typedef {xyz.swapee.wc.front.TypeWriterController} xyz.swapee.wc.front.BoundTypeWriterController */

/**
 * Contains getters to cast the _ITypeWriterController_ interface.
 * @interface xyz.swapee.wc.front.ITypeWriterControllerCaster
 */
xyz.swapee.wc.front.ITypeWriterControllerCaster = class { }
/**
 * Cast the _ITypeWriterController_ instance into the _BoundITypeWriterController_ type.
 * @type {!xyz.swapee.wc.front.BoundITypeWriterController}
 */
xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.asITypeWriterController
/**
 * Access the _TypeWriterController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTypeWriterController}
 */
xyz.swapee.wc.front.ITypeWriterControllerCaster.prototype.superTypeWriterController

/**
 * @typedef {(this: THIS) => ?} xyz.swapee.wc.front.ITypeWriterController.__next
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.ITypeWriterController.__next<!xyz.swapee.wc.front.ITypeWriterController>} xyz.swapee.wc.front.ITypeWriterController._next */
/** @typedef {typeof xyz.swapee.wc.front.ITypeWriterController.next} */
/**
 * After the phrase has been entered and erased, the screen will ask the
 * controller to select the next phrase.
 * @return {?}
 */
xyz.swapee.wc.front.ITypeWriterController.next = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.ITypeWriterController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/52-ITypeWriterControllerBack.xml}  408c1b42749abcf20ee27a918ea72768 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.ITypeWriterController.Inputs>&xyz.swapee.wc.ITypeWriterController.Initialese} xyz.swapee.wc.back.ITypeWriterController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.TypeWriterController)} xyz.swapee.wc.back.AbstractTypeWriterController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TypeWriterController} xyz.swapee.wc.back.TypeWriterController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITypeWriterController` interface.
 * @constructor xyz.swapee.wc.back.AbstractTypeWriterController
 */
xyz.swapee.wc.back.AbstractTypeWriterController = class extends /** @type {xyz.swapee.wc.back.AbstractTypeWriterController.constructor&xyz.swapee.wc.back.TypeWriterController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTypeWriterController.prototype.constructor = xyz.swapee.wc.back.AbstractTypeWriterController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTypeWriterController.class = /** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterController|typeof xyz.swapee.wc.back.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.AbstractTypeWriterController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITypeWriterController.Initialese[]) => xyz.swapee.wc.back.ITypeWriterController} xyz.swapee.wc.back.TypeWriterControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITypeWriterControllerCaster&xyz.swapee.wc.ITypeWriterController&com.webcircuits.IDriverBack<!xyz.swapee.wc.ITypeWriterController.Inputs>)} xyz.swapee.wc.back.ITypeWriterController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.ITypeWriterController
 */
xyz.swapee.wc.back.ITypeWriterController = class extends /** @type {xyz.swapee.wc.back.ITypeWriterController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.ITypeWriterController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITypeWriterController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterController&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterController.Initialese>)} xyz.swapee.wc.back.TypeWriterController.constructor */
/**
 * A concrete class of _ITypeWriterController_ instances.
 * @constructor xyz.swapee.wc.back.TypeWriterController
 * @implements {xyz.swapee.wc.back.ITypeWriterController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterController.Initialese>} ‎
 */
xyz.swapee.wc.back.TypeWriterController = class extends /** @type {xyz.swapee.wc.back.TypeWriterController.constructor&xyz.swapee.wc.back.ITypeWriterController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TypeWriterController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterController}
 */
xyz.swapee.wc.back.TypeWriterController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITypeWriterController} */
xyz.swapee.wc.back.RecordITypeWriterController

/** @typedef {xyz.swapee.wc.back.ITypeWriterController} xyz.swapee.wc.back.BoundITypeWriterController */

/** @typedef {xyz.swapee.wc.back.TypeWriterController} xyz.swapee.wc.back.BoundTypeWriterController */

/**
 * Contains getters to cast the _ITypeWriterController_ interface.
 * @interface xyz.swapee.wc.back.ITypeWriterControllerCaster
 */
xyz.swapee.wc.back.ITypeWriterControllerCaster = class { }
/**
 * Cast the _ITypeWriterController_ instance into the _BoundITypeWriterController_ type.
 * @type {!xyz.swapee.wc.back.BoundITypeWriterController}
 */
xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.asITypeWriterController
/**
 * Access the _TypeWriterController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTypeWriterController}
 */
xyz.swapee.wc.back.ITypeWriterControllerCaster.prototype.superTypeWriterController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/53-ITypeWriterControllerAR.xml}  946702a0b145eac225b3fb0e0b8c0dae */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITypeWriterController.Initialese} xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TypeWriterControllerAR)} xyz.swapee.wc.back.AbstractTypeWriterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TypeWriterControllerAR} xyz.swapee.wc.back.TypeWriterControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITypeWriterControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractTypeWriterControllerAR
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractTypeWriterControllerAR.constructor&xyz.swapee.wc.back.TypeWriterControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractTypeWriterControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterControllerAR|typeof xyz.swapee.wc.back.TypeWriterControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterController|typeof xyz.swapee.wc.TypeWriterController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.AbstractTypeWriterControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese[]) => xyz.swapee.wc.back.ITypeWriterControllerAR} xyz.swapee.wc.back.TypeWriterControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITypeWriterControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITypeWriterController)} xyz.swapee.wc.back.ITypeWriterControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _ITypeWriterControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.ITypeWriterControllerAR
 */
xyz.swapee.wc.back.ITypeWriterControllerAR = class extends /** @type {xyz.swapee.wc.back.ITypeWriterControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITypeWriterController.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITypeWriterControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese>)} xyz.swapee.wc.back.TypeWriterControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterControllerAR} xyz.swapee.wc.back.ITypeWriterControllerAR.typeof */
/**
 * A concrete class of _ITypeWriterControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.TypeWriterControllerAR
 * @implements {xyz.swapee.wc.back.ITypeWriterControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _ITypeWriterControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.TypeWriterControllerAR = class extends /** @type {xyz.swapee.wc.back.TypeWriterControllerAR.constructor&xyz.swapee.wc.back.ITypeWriterControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TypeWriterControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterControllerAR}
 */
xyz.swapee.wc.back.TypeWriterControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITypeWriterControllerAR} */
xyz.swapee.wc.back.RecordITypeWriterControllerAR

/** @typedef {xyz.swapee.wc.back.ITypeWriterControllerAR} xyz.swapee.wc.back.BoundITypeWriterControllerAR */

/** @typedef {xyz.swapee.wc.back.TypeWriterControllerAR} xyz.swapee.wc.back.BoundTypeWriterControllerAR */

/**
 * Contains getters to cast the _ITypeWriterControllerAR_ interface.
 * @interface xyz.swapee.wc.back.ITypeWriterControllerARCaster
 */
xyz.swapee.wc.back.ITypeWriterControllerARCaster = class { }
/**
 * Cast the _ITypeWriterControllerAR_ instance into the _BoundITypeWriterControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundITypeWriterControllerAR}
 */
xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.asITypeWriterControllerAR
/**
 * Access the _TypeWriterControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTypeWriterControllerAR}
 */
xyz.swapee.wc.back.ITypeWriterControllerARCaster.prototype.superTypeWriterControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/54-ITypeWriterControllerAT.xml}  d6e9748744c7dda41cb1aa197b458ce7 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TypeWriterControllerAT)} xyz.swapee.wc.front.AbstractTypeWriterControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TypeWriterControllerAT} xyz.swapee.wc.front.TypeWriterControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITypeWriterControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractTypeWriterControllerAT
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractTypeWriterControllerAT.constructor&xyz.swapee.wc.front.TypeWriterControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractTypeWriterControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterControllerAT|typeof xyz.swapee.wc.front.TypeWriterControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.AbstractTypeWriterControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese[]) => xyz.swapee.wc.front.ITypeWriterControllerAT} xyz.swapee.wc.front.TypeWriterControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITypeWriterControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.ITypeWriterControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITypeWriterControllerAR_ trait.
 * @interface xyz.swapee.wc.front.ITypeWriterControllerAT
 */
xyz.swapee.wc.front.ITypeWriterControllerAT = class extends /** @type {xyz.swapee.wc.front.ITypeWriterControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITypeWriterControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese>)} xyz.swapee.wc.front.TypeWriterControllerAT.constructor */
/**
 * A concrete class of _ITypeWriterControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.TypeWriterControllerAT
 * @implements {xyz.swapee.wc.front.ITypeWriterControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _ITypeWriterControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.TypeWriterControllerAT = class extends /** @type {xyz.swapee.wc.front.TypeWriterControllerAT.constructor&xyz.swapee.wc.front.ITypeWriterControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TypeWriterControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterControllerAT}
 */
xyz.swapee.wc.front.TypeWriterControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITypeWriterControllerAT} */
xyz.swapee.wc.front.RecordITypeWriterControllerAT

/** @typedef {xyz.swapee.wc.front.ITypeWriterControllerAT} xyz.swapee.wc.front.BoundITypeWriterControllerAT */

/** @typedef {xyz.swapee.wc.front.TypeWriterControllerAT} xyz.swapee.wc.front.BoundTypeWriterControllerAT */

/**
 * Contains getters to cast the _ITypeWriterControllerAT_ interface.
 * @interface xyz.swapee.wc.front.ITypeWriterControllerATCaster
 */
xyz.swapee.wc.front.ITypeWriterControllerATCaster = class { }
/**
 * Cast the _ITypeWriterControllerAT_ instance into the _BoundITypeWriterControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundITypeWriterControllerAT}
 */
xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.asITypeWriterControllerAT
/**
 * Access the _TypeWriterControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTypeWriterControllerAT}
 */
xyz.swapee.wc.front.ITypeWriterControllerATCaster.prototype.superTypeWriterControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreen.xml}  14227cdb0561038ce2ee6c141192529b */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null>&xyz.swapee.wc.ITypeWriterDisplay.Initialese} xyz.swapee.wc.ITypeWriterScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterScreen)} xyz.swapee.wc.AbstractTypeWriterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterScreen} xyz.swapee.wc.TypeWriterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterScreen` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterScreen
 */
xyz.swapee.wc.AbstractTypeWriterScreen = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterScreen.constructor&xyz.swapee.wc.TypeWriterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterScreen.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterScreen.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.ITypeWriterController|typeof xyz.swapee.wc.front.TypeWriterController)|(!xyz.swapee.wc.ITypeWriterDisplay|typeof xyz.swapee.wc.TypeWriterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.AbstractTypeWriterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterScreen.Initialese[]) => xyz.swapee.wc.ITypeWriterScreen} xyz.swapee.wc.TypeWriterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.TypeWriterMemory, !xyz.swapee.wc.front.TypeWriterInputs, !HTMLDivElement, !xyz.swapee.wc.ITypeWriterDisplay.Settings, !xyz.swapee.wc.ITypeWriterDisplay.Queries, null, null>&xyz.swapee.wc.front.ITypeWriterController&xyz.swapee.wc.ITypeWriterDisplay)} xyz.swapee.wc.ITypeWriterScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.ITypeWriterScreen
 */
xyz.swapee.wc.ITypeWriterScreen = class extends /** @type {xyz.swapee.wc.ITypeWriterScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.ITypeWriterController.typeof&xyz.swapee.wc.ITypeWriterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterScreen.Initialese>)} xyz.swapee.wc.TypeWriterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ITypeWriterScreen} xyz.swapee.wc.ITypeWriterScreen.typeof */
/**
 * A concrete class of _ITypeWriterScreen_ instances.
 * @constructor xyz.swapee.wc.TypeWriterScreen
 * @implements {xyz.swapee.wc.ITypeWriterScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterScreen.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterScreen = class extends /** @type {xyz.swapee.wc.TypeWriterScreen.constructor&xyz.swapee.wc.ITypeWriterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterScreen}
 */
xyz.swapee.wc.TypeWriterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.ITypeWriterScreen} */
xyz.swapee.wc.RecordITypeWriterScreen

/** @typedef {xyz.swapee.wc.ITypeWriterScreen} xyz.swapee.wc.BoundITypeWriterScreen */

/** @typedef {xyz.swapee.wc.TypeWriterScreen} xyz.swapee.wc.BoundTypeWriterScreen */

/**
 * Contains getters to cast the _ITypeWriterScreen_ interface.
 * @interface xyz.swapee.wc.ITypeWriterScreenCaster
 */
xyz.swapee.wc.ITypeWriterScreenCaster = class { }
/**
 * Cast the _ITypeWriterScreen_ instance into the _BoundITypeWriterScreen_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterScreen}
 */
xyz.swapee.wc.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/**
 * Access the _TypeWriterScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterScreen}
 */
xyz.swapee.wc.ITypeWriterScreenCaster.prototype.superTypeWriterScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/70-ITypeWriterScreenBack.xml}  ab35938b8e9911e6e67c3e1677580cbc */
/** @typedef {xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} xyz.swapee.wc.back.ITypeWriterScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TypeWriterScreen)} xyz.swapee.wc.back.AbstractTypeWriterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TypeWriterScreen} xyz.swapee.wc.back.TypeWriterScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITypeWriterScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractTypeWriterScreen
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen = class extends /** @type {xyz.swapee.wc.back.AbstractTypeWriterScreen.constructor&xyz.swapee.wc.back.TypeWriterScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTypeWriterScreen.prototype.constructor = xyz.swapee.wc.back.AbstractTypeWriterScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreen|typeof xyz.swapee.wc.back.TypeWriterScreen)|(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITypeWriterScreen.Initialese[]) => xyz.swapee.wc.back.ITypeWriterScreen} xyz.swapee.wc.back.TypeWriterScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITypeWriterScreenCaster&xyz.swapee.wc.back.ITypeWriterScreenAT)} xyz.swapee.wc.back.ITypeWriterScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ITypeWriterScreenAT} xyz.swapee.wc.back.ITypeWriterScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.ITypeWriterScreen
 */
xyz.swapee.wc.back.ITypeWriterScreen = class extends /** @type {xyz.swapee.wc.back.ITypeWriterScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.ITypeWriterScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITypeWriterScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreen.Initialese>)} xyz.swapee.wc.back.TypeWriterScreen.constructor */
/**
 * A concrete class of _ITypeWriterScreen_ instances.
 * @constructor xyz.swapee.wc.back.TypeWriterScreen
 * @implements {xyz.swapee.wc.back.ITypeWriterScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.TypeWriterScreen = class extends /** @type {xyz.swapee.wc.back.TypeWriterScreen.constructor&xyz.swapee.wc.back.ITypeWriterScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TypeWriterScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreen}
 */
xyz.swapee.wc.back.TypeWriterScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITypeWriterScreen} */
xyz.swapee.wc.back.RecordITypeWriterScreen

/** @typedef {xyz.swapee.wc.back.ITypeWriterScreen} xyz.swapee.wc.back.BoundITypeWriterScreen */

/** @typedef {xyz.swapee.wc.back.TypeWriterScreen} xyz.swapee.wc.back.BoundTypeWriterScreen */

/**
 * Contains getters to cast the _ITypeWriterScreen_ interface.
 * @interface xyz.swapee.wc.back.ITypeWriterScreenCaster
 */
xyz.swapee.wc.back.ITypeWriterScreenCaster = class { }
/**
 * Cast the _ITypeWriterScreen_ instance into the _BoundITypeWriterScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundITypeWriterScreen}
 */
xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.asITypeWriterScreen
/**
 * Access the _TypeWriterScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTypeWriterScreen}
 */
xyz.swapee.wc.back.ITypeWriterScreenCaster.prototype.superTypeWriterScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/73-ITypeWriterScreenAR.xml}  fd9013389e7824cef26bde83de80dd3e */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.ITypeWriterScreen.Initialese} xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.TypeWriterScreenAR)} xyz.swapee.wc.front.AbstractTypeWriterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.TypeWriterScreenAR} xyz.swapee.wc.front.TypeWriterScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.ITypeWriterScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractTypeWriterScreenAR
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractTypeWriterScreenAR.constructor&xyz.swapee.wc.front.TypeWriterScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractTypeWriterScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractTypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.ITypeWriterScreenAR|typeof xyz.swapee.wc.front.TypeWriterScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.ITypeWriterScreen|typeof xyz.swapee.wc.TypeWriterScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.AbstractTypeWriterScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese[]) => xyz.swapee.wc.front.ITypeWriterScreenAR} xyz.swapee.wc.front.TypeWriterScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.ITypeWriterScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.ITypeWriterScreen)} xyz.swapee.wc.front.ITypeWriterScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _ITypeWriterScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.ITypeWriterScreenAR
 */
xyz.swapee.wc.front.ITypeWriterScreenAR = class extends /** @type {xyz.swapee.wc.front.ITypeWriterScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.ITypeWriterScreen.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ITypeWriterScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.ITypeWriterScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese>)} xyz.swapee.wc.front.TypeWriterScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ITypeWriterScreenAR} xyz.swapee.wc.front.ITypeWriterScreenAR.typeof */
/**
 * A concrete class of _ITypeWriterScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.TypeWriterScreenAR
 * @implements {xyz.swapee.wc.front.ITypeWriterScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _ITypeWriterScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.TypeWriterScreenAR = class extends /** @type {xyz.swapee.wc.front.TypeWriterScreenAR.constructor&xyz.swapee.wc.front.ITypeWriterScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.ITypeWriterScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.TypeWriterScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.TypeWriterScreenAR}
 */
xyz.swapee.wc.front.TypeWriterScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.ITypeWriterScreenAR} */
xyz.swapee.wc.front.RecordITypeWriterScreenAR

/** @typedef {xyz.swapee.wc.front.ITypeWriterScreenAR} xyz.swapee.wc.front.BoundITypeWriterScreenAR */

/** @typedef {xyz.swapee.wc.front.TypeWriterScreenAR} xyz.swapee.wc.front.BoundTypeWriterScreenAR */

/**
 * Contains getters to cast the _ITypeWriterScreenAR_ interface.
 * @interface xyz.swapee.wc.front.ITypeWriterScreenARCaster
 */
xyz.swapee.wc.front.ITypeWriterScreenARCaster = class { }
/**
 * Cast the _ITypeWriterScreenAR_ instance into the _BoundITypeWriterScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundITypeWriterScreenAR}
 */
xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.asITypeWriterScreenAR
/**
 * Access the _TypeWriterScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundTypeWriterScreenAR}
 */
xyz.swapee.wc.front.ITypeWriterScreenARCaster.prototype.superTypeWriterScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/74-ITypeWriterScreenAT.xml}  d224c62115749f710a925a46a2fc6402 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.TypeWriterScreenAT)} xyz.swapee.wc.back.AbstractTypeWriterScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.TypeWriterScreenAT} xyz.swapee.wc.back.TypeWriterScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.ITypeWriterScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractTypeWriterScreenAT
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractTypeWriterScreenAT.constructor&xyz.swapee.wc.back.TypeWriterScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractTypeWriterScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractTypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.ITypeWriterScreenAT|typeof xyz.swapee.wc.back.TypeWriterScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.AbstractTypeWriterScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese[]) => xyz.swapee.wc.back.ITypeWriterScreenAT} xyz.swapee.wc.back.TypeWriterScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.ITypeWriterScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.ITypeWriterScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITypeWriterScreenAR_ trait.
 * @interface xyz.swapee.wc.back.ITypeWriterScreenAT
 */
xyz.swapee.wc.back.ITypeWriterScreenAT = class extends /** @type {xyz.swapee.wc.back.ITypeWriterScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ITypeWriterScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.ITypeWriterScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese>)} xyz.swapee.wc.back.TypeWriterScreenAT.constructor */
/**
 * A concrete class of _ITypeWriterScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.TypeWriterScreenAT
 * @implements {xyz.swapee.wc.back.ITypeWriterScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _ITypeWriterScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.TypeWriterScreenAT = class extends /** @type {xyz.swapee.wc.back.TypeWriterScreenAT.constructor&xyz.swapee.wc.back.ITypeWriterScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.ITypeWriterScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.TypeWriterScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.TypeWriterScreenAT}
 */
xyz.swapee.wc.back.TypeWriterScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.ITypeWriterScreenAT} */
xyz.swapee.wc.back.RecordITypeWriterScreenAT

/** @typedef {xyz.swapee.wc.back.ITypeWriterScreenAT} xyz.swapee.wc.back.BoundITypeWriterScreenAT */

/** @typedef {xyz.swapee.wc.back.TypeWriterScreenAT} xyz.swapee.wc.back.BoundTypeWriterScreenAT */

/**
 * Contains getters to cast the _ITypeWriterScreenAT_ interface.
 * @interface xyz.swapee.wc.back.ITypeWriterScreenATCaster
 */
xyz.swapee.wc.back.ITypeWriterScreenATCaster = class { }
/**
 * Cast the _ITypeWriterScreenAT_ instance into the _BoundITypeWriterScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundITypeWriterScreenAT}
 */
xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.asITypeWriterScreenAT
/**
 * Access the _TypeWriterScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundTypeWriterScreenAT}
 */
xyz.swapee.wc.back.ITypeWriterScreenATCaster.prototype.superTypeWriterScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/type-writer/TypeWriter.mvc/design/80-ITypeWriterGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.ITypeWriterDisplay.Initialese} xyz.swapee.wc.ITypeWriterGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.TypeWriterGPU)} xyz.swapee.wc.AbstractTypeWriterGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.TypeWriterGPU} xyz.swapee.wc.TypeWriterGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterGPU` interface.
 * @constructor xyz.swapee.wc.AbstractTypeWriterGPU
 */
xyz.swapee.wc.AbstractTypeWriterGPU = class extends /** @type {xyz.swapee.wc.AbstractTypeWriterGPU.constructor&xyz.swapee.wc.TypeWriterGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractTypeWriterGPU.prototype.constructor = xyz.swapee.wc.AbstractTypeWriterGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractTypeWriterGPU.class = /** @type {typeof xyz.swapee.wc.AbstractTypeWriterGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractTypeWriterGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractTypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.ITypeWriterGPU|typeof xyz.swapee.wc.TypeWriterGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.ITypeWriterDisplay|typeof xyz.swapee.wc.back.TypeWriterDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.AbstractTypeWriterGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.ITypeWriterGPU.Initialese[]) => xyz.swapee.wc.ITypeWriterGPU} xyz.swapee.wc.TypeWriterGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterGPUFields&engineering.type.IEngineer&xyz.swapee.wc.ITypeWriterGPUCaster&com.webcircuits.IBrowserView<.!TypeWriterMemory,>&xyz.swapee.wc.back.ITypeWriterDisplay)} xyz.swapee.wc.ITypeWriterGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!TypeWriterMemory,>} com.webcircuits.IBrowserView<.!TypeWriterMemory,>.typeof */
/**
 * Handles the periphery of the _ITypeWriterDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.ITypeWriterGPU
 */
xyz.swapee.wc.ITypeWriterGPU = class extends /** @type {xyz.swapee.wc.ITypeWriterGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!TypeWriterMemory,>.typeof&xyz.swapee.wc.back.ITypeWriterDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *ITypeWriterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ITypeWriterGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.ITypeWriterGPU&engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterGPU.Initialese>)} xyz.swapee.wc.TypeWriterGPU.constructor */
/**
 * A concrete class of _ITypeWriterGPU_ instances.
 * @constructor xyz.swapee.wc.TypeWriterGPU
 * @implements {xyz.swapee.wc.ITypeWriterGPU} Handles the periphery of the _ITypeWriterDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.ITypeWriterGPU.Initialese>} ‎
 */
xyz.swapee.wc.TypeWriterGPU = class extends /** @type {xyz.swapee.wc.TypeWriterGPU.constructor&xyz.swapee.wc.ITypeWriterGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *ITypeWriterGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *ITypeWriterGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.ITypeWriterGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.TypeWriterGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.TypeWriterGPU}
 */
xyz.swapee.wc.TypeWriterGPU.__extend = function(...Extensions) {}

/**
 * Fields of the ITypeWriterGPU.
 * @interface xyz.swapee.wc.ITypeWriterGPUFields
 */
xyz.swapee.wc.ITypeWriterGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.ITypeWriterGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.ITypeWriterGPU} */
xyz.swapee.wc.RecordITypeWriterGPU

/** @typedef {xyz.swapee.wc.ITypeWriterGPU} xyz.swapee.wc.BoundITypeWriterGPU */

/** @typedef {xyz.swapee.wc.TypeWriterGPU} xyz.swapee.wc.BoundTypeWriterGPU */

/**
 * Contains getters to cast the _ITypeWriterGPU_ interface.
 * @interface xyz.swapee.wc.ITypeWriterGPUCaster
 */
xyz.swapee.wc.ITypeWriterGPUCaster = class { }
/**
 * Cast the _ITypeWriterGPU_ instance into the _BoundITypeWriterGPU_ type.
 * @type {!xyz.swapee.wc.BoundITypeWriterGPU}
 */
xyz.swapee.wc.ITypeWriterGPUCaster.prototype.asITypeWriterGPU
/**
 * Access the _TypeWriterGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundTypeWriterGPU}
 */
xyz.swapee.wc.ITypeWriterGPUCaster.prototype.superTypeWriterGPU

// nss:xyz.swapee.wc
/* @typal-end */