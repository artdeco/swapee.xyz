import {TypeWriterMemoryPQs} from './TypeWriterMemoryPQs'
export const TypeWriterMemoryQPs=/**@type {!xyz.swapee.wc.TypeWriterMemoryQPs}*/(Object.keys(TypeWriterMemoryPQs)
 .reduce((a,k)=>{a[TypeWriterMemoryPQs[k]]=k;return a},{}))