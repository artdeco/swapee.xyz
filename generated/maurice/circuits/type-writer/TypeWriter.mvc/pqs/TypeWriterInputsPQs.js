import {TypeWriterMemoryPQs} from './TypeWriterMemoryPQs'
export const TypeWriterInputsPQs=/**@type {!xyz.swapee.wc.TypeWriterInputsQPs}*/({
 ...TypeWriterMemoryPQs,
})