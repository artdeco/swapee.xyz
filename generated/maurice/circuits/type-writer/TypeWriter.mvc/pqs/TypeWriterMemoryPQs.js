export const TypeWriterMemoryPQs=/**@type {!xyz.swapee.wc.TypeWriterMemoryPQs}*/({
 phrases:'5a196',
 hold:'af1d8',
 holdClear:'57bdd',
 enterDelay:'3b40c',
 eraseDelay:'26e0c',
 currentPhrase:'53af0',
 nextPhrase:'25444',
 nextIndex:'b6f75',
})