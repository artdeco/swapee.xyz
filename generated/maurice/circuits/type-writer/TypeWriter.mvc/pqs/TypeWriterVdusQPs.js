import {TypeWriterVdusPQs} from './TypeWriterVdusPQs'
export const TypeWriterVdusQPs=/**@type {!xyz.swapee.wc.TypeWriterVdusQPs}*/(Object.keys(TypeWriterVdusPQs)
 .reduce((a,k)=>{a[TypeWriterVdusPQs[k]]=k;return a},{}))