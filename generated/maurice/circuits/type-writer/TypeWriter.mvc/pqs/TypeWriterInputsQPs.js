import {TypeWriterInputsPQs} from './TypeWriterInputsPQs'
export const TypeWriterInputsQPs=/**@type {!xyz.swapee.wc.TypeWriterInputsQPs}*/(Object.keys(TypeWriterInputsPQs)
 .reduce((a,k)=>{a[TypeWriterInputsPQs[k]]=k;return a},{}))