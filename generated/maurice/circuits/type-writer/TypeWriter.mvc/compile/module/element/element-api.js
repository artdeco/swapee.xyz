import { TypeWriterController, TypeWriterElement, TypeWriterPort,
 AbstractTypeWriterController, AbstractTypeWriterComputer, AbstractTypeWriter,
 TypeWriterBuffer } from './element-exports'

/** @lazy @api {xyz.swapee.wc.TypeWriterController} */
export { TypeWriterController }
/** @lazy @api {xyz.swapee.wc.TypeWriterElement} */
export { TypeWriterElement }
/** @lazy @api {xyz.swapee.wc.TypeWriterPort} */
export { TypeWriterPort }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriterController} */
export { AbstractTypeWriterController }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriterComputer} */
export { AbstractTypeWriterComputer }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriter} */
export { AbstractTypeWriter }
/** @lazy @api {xyz.swapee.wc.TypeWriterBuffer} */
export { TypeWriterBuffer }