import { TypeWriterDisplay, TypeWriterScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.TypeWriterDisplay} */
export { TypeWriterDisplay }
/** @lazy @api {xyz.swapee.wc.TypeWriterScreen} */
export { TypeWriterScreen }