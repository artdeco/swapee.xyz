import { TypeWriterComputer, TypeWriterController, TypeWriterProcessor,
 TypeWriterHtmlComponent, TypeWriterPort, AbstractTypeWriterController,
 AbstractTypeWriterComputer, AbstractTypeWriter, TypeWriterBuffer } from './back-exports'

/** @lazy @api {xyz.swapee.wc.TypeWriterComputer} */
export { TypeWriterComputer }
/** @lazy @api {xyz.swapee.wc.back.TypeWriterController} */
export { TypeWriterController }
/** @lazy @api {xyz.swapee.wc.TypeWriterProcessor} */
export { TypeWriterProcessor }
/** @lazy @api {xyz.swapee.wc.TypeWriterHtmlComponent} */
export { TypeWriterHtmlComponent }
/** @lazy @api {xyz.swapee.wc.TypeWriterPort} */
export { TypeWriterPort }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriterController} */
export { AbstractTypeWriterController }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriterComputer} */
export { AbstractTypeWriterComputer }
/** @lazy @api {xyz.swapee.wc.AbstractTypeWriter} */
export { AbstractTypeWriter }
/** @lazy @api {xyz.swapee.wc.TypeWriterBuffer} */
export { TypeWriterBuffer }