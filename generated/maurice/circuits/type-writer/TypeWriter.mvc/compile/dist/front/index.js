/**
 * Display for presenting information from the _ITypeWriter_.
 * @extends {xyz.swapee.wc.TypeWriterDisplay}
 */
class TypeWriterDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.TypeWriterScreen}
 */
class TypeWriterScreen extends (class {/* lazy-loaded */}) {}

module.exports.TypeWriterDisplay = TypeWriterDisplay
module.exports.TypeWriterScreen = TypeWriterScreen