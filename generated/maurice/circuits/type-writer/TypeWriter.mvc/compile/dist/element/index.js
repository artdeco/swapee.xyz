/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.TypeWriterController}
 */
class TypeWriterController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _ITypeWriter_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.TypeWriterElement}
 */
class TypeWriterElement extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITypeWriter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TypeWriterPort}
 */
class TypeWriterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterController` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriterController}
 */
class AbstractTypeWriterController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriterComputer}
 */
class AbstractTypeWriterComputer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriter` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriter}
 */
class AbstractTypeWriter extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TypeWriterBuffer}
 */
class TypeWriterBuffer extends (class {/* lazy-loaded */}) {}

module.exports.TypeWriterController = TypeWriterController
module.exports.TypeWriterElement = TypeWriterElement
module.exports.TypeWriterPort = TypeWriterPort
module.exports.AbstractTypeWriterController = AbstractTypeWriterController
module.exports.AbstractTypeWriterComputer = AbstractTypeWriterComputer
module.exports.AbstractTypeWriter = AbstractTypeWriter
module.exports.TypeWriterBuffer = TypeWriterBuffer

Object.defineProperties(module.exports, {
 'TypeWriterController': {get: () => require('./precompile/internal')[605319507061]},
 [605319507061]: {get: () => module.exports['TypeWriterController']},
 'TypeWriterElement': {get: () => require('./precompile/internal')[60531950708]},
 [60531950708]: {get: () => module.exports['TypeWriterElement']},
 'TypeWriterPort': {get: () => require('./precompile/internal')[60531950703]},
 [60531950703]: {get: () => module.exports['TypeWriterPort']},
 'AbstractTypeWriterController': {get: () => require('./precompile/internal')[60531950704]},
 [60531950704]: {get: () => module.exports['AbstractTypeWriterController']},
 'AbstractTypeWriterComputer': {get: () => require('./precompile/internal')[605319507030]},
 [605319507030]: {get: () => module.exports['AbstractTypeWriterComputer']},
 'AbstractTypeWriter': {get: () => require('./precompile/internal')[60531950701]},
 [60531950701]: {get: () => module.exports['AbstractTypeWriter']},
 'TypeWriterBuffer': {get: () => require('./precompile/internal')[605319507011]},
 [605319507011]: {get: () => module.exports['TypeWriterBuffer']},
})