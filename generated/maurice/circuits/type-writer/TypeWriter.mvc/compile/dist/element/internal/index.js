import Module from './element'

/** @type {typeof xyz.swapee.wc.TypeWriterController} */
export const TypeWriterController=Module['605319507061']
/** @type {typeof xyz.swapee.wc.TypeWriterElement} */
export const TypeWriterElement=Module['60531950708']
/** @type {typeof xyz.swapee.wc.TypeWriterPort} */
export const TypeWriterPort=Module['60531950703']
/**@extends {xyz.swapee.wc.AbstractTypeWriterController}*/
export class AbstractTypeWriterController extends Module['60531950704'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterController} */
AbstractTypeWriterController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractTypeWriterComputer}*/
export class AbstractTypeWriterComputer extends Module['605319507030'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterComputer} */
AbstractTypeWriterComputer.class=function(){}
/**@extends {xyz.swapee.wc.AbstractTypeWriter}*/
export class AbstractTypeWriter extends Module['60531950701'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriter} */
AbstractTypeWriter.class=function(){}
/** @type {typeof xyz.swapee.wc.TypeWriterBuffer} */
export const TypeWriterBuffer=Module['605319507011']