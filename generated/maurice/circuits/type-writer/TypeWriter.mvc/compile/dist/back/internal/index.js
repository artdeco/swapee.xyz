import Module from './browser'

/** @type {typeof xyz.swapee.wc.TypeWriterComputer} */
export const TypeWriterComputer=Module['605319507031']
/** @type {typeof xyz.swapee.wc.back.TypeWriterController} */
export const TypeWriterController=Module['605319507061']
/** @type {typeof xyz.swapee.wc.TypeWriterProcessor} */
export const TypeWriterProcessor=Module['605319507051']
/** @type {typeof xyz.swapee.wc.TypeWriterHtmlComponent} */
export const TypeWriterHtmlComponent=Module['605319507010']
/** @type {typeof xyz.swapee.wc.TypeWriterPort} */
export const TypeWriterPort=Module['60531950703']
/**@extends {xyz.swapee.wc.AbstractTypeWriterController}*/
export class AbstractTypeWriterController extends Module['60531950704'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterController} */
AbstractTypeWriterController.class=function(){}
/**@extends {xyz.swapee.wc.AbstractTypeWriterComputer}*/
export class AbstractTypeWriterComputer extends Module['605319507030'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriterComputer} */
AbstractTypeWriterComputer.class=function(){}
/**@extends {xyz.swapee.wc.AbstractTypeWriter}*/
export class AbstractTypeWriter extends Module['60531950701'] {}
/** @type {typeof xyz.swapee.wc.AbstractTypeWriter} */
AbstractTypeWriter.class=function(){}
/** @type {typeof xyz.swapee.wc.TypeWriterBuffer} */
export const TypeWriterBuffer=Module['605319507011']