/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.TypeWriterComputer}
 */
class TypeWriterComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.TypeWriterController}
 */
class TypeWriterController extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _ITypeWriter_.
 * @extends {xyz.swapee.wc.TypeWriterProcessor}
 */
class TypeWriterProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The _ITypeWriter_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.TypeWriterHtmlComponent}
 */
class TypeWriterHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _ITypeWriter_, providing input
 * pins.
 * @extends {xyz.swapee.wc.TypeWriterPort}
 */
class TypeWriterPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterController` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriterController}
 */
class AbstractTypeWriterController extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriterComputer` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriterComputer}
 */
class AbstractTypeWriterComputer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.ITypeWriter` interface.
 * @extends {xyz.swapee.wc.AbstractTypeWriter}
 */
class AbstractTypeWriter extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.TypeWriterBuffer}
 */
class TypeWriterBuffer extends (class {/* lazy-loaded */}) {}

module.exports.TypeWriterComputer = TypeWriterComputer
module.exports.TypeWriterController = TypeWriterController
module.exports.TypeWriterProcessor = TypeWriterProcessor
module.exports.TypeWriterHtmlComponent = TypeWriterHtmlComponent
module.exports.TypeWriterPort = TypeWriterPort
module.exports.AbstractTypeWriterController = AbstractTypeWriterController
module.exports.AbstractTypeWriterComputer = AbstractTypeWriterComputer
module.exports.AbstractTypeWriter = AbstractTypeWriter
module.exports.TypeWriterBuffer = TypeWriterBuffer