import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {6053195070} */
function aa({g:a,h:b}){return{i:b[a]}};function ba(a,b,e){a={g:a.g,h:a.h};a=e?e(a):a;b=e?e(b):b;return this.v(a,b)};/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function f(a,b,e,m){return c["372700389812"](a,b,e,m,!1,void 0)};
const g=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=g["61505580523"],da=g["61505580526"],ea=g["615055805212"],fa=g["615055805218"],ha=g["615055805221"],ia=g["615055805223"],h=g["615055805233"];function k(){}k.prototype={};class ja{}class l extends f(ja,60531950701,null,{F:1,T:2}){}l[d]=[k,fa];class n extends l.implements({v:aa,adapt:[ba]}){};
const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const ka=p["12817393923"],la=p["12817393924"],ma=p["12817393925"],na=p["12817393926"];function q(){}q.prototype={};class oa{}class r extends f(oa,605319507021,null,{G:1,U:2}){}r[d]=[q,na,{allocator(){this.methods={next:"684d2"}}}];const t={regulate:ea({h:[String],m:Number,o:Number,j:Number,l:Number,u:String,i:String,g:Number})};

const u=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const pa=u.IntegratedController,qa=u.Parametric;const v={h:"5a196",m:"af1d8",o:"57bdd",j:"3b40c",l:"26e0c",u:"53af0",i:"25444",g:"b6f75"};const w={...v};function x(){}x.prototype={};class ra{}class y extends f(ra,60531950706,null,{A:1,V:2}){}function z(){}z.prototype={};function A(){this.model={h:[],m:1250,o:100,j:100,l:100,u:"",i:"",g:1}}class sa{}class B extends f(sa,60531950703,A,{K:1,Z:2}){}B[d]=[z,{constructor(){h(this.model,"",v)}}];y[d]=[{},x,B];function C(){}C.prototype={};function ta(){const a={model:null};A.call(a);this.inputs=a.model}class ua{}class D extends f(ua,60531950705,ta,{L:1,$:2}){}function E(){}D[d]=[E.prototype={},C,qa,E.prototype={constructor(){h(this.inputs,"",w)}}];function F(){}F.prototype={};class va{}class G extends f(va,605319507018,null,{s:1,C:2}){}G[d]=[{},F,t,pa,{get Port(){return D}}];function H(){}H.prototype={};class wa{}class I extends f(wa,605319507020,null,{s:1,C:2}){}I[d]=[H,G,r,ka];var J=class extends I.implements(){};function xa(){const {s:{setInputs:a},A:{model:{h:b,g:e}}}=this;a({g:(e+1)%b.length})};function K(){}K.prototype={};class ya{}class L extends f(ya,60531950707,null,{M:1,aa:2}){}L[d]=[K];var M=class extends L.implements({next:xa}){};const za=da.__trait({paint:function({i:a,j:b,l:e,m,o:Aa}){const {asIGraphicsDriverBack:{serMemory:Ba,t_pa:Ca}}=this;a=Ba({i:a,j:b,l:e,m,o:Aa});Ca({pid:"7d382a8",mem:a})}});
const N=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const Da=N.IntegratedComponentInitialiser,Ea=N.IntegratedComponent;function O(){}O.prototype={};class Fa{}class P extends f(Fa,605319507017,null,{H:1,W:2}){}P[d]=[O,la];const Q={};const Ga=Object.keys(Q).reduce((a,b)=>{a[Q[b]]=b;return a},{});function R(){}R.prototype={};class Ha{}class S extends f(Ha,605319507014,null,{I:1,X:2}){}S[d]=[R,{vdusQPs:Ga,memoryPQs:v},P,ca];function T(){}T.prototype={};class Ia{}class U extends f(Ia,605319507026,null,{P:1,ca:2}){}U[d]=[T,ma];function V(){}V.prototype={};class Ja{}class W extends f(Ja,605319507024,null,{O:1,ba:2}){}W[d]=[V,U];const Ka=Object.keys(w).reduce((a,b)=>{a[w[b]]=b;return a},{});function X(){}X.prototype={};class La{}class Y extends f(La,60531950708,null,{D:1,R:2}){}Y[d]=[X,y,L,Ea,l,G];function Z(){}Z.prototype={};class Ma{static mvc(a,b,e){return ia(this,a,b,null,e)}}class Na extends f(Ma,605319507010,null,{J:1,Y:2}){}Na[d]=[Z,ha,Y,S,W,{inputsQPs:Ka}];var Oa=class extends Na.implements(J,n,M,za,Da,{}){};module.exports["605319507031"]=n;module.exports["605319507061"]=J;module.exports["605319507051"]=M;module.exports["605319507010"]=Oa;module.exports["60531950703"]=D;module.exports["60531950704"]=G;module.exports["605319507030"]=l;module.exports["60531950700"]=Y;module.exports["60531950701"]=Y;module.exports["605319507011"]=t;
/*! @embed-object-end {6053195070} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule