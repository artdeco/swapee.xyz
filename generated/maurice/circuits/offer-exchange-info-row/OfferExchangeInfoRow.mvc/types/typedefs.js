/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IOfferExchangeInfoRowComputer={}
xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute={}
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore={}
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model={}
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core={}
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel={}
xyz.swapee.wc.IOfferExchangeInfoRowPort={}
xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs={}
xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs={}
xyz.swapee.wc.IOfferExchangeInfoRowCore={}
xyz.swapee.wc.IOfferExchangeInfoRowCore.Model={}
xyz.swapee.wc.IOfferExchangeInfoRowPortInterface={}
xyz.swapee.wc.IOfferExchangeInfoRowProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IOfferExchangeInfoRowController={}
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT={}
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR={}
xyz.swapee.wc.IOfferExchangeInfoRow={}
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil={}
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent={}
xyz.swapee.wc.IOfferExchangeInfoRowElement={}
xyz.swapee.wc.IOfferExchangeInfoRowElement.build={}
xyz.swapee.wc.IOfferExchangeInfoRowElement.short={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs={}
xyz.swapee.wc.IOfferExchangeInfoRowDesigner={}
xyz.swapee.wc.IOfferExchangeInfoRowDesigner.communicator={}
xyz.swapee.wc.IOfferExchangeInfoRowDesigner.relay={}
xyz.swapee.wc.IOfferExchangeInfoRowDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay={}
xyz.swapee.wc.back.IOfferExchangeInfoRowController={}
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR={}
xyz.swapee.wc.back.IOfferExchangeInfoRowScreen={}
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT={}
xyz.swapee.wc.IOfferExchangeInfoRowController={}
xyz.swapee.wc.IOfferExchangeInfoRowScreen={}
xyz.swapee.wc.IOfferExchangeInfoRowGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/02-IOfferExchangeInfoRowComputer.xml}  14a191f03d87ed8585fe62231b42b945 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowComputer)} xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer} xyz.swapee.wc.OfferExchangeInfoRowComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowComputer` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.constructor&xyz.swapee.wc.OfferExchangeInfoRowComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowComputer} xyz.swapee.wc.OfferExchangeInfoRowComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.OfferExchangeInfoRowMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.OfferExchangeInfoRowLand>)} xyz.swapee.wc.IOfferExchangeInfoRowComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowComputer
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputer = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute} */
xyz.swapee.wc.IOfferExchangeInfoRowComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowComputer} xyz.swapee.wc.IOfferExchangeInfoRowComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowComputer_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowComputer
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowComputer = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowComputer.constructor&xyz.swapee.wc.IOfferExchangeInfoRowComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.OfferExchangeInfoRowComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowComputer} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowComputer

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowComputer} xyz.swapee.wc.BoundIOfferExchangeInfoRowComputer */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowComputer} xyz.swapee.wc.BoundOfferExchangeInfoRowComputer */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowComputer_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowComputerCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputerCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowComputer_ instance into the _BoundIOfferExchangeInfoRowComputer_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputerCaster.prototype.asIOfferExchangeInfoRowComputer
/**
 * Access the _OfferExchangeInfoRowComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowComputer}
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputerCaster.prototype.superOfferExchangeInfoRowComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.OfferExchangeInfoRowMemory, land: !xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute.Land) => void} xyz.swapee.wc.IOfferExchangeInfoRowComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowComputer.__compute<!xyz.swapee.wc.IOfferExchangeInfoRowComputer>} xyz.swapee.wc.IOfferExchangeInfoRowComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.OfferExchangeInfoRowMemory} mem The memory.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute.Land} land The land.
 * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_ `.`
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker `.`
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/03-IOfferExchangeInfoRowOuterCore.xml}  38232850ea60cc4a5e14b5b80773ce3c */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowOuterCore)} xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore} xyz.swapee.wc.OfferExchangeInfoRowOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.constructor&xyz.swapee.wc.OfferExchangeInfoRowOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreCaster)} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.constructor */
/**
 * The _IOfferExchangeInfoRow_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowOuterCore
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowOuterCore

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowOuterCore} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowOuterCore_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowOuterCore
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore} The _IOfferExchangeInfoRow_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowOuterCore = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowOuterCore.constructor&xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OfferExchangeInfoRowOuterCore.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.OfferExchangeInfoRowOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowOuterCore.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreFields = class { }
/**
 * The _IOfferExchangeInfoRow_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowOuterCore

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore} xyz.swapee.wc.BoundIOfferExchangeInfoRowOuterCore */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowOuterCore} xyz.swapee.wc.BoundOfferExchangeInfoRowOuterCore */

/**
 * The core property.
 * @typedef {string}
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core.core

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model The _IOfferExchangeInfoRow_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel The _IOfferExchangeInfoRow_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowOuterCore_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowOuterCore_ instance into the _BoundIOfferExchangeInfoRowOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreCaster.prototype.asIOfferExchangeInfoRowOuterCore
/**
 * Access the _OfferExchangeInfoRowOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowOuterCore}
 */
xyz.swapee.wc.IOfferExchangeInfoRowOuterCoreCaster.prototype.superOfferExchangeInfoRowOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core The core property (optional overlay).
 * @prop {string} [core=""] The core property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core_Safe The core property (required overlay).
 * @prop {string} core The core property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core The core property (optional overlay).
 * @prop {*} [core=null] The core property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core_Safe The core property (required overlay).
 * @prop {*} core The core property.
 */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core} xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.Core_Safe} xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs.Core_Safe The core property (required overlay). */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core} xyz.swapee.wc.IOfferExchangeInfoRowCore.Model.Core The core property (optional overlay). */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model.Core_Safe} xyz.swapee.wc.IOfferExchangeInfoRowCore.Model.Core_Safe The core property (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/04-IOfferExchangeInfoRowPort.xml}  9e8a2ab8d860e8098859f8a6cbc20b48 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowPort)} xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowPort} xyz.swapee.wc.OfferExchangeInfoRowPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowPort` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowPort
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.constructor&xyz.swapee.wc.OfferExchangeInfoRowPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowPort|typeof xyz.swapee.wc.OfferExchangeInfoRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowPort|typeof xyz.swapee.wc.OfferExchangeInfoRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowPort|typeof xyz.swapee.wc.OfferExchangeInfoRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowPort} xyz.swapee.wc.OfferExchangeInfoRowPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs>)} xyz.swapee.wc.IOfferExchangeInfoRowPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IOfferExchangeInfoRow_, providing input
 * pins.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowPort
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowPort.resetPort} */
xyz.swapee.wc.IOfferExchangeInfoRowPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowPort.resetOfferExchangeInfoRowPort} */
xyz.swapee.wc.IOfferExchangeInfoRowPort.prototype.resetOfferExchangeInfoRowPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowPort} xyz.swapee.wc.IOfferExchangeInfoRowPort.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowPort_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowPort
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowPort} The port that serves as an interface to the _IOfferExchangeInfoRow_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowPort = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowPort.constructor&xyz.swapee.wc.IOfferExchangeInfoRowPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
xyz.swapee.wc.OfferExchangeInfoRowPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowPort.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowPortFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortFields = class { }
/**
 * The inputs to the _IOfferExchangeInfoRow_'s controller via its port.
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowPort

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort} xyz.swapee.wc.BoundIOfferExchangeInfoRowPort */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowPort} xyz.swapee.wc.BoundOfferExchangeInfoRowPort */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel)} xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel} xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IOfferExchangeInfoRow_'s controller via its port.
 * @record xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs.constructor&xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel)} xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs.constructor */
/**
 * The inputs to the _IOfferExchangeInfoRow_'s controller via its port.
 * @record xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs.constructor&xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowPortInterface
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IOfferExchangeInfoRowPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IOfferExchangeInfoRowPortInterface.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowPortInterface

/**
 * A concrete class of _IOfferExchangeInfoRowPortInterface_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowPortInterface
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowPortInterface} The port interface.
 */
xyz.swapee.wc.OfferExchangeInfoRowPortInterface = class extends xyz.swapee.wc.IOfferExchangeInfoRowPortInterface { }
xyz.swapee.wc.OfferExchangeInfoRowPortInterface.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowPortInterface.Props
 * @prop {string} core The core property.
 */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowPort_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowPortCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowPort_ instance into the _BoundIOfferExchangeInfoRowPort_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowPort}
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortCaster.prototype.asIOfferExchangeInfoRowPort
/**
 * Access the _OfferExchangeInfoRowPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowPort}
 */
xyz.swapee.wc.IOfferExchangeInfoRowPortCaster.prototype.superOfferExchangeInfoRowPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInfoRowPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort.__resetPort<!xyz.swapee.wc.IOfferExchangeInfoRowPort>} xyz.swapee.wc.IOfferExchangeInfoRowPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowPort.resetPort} */
/**
 * Resets the _IOfferExchangeInfoRow_ port.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInfoRowPort.__resetOfferExchangeInfoRowPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort.__resetOfferExchangeInfoRowPort<!xyz.swapee.wc.IOfferExchangeInfoRowPort>} xyz.swapee.wc.IOfferExchangeInfoRowPort._resetOfferExchangeInfoRowPort */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowPort.resetOfferExchangeInfoRowPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowPort.resetOfferExchangeInfoRowPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/09-IOfferExchangeInfoRowCore.xml}  c196b987cf2eed93991ac337553e7c23 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IOfferExchangeInfoRowCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowCore)} xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowCore} xyz.swapee.wc.OfferExchangeInfoRowCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowCore` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowCore
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.constructor&xyz.swapee.wc.OfferExchangeInfoRowCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore|typeof xyz.swapee.wc.OfferExchangeInfoRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowCore}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowCoreCaster&xyz.swapee.wc.IOfferExchangeInfoRowOuterCore)} xyz.swapee.wc.IOfferExchangeInfoRowCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowCore
 */
xyz.swapee.wc.IOfferExchangeInfoRowCore = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowCore.resetCore} */
xyz.swapee.wc.IOfferExchangeInfoRowCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowCore.resetOfferExchangeInfoRowCore} */
xyz.swapee.wc.IOfferExchangeInfoRowCore.prototype.resetOfferExchangeInfoRowCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowCore&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowCore.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowCore} xyz.swapee.wc.IOfferExchangeInfoRowCore.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowCore_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowCore
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowCore.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowCore = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowCore.constructor&xyz.swapee.wc.IOfferExchangeInfoRowCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.OfferExchangeInfoRowCore.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowCore}
 */
xyz.swapee.wc.OfferExchangeInfoRowCore.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowCore.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowCoreFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreFields = class { }
/**
 * The _IOfferExchangeInfoRow_'s memory.
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreFields.prototype.props = /** @type {xyz.swapee.wc.IOfferExchangeInfoRowCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowCore} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowCore

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowCore} xyz.swapee.wc.BoundIOfferExchangeInfoRowCore */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowCore} xyz.swapee.wc.BoundOfferExchangeInfoRowCore */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model} xyz.swapee.wc.IOfferExchangeInfoRowCore.Model The _IOfferExchangeInfoRow_'s memory. */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowCore_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowCoreCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowCore_ instance into the _BoundIOfferExchangeInfoRowCore_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowCore}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreCaster.prototype.asIOfferExchangeInfoRowCore
/**
 * Access the _OfferExchangeInfoRowCore_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowCore}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCoreCaster.prototype.superOfferExchangeInfoRowCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInfoRowCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowCore.__resetCore<!xyz.swapee.wc.IOfferExchangeInfoRowCore>} xyz.swapee.wc.IOfferExchangeInfoRowCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowCore.resetCore} */
/**
 * Resets the _IOfferExchangeInfoRow_ core.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInfoRowCore.__resetOfferExchangeInfoRowCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowCore.__resetOfferExchangeInfoRowCore<!xyz.swapee.wc.IOfferExchangeInfoRowCore>} xyz.swapee.wc.IOfferExchangeInfoRowCore._resetOfferExchangeInfoRowCore */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowCore.resetOfferExchangeInfoRowCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCore.resetOfferExchangeInfoRowCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/10-IOfferExchangeInfoRowProcessor.xml}  79c8072e0bd5d47efe8471c592d2ea63 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowProcessor)} xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor} xyz.swapee.wc.OfferExchangeInfoRowProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.constructor&xyz.swapee.wc.OfferExchangeInfoRowProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowCore|typeof xyz.swapee.wc.OfferExchangeInfoRowCore)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowProcessor} xyz.swapee.wc.OfferExchangeInfoRowProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowProcessorCaster&xyz.swapee.wc.IOfferExchangeInfoRowComputer&xyz.swapee.wc.IOfferExchangeInfoRowCore&xyz.swapee.wc.IOfferExchangeInfoRowController)} xyz.swapee.wc.IOfferExchangeInfoRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowController} xyz.swapee.wc.IOfferExchangeInfoRowController.typeof */
/**
 * The processor to compute changes to the memory for the _IOfferExchangeInfoRow_.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowProcessor
 */
xyz.swapee.wc.IOfferExchangeInfoRowProcessor = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowComputer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowCore.typeof&xyz.swapee.wc.IOfferExchangeInfoRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowProcessor} xyz.swapee.wc.IOfferExchangeInfoRowProcessor.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowProcessor_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowProcessor
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowProcessor} The processor to compute changes to the memory for the _IOfferExchangeInfoRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowProcessor = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowProcessor.constructor&xyz.swapee.wc.IOfferExchangeInfoRowProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.OfferExchangeInfoRowProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowProcessor} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowProcessor

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowProcessor} xyz.swapee.wc.BoundIOfferExchangeInfoRowProcessor */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowProcessor} xyz.swapee.wc.BoundOfferExchangeInfoRowProcessor */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowProcessor_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowProcessorCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowProcessorCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowProcessor_ instance into the _BoundIOfferExchangeInfoRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.IOfferExchangeInfoRowProcessorCaster.prototype.asIOfferExchangeInfoRowProcessor
/**
 * Access the _OfferExchangeInfoRowProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.IOfferExchangeInfoRowProcessorCaster.prototype.superOfferExchangeInfoRowProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/100-OfferExchangeInfoRowMemory.xml}  71b3707eecf4c7023585358ff4c1ce03 */
/**
 * The memory of the _IOfferExchangeInfoRow_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.OfferExchangeInfoRowMemory
 */
xyz.swapee.wc.OfferExchangeInfoRowMemory = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.OfferExchangeInfoRowMemory.prototype.core = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/102-OfferExchangeInfoRowInputs.xml}  60058321e161be28b8e4d48111d5d5cd */
/**
 * The inputs of the _IOfferExchangeInfoRow_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.OfferExchangeInfoRowInputs
 */
xyz.swapee.wc.front.OfferExchangeInfoRowInputs = class { }
/**
 * The core property. Default empty string.
 */
xyz.swapee.wc.front.OfferExchangeInfoRowInputs.prototype.core = /** @type {string|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/11-IOfferExchangeInfoRow.xml}  753471e56348af61017ce73176e55a92 */
/**
 * An atomic wrapper for the _IOfferExchangeInfoRow_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.OfferExchangeInfoRowEnv
 */
xyz.swapee.wc.OfferExchangeInfoRowEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.OfferExchangeInfoRowEnv.prototype.offerExchangeInfoRow = /** @type {xyz.swapee.wc.IOfferExchangeInfoRow} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>&xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} xyz.swapee.wc.IOfferExchangeInfoRow.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRow)} xyz.swapee.wc.AbstractOfferExchangeInfoRow.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRow} xyz.swapee.wc.OfferExchangeInfoRow.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRow` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRow
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRow.constructor&xyz.swapee.wc.OfferExchangeInfoRow.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRow.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRow
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRow} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRow}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRow}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRow}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRow}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRow}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRow.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRow.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRow} xyz.swapee.wc.OfferExchangeInfoRowConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRow.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.OfferExchangeInfoRowMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.OfferExchangeInfoRowClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowCaster&xyz.swapee.wc.IOfferExchangeInfoRowProcessor&xyz.swapee.wc.IOfferExchangeInfoRowComputer&xyz.swapee.wc.IOfferExchangeInfoRowController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !xyz.swapee.wc.OfferExchangeInfoRowLand>)} xyz.swapee.wc.IOfferExchangeInfoRow.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRow
 */
xyz.swapee.wc.IOfferExchangeInfoRow = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRow.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowProcessor.typeof&xyz.swapee.wc.IOfferExchangeInfoRowComputer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRow.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRow&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRow.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRow.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRow} xyz.swapee.wc.IOfferExchangeInfoRow.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRow_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRow
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRow} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRow.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRow = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRow.constructor&xyz.swapee.wc.IOfferExchangeInfoRow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRow* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRow.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRow}
 */
xyz.swapee.wc.OfferExchangeInfoRow.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRow.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowFields = class { }
/**
 * The input pins of the _IOfferExchangeInfoRow_ port.
 */
xyz.swapee.wc.IOfferExchangeInfoRowFields.prototype.pinout = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRow} */
xyz.swapee.wc.RecordIOfferExchangeInfoRow

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRow} xyz.swapee.wc.BoundIOfferExchangeInfoRow */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRow} xyz.swapee.wc.BoundOfferExchangeInfoRow */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs} xyz.swapee.wc.IOfferExchangeInfoRow.Pinout The input pins of the _IOfferExchangeInfoRow_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>)} xyz.swapee.wc.IOfferExchangeInfoRowBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowBuffer
 */
xyz.swapee.wc.IOfferExchangeInfoRowBuffer = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowBuffer.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowBuffer

/**
 * A concrete class of _IOfferExchangeInfoRowBuffer_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowBuffer
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.OfferExchangeInfoRowBuffer = class extends xyz.swapee.wc.IOfferExchangeInfoRowBuffer { }
xyz.swapee.wc.OfferExchangeInfoRowBuffer.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowBuffer

/**
 * Contains getters to cast the _IOfferExchangeInfoRow_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRow_ instance into the _BoundIOfferExchangeInfoRow_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRow}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCaster.prototype.asIOfferExchangeInfoRow
/**
 * Access the _OfferExchangeInfoRow_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRow}
 */
xyz.swapee.wc.IOfferExchangeInfoRowCaster.prototype.superOfferExchangeInfoRow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/110-OfferExchangeInfoRowSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs
 */
xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowMemoryQPs
 * @dict
 */
xyz.swapee.wc.OfferExchangeInfoRowMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.OfferExchangeInfoRowMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs)} xyz.swapee.wc.OfferExchangeInfoRowInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs} xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferExchangeInfoRowInputsPQs
 */
xyz.swapee.wc.OfferExchangeInfoRowInputsPQs = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowInputsPQs.constructor&xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OfferExchangeInfoRowInputsPQs.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowInputsPQs

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs)} xyz.swapee.wc.OfferExchangeInfoRowInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowInputsQPs
 * @dict
 */
xyz.swapee.wc.OfferExchangeInfoRowInputsQPs = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowInputsQPs.constructor&xyz.swapee.wc.OfferExchangeInfoRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.OfferExchangeInfoRowInputsQPs.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferExchangeInfoRowQueriesPQs
 */
xyz.swapee.wc.OfferExchangeInfoRowQueriesPQs = class {
  constructor() {
    /**
     * `ecfe5`
     */
    this.exchangeBrokerSel=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OfferExchangeInfoRowQueriesPQs.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowQueriesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowQueriesQPs
 * @dict
 */
xyz.swapee.wc.OfferExchangeInfoRowQueriesQPs = class { }
/**
 * `exchangeBrokerSel`
 */
xyz.swapee.wc.OfferExchangeInfoRowQueriesQPs.prototype.ecfe5 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.OfferExchangeInfoRowVdusPQs
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusPQs = class {
  constructor() {
    /**
     * `hd091`
     */
    this.NetworkFeeWr=/** @type {string} */ (void 0)
    /**
     * `hd092`
     */
    this.NetworkFeeLa=/** @type {string} */ (void 0)
    /**
     * `hd093`
     */
    this.PartnerFeeWr=/** @type {string} */ (void 0)
    /**
     * `hd094`
     */
    this.PartnerFeeLa=/** @type {string} */ (void 0)
    /**
     * `hd095`
     */
    this.InIm=/** @type {string} */ (void 0)
    /**
     * `hd096`
     */
    this.OutIm=/** @type {string} */ (void 0)
    /**
     * `hd097`
     */
    this.CryptoIn=/** @type {string} */ (void 0)
    /**
     * `hd098`
     */
    this.CryptoOut=/** @type {string} */ (void 0)
    /**
     * `hd099`
     */
    this.AmountIn=/** @type {string} */ (void 0)
    /**
     * `hd0910`
     */
    this.AmountOut=/** @type {string} */ (void 0)
    /**
     * `hd0911`
     */
    this.ReloadGetOfferBu=/** @type {string} */ (void 0)
    /**
     * `hd0912`
     */
    this.AmountOutLoIn=/** @type {string} */ (void 0)
    /**
     * `hd0913`
     */
    this.GetOfferErrorWr=/** @type {string} */ (void 0)
    /**
     * `hd0914`
     */
    this.GetOfferErrorLa=/** @type {string} */ (void 0)
    /**
     * `hd0915`
     */
    this.EstimatedTildeOut=/** @type {string} */ (void 0)
    /**
     * `hd0916`
     */
    this.EstimatedFixedTildeOut=/** @type {string} */ (void 0)
    /**
     * `hd0917`
     */
    this.ExchangeBroker=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.OfferExchangeInfoRowVdusPQs.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowVdusQPs
 * @dict
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs = class { }
/**
 * `NetworkFeeWr`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd091 = /** @type {string} */ (void 0)
/**
 * `NetworkFeeLa`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd092 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeWr`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd093 = /** @type {string} */ (void 0)
/**
 * `PartnerFeeLa`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd094 = /** @type {string} */ (void 0)
/**
 * `InIm`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd095 = /** @type {string} */ (void 0)
/**
 * `OutIm`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd096 = /** @type {string} */ (void 0)
/**
 * `CryptoIn`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd097 = /** @type {string} */ (void 0)
/**
 * `CryptoOut`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd098 = /** @type {string} */ (void 0)
/**
 * `AmountIn`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd099 = /** @type {string} */ (void 0)
/**
 * `AmountOut`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0910 = /** @type {string} */ (void 0)
/**
 * `ReloadGetOfferBu`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0911 = /** @type {string} */ (void 0)
/**
 * `AmountOutLoIn`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0912 = /** @type {string} */ (void 0)
/**
 * `GetOfferErrorWr`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0913 = /** @type {string} */ (void 0)
/**
 * `GetOfferErrorLa`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0914 = /** @type {string} */ (void 0)
/**
 * `EstimatedTildeOut`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0915 = /** @type {string} */ (void 0)
/**
 * `EstimatedFixedTildeOut`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0916 = /** @type {string} */ (void 0)
/**
 * `ExchangeBroker`
 */
xyz.swapee.wc.OfferExchangeInfoRowVdusQPs.prototype.hd0917 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/12-IOfferExchangeInfoRowHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtilFields)} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.router} */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IOfferExchangeInfoRowHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentUtil
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentUtil = class extends xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil { }
xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentUtil

/**
 * Fields of the IOfferExchangeInfoRowHtmlComponentUtil.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtilFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil} xyz.swapee.wc.BoundIOfferExchangeInfoRowHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentUtil} xyz.swapee.wc.BoundOfferExchangeInfoRowHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IDealBrokerPort} DealBroker
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOfferExchangeInfoRowPort} OfferExchangeInfoRow The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.OfferExchangeInfoRowMemory} OfferExchangeInfoRow
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IDealBroker.Pinout} DealBroker
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout} OfferExchangeInfoRow
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.__router<!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil>} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `DealBroker` _typeof IDealBrokerPort_
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `OfferExchangeInfoRow` _typeof IOfferExchangeInfoRowPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `DealBroker` _!DealBrokerMemory_
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `OfferExchangeInfoRow` _!OfferExchangeInfoRowMemory_
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `DealBroker` _!IDealBroker.Pinout_
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `OfferExchangeInfoRow` _!IOfferExchangeInfoRow.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/12-IOfferExchangeInfoRowHtmlComponent.xml}  a5c0d6fd4f8fa735eaefea45a4c260a5 */
/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese&xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese&xyz.swapee.wc.IOfferExchangeInfoRow.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowProcessor.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowComputer.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent)} xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent} xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.constructor&xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent|typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent|typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent|typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.IOfferExchangeInfoRow|typeof xyz.swapee.wc.OfferExchangeInfoRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IOfferExchangeInfoRowProcessor|typeof xyz.swapee.wc.OfferExchangeInfoRowProcessor)|(!xyz.swapee.wc.IOfferExchangeInfoRowComputer|typeof xyz.swapee.wc.OfferExchangeInfoRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent} xyz.swapee.wc.OfferExchangeInfoRowHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentCaster&xyz.swapee.wc.back.IOfferExchangeInfoRowController&xyz.swapee.wc.back.IOfferExchangeInfoRowScreen&xyz.swapee.wc.IOfferExchangeInfoRow&com.webcircuits.ILanded<!xyz.swapee.wc.OfferExchangeInfoRowLand>&xyz.swapee.wc.IOfferExchangeInfoRowGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !HTMLDivElement, !xyz.swapee.wc.OfferExchangeInfoRowLand>&xyz.swapee.wc.IOfferExchangeInfoRowProcessor&xyz.swapee.wc.IOfferExchangeInfoRowComputer)} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowController} xyz.swapee.wc.back.IOfferExchangeInfoRowController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowScreen} xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRow} xyz.swapee.wc.IOfferExchangeInfoRow.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowGPU} xyz.swapee.wc.IOfferExchangeInfoRowGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowProcessor} xyz.swapee.wc.IOfferExchangeInfoRowProcessor.typeof */
/**
 * The _IOfferExchangeInfoRow_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOfferExchangeInfoRowController.typeof&xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.typeof&xyz.swapee.wc.IOfferExchangeInfoRow.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IOfferExchangeInfoRowGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IOfferExchangeInfoRowProcessor.typeof&xyz.swapee.wc.IOfferExchangeInfoRowComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent} xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent} The _IOfferExchangeInfoRow_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.constructor&xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowHtmlComponent

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent} xyz.swapee.wc.BoundIOfferExchangeInfoRowHtmlComponent */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent} xyz.swapee.wc.BoundOfferExchangeInfoRowHtmlComponent */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowHtmlComponent_ instance into the _BoundIOfferExchangeInfoRowHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentCaster.prototype.asIOfferExchangeInfoRowHtmlComponent
/**
 * Access the _OfferExchangeInfoRowHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowHtmlComponent}
 */
xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponentCaster.prototype.superOfferExchangeInfoRowHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/130-IOfferExchangeInfoRowElement.xml}  f0c9d994da432b478312ebada8622e50 */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.OfferExchangeInfoRowLand>&guest.maurice.IMilleu.Initialese<!(xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IExchangeIntent)>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowElement)} xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowElement} xyz.swapee.wc.OfferExchangeInfoRowElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowElement` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowElement
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.constructor&xyz.swapee.wc.OfferExchangeInfoRowElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElement|typeof xyz.swapee.wc.OfferExchangeInfoRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElement}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElement}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElement|typeof xyz.swapee.wc.OfferExchangeInfoRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElement}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElement|typeof xyz.swapee.wc.OfferExchangeInfoRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElement}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowElement} xyz.swapee.wc.OfferExchangeInfoRowElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElementFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs, !xyz.swapee.wc.OfferExchangeInfoRowLand>&com.webcircuits.ILanded<!xyz.swapee.wc.OfferExchangeInfoRowLand>&guest.maurice.IMilleu<!(xyz.swapee.wc.IDealBroker|xyz.swapee.wc.IExchangeIntent)>)} xyz.swapee.wc.IOfferExchangeInfoRowElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IOfferExchangeInfoRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElement
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.solder} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.render} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.build} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.buildDealBroker} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.buildDealBroker = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.buildExchangeIntent} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.short} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.server} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement.inducer} */
xyz.swapee.wc.IOfferExchangeInfoRowElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElement&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement} xyz.swapee.wc.IOfferExchangeInfoRowElement.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowElement_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowElement
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowElement} A component description.
 *
 * The _IOfferExchangeInfoRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowElement = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowElement.constructor&xyz.swapee.wc.IOfferExchangeInfoRowElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElement}
 */
xyz.swapee.wc.OfferExchangeInfoRowElement.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowElement.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElementFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementFields = class { }
/**
 * The element-specific inputs to the _IOfferExchangeInfoRow_ component.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowElement

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement} xyz.swapee.wc.BoundIOfferExchangeInfoRowElement */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowElement} xyz.swapee.wc.BoundOfferExchangeInfoRowElement */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs&xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries&xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs} xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs The element-specific inputs to the _IOfferExchangeInfoRow_ component. */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowElement_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElementCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowElement_ instance into the _BoundIOfferExchangeInfoRowElement_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowElement}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementCaster.prototype.asIOfferExchangeInfoRowElement
/**
 * Access the _OfferExchangeInfoRowElement_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowElement}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementCaster.prototype.superOfferExchangeInfoRowElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OfferExchangeInfoRowMemory, props: !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs) => Object<string, *>} xyz.swapee.wc.IOfferExchangeInfoRowElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__solder<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._solder */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} model The model.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs} props The element props.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* Default `null`.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[reloadGetOfferBuOpts]` _!Object?_ The options to pass to the _ReloadGetOfferBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[getOfferErrorWrOpts]` _!Object?_ The options to pass to the _GetOfferErrorWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts* Default `{}`.
 * - `[getOfferErrorLaOpts]` _!Object?_ The options to pass to the _GetOfferErrorLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts* Default `{}`.
 * - `[estimatedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts* Default `{}`.
 * - `[estimatedFixedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedFixedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OfferExchangeInfoRowMemory, instance?: !xyz.swapee.wc.IOfferExchangeInfoRowScreen&xyz.swapee.wc.IOfferExchangeInfoRowController) => !engineering.type.VNode} xyz.swapee.wc.IOfferExchangeInfoRowElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__render<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._render */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} [model] The model for the view.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowScreen&xyz.swapee.wc.IOfferExchangeInfoRowController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Cores, instances: !xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Instances) => ?} xyz.swapee.wc.IOfferExchangeInfoRowElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__build<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._build */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Cores} cores The models of components on the land.
 * - `DealBroker` _!xyz.swapee.wc.IDealBrokerCore.Model_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `DealBroker` _!xyz.swapee.wc.IDealBroker_
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IDealBroker} DealBroker
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IDealBrokerCore.Model, instance: !xyz.swapee.wc.IDealBroker) => ?} xyz.swapee.wc.IOfferExchangeInfoRowElement.__buildDealBroker
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__buildDealBroker<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._buildDealBroker */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.buildDealBroker} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IDealBroker_ component.
 * @param {!xyz.swapee.wc.IDealBrokerCore.Model} model
 * @param {!xyz.swapee.wc.IDealBroker} instance
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.buildDealBroker = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IOfferExchangeInfoRowElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__buildExchangeIntent<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.OfferExchangeInfoRowMemory, ports: !xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Ports, cores: !xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Cores) => ?} xyz.swapee.wc.IOfferExchangeInfoRowElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__short<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._short */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} model The model from which to feed properties to peer's ports.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Ports} ports The ports of the peers.
 * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerPortInterface_
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Cores} cores The cores of the peers.
 * - `DealBroker` _xyz.swapee.wc.IDealBrokerCore.Model_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IDealBrokerPortInterface} DealBroker
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IDealBrokerCore.Model} DealBroker
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OfferExchangeInfoRowMemory, inputs: !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IOfferExchangeInfoRowElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__server<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._server */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} memory The memory registers.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs} inputs The inputs to the port.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* Default `null`.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[reloadGetOfferBuOpts]` _!Object?_ The options to pass to the _ReloadGetOfferBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[getOfferErrorWrOpts]` _!Object?_ The options to pass to the _GetOfferErrorWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts* Default `{}`.
 * - `[getOfferErrorLaOpts]` _!Object?_ The options to pass to the _GetOfferErrorLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts* Default `{}`.
 * - `[estimatedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts* Default `{}`.
 * - `[estimatedFixedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedFixedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.OfferExchangeInfoRowMemory, port?: !xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs) => ?} xyz.swapee.wc.IOfferExchangeInfoRowElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElement.__inducer<!xyz.swapee.wc.IOfferExchangeInfoRowElement>} xyz.swapee.wc.IOfferExchangeInfoRowElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} [model] The model of the component into which to induce the state.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.IOfferExchangeInfoRowElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[core=null]` _&#42;?_ The core property. ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* ⤴ *IOfferExchangeInfoRowOuterCore.WeakModel.Core* Default `null`.
 * - `[dealBrokerSel=""]` _string?_ The query to discover the _DealBroker_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IOfferExchangeInfoRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[networkFeeWrOpts]` _!Object?_ The options to pass to the _NetworkFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts* Default `{}`.
 * - `[networkFeeLaOpts]` _!Object?_ The options to pass to the _NetworkFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts* Default `{}`.
 * - `[partnerFeeWrOpts]` _!Object?_ The options to pass to the _PartnerFeeWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts* Default `{}`.
 * - `[partnerFeeLaOpts]` _!Object?_ The options to pass to the _PartnerFeeLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts* Default `{}`.
 * - `[inImOpts]` _!Object?_ The options to pass to the _InIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.InImOpts* Default `{}`.
 * - `[outImOpts]` _!Object?_ The options to pass to the _OutIm_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.OutImOpts* Default `{}`.
 * - `[cryptoInOpts]` _!Object?_ The options to pass to the _CryptoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts* Default `{}`.
 * - `[cryptoOutOpts]` _!Object?_ The options to pass to the _CryptoOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts* Default `{}`.
 * - `[amountInOpts]` _!Object?_ The options to pass to the _AmountIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts* Default `{}`.
 * - `[amountOutOpts]` _!Object?_ The options to pass to the _AmountOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts* Default `{}`.
 * - `[visibleAmountWrOpts]` _!Object?_ The options to pass to the _VisibleAmountWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmountWrOpts* Default `{}`.
 * - `[visibleAmounLaOpts]` _!Object?_ The options to pass to the _VisibleAmounLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.VisibleAmounLaOpts* Default `{}`.
 * - `[breakdownBuOpts]` _!Object?_ The options to pass to the _BreakdownBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts* Default `{}`.
 * - `[reloadGetOfferBuOpts]` _!Object?_ The options to pass to the _ReloadGetOfferBu_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts* Default `{}`.
 * - `[amountOutLoInOpts]` _!Object?_ The options to pass to the _AmountOutLoIn_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts* Default `{}`.
 * - `[getOfferErrorWrOpts]` _!Object?_ The options to pass to the _GetOfferErrorWr_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts* Default `{}`.
 * - `[getOfferErrorLaOpts]` _!Object?_ The options to pass to the _GetOfferErrorLa_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts* Default `{}`.
 * - `[estimatedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts* Default `{}`.
 * - `[estimatedFixedTildeOutOpts]` _!Object?_ The options to pass to the _EstimatedFixedTildeOut_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts* Default `{}`.
 * - `[dealBrokerOpts]` _!Object?_ The options to pass to the _DealBroker_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/140-IOfferExchangeInfoRowElementPort.xml}  c1c4561a83153ffe6056131d53d7decf */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowElementPort)} xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort} xyz.swapee.wc.OfferExchangeInfoRowElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.constructor&xyz.swapee.wc.OfferExchangeInfoRowElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElementPort|typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElementPort|typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowElementPort|typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowElementPort} xyz.swapee.wc.OfferExchangeInfoRowElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs>)} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElementPort
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowElementPort_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowElementPort
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowElementPort = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowElementPort.constructor&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.OfferExchangeInfoRowElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowElementPort.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElementPortFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortFields = class { }
/**
 * The inputs to the _IOfferExchangeInfoRowElement_'s controller via its element port.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElementPort} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowElementPort

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowElementPort} xyz.swapee.wc.BoundIOfferExchangeInfoRowElementPort */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowElementPort} xyz.swapee.wc.BoundOfferExchangeInfoRowElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _NetworkFeeWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts.networkFeeWrOpts

/**
 * The options to pass to the _NetworkFeeLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts.networkFeeLaOpts

/**
 * The options to pass to the _PartnerFeeWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts.partnerFeeWrOpts

/**
 * The options to pass to the _PartnerFeeLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts.partnerFeeLaOpts

/**
 * The options to pass to the _InImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts.inImWrOpts

/**
 * The options to pass to the _OutImWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts.outImWrOpts

/**
 * The options to pass to the _CryptoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts.cryptoInOpts

/**
 * The options to pass to the _CryptoOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts.cryptoOutOpts

/**
 * The options to pass to the _AmountIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts.amountInOpts

/**
 * The options to pass to the _AmountOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts.amountOutOpts

/**
 * The options to pass to the _BreakdownBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts.breakdownBuOpts

/**
 * The options to pass to the _ReloadGetOfferBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts.reloadGetOfferBuOpts

/**
 * The options to pass to the _AmountOutLoIn_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts.amountOutLoInOpts

/**
 * The options to pass to the _GetOfferErrorWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts.getOfferErrorWrOpts

/**
 * The options to pass to the _RateWr_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts.rateWrOpts

/**
 * The options to pass to the _RateLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts.rateLaOpts

/**
 * The options to pass to the _GetOfferErrorLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts.getOfferErrorLaOpts

/**
 * The options to pass to the _EstimatedTildeOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts.estimatedTildeOutOpts

/**
 * The options to pass to the _EstimatedFixedTildeOut_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts.estimatedFixedTildeOutOpts

/**
 * The options to pass to the _DealBroker_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts.dealBrokerOpts

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts)} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IOfferExchangeInfoRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.constructor&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts)} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IOfferExchangeInfoRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.constructor&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts.typeof&xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs

/**
 * Contains getters to cast the _IOfferExchangeInfoRowElementPort_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowElementPortCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowElementPort_ instance into the _BoundIOfferExchangeInfoRowElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortCaster.prototype.asIOfferExchangeInfoRowElementPort
/**
 * Access the _OfferExchangeInfoRowElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowElementPort}
 */
xyz.swapee.wc.IOfferExchangeInfoRowElementPortCaster.prototype.superOfferExchangeInfoRowElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu (optional overlay).
 * @prop {!Object} [networkFeeWrOpts] The options to pass to the _NetworkFeeWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeWrOpts_Safe The options to pass to the _NetworkFeeWr_ vdu (required overlay).
 * @prop {!Object} networkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu (optional overlay).
 * @prop {!Object} [networkFeeLaOpts] The options to pass to the _NetworkFeeLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.NetworkFeeLaOpts_Safe The options to pass to the _NetworkFeeLa_ vdu (required overlay).
 * @prop {!Object} networkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu (optional overlay).
 * @prop {!Object} [partnerFeeWrOpts] The options to pass to the _PartnerFeeWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeWrOpts_Safe The options to pass to the _PartnerFeeWr_ vdu (required overlay).
 * @prop {!Object} partnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu (optional overlay).
 * @prop {!Object} [partnerFeeLaOpts] The options to pass to the _PartnerFeeLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.PartnerFeeLaOpts_Safe The options to pass to the _PartnerFeeLa_ vdu (required overlay).
 * @prop {!Object} partnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts The options to pass to the _InImWr_ vdu (optional overlay).
 * @prop {!Object} [inImWrOpts] The options to pass to the _InImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.InImWrOpts_Safe The options to pass to the _InImWr_ vdu (required overlay).
 * @prop {!Object} inImWrOpts The options to pass to the _InImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts The options to pass to the _OutImWr_ vdu (optional overlay).
 * @prop {!Object} [outImWrOpts] The options to pass to the _OutImWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.OutImWrOpts_Safe The options to pass to the _OutImWr_ vdu (required overlay).
 * @prop {!Object} outImWrOpts The options to pass to the _OutImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts The options to pass to the _CryptoIn_ vdu (optional overlay).
 * @prop {!Object} [cryptoInOpts] The options to pass to the _CryptoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoInOpts_Safe The options to pass to the _CryptoIn_ vdu (required overlay).
 * @prop {!Object} cryptoInOpts The options to pass to the _CryptoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts The options to pass to the _CryptoOut_ vdu (optional overlay).
 * @prop {!Object} [cryptoOutOpts] The options to pass to the _CryptoOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.CryptoOutOpts_Safe The options to pass to the _CryptoOut_ vdu (required overlay).
 * @prop {!Object} cryptoOutOpts The options to pass to the _CryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {!Object} [amountInOpts] The options to pass to the _AmountIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {!Object} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {!Object} [amountOutOpts] The options to pass to the _AmountOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {!Object} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts The options to pass to the _BreakdownBu_ vdu (optional overlay).
 * @prop {!Object} [breakdownBuOpts] The options to pass to the _BreakdownBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.BreakdownBuOpts_Safe The options to pass to the _BreakdownBu_ vdu (required overlay).
 * @prop {!Object} breakdownBuOpts The options to pass to the _BreakdownBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts The options to pass to the _ReloadGetOfferBu_ vdu (optional overlay).
 * @prop {!Object} [reloadGetOfferBuOpts] The options to pass to the _ReloadGetOfferBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ReloadGetOfferBuOpts_Safe The options to pass to the _ReloadGetOfferBu_ vdu (required overlay).
 * @prop {!Object} reloadGetOfferBuOpts The options to pass to the _ReloadGetOfferBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu (optional overlay).
 * @prop {!Object} [amountOutLoInOpts] The options to pass to the _AmountOutLoIn_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.AmountOutLoInOpts_Safe The options to pass to the _AmountOutLoIn_ vdu (required overlay).
 * @prop {!Object} amountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts The options to pass to the _GetOfferErrorWr_ vdu (optional overlay).
 * @prop {!Object} [getOfferErrorWrOpts] The options to pass to the _GetOfferErrorWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorWrOpts_Safe The options to pass to the _GetOfferErrorWr_ vdu (required overlay).
 * @prop {!Object} getOfferErrorWrOpts The options to pass to the _GetOfferErrorWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts The options to pass to the _RateWr_ vdu (optional overlay).
 * @prop {!Object} [rateWrOpts] The options to pass to the _RateWr_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateWrOpts_Safe The options to pass to the _RateWr_ vdu (required overlay).
 * @prop {!Object} rateWrOpts The options to pass to the _RateWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts The options to pass to the _RateLa_ vdu (optional overlay).
 * @prop {!Object} [rateLaOpts] The options to pass to the _RateLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.RateLaOpts_Safe The options to pass to the _RateLa_ vdu (required overlay).
 * @prop {!Object} rateLaOpts The options to pass to the _RateLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts The options to pass to the _GetOfferErrorLa_ vdu (optional overlay).
 * @prop {!Object} [getOfferErrorLaOpts] The options to pass to the _GetOfferErrorLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.GetOfferErrorLaOpts_Safe The options to pass to the _GetOfferErrorLa_ vdu (required overlay).
 * @prop {!Object} getOfferErrorLaOpts The options to pass to the _GetOfferErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts The options to pass to the _EstimatedTildeOut_ vdu (optional overlay).
 * @prop {!Object} [estimatedTildeOutOpts] The options to pass to the _EstimatedTildeOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedTildeOutOpts_Safe The options to pass to the _EstimatedTildeOut_ vdu (required overlay).
 * @prop {!Object} estimatedTildeOutOpts The options to pass to the _EstimatedTildeOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts The options to pass to the _EstimatedFixedTildeOut_ vdu (optional overlay).
 * @prop {!Object} [estimatedFixedTildeOutOpts] The options to pass to the _EstimatedFixedTildeOut_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.EstimatedFixedTildeOutOpts_Safe The options to pass to the _EstimatedFixedTildeOut_ vdu (required overlay).
 * @prop {!Object} estimatedFixedTildeOutOpts The options to pass to the _EstimatedFixedTildeOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {!Object} [dealBrokerOpts] The options to pass to the _DealBroker_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {!Object} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu (optional overlay).
 * @prop {*} [networkFeeWrOpts=null] The options to pass to the _NetworkFeeWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeWrOpts_Safe The options to pass to the _NetworkFeeWr_ vdu (required overlay).
 * @prop {*} networkFeeWrOpts The options to pass to the _NetworkFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu (optional overlay).
 * @prop {*} [networkFeeLaOpts=null] The options to pass to the _NetworkFeeLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.NetworkFeeLaOpts_Safe The options to pass to the _NetworkFeeLa_ vdu (required overlay).
 * @prop {*} networkFeeLaOpts The options to pass to the _NetworkFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu (optional overlay).
 * @prop {*} [partnerFeeWrOpts=null] The options to pass to the _PartnerFeeWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeWrOpts_Safe The options to pass to the _PartnerFeeWr_ vdu (required overlay).
 * @prop {*} partnerFeeWrOpts The options to pass to the _PartnerFeeWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu (optional overlay).
 * @prop {*} [partnerFeeLaOpts=null] The options to pass to the _PartnerFeeLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.PartnerFeeLaOpts_Safe The options to pass to the _PartnerFeeLa_ vdu (required overlay).
 * @prop {*} partnerFeeLaOpts The options to pass to the _PartnerFeeLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts The options to pass to the _InImWr_ vdu (optional overlay).
 * @prop {*} [inImWrOpts=null] The options to pass to the _InImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.InImWrOpts_Safe The options to pass to the _InImWr_ vdu (required overlay).
 * @prop {*} inImWrOpts The options to pass to the _InImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts The options to pass to the _OutImWr_ vdu (optional overlay).
 * @prop {*} [outImWrOpts=null] The options to pass to the _OutImWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.OutImWrOpts_Safe The options to pass to the _OutImWr_ vdu (required overlay).
 * @prop {*} outImWrOpts The options to pass to the _OutImWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts The options to pass to the _CryptoIn_ vdu (optional overlay).
 * @prop {*} [cryptoInOpts=null] The options to pass to the _CryptoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoInOpts_Safe The options to pass to the _CryptoIn_ vdu (required overlay).
 * @prop {*} cryptoInOpts The options to pass to the _CryptoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts The options to pass to the _CryptoOut_ vdu (optional overlay).
 * @prop {*} [cryptoOutOpts=null] The options to pass to the _CryptoOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.CryptoOutOpts_Safe The options to pass to the _CryptoOut_ vdu (required overlay).
 * @prop {*} cryptoOutOpts The options to pass to the _CryptoOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts The options to pass to the _AmountIn_ vdu (optional overlay).
 * @prop {*} [amountInOpts=null] The options to pass to the _AmountIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountInOpts_Safe The options to pass to the _AmountIn_ vdu (required overlay).
 * @prop {*} amountInOpts The options to pass to the _AmountIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts The options to pass to the _AmountOut_ vdu (optional overlay).
 * @prop {*} [amountOutOpts=null] The options to pass to the _AmountOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutOpts_Safe The options to pass to the _AmountOut_ vdu (required overlay).
 * @prop {*} amountOutOpts The options to pass to the _AmountOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts The options to pass to the _BreakdownBu_ vdu (optional overlay).
 * @prop {*} [breakdownBuOpts=null] The options to pass to the _BreakdownBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.BreakdownBuOpts_Safe The options to pass to the _BreakdownBu_ vdu (required overlay).
 * @prop {*} breakdownBuOpts The options to pass to the _BreakdownBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts The options to pass to the _ReloadGetOfferBu_ vdu (optional overlay).
 * @prop {*} [reloadGetOfferBuOpts=null] The options to pass to the _ReloadGetOfferBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ReloadGetOfferBuOpts_Safe The options to pass to the _ReloadGetOfferBu_ vdu (required overlay).
 * @prop {*} reloadGetOfferBuOpts The options to pass to the _ReloadGetOfferBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu (optional overlay).
 * @prop {*} [amountOutLoInOpts=null] The options to pass to the _AmountOutLoIn_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.AmountOutLoInOpts_Safe The options to pass to the _AmountOutLoIn_ vdu (required overlay).
 * @prop {*} amountOutLoInOpts The options to pass to the _AmountOutLoIn_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts The options to pass to the _GetOfferErrorWr_ vdu (optional overlay).
 * @prop {*} [getOfferErrorWrOpts=null] The options to pass to the _GetOfferErrorWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorWrOpts_Safe The options to pass to the _GetOfferErrorWr_ vdu (required overlay).
 * @prop {*} getOfferErrorWrOpts The options to pass to the _GetOfferErrorWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts The options to pass to the _RateWr_ vdu (optional overlay).
 * @prop {*} [rateWrOpts=null] The options to pass to the _RateWr_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateWrOpts_Safe The options to pass to the _RateWr_ vdu (required overlay).
 * @prop {*} rateWrOpts The options to pass to the _RateWr_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts The options to pass to the _RateLa_ vdu (optional overlay).
 * @prop {*} [rateLaOpts=null] The options to pass to the _RateLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.RateLaOpts_Safe The options to pass to the _RateLa_ vdu (required overlay).
 * @prop {*} rateLaOpts The options to pass to the _RateLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts The options to pass to the _GetOfferErrorLa_ vdu (optional overlay).
 * @prop {*} [getOfferErrorLaOpts=null] The options to pass to the _GetOfferErrorLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.GetOfferErrorLaOpts_Safe The options to pass to the _GetOfferErrorLa_ vdu (required overlay).
 * @prop {*} getOfferErrorLaOpts The options to pass to the _GetOfferErrorLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts The options to pass to the _EstimatedTildeOut_ vdu (optional overlay).
 * @prop {*} [estimatedTildeOutOpts=null] The options to pass to the _EstimatedTildeOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedTildeOutOpts_Safe The options to pass to the _EstimatedTildeOut_ vdu (required overlay).
 * @prop {*} estimatedTildeOutOpts The options to pass to the _EstimatedTildeOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts The options to pass to the _EstimatedFixedTildeOut_ vdu (optional overlay).
 * @prop {*} [estimatedFixedTildeOutOpts=null] The options to pass to the _EstimatedFixedTildeOut_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.EstimatedFixedTildeOutOpts_Safe The options to pass to the _EstimatedFixedTildeOut_ vdu (required overlay).
 * @prop {*} estimatedFixedTildeOutOpts The options to pass to the _EstimatedFixedTildeOut_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts The options to pass to the _DealBroker_ vdu (optional overlay).
 * @prop {*} [dealBrokerOpts=null] The options to pass to the _DealBroker_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.DealBrokerOpts_Safe The options to pass to the _DealBroker_ vdu (required overlay).
 * @prop {*} dealBrokerOpts The options to pass to the _DealBroker_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/170-IOfferExchangeInfoRowDesigner.xml}  b1af5cc8ec9812d86d6f54e02d0a4656 */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowDesigner
 */
xyz.swapee.wc.IOfferExchangeInfoRowDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.OfferExchangeInfoRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOfferExchangeInfoRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OfferExchangeInfoRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IOfferExchangeInfoRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.OfferExchangeInfoRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOfferExchangeInfoRowDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `OfferExchangeInfoRow` _typeof IOfferExchangeInfoRowController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IOfferExchangeInfoRowDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `DealBroker` _typeof xyz.swapee.wc.IDealBrokerController_
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `OfferExchangeInfoRow` _typeof IOfferExchangeInfoRowController_
   * - `This` _typeof IOfferExchangeInfoRowController_
   * @param {!xyz.swapee.wc.IOfferExchangeInfoRowDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `DealBroker` _!xyz.swapee.wc.DealBrokerMemory_
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `OfferExchangeInfoRow` _!OfferExchangeInfoRowMemory_
   * - `This` _!OfferExchangeInfoRowMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OfferExchangeInfoRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.OfferExchangeInfoRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IOfferExchangeInfoRowDesigner.prototype.constructor = xyz.swapee.wc.IOfferExchangeInfoRowDesigner

/**
 * A concrete class of _IOfferExchangeInfoRowDesigner_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowDesigner
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.OfferExchangeInfoRowDesigner = class extends xyz.swapee.wc.IOfferExchangeInfoRowDesigner { }
xyz.swapee.wc.OfferExchangeInfoRowDesigner.prototype.constructor = xyz.swapee.wc.OfferExchangeInfoRowDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOfferExchangeInfoRowController} OfferExchangeInfoRow
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IDealBrokerController} DealBroker
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IOfferExchangeInfoRowController} OfferExchangeInfoRow
 * @prop {typeof xyz.swapee.wc.IOfferExchangeInfoRowController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.DealBrokerMemory} DealBroker
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.OfferExchangeInfoRowMemory} OfferExchangeInfoRow
 * @prop {!xyz.swapee.wc.OfferExchangeInfoRowMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/200-OfferExchangeInfoRowLand.xml}  a823a76050f8ef3ee813c3396af0424c */
/**
 * The surrounding of the _IOfferExchangeInfoRow_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.OfferExchangeInfoRowLand
 */
xyz.swapee.wc.OfferExchangeInfoRowLand = class { }
/**
 *
 */
xyz.swapee.wc.OfferExchangeInfoRowLand.prototype.DealBroker = /** @type {xyz.swapee.wc.IDealBroker} */ (void 0)
/**
 *
 */
xyz.swapee.wc.OfferExchangeInfoRowLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/40-IOfferExchangeInfoRowDisplay.xml}  7ef7e708251c137866581ae7b4c67aec */
/**
 * @typedef {Object} $xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese
 * @prop {HTMLSpanElement} [NetworkFeeWr]
 * @prop {HTMLSpanElement} [NetworkFeeLa]
 * @prop {HTMLSpanElement} [PartnerFeeWr]
 * @prop {HTMLSpanElement} [PartnerFeeLa]
 * @prop {HTMLElement} [InImWr]
 * @prop {HTMLElement} [OutImWr]
 * @prop {!Array<!HTMLSpanElement>} [CryptoIns]
 * @prop {!Array<!HTMLSpanElement>} [CryptoOuts]
 * @prop {HTMLSpanElement} [AmountIn]
 * @prop {HTMLSpanElement} [AmountOut]
 * @prop {HTMLButtonElement} [BreakdownBu]
 * @prop {HTMLButtonElement} [ReloadGetOfferBu]
 * @prop {HTMLSpanElement} [AmountOutLoIn]
 * @prop {HTMLSpanElement} [GetOfferErrorWr]
 * @prop {HTMLDivElement} [RateWr]
 * @prop {HTMLSpanElement} [RateLa]
 * @prop {HTMLSpanElement} [GetOfferErrorLa]
 * @prop {HTMLSpanElement} [EstimatedTildeOut]
 * @prop {HTMLSpanElement} [EstimatedFixedTildeOut]
 * @prop {HTMLElement} [DealBroker] The via for the _DealBroker_ peer.
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings>} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowDisplay)} xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay} xyz.swapee.wc.OfferExchangeInfoRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.constructor&xyz.swapee.wc.OfferExchangeInfoRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowDisplay} xyz.swapee.wc.OfferExchangeInfoRowDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !HTMLDivElement, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings, xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries, null>)} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IOfferExchangeInfoRow_.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowDisplay
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowDisplay.paint} */
xyz.swapee.wc.IOfferExchangeInfoRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowDisplay} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowDisplay_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowDisplay
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowDisplay} Display for presenting information from the _IOfferExchangeInfoRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowDisplay.constructor&xyz.swapee.wc.IOfferExchangeInfoRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.OfferExchangeInfoRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowDisplay.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.NetworkFeeWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.NetworkFeeLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.PartnerFeeWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.PartnerFeeLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.InImWr = /** @type {HTMLElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.OutImWr = /** @type {HTMLElement} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.CryptoIns = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.CryptoOuts = /** @type {!Array<!HTMLSpanElement>} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.AmountIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.AmountOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.BreakdownBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.ReloadGetOfferBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.AmountOutLoIn = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.GetOfferErrorWr = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.RateWr = /** @type {HTMLDivElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.RateLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.GetOfferErrorLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.EstimatedTildeOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.EstimatedFixedTildeOut = /** @type {HTMLSpanElement} */ (void 0)
/**
 * The via for the _DealBroker_ peer. Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.DealBroker = /** @type {HTMLElement} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowDisplay} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowDisplay

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowDisplay} xyz.swapee.wc.BoundIOfferExchangeInfoRowDisplay */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowDisplay} xyz.swapee.wc.BoundOfferExchangeInfoRowDisplay */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [dealBrokerSel=""] The query to discover the _DealBroker_ VDU. Default empty string.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowDisplay_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowDisplay_ instance into the _BoundIOfferExchangeInfoRowDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster.prototype.asIOfferExchangeInfoRowDisplay
/**
 * Cast the _IOfferExchangeInfoRowDisplay_ instance into the _BoundIOfferExchangeInfoRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster.prototype.asIOfferExchangeInfoRowScreen
/**
 * Access the _OfferExchangeInfoRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplayCaster.prototype.superOfferExchangeInfoRowDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.OfferExchangeInfoRowMemory, land: null) => void} xyz.swapee.wc.IOfferExchangeInfoRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowDisplay.__paint<!xyz.swapee.wc.IOfferExchangeInfoRowDisplay>} xyz.swapee.wc.IOfferExchangeInfoRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} memory The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/40-IOfferExchangeInfoRowDisplayBack.xml}  2bb25a82371585a8e4355363a2c7ec8f */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [NetworkFeeWr]
 * @prop {!com.webcircuits.IHtmlTwin} [NetworkFeeLa]
 * @prop {!com.webcircuits.IHtmlTwin} [PartnerFeeWr]
 * @prop {!com.webcircuits.IHtmlTwin} [PartnerFeeLa]
 * @prop {!com.webcircuits.IHtmlTwin} [InImWr]
 * @prop {!com.webcircuits.IHtmlTwin} [OutImWr]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [CryptoIns]
 * @prop {!Array<!com.webcircuits.IHtmlTwin>} [CryptoOuts]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountIn]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountOut]
 * @prop {!com.webcircuits.IHtmlTwin} [BreakdownBu]
 * @prop {!com.webcircuits.IHtmlTwin} [ReloadGetOfferBu]
 * @prop {!com.webcircuits.IHtmlTwin} [AmountOutLoIn]
 * @prop {!com.webcircuits.IHtmlTwin} [GetOfferErrorWr]
 * @prop {!com.webcircuits.IHtmlTwin} [RateWr]
 * @prop {!com.webcircuits.IHtmlTwin} [RateLa]
 * @prop {!com.webcircuits.IHtmlTwin} [GetOfferErrorLa]
 * @prop {!com.webcircuits.IHtmlTwin} [EstimatedTildeOut]
 * @prop {!com.webcircuits.IHtmlTwin} [EstimatedFixedTildeOut]
 * @prop {!com.webcircuits.IHtmlTwin} [DealBroker] The via for the _DealBroker_ peer.
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.OfferExchangeInfoRowClasses>} xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)} xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay} xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.constructor&xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.OfferExchangeInfoRowClasses, !xyz.swapee.wc.OfferExchangeInfoRowLand>)} xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.paint} */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese>)} xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay} xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowDisplay_ instances.
 * @constructor xyz.swapee.wc.back.OfferExchangeInfoRowDisplay
 * @implements {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferExchangeInfoRowDisplay = class extends /** @type {xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.constructor&xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.prototype.constructor = xyz.swapee.wc.back.OfferExchangeInfoRowDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.OfferExchangeInfoRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowDisplay.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.NetworkFeeWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.NetworkFeeLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.PartnerFeeWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.PartnerFeeLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.InImWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.OutImWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.CryptoIns = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 * Default `[]`.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.CryptoOuts = /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.AmountIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.AmountOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.BreakdownBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.ReloadGetOfferBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.AmountOutLoIn = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.GetOfferErrorWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.RateWr = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.RateLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.GetOfferErrorLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.EstimatedTildeOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.EstimatedFixedTildeOut = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _DealBroker_ peer.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.DealBroker = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay} */
xyz.swapee.wc.back.RecordIOfferExchangeInfoRowDisplay

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay} xyz.swapee.wc.back.BoundIOfferExchangeInfoRowDisplay */

/** @typedef {xyz.swapee.wc.back.OfferExchangeInfoRowDisplay} xyz.swapee.wc.back.BoundOfferExchangeInfoRowDisplay */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowDisplay_ interface.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayCaster
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowDisplay_ instance into the _BoundIOfferExchangeInfoRowDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayCaster.prototype.asIOfferExchangeInfoRowDisplay
/**
 * Access the _OfferExchangeInfoRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferExchangeInfoRowDisplay}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplayCaster.prototype.superOfferExchangeInfoRowDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.OfferExchangeInfoRowMemory, land?: !xyz.swapee.wc.OfferExchangeInfoRowLand) => void} xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.__paint<!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay>} xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowMemory} [memory] The display data.
 * - `core` _string_ The core property. Default empty string.
 * @param {!xyz.swapee.wc.OfferExchangeInfoRowLand} [land] The land data.
 * - `DealBroker` _xyz.swapee.wc.IDealBroker_
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * @return {void}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/41-OfferExchangeInfoRowClasses.xml}  785501f209d24106bae86ca808e52578 */
/**
 * The classes of the _IOfferExchangeInfoRowDisplay_.
 * @record xyz.swapee.wc.OfferExchangeInfoRowClasses
 */
xyz.swapee.wc.OfferExchangeInfoRowClasses = class { }
/**
 *
 */
xyz.swapee.wc.OfferExchangeInfoRowClasses.prototype.CryptoAmount = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.OfferExchangeInfoRowClasses.prototype.props = /** @type {xyz.swapee.wc.OfferExchangeInfoRowClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/50-IOfferExchangeInfoRowController.xml}  80f860560a4d269f716aec7b0c0a4460 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel>} xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowController)} xyz.swapee.wc.AbstractOfferExchangeInfoRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowController} xyz.swapee.wc.OfferExchangeInfoRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowController` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowController
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowController.constructor&xyz.swapee.wc.OfferExchangeInfoRowController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowController}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowController} xyz.swapee.wc.OfferExchangeInfoRowControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !xyz.swapee.wc.IOfferExchangeInfoRowController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs, !xyz.swapee.wc.OfferExchangeInfoRowMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>)} xyz.swapee.wc.IOfferExchangeInfoRowController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowController
 */
xyz.swapee.wc.IOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IOfferExchangeInfoRowController.resetPort} */
xyz.swapee.wc.IOfferExchangeInfoRowController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowController&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowController.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowController_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowController
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowController.constructor&xyz.swapee.wc.IOfferExchangeInfoRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.OfferExchangeInfoRowController.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowController.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowControllerFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerFields = class { }
/**
 * The inputs to the _IOfferExchangeInfoRow_'s controller.
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerFields.prototype.props = /** @type {xyz.swapee.wc.IOfferExchangeInfoRowController} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowController} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowController

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowController} xyz.swapee.wc.BoundIOfferExchangeInfoRowController */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowController} xyz.swapee.wc.BoundOfferExchangeInfoRowController */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort.Inputs} xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs The inputs to the _IOfferExchangeInfoRow_'s controller. */

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowPort.WeakInputs} xyz.swapee.wc.IOfferExchangeInfoRowController.WeakInputs The inputs to the _IOfferExchangeInfoRow_'s controller. */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowController_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowController_ instance into the _BoundIOfferExchangeInfoRowController_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowController}
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster.prototype.asIOfferExchangeInfoRowController
/**
 * Cast the _IOfferExchangeInfoRowController_ instance into the _BoundIOfferExchangeInfoRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowProcessor}
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster.prototype.asIOfferExchangeInfoRowProcessor
/**
 * Access the _OfferExchangeInfoRowController_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowController}
 */
xyz.swapee.wc.IOfferExchangeInfoRowControllerCaster.prototype.superOfferExchangeInfoRowController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IOfferExchangeInfoRowController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowController.__resetPort<!xyz.swapee.wc.IOfferExchangeInfoRowController>} xyz.swapee.wc.IOfferExchangeInfoRowController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IOfferExchangeInfoRowController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IOfferExchangeInfoRowController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/51-IOfferExchangeInfoRowControllerFront.xml}  468c9e57e747712a0711ed324d663fa1 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferExchangeInfoRowController)} xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController} xyz.swapee.wc.front.OfferExchangeInfoRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferExchangeInfoRowController` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.constructor&xyz.swapee.wc.front.OfferExchangeInfoRowController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.prototype.constructor = xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese[]) => xyz.swapee.wc.front.IOfferExchangeInfoRowController} xyz.swapee.wc.front.OfferExchangeInfoRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferExchangeInfoRowControllerCaster&xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT)} xyz.swapee.wc.front.IOfferExchangeInfoRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT} xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowController
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.front.IOfferExchangeInfoRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferExchangeInfoRowController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese>)} xyz.swapee.wc.front.OfferExchangeInfoRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferExchangeInfoRowController} xyz.swapee.wc.front.IOfferExchangeInfoRowController.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowController_ instances.
 * @constructor xyz.swapee.wc.front.OfferExchangeInfoRowController
 * @implements {xyz.swapee.wc.front.IOfferExchangeInfoRowController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.front.OfferExchangeInfoRowController.constructor&xyz.swapee.wc.front.IOfferExchangeInfoRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferExchangeInfoRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.OfferExchangeInfoRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowController} */
xyz.swapee.wc.front.RecordIOfferExchangeInfoRowController

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowController} xyz.swapee.wc.front.BoundIOfferExchangeInfoRowController */

/** @typedef {xyz.swapee.wc.front.OfferExchangeInfoRowController} xyz.swapee.wc.front.BoundOfferExchangeInfoRowController */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowController_ interface.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowControllerCaster
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowController_ instance into the _BoundIOfferExchangeInfoRowController_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerCaster.prototype.asIOfferExchangeInfoRowController
/**
 * Access the _OfferExchangeInfoRowController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferExchangeInfoRowController}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerCaster.prototype.superOfferExchangeInfoRowController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/52-IOfferExchangeInfoRowControllerBack.xml}  49dad94775c25f4f238daf007c7f9b1a */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>&xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.OfferExchangeInfoRowController)} xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController} xyz.swapee.wc.back.OfferExchangeInfoRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferExchangeInfoRowController` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.constructor&xyz.swapee.wc.back.OfferExchangeInfoRowController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.prototype.constructor = xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.back.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese[]) => xyz.swapee.wc.back.IOfferExchangeInfoRowController} xyz.swapee.wc.back.OfferExchangeInfoRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferExchangeInfoRowControllerCaster&xyz.swapee.wc.IOfferExchangeInfoRowController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IOfferExchangeInfoRowController.Inputs>)} xyz.swapee.wc.back.IOfferExchangeInfoRowController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowController
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IOfferExchangeInfoRowController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese>)} xyz.swapee.wc.back.OfferExchangeInfoRowController.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowController_ instances.
 * @constructor xyz.swapee.wc.back.OfferExchangeInfoRowController
 * @implements {xyz.swapee.wc.back.IOfferExchangeInfoRowController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferExchangeInfoRowController = class extends /** @type {xyz.swapee.wc.back.OfferExchangeInfoRowController.constructor&xyz.swapee.wc.back.IOfferExchangeInfoRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferExchangeInfoRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.OfferExchangeInfoRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowController} */
xyz.swapee.wc.back.RecordIOfferExchangeInfoRowController

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowController} xyz.swapee.wc.back.BoundIOfferExchangeInfoRowController */

/** @typedef {xyz.swapee.wc.back.OfferExchangeInfoRowController} xyz.swapee.wc.back.BoundOfferExchangeInfoRowController */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowController_ interface.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowControllerCaster
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowController_ instance into the _BoundIOfferExchangeInfoRowController_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerCaster.prototype.asIOfferExchangeInfoRowController
/**
 * Access the _OfferExchangeInfoRowController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferExchangeInfoRowController}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerCaster.prototype.superOfferExchangeInfoRowController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/53-IOfferExchangeInfoRowControllerAR.xml}  9fc4427c2c017d4a2b10ff375e5bc3a3 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowController.Initialese} xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR)} xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR} xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.constructor&xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR|typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR|typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR|typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.OfferExchangeInfoRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese[]) => xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR} xyz.swapee.wc.back.OfferExchangeInfoRowControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferExchangeInfoRowControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOfferExchangeInfoRowController)} xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOfferExchangeInfoRowControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR = class extends /** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOfferExchangeInfoRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese>)} xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR} xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR
 * @implements {xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IOfferExchangeInfoRowControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR = class extends /** @type {xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.constructor&xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR} */
xyz.swapee.wc.back.RecordIOfferExchangeInfoRowControllerAR

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR} xyz.swapee.wc.back.BoundIOfferExchangeInfoRowControllerAR */

/** @typedef {xyz.swapee.wc.back.OfferExchangeInfoRowControllerAR} xyz.swapee.wc.back.BoundOfferExchangeInfoRowControllerAR */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowControllerARCaster
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerARCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowControllerAR_ instance into the _BoundIOfferExchangeInfoRowControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerARCaster.prototype.asIOfferExchangeInfoRowControllerAR
/**
 * Access the _OfferExchangeInfoRowControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferExchangeInfoRowControllerAR}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowControllerARCaster.prototype.superOfferExchangeInfoRowControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/54-IOfferExchangeInfoRowControllerAT.xml}  8a5a4c3899a8c76411e408c3fb0b070f */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)} xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT} xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.constructor&xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT|typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese[]) => xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT} xyz.swapee.wc.front.OfferExchangeInfoRowControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferExchangeInfoRowControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferExchangeInfoRowControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT = class extends /** @type {xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese>)} xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT
 * @implements {xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferExchangeInfoRowControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT = class extends /** @type {xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.constructor&xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT} */
xyz.swapee.wc.front.RecordIOfferExchangeInfoRowControllerAT

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT} xyz.swapee.wc.front.BoundIOfferExchangeInfoRowControllerAT */

/** @typedef {xyz.swapee.wc.front.OfferExchangeInfoRowControllerAT} xyz.swapee.wc.front.BoundOfferExchangeInfoRowControllerAT */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowControllerATCaster
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerATCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowControllerAT_ instance into the _BoundIOfferExchangeInfoRowControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerATCaster.prototype.asIOfferExchangeInfoRowControllerAT
/**
 * Access the _OfferExchangeInfoRowControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferExchangeInfoRowControllerAT}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowControllerATCaster.prototype.superOfferExchangeInfoRowControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/70-IOfferExchangeInfoRowScreen.xml}  95ac63e0c0436be14da2aea2510a097e */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.front.OfferExchangeInfoRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries, null>&xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowScreen)} xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen} xyz.swapee.wc.OfferExchangeInfoRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowScreen` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.constructor&xyz.swapee.wc.OfferExchangeInfoRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IOfferExchangeInfoRowController|typeof xyz.swapee.wc.front.OfferExchangeInfoRowController)|(!xyz.swapee.wc.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.OfferExchangeInfoRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowScreen} xyz.swapee.wc.OfferExchangeInfoRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.OfferExchangeInfoRowMemory, !xyz.swapee.wc.front.OfferExchangeInfoRowInputs, !HTMLDivElement, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Settings, !xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Queries, null, null>&xyz.swapee.wc.front.IOfferExchangeInfoRowController&xyz.swapee.wc.IOfferExchangeInfoRowDisplay)} xyz.swapee.wc.IOfferExchangeInfoRowScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowScreen
 */
xyz.swapee.wc.IOfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IOfferExchangeInfoRowController.typeof&xyz.swapee.wc.IOfferExchangeInfoRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IOfferExchangeInfoRowScreen} xyz.swapee.wc.IOfferExchangeInfoRowScreen.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowScreen_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowScreen
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowScreen.constructor&xyz.swapee.wc.IOfferExchangeInfoRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.OfferExchangeInfoRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowScreen} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowScreen

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowScreen} xyz.swapee.wc.BoundIOfferExchangeInfoRowScreen */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowScreen} xyz.swapee.wc.BoundOfferExchangeInfoRowScreen */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowScreen_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowScreenCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowScreenCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowScreen_ instance into the _BoundIOfferExchangeInfoRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.IOfferExchangeInfoRowScreenCaster.prototype.asIOfferExchangeInfoRowScreen
/**
 * Access the _OfferExchangeInfoRowScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.IOfferExchangeInfoRowScreenCaster.prototype.superOfferExchangeInfoRowScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/70-IOfferExchangeInfoRowScreenBack.xml}  00000629291f4305f1bc30f10c5e7015 */
/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese} xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferExchangeInfoRowScreen)} xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen} xyz.swapee.wc.back.OfferExchangeInfoRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferExchangeInfoRowScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.constructor&xyz.swapee.wc.back.OfferExchangeInfoRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.prototype.constructor = xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese[]) => xyz.swapee.wc.back.IOfferExchangeInfoRowScreen} xyz.swapee.wc.back.OfferExchangeInfoRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferExchangeInfoRowScreenCaster&xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT)} xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT} xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowScreen
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese>)} xyz.swapee.wc.back.OfferExchangeInfoRowScreen.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowScreen_ instances.
 * @constructor xyz.swapee.wc.back.OfferExchangeInfoRowScreen
 * @implements {xyz.swapee.wc.back.IOfferExchangeInfoRowScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreen = class extends /** @type {xyz.swapee.wc.back.OfferExchangeInfoRowScreen.constructor&xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowScreen} */
xyz.swapee.wc.back.RecordIOfferExchangeInfoRowScreen

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowScreen} xyz.swapee.wc.back.BoundIOfferExchangeInfoRowScreen */

/** @typedef {xyz.swapee.wc.back.OfferExchangeInfoRowScreen} xyz.swapee.wc.back.BoundOfferExchangeInfoRowScreen */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowScreen_ interface.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowScreenCaster
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowScreen_ instance into the _BoundIOfferExchangeInfoRowScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenCaster.prototype.asIOfferExchangeInfoRowScreen
/**
 * Access the _OfferExchangeInfoRowScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferExchangeInfoRowScreen}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenCaster.prototype.superOfferExchangeInfoRowScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/73-IOfferExchangeInfoRowScreenAR.xml}  3ab95a09d402788817a0b5f8cbb70e54 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IOfferExchangeInfoRowScreen.Initialese} xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR)} xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR} xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.constructor&xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR|typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR|typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR|typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IOfferExchangeInfoRowScreen|typeof xyz.swapee.wc.OfferExchangeInfoRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese[]) => xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR} xyz.swapee.wc.front.OfferExchangeInfoRowScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IOfferExchangeInfoRowScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IOfferExchangeInfoRowScreen)} xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOfferExchangeInfoRowScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR = class extends /** @type {xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IOfferExchangeInfoRowScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese>)} xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR} xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.typeof */
/**
 * A concrete class of _IOfferExchangeInfoRowScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR
 * @implements {xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IOfferExchangeInfoRowScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR = class extends /** @type {xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.constructor&xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR} */
xyz.swapee.wc.front.RecordIOfferExchangeInfoRowScreenAR

/** @typedef {xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR} xyz.swapee.wc.front.BoundIOfferExchangeInfoRowScreenAR */

/** @typedef {xyz.swapee.wc.front.OfferExchangeInfoRowScreenAR} xyz.swapee.wc.front.BoundOfferExchangeInfoRowScreenAR */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IOfferExchangeInfoRowScreenARCaster
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenARCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowScreenAR_ instance into the _BoundIOfferExchangeInfoRowScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIOfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenARCaster.prototype.asIOfferExchangeInfoRowScreenAR
/**
 * Access the _OfferExchangeInfoRowScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundOfferExchangeInfoRowScreenAR}
 */
xyz.swapee.wc.front.IOfferExchangeInfoRowScreenARCaster.prototype.superOfferExchangeInfoRowScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/74-IOfferExchangeInfoRowScreenAT.xml}  e794c93f1fe5d1fa50dc09ac0ff36304 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)} xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT} xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.constructor&xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT|typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese[]) => xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT} xyz.swapee.wc.back.OfferExchangeInfoRowScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IOfferExchangeInfoRowScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferExchangeInfoRowScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT = class extends /** @type {xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese>)} xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT
 * @implements {xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferExchangeInfoRowScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT = class extends /** @type {xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.constructor&xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT} */
xyz.swapee.wc.back.RecordIOfferExchangeInfoRowScreenAT

/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowScreenAT} xyz.swapee.wc.back.BoundIOfferExchangeInfoRowScreenAT */

/** @typedef {xyz.swapee.wc.back.OfferExchangeInfoRowScreenAT} xyz.swapee.wc.back.BoundOfferExchangeInfoRowScreenAT */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IOfferExchangeInfoRowScreenATCaster
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenATCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowScreenAT_ instance into the _BoundIOfferExchangeInfoRowScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIOfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenATCaster.prototype.asIOfferExchangeInfoRowScreenAT
/**
 * Access the _OfferExchangeInfoRowScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundOfferExchangeInfoRowScreenAT}
 */
xyz.swapee.wc.back.IOfferExchangeInfoRowScreenATCaster.prototype.superOfferExchangeInfoRowScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/offer-exchange-info-row/OfferExchangeInfoRow.mvc/design/80-IOfferExchangeInfoRowGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese} xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.OfferExchangeInfoRowGPU)} xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU} xyz.swapee.wc.OfferExchangeInfoRowGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowGPU` interface.
 * @constructor xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU = class extends /** @type {xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.constructor&xyz.swapee.wc.OfferExchangeInfoRowGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.prototype.constructor = xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.class = /** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IOfferExchangeInfoRowGPU|typeof xyz.swapee.wc.OfferExchangeInfoRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay|typeof xyz.swapee.wc.back.OfferExchangeInfoRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese[]) => xyz.swapee.wc.IOfferExchangeInfoRowGPU} xyz.swapee.wc.OfferExchangeInfoRowGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IOfferExchangeInfoRowGPUCaster&com.webcircuits.IBrowserView<.!OfferExchangeInfoRowMemory,.!OfferExchangeInfoRowLand>&xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay)} xyz.swapee.wc.IOfferExchangeInfoRowGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!OfferExchangeInfoRowMemory,.!OfferExchangeInfoRowLand>} com.webcircuits.IBrowserView<.!OfferExchangeInfoRowMemory,.!OfferExchangeInfoRowLand>.typeof */
/**
 * Handles the periphery of the _IOfferExchangeInfoRowDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowGPU
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPU = class extends /** @type {xyz.swapee.wc.IOfferExchangeInfoRowGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!OfferExchangeInfoRowMemory,.!OfferExchangeInfoRowLand>.typeof&xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IOfferExchangeInfoRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IOfferExchangeInfoRowGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese>)} xyz.swapee.wc.OfferExchangeInfoRowGPU.constructor */
/**
 * A concrete class of _IOfferExchangeInfoRowGPU_ instances.
 * @constructor xyz.swapee.wc.OfferExchangeInfoRowGPU
 * @implements {xyz.swapee.wc.IOfferExchangeInfoRowGPU} Handles the periphery of the _IOfferExchangeInfoRowDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese>} ‎
 */
xyz.swapee.wc.OfferExchangeInfoRowGPU = class extends /** @type {xyz.swapee.wc.OfferExchangeInfoRowGPU.constructor&xyz.swapee.wc.IOfferExchangeInfoRowGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IOfferExchangeInfoRowGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IOfferExchangeInfoRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IOfferExchangeInfoRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.OfferExchangeInfoRowGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.OfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.OfferExchangeInfoRowGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IOfferExchangeInfoRowGPU.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowGPUFields
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowGPU} */
xyz.swapee.wc.RecordIOfferExchangeInfoRowGPU

/** @typedef {xyz.swapee.wc.IOfferExchangeInfoRowGPU} xyz.swapee.wc.BoundIOfferExchangeInfoRowGPU */

/** @typedef {xyz.swapee.wc.OfferExchangeInfoRowGPU} xyz.swapee.wc.BoundOfferExchangeInfoRowGPU */

/**
 * Contains getters to cast the _IOfferExchangeInfoRowGPU_ interface.
 * @interface xyz.swapee.wc.IOfferExchangeInfoRowGPUCaster
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPUCaster = class { }
/**
 * Cast the _IOfferExchangeInfoRowGPU_ instance into the _BoundIOfferExchangeInfoRowGPU_ type.
 * @type {!xyz.swapee.wc.BoundIOfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPUCaster.prototype.asIOfferExchangeInfoRowGPU
/**
 * Access the _OfferExchangeInfoRowGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundOfferExchangeInfoRowGPU}
 */
xyz.swapee.wc.IOfferExchangeInfoRowGPUCaster.prototype.superOfferExchangeInfoRowGPU

// nss:xyz.swapee.wc
/* @typal-end */