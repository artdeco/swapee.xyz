import OfferExchangeInfoRowHtmlController from '../OfferExchangeInfoRowHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractOfferExchangeInfoRowHtmlComponent} from '../../gen/AbstractOfferExchangeInfoRowHtmlComponent'

/** @extends {xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent} */
export default class extends AbstractOfferExchangeInfoRowHtmlComponent.implements(
 OfferExchangeInfoRowHtmlController,
 IntegratedComponentInitialiser,
){}