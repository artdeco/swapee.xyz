import solder from './methods/solder'
import server from './methods/server'
import buildExchangeIntent from './methods/build-exchange-intent'
import buildDealBroker from './methods/build-deal-broker'
import render from './methods/render'
import OfferExchangeInfoRowServerController from '../OfferExchangeInfoRowServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractOfferExchangeInfoRowElement from '../../gen/AbstractOfferExchangeInfoRowElement'

/** @extends {xyz.swapee.wc.OfferExchangeInfoRowElement} */
export default class OfferExchangeInfoRowElement extends AbstractOfferExchangeInfoRowElement.implements(
 OfferExchangeInfoRowServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowElement} */ ({
  solder:solder,
  server:server,
  buildExchangeIntent:buildExchangeIntent,
  buildDealBroker:buildDealBroker,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
   classesMap: true,
   rootSelector:     `.OfferExchangeInfoRow`,
   stylesheet:       'html/styles/OfferExchangeInfoRow.css',
   blockName:        'html/OfferExchangeInfoRowBlock.html',
  }),
){}

// thank you for using web circuits
