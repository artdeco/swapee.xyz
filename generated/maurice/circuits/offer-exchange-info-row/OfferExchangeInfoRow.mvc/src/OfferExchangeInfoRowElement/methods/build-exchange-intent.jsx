/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement._buildExchangeIntent} */
export default function buildExchangeIntent({
 currencyTo:currencyTo,
 currencyFrom:currencyFrom,
 amountFrom:amountFrom,
 ready:ready,
}){
 return <div $id="OfferExchangeInfoRow">
  {/* todo: $show */}
  <span $id="CryptoIn" $show={ready&&currencyFrom}>{currencyFrom}</span>
  <span $id="CryptoOut" $show={ready&&currencyTo}>{currencyTo}</span>
  <span $id="AmountIn" $show={ready&&amountFrom}>{amountFrom||0}</span>
 </div>
}