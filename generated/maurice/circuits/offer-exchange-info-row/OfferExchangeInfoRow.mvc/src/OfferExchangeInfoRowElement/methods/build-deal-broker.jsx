/** @type {xyz.swapee.wc.IOfferExchangeInfoRowElement._buildDealBroker} */
export default function buildDealBroker({
 estimatedAmountTo:estimatedAmountTo,
 estimatedFixedAmountTo:estimatedFixedAmountTo,
 estimatedFloatAmountTo:estimatedFloatAmountTo,
 getOfferError:getOfferError,
 gettingOffer:gettingOffer,
 networkFee:networkFee,
 partnerFee:partnerFee,
 rate:rate,
},{
 pulseGetOffer:pulseGetOffer,
}) {
 return <div $id="OfferExchangeInfoRow">
  <span $id="AmountOut" $show={estimatedAmountTo} $conceal={gettingOffer}>{estimatedAmountTo||0}</span>
  <span $id="GetOfferErrorWr" $reveal={getOfferError} onClick={pulseGetOffer}>
   <span $id="GetOfferErrorLa">
    {getOfferError}
   </span>
  </span>
  <span $id="AmountOutLoIn" $reveal={gettingOffer} />
  <button $id="ReloadGetOfferBu" onClick={pulseGetOffer} />

  <span $id="EstimatedTildeOut" $reveal={estimatedFloatAmountTo} $conceal={gettingOffer} />
  <span $id="EstimatedFixedTildeOut" $reveal={estimatedFixedAmountTo} $conceal={gettingOffer} />

  <span $id="NetworkFeeWr" $reveal={networkFee}>
   <span $id="NetworkFeeLa">{networkFee}</span>
  </span>
  <span $id="RateWr" $reveal={rate}>
   <span $id="RateLa">{rate}</span>
  </span>
  <span $id="PartnerFeeWr" $reveal={partnerFee}>
   <span $id="PartnerFeeLa">{partnerFee}</span>
  </span>
  {/* <span $id="VisibleAmountWr" $reveal={visibleAmount}>
   <span $id="VisibleAmounLa">{visibleAmount}</span>
  </span> */}

  <button $id="BreakdownBu" $reveal={networkFee} />
 </div>
}