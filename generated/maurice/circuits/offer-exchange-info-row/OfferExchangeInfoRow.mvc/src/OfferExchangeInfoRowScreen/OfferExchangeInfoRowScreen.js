import deduceInputs from './methods/deduce-inputs'
import AbstractOfferExchangeInfoRowControllerAT from '../../gen/AbstractOfferExchangeInfoRowControllerAT'
import OfferExchangeInfoRowDisplay from '../OfferExchangeInfoRowDisplay'
import AbstractOfferExchangeInfoRowScreen from '../../gen/AbstractOfferExchangeInfoRowScreen'

/** @extends {xyz.swapee.wc.OfferExchangeInfoRowScreen} */
export default class extends AbstractOfferExchangeInfoRowScreen.implements(
 AbstractOfferExchangeInfoRowControllerAT,
 OfferExchangeInfoRowDisplay,
 /**@type {!xyz.swapee.wc.IOfferExchangeInfoRowScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IOfferExchangeInfoRowScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:8711883565,
 }),
){}