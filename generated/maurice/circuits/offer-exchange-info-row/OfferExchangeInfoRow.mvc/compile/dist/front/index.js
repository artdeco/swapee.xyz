/**
 * Display for presenting information from the _IOfferExchangeInfoRow_.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowDisplay}
 */
class OfferExchangeInfoRowDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowScreen}
 */
class OfferExchangeInfoRowScreen extends (class {/* lazy-loaded */}) {}

module.exports.OfferExchangeInfoRowDisplay = OfferExchangeInfoRowDisplay
module.exports.OfferExchangeInfoRowScreen = OfferExchangeInfoRowScreen