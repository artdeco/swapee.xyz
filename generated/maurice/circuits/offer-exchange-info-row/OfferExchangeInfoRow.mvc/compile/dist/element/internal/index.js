import Module from './element'

/**@extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow}*/
export class AbstractOfferExchangeInfoRow extends Module['87118835651'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRow} */
AbstractOfferExchangeInfoRow.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangeInfoRowPort} */
export const OfferExchangeInfoRowPort=Module['87118835653']
/**@extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController}*/
export class AbstractOfferExchangeInfoRowController extends Module['87118835654'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowController} */
AbstractOfferExchangeInfoRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangeInfoRowElement} */
export const OfferExchangeInfoRowElement=Module['87118835658']
/** @type {typeof xyz.swapee.wc.OfferExchangeInfoRowBuffer} */
export const OfferExchangeInfoRowBuffer=Module['871188356511']
/**@extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer}*/
export class AbstractOfferExchangeInfoRowComputer extends Module['871188356530'] {}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} */
AbstractOfferExchangeInfoRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.OfferExchangeInfoRowController} */
export const OfferExchangeInfoRowController=Module['871188356561']