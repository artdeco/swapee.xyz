/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRow` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow}
 */
class AbstractOfferExchangeInfoRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferExchangeInfoRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
class OfferExchangeInfoRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController}
 */
class AbstractOfferExchangeInfoRowController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IOfferExchangeInfoRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowElement}
 */
class OfferExchangeInfoRowElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowBuffer}
 */
class OfferExchangeInfoRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer}
 */
class AbstractOfferExchangeInfoRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowController}
 */
class OfferExchangeInfoRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferExchangeInfoRow = AbstractOfferExchangeInfoRow
module.exports.OfferExchangeInfoRowPort = OfferExchangeInfoRowPort
module.exports.AbstractOfferExchangeInfoRowController = AbstractOfferExchangeInfoRowController
module.exports.OfferExchangeInfoRowElement = OfferExchangeInfoRowElement
module.exports.OfferExchangeInfoRowBuffer = OfferExchangeInfoRowBuffer
module.exports.AbstractOfferExchangeInfoRowComputer = AbstractOfferExchangeInfoRowComputer
module.exports.OfferExchangeInfoRowController = OfferExchangeInfoRowController

Object.defineProperties(module.exports, {
 'AbstractOfferExchangeInfoRow': {get: () => require('./precompile/internal')[87118835651]},
 [87118835651]: {get: () => module.exports['AbstractOfferExchangeInfoRow']},
 'OfferExchangeInfoRowPort': {get: () => require('./precompile/internal')[87118835653]},
 [87118835653]: {get: () => module.exports['OfferExchangeInfoRowPort']},
 'AbstractOfferExchangeInfoRowController': {get: () => require('./precompile/internal')[87118835654]},
 [87118835654]: {get: () => module.exports['AbstractOfferExchangeInfoRowController']},
 'OfferExchangeInfoRowElement': {get: () => require('./precompile/internal')[87118835658]},
 [87118835658]: {get: () => module.exports['OfferExchangeInfoRowElement']},
 'OfferExchangeInfoRowBuffer': {get: () => require('./precompile/internal')[871188356511]},
 [871188356511]: {get: () => module.exports['OfferExchangeInfoRowBuffer']},
 'AbstractOfferExchangeInfoRowComputer': {get: () => require('./precompile/internal')[871188356530]},
 [871188356530]: {get: () => module.exports['AbstractOfferExchangeInfoRowComputer']},
 'OfferExchangeInfoRowController': {get: () => require('./precompile/internal')[871188356561]},
 [871188356561]: {get: () => module.exports['OfferExchangeInfoRowController']},
})