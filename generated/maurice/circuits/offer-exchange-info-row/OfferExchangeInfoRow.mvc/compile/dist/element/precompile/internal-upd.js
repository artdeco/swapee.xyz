import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {8711883565} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const d=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const f=d["372700389811"];function h(a,b,c,e){return d["372700389812"](a,b,c,e,!1,void 0)};function k(){}k.prototype={};class aa{}class l extends h(aa,87118835658,null,{$:1,Da:2}){}l[f]=[k];


const p=function(){return require(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const q={core:"a74ad"};function r(){}r.prototype={};class ba{}class t extends h(ba,87118835657,null,{W:1,ya:2}){}function w(){}w.prototype={};function x(){this.model={core:""}}class ca{}class y extends h(ca,87118835653,x,{Y:1,Ba:2}){}y[f]=[w,{constructor(){p(this.model,q)}}];t[f]=[{},r,y];

const z=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const da=z.IntegratedComponentInitialiser,B=z.IntegratedComponent,ea=z["95173443851"],fa=z["95173443852"];
const C=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ha=C["615055805212"],ia=C["615055805218"],ja=C["615055805235"];function D(){}D.prototype={};class ka{}class E extends h(ka,87118835651,null,{U:1,wa:2}){}E[f]=[D,ia];const F={regulate:ha({core:String})};
const G=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=G.IntegratedController,ma=G.Parametric;const H={...q};function I(){}I.prototype={};function J(){const a={model:null};x.call(a);this.inputs=a.model}class na{}class K extends h(na,87118835655,J,{Z:1,Ca:2}){}function L(){}K[f]=[L.prototype={resetPort(){J.call(this)}},I,ma,L.prototype={constructor(){p(this.inputs,H)}}];function M(){}M.prototype={};class oa{}class N extends h(oa,871188356521,null,{V:1,xa:2}){}N[f]=[{resetPort(){this.port.resetPort()}},M,F,la,{get Port(){return K}}];function O(){}O.prototype={};class pa{}class P extends h(pa,87118835659,null,{T:1,va:2}){}P[f]=[O,t,l,B,E,N];function qa(){return{}};const ra=require(eval('"@type.engineering/web-computing"')).h;function sa(){return ra("div",{$id:"OfferExchangeInfoRow"})};const Q=require(eval('"@type.engineering/web-computing"')).h;function ta({A:a,v:b,o:c,ready:e}){return Q("div",{$id:"OfferExchangeInfoRow"},Q("span",{$id:"CryptoIn",$show:e&&b},b),Q("span",{$id:"CryptoOut",$show:e&&a},a),Q("span",{$id:"AmountIn",$show:e&&c},c||0))};const R=require(eval('"@type.engineering/web-computing"')).h;
function ua({D:a,F:b,G:c,H:e,I:g,J:n,K:m,rate:u},{ra:v}){return R("div",{$id:"OfferExchangeInfoRow"},R("span",{$id:"AmountOut",$show:a,$conceal:g},a||0),R("span",{$id:"GetOfferErrorWr",$reveal:e,onClick:v},R("span",{$id:"GetOfferErrorLa"},e)),R("span",{$id:"AmountOutLoIn",$reveal:g}),R("button",{$id:"ReloadGetOfferBu",onClick:v}),R("span",{$id:"EstimatedTildeOut",$reveal:c,$conceal:g}),R("span",{$id:"EstimatedFixedTildeOut",$reveal:b,$conceal:g}),R("span",{$id:"NetworkFeeWr",$reveal:n},R("span",{$id:"NetworkFeeLa"},
n)),R("span",{$id:"RateWr",$reveal:u},R("span",{$id:"RateLa"},u)),R("span",{$id:"PartnerFeeWr",$reveal:m},R("span",{$id:"PartnerFeeLa"},m)),R("button",{$id:"BreakdownBu",$reveal:n}))};const va=require(eval('"@type.engineering/web-computing"')).h;function wa(){return va("div",{$id:"OfferExchangeInfoRow"})};var S=class extends N.implements(){};require("https");require("http");const xa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}xa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const ya=T.ElementBase,za=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function Aa(){this.inputs={noSolder:!1,na:{},ma:{},qa:{},pa:{},la:{},oa:{},ba:{},da:{},M:{},R:{},aa:{},ua:{},P:{},ka:{},ta:{},sa:{},ja:{},ga:{},fa:{},ea:{},ia:{}}}class Ba{}class W extends h(Ba,871188356514,Aa,{X:1,Aa:2}){}
W[f]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"network-fee-wr-opts":void 0,"network-fee-la-opts":void 0,"partner-fee-wr-opts":void 0,"partner-fee-la-opts":void 0,"in-im-wr-opts":void 0,"out-im-wr-opts":void 0,"crypto-in-opts":void 0,"crypto-out-opts":void 0,"amount-in-opts":void 0,"amount-out-opts":void 0,"breakdown-bu-opts":void 0,"reload-get-offer-bu-opts":void 0,"amount-out-lo-in-opts":void 0,"get-offer-error-wr-opts":void 0,"rate-wr-opts":void 0,"rate-la-opts":void 0,
"get-offer-error-la-opts":void 0,"estimated-tilde-out-opts":void 0,"estimated-fixed-tilde-out-opts":void 0,"deal-broker-opts":void 0,"exchange-intent-opts":void 0})}}];function X(){}X.prototype={};class Ca{}class Y extends h(Ca,871188356513,null,{m:1,za:2}){}function Z(){}
Y[f]=[X,ya,Z.prototype={calibrate:function({":no-solder":a,":core":b}){const {attributes:{"no-solder":c,core:e}}=this;return{...(void 0===c?{"no-solder":a}:{}),...(void 0===e?{core:b}:{})}}},Z.prototype={calibrate:({"no-solder":a,core:b})=>({noSolder:a,core:b})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},ja,Z.prototype={render:function(){const {asILanded:{land:{j:a}},m:{s:b}}=this;if(a){var {c63f7:c,ea0a0:e,"8ca05":g,"5f4a3":n,"341da":m,"5fd9c":u,"96f44":v,67942:Da}=a.model,A=b({D:c,F:e,
G:g,H:n,I:m,J:u,K:v,rate:Da},{});A.attributes.$id=this.rootId;A.nodeName="div";return A}}},Z.prototype={render:function(){const {asILanded:{land:{l:a}},m:{u:b}}=this;if(a){var {c23cd:c,"96c88":e,"748e6":g,b2fda:n}=a.model,m=b({A:c,v:e,o:g,ready:n},{});m.attributes.$id=this.rootId;m.nodeName="div";return m}}},Z.prototype={constructor(){Object.assign(this,{land:{j:null,l:null}})}},Z.prototype={render:function(){return U("div",{$id:"OfferExchangeInfoRow"},U("vdu",{$id:"NetworkFeeWr"}),U("vdu",{$id:"NetworkFeeLa"}),
U("vdu",{$id:"PartnerFeeWr"}),U("vdu",{$id:"PartnerFeeLa"}),U("vdu",{$id:"InImWr"}),U("vdu",{$id:"OutImWr"}),U("vdu",{$id:"CryptoIn"}),U("vdu",{$id:"CryptoOut"}),U("vdu",{$id:"AmountIn"}),U("vdu",{$id:"AmountOut"}),U("vdu",{$id:"BreakdownBu"}),U("vdu",{$id:"ReloadGetOfferBu"}),U("vdu",{$id:"AmountOutLoIn"}),U("vdu",{$id:"GetOfferErrorWr"}),U("vdu",{$id:"RateWr"}),U("vdu",{$id:"RateLa"}),U("vdu",{$id:"GetOfferErrorLa"}),U("vdu",{$id:"EstimatedTildeOut"}),U("vdu",{$id:"EstimatedFixedTildeOut"}))}},
Z.prototype={classes:{CryptoAmount:"81b89"},inputsPQs:H,queriesPQs:{ha:"ecfe5",g:"f9c2e",i:"13da4"},vdus:{NetworkFeeWr:"hd091",NetworkFeeLa:"hd092",PartnerFeeWr:"hd093",PartnerFeeLa:"hd094",AmountIn:"hd099",AmountOut:"hd0910",ReloadGetOfferBu:"hd0911",AmountOutLoIn:"hd0912",GetOfferErrorWr:"hd0913",GetOfferErrorLa:"hd0914",EstimatedTildeOut:"hd0915",EstimatedFixedTildeOut:"hd0916",BreakdownBu:"hd0920",DealBroker:"hd0921",ExchangeIntent:"hd0922",RateLa:"hd0925",RateWr:"hd0926",InImWr:"hd0927",OutImWr:"hd0928",
CryptoIn:"hd0923",CryptoOut:"hd0924"}},B,fa,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder core query:deal-broker query:exchange-intent no-solder :no-solder :core fe646 6c941 72e15 97166 ed9b2 4e37d c61f1 3ce39 2de4e b88f2 cd1af 54094 cdf62 365b9 bf2c1 82f48 e0e01 b47f4 c1be4 65c2c c9e36 c3b6b a74ad children".split(" "))})},get Port(){return W},calibrate:function({"query:deal-broker":a,"query:exchange-intent":b}){const c={};a&&(c.g=a);b&&(c.i=b);return c}},Z.prototype=
{constructor(){this.land={j:null,l:null}}},Z.prototype={calibrate:async function({g:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:c},asIElement:{fqn:e}}=this,g=await b(a,!0);if(!g)return console.warn("\\u2757\\ufe0f dealBrokerSel %s must be present on the page for %s to work",a,e),{};c.j=g}},Z.prototype={calibrate:async function({i:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:c},asIElement:{fqn:e}}=this,g=await b(a,!0);if(!g)return console.warn("\\u2757\\ufe0f exchangeIntentSel %s must be present on the page for %s to work",
a,e),{};c.l=g}},Z.prototype={solder:(a,{g:b,i:c})=>({g:b,i:c})}];Y[f]=[P,{rootId:"OfferExchangeInfoRow",__$id:8711883565,fqn:"xyz.swapee.wc.IOfferExchangeInfoRow",maurice_element_v3:!0}];class Ea extends Y.implements(S,ea,za,da,{solder:qa,server:sa,u:ta,s:ua,render:wa},{classesMap:!0,rootSelector:".OfferExchangeInfoRow",stylesheet:"html/styles/OfferExchangeInfoRow.css",blockName:"html/OfferExchangeInfoRowBlock.html"}){};module.exports["87118835650"]=P;module.exports["87118835651"]=P;module.exports["87118835653"]=K;module.exports["87118835654"]=N;module.exports["87118835658"]=Ea;module.exports["871188356511"]=F;module.exports["871188356530"]=E;module.exports["871188356561"]=S;
/*! @embed-object-end {8711883565} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule