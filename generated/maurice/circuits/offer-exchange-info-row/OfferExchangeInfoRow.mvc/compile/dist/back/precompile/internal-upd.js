import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {8711883565} */
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const e=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const la=e["372700389810"],h=e["372700389811"];function k(b,a,c,d){return e["372700389812"](b,a,c,d,!1,void 0)};function l(){}l.prototype={};class ma{}class n extends k(ma,87118835658,null,{fa:1,sa:2}){}n[h]=[l];


const p=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/seers"'))}()["420776143822"];const q={R:"a74ad"};function r(){}r.prototype={};class na{}class t extends k(na,87118835657,null,{aa:1,la:2}){}function u(){}u.prototype={};function v(){this.model={R:""}}class oa{}class w extends k(oa,87118835653,v,{da:1,qa:2}){}w[h]=[u,{constructor(){p(this.model,q)}}];t[h]=[{},r,w];

const x=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const pa=x.IntegratedController,qa=x.Parametric;
const y=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ra=y["61505580523"],sa=y["615055805212"],ta=y["615055805218"],ua=y["615055805221"],va=y["615055805223"],wa=y["615055805235"];
const z=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const xa=z.IntegratedComponentInitialiser,ya=z.IntegratedComponent,za=z["38"];function A(){}A.prototype={};class Aa{}class B extends k(Aa,87118835651,null,{Z:1,ja:2}){}B[h]=[A,ta];const C={regulate:sa({R:String})};const D={...q};function E(){}E.prototype={};function F(){const b={model:null};v.call(b);this.inputs=b.model}class Ba{}class G extends k(Ba,87118835655,F,{ea:1,ra:2}){}function H(){}G[h]=[H.prototype={resetPort(){F.call(this)}},E,qa,H.prototype={constructor(){p(this.inputs,D)}}];function I(){}I.prototype={};class Ca{}class J extends k(Ca,871188356521,null,{T:1,W:2}){}J[h]=[{resetPort(){this.port.resetPort()}},I,C,pa,{get Port(){return G}}];function K(){}K.prototype={};class Da{}class L extends k(Da,87118835659,null,{Y:1,ia:2}){}L[h]=[K,t,n,ya,B,J];
const M=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const Ea=M["12817393923"],Fa=M["12817393924"],Ga=M["12817393925"],Ha=M["12817393926"];function N(){}N.prototype={};class Ia{}class O extends k(Ia,871188356524,null,{$:1,ka:2}){}O[h]=[N,Ha,{allocator(){this.methods={}}}];function P(){}P.prototype={};class Ja{}class Q extends k(Ja,871188356523,null,{T:1,W:2}){}Q[h]=[P,J,O,Ea];var R=class extends Q.implements(){};function S(){}S.prototype={};function Ka(){this.m=[];this.o=[]}class La{}class T extends k(La,871188356520,Ka,{ba:1,ma:2}){}
T[h]=[S,Fa,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:b}}=this;Object.assign(this,{G:b,F:b,I:b,H:b,M:b,O:b,l:b,j:b,v:b,L:b,u:b,s:b,K:b,J:b,D:b,C:b,A:b,g:b,i:b})}},{[la]:{G:1,F:1,I:1,H:1,M:1,O:1,m:1,o:1,l:1,j:1,v:1,L:1,u:1,s:1,K:1,J:1,D:1,C:1,A:1,g:1,i:1},initializer({G:b,F:a,I:c,H:d,M:f,O:g,m,o:W,l:X,j:Y,v:Z,L:aa,u:ba,s:ca,K:da,J:ea,D:fa,C:ha,A:ia,g:ja,i:ka}){void 0!==b&&(this.G=b);void 0!==a&&(this.F=a);void 0!==c&&(this.I=c);void 0!==d&&(this.H=d);void 0!==
f&&(this.M=f);void 0!==g&&(this.O=g);void 0!==m&&(this.m=m);void 0!==W&&(this.o=W);void 0!==X&&(this.l=X);void 0!==Y&&(this.j=Y);void 0!==Z&&(this.v=Z);void 0!==aa&&(this.L=aa);void 0!==ba&&(this.u=ba);void 0!==ca&&(this.s=ca);void 0!==da&&(this.K=da);void 0!==ea&&(this.J=ea);void 0!==fa&&(this.D=fa);void 0!==ha&&(this.C=ha);void 0!==ia&&(this.A=ia);void 0!==ja&&(this.g=ja);void 0!==ka&&(this.i=ka)}}];const U={X:"81b89"};const Ma=Object.keys(U).reduce((b,a)=>{b[U[a]]=a;return b},{});const Na={G:"hd091",F:"hd092",I:"hd093",H:"hd094",l:"hd099",j:"hd0910",L:"hd0911",u:"hd0912",s:"hd0913",D:"hd0914",C:"hd0915",A:"hd0916",v:"hd0920",g:"hd0921",i:"hd0922",m:"hd0923",o:"hd0924",J:"hd0925",K:"hd0926",M:"hd0927",O:"hd0928"};const Oa=Object.keys(Na).reduce((b,a)=>{b[Na[a]]=a;return b},{});function Pa(){}Pa.prototype={};class Qa{}class Ra extends k(Qa,871188356516,null,{h:1,oa:2}){}function Sa(){}Ra[h]=[Pa,Sa.prototype={classesQPs:Ma,vdusQPs:Oa,memoryPQs:q},T,ra,Sa.prototype={allocator(){za(this.classes,"",U)}}];function Ta(){}Ta.prototype={};class Ua{}class Va extends k(Ua,871188356529,null,{ha:1,ua:2}){}Va[h]=[Ta,Ga];function Wa(){}Wa.prototype={};class Xa{}class Ya extends k(Xa,871188356527,null,{ga:1,ta:2}){}Ya[h]=[Wa,Va];const Za=Object.keys(D).reduce((b,a)=>{b[D[a]]=a;return b},{});function $a(){}$a.prototype={};class ab{static mvc(b,a,c){return va(this,b,a,null,c)}}class bb extends k(ab,871188356512,null,{ca:1,pa:2}){}function V(){}
bb[h]=[$a,ua,L,Ra,Ya,wa,V.prototype={constructor(){this.land={g:null,i:null}}},V.prototype={inputsQPs:Za},V.prototype={paint:function(b,{g:a}){if(a){({c63f7:b}=a.model);var {h:{j:c},asIBrowserView:{style:d}}=this;d(c,"opacity",b?null:0,!0);d(c,"pointer-events",b?null:"none",!0)}}},V.prototype={paint:function(b,{g:a}){if(a){({"341da":b}=a.model);var {h:{j:c},asIBrowserView:{conceal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({c63f7:b}=a.model,{h:{j:a}}=this,a.setText(b||0))}},V.prototype=
{paint:function(b,{g:a}){if(a){({"5f4a3":b}=a.model);var {h:{s:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({h:{s:b}}=this,b.listen("click",()=>{a.e0519()}))}},V.prototype={paint:function(b,{g:a}){a&&({"5f4a3":b}=a.model,{h:{D:a}}=this,a.setText(b))}},V.prototype={paint:function(b,{g:a}){if(a){({"341da":b}=a.model);var {h:{u:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({h:{L:b}}=this,b.listen("click",()=>{a.e0519()}))}},
V.prototype={paint:function(b,{g:a}){if(a){var {"341da":c,"8ca05":d}=a.model,{h:{C:f},asIBrowserView:{conceal:g,reveal:m}}=this;({P:c,V:d}={P:c,V:d});c?g(f,!0):m(f,d)}}},V.prototype={paint:function(b,{g:a}){if(a){var {"341da":c,ea0a0:d}=a.model,{h:{A:f},asIBrowserView:{conceal:g,reveal:m}}=this;({P:c,U:d}={P:c,U:d});c?g(f,!0):m(f,d)}}},V.prototype={paint:function(b,{g:a}){if(a){({"5fd9c":b}=a.model);var {h:{G:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({"5fd9c":b}=
a.model,{h:{F:a}}=this,a.setText(b))}},V.prototype={paint:function(b,{g:a}){if(a){({67942:b}=a.model);var {h:{K:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({67942:b}=a.model,{h:{J:a}}=this,a.setText(b))}},V.prototype={paint:function(b,{g:a}){if(a){({"96f44":b}=a.model);var {h:{I:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{g:a}){a&&({"96f44":b}=a.model,{h:{H:a}}=this,a.setText(b))}},V.prototype={paint:function(b,{g:a}){if(a){({"5fd9c":b}=
a.model);var {h:{v:c},asIBrowserView:{reveal:d}}=this;d(c,b)}}},V.prototype={paint:function(b,{i:a}){if(a){var {b2fda:c,"96c88":d}=a.model,{h:{m:f},asIBrowserView:{style:g}}=this;g(f,"opacity",c&&d?null:0,!0);g(f,"pointer-events",c&&d?null:"none",!0)}}},V.prototype={paint:function(b,{i:a}){if(a){({"96c88":b}=a.model);({h:{m:a}}=this);for(const c of a)c.setText(b)}}},V.prototype={paint:function(b,{i:a}){if(a){var {b2fda:c,c23cd:d}=a.model,{h:{o:f},asIBrowserView:{style:g}}=this;g(f,"opacity",c&&d?
null:0,!0);g(f,"pointer-events",c&&d?null:"none",!0)}}},V.prototype={paint:function(b,{i:a}){if(a){({c23cd:b}=a.model);({h:{o:a}}=this);for(const c of a)c.setText(b)}}},V.prototype={paint:function(b,{i:a}){if(a){var {b2fda:c,"748e6":d}=a.model,{h:{l:f},asIBrowserView:{style:g}}=this;g(f,"opacity",c&&d?null:0,!0);g(f,"pointer-events",c&&d?null:"none",!0)}}},V.prototype={paint:function(b,{i:a}){a&&({"748e6":b}=a.model,{h:{l:a}}=this,a.setText(b||0))}},V.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:b},
h:{g:a,i:c}}=this;b(3427226314,{g:a});b(7833868048,{i:c})}}];var cb=class extends bb.implements(R,xa){};module.exports["87118835650"]=L;module.exports["87118835651"]=L;module.exports["87118835653"]=G;module.exports["87118835654"]=J;module.exports["871188356510"]=cb;module.exports["871188356511"]=C;module.exports["871188356530"]=B;module.exports["871188356561"]=R;
/*! @embed-object-end {8711883565} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule