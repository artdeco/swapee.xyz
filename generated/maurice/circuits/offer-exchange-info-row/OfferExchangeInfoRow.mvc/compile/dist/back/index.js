/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRow` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow}
 */
class AbstractOfferExchangeInfoRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IOfferExchangeInfoRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowPort}
 */
class OfferExchangeInfoRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowController` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController}
 */
class AbstractOfferExchangeInfoRowController extends (class {/* lazy-loaded */}) {}
/**
 * The _IOfferExchangeInfoRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent}
 */
class OfferExchangeInfoRowHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.OfferExchangeInfoRowBuffer}
 */
class OfferExchangeInfoRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IOfferExchangeInfoRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer}
 */
class AbstractOfferExchangeInfoRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.OfferExchangeInfoRowController}
 */
class OfferExchangeInfoRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractOfferExchangeInfoRow = AbstractOfferExchangeInfoRow
module.exports.OfferExchangeInfoRowPort = OfferExchangeInfoRowPort
module.exports.AbstractOfferExchangeInfoRowController = AbstractOfferExchangeInfoRowController
module.exports.OfferExchangeInfoRowHtmlComponent = OfferExchangeInfoRowHtmlComponent
module.exports.OfferExchangeInfoRowBuffer = OfferExchangeInfoRowBuffer
module.exports.AbstractOfferExchangeInfoRowComputer = AbstractOfferExchangeInfoRowComputer
module.exports.OfferExchangeInfoRowController = OfferExchangeInfoRowController