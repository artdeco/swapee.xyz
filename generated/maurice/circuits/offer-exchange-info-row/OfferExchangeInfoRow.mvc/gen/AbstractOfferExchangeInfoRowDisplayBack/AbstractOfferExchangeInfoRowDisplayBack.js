import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowDisplay}
 */
function __AbstractOfferExchangeInfoRowDisplay() {}
__AbstractOfferExchangeInfoRowDisplay.prototype = /** @type {!_AbstractOfferExchangeInfoRowDisplay} */ ({ })
/** @this {xyz.swapee.wc.back.OfferExchangeInfoRowDisplay} */ function OfferExchangeInfoRowDisplayConstructor() {
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CryptoIns=[]
  /** @type {!Array<!com.webcircuits.IHtmlTwin>} */ this.CryptoOuts=[]
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay}
 */
class _AbstractOfferExchangeInfoRowDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay} ‎
 */
class AbstractOfferExchangeInfoRowDisplay extends newAbstract(
 _AbstractOfferExchangeInfoRowDisplay,871188356520,OfferExchangeInfoRowDisplayConstructor,{
  asIOfferExchangeInfoRowDisplay:1,
  superOfferExchangeInfoRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay} */
AbstractOfferExchangeInfoRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowDisplay} */
function AbstractOfferExchangeInfoRowDisplayClass(){}

export default AbstractOfferExchangeInfoRowDisplay


AbstractOfferExchangeInfoRowDisplay[$implementations]=[
 __AbstractOfferExchangeInfoRowDisplay,
 GraphicsDriverBack,
 AbstractOfferExchangeInfoRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay}*/({
    NetworkFeeWr:twinMock,
    NetworkFeeLa:twinMock,
    PartnerFeeWr:twinMock,
    PartnerFeeLa:twinMock,
    InImWr:twinMock,
    OutImWr:twinMock,
    AmountIn:twinMock,
    AmountOut:twinMock,
    BreakdownBu:twinMock,
    ReloadGetOfferBu:twinMock,
    AmountOutLoIn:twinMock,
    GetOfferErrorWr:twinMock,
    RateWr:twinMock,
    RateLa:twinMock,
    GetOfferErrorLa:twinMock,
    EstimatedTildeOut:twinMock,
    EstimatedFixedTildeOut:twinMock,
    DealBroker:twinMock,
    ExchangeIntent:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.OfferExchangeInfoRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IOfferExchangeInfoRowDisplay.Initialese}*/({
   NetworkFeeWr:1,
   NetworkFeeLa:1,
   PartnerFeeWr:1,
   PartnerFeeLa:1,
   InImWr:1,
   OutImWr:1,
   CryptoIns:1,
   CryptoOuts:1,
   AmountIn:1,
   AmountOut:1,
   BreakdownBu:1,
   ReloadGetOfferBu:1,
   AmountOutLoIn:1,
   GetOfferErrorWr:1,
   RateWr:1,
   RateLa:1,
   GetOfferErrorLa:1,
   EstimatedTildeOut:1,
   EstimatedFixedTildeOut:1,
   DealBroker:1,
   ExchangeIntent:1,
  }),
  initializer({
   NetworkFeeWr:_NetworkFeeWr,
   NetworkFeeLa:_NetworkFeeLa,
   PartnerFeeWr:_PartnerFeeWr,
   PartnerFeeLa:_PartnerFeeLa,
   InImWr:_InImWr,
   OutImWr:_OutImWr,
   CryptoIns:_CryptoIns,
   CryptoOuts:_CryptoOuts,
   AmountIn:_AmountIn,
   AmountOut:_AmountOut,
   BreakdownBu:_BreakdownBu,
   ReloadGetOfferBu:_ReloadGetOfferBu,
   AmountOutLoIn:_AmountOutLoIn,
   GetOfferErrorWr:_GetOfferErrorWr,
   RateWr:_RateWr,
   RateLa:_RateLa,
   GetOfferErrorLa:_GetOfferErrorLa,
   EstimatedTildeOut:_EstimatedTildeOut,
   EstimatedFixedTildeOut:_EstimatedFixedTildeOut,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_NetworkFeeWr!==undefined) this.NetworkFeeWr=_NetworkFeeWr
   if(_NetworkFeeLa!==undefined) this.NetworkFeeLa=_NetworkFeeLa
   if(_PartnerFeeWr!==undefined) this.PartnerFeeWr=_PartnerFeeWr
   if(_PartnerFeeLa!==undefined) this.PartnerFeeLa=_PartnerFeeLa
   if(_InImWr!==undefined) this.InImWr=_InImWr
   if(_OutImWr!==undefined) this.OutImWr=_OutImWr
   if(_CryptoIns!==undefined) this.CryptoIns=_CryptoIns
   if(_CryptoOuts!==undefined) this.CryptoOuts=_CryptoOuts
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_BreakdownBu!==undefined) this.BreakdownBu=_BreakdownBu
   if(_ReloadGetOfferBu!==undefined) this.ReloadGetOfferBu=_ReloadGetOfferBu
   if(_AmountOutLoIn!==undefined) this.AmountOutLoIn=_AmountOutLoIn
   if(_GetOfferErrorWr!==undefined) this.GetOfferErrorWr=_GetOfferErrorWr
   if(_RateWr!==undefined) this.RateWr=_RateWr
   if(_RateLa!==undefined) this.RateLa=_RateLa
   if(_GetOfferErrorLa!==undefined) this.GetOfferErrorLa=_GetOfferErrorLa
   if(_EstimatedTildeOut!==undefined) this.EstimatedTildeOut=_EstimatedTildeOut
   if(_EstimatedFixedTildeOut!==undefined) this.EstimatedFixedTildeOut=_EstimatedFixedTildeOut
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]