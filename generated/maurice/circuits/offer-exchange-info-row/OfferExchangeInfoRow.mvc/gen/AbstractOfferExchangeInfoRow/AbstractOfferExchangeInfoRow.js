import AbstractOfferExchangeInfoRowProcessor from '../AbstractOfferExchangeInfoRowProcessor'
import {OfferExchangeInfoRowCore} from '../OfferExchangeInfoRowCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractOfferExchangeInfoRowComputer} from '../AbstractOfferExchangeInfoRowComputer'
import {AbstractOfferExchangeInfoRowController} from '../AbstractOfferExchangeInfoRowController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRow}
 */
function __AbstractOfferExchangeInfoRow() {}
__AbstractOfferExchangeInfoRow.prototype = /** @type {!_AbstractOfferExchangeInfoRow} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow}
 */
class _AbstractOfferExchangeInfoRow { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow} ‎
 */
class AbstractOfferExchangeInfoRow extends newAbstract(
 _AbstractOfferExchangeInfoRow,87118835659,null,{
  asIOfferExchangeInfoRow:1,
  superOfferExchangeInfoRow:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRow} */
AbstractOfferExchangeInfoRow.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRow} */
function AbstractOfferExchangeInfoRowClass(){}

export default AbstractOfferExchangeInfoRow


AbstractOfferExchangeInfoRow[$implementations]=[
 __AbstractOfferExchangeInfoRow,
 OfferExchangeInfoRowCore,
 AbstractOfferExchangeInfoRowProcessor,
 IntegratedComponent,
 AbstractOfferExchangeInfoRowComputer,
 AbstractOfferExchangeInfoRowController,
]


export {AbstractOfferExchangeInfoRow}