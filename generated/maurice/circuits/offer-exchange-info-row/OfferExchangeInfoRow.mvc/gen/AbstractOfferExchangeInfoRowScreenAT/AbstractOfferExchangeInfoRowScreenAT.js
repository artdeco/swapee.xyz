import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowScreenAT}
 */
function __AbstractOfferExchangeInfoRowScreenAT() {}
__AbstractOfferExchangeInfoRowScreenAT.prototype = /** @type {!_AbstractOfferExchangeInfoRowScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT}
 */
class _AbstractOfferExchangeInfoRowScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IOfferExchangeInfoRowScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT} ‎
 */
class AbstractOfferExchangeInfoRowScreenAT extends newAbstract(
 _AbstractOfferExchangeInfoRowScreenAT,871188356529,null,{
  asIOfferExchangeInfoRowScreenAT:1,
  superOfferExchangeInfoRowScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT} */
AbstractOfferExchangeInfoRowScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreenAT} */
function AbstractOfferExchangeInfoRowScreenATClass(){}

export default AbstractOfferExchangeInfoRowScreenAT


AbstractOfferExchangeInfoRowScreenAT[$implementations]=[
 __AbstractOfferExchangeInfoRowScreenAT,
 UartUniversal,
]