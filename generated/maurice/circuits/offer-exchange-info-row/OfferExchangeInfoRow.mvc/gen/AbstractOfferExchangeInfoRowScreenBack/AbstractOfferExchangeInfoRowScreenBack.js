import AbstractOfferExchangeInfoRowScreenAT from '../AbstractOfferExchangeInfoRowScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowScreenBack}
 */
function __AbstractOfferExchangeInfoRowScreenBack() {}
__AbstractOfferExchangeInfoRowScreenBack.prototype = /** @type {!_AbstractOfferExchangeInfoRowScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen}
 */
class _AbstractOfferExchangeInfoRowScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen} ‎
 */
class AbstractOfferExchangeInfoRowScreenBack extends newAbstract(
 _AbstractOfferExchangeInfoRowScreenBack,871188356527,null,{
  asIOfferExchangeInfoRowScreen:1,
  superOfferExchangeInfoRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen} */
AbstractOfferExchangeInfoRowScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowScreen} */
function AbstractOfferExchangeInfoRowScreenBackClass(){}

export default AbstractOfferExchangeInfoRowScreenBack


AbstractOfferExchangeInfoRowScreenBack[$implementations]=[
 __AbstractOfferExchangeInfoRowScreenBack,
 AbstractOfferExchangeInfoRowScreenAT,
]