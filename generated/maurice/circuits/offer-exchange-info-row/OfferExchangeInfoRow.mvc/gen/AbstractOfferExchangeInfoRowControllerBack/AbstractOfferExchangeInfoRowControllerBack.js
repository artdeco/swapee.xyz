import AbstractOfferExchangeInfoRowControllerAR from '../AbstractOfferExchangeInfoRowControllerAR'
import {AbstractOfferExchangeInfoRowController} from '../AbstractOfferExchangeInfoRowController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowControllerBack}
 */
function __AbstractOfferExchangeInfoRowControllerBack() {}
__AbstractOfferExchangeInfoRowControllerBack.prototype = /** @type {!_AbstractOfferExchangeInfoRowControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController}
 */
class _AbstractOfferExchangeInfoRowControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController} ‎
 */
class AbstractOfferExchangeInfoRowControllerBack extends newAbstract(
 _AbstractOfferExchangeInfoRowControllerBack,871188356523,null,{
  asIOfferExchangeInfoRowController:1,
  superOfferExchangeInfoRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController} */
AbstractOfferExchangeInfoRowControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowController} */
function AbstractOfferExchangeInfoRowControllerBackClass(){}

export default AbstractOfferExchangeInfoRowControllerBack


AbstractOfferExchangeInfoRowControllerBack[$implementations]=[
 __AbstractOfferExchangeInfoRowControllerBack,
 AbstractOfferExchangeInfoRowController,
 AbstractOfferExchangeInfoRowControllerAR,
 DriverBack,
]