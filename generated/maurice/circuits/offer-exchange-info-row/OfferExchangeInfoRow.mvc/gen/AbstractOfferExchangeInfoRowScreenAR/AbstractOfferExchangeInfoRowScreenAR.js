import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowScreenAR}
 */
function __AbstractOfferExchangeInfoRowScreenAR() {}
__AbstractOfferExchangeInfoRowScreenAR.prototype = /** @type {!_AbstractOfferExchangeInfoRowScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR}
 */
class _AbstractOfferExchangeInfoRowScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IOfferExchangeInfoRowScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR} ‎
 */
class AbstractOfferExchangeInfoRowScreenAR extends newAbstract(
 _AbstractOfferExchangeInfoRowScreenAR,871188356528,null,{
  asIOfferExchangeInfoRowScreenAR:1,
  superOfferExchangeInfoRowScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR} */
AbstractOfferExchangeInfoRowScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowScreenAR} */
function AbstractOfferExchangeInfoRowScreenARClass(){}

export default AbstractOfferExchangeInfoRowScreenAR


AbstractOfferExchangeInfoRowScreenAR[$implementations]=[
 __AbstractOfferExchangeInfoRowScreenAR,
 AR,
 AbstractOfferExchangeInfoRowScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferExchangeInfoRowScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractOfferExchangeInfoRowScreenAR}