import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeInfoRowElementPort}
 */
function __OfferExchangeInfoRowElementPort() {}
__OfferExchangeInfoRowElementPort.prototype = /** @type {!_OfferExchangeInfoRowElementPort} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeInfoRowElementPort} */ function OfferExchangeInfoRowElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    networkFeeWrOpts: {},
    networkFeeLaOpts: {},
    partnerFeeWrOpts: {},
    partnerFeeLaOpts: {},
    inImWrOpts: {},
    outImWrOpts: {},
    cryptoInOpts: {},
    cryptoOutOpts: {},
    amountInOpts: {},
    amountOutOpts: {},
    breakdownBuOpts: {},
    reloadGetOfferBuOpts: {},
    amountOutLoInOpts: {},
    getOfferErrorWrOpts: {},
    rateWrOpts: {},
    rateLaOpts: {},
    getOfferErrorLaOpts: {},
    estimatedTildeOutOpts: {},
    estimatedFixedTildeOutOpts: {},
    dealBrokerOpts: {},
    exchangeIntentOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort}
 */
class _OfferExchangeInfoRowElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort} ‎
 */
class OfferExchangeInfoRowElementPort extends newAbstract(
 _OfferExchangeInfoRowElementPort,871188356514,OfferExchangeInfoRowElementPortConstructor,{
  asIOfferExchangeInfoRowElementPort:1,
  superOfferExchangeInfoRowElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort} */
OfferExchangeInfoRowElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElementPort} */
function OfferExchangeInfoRowElementPortClass(){}

export default OfferExchangeInfoRowElementPort


OfferExchangeInfoRowElementPort[$implementations]=[
 __OfferExchangeInfoRowElementPort,
 OfferExchangeInfoRowElementPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'network-fee-wr-opts':undefined,
    'network-fee-la-opts':undefined,
    'partner-fee-wr-opts':undefined,
    'partner-fee-la-opts':undefined,
    'in-im-wr-opts':undefined,
    'out-im-wr-opts':undefined,
    'crypto-in-opts':undefined,
    'crypto-out-opts':undefined,
    'amount-in-opts':undefined,
    'amount-out-opts':undefined,
    'breakdown-bu-opts':undefined,
    'reload-get-offer-bu-opts':undefined,
    'amount-out-lo-in-opts':undefined,
    'get-offer-error-wr-opts':undefined,
    'rate-wr-opts':undefined,
    'rate-la-opts':undefined,
    'get-offer-error-la-opts':undefined,
    'estimated-tilde-out-opts':undefined,
    'estimated-fixed-tilde-out-opts':undefined,
    'deal-broker-opts':undefined,
    'exchange-intent-opts':undefined,
   })
  },
 }),
]