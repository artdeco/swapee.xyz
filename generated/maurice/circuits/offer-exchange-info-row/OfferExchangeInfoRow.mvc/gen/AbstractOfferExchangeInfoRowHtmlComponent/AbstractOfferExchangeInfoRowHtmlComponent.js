import AbstractOfferExchangeInfoRowGPU from '../AbstractOfferExchangeInfoRowGPU'
import AbstractOfferExchangeInfoRowScreenBack from '../AbstractOfferExchangeInfoRowScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {OfferExchangeInfoRowInputsQPs} from '../../pqs/OfferExchangeInfoRowInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractOfferExchangeInfoRow from '../AbstractOfferExchangeInfoRow'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowHtmlComponent}
 */
function __AbstractOfferExchangeInfoRowHtmlComponent() {}
__AbstractOfferExchangeInfoRowHtmlComponent.prototype = /** @type {!_AbstractOfferExchangeInfoRowHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent}
 */
class _AbstractOfferExchangeInfoRowHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.OfferExchangeInfoRowHtmlComponent} */ (res)
  }
}
/**
 * The _IOfferExchangeInfoRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent} ‎
 */
export class AbstractOfferExchangeInfoRowHtmlComponent extends newAbstract(
 _AbstractOfferExchangeInfoRowHtmlComponent,871188356512,null,{
  asIOfferExchangeInfoRowHtmlComponent:1,
  superOfferExchangeInfoRowHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent} */
AbstractOfferExchangeInfoRowHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent} */
function AbstractOfferExchangeInfoRowHtmlComponentClass(){}


AbstractOfferExchangeInfoRowHtmlComponent[$implementations]=[
 __AbstractOfferExchangeInfoRowHtmlComponent,
 HtmlComponent,
 AbstractOfferExchangeInfoRow,
 AbstractOfferExchangeInfoRowGPU,
 AbstractOfferExchangeInfoRowScreenBack,
 Landed,
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  constructor(){
   this.land={
    DealBroker:null,
    ExchangeIntent:null,
   }
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  inputsQPs:OfferExchangeInfoRowInputsQPs,
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $show_AmountOut(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'c63f7':estimatedAmountTo,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{AmountOut:AmountOut},
    asIBrowserView:{style:style},
   }=this
   style(AmountOut,'opacity',estimatedAmountTo?null:0,true)
   style(AmountOut,'pointer-events',estimatedAmountTo?null:'none',true)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $conceal_AmountOut(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '341da':gettingOffer,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{AmountOut:AmountOut},
    asIBrowserView:{conceal:conceal},
   }=this
   conceal(AmountOut,gettingOffer)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_AmountOut_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    'c63f7':estimatedAmountTo,
   }=DealBroker.model
   const{asIOfferExchangeInfoRowGPU:{AmountOut:AmountOut}}=this
   AmountOut.setText(estimatedAmountTo||0)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_GetOfferErrorWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '5f4a3':getOfferError,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{GetOfferErrorWr:GetOfferErrorWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(GetOfferErrorWr,getOfferError)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function interrupt_click_on_GetOfferErrorWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   const{asIOfferExchangeInfoRowGPU:{GetOfferErrorWr:GetOfferErrorWr}}=this
   GetOfferErrorWr.listen('click',(ev)=>{
    DealBroker['e0519']()
   })
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_GetOfferErrorLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '5f4a3':getOfferError,
   }=DealBroker.model
   const{asIOfferExchangeInfoRowGPU:{GetOfferErrorLa:GetOfferErrorLa}}=this
   GetOfferErrorLa.setText(getOfferError)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_AmountOutLoIn(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '341da':gettingOffer,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{AmountOutLoIn:AmountOutLoIn},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(AmountOutLoIn,gettingOffer)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function interrupt_click_on_ReloadGetOfferBu(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   const{asIOfferExchangeInfoRowGPU:{ReloadGetOfferBu:ReloadGetOfferBu}}=this
   ReloadGetOfferBu.listen('click',(ev)=>{
    DealBroker['e0519']()
   })
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_EstimatedTildeOut(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '341da':gettingOffer,
    '8ca05':estimatedFloatAmountTo,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{EstimatedTildeOut:EstimatedTildeOut},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={gettingOffer:gettingOffer,estimatedFloatAmountTo:estimatedFloatAmountTo}
   ;({gettingOffer:gettingOffer,estimatedFloatAmountTo:estimatedFloatAmountTo}=state)
   if(gettingOffer) conceal(EstimatedTildeOut,true)
   else reveal(EstimatedTildeOut,estimatedFloatAmountTo)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_ifNot_$conceal_EstimatedFixedTildeOut(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '341da':gettingOffer,
    'ea0a0':estimatedFixedAmountTo,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{EstimatedFixedTildeOut:EstimatedFixedTildeOut},
    asIBrowserView:{conceal:conceal,reveal:reveal},
   }=this
   const state={gettingOffer:gettingOffer,estimatedFixedAmountTo:estimatedFixedAmountTo}
   ;({gettingOffer:gettingOffer,estimatedFixedAmountTo:estimatedFixedAmountTo}=state)
   if(gettingOffer) conceal(EstimatedFixedTildeOut,true)
   else reveal(EstimatedFixedTildeOut,estimatedFixedAmountTo)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_NetworkFeeWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '5fd9c':networkFee,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{NetworkFeeWr:NetworkFeeWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(NetworkFeeWr,networkFee)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_NetworkFeeLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '5fd9c':networkFee,
   }=DealBroker.model
   const{asIOfferExchangeInfoRowGPU:{NetworkFeeLa:NetworkFeeLa}}=this
   NetworkFeeLa.setText(networkFee)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_RateWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '67942':rate,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{RateWr:RateWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(RateWr,rate)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_RateLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '67942':rate,
   }=DealBroker.model
   const{asIOfferExchangeInfoRowGPU:{RateLa:RateLa}}=this
   RateLa.setText(rate)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_PartnerFeeWr(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '96f44':partnerFee,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{PartnerFeeWr:PartnerFeeWr},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(PartnerFeeWr,partnerFee)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_PartnerFeeLa_Content(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '96f44':partnerFee,
   }=DealBroker.model
   const{asIOfferExchangeInfoRowGPU:{PartnerFeeLa:PartnerFeeLa}}=this
   PartnerFeeLa.setText(partnerFee)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $reveal_BreakdownBu(_,{DealBroker:DealBroker}){
   if(!DealBroker) return
   let{
    '5fd9c':networkFee,
   }=DealBroker.model
   const{
    asIOfferExchangeInfoRowGPU:{BreakdownBu:BreakdownBu},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(BreakdownBu,networkFee)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $show_CryptoIn(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    '96c88':currencyFrom,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeInfoRowGPU:{CryptoIns:CryptoIns},
    asIBrowserView:{style:style},
   }=this
   style(CryptoIns,'opacity',(ready&&currencyFrom)?null:0,true)
   style(CryptoIns,'pointer-events',(ready&&currencyFrom)?null:'none',true)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_CryptoIns_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '96c88':currencyFrom,
   }=ExchangeIntent.model
   const{asIOfferExchangeInfoRowGPU:{CryptoIns:CryptoIns}}=this
   for(const CryptoIn of CryptoIns){
    CryptoIn.setText(currencyFrom)
   }
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $show_CryptoOut(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    'c23cd':currencyTo,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeInfoRowGPU:{CryptoOuts:CryptoOuts},
    asIBrowserView:{style:style},
   }=this
   style(CryptoOuts,'opacity',(ready&&currencyTo)?null:0,true)
   style(CryptoOuts,'pointer-events',(ready&&currencyTo)?null:'none',true)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_CryptoOuts_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'c23cd':currencyTo,
   }=ExchangeIntent.model
   const{asIOfferExchangeInfoRowGPU:{CryptoOuts:CryptoOuts}}=this
   for(const CryptoOut of CryptoOuts){
    CryptoOut.setText(currencyTo)
   }
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function $show_AmountIn(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    'b2fda':ready,
    '748e6':amountFrom,
   }=ExchangeIntent.model
   const{
    asIOfferExchangeInfoRowGPU:{AmountIn:AmountIn},
    asIBrowserView:{style:style},
   }=this
   style(AmountIn,'opacity',(ready&&amountFrom)?null:0,true)
   style(AmountIn,'pointer-events',(ready&&amountFrom)?null:'none',true)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  paint:function paint_AmountIn_Content(_,{ExchangeIntent:ExchangeIntent}){
   if(!ExchangeIntent) return
   let{
    '748e6':amountFrom,
   }=ExchangeIntent.model
   const{asIOfferExchangeInfoRowGPU:{AmountIn:AmountIn}}=this
   AmountIn.setText(amountFrom||0)
  },
 }),
 AbstractOfferExchangeInfoRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIOfferExchangeInfoRowGPU:{
     DealBroker:DealBroker,
     ExchangeIntent:ExchangeIntent,
    },
   }=this
   complete(3427226314,{DealBroker:DealBroker})
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
  },
 }),
]