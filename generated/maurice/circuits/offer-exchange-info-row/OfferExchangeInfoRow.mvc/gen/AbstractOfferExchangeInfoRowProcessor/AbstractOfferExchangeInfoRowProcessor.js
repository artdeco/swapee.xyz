import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowProcessor}
 */
function __AbstractOfferExchangeInfoRowProcessor() {}
__AbstractOfferExchangeInfoRowProcessor.prototype = /** @type {!_AbstractOfferExchangeInfoRowProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor}
 */
class _AbstractOfferExchangeInfoRowProcessor { }
/**
 * The processor to compute changes to the memory for the _IOfferExchangeInfoRow_.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor} ‎
 */
class AbstractOfferExchangeInfoRowProcessor extends newAbstract(
 _AbstractOfferExchangeInfoRowProcessor,87118835658,null,{
  asIOfferExchangeInfoRowProcessor:1,
  superOfferExchangeInfoRowProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor} */
AbstractOfferExchangeInfoRowProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowProcessor} */
function AbstractOfferExchangeInfoRowProcessorClass(){}

export default AbstractOfferExchangeInfoRowProcessor


AbstractOfferExchangeInfoRowProcessor[$implementations]=[
 __AbstractOfferExchangeInfoRowProcessor,
]