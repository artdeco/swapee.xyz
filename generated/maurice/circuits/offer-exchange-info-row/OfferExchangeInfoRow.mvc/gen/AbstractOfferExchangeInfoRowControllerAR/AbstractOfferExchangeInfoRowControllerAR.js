import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowControllerAR}
 */
function __AbstractOfferExchangeInfoRowControllerAR() {}
__AbstractOfferExchangeInfoRowControllerAR.prototype = /** @type {!_AbstractOfferExchangeInfoRowControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR}
 */
class _AbstractOfferExchangeInfoRowControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IOfferExchangeInfoRowControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR} ‎
 */
class AbstractOfferExchangeInfoRowControllerAR extends newAbstract(
 _AbstractOfferExchangeInfoRowControllerAR,871188356524,null,{
  asIOfferExchangeInfoRowControllerAR:1,
  superOfferExchangeInfoRowControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR} */
AbstractOfferExchangeInfoRowControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractOfferExchangeInfoRowControllerAR} */
function AbstractOfferExchangeInfoRowControllerARClass(){}

export default AbstractOfferExchangeInfoRowControllerAR


AbstractOfferExchangeInfoRowControllerAR[$implementations]=[
 __AbstractOfferExchangeInfoRowControllerAR,
 AR,
 AbstractOfferExchangeInfoRowControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IOfferExchangeInfoRowControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]