import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowComputer}
 */
function __AbstractOfferExchangeInfoRowComputer() {}
__AbstractOfferExchangeInfoRowComputer.prototype = /** @type {!_AbstractOfferExchangeInfoRowComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer}
 */
class _AbstractOfferExchangeInfoRowComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} ‎
 */
export class AbstractOfferExchangeInfoRowComputer extends newAbstract(
 _AbstractOfferExchangeInfoRowComputer,87118835651,null,{
  asIOfferExchangeInfoRowComputer:1,
  superOfferExchangeInfoRowComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} */
AbstractOfferExchangeInfoRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} */
function AbstractOfferExchangeInfoRowComputerClass(){}


AbstractOfferExchangeInfoRowComputer[$implementations]=[
 __AbstractOfferExchangeInfoRowComputer,
 Adapter,
]


export default AbstractOfferExchangeInfoRowComputer