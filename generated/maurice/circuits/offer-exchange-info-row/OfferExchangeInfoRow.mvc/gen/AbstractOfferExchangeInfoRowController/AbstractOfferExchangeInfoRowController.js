import OfferExchangeInfoRowBuffer from '../OfferExchangeInfoRowBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {OfferExchangeInfoRowPortConnector} from '../OfferExchangeInfoRowPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowController}
 */
function __AbstractOfferExchangeInfoRowController() {}
__AbstractOfferExchangeInfoRowController.prototype = /** @type {!_AbstractOfferExchangeInfoRowController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController}
 */
class _AbstractOfferExchangeInfoRowController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController} ‎
 */
export class AbstractOfferExchangeInfoRowController extends newAbstract(
 _AbstractOfferExchangeInfoRowController,871188356521,null,{
  asIOfferExchangeInfoRowController:1,
  superOfferExchangeInfoRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowController} */
AbstractOfferExchangeInfoRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowController} */
function AbstractOfferExchangeInfoRowControllerClass(){}


AbstractOfferExchangeInfoRowController[$implementations]=[
 AbstractOfferExchangeInfoRowControllerClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IOfferExchangeInfoRowPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractOfferExchangeInfoRowController,
 OfferExchangeInfoRowBuffer,
 IntegratedController,
 /**@type {!AbstractOfferExchangeInfoRowController}*/(OfferExchangeInfoRowPortConnector),
]


export default AbstractOfferExchangeInfoRowController