import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowControllerAT}
 */
function __AbstractOfferExchangeInfoRowControllerAT() {}
__AbstractOfferExchangeInfoRowControllerAT.prototype = /** @type {!_AbstractOfferExchangeInfoRowControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT}
 */
class _AbstractOfferExchangeInfoRowControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IOfferExchangeInfoRowControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT} ‎
 */
class AbstractOfferExchangeInfoRowControllerAT extends newAbstract(
 _AbstractOfferExchangeInfoRowControllerAT,871188356525,null,{
  asIOfferExchangeInfoRowControllerAT:1,
  superOfferExchangeInfoRowControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT} */
AbstractOfferExchangeInfoRowControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractOfferExchangeInfoRowControllerAT} */
function AbstractOfferExchangeInfoRowControllerATClass(){}

export default AbstractOfferExchangeInfoRowControllerAT


AbstractOfferExchangeInfoRowControllerAT[$implementations]=[
 __AbstractOfferExchangeInfoRowControllerAT,
 UartUniversal,
 AbstractOfferExchangeInfoRowControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IOfferExchangeInfoRowControllerAT}*/({
  get asIOfferExchangeInfoRowController(){
   return this
  },
 }),
]