import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@type.engineering/seers'
import {OfferExchangeInfoRowInputsPQs} from '../../pqs/OfferExchangeInfoRowInputsPQs'
import {OfferExchangeInfoRowOuterCoreConstructor} from '../OfferExchangeInfoRowCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeInfoRowPort}
 */
function __OfferExchangeInfoRowPort() {}
__OfferExchangeInfoRowPort.prototype = /** @type {!_OfferExchangeInfoRowPort} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeInfoRowPort} */ function OfferExchangeInfoRowPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.OfferExchangeInfoRowOuterCore} */ ({model:null})
  OfferExchangeInfoRowOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowPort}
 */
class _OfferExchangeInfoRowPort { }
/**
 * The port that serves as an interface to the _IOfferExchangeInfoRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowPort} ‎
 */
export class OfferExchangeInfoRowPort extends newAbstract(
 _OfferExchangeInfoRowPort,87118835655,OfferExchangeInfoRowPortConstructor,{
  asIOfferExchangeInfoRowPort:1,
  superOfferExchangeInfoRowPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowPort} */
OfferExchangeInfoRowPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowPort} */
function OfferExchangeInfoRowPortClass(){}

export const OfferExchangeInfoRowPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IOfferExchangeInfoRow.Pinout>}*/({
 get Port() { return OfferExchangeInfoRowPort },
})

OfferExchangeInfoRowPort[$implementations]=[
 OfferExchangeInfoRowPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowPort}*/({
  resetPort(){
   this.resetOfferExchangeInfoRowPort()
  },
  resetOfferExchangeInfoRowPort(){
   OfferExchangeInfoRowPortConstructor.call(this)
  },
 }),
 __OfferExchangeInfoRowPort,
 Parametric,
 OfferExchangeInfoRowPortClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowPort}*/({
  constructor(){
   mountPins(this.inputs,OfferExchangeInfoRowInputsPQs)
  },
 }),
]


export default OfferExchangeInfoRowPort