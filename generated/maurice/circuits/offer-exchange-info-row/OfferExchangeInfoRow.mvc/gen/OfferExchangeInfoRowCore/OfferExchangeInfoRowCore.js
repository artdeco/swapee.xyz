import {mountPins} from '@type.engineering/seers'
import {OfferExchangeInfoRowMemoryPQs} from '../../pqs/OfferExchangeInfoRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeInfoRowCore}
 */
function __OfferExchangeInfoRowCore() {}
__OfferExchangeInfoRowCore.prototype = /** @type {!_OfferExchangeInfoRowCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowCore}
 */
class _OfferExchangeInfoRowCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowCore} ‎
 */
class OfferExchangeInfoRowCore extends newAbstract(
 _OfferExchangeInfoRowCore,87118835657,null,{
  asIOfferExchangeInfoRowCore:1,
  superOfferExchangeInfoRowCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowCore} */
OfferExchangeInfoRowCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowCore} */
function OfferExchangeInfoRowCoreClass(){}

export default OfferExchangeInfoRowCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_OfferExchangeInfoRowOuterCore}
 */
function __OfferExchangeInfoRowOuterCore() {}
__OfferExchangeInfoRowOuterCore.prototype = /** @type {!_OfferExchangeInfoRowOuterCore} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeInfoRowOuterCore} */
export function OfferExchangeInfoRowOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore.Model}*/
  this.model={
    core: '',
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore}
 */
class _OfferExchangeInfoRowOuterCore { }
/**
 * The _IOfferExchangeInfoRow_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore} ‎
 */
export class OfferExchangeInfoRowOuterCore extends newAbstract(
 _OfferExchangeInfoRowOuterCore,87118835653,OfferExchangeInfoRowOuterCoreConstructor,{
  asIOfferExchangeInfoRowOuterCore:1,
  superOfferExchangeInfoRowOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore} */
OfferExchangeInfoRowOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowOuterCore} */
function OfferExchangeInfoRowOuterCoreClass(){}


OfferExchangeInfoRowOuterCore[$implementations]=[
 __OfferExchangeInfoRowOuterCore,
 OfferExchangeInfoRowOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore}*/({
  constructor(){
   mountPins(this.model,OfferExchangeInfoRowMemoryPQs)

  },
 }),
]

OfferExchangeInfoRowCore[$implementations]=[
 OfferExchangeInfoRowCoreClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowCore}*/({
  resetCore(){
   this.resetOfferExchangeInfoRowCore()
  },
  resetOfferExchangeInfoRowCore(){
   OfferExchangeInfoRowOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.OfferExchangeInfoRowOuterCore}*/(
     /**@type {!xyz.swapee.wc.IOfferExchangeInfoRowOuterCore}*/(this)),
   )
  },
 }),
 __OfferExchangeInfoRowCore,
 OfferExchangeInfoRowOuterCore,
]

export {OfferExchangeInfoRowCore}