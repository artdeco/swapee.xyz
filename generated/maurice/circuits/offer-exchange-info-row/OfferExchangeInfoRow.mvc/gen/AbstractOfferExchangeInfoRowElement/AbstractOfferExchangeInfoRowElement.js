import OfferExchangeInfoRowRenderVdus from './methods/render-vdus'
import OfferExchangeInfoRowElementPort from '../OfferExchangeInfoRowElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {Landed} from '@webcircuits/webcircuits'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {OfferExchangeInfoRowInputsPQs} from '../../pqs/OfferExchangeInfoRowInputsPQs'
import {OfferExchangeInfoRowQueriesPQs} from '../../pqs/OfferExchangeInfoRowQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractOfferExchangeInfoRow from '../AbstractOfferExchangeInfoRow'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowElement}
 */
function __AbstractOfferExchangeInfoRowElement() {}
__AbstractOfferExchangeInfoRowElement.prototype = /** @type {!_AbstractOfferExchangeInfoRowElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowElement}
 */
class _AbstractOfferExchangeInfoRowElement { }
/**
 * A component description.
 *
 * The _IOfferExchangeInfoRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowElement} ‎
 */
class AbstractOfferExchangeInfoRowElement extends newAbstract(
 _AbstractOfferExchangeInfoRowElement,871188356513,null,{
  asIOfferExchangeInfoRowElement:1,
  superOfferExchangeInfoRowElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElement} */
AbstractOfferExchangeInfoRowElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowElement} */
function AbstractOfferExchangeInfoRowElementClass(){}

export default AbstractOfferExchangeInfoRowElement


AbstractOfferExchangeInfoRowElement[$implementations]=[
 __AbstractOfferExchangeInfoRowElement,
 ElementBase,
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':core':coreColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'core':coreAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(coreAttr===undefined?{'core':coreColAttr}:{}),
   }
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'core':coreAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    core:coreAttr,
   }
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 Landed,
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  render:function renderDealBroker(){
   const{
    asILanded:{
     land:{
      DealBroker:DealBroker,
     },
    },
    asIOfferExchangeInfoRowElement:{
     buildDealBroker:buildDealBroker,
    },
   }=this
   if(!DealBroker) return
   const{model:DealBrokerModel}=DealBroker
   const{
    'c63f7':estimatedAmountTo,
    'ea0a0':estimatedFixedAmountTo,
    '8ca05':estimatedFloatAmountTo,
    '5f4a3':getOfferError,
    '341da':gettingOffer,
    '5fd9c':networkFee,
    '96f44':partnerFee,
    '67942':rate,
   }=DealBrokerModel
   const res=buildDealBroker({
    estimatedAmountTo:estimatedAmountTo,
    estimatedFixedAmountTo:estimatedFixedAmountTo,
    estimatedFloatAmountTo:estimatedFloatAmountTo,
    getOfferError:getOfferError,
    gettingOffer:gettingOffer,
    networkFee:networkFee,
    partnerFee:partnerFee,
    rate:rate,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  render:function renderExchangeIntent(){
   const{
    asILanded:{
     land:{
      ExchangeIntent:ExchangeIntent,
     },
    },
    asIOfferExchangeInfoRowElement:{
     buildExchangeIntent:buildExchangeIntent,
    },
   }=this
   if(!ExchangeIntent) return
   const{model:ExchangeIntentModel}=ExchangeIntent
   const{
    'c23cd':currencyTo,
    '96c88':currencyFrom,
    '748e6':amountFrom,
    'b2fda':ready,
   }=ExchangeIntentModel
   const res=buildExchangeIntent({
    currencyTo:currencyTo,
    currencyFrom:currencyFrom,
    amountFrom:amountFrom,
    ready:ready,
   },{})
   res.attributes['$id']=this.rootId
   res.nodeName='div'
   return res
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  constructor(){
   Object.assign(this,/**@type {xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({land:{
    DealBroker:null,
    ExchangeIntent:null,
   }}))
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  render:OfferExchangeInfoRowRenderVdus,
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  classes:{
   'CryptoAmount': '81b89',
  },
  inputsPQs:OfferExchangeInfoRowInputsPQs,
  queriesPQs:OfferExchangeInfoRowQueriesPQs,
  vdus:{
   'NetworkFeeWr': 'hd091',
   'NetworkFeeLa': 'hd092',
   'PartnerFeeWr': 'hd093',
   'PartnerFeeLa': 'hd094',
   'AmountIn': 'hd099',
   'AmountOut': 'hd0910',
   'ReloadGetOfferBu': 'hd0911',
   'AmountOutLoIn': 'hd0912',
   'GetOfferErrorWr': 'hd0913',
   'GetOfferErrorLa': 'hd0914',
   'EstimatedTildeOut': 'hd0915',
   'EstimatedFixedTildeOut': 'hd0916',
   'BreakdownBu': 'hd0920',
   'DealBroker': 'hd0921',
   'ExchangeIntent': 'hd0922',
   'RateLa': 'hd0925',
   'RateWr': 'hd0926',
   'InImWr': 'hd0927',
   'OutImWr': 'hd0928',
   'CryptoIn': 'hd0923',
   'CryptoOut': 'hd0924',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','core','query:deal-broker','query:exchange-intent','no-solder',':no-solder',':core','fe646','6c941','72e15','97166','ed9b2','4e37d','c61f1','3ce39','2de4e','b88f2','cd1af','54094','cdf62','365b9','bf2c1','82f48','e0e01','b47f4','c1be4','65c2c','c9e36','c3b6b','a74ad','children']),
   })
  },
  get Port(){
   return OfferExchangeInfoRowElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:deal-broker':dealBrokerSel,'query:exchange-intent':exchangeIntentSel}){
   const _ret={}
   if(dealBrokerSel) _ret.dealBrokerSel=dealBrokerSel
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   return _ret
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  constructor(){
   this.land={
    DealBroker:null,
    ExchangeIntent:null,
   }
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  calibrate:async function awaitOnDealBroker({dealBrokerSel:dealBrokerSel}){
   if(!dealBrokerSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const DealBroker=await milleu(dealBrokerSel,true)
   if(!DealBroker) {
    console.warn('❗️ dealBrokerSel %s must be present on the page for %s to work',dealBrokerSel,fqn)
    return{}
   }
   land.DealBroker=DealBroker
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel,true)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractOfferExchangeInfoRowElementClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowElement}*/({
  solder:(_,{
   dealBrokerSel:dealBrokerSel,
   exchangeIntentSel:exchangeIntentSel,
  })=>{
   return{
    dealBrokerSel:dealBrokerSel,
    exchangeIntentSel:exchangeIntentSel,
   }
  },
 }),
]



AbstractOfferExchangeInfoRowElement[$implementations]=[AbstractOfferExchangeInfoRow,
 /** @type {!AbstractOfferExchangeInfoRowElement} */ ({
  rootId:'OfferExchangeInfoRow',
  __$id:8711883565,
  fqn:'xyz.swapee.wc.IOfferExchangeInfoRow',
  maurice_element_v3:true,
 }),
]