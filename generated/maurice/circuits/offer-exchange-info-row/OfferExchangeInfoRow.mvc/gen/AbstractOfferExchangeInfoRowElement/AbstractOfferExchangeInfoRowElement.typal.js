
import AbstractOfferExchangeInfoRow from '../AbstractOfferExchangeInfoRow'

/** @abstract {xyz.swapee.wc.IOfferExchangeInfoRowElement} */
export default class AbstractOfferExchangeInfoRowElement { }



AbstractOfferExchangeInfoRowElement[$implementations]=[AbstractOfferExchangeInfoRow,
 /** @type {!AbstractOfferExchangeInfoRowElement} */ ({
  rootId:'OfferExchangeInfoRow',
  __$id:8711883565,
  fqn:'xyz.swapee.wc.IOfferExchangeInfoRow',
  maurice_element_v3:true,
 }),
]