export default function OfferExchangeInfoRowRenderVdus(){
 return (<div $id="OfferExchangeInfoRow">
  <vdu $id="NetworkFeeWr" />
  <vdu $id="NetworkFeeLa" />
  <vdu $id="PartnerFeeWr" />
  <vdu $id="PartnerFeeLa" />
  <vdu $id="InImWr" />
  <vdu $id="OutImWr" />
  <vdu $id="CryptoIn" />
  <vdu $id="CryptoOut" />
  <vdu $id="AmountIn" />
  <vdu $id="AmountOut" />
  <vdu $id="BreakdownBu" />
  <vdu $id="ReloadGetOfferBu" />
  <vdu $id="AmountOutLoIn" />
  <vdu $id="GetOfferErrorWr" />
  <vdu $id="RateWr" />
  <vdu $id="RateLa" />
  <vdu $id="GetOfferErrorLa" />
  <vdu $id="EstimatedTildeOut" />
  <vdu $id="EstimatedFixedTildeOut" />
 </div>)
}