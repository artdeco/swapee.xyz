import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowDisplay}
 */
function __AbstractOfferExchangeInfoRowDisplay() {}
__AbstractOfferExchangeInfoRowDisplay.prototype = /** @type {!_AbstractOfferExchangeInfoRowDisplay} */ ({ })
/** @this {xyz.swapee.wc.OfferExchangeInfoRowDisplay} */ function OfferExchangeInfoRowDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.NetworkFeeWr=null
  /** @type {HTMLSpanElement} */ this.NetworkFeeLa=null
  /** @type {HTMLSpanElement} */ this.PartnerFeeWr=null
  /** @type {HTMLSpanElement} */ this.PartnerFeeLa=null
  /** @type {HTMLElement} */ this.InImWr=null
  /** @type {HTMLElement} */ this.OutImWr=null
  /** @type {!Array<!HTMLSpanElement>} */ this.CryptoIns=[]
  /** @type {!Array<!HTMLSpanElement>} */ this.CryptoOuts=[]
  /** @type {HTMLSpanElement} */ this.AmountIn=null
  /** @type {HTMLSpanElement} */ this.AmountOut=null
  /** @type {HTMLButtonElement} */ this.BreakdownBu=null
  /** @type {HTMLButtonElement} */ this.ReloadGetOfferBu=null
  /** @type {HTMLSpanElement} */ this.AmountOutLoIn=null
  /** @type {HTMLSpanElement} */ this.GetOfferErrorWr=null
  /** @type {HTMLDivElement} */ this.RateWr=null
  /** @type {HTMLSpanElement} */ this.RateLa=null
  /** @type {HTMLSpanElement} */ this.GetOfferErrorLa=null
  /** @type {HTMLSpanElement} */ this.EstimatedTildeOut=null
  /** @type {HTMLSpanElement} */ this.EstimatedFixedTildeOut=null
  /** @type {HTMLElement} */ this.DealBroker=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay}
 */
class _AbstractOfferExchangeInfoRowDisplay { }
/**
 * Display for presenting information from the _IOfferExchangeInfoRow_.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay} ‎
 */
class AbstractOfferExchangeInfoRowDisplay extends newAbstract(
 _AbstractOfferExchangeInfoRowDisplay,871188356517,OfferExchangeInfoRowDisplayConstructor,{
  asIOfferExchangeInfoRowDisplay:1,
  superOfferExchangeInfoRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay} */
AbstractOfferExchangeInfoRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowDisplay} */
function AbstractOfferExchangeInfoRowDisplayClass(){}

export default AbstractOfferExchangeInfoRowDisplay


AbstractOfferExchangeInfoRowDisplay[$implementations]=[
 __AbstractOfferExchangeInfoRowDisplay,
 Display,
 AbstractOfferExchangeInfoRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{dealBrokerScopeSel:dealBrokerScopeSel,exchangeIntentScopeSel:exchangeIntentScopeSel}}=this
    this.scan({
     dealBrokerSel:dealBrokerScopeSel,
     exchangeIntentSel:exchangeIntentScopeSel,
    })
   })
  },
  scan:function vduScan({dealBrokerSel:dealBrokerSelScope,exchangeIntentSel:exchangeIntentSelScope}){
   const{element:element,asIOfferExchangeInfoRowScreen:{vdusPQs:{
    NetworkFeeWr:NetworkFeeWr,
    NetworkFeeLa:NetworkFeeLa,
    PartnerFeeWr:PartnerFeeWr,
    PartnerFeeLa:PartnerFeeLa,
    InImWr:InImWr,
    OutImWr:OutImWr,
    AmountIn:AmountIn,
    AmountOut:AmountOut,
    BreakdownBu:BreakdownBu,
    ReloadGetOfferBu:ReloadGetOfferBu,
    AmountOutLoIn:AmountOutLoIn,
    GetOfferErrorWr:GetOfferErrorWr,
    RateWr:RateWr,
    RateLa:RateLa,
    GetOfferErrorLa:GetOfferErrorLa,
    EstimatedTildeOut:EstimatedTildeOut,
    EstimatedFixedTildeOut:EstimatedFixedTildeOut,
   }},queries:{dealBrokerSel,exchangeIntentSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const DealBroker=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _DealBroker=dealBrokerSel?root.querySelector(dealBrokerSel):void 0
    return _DealBroker
   })(dealBrokerSelScope)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   Object.assign(this,{
    NetworkFeeWr:/**@type {HTMLSpanElement}*/(children[NetworkFeeWr]),
    NetworkFeeLa:/**@type {HTMLSpanElement}*/(children[NetworkFeeLa]),
    PartnerFeeWr:/**@type {HTMLSpanElement}*/(children[PartnerFeeWr]),
    PartnerFeeLa:/**@type {HTMLSpanElement}*/(children[PartnerFeeLa]),
    InImWr:/**@type {HTMLElement}*/(children[InImWr]),
    OutImWr:/**@type {HTMLElement}*/(children[OutImWr]),
    CryptoIns:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=hd0923]')]):[],
    CryptoOuts:element?/**@type {!Array<!HTMLSpanElement>}*/([...element.querySelectorAll('[data-id~=hd0924]')]):[],
    AmountIn:/**@type {HTMLSpanElement}*/(children[AmountIn]),
    AmountOut:/**@type {HTMLSpanElement}*/(children[AmountOut]),
    BreakdownBu:/**@type {HTMLButtonElement}*/(children[BreakdownBu]),
    ReloadGetOfferBu:/**@type {HTMLButtonElement}*/(children[ReloadGetOfferBu]),
    AmountOutLoIn:/**@type {HTMLSpanElement}*/(children[AmountOutLoIn]),
    GetOfferErrorWr:/**@type {HTMLSpanElement}*/(children[GetOfferErrorWr]),
    RateWr:/**@type {HTMLDivElement}*/(children[RateWr]),
    RateLa:/**@type {HTMLSpanElement}*/(children[RateLa]),
    GetOfferErrorLa:/**@type {HTMLSpanElement}*/(children[GetOfferErrorLa]),
    EstimatedTildeOut:/**@type {HTMLSpanElement}*/(children[EstimatedTildeOut]),
    EstimatedFixedTildeOut:/**@type {HTMLSpanElement}*/(children[EstimatedFixedTildeOut]),
    DealBroker:DealBroker,
    ExchangeIntent:ExchangeIntent,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.OfferExchangeInfoRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowDisplay.Initialese}*/({
   NetworkFeeWr:1,
   NetworkFeeLa:1,
   PartnerFeeWr:1,
   PartnerFeeLa:1,
   InImWr:1,
   OutImWr:1,
   CryptoIns:1,
   CryptoOuts:1,
   AmountIn:1,
   AmountOut:1,
   BreakdownBu:1,
   ReloadGetOfferBu:1,
   AmountOutLoIn:1,
   GetOfferErrorWr:1,
   RateWr:1,
   RateLa:1,
   GetOfferErrorLa:1,
   EstimatedTildeOut:1,
   EstimatedFixedTildeOut:1,
   DealBroker:1,
   ExchangeIntent:1,
  }),
  initializer({
   NetworkFeeWr:_NetworkFeeWr,
   NetworkFeeLa:_NetworkFeeLa,
   PartnerFeeWr:_PartnerFeeWr,
   PartnerFeeLa:_PartnerFeeLa,
   InImWr:_InImWr,
   OutImWr:_OutImWr,
   CryptoIns:_CryptoIns,
   CryptoOuts:_CryptoOuts,
   AmountIn:_AmountIn,
   AmountOut:_AmountOut,
   BreakdownBu:_BreakdownBu,
   ReloadGetOfferBu:_ReloadGetOfferBu,
   AmountOutLoIn:_AmountOutLoIn,
   GetOfferErrorWr:_GetOfferErrorWr,
   RateWr:_RateWr,
   RateLa:_RateLa,
   GetOfferErrorLa:_GetOfferErrorLa,
   EstimatedTildeOut:_EstimatedTildeOut,
   EstimatedFixedTildeOut:_EstimatedFixedTildeOut,
   DealBroker:_DealBroker,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_NetworkFeeWr!==undefined) this.NetworkFeeWr=_NetworkFeeWr
   if(_NetworkFeeLa!==undefined) this.NetworkFeeLa=_NetworkFeeLa
   if(_PartnerFeeWr!==undefined) this.PartnerFeeWr=_PartnerFeeWr
   if(_PartnerFeeLa!==undefined) this.PartnerFeeLa=_PartnerFeeLa
   if(_InImWr!==undefined) this.InImWr=_InImWr
   if(_OutImWr!==undefined) this.OutImWr=_OutImWr
   if(_CryptoIns!==undefined) this.CryptoIns=_CryptoIns
   if(_CryptoOuts!==undefined) this.CryptoOuts=_CryptoOuts
   if(_AmountIn!==undefined) this.AmountIn=_AmountIn
   if(_AmountOut!==undefined) this.AmountOut=_AmountOut
   if(_BreakdownBu!==undefined) this.BreakdownBu=_BreakdownBu
   if(_ReloadGetOfferBu!==undefined) this.ReloadGetOfferBu=_ReloadGetOfferBu
   if(_AmountOutLoIn!==undefined) this.AmountOutLoIn=_AmountOutLoIn
   if(_GetOfferErrorWr!==undefined) this.GetOfferErrorWr=_GetOfferErrorWr
   if(_RateWr!==undefined) this.RateWr=_RateWr
   if(_RateLa!==undefined) this.RateLa=_RateLa
   if(_GetOfferErrorLa!==undefined) this.GetOfferErrorLa=_GetOfferErrorLa
   if(_EstimatedTildeOut!==undefined) this.EstimatedTildeOut=_EstimatedTildeOut
   if(_EstimatedFixedTildeOut!==undefined) this.EstimatedFixedTildeOut=_EstimatedFixedTildeOut
   if(_DealBroker!==undefined) this.DealBroker=_DealBroker
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]