import OfferExchangeInfoRowClassesPQs from '../../pqs/OfferExchangeInfoRowClassesPQs'
import AbstractOfferExchangeInfoRowScreenAR from '../AbstractOfferExchangeInfoRowScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {OfferExchangeInfoRowInputsPQs} from '../../pqs/OfferExchangeInfoRowInputsPQs'
import {OfferExchangeInfoRowQueriesPQs} from '../../pqs/OfferExchangeInfoRowQueriesPQs'
import {OfferExchangeInfoRowMemoryQPs} from '../../pqs/OfferExchangeInfoRowMemoryQPs'
import {OfferExchangeInfoRowVdusPQs} from '../../pqs/OfferExchangeInfoRowVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowScreen}
 */
function __AbstractOfferExchangeInfoRowScreen() {}
__AbstractOfferExchangeInfoRowScreen.prototype = /** @type {!_AbstractOfferExchangeInfoRowScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen}
 */
class _AbstractOfferExchangeInfoRowScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen} ‎
 */
class AbstractOfferExchangeInfoRowScreen extends newAbstract(
 _AbstractOfferExchangeInfoRowScreen,871188356526,null,{
  asIOfferExchangeInfoRowScreen:1,
  superOfferExchangeInfoRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen} */
AbstractOfferExchangeInfoRowScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowScreen} */
function AbstractOfferExchangeInfoRowScreenClass(){}

export default AbstractOfferExchangeInfoRowScreen


AbstractOfferExchangeInfoRowScreen[$implementations]=[
 __AbstractOfferExchangeInfoRowScreen,
 AbstractOfferExchangeInfoRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowScreen}*/({
  inputsPQs:OfferExchangeInfoRowInputsPQs,
  classesPQs:OfferExchangeInfoRowClassesPQs,
  queriesPQs:OfferExchangeInfoRowQueriesPQs,
  memoryQPs:OfferExchangeInfoRowMemoryQPs,
 }),
 Screen,
 AbstractOfferExchangeInfoRowScreenAR,
 AbstractOfferExchangeInfoRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowScreen}*/({
  vdusPQs:OfferExchangeInfoRowVdusPQs,
 }),
]