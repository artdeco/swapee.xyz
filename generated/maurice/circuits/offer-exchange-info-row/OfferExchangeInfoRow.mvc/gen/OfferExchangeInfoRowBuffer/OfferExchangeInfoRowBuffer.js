import {makeBuffers} from '@webcircuits/webcircuits'

export const OfferExchangeInfoRowBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  core:String,
 }),
})

export default OfferExchangeInfoRowBuffer