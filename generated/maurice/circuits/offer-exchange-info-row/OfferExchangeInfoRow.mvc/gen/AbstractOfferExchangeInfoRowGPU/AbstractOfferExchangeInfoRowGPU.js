import AbstractOfferExchangeInfoRowDisplay from '../AbstractOfferExchangeInfoRowDisplayBack'
import OfferExchangeInfoRowClassesPQs from '../../pqs/OfferExchangeInfoRowClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {OfferExchangeInfoRowClassesQPs} from '../../pqs/OfferExchangeInfoRowClassesQPs'
import {OfferExchangeInfoRowVdusPQs} from '../../pqs/OfferExchangeInfoRowVdusPQs'
import {OfferExchangeInfoRowVdusQPs} from '../../pqs/OfferExchangeInfoRowVdusQPs'
import {OfferExchangeInfoRowMemoryPQs} from '../../pqs/OfferExchangeInfoRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractOfferExchangeInfoRowGPU}
 */
function __AbstractOfferExchangeInfoRowGPU() {}
__AbstractOfferExchangeInfoRowGPU.prototype = /** @type {!_AbstractOfferExchangeInfoRowGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU}
 */
class _AbstractOfferExchangeInfoRowGPU { }
/**
 * Handles the periphery of the _IOfferExchangeInfoRowDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU} ‎
 */
class AbstractOfferExchangeInfoRowGPU extends newAbstract(
 _AbstractOfferExchangeInfoRowGPU,871188356516,null,{
  asIOfferExchangeInfoRowGPU:1,
  superOfferExchangeInfoRowGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU} */
AbstractOfferExchangeInfoRowGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractOfferExchangeInfoRowGPU} */
function AbstractOfferExchangeInfoRowGPUClass(){}

export default AbstractOfferExchangeInfoRowGPU


AbstractOfferExchangeInfoRowGPU[$implementations]=[
 __AbstractOfferExchangeInfoRowGPU,
 AbstractOfferExchangeInfoRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowGPU}*/({
  classesQPs:OfferExchangeInfoRowClassesQPs,
  vdusPQs:OfferExchangeInfoRowVdusPQs,
  vdusQPs:OfferExchangeInfoRowVdusQPs,
  memoryPQs:OfferExchangeInfoRowMemoryPQs,
 }),
 AbstractOfferExchangeInfoRowDisplay,
 BrowserView,
 AbstractOfferExchangeInfoRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IOfferExchangeInfoRowGPU}*/({
  allocator(){
   pressFit(this.classes,'',OfferExchangeInfoRowClassesPQs)
  },
 }),
]