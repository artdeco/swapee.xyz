import {OfferExchangeInfoRowMemoryPQs} from './OfferExchangeInfoRowMemoryPQs'
export const OfferExchangeInfoRowMemoryQPs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowMemoryQPs}*/(Object.keys(OfferExchangeInfoRowMemoryPQs)
 .reduce((a,k)=>{a[OfferExchangeInfoRowMemoryPQs[k]]=k;return a},{}))