import {OfferExchangeInfoRowVdusPQs} from './OfferExchangeInfoRowVdusPQs'
export const OfferExchangeInfoRowVdusQPs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowVdusQPs}*/(Object.keys(OfferExchangeInfoRowVdusPQs)
 .reduce((a,k)=>{a[OfferExchangeInfoRowVdusPQs[k]]=k;return a},{}))