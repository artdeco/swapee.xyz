import {OfferExchangeInfoRowMemoryPQs} from './OfferExchangeInfoRowMemoryPQs'
export const OfferExchangeInfoRowInputsPQs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowInputsQPs}*/({
 ...OfferExchangeInfoRowMemoryPQs,
})