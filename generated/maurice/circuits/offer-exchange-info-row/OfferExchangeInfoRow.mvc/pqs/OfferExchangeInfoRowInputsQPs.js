import {OfferExchangeInfoRowInputsPQs} from './OfferExchangeInfoRowInputsPQs'
export const OfferExchangeInfoRowInputsQPs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowInputsQPs}*/(Object.keys(OfferExchangeInfoRowInputsPQs)
 .reduce((a,k)=>{a[OfferExchangeInfoRowInputsPQs[k]]=k;return a},{}))