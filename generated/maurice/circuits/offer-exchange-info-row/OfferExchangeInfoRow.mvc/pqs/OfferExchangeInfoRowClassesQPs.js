import OfferExchangeInfoRowClassesPQs from './OfferExchangeInfoRowClassesPQs'
export const OfferExchangeInfoRowClassesQPs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowClassesQPs}*/(Object.keys(OfferExchangeInfoRowClassesPQs)
 .reduce((a,k)=>{a[OfferExchangeInfoRowClassesPQs[k]]=k;return a},{}))