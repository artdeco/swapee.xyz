import {OfferExchangeInfoRowQueriesPQs} from './OfferExchangeInfoRowQueriesPQs'
export const OfferExchangeInfoRowQueriesQPs=/**@type {!xyz.swapee.wc.OfferExchangeInfoRowQueriesQPs}*/(Object.keys(OfferExchangeInfoRowQueriesPQs)
 .reduce((a,k)=>{a[OfferExchangeInfoRowQueriesPQs[k]]=k;return a},{}))