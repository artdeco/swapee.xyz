/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRow} */
export default class AbstractOfferExchangeInfoRow extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowComputer} */
export class AbstractOfferExchangeInfoRowComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowController} */
export class AbstractOfferExchangeInfoRowController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowPort} */
export class OfferExchangeInfoRowPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowView} */
export class AbstractOfferExchangeInfoRowView extends (<view>
  <classes>
   <string opt name="CryptoAmount" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowElement} */
export class AbstractOfferExchangeInfoRowElement extends (<element v3 html mv>
 <block src="./OfferExchangeInfoRow.mvc/src/OfferExchangeInfoRowElement/methods/render.jsx" />
 <inducer src="./OfferExchangeInfoRow.mvc/src/OfferExchangeInfoRowElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractOfferExchangeInfoRowHtmlComponent} */
export class AbstractOfferExchangeInfoRowHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IDealBroker via="DealBroker" />
   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
  </connectors>

</html-ic>) { }
// </class-end>