import AbstractHyperOfferExchangeInfoRowInterruptLine from '../../../../gen/AbstractOfferExchangeInfoRowInterruptLine/hyper/AbstractHyperOfferExchangeInfoRowInterruptLine'
import OfferExchangeInfoRowInterruptLine from '../../OfferExchangeInfoRowInterruptLine'
import OfferExchangeInfoRowInterruptLineGeneralAspects from '../OfferExchangeInfoRowInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperOfferExchangeInfoRowInterruptLine} */
export default class extends AbstractHyperOfferExchangeInfoRowInterruptLine
 .consults(
  OfferExchangeInfoRowInterruptLineGeneralAspects,
 )
 .implements(
  OfferExchangeInfoRowInterruptLine,
 )
{}