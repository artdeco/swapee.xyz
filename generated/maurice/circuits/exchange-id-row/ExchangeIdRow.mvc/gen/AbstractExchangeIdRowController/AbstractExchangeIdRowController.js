import ExchangeIdRowBuffer from '../ExchangeIdRowBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {ExchangeIdRowPortConnector} from '../ExchangeIdRowPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowController}
 */
function __AbstractExchangeIdRowController() {}
__AbstractExchangeIdRowController.prototype = /** @type {!_AbstractExchangeIdRowController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowController}
 */
class _AbstractExchangeIdRowController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowController} ‎
 */
export class AbstractExchangeIdRowController extends newAbstract(
 _AbstractExchangeIdRowController,109947522721,null,{
  asIExchangeIdRowController:1,
  superExchangeIdRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowController} */
AbstractExchangeIdRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowController} */
function AbstractExchangeIdRowControllerClass(){}


AbstractExchangeIdRowController[$implementations]=[
 AbstractExchangeIdRowControllerClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowController}*/({
  resetPort(){
   /**@type {!xyz.swapee.wc.IExchangeIdRowPort}*/
   (/**@type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractExchangeIdRowController,
 ExchangeIdRowBuffer,
 IntegratedController,
 /**@type {!AbstractExchangeIdRowController}*/(ExchangeIdRowPortConnector),
]


export default AbstractExchangeIdRowController