import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeIdRowInputsPQs} from '../../pqs/ExchangeIdRowInputsPQs'
import {ExchangeIdRowOuterCoreConstructor} from '../ExchangeIdRowCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIdRowPort}
 */
function __ExchangeIdRowPort() {}
__ExchangeIdRowPort.prototype = /** @type {!_ExchangeIdRowPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIdRowPort} */ function ExchangeIdRowPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.ExchangeIdRowOuterCore} */ ({model:null})
  ExchangeIdRowOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowPort}
 */
class _ExchangeIdRowPort { }
/**
 * The port that serves as an interface to the _IExchangeIdRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowPort} ‎
 */
export class ExchangeIdRowPort extends newAbstract(
 _ExchangeIdRowPort,10994752275,ExchangeIdRowPortConstructor,{
  asIExchangeIdRowPort:1,
  superExchangeIdRowPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowPort} */
ExchangeIdRowPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowPort} */
function ExchangeIdRowPortClass(){}

export const ExchangeIdRowPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IExchangeIdRow.Pinout>}*/({
 get Port() { return ExchangeIdRowPort },
})

ExchangeIdRowPort[$implementations]=[
 ExchangeIdRowPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowPort}*/({
  resetPort(){
   this.resetExchangeIdRowPort()
  },
  resetExchangeIdRowPort(){
   ExchangeIdRowPortConstructor.call(this)
  },
 }),
 __ExchangeIdRowPort,
 Parametric,
 ExchangeIdRowPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowPort}*/({
  constructor(){
   mountPins(this.inputs,'',ExchangeIdRowInputsPQs)
  },
 }),
]


export default ExchangeIdRowPort