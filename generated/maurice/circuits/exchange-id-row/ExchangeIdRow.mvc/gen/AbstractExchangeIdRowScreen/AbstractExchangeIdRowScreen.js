import ExchangeIdRowClassesPQs from '../../pqs/ExchangeIdRowClassesPQs'
import AbstractExchangeIdRowScreenAR from '../AbstractExchangeIdRowScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {ExchangeIdRowInputsPQs} from '../../pqs/ExchangeIdRowInputsPQs'
import {ExchangeIdRowQueriesPQs} from '../../pqs/ExchangeIdRowQueriesPQs'
import {ExchangeIdRowMemoryQPs} from '../../pqs/ExchangeIdRowMemoryQPs'
import {ExchangeIdRowVdusPQs} from '../../pqs/ExchangeIdRowVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowScreen}
 */
function __AbstractExchangeIdRowScreen() {}
__AbstractExchangeIdRowScreen.prototype = /** @type {!_AbstractExchangeIdRowScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowScreen}
 */
class _AbstractExchangeIdRowScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowScreen} ‎
 */
class AbstractExchangeIdRowScreen extends newAbstract(
 _AbstractExchangeIdRowScreen,109947522726,null,{
  asIExchangeIdRowScreen:1,
  superExchangeIdRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowScreen} */
AbstractExchangeIdRowScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowScreen} */
function AbstractExchangeIdRowScreenClass(){}

export default AbstractExchangeIdRowScreen


AbstractExchangeIdRowScreen[$implementations]=[
 __AbstractExchangeIdRowScreen,
 AbstractExchangeIdRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowScreen}*/({
  deduceInputs(){
   const{asIExchangeIdRowDisplay:{
    ExchangeIdLa:ExchangeIdLa,
   }}=this
   return{
    id:ExchangeIdLa?.innerText,
   }
  },
 }),
 AbstractExchangeIdRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowScreen}*/({
  inputsPQs:ExchangeIdRowInputsPQs,
  classesPQs:ExchangeIdRowClassesPQs,
  queriesPQs:ExchangeIdRowQueriesPQs,
  memoryQPs:ExchangeIdRowMemoryQPs,
 }),
 Screen,
 AbstractExchangeIdRowScreenAR,
 AbstractExchangeIdRowScreenClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowScreen}*/({
  vdusPQs:ExchangeIdRowVdusPQs,
 }),
]