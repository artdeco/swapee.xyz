import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIdRowElementPort}
 */
function __ExchangeIdRowElementPort() {}
__ExchangeIdRowElementPort.prototype = /** @type {!_ExchangeIdRowElementPort} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIdRowElementPort} */ function ExchangeIdRowElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeIdRowElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    exchangeIdLaOpts: {},
    exchangeIdFloatingLaOpts: {},
    exchangeIdFixedLaOpts: {},
    restartBuOpts: {},
    exchangeIntentOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowElementPort}
 */
class _ExchangeIdRowElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowElementPort} ‎
 */
class ExchangeIdRowElementPort extends newAbstract(
 _ExchangeIdRowElementPort,109947522714,ExchangeIdRowElementPortConstructor,{
  asIExchangeIdRowElementPort:1,
  superExchangeIdRowElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElementPort} */
ExchangeIdRowElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElementPort} */
function ExchangeIdRowElementPortClass(){}

export default ExchangeIdRowElementPort


ExchangeIdRowElementPort[$implementations]=[
 __ExchangeIdRowElementPort,
 ExchangeIdRowElementPortClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'exchange-id-la-opts':undefined,
    'exchange-id-floating-la-opts':undefined,
    'exchange-id-fixed-la-opts':undefined,
    'restart-bu-opts':undefined,
    'exchange-intent-opts':undefined,
   })
  },
 }),
]