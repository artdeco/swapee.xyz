import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowControllerAR}
 */
function __AbstractExchangeIdRowControllerAR() {}
__AbstractExchangeIdRowControllerAR.prototype = /** @type {!_AbstractExchangeIdRowControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR}
 */
class _AbstractExchangeIdRowControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIdRowControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR} ‎
 */
class AbstractExchangeIdRowControllerAR extends newAbstract(
 _AbstractExchangeIdRowControllerAR,109947522724,null,{
  asIExchangeIdRowControllerAR:1,
  superExchangeIdRowControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR} */
AbstractExchangeIdRowControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR} */
function AbstractExchangeIdRowControllerARClass(){}

export default AbstractExchangeIdRowControllerAR


AbstractExchangeIdRowControllerAR[$implementations]=[
 __AbstractExchangeIdRowControllerAR,
 AR,
 AbstractExchangeIdRowControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeIdRowControllerAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]