import {mountPins} from '@webcircuits/webcircuits'
import {ExchangeIdRowMemoryPQs} from '../../pqs/ExchangeIdRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIdRowCore}
 */
function __ExchangeIdRowCore() {}
__ExchangeIdRowCore.prototype = /** @type {!_ExchangeIdRowCore} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowCore}
 */
class _ExchangeIdRowCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowCore} ‎
 */
class ExchangeIdRowCore extends newAbstract(
 _ExchangeIdRowCore,10994752277,null,{
  asIExchangeIdRowCore:1,
  superExchangeIdRowCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowCore} */
ExchangeIdRowCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowCore} */
function ExchangeIdRowCoreClass(){}

export default ExchangeIdRowCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_ExchangeIdRowOuterCore}
 */
function __ExchangeIdRowOuterCore() {}
__ExchangeIdRowOuterCore.prototype = /** @type {!_ExchangeIdRowOuterCore} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIdRowOuterCore} */
export function ExchangeIdRowOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IExchangeIdRowOuterCore.Model}*/
  this.model={
    id: '',
    fixed: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowOuterCore}
 */
class _ExchangeIdRowOuterCore { }
/**
 * The _IExchangeIdRow_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowOuterCore} ‎
 */
export class ExchangeIdRowOuterCore extends newAbstract(
 _ExchangeIdRowOuterCore,10994752273,ExchangeIdRowOuterCoreConstructor,{
  asIExchangeIdRowOuterCore:1,
  superExchangeIdRowOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowOuterCore} */
ExchangeIdRowOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowOuterCore} */
function ExchangeIdRowOuterCoreClass(){}


ExchangeIdRowOuterCore[$implementations]=[
 __ExchangeIdRowOuterCore,
 ExchangeIdRowOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowOuterCore}*/({
  constructor(){
   mountPins(this.model,'',ExchangeIdRowMemoryPQs)

  },
 }),
]

ExchangeIdRowCore[$implementations]=[
 ExchangeIdRowCoreClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowCore}*/({
  resetCore(){
   this.resetExchangeIdRowCore()
  },
  resetExchangeIdRowCore(){
   ExchangeIdRowOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.ExchangeIdRowOuterCore}*/(
     /**@type {!xyz.swapee.wc.IExchangeIdRowOuterCore}*/(this)),
   )
  },
 }),
 __ExchangeIdRowCore,
 ExchangeIdRowOuterCore,
]

export {ExchangeIdRowCore}