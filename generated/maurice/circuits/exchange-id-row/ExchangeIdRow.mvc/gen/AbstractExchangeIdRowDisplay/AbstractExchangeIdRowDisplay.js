import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowDisplay}
 */
function __AbstractExchangeIdRowDisplay() {}
__AbstractExchangeIdRowDisplay.prototype = /** @type {!_AbstractExchangeIdRowDisplay} */ ({ })
/** @this {xyz.swapee.wc.ExchangeIdRowDisplay} */ function ExchangeIdRowDisplayConstructor() {
  /** @type {HTMLSpanElement} */ this.ExchangeIdLa=null
  /** @type {HTMLSpanElement} */ this.ExchangeIdFloatingLa=null
  /** @type {HTMLSpanElement} */ this.ExchangeIdFixedLa=null
  /** @type {HTMLButtonElement} */ this.RestartBu=null
  /** @type {HTMLElement} */ this.ExchangeIntent=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowDisplay}
 */
class _AbstractExchangeIdRowDisplay { }
/**
 * Display for presenting information from the _IExchangeIdRow_.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowDisplay} ‎
 */
class AbstractExchangeIdRowDisplay extends newAbstract(
 _AbstractExchangeIdRowDisplay,109947522717,ExchangeIdRowDisplayConstructor,{
  asIExchangeIdRowDisplay:1,
  superExchangeIdRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowDisplay} */
AbstractExchangeIdRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowDisplay} */
function AbstractExchangeIdRowDisplayClass(){}

export default AbstractExchangeIdRowDisplay


AbstractExchangeIdRowDisplay[$implementations]=[
 __AbstractExchangeIdRowDisplay,
 Display,
 AbstractExchangeIdRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{exchangeIntentScopeSel:exchangeIntentScopeSel}}=this
    this.scan({
     exchangeIntentSel:exchangeIntentScopeSel,
    })
   })
  },
  scan:function vduScan({exchangeIntentSel:exchangeIntentSelScope}){
   const{element:element,asIExchangeIdRowScreen:{vdusPQs:{
    ExchangeIdLa:ExchangeIdLa,
    ExchangeIdFixedLa:ExchangeIdFixedLa,
    ExchangeIdFloatingLa:ExchangeIdFloatingLa,
    RestartBu:RestartBu,
   }},queries:{exchangeIntentSel}}=this
   const children=getDataIds(element)
   /**@type {HTMLElement}*/
   const ExchangeIntent=((scope)=>{
    /**@type {HTMLElement}*/
    let root
    if(scope){
     root=element.closest(scope)
    }else root=document
    const _ExchangeIntent=exchangeIntentSel?root.querySelector(exchangeIntentSel):void 0
    return _ExchangeIntent
   })(exchangeIntentSelScope)
   Object.assign(this,{
    ExchangeIdLa:/**@type {HTMLSpanElement}*/(children[ExchangeIdLa]),
    ExchangeIdFixedLa:/**@type {HTMLSpanElement}*/(children[ExchangeIdFixedLa]),
    ExchangeIdFloatingLa:/**@type {HTMLSpanElement}*/(children[ExchangeIdFloatingLa]),
    RestartBu:/**@type {HTMLButtonElement}*/(children[RestartBu]),
    ExchangeIntent:ExchangeIntent,
   })
  },
 }),
 /** @type {!xyz.swapee.wc.ExchangeIdRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese}*/({
   ExchangeIdLa:1,
   ExchangeIdFloatingLa:1,
   ExchangeIdFixedLa:1,
   RestartBu:1,
   ExchangeIntent:1,
  }),
  initializer({
   ExchangeIdLa:_ExchangeIdLa,
   ExchangeIdFloatingLa:_ExchangeIdFloatingLa,
   ExchangeIdFixedLa:_ExchangeIdFixedLa,
   RestartBu:_RestartBu,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_ExchangeIdLa!==undefined) this.ExchangeIdLa=_ExchangeIdLa
   if(_ExchangeIdFloatingLa!==undefined) this.ExchangeIdFloatingLa=_ExchangeIdFloatingLa
   if(_ExchangeIdFixedLa!==undefined) this.ExchangeIdFixedLa=_ExchangeIdFixedLa
   if(_RestartBu!==undefined) this.RestartBu=_RestartBu
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]