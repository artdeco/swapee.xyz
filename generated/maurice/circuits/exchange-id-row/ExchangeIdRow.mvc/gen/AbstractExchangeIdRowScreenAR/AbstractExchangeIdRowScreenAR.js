import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowScreenAR}
 */
function __AbstractExchangeIdRowScreenAR() {}
__AbstractExchangeIdRowScreenAR.prototype = /** @type {!_AbstractExchangeIdRowScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR}
 */
class _AbstractExchangeIdRowScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIdRowScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR} ‎
 */
class AbstractExchangeIdRowScreenAR extends newAbstract(
 _AbstractExchangeIdRowScreenAR,109947522728,null,{
  asIExchangeIdRowScreenAR:1,
  superExchangeIdRowScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR} */
AbstractExchangeIdRowScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR} */
function AbstractExchangeIdRowScreenARClass(){}

export default AbstractExchangeIdRowScreenAR


AbstractExchangeIdRowScreenAR[$implementations]=[
 __AbstractExchangeIdRowScreenAR,
 AR,
 AbstractExchangeIdRowScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeIdRowScreenAR}*/({
  allocator(){
   this.methods={
   }
  },
 }),
]
export {AbstractExchangeIdRowScreenAR}