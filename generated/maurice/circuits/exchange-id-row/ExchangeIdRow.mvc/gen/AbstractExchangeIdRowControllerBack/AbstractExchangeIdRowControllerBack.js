import AbstractExchangeIdRowControllerAR from '../AbstractExchangeIdRowControllerAR'
import {AbstractExchangeIdRowController} from '../AbstractExchangeIdRowController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowControllerBack}
 */
function __AbstractExchangeIdRowControllerBack() {}
__AbstractExchangeIdRowControllerBack.prototype = /** @type {!_AbstractExchangeIdRowControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowController}
 */
class _AbstractExchangeIdRowControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowController} ‎
 */
class AbstractExchangeIdRowControllerBack extends newAbstract(
 _AbstractExchangeIdRowControllerBack,109947522723,null,{
  asIExchangeIdRowController:1,
  superExchangeIdRowController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowController} */
AbstractExchangeIdRowControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowController} */
function AbstractExchangeIdRowControllerBackClass(){}

export default AbstractExchangeIdRowControllerBack


AbstractExchangeIdRowControllerBack[$implementations]=[
 __AbstractExchangeIdRowControllerBack,
 AbstractExchangeIdRowController,
 AbstractExchangeIdRowControllerAR,
 DriverBack,
]