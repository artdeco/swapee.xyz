import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowComputer}
 */
function __AbstractExchangeIdRowComputer() {}
__AbstractExchangeIdRowComputer.prototype = /** @type {!_AbstractExchangeIdRowComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowComputer}
 */
class _AbstractExchangeIdRowComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowComputer} ‎
 */
export class AbstractExchangeIdRowComputer extends newAbstract(
 _AbstractExchangeIdRowComputer,10994752271,null,{
  asIExchangeIdRowComputer:1,
  superExchangeIdRowComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowComputer} */
AbstractExchangeIdRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowComputer} */
function AbstractExchangeIdRowComputerClass(){}


AbstractExchangeIdRowComputer[$implementations]=[
 __AbstractExchangeIdRowComputer,
 Adapter,
]


export default AbstractExchangeIdRowComputer