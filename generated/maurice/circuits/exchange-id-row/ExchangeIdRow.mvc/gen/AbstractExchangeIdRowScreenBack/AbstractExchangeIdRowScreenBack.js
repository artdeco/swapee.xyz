import AbstractExchangeIdRowScreenAT from '../AbstractExchangeIdRowScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowScreenBack}
 */
function __AbstractExchangeIdRowScreenBack() {}
__AbstractExchangeIdRowScreenBack.prototype = /** @type {!_AbstractExchangeIdRowScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowScreen}
 */
class _AbstractExchangeIdRowScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowScreen} ‎
 */
class AbstractExchangeIdRowScreenBack extends newAbstract(
 _AbstractExchangeIdRowScreenBack,109947522727,null,{
  asIExchangeIdRowScreen:1,
  superExchangeIdRowScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreen} */
AbstractExchangeIdRowScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreen} */
function AbstractExchangeIdRowScreenBackClass(){}

export default AbstractExchangeIdRowScreenBack


AbstractExchangeIdRowScreenBack[$implementations]=[
 __AbstractExchangeIdRowScreenBack,
 AbstractExchangeIdRowScreenAT,
]