import AbstractExchangeIdRowGPU from '../AbstractExchangeIdRowGPU'
import AbstractExchangeIdRowScreenBack from '../AbstractExchangeIdRowScreenBack'
import {HtmlComponent,Landed,mvc} from '@webcircuits/webcircuits'
import {access} from '@mauriceguest/guest2'
import {ExchangeIdRowInputsQPs} from '../../pqs/ExchangeIdRowInputsQPs'
import { newAbstract, $implementations, schedule } from '@type.engineering/type-engineer'
import AbstractExchangeIdRow from '../AbstractExchangeIdRow'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowHtmlComponent}
 */
function __AbstractExchangeIdRowHtmlComponent() {}
__AbstractExchangeIdRowHtmlComponent.prototype = /** @type {!_AbstractExchangeIdRowHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent}
 */
class _AbstractExchangeIdRowHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.ExchangeIdRowHtmlComponent} */ (res)
  }
}
/**
 * The _IExchangeIdRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent} ‎
 */
export class AbstractExchangeIdRowHtmlComponent extends newAbstract(
 _AbstractExchangeIdRowHtmlComponent,109947522712,null,{
  asIExchangeIdRowHtmlComponent:1,
  superExchangeIdRowHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent} */
AbstractExchangeIdRowHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent} */
function AbstractExchangeIdRowHtmlComponentClass(){}


AbstractExchangeIdRowHtmlComponent[$implementations]=[
 __AbstractExchangeIdRowHtmlComponent,
 HtmlComponent,
 AbstractExchangeIdRow,
 AbstractExchangeIdRowGPU,
 AbstractExchangeIdRowScreenBack,
 Landed,
 AbstractExchangeIdRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractExchangeIdRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/({
  inputsQPs:ExchangeIdRowInputsQPs,
 }),

/** @type {!AbstractExchangeIdRowHtmlComponent} */ ({ paint: [
   /**@this {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/function paintExchangeIdLa() {
   this.ExchangeIdLa.setText(this.model.id)
  }
 ] }),
 AbstractExchangeIdRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/({
  paint:function $reveal_ExchangeIdFixedLa({fixed:fixed}){
   const{
    asIExchangeIdRowGPU:{ExchangeIdFixedLa:ExchangeIdFixedLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExchangeIdFixedLa,fixed===true)
  },
 }),
 AbstractExchangeIdRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/({
  paint:function $reveal_ExchangeIdFloatingLa({fixed:fixed}){
   const{
    asIExchangeIdRowGPU:{ExchangeIdFloatingLa:ExchangeIdFloatingLa},
    asIBrowserView:{reveal:reveal},
   }=this
   reveal(ExchangeIdFloatingLa,fixed===false)
  },
 }),
 AbstractExchangeIdRowHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowHtmlComponent}*/({
  scheduleTwo:function shortLand(){
   const{
    asIHtmlComponent:{complete:complete},
    asIExchangeIdRowGPU:{
     ExchangeIntent:ExchangeIntent,
    },
   }=this
   complete(7833868048,{ExchangeIntent:ExchangeIntent})
  },
 }),
]