import AbstractExchangeIdRowProcessor from '../AbstractExchangeIdRowProcessor'
import {ExchangeIdRowCore} from '../ExchangeIdRowCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractExchangeIdRowComputer} from '../AbstractExchangeIdRowComputer'
import {AbstractExchangeIdRowController} from '../AbstractExchangeIdRowController'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRow}
 */
function __AbstractExchangeIdRow() {}
__AbstractExchangeIdRow.prototype = /** @type {!_AbstractExchangeIdRow} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRow}
 */
class _AbstractExchangeIdRow { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRow} ‎
 */
class AbstractExchangeIdRow extends newAbstract(
 _AbstractExchangeIdRow,10994752279,null,{
  asIExchangeIdRow:1,
  superExchangeIdRow:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRow} */
AbstractExchangeIdRow.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRow} */
function AbstractExchangeIdRowClass(){}

export default AbstractExchangeIdRow


AbstractExchangeIdRow[$implementations]=[
 __AbstractExchangeIdRow,
 ExchangeIdRowCore,
 AbstractExchangeIdRowProcessor,
 IntegratedComponent,
 AbstractExchangeIdRowComputer,
 AbstractExchangeIdRowController,
]


export {AbstractExchangeIdRow}