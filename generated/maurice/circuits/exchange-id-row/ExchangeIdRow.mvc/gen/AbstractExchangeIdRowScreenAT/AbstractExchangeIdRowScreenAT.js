import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowScreenAT}
 */
function __AbstractExchangeIdRowScreenAT() {}
__AbstractExchangeIdRowScreenAT.prototype = /** @type {!_AbstractExchangeIdRowScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT}
 */
class _AbstractExchangeIdRowScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIdRowScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT} ‎
 */
class AbstractExchangeIdRowScreenAT extends newAbstract(
 _AbstractExchangeIdRowScreenAT,109947522729,null,{
  asIExchangeIdRowScreenAT:1,
  superExchangeIdRowScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT} */
AbstractExchangeIdRowScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT} */
function AbstractExchangeIdRowScreenATClass(){}

export default AbstractExchangeIdRowScreenAT


AbstractExchangeIdRowScreenAT[$implementations]=[
 __AbstractExchangeIdRowScreenAT,
 UartUniversal,
]