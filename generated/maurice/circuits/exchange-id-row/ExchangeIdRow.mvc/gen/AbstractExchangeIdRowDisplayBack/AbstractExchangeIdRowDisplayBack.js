import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowDisplay}
 */
function __AbstractExchangeIdRowDisplay() {}
__AbstractExchangeIdRowDisplay.prototype = /** @type {!_AbstractExchangeIdRowDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowDisplay}
 */
class _AbstractExchangeIdRowDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractExchangeIdRowDisplay} ‎
 */
class AbstractExchangeIdRowDisplay extends newAbstract(
 _AbstractExchangeIdRowDisplay,109947522720,null,{
  asIExchangeIdRowDisplay:1,
  superExchangeIdRowDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowDisplay} */
AbstractExchangeIdRowDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowDisplay} */
function AbstractExchangeIdRowDisplayClass(){}

export default AbstractExchangeIdRowDisplay


AbstractExchangeIdRowDisplay[$implementations]=[
 __AbstractExchangeIdRowDisplay,
 GraphicsDriverBack,
 AbstractExchangeIdRowDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IExchangeIdRowDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IExchangeIdRowDisplay}*/({
    ExchangeIdLa:twinMock,
    ExchangeIdFixedLa:twinMock,
    ExchangeIdFloatingLa:twinMock,
    RestartBu:twinMock,
    ExchangeIntent:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.ExchangeIdRowDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese}*/({
   ExchangeIdLa:1,
   ExchangeIdFloatingLa:1,
   ExchangeIdFixedLa:1,
   RestartBu:1,
   ExchangeIntent:1,
  }),
  initializer({
   ExchangeIdLa:_ExchangeIdLa,
   ExchangeIdFloatingLa:_ExchangeIdFloatingLa,
   ExchangeIdFixedLa:_ExchangeIdFixedLa,
   RestartBu:_RestartBu,
   ExchangeIntent:_ExchangeIntent,
  }) {
   if(_ExchangeIdLa!==undefined) this.ExchangeIdLa=_ExchangeIdLa
   if(_ExchangeIdFloatingLa!==undefined) this.ExchangeIdFloatingLa=_ExchangeIdFloatingLa
   if(_ExchangeIdFixedLa!==undefined) this.ExchangeIdFixedLa=_ExchangeIdFixedLa
   if(_RestartBu!==undefined) this.RestartBu=_RestartBu
   if(_ExchangeIntent!==undefined) this.ExchangeIntent=_ExchangeIntent
  },
 }),
]