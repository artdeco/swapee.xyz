import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowControllerAT}
 */
function __AbstractExchangeIdRowControllerAT() {}
__AbstractExchangeIdRowControllerAT.prototype = /** @type {!_AbstractExchangeIdRowControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT}
 */
class _AbstractExchangeIdRowControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIdRowControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT} ‎
 */
class AbstractExchangeIdRowControllerAT extends newAbstract(
 _AbstractExchangeIdRowControllerAT,109947522725,null,{
  asIExchangeIdRowControllerAT:1,
  superExchangeIdRowControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT} */
AbstractExchangeIdRowControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT} */
function AbstractExchangeIdRowControllerATClass(){}

export default AbstractExchangeIdRowControllerAT


AbstractExchangeIdRowControllerAT[$implementations]=[
 __AbstractExchangeIdRowControllerAT,
 UartUniversal,
 AbstractExchangeIdRowControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IExchangeIdRowControllerAT}*/({
  get asIExchangeIdRowController(){
   return this
  },
 }),
]