
import AbstractExchangeIdRow from '../AbstractExchangeIdRow'

/** @abstract {xyz.swapee.wc.IExchangeIdRowElement} */
export default class AbstractExchangeIdRowElement { }



AbstractExchangeIdRowElement[$implementations]=[AbstractExchangeIdRow,
 /** @type {!AbstractExchangeIdRowElement} */ ({
  rootId:'ExchangeIdRow',
  __$id:1099475227,
  fqn:'xyz.swapee.wc.IExchangeIdRow',
  maurice_element_v3:true,
 }),
]