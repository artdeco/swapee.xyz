export default function ExchangeIdRowRenderVdus(){
 return (<div $id="ExchangeIdRow">
  <vdu $id="ExchangeIdLa" />
  <vdu $id="ExchangeIdFixedLa" />
  <vdu $id="ExchangeIdFloatingLa" />
  <vdu $id="RestartBu" />
 </div>)
}