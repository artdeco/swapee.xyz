import ExchangeIdRowRenderVdus from './methods/render-vdus'
import ExchangeIdRowElementPort from '../ExchangeIdRowElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent,Milleu} from '@mauriceguest/guest2'
import {Landed} from '@webcircuits/webcircuits'
import {ExchangeIdRowInputsPQs} from '../../pqs/ExchangeIdRowInputsPQs'
import {ExchangeIdRowQueriesPQs} from '../../pqs/ExchangeIdRowQueriesPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractExchangeIdRow from '../AbstractExchangeIdRow'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowElement}
 */
function __AbstractExchangeIdRowElement() {}
__AbstractExchangeIdRowElement.prototype = /** @type {!_AbstractExchangeIdRowElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowElement}
 */
class _AbstractExchangeIdRowElement { }
/**
 * A component description.
 *
 * The _IExchangeIdRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowElement} ‎
 */
class AbstractExchangeIdRowElement extends newAbstract(
 _AbstractExchangeIdRowElement,109947522713,null,{
  asIExchangeIdRowElement:1,
  superExchangeIdRowElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElement} */
AbstractExchangeIdRowElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElement} */
function AbstractExchangeIdRowElementClass(){}

export default AbstractExchangeIdRowElement


AbstractExchangeIdRowElement[$implementations]=[
 __AbstractExchangeIdRowElement,
 ElementBase,
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':id':idColAttr,
   ':fixed':fixedColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'id':idAttr,
    'fixed':fixedAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(idAttr===undefined?{'id':idColAttr}:{}),
    ...(fixedAttr===undefined?{'fixed':fixedColAttr}:{}),
   }
  },
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'id':idAttr,
   'fixed':fixedAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    id:idAttr,
    fixed:fixedAttr,
   }
  },
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  render:ExchangeIdRowRenderVdus,
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  classes:{
   'Class': '9bd81',
  },
  inputsPQs:ExchangeIdRowInputsPQs,
  queriesPQs:ExchangeIdRowQueriesPQs,
  vdus:{
   'ExchangeIdLa': 'e02a1',
   'ExchangeIdFloatingLa': 'e02a2',
   'ExchangeIdFixedLa': 'e02a3',
   'ExchangeIntent': 'e02a5',
   'RestartBu': 'e02a6',
  },
 }),
 IntegratedComponent,
 Milleu,
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','id','fixed','query:exchange-intent','no-solder',':no-solder',':id',':fixed','fe646','fb723','4129d','d3026','7a978','c3b6b','b80bb','cec31','children']),
   })
  },
  get Port(){
   return ExchangeIdRowElementPort
  },
  calibrate:function calibrateDynQueriesFromAttrs({'query:exchange-intent':exchangeIntentSel}){
   const _ret={}
   if(exchangeIntentSel) _ret.exchangeIntentSel=exchangeIntentSel
   return _ret
  },
 }),
 Landed,
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  constructor(){
   this.land={
    ExchangeIntent:null,
   }
  },
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  calibrate:async function awaitOnExchangeIntent({exchangeIntentSel:exchangeIntentSel}){
   if(!exchangeIntentSel) return{}

   const{
    asIMilleu:{milleu:milleu},
    asILanded:{land:land},
    asIElement:{fqn:fqn},
   }=this
   const ExchangeIntent=await milleu(exchangeIntentSel,true)
   if(!ExchangeIntent) {
    console.warn('❗️ exchangeIntentSel %s must be present on the page for %s to work',exchangeIntentSel,fqn)
    return{}
   }
   land.ExchangeIntent=ExchangeIntent
  },
 }),
 AbstractExchangeIdRowElementClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
  solder:(_,{
   exchangeIntentSel:exchangeIntentSel,
  })=>{
   return{
    exchangeIntentSel:exchangeIntentSel,
   }
  },
 }),
]



AbstractExchangeIdRowElement[$implementations]=[AbstractExchangeIdRow,
 /** @type {!AbstractExchangeIdRowElement} */ ({
  rootId:'ExchangeIdRow',
  __$id:1099475227,
  fqn:'xyz.swapee.wc.IExchangeIdRow',
  maurice_element_v3:true,
 }),
]