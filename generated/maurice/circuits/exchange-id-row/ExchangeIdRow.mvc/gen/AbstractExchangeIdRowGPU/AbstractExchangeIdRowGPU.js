import AbstractExchangeIdRowDisplay from '../AbstractExchangeIdRowDisplayBack'
import ExchangeIdRowClassesPQs from '../../pqs/ExchangeIdRowClassesPQs'
import {BrowserView} from '@webcircuits/webcircuits'
import {pressFit} from '@mauriceguest/guest2'
import {ExchangeIdRowClassesQPs} from '../../pqs/ExchangeIdRowClassesQPs'
import {ExchangeIdRowVdusPQs} from '../../pqs/ExchangeIdRowVdusPQs'
import {ExchangeIdRowVdusQPs} from '../../pqs/ExchangeIdRowVdusQPs'
import {ExchangeIdRowMemoryPQs} from '../../pqs/ExchangeIdRowMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowGPU}
 */
function __AbstractExchangeIdRowGPU() {}
__AbstractExchangeIdRowGPU.prototype = /** @type {!_AbstractExchangeIdRowGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowGPU}
 */
class _AbstractExchangeIdRowGPU { }
/**
 * Handles the periphery of the _IExchangeIdRowDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowGPU} ‎
 */
class AbstractExchangeIdRowGPU extends newAbstract(
 _AbstractExchangeIdRowGPU,109947522716,null,{
  asIExchangeIdRowGPU:1,
  superExchangeIdRowGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowGPU} */
AbstractExchangeIdRowGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowGPU} */
function AbstractExchangeIdRowGPUClass(){}

export default AbstractExchangeIdRowGPU


AbstractExchangeIdRowGPU[$implementations]=[
 __AbstractExchangeIdRowGPU,
 AbstractExchangeIdRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowGPU}*/({
  classesQPs:ExchangeIdRowClassesQPs,
  vdusPQs:ExchangeIdRowVdusPQs,
  vdusQPs:ExchangeIdRowVdusQPs,
  memoryPQs:ExchangeIdRowMemoryPQs,
 }),
 AbstractExchangeIdRowDisplay,
 BrowserView,
 AbstractExchangeIdRowGPUClass.prototype=/**@type {!xyz.swapee.wc.IExchangeIdRowGPU}*/({
  allocator(){
   pressFit(this.classes,'',ExchangeIdRowClassesPQs)
  },
 }),
]