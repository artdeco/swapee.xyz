import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractExchangeIdRowProcessor}
 */
function __AbstractExchangeIdRowProcessor() {}
__AbstractExchangeIdRowProcessor.prototype = /** @type {!_AbstractExchangeIdRowProcessor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowProcessor}
 */
class _AbstractExchangeIdRowProcessor { }
/**
 * The processor to compute changes to the memory for the _IExchangeIdRow_.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowProcessor} ‎
 */
class AbstractExchangeIdRowProcessor extends newAbstract(
 _AbstractExchangeIdRowProcessor,10994752278,null,{
  asIExchangeIdRowProcessor:1,
  superExchangeIdRowProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowProcessor} */
AbstractExchangeIdRowProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowProcessor} */
function AbstractExchangeIdRowProcessorClass(){}

export default AbstractExchangeIdRowProcessor


AbstractExchangeIdRowProcessor[$implementations]=[
 __AbstractExchangeIdRowProcessor,
]