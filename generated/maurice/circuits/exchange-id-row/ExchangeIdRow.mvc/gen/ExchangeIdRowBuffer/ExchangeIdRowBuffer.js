import {makeBuffers} from '@webcircuits/webcircuits'

export const ExchangeIdRowBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  id:String,
  fixed:Boolean,
 }),
})

export default ExchangeIdRowBuffer