import deduceInputs from './methods/deduce-inputs'
import AbstractExchangeIdRowControllerAT from '../../gen/AbstractExchangeIdRowControllerAT'
import ExchangeIdRowDisplay from '../ExchangeIdRowDisplay'
import AbstractExchangeIdRowScreen from '../../gen/AbstractExchangeIdRowScreen'

/** @extends {xyz.swapee.wc.ExchangeIdRowScreen} */
export default class extends AbstractExchangeIdRowScreen.implements(
 AbstractExchangeIdRowControllerAT,
 ExchangeIdRowDisplay,
 /**@type {!xyz.swapee.wc.IExchangeIdRowScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IExchangeIdRowScreen} */ ({
  deduceInputs:deduceInputs,
  __$id:1099475227,
 }),
){}