/** @type {xyz.swapee.wc.IExchangeIdRowElement._render} */
export default function ExchangeIdRowRender({id:id,fixed:fixed},{}) {
 return (<div $id="ExchangeIdRow">
  <span $id="ExchangeIdLa">{id}</span>
  <label $id="ExchangeIdFixedLa" $reveal={fixed===true} />
  <label $id="ExchangeIdFloatingLa" $reveal={fixed===false} />
 </div>)
}