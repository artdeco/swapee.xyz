import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import ExchangeIdRowServerController from '../ExchangeIdRowServerController'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractExchangeIdRowElement from '../../gen/AbstractExchangeIdRowElement'

/** @extends {xyz.swapee.wc.ExchangeIdRowElement} */
export default class ExchangeIdRowElement extends AbstractExchangeIdRowElement.implements(
 ExchangeIdRowServerController,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IExchangeIdRowElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IExchangeIdRowElement}*/({
   classesMap: true,
   rootSelector:     `.ExchangeIdRow`,
   stylesheet:       'html/styles/ExchangeIdRow.css',
   blockName:        'html/ExchangeIdRowBlock.html',
   // buildExchangeIntent({
   //  fixed:fixed,
   // }){
   //  return (<div $id="ExchangeIdRow">

   //  </div>)
   // },
   // buildExchangeBroker({
   //  id:id,
   // }){
   //  return (<div $id="ExchangeIdRow" $reveal={id}>
   //   <span $id="ExchangeIdLa">{id}</span>
   //  </div>)
   // },
  }),
){}

// thank you for using web circuits
