import ExchangeIdRowHtmlController from '../ExchangeIdRowHtmlController'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractExchangeIdRowHtmlComponent} from '../../gen/AbstractExchangeIdRowHtmlComponent'

/** @extends {xyz.swapee.wc.ExchangeIdRowHtmlComponent} */
export default class extends AbstractExchangeIdRowHtmlComponent.implements(
 ExchangeIdRowHtmlController,
 IntegratedComponentInitialiser,
){}