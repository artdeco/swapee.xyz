import {ExchangeIdRowMemoryPQs} from './ExchangeIdRowMemoryPQs'
export const ExchangeIdRowMemoryQPs=/**@type {!xyz.swapee.wc.ExchangeIdRowMemoryQPs}*/(Object.keys(ExchangeIdRowMemoryPQs)
 .reduce((a,k)=>{a[ExchangeIdRowMemoryPQs[k]]=k;return a},{}))