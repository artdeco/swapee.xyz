export const ExchangeIdRowVdusPQs=/**@type {!xyz.swapee.wc.ExchangeIdRowVdusPQs}*/({
 ExchangeIdLa:'e02a1',
 ExchangeIdFloatingLa:'e02a2',
 ExchangeIdFixedLa:'e02a3',
 ExchangeIntent:'e02a5',
 RestartBu:'e02a6',
})