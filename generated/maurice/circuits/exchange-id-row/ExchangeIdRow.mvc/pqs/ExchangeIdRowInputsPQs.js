import {ExchangeIdRowMemoryPQs} from './ExchangeIdRowMemoryPQs'
export const ExchangeIdRowInputsPQs=/**@type {!xyz.swapee.wc.ExchangeIdRowInputsQPs}*/({
 ...ExchangeIdRowMemoryPQs,
})