import {ExchangeIdRowVdusPQs} from './ExchangeIdRowVdusPQs'
export const ExchangeIdRowVdusQPs=/**@type {!xyz.swapee.wc.ExchangeIdRowVdusQPs}*/(Object.keys(ExchangeIdRowVdusPQs)
 .reduce((a,k)=>{a[ExchangeIdRowVdusPQs[k]]=k;return a},{}))