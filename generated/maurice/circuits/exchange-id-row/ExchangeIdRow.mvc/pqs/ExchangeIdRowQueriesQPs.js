import {ExchangeIdRowQueriesPQs} from './ExchangeIdRowQueriesPQs'
export const ExchangeIdRowQueriesQPs=/**@type {!xyz.swapee.wc.ExchangeIdRowQueriesQPs}*/(Object.keys(ExchangeIdRowQueriesPQs)
 .reduce((a,k)=>{a[ExchangeIdRowQueriesPQs[k]]=k;return a},{}))