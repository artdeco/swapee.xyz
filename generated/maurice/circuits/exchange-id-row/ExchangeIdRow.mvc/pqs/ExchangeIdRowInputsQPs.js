import {ExchangeIdRowInputsPQs} from './ExchangeIdRowInputsPQs'
export const ExchangeIdRowInputsQPs=/**@type {!xyz.swapee.wc.ExchangeIdRowInputsQPs}*/(Object.keys(ExchangeIdRowInputsPQs)
 .reduce((a,k)=>{a[ExchangeIdRowInputsPQs[k]]=k;return a},{}))