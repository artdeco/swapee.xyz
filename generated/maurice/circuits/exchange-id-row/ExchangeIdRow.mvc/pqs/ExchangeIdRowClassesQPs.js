import ExchangeIdRowClassesPQs from './ExchangeIdRowClassesPQs'
export const ExchangeIdRowClassesQPs=/**@type {!xyz.swapee.wc.ExchangeIdRowClassesQPs}*/(Object.keys(ExchangeIdRowClassesPQs)
 .reduce((a,k)=>{a[ExchangeIdRowClassesPQs[k]]=k;return a},{}))