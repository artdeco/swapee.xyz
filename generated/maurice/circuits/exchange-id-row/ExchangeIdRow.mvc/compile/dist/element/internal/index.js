import Module from './element'

/**@extends {xyz.swapee.wc.AbstractExchangeIdRow}*/
export class AbstractExchangeIdRow extends Module['10994752271'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRow} */
AbstractExchangeIdRow.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIdRowPort} */
export const ExchangeIdRowPort=Module['10994752273']
/**@extends {xyz.swapee.wc.AbstractExchangeIdRowController}*/
export class AbstractExchangeIdRowController extends Module['10994752274'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowController} */
AbstractExchangeIdRowController.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIdRowElement} */
export const ExchangeIdRowElement=Module['10994752278']
/** @type {typeof xyz.swapee.wc.ExchangeIdRowBuffer} */
export const ExchangeIdRowBuffer=Module['109947522711']
/**@extends {xyz.swapee.wc.AbstractExchangeIdRowComputer}*/
export class AbstractExchangeIdRowComputer extends Module['109947522730'] {}
/** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowComputer} */
AbstractExchangeIdRowComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.ExchangeIdRowController} */
export const ExchangeIdRowController=Module['109947522761']