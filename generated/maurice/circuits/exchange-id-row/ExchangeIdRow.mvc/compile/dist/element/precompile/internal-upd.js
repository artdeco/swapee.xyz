import {getPackageName, isShared} from '../version'
/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@mauriceguest/find-tags (c) by Art Deco? 2023.
@type.engineering/short-id (c) by Type Engineering 2023.
@artdeco/super-maps (c) by Art Deco? 2023.
@mauriceguest/fin-de-siecle (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
const packageName=getPackageName()

/** @nosideeffects */
function getEmbeddedModule(exports,require,module={},__filename='',__dirname='') {
 const fn=new Function('exports,require,module,__filename,__dirname',`
/*! @embed-object-start {1099475227} */
'use strict';
const https = require('https');
const http = require('http');
const util = require('util');
const os = require('os');
const url = require('url');
const stream = require('stream');
const zlib = require('zlib');
const _crypto = require('crypto');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const child_process = require('child_process');/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return require(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const d=c["372700389811"];function e(a,b,f,g){return c["372700389812"](a,b,f,g,!1,void 0)};function h(){}h.prototype={};class k{}class l extends e(k,10994752278,null,{D:1,Z:2}){}l[d]=[h];

const m=function(){return require(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const n=m["615055805212"],p=m["615055805218"],r=m["615055805233"],t=m["615055805235"];const u={core:"a74ad",id:"b80bb",K:"5a67c",fixed:"cec31"};function v(){}v.prototype={};class w{}class x extends e(w,10994752277,null,{o:1,U:2}){}function y(){}y.prototype={};function z(){this.model={id:"",fixed:null}}class A{}class B extends e(A,10994752273,z,{v:1,X:2}){}B[d]=[y,{constructor(){r(this.model,"",u)}}];x[d]=[{},v,B];
const C=function(){return require(eval('/*dequire*/"@mauriceguest/guest2"'))}();const aa=C.IntegratedComponentInitialiser,D=C.IntegratedComponent,ba=C["95173443851"],ca=C["95173443852"];function E(){}E.prototype={};class da{}class F extends e(da,10994752271,null,{l:1,R:2}){}F[d]=[E,p];const G={regulate:n({id:String,fixed:Boolean})};
const H=function(){return require(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const ea=H.IntegratedController,fa=H.Parametric;const I={...u};function J(){}J.prototype={};function K(){const a={model:null};z.call(a);this.inputs=a.model}class ha{}class L extends e(ha,10994752275,K,{A:1,Y:2}){}function M(){}L[d]=[M.prototype={resetPort(){K.call(this)}},J,fa,M.prototype={constructor(){r(this.inputs,"",I)}}];function N(){}N.prototype={};class ia{}class O extends e(ia,109947522721,null,{m:1,T:2}){}O[d]=[{resetPort(){this.port.resetPort()}},N,G,ea,{get Port(){return L}}];function P(){}P.prototype={};class ja{}class Q extends e(ja,10994752279,null,{j:1,P:2}){}Q[d]=[P,x,l,D,F,O];function ka(){return{}};const la=require(eval('"@type.engineering/web-computing"')).h;function ma(){return la("div",{$id:"ExchangeIdRow"})};const R=require(eval('"@type.engineering/web-computing"')).h;function na({id:a,fixed:b}){return R("div",{$id:"ExchangeIdRow"},R("span",{$id:"ExchangeIdLa"},a),R("label",{$id:"ExchangeIdFixedLa",$reveal:!0===b}),R("label",{$id:"ExchangeIdFloatingLa",$reveal:!1===b}))};var S=class extends O.implements(){};require("https");require("http");const oa=require("util").debuglog;require("os");require("url");require("stream");require("zlib");try{require("../package.json")}catch(a){}oa("aqt");require("fs");require("child_process");
const T=function(){return require(eval('/*dequire*/"@mauriceguest/fin-de-siecle"'))}();
const pa=T.ElementBase,qa=T.HTMLBlocker;const U=require(eval('"@type.engineering/web-computing"')).h;function V(){}V.prototype={};function ra(){this.inputs={noSolder:!1,I:{},H:{},G:{},M:{},J:{}}}class sa{}class W extends e(sa,109947522714,ra,{u:1,W:2}){}W[d]=[V,{constructor(){Object.assign(this.inputs,{"no-solder":void 0,"exchange-id-la-opts":void 0,"exchange-id-floating-la-opts":void 0,"exchange-id-fixed-la-opts":void 0,"restart-bu-opts":void 0,"exchange-intent-opts":void 0})}}];function X(){}X.prototype={};class ta{}class Y extends e(ta,109947522713,null,{s:1,V:2}){}function Z(){}
Y[d]=[X,pa,Z.prototype={calibrate:function({":no-solder":a,":id":b,":fixed":f}){const {attributes:{"no-solder":g,id:q,fixed:ua}}=this;return{...(void 0===g?{"no-solder":a}:{}),...(void 0===q?{id:b}:{}),...(void 0===ua?{fixed:f}:{})}}},Z.prototype={calibrate:({"no-solder":a,id:b,fixed:f})=>({noSolder:a,id:b,fixed:f})},Z.prototype={__$constructor(){this.vdusOpts=new Map}},Z.prototype={render:function(){return U("div",{$id:"ExchangeIdRow"},U("vdu",{$id:"ExchangeIdLa"}),U("vdu",{$id:"ExchangeIdFixedLa"}),
U("vdu",{$id:"ExchangeIdFloatingLa"}),U("vdu",{$id:"RestartBu"}))}},Z.prototype={classes:{Class:"9bd81"},inputsPQs:I,queriesPQs:{F:"ecfe5",g:"13da4"},vdus:{ExchangeIdLa:"e02a1",ExchangeIdFloatingLa:"e02a2",ExchangeIdFixedLa:"e02a3",ExchangeIntent:"e02a5",RestartBu:"e02a6"}},D,ca,Z.prototype={constructor(){Object.assign(this,{knownInputs:new Set("noSolder id fixed query:exchange-intent no-solder :no-solder :id :fixed fe646 fb723 4129d d3026 7a978 c3b6b b80bb cec31 children".split(" "))})},get Port(){return W},
calibrate:function({"query:exchange-intent":a}){const b={};a&&(b.g=a);return b}},t,Z.prototype={constructor(){this.land={i:null}}},Z.prototype={calibrate:async function({g:a}){if(!a)return{};const {asIMilleu:{milleu:b},asILanded:{land:f},asIElement:{fqn:g}}=this,q=await b(a,!0);if(!q)return console.warn("\\u2757\\ufe0f exchangeIntentSel %s must be present on the page for %s to work",a,g),{};f.i=q}},Z.prototype={solder:(a,{g:b})=>({g:b})}];
Y[d]=[Q,{rootId:"ExchangeIdRow",__$id:1099475227,fqn:"xyz.swapee.wc.IExchangeIdRow",maurice_element_v3:!0}];class va extends Y.implements(S,ba,qa,aa,{solder:ka,server:ma,render:na},{classesMap:!0,rootSelector:".ExchangeIdRow",stylesheet:"html/styles/ExchangeIdRow.css",blockName:"html/ExchangeIdRowBlock.html"}){};module.exports["10994752270"]=Q;module.exports["10994752271"]=Q;module.exports["10994752273"]=L;module.exports["10994752274"]=O;module.exports["10994752278"]=va;module.exports["109947522711"]=G;module.exports["109947522730"]=F;module.exports["109947522761"]=S;
/*! @embed-object-end {1099475227} */`)
 module['exports']=module.exports||exports||{}
 fn(exports,require,module,__filename,__dirname)
 const m=module['exports']
 if(require) require[packageName]=m
 return m
}

/** @nosideeffects */
function getSharedModule(exports,req) {
 return req(eval(`/*dequire*/"${packageName}"`))
}

const shared=isShared()

export default shared?getSharedModule:getEmbeddedModule