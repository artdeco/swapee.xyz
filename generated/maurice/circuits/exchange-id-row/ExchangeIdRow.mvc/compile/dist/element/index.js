/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRow` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRow}
 */
class AbstractExchangeIdRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeIdRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeIdRowPort}
 */
class ExchangeIdRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowController}
 */
class AbstractExchangeIdRowController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IExchangeIdRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.ExchangeIdRowElement}
 */
class ExchangeIdRowElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeIdRowBuffer}
 */
class ExchangeIdRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowComputer}
 */
class AbstractExchangeIdRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.ExchangeIdRowController}
 */
class ExchangeIdRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeIdRow = AbstractExchangeIdRow
module.exports.ExchangeIdRowPort = ExchangeIdRowPort
module.exports.AbstractExchangeIdRowController = AbstractExchangeIdRowController
module.exports.ExchangeIdRowElement = ExchangeIdRowElement
module.exports.ExchangeIdRowBuffer = ExchangeIdRowBuffer
module.exports.AbstractExchangeIdRowComputer = AbstractExchangeIdRowComputer
module.exports.ExchangeIdRowController = ExchangeIdRowController

Object.defineProperties(module.exports, {
 'AbstractExchangeIdRow': {get: () => require('./precompile/internal')[10994752271]},
 [10994752271]: {get: () => module.exports['AbstractExchangeIdRow']},
 'ExchangeIdRowPort': {get: () => require('./precompile/internal')[10994752273]},
 [10994752273]: {get: () => module.exports['ExchangeIdRowPort']},
 'AbstractExchangeIdRowController': {get: () => require('./precompile/internal')[10994752274]},
 [10994752274]: {get: () => module.exports['AbstractExchangeIdRowController']},
 'ExchangeIdRowElement': {get: () => require('./precompile/internal')[10994752278]},
 [10994752278]: {get: () => module.exports['ExchangeIdRowElement']},
 'ExchangeIdRowBuffer': {get: () => require('./precompile/internal')[109947522711]},
 [109947522711]: {get: () => module.exports['ExchangeIdRowBuffer']},
 'AbstractExchangeIdRowComputer': {get: () => require('./precompile/internal')[109947522730]},
 [109947522730]: {get: () => module.exports['AbstractExchangeIdRowComputer']},
 'ExchangeIdRowController': {get: () => require('./precompile/internal')[109947522761]},
 [109947522761]: {get: () => module.exports['ExchangeIdRowController']},
})