/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/seers (c) by Art Deco (tm) 2024.
@type.engineering/Seers (c) by Art Deco (tm) 2023.
@type.engineering/Real-MVC (c) by Art Deco (tm) 2023.
@webcircuits/back (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
Please make sure you have Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const c=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const aa=c["372700389810"],d=c["372700389811"];function f(a,b,e,h){return c["372700389812"](a,b,e,h,!1,void 0)};function g(){}g.prototype={};class ba{}class k extends f(ba,10994752278,null,{J:1,$:2}){}k[d]=[g];
const l=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/webcircuits"'))}();const ca=l["61505580523"],da=l["615055805212"],ea=l["615055805218"],fa=l["615055805221"],ha=l["615055805223"],m=l["615055805233"],ia=l["615055805235"];const n={M:"a74ad",id:"b80bb",O:"5a67c",fixed:"cec31"};function p(){}p.prototype={};class ja{}class q extends f(ja,10994752277,null,{D:1,U:2}){}function r(){}r.prototype={};function t(){this.model={id:"",fixed:null}}class ka{}class u extends f(ka,10994752273,t,{H:1,Y:2}){}u[d]=[r,{constructor(){m(this.model,"",n)}}];q[d]=[{},p,u];

const v=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/real-mvc"'))}();
const la=v.IntegratedController,ma=v.Parametric;
const w=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@mauriceguest/guest2"'))}();const na=w.IntegratedComponentInitialiser,oa=w.IntegratedComponent,pa=w["38"];function x(){}x.prototype={};class qa{}class y extends f(qa,10994752271,null,{A:1,R:2}){}y[d]=[x,ea];const z={regulate:da({id:String,fixed:Boolean})};const A={...n};function B(){}B.prototype={};function C(){const a={model:null};t.call(a);this.inputs=a.model}class ra{}class D extends f(ra,10994752275,C,{I:1,Z:2}){}function E(){}D[d]=[E.prototype={resetPort(){C.call(this)}},B,ma,E.prototype={constructor(){m(this.inputs,"",A)}}];function F(){}F.prototype={};class sa{}class G extends f(sa,109947522721,null,{o:1,s:2}){}G[d]=[{resetPort(){this.port.resetPort()}},F,z,la,{get Port(){return D}}];function H(){}H.prototype={};class ta{}class I extends f(ta,10994752279,null,{v:1,P:2}){}I[d]=[H,q,k,oa,y,G];
const J=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/back"'))}();
const ua=J["12817393923"],va=J["12817393924"],wa=J["12817393925"],xa=J["12817393926"];function K(){}K.prototype={};class ya{}class L extends f(ya,109947522724,null,{C:1,T:2}){}L[d]=[K,xa,{allocator(){this.methods={}}}];function M(){}M.prototype={};class za{}class N extends f(za,109947522723,null,{o:1,s:2}){}N[d]=[M,G,L,ua];var O=class extends N.implements(){};function P(){}P.prototype={};class Aa{}class Q extends f(Aa,109947522720,null,{F:1,V:2}){}Q[d]=[P,va,{constructor:function(){const {asIGraphicsDriverBack:{twinMock:a}}=this;Object.assign(this,{j:a,h:a,i:a,l:a,g:a})}},{[aa]:{j:1,i:1,h:1,l:1,g:1},initializer({j:a,i:b,h:e,l:h,g:T}){void 0!==a&&(this.j=a);void 0!==b&&(this.i=b);void 0!==e&&(this.h=e);void 0!==h&&(this.l=h);void 0!==T&&(this.g=T)}}];const R={u:"9bd81"};const Ba=Object.keys(R).reduce((a,b)=>{a[R[b]]=b;return a},{});const S={j:"e02a1",i:"e02a2",h:"e02a3",g:"e02a5",l:"e02a6"};const Ca=Object.keys(S).reduce((a,b)=>{a[S[b]]=b;return a},{});function U(){}U.prototype={};class Da{}class V extends f(Da,109947522716,null,{m:1,W:2}){}function W(){}V[d]=[U,W.prototype={classesQPs:Ba,vdusQPs:Ca,memoryPQs:n},Q,ca,W.prototype={allocator(){pa(this.classes,"",R)}}];function X(){}X.prototype={};class Ea{}class Y extends f(Ea,109947522729,null,{L:1,ba:2}){}Y[d]=[X,wa];function Fa(){}Fa.prototype={};class Ga{}class Ha extends f(Ga,109947522727,null,{K:1,aa:2}){}Ha[d]=[Fa,Y];const Ia=Object.keys(A).reduce((a,b)=>{a[A[b]]=b;return a},{});function Ja(){}Ja.prototype={};class Ka{static mvc(a,b,e){return ha(this,a,b,null,e)}}class La extends f(Ka,109947522712,null,{G:1,X:2}){}function Z(){}
La[d]=[Ja,fa,I,V,Ha,ia,Z.prototype={constructor(){this.land={g:null}}},Z.prototype={inputsQPs:Ia},{paint:[function(){this.j.setText(this.model.id)}]},Z.prototype={paint:function({fixed:a}){const {m:{h:b},asIBrowserView:{reveal:e}}=this;e(b,!0===a)}},Z.prototype={paint:function({fixed:a}){const {m:{i:b},asIBrowserView:{reveal:e}}=this;e(b,!1===a)}},Z.prototype={scheduleTwo:function(){const {asIHtmlComponent:{complete:a},m:{g:b}}=this;a(7833868048,{g:b})}}];var Ma=class extends La.implements(O,na){};module.exports["10994752270"]=I;module.exports["10994752271"]=I;module.exports["10994752273"]=D;module.exports["10994752274"]=G;module.exports["109947522710"]=Ma;module.exports["109947522711"]=z;module.exports["109947522730"]=y;module.exports["109947522761"]=O;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1099475227']=module.exports