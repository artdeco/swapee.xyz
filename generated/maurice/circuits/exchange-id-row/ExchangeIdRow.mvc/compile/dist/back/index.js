/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRow` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRow}
 */
class AbstractExchangeIdRow extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IExchangeIdRow_, providing input
 * pins.
 * @extends {xyz.swapee.wc.ExchangeIdRowPort}
 */
class ExchangeIdRowPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowController` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowController}
 */
class AbstractExchangeIdRowController extends (class {/* lazy-loaded */}) {}
/**
 * The _IExchangeIdRow_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 */
class ExchangeIdRowHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.ExchangeIdRowBuffer}
 */
class ExchangeIdRowBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowComputer` interface.
 * @extends {xyz.swapee.wc.AbstractExchangeIdRowComputer}
 */
class AbstractExchangeIdRowComputer extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.ExchangeIdRowController}
 */
class ExchangeIdRowController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractExchangeIdRow = AbstractExchangeIdRow
module.exports.ExchangeIdRowPort = ExchangeIdRowPort
module.exports.AbstractExchangeIdRowController = AbstractExchangeIdRowController
module.exports.ExchangeIdRowHtmlComponent = ExchangeIdRowHtmlComponent
module.exports.ExchangeIdRowBuffer = ExchangeIdRowBuffer
module.exports.AbstractExchangeIdRowComputer = AbstractExchangeIdRowComputer
module.exports.ExchangeIdRowController = ExchangeIdRowController