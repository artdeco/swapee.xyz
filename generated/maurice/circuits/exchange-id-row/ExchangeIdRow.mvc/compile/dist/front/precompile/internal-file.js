/**
@license
@LICENSE                      Warning!
This file links to (or embeds, in which case you will see object code
boundaries comments before and after), proprietary code from the package(s):
[? Any reverse engineering of source code is strictly prohibited !]
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2024.
@type.engineering/protypes (c) by Art Deco (tm) 2023.
@type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
@webcircuits.com/front (c) by Art Deco 2023.
Please make sure you have a Commercial License to use this library.
*/
var module=self.module||{exports:{}}
/*

[? Any reverse engineering of source code is strictly prohibited !]
@LICENSE @type.engineering/Type-Engineer (c) by Art Deco (tm) 2023.
Please make sure you have a Commercial License to use this library.
*/

const g=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@type.engineering/type-engineer"'))}();
const h=g["37270038985"],k=g["372700389810"],l=g["372700389811"];function q(a,b,c,d){return g["372700389812"](a,b,c,d,!1,void 0)};
const r=function(){return(0,self.DEPACK_REQUIRE)(eval('/*dequire*/"@webcircuits/front"'))}();
const t=r["61893096584"],u=r["61893096586"],w=r["618930965811"],x=r["618930965812"],y=r["618930965815"];function z(){}z.prototype={};function A(){this.l=this.j=this.h=this.i=this.g=null}class B{}class C extends q(B,109947522717,A,{o:1,H:2}){}
C[l]=[z,t,{constructor(){h(this,()=>{const {queries:{D:a}}=this;this.scan({m:a})})},scan:function({m:a}){const {element:b,s:{vdusPQs:{g:c,h:d,i:m,j:M}},queries:{m:v}}=this,n=y(b);let p;a?p=b.closest(a):p=document;Object.assign(this,{g:n[c],h:n[d],i:n[m],j:n[M],l:v?p.querySelector(v):void 0})}},{[k]:{g:1,i:1,h:1,j:1,l:1},initializer({g:a,i:b,h:c,j:d,l:m}){void 0!==a&&(this.g=a);void 0!==b&&(this.i=b);void 0!==c&&(this.h=c);void 0!==d&&(this.j=d);void 0!==m&&(this.l=m)}}];var D=class extends C.implements(){};function E(){};function F(){}F.prototype={};class G{}class H extends q(G,109947522725,null,{u:1,G:2}){}H[l]=[F,w,{}];function I(){}I.prototype={};class J{}class K extends q(J,109947522728,null,{v:1,J:2}){}K[l]=[I,x,{allocator(){this.methods={}}}];const L={A:"a74ad",id:"b80bb",F:"5a67c",fixed:"cec31"};const N={...L};const O=Object.keys(L).reduce((a,b)=>{a[L[b]]=b;return a},{});function P(){}P.prototype={};class Q{}class R extends q(Q,109947522726,null,{s:1,I:2}){}function S(){}R[l]=[P,S.prototype={deduceInputs(){const {o:{g:a}}=this;return{id:null==a?void 0:a.innerText}}},S.prototype={inputsPQs:N,queriesPQs:{C:"ecfe5",m:"13da4"},memoryQPs:O},u,K,S.prototype={vdusPQs:{g:"e02a1",i:"e02a2",h:"e02a3",l:"e02a5",j:"e02a6"}}];var T=class extends R.implements(H,D,{get queries(){return this.settings}},{deduceInputs:E,__$id:1099475227}){};module.exports["109947522741"]=D;module.exports["109947522771"]=T;

//# sourceMappingURL=internal.js.map
self.DEPACK_REQUIRE['1099475227']=module.exports