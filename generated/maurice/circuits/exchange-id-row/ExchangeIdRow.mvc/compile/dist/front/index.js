/**
 * Display for presenting information from the _IExchangeIdRow_.
 * @extends {xyz.swapee.wc.ExchangeIdRowDisplay}
 */
class ExchangeIdRowDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.ExchangeIdRowScreen}
 */
class ExchangeIdRowScreen extends (class {/* lazy-loaded */}) {}

module.exports.ExchangeIdRowDisplay = ExchangeIdRowDisplay
module.exports.ExchangeIdRowScreen = ExchangeIdRowScreen