/** @nocompile */
import {registerTypology} from '@type.engineering/type-engineer'

registerTypology({
 'xyz.swapee.wc.IExchangeIdRowComputer': {
  'id': 10994752271,
  'symbols': {},
  'methods': {
   'compute': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowMemoryPQs': {
  'id': 10994752272,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowOuterCore': {
  'id': 10994752273,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeIdRowInputsPQs': {
  'id': 10994752274,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowPort': {
  'id': 10994752275,
  'symbols': {},
  'methods': {
   'resetPort': 1,
   'resetExchangeIdRowPort': 2
  }
 },
 'xyz.swapee.wc.IExchangeIdRowPortInterface': {
  'id': 10994752276,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowCore': {
  'id': 10994752277,
  'symbols': {},
  'methods': {
   'resetCore': 1,
   'resetExchangeIdRowCore': 2
  }
 },
 'xyz.swapee.wc.IExchangeIdRowProcessor': {
  'id': 10994752278,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRow': {
  'id': 10994752279,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowBuffer': {
  'id': 109947522710,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil': {
  'id': 109947522711,
  'symbols': {},
  'methods': {
   'router': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowHtmlComponent': {
  'id': 109947522712,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowElement': {
  'id': 109947522713,
  'symbols': {},
  'methods': {
   'solder': 1,
   'render': 2,
   'build': 3,
   'buildExchangeIntent': 5,
   'short': 6,
   'server': 7,
   'inducer': 8
  }
 },
 'xyz.swapee.wc.IExchangeIdRowElementPort': {
  'id': 109947522714,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowDesigner': {
  'id': 109947522715,
  'symbols': {},
  'methods': {
   'borrowClasses': 1,
   'classes': 2,
   'communicator': 3,
   'relay': 4
  }
 },
 'xyz.swapee.wc.IExchangeIdRowGPU': {
  'id': 109947522716,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowDisplay': {
  'id': 109947522717,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowQueriesPQs': {
  'id': 109947522718,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.ExchangeIdRowVdusPQs': {
  'id': 109947522719,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 },
 'xyz.swapee.wc.back.IExchangeIdRowDisplay': {
  'id': 109947522720,
  'symbols': {},
  'methods': {
   'paint': 1
  }
 },
 'xyz.swapee.wc.IExchangeIdRowController': {
  'id': 109947522721,
  'symbols': {},
  'methods': {
   'resetPort': 1
  }
 },
 'xyz.swapee.wc.front.IExchangeIdRowController': {
  'id': 109947522722,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowController': {
  'id': 109947522723,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowControllerAR': {
  'id': 109947522724,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIdRowControllerAT': {
  'id': 109947522725,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.IExchangeIdRowScreen': {
  'id': 109947522726,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowScreen': {
  'id': 109947522727,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.front.IExchangeIdRowScreenAR': {
  'id': 109947522728,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.back.IExchangeIdRowScreenAT': {
  'id': 109947522729,
  'symbols': {},
  'methods': {}
 },
 'xyz.swapee.wc.ExchangeIdRowClassesPQs': {
  'id': 109947522730,
  'symbols': {},
  'methods': {
   'constructor': 1
  }
 }
})