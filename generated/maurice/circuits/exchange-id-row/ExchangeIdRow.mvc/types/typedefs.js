/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IExchangeIdRowComputer={}
xyz.swapee.wc.IExchangeIdRowComputer.compute={}
xyz.swapee.wc.IExchangeIdRowOuterCore={}
xyz.swapee.wc.IExchangeIdRowOuterCore.Model={}
xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id={}
xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed={}
xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel={}
xyz.swapee.wc.IExchangeIdRowPort={}
xyz.swapee.wc.IExchangeIdRowPort.Inputs={}
xyz.swapee.wc.IExchangeIdRowPort.WeakInputs={}
xyz.swapee.wc.IExchangeIdRowCore={}
xyz.swapee.wc.IExchangeIdRowCore.Model={}
xyz.swapee.wc.IExchangeIdRowPortInterface={}
xyz.swapee.wc.IExchangeIdRowProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IExchangeIdRowController={}
xyz.swapee.wc.front.IExchangeIdRowControllerAT={}
xyz.swapee.wc.front.IExchangeIdRowScreenAR={}
xyz.swapee.wc.IExchangeIdRow={}
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil={}
xyz.swapee.wc.IExchangeIdRowHtmlComponent={}
xyz.swapee.wc.IExchangeIdRowElement={}
xyz.swapee.wc.IExchangeIdRowElement.build={}
xyz.swapee.wc.IExchangeIdRowElement.short={}
xyz.swapee.wc.IExchangeIdRowElementPort={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts={}
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts={}
xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs={}
xyz.swapee.wc.IExchangeIdRowDesigner={}
xyz.swapee.wc.IExchangeIdRowDesigner.communicator={}
xyz.swapee.wc.IExchangeIdRowDesigner.relay={}
xyz.swapee.wc.IExchangeIdRowDisplay={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IExchangeIdRowDisplay={}
xyz.swapee.wc.back.IExchangeIdRowController={}
xyz.swapee.wc.back.IExchangeIdRowControllerAR={}
xyz.swapee.wc.back.IExchangeIdRowScreen={}
xyz.swapee.wc.back.IExchangeIdRowScreenAT={}
xyz.swapee.wc.IExchangeIdRowController={}
xyz.swapee.wc.IExchangeIdRowScreen={}
xyz.swapee.wc.IExchangeIdRowGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/02-IExchangeIdRowComputer.xml}  d722d67d74c81ca1d8c4ab578b35a7b3 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IExchangeIdRowComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowComputer)} xyz.swapee.wc.AbstractExchangeIdRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowComputer} xyz.swapee.wc.ExchangeIdRowComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowComputer` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowComputer
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowComputer.constructor&xyz.swapee.wc.ExchangeIdRowComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowComputer.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowComputer}
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowComputer}
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowComputer}
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowComputer}
 */
xyz.swapee.wc.AbstractExchangeIdRowComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowComputer.Initialese[]) => xyz.swapee.wc.IExchangeIdRowComputer} xyz.swapee.wc.ExchangeIdRowComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.ExchangeIdRowMemory>&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangeIdRowLand>)} xyz.swapee.wc.IExchangeIdRowComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IExchangeIdRowComputer
 */
xyz.swapee.wc.IExchangeIdRowComputer = class extends /** @type {xyz.swapee.wc.IExchangeIdRowComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIdRowComputer.compute} */
xyz.swapee.wc.IExchangeIdRowComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowComputer.Initialese>)} xyz.swapee.wc.ExchangeIdRowComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowComputer} xyz.swapee.wc.IExchangeIdRowComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIdRowComputer_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowComputer
 * @implements {xyz.swapee.wc.IExchangeIdRowComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowComputer.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowComputer = class extends /** @type {xyz.swapee.wc.ExchangeIdRowComputer.constructor&xyz.swapee.wc.IExchangeIdRowComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowComputer}
 */
xyz.swapee.wc.ExchangeIdRowComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIdRowComputer} */
xyz.swapee.wc.RecordIExchangeIdRowComputer

/** @typedef {xyz.swapee.wc.IExchangeIdRowComputer} xyz.swapee.wc.BoundIExchangeIdRowComputer */

/** @typedef {xyz.swapee.wc.ExchangeIdRowComputer} xyz.swapee.wc.BoundExchangeIdRowComputer */

/**
 * Contains getters to cast the _IExchangeIdRowComputer_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowComputerCaster
 */
xyz.swapee.wc.IExchangeIdRowComputerCaster = class { }
/**
 * Cast the _IExchangeIdRowComputer_ instance into the _BoundIExchangeIdRowComputer_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowComputer}
 */
xyz.swapee.wc.IExchangeIdRowComputerCaster.prototype.asIExchangeIdRowComputer
/**
 * Access the _ExchangeIdRowComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowComputer}
 */
xyz.swapee.wc.IExchangeIdRowComputerCaster.prototype.superExchangeIdRowComputer

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.ExchangeIdRowMemory, land: !xyz.swapee.wc.IExchangeIdRowComputer.compute.Land) => void} xyz.swapee.wc.IExchangeIdRowComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowComputer.__compute<!xyz.swapee.wc.IExchangeIdRowComputer>} xyz.swapee.wc.IExchangeIdRowComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.ExchangeIdRowMemory} mem The memory.
 * @param {!xyz.swapee.wc.IExchangeIdRowComputer.compute.Land} land The land.
 * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_ `.`
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowComputer.compute = function(mem, land) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowComputer.compute.Land The land.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent `.`
 */

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/03-IExchangeIdRowOuterCore.xml}  033dd537cc0087a1d608e0ef2299fed3 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeIdRowOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowOuterCore)} xyz.swapee.wc.AbstractExchangeIdRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowOuterCore} xyz.swapee.wc.ExchangeIdRowOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowOuterCore
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowOuterCore.constructor&xyz.swapee.wc.ExchangeIdRowOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowOuterCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowOuterCoreCaster)} xyz.swapee.wc.IExchangeIdRowOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IExchangeIdRow_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IExchangeIdRowOuterCore
 */
xyz.swapee.wc.IExchangeIdRowOuterCore = class extends /** @type {xyz.swapee.wc.IExchangeIdRowOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowOuterCore.prototype.constructor = xyz.swapee.wc.IExchangeIdRowOuterCore

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowOuterCore.Initialese>)} xyz.swapee.wc.ExchangeIdRowOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowOuterCore} xyz.swapee.wc.IExchangeIdRowOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIdRowOuterCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowOuterCore
 * @implements {xyz.swapee.wc.IExchangeIdRowOuterCore} The _IExchangeIdRow_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowOuterCore = class extends /** @type {xyz.swapee.wc.ExchangeIdRowOuterCore.constructor&xyz.swapee.wc.IExchangeIdRowOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIdRowOuterCore.prototype.constructor = xyz.swapee.wc.ExchangeIdRowOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowOuterCore}
 */
xyz.swapee.wc.ExchangeIdRowOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowOuterCore.
 * @interface xyz.swapee.wc.IExchangeIdRowOuterCoreFields
 */
xyz.swapee.wc.IExchangeIdRowOuterCoreFields = class { }
/**
 * The _IExchangeIdRow_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IExchangeIdRowOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeIdRowOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore} */
xyz.swapee.wc.RecordIExchangeIdRowOuterCore

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore} xyz.swapee.wc.BoundIExchangeIdRowOuterCore */

/** @typedef {xyz.swapee.wc.ExchangeIdRowOuterCore} xyz.swapee.wc.BoundExchangeIdRowOuterCore */

/**
 * The ID of the transaction.
 * @typedef {string}
 */
xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id.id

/**
 * Whether this is a fixed-rate transaction.
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed.fixed

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id&xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed} xyz.swapee.wc.IExchangeIdRowOuterCore.Model The _IExchangeIdRow_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id&xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel The _IExchangeIdRow_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IExchangeIdRowOuterCore_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowOuterCoreCaster
 */
xyz.swapee.wc.IExchangeIdRowOuterCoreCaster = class { }
/**
 * Cast the _IExchangeIdRowOuterCore_ instance into the _BoundIExchangeIdRowOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowOuterCore}
 */
xyz.swapee.wc.IExchangeIdRowOuterCoreCaster.prototype.asIExchangeIdRowOuterCore
/**
 * Access the _ExchangeIdRowOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowOuterCore}
 */
xyz.swapee.wc.IExchangeIdRowOuterCoreCaster.prototype.superExchangeIdRowOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id The ID of the transaction (optional overlay).
 * @prop {string} [id=""] The ID of the transaction. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id_Safe The ID of the transaction (required overlay).
 * @prop {string} id The ID of the transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed Whether this is a fixed-rate transaction (optional overlay).
 * @prop {boolean} [fixed=null] Whether this is a fixed-rate transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed_Safe Whether this is a fixed-rate transaction (required overlay).
 * @prop {boolean} fixed Whether this is a fixed-rate transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id The ID of the transaction (optional overlay).
 * @prop {*} [id=null] The ID of the transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id_Safe The ID of the transaction (required overlay).
 * @prop {*} id The ID of the transaction.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed Whether this is a fixed-rate transaction (optional overlay).
 * @prop {*} [fixed=null] Whether this is a fixed-rate transaction. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed_Safe Whether this is a fixed-rate transaction (required overlay).
 * @prop {*} fixed Whether this is a fixed-rate transaction.
 */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id} xyz.swapee.wc.IExchangeIdRowPort.Inputs.Id The ID of the transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id_Safe} xyz.swapee.wc.IExchangeIdRowPort.Inputs.Id_Safe The ID of the transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed} xyz.swapee.wc.IExchangeIdRowPort.Inputs.Fixed Whether this is a fixed-rate transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.IExchangeIdRowPort.Inputs.Fixed_Safe Whether this is a fixed-rate transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id} xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.Id The ID of the transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Id_Safe} xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.Id_Safe The ID of the transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed} xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.Fixed Whether this is a fixed-rate transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.Fixed_Safe} xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.Fixed_Safe Whether this is a fixed-rate transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id} xyz.swapee.wc.IExchangeIdRowCore.Model.Id The ID of the transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Id_Safe} xyz.swapee.wc.IExchangeIdRowCore.Model.Id_Safe The ID of the transaction (required overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed} xyz.swapee.wc.IExchangeIdRowCore.Model.Fixed Whether this is a fixed-rate transaction (optional overlay). */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model.Fixed_Safe} xyz.swapee.wc.IExchangeIdRowCore.Model.Fixed_Safe Whether this is a fixed-rate transaction (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/04-IExchangeIdRowPort.xml}  7604640a33148518957c43cf4b2bd719 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeIdRowPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowPort)} xyz.swapee.wc.AbstractExchangeIdRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowPort} xyz.swapee.wc.ExchangeIdRowPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowPort
 */
xyz.swapee.wc.AbstractExchangeIdRowPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowPort.constructor&xyz.swapee.wc.ExchangeIdRowPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowPort|typeof xyz.swapee.wc.ExchangeIdRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowPort|typeof xyz.swapee.wc.ExchangeIdRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowPort|typeof xyz.swapee.wc.ExchangeIdRowPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowPort.Initialese[]) => xyz.swapee.wc.IExchangeIdRowPort} xyz.swapee.wc.ExchangeIdRowPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeIdRowPort.Inputs>)} xyz.swapee.wc.IExchangeIdRowPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IExchangeIdRow_, providing input
 * pins.
 * @interface xyz.swapee.wc.IExchangeIdRowPort
 */
xyz.swapee.wc.IExchangeIdRowPort = class extends /** @type {xyz.swapee.wc.IExchangeIdRowPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIdRowPort.resetPort} */
xyz.swapee.wc.IExchangeIdRowPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowPort.resetExchangeIdRowPort} */
xyz.swapee.wc.IExchangeIdRowPort.prototype.resetExchangeIdRowPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowPort.Initialese>)} xyz.swapee.wc.ExchangeIdRowPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowPort} xyz.swapee.wc.IExchangeIdRowPort.typeof */
/**
 * A concrete class of _IExchangeIdRowPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowPort
 * @implements {xyz.swapee.wc.IExchangeIdRowPort} The port that serves as an interface to the _IExchangeIdRow_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowPort = class extends /** @type {xyz.swapee.wc.ExchangeIdRowPort.constructor&xyz.swapee.wc.IExchangeIdRowPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowPort}
 */
xyz.swapee.wc.ExchangeIdRowPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowPort.
 * @interface xyz.swapee.wc.IExchangeIdRowPortFields
 */
xyz.swapee.wc.IExchangeIdRowPortFields = class { }
/**
 * The inputs to the _IExchangeIdRow_'s controller via its port.
 */
xyz.swapee.wc.IExchangeIdRowPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIdRowPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeIdRowPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeIdRowPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowPort} */
xyz.swapee.wc.RecordIExchangeIdRowPort

/** @typedef {xyz.swapee.wc.IExchangeIdRowPort} xyz.swapee.wc.BoundIExchangeIdRowPort */

/** @typedef {xyz.swapee.wc.ExchangeIdRowPort} xyz.swapee.wc.BoundExchangeIdRowPort */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel)} xyz.swapee.wc.IExchangeIdRowPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel} xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IExchangeIdRow_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeIdRowPort.Inputs
 */
xyz.swapee.wc.IExchangeIdRowPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeIdRowPort.Inputs.constructor&xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeIdRowPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel)} xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.constructor */
/**
 * The inputs to the _IExchangeIdRow_'s controller via its port.
 * @record xyz.swapee.wc.IExchangeIdRowPort.WeakInputs
 */
xyz.swapee.wc.IExchangeIdRowPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeIdRowPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IExchangeIdRowPortInterface
 */
xyz.swapee.wc.IExchangeIdRowPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IExchangeIdRowPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IExchangeIdRowPortInterface.prototype.constructor = xyz.swapee.wc.IExchangeIdRowPortInterface

/**
 * A concrete class of _IExchangeIdRowPortInterface_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowPortInterface
 * @implements {xyz.swapee.wc.IExchangeIdRowPortInterface} The port interface.
 */
xyz.swapee.wc.ExchangeIdRowPortInterface = class extends xyz.swapee.wc.IExchangeIdRowPortInterface { }
xyz.swapee.wc.ExchangeIdRowPortInterface.prototype.constructor = xyz.swapee.wc.ExchangeIdRowPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowPortInterface.Props
 * @prop {string} id The ID of the transaction.
 * @prop {boolean} [fixed=null] Whether this is a fixed-rate transaction. Default `null`.
 */

/**
 * Contains getters to cast the _IExchangeIdRowPort_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowPortCaster
 */
xyz.swapee.wc.IExchangeIdRowPortCaster = class { }
/**
 * Cast the _IExchangeIdRowPort_ instance into the _BoundIExchangeIdRowPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowPort}
 */
xyz.swapee.wc.IExchangeIdRowPortCaster.prototype.asIExchangeIdRowPort
/**
 * Access the _ExchangeIdRowPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowPort}
 */
xyz.swapee.wc.IExchangeIdRowPortCaster.prototype.superExchangeIdRowPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIdRowPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowPort.__resetPort<!xyz.swapee.wc.IExchangeIdRowPort>} xyz.swapee.wc.IExchangeIdRowPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowPort.resetPort} */
/**
 * Resets the _IExchangeIdRow_ port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIdRowPort.__resetExchangeIdRowPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowPort.__resetExchangeIdRowPort<!xyz.swapee.wc.IExchangeIdRowPort>} xyz.swapee.wc.IExchangeIdRowPort._resetExchangeIdRowPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowPort.resetExchangeIdRowPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowPort.resetExchangeIdRowPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/09-IExchangeIdRowCore.xml}  f66511040fc2b984cd0d95472cf316f6 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IExchangeIdRowCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowCore)} xyz.swapee.wc.AbstractExchangeIdRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowCore} xyz.swapee.wc.ExchangeIdRowCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowCore` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowCore
 */
xyz.swapee.wc.AbstractExchangeIdRowCore = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowCore.constructor&xyz.swapee.wc.ExchangeIdRowCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowCore.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowOuterCore|typeof xyz.swapee.wc.ExchangeIdRowOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowCore}
 */
xyz.swapee.wc.AbstractExchangeIdRowCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowCoreCaster&xyz.swapee.wc.IExchangeIdRowOuterCore)} xyz.swapee.wc.IExchangeIdRowCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IExchangeIdRowCore
 */
xyz.swapee.wc.IExchangeIdRowCore = class extends /** @type {xyz.swapee.wc.IExchangeIdRowCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIdRowOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangeIdRowCore.resetCore} */
xyz.swapee.wc.IExchangeIdRowCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowCore.resetExchangeIdRowCore} */
xyz.swapee.wc.IExchangeIdRowCore.prototype.resetExchangeIdRowCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowCore&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowCore.Initialese>)} xyz.swapee.wc.ExchangeIdRowCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowCore} xyz.swapee.wc.IExchangeIdRowCore.typeof */
/**
 * A concrete class of _IExchangeIdRowCore_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowCore
 * @implements {xyz.swapee.wc.IExchangeIdRowCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowCore.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowCore = class extends /** @type {xyz.swapee.wc.ExchangeIdRowCore.constructor&xyz.swapee.wc.IExchangeIdRowCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIdRowCore.prototype.constructor = xyz.swapee.wc.ExchangeIdRowCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowCore}
 */
xyz.swapee.wc.ExchangeIdRowCore.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowCore.
 * @interface xyz.swapee.wc.IExchangeIdRowCoreFields
 */
xyz.swapee.wc.IExchangeIdRowCoreFields = class { }
/**
 * The _IExchangeIdRow_'s memory.
 */
xyz.swapee.wc.IExchangeIdRowCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IExchangeIdRowCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IExchangeIdRowCoreFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeIdRowCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowCore} */
xyz.swapee.wc.RecordIExchangeIdRowCore

/** @typedef {xyz.swapee.wc.IExchangeIdRowCore} xyz.swapee.wc.BoundIExchangeIdRowCore */

/** @typedef {xyz.swapee.wc.ExchangeIdRowCore} xyz.swapee.wc.BoundExchangeIdRowCore */

/** @typedef {xyz.swapee.wc.IExchangeIdRowOuterCore.Model} xyz.swapee.wc.IExchangeIdRowCore.Model The _IExchangeIdRow_'s memory. */

/**
 * Contains getters to cast the _IExchangeIdRowCore_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowCoreCaster
 */
xyz.swapee.wc.IExchangeIdRowCoreCaster = class { }
/**
 * Cast the _IExchangeIdRowCore_ instance into the _BoundIExchangeIdRowCore_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowCore}
 */
xyz.swapee.wc.IExchangeIdRowCoreCaster.prototype.asIExchangeIdRowCore
/**
 * Access the _ExchangeIdRowCore_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowCore}
 */
xyz.swapee.wc.IExchangeIdRowCoreCaster.prototype.superExchangeIdRowCore

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIdRowCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowCore.__resetCore<!xyz.swapee.wc.IExchangeIdRowCore>} xyz.swapee.wc.IExchangeIdRowCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowCore.resetCore} */
/**
 * Resets the _IExchangeIdRow_ core.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIdRowCore.__resetExchangeIdRowCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowCore.__resetExchangeIdRowCore<!xyz.swapee.wc.IExchangeIdRowCore>} xyz.swapee.wc.IExchangeIdRowCore._resetExchangeIdRowCore */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowCore.resetExchangeIdRowCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowCore.resetExchangeIdRowCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/10-IExchangeIdRowProcessor.xml}  95c395fa2be759e16a70d168e709ed02 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowComputer.Initialese&xyz.swapee.wc.IExchangeIdRowController.Initialese} xyz.swapee.wc.IExchangeIdRowProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowProcessor)} xyz.swapee.wc.AbstractExchangeIdRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowProcessor} xyz.swapee.wc.ExchangeIdRowProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowProcessor
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowProcessor.constructor&xyz.swapee.wc.ExchangeIdRowProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowProcessor.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowProcessor}
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowProcessor}
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowProcessor}
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowCore|typeof xyz.swapee.wc.ExchangeIdRowCore)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowProcessor}
 */
xyz.swapee.wc.AbstractExchangeIdRowProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowProcessor.Initialese[]) => xyz.swapee.wc.IExchangeIdRowProcessor} xyz.swapee.wc.ExchangeIdRowProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowProcessorCaster&xyz.swapee.wc.IExchangeIdRowComputer&xyz.swapee.wc.IExchangeIdRowCore&xyz.swapee.wc.IExchangeIdRowController)} xyz.swapee.wc.IExchangeIdRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowComputer} xyz.swapee.wc.IExchangeIdRowComputer.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowCore} xyz.swapee.wc.IExchangeIdRowCore.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowController} xyz.swapee.wc.IExchangeIdRowController.typeof */
/**
 * The processor to compute changes to the memory for the _IExchangeIdRow_.
 * @interface xyz.swapee.wc.IExchangeIdRowProcessor
 */
xyz.swapee.wc.IExchangeIdRowProcessor = class extends /** @type {xyz.swapee.wc.IExchangeIdRowProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIdRowComputer.typeof&xyz.swapee.wc.IExchangeIdRowCore.typeof&xyz.swapee.wc.IExchangeIdRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowProcessor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowProcessor.Initialese>)} xyz.swapee.wc.ExchangeIdRowProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowProcessor} xyz.swapee.wc.IExchangeIdRowProcessor.typeof */
/**
 * A concrete class of _IExchangeIdRowProcessor_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowProcessor
 * @implements {xyz.swapee.wc.IExchangeIdRowProcessor} The processor to compute changes to the memory for the _IExchangeIdRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowProcessor.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowProcessor = class extends /** @type {xyz.swapee.wc.ExchangeIdRowProcessor.constructor&xyz.swapee.wc.IExchangeIdRowProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowProcessor}
 */
xyz.swapee.wc.ExchangeIdRowProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIdRowProcessor} */
xyz.swapee.wc.RecordIExchangeIdRowProcessor

/** @typedef {xyz.swapee.wc.IExchangeIdRowProcessor} xyz.swapee.wc.BoundIExchangeIdRowProcessor */

/** @typedef {xyz.swapee.wc.ExchangeIdRowProcessor} xyz.swapee.wc.BoundExchangeIdRowProcessor */

/**
 * Contains getters to cast the _IExchangeIdRowProcessor_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowProcessorCaster
 */
xyz.swapee.wc.IExchangeIdRowProcessorCaster = class { }
/**
 * Cast the _IExchangeIdRowProcessor_ instance into the _BoundIExchangeIdRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowProcessor}
 */
xyz.swapee.wc.IExchangeIdRowProcessorCaster.prototype.asIExchangeIdRowProcessor
/**
 * Access the _ExchangeIdRowProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowProcessor}
 */
xyz.swapee.wc.IExchangeIdRowProcessorCaster.prototype.superExchangeIdRowProcessor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/100-ExchangeIdRowMemory.xml}  2ec58742c25e1c393cb8b44ab88d24db */
/**
 * The memory of the _IExchangeIdRow_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.ExchangeIdRowMemory
 */
xyz.swapee.wc.ExchangeIdRowMemory = class { }
/**
 * The ID of the transaction. Default empty string.
 */
xyz.swapee.wc.ExchangeIdRowMemory.prototype.id = /** @type {string} */ (void 0)
/**
 * Whether this is a fixed-rate transaction. Default `false`.
 */
xyz.swapee.wc.ExchangeIdRowMemory.prototype.fixed = /** @type {boolean} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/102-ExchangeIdRowInputs.xml}  198531b7c46d434c3d3cd2ca35d196f0 */
/**
 * The inputs of the _IExchangeIdRow_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.ExchangeIdRowInputs
 */
xyz.swapee.wc.front.ExchangeIdRowInputs = class { }
/**
 * The ID of the transaction. Default empty string.
 */
xyz.swapee.wc.front.ExchangeIdRowInputs.prototype.id = /** @type {string|undefined} */ (void 0)
/**
 * Whether this is a fixed-rate transaction. Default `null`.
 */
xyz.swapee.wc.front.ExchangeIdRowInputs.prototype.fixed = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/11-IExchangeIdRow.xml}  b095ff61de90f194a663fa7f93344fe0 */
/**
 * An atomic wrapper for the _IExchangeIdRow_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.ExchangeIdRowEnv
 */
xyz.swapee.wc.ExchangeIdRowEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.ExchangeIdRowEnv.prototype.exchangeIdRow = /** @type {xyz.swapee.wc.IExchangeIdRow} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowController.Inputs>&xyz.swapee.wc.IExchangeIdRowProcessor.Initialese&xyz.swapee.wc.IExchangeIdRowComputer.Initialese&xyz.swapee.wc.IExchangeIdRowController.Initialese} xyz.swapee.wc.IExchangeIdRow.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRow)} xyz.swapee.wc.AbstractExchangeIdRow.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRow} xyz.swapee.wc.ExchangeIdRow.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRow` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRow
 */
xyz.swapee.wc.AbstractExchangeIdRow = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRow.constructor&xyz.swapee.wc.ExchangeIdRow.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRow.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRow
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRow.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRow} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRow}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRow.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRow}
 */
xyz.swapee.wc.AbstractExchangeIdRow.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRow}
 */
xyz.swapee.wc.AbstractExchangeIdRow.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRow}
 */
xyz.swapee.wc.AbstractExchangeIdRow.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRow}
 */
xyz.swapee.wc.AbstractExchangeIdRow.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRow.Initialese[]) => xyz.swapee.wc.IExchangeIdRow} xyz.swapee.wc.ExchangeIdRowConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRow.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IExchangeIdRow.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IExchangeIdRow.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IExchangeIdRow.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.ExchangeIdRowClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowCaster&xyz.swapee.wc.IExchangeIdRowProcessor&xyz.swapee.wc.IExchangeIdRowComputer&xyz.swapee.wc.IExchangeIdRowController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowController.Inputs, !xyz.swapee.wc.ExchangeIdRowLand>)} xyz.swapee.wc.IExchangeIdRow.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IExchangeIdRow
 */
xyz.swapee.wc.IExchangeIdRow = class extends /** @type {xyz.swapee.wc.IExchangeIdRow.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIdRowProcessor.typeof&xyz.swapee.wc.IExchangeIdRowComputer.typeof&xyz.swapee.wc.IExchangeIdRowController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRow.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRow&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRow.Initialese>)} xyz.swapee.wc.ExchangeIdRow.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRow} xyz.swapee.wc.IExchangeIdRow.typeof */
/**
 * A concrete class of _IExchangeIdRow_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRow
 * @implements {xyz.swapee.wc.IExchangeIdRow} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRow.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRow = class extends /** @type {xyz.swapee.wc.ExchangeIdRow.constructor&xyz.swapee.wc.IExchangeIdRow.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRow* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRow.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRow* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRow.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRow.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRow}
 */
xyz.swapee.wc.ExchangeIdRow.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRow.
 * @interface xyz.swapee.wc.IExchangeIdRowFields
 */
xyz.swapee.wc.IExchangeIdRowFields = class { }
/**
 * The input pins of the _IExchangeIdRow_ port.
 */
xyz.swapee.wc.IExchangeIdRowFields.prototype.pinout = /** @type {!xyz.swapee.wc.IExchangeIdRow.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRow} */
xyz.swapee.wc.RecordIExchangeIdRow

/** @typedef {xyz.swapee.wc.IExchangeIdRow} xyz.swapee.wc.BoundIExchangeIdRow */

/** @typedef {xyz.swapee.wc.ExchangeIdRow} xyz.swapee.wc.BoundExchangeIdRow */

/** @typedef {xyz.swapee.wc.IExchangeIdRowController.Inputs} xyz.swapee.wc.IExchangeIdRow.Pinout The input pins of the _IExchangeIdRow_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeIdRowController.Inputs>)} xyz.swapee.wc.IExchangeIdRowBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IExchangeIdRowBuffer
 */
xyz.swapee.wc.IExchangeIdRowBuffer = class extends /** @type {xyz.swapee.wc.IExchangeIdRowBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowBuffer.prototype.constructor = xyz.swapee.wc.IExchangeIdRowBuffer

/**
 * A concrete class of _IExchangeIdRowBuffer_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowBuffer
 * @implements {xyz.swapee.wc.IExchangeIdRowBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.ExchangeIdRowBuffer = class extends xyz.swapee.wc.IExchangeIdRowBuffer { }
xyz.swapee.wc.ExchangeIdRowBuffer.prototype.constructor = xyz.swapee.wc.ExchangeIdRowBuffer

/**
 * Contains getters to cast the _IExchangeIdRow_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowCaster
 */
xyz.swapee.wc.IExchangeIdRowCaster = class { }
/**
 * Cast the _IExchangeIdRow_ instance into the _BoundIExchangeIdRow_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRow}
 */
xyz.swapee.wc.IExchangeIdRowCaster.prototype.asIExchangeIdRow
/**
 * Access the _ExchangeIdRow_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRow}
 */
xyz.swapee.wc.IExchangeIdRowCaster.prototype.superExchangeIdRow

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/110-ExchangeIdRowSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIdRowMemoryPQs
 */
xyz.swapee.wc.ExchangeIdRowMemoryPQs = class {
  constructor() {
    /**
     * `a74ad`
     */
    this.core=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeIdRowMemoryPQs.prototype.constructor = xyz.swapee.wc.ExchangeIdRowMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIdRowMemoryQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIdRowMemoryQPs = class { }
/**
 * `core`
 */
xyz.swapee.wc.ExchangeIdRowMemoryQPs.prototype.a74ad = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowMemoryPQs)} xyz.swapee.wc.ExchangeIdRowInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowMemoryPQs} xyz.swapee.wc.ExchangeIdRowMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIdRowInputsPQs
 */
xyz.swapee.wc.ExchangeIdRowInputsPQs = class extends /** @type {xyz.swapee.wc.ExchangeIdRowInputsPQs.constructor&xyz.swapee.wc.ExchangeIdRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIdRowInputsPQs.prototype.constructor = xyz.swapee.wc.ExchangeIdRowInputsPQs

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowMemoryPQs)} xyz.swapee.wc.ExchangeIdRowInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIdRowInputsQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIdRowInputsQPs = class extends /** @type {xyz.swapee.wc.ExchangeIdRowInputsQPs.constructor&xyz.swapee.wc.ExchangeIdRowMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.ExchangeIdRowInputsQPs.prototype.constructor = xyz.swapee.wc.ExchangeIdRowInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIdRowQueriesPQs
 */
xyz.swapee.wc.ExchangeIdRowQueriesPQs = class {
  constructor() {
    /**
     * `ecfe5`
     */
    this.exchangeBrokerSel=/** @type {string} */ (void 0)
    /**
     * `b3da4`
     */
    this.exchangeIntentSel=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeIdRowQueriesPQs.prototype.constructor = xyz.swapee.wc.ExchangeIdRowQueriesPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIdRowQueriesQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIdRowQueriesQPs = class { }
/**
 * `exchangeBrokerSel`
 */
xyz.swapee.wc.ExchangeIdRowQueriesQPs.prototype.ecfe5 = /** @type {string} */ (void 0)
/**
 * `exchangeIntentSel`
 */
xyz.swapee.wc.ExchangeIdRowQueriesQPs.prototype.b3da4 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.ExchangeIdRowVdusPQs
 */
xyz.swapee.wc.ExchangeIdRowVdusPQs = class {
  constructor() {
    /**
     * `e02a1`
     */
    this.ExchangeIdLa=/** @type {string} */ (void 0)
    /**
     * `e02a2`
     */
    this.ExchangeIdFloatingLa=/** @type {string} */ (void 0)
    /**
     * `e02a3`
     */
    this.ExchangeIdFixedLa=/** @type {string} */ (void 0)
    /**
     * `e02a4`
     */
    this.ExchangeBroker=/** @type {string} */ (void 0)
    /**
     * `e02a5`
     */
    this.ExchangeIntent=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.ExchangeIdRowVdusPQs.prototype.constructor = xyz.swapee.wc.ExchangeIdRowVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.ExchangeIdRowVdusQPs
 * @dict
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs = class { }
/**
 * `ExchangeIdLa`
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs.prototype.e02a1 = /** @type {string} */ (void 0)
/**
 * `ExchangeIdFloatingLa`
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs.prototype.e02a2 = /** @type {string} */ (void 0)
/**
 * `ExchangeIdFixedLa`
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs.prototype.e02a3 = /** @type {string} */ (void 0)
/**
 * `ExchangeBroker`
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs.prototype.e02a4 = /** @type {string} */ (void 0)
/**
 * `ExchangeIntent`
 */
xyz.swapee.wc.ExchangeIdRowVdusQPs.prototype.e02a5 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/12-IExchangeIdRowHtmlComponent.element.xml}   */
/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowHtmlComponentUtilFields)} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.constructor */
/** @interface xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil = class extends /** @type {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.constructor} */ (class {}) { }
/** @type {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.router} */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.prototype.router = function() {}

/**
 * A concrete class of _IExchangeIdRowHtmlComponentUtil_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowHtmlComponentUtil
 * @implements {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil} ‎
 */
xyz.swapee.wc.ExchangeIdRowHtmlComponentUtil = class extends xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil { }
xyz.swapee.wc.ExchangeIdRowHtmlComponentUtil.prototype.constructor = xyz.swapee.wc.ExchangeIdRowHtmlComponentUtil

/**
 * Fields of the IExchangeIdRowHtmlComponentUtil.
 * @interface xyz.swapee.wc.IExchangeIdRowHtmlComponentUtilFields
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtilFields = class { }
/**
 * </arc>
 *     <arc name="RouterCores" type="xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterCores">
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtilFields.prototype.RouterNet = /** @type {!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterNet} */ (void 0)
/**
 *
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtilFields.prototype.RouterPorts = /** @type {!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterPorts} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil} */
xyz.swapee.wc.RecordIExchangeIdRowHtmlComponentUtil

/** @typedef {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil} xyz.swapee.wc.BoundIExchangeIdRowHtmlComponentUtil */

/** @typedef {xyz.swapee.wc.ExchangeIdRowHtmlComponentUtil} xyz.swapee.wc.BoundExchangeIdRowHtmlComponentUtil */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterNet
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPort} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowPort} ExchangeIdRow The root component that encapsulates the routed net.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterCores
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} ExchangeIdRow
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterPorts
 * @prop {!xyz.swapee.wc.IExchangeIntent.Pinout} ExchangeIntent
 * @prop {!xyz.swapee.wc.IExchangeIdRow.Pinout} ExchangeIdRow
 */

/**
 * @typedef {(this: THIS, net?: !xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterNet, cores?: !xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterCores, ports?: !xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterPorts) => ?} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.__router
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.__router<!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil>} xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil._router */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.router} */
/**
 * Allows to route the connectors.
 * @param {!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterNet} [net] Components on the net properties of which can be routed.
 * - `ExchangeIntent` _typeof IExchangeIntentPort_
 * - `ExchangeIdRow` _typeof IExchangeIdRowPort_ The root component that encapsulates the routed net.
 * @param {!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterCores} [cores] Provides access to model values of router components on the net.
 * - `ExchangeIntent` _!ExchangeIntentMemory_
 * - `ExchangeIdRow` _!ExchangeIdRowMemory_
 * @param {!xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.RouterPorts} [ports] Provides access to port values of the components on the net.
 * - `ExchangeIntent` _!IExchangeIntent.Pinout_
 * - `ExchangeIdRow` _!IExchangeIdRow.Pinout_
 * @return {?}
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil.router = function(net, cores, ports) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowHtmlComponentUtil
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/12-IExchangeIdRowHtmlComponent.xml}  04ee0faf1ab298033b06610ac4fd7005 */
/** @typedef {xyz.swapee.wc.back.IExchangeIdRowController.Initialese&xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese&xyz.swapee.wc.IExchangeIdRow.Initialese&com.webcircuits.ILanded.Initialese&xyz.swapee.wc.IExchangeIdRowGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IExchangeIdRowProcessor.Initialese&xyz.swapee.wc.IExchangeIdRowComputer.Initialese} xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowHtmlComponent)} xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent} xyz.swapee.wc.ExchangeIdRowHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.constructor&xyz.swapee.wc.ExchangeIdRowHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowHtmlComponent|typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowHtmlComponent|typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowHtmlComponent|typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent)|(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.IExchangeIdRow|typeof xyz.swapee.wc.ExchangeIdRow)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IExchangeIdRowProcessor|typeof xyz.swapee.wc.ExchangeIdRowProcessor)|(!xyz.swapee.wc.IExchangeIdRowComputer|typeof xyz.swapee.wc.ExchangeIdRowComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese[]) => xyz.swapee.wc.IExchangeIdRowHtmlComponent} xyz.swapee.wc.ExchangeIdRowHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowHtmlComponentCaster&xyz.swapee.wc.back.IExchangeIdRowController&xyz.swapee.wc.back.IExchangeIdRowScreen&xyz.swapee.wc.IExchangeIdRow&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangeIdRowLand>&xyz.swapee.wc.IExchangeIdRowGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowController.Inputs, !HTMLDivElement, !xyz.swapee.wc.ExchangeIdRowLand>&xyz.swapee.wc.IExchangeIdRowProcessor&xyz.swapee.wc.IExchangeIdRowComputer)} xyz.swapee.wc.IExchangeIdRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowController} xyz.swapee.wc.back.IExchangeIdRowController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowScreen} xyz.swapee.wc.back.IExchangeIdRowScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRow} xyz.swapee.wc.IExchangeIdRow.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowGPU} xyz.swapee.wc.IExchangeIdRowGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IExchangeIdRow_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IExchangeIdRowHtmlComponent
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponent = class extends /** @type {xyz.swapee.wc.IExchangeIdRowHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeIdRowController.typeof&xyz.swapee.wc.back.IExchangeIdRowScreen.typeof&xyz.swapee.wc.IExchangeIdRow.typeof&com.webcircuits.ILanded.typeof&xyz.swapee.wc.IExchangeIdRowGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IExchangeIdRowProcessor.typeof&xyz.swapee.wc.IExchangeIdRowComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese>)} xyz.swapee.wc.ExchangeIdRowHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowHtmlComponent} xyz.swapee.wc.IExchangeIdRowHtmlComponent.typeof */
/**
 * A concrete class of _IExchangeIdRowHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowHtmlComponent
 * @implements {xyz.swapee.wc.IExchangeIdRowHtmlComponent} The _IExchangeIdRow_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowHtmlComponent = class extends /** @type {xyz.swapee.wc.ExchangeIdRowHtmlComponent.constructor&xyz.swapee.wc.IExchangeIdRowHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.ExchangeIdRowHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIdRowHtmlComponent} */
xyz.swapee.wc.RecordIExchangeIdRowHtmlComponent

/** @typedef {xyz.swapee.wc.IExchangeIdRowHtmlComponent} xyz.swapee.wc.BoundIExchangeIdRowHtmlComponent */

/** @typedef {xyz.swapee.wc.ExchangeIdRowHtmlComponent} xyz.swapee.wc.BoundExchangeIdRowHtmlComponent */

/**
 * Contains getters to cast the _IExchangeIdRowHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowHtmlComponentCaster
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentCaster = class { }
/**
 * Cast the _IExchangeIdRowHtmlComponent_ instance into the _BoundIExchangeIdRowHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentCaster.prototype.asIExchangeIdRowHtmlComponent
/**
 * Access the _ExchangeIdRowHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowHtmlComponent}
 */
xyz.swapee.wc.IExchangeIdRowHtmlComponentCaster.prototype.superExchangeIdRowHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/130-IExchangeIdRowElement.xml}  5e48ad6808a786a2b962d6ace38d836b */
/** @typedef {com.webcircuits.ILanded.Initialese<!xyz.swapee.wc.ExchangeIdRowLand>&guest.maurice.IMilleu.Initialese<!xyz.swapee.wc.IExchangeIntent>&guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IExchangeIdRowElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowElement)} xyz.swapee.wc.AbstractExchangeIdRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowElement} xyz.swapee.wc.ExchangeIdRowElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowElement` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowElement
 */
xyz.swapee.wc.AbstractExchangeIdRowElement = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowElement.constructor&xyz.swapee.wc.ExchangeIdRowElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowElement.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElement|typeof xyz.swapee.wc.ExchangeIdRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowElement}
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElement}
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElement|typeof xyz.swapee.wc.ExchangeIdRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElement}
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElement|typeof xyz.swapee.wc.ExchangeIdRowElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)|(!guest.maurice.IMilleu|typeof guest.maurice.Milleu)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElement}
 */
xyz.swapee.wc.AbstractExchangeIdRowElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowElement.Initialese[]) => xyz.swapee.wc.IExchangeIdRowElement} xyz.swapee.wc.ExchangeIdRowElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElementFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.IExchangeIdRowElement.Inputs, !xyz.swapee.wc.ExchangeIdRowLand>&com.webcircuits.ILanded<!xyz.swapee.wc.ExchangeIdRowLand>&guest.maurice.IMilleu<!xyz.swapee.wc.IExchangeIntent>)} xyz.swapee.wc.IExchangeIdRowElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/** @typedef {typeof guest.maurice.IMilleu} guest.maurice.IMilleu.typeof */
/**
 * A component description.
 *
 * The _IExchangeIdRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IExchangeIdRowElement
 */
xyz.swapee.wc.IExchangeIdRowElement = class extends /** @type {xyz.swapee.wc.IExchangeIdRowElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof&com.webcircuits.ILanded.typeof&guest.maurice.IMilleu.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.solder} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.render} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.build} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.build = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.buildExchangeIntent} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.buildExchangeIntent = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.short} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.short = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.server} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IExchangeIdRowElement.inducer} */
xyz.swapee.wc.IExchangeIdRowElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElement&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowElement.Initialese>)} xyz.swapee.wc.ExchangeIdRowElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement} xyz.swapee.wc.IExchangeIdRowElement.typeof */
/**
 * A concrete class of _IExchangeIdRowElement_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowElement
 * @implements {xyz.swapee.wc.IExchangeIdRowElement} A component description.
 *
 * The _IExchangeIdRow_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowElement.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowElement = class extends /** @type {xyz.swapee.wc.ExchangeIdRowElement.constructor&xyz.swapee.wc.IExchangeIdRowElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElement}
 */
xyz.swapee.wc.ExchangeIdRowElement.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowElement.
 * @interface xyz.swapee.wc.IExchangeIdRowElementFields
 */
xyz.swapee.wc.IExchangeIdRowElementFields = class { }
/**
 * The element-specific inputs to the _IExchangeIdRow_ component.
 */
xyz.swapee.wc.IExchangeIdRowElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIdRowElement.Inputs} */ (void 0)
/**
 * Holds the ids of component constructors for buildable items in the land.
 */
xyz.swapee.wc.IExchangeIdRowElementFields.prototype.buildees = /** @type {!Object<string, !Object<string, number>>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowElement} */
xyz.swapee.wc.RecordIExchangeIdRowElement

/** @typedef {xyz.swapee.wc.IExchangeIdRowElement} xyz.swapee.wc.BoundIExchangeIdRowElement */

/** @typedef {xyz.swapee.wc.ExchangeIdRowElement} xyz.swapee.wc.BoundExchangeIdRowElement */

/** @typedef {xyz.swapee.wc.IExchangeIdRowPort.Inputs&xyz.swapee.wc.IExchangeIdRowDisplay.Queries&xyz.swapee.wc.IExchangeIdRowController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs} xyz.swapee.wc.IExchangeIdRowElement.Inputs The element-specific inputs to the _IExchangeIdRow_ component. */

/**
 * Contains getters to cast the _IExchangeIdRowElement_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowElementCaster
 */
xyz.swapee.wc.IExchangeIdRowElementCaster = class { }
/**
 * Cast the _IExchangeIdRowElement_ instance into the _BoundIExchangeIdRowElement_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowElement}
 */
xyz.swapee.wc.IExchangeIdRowElementCaster.prototype.asIExchangeIdRowElement
/**
 * Access the _ExchangeIdRowElement_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowElement}
 */
xyz.swapee.wc.IExchangeIdRowElementCaster.prototype.superExchangeIdRowElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangeIdRowMemory, props: !xyz.swapee.wc.IExchangeIdRowElement.Inputs) => Object<string, *>} xyz.swapee.wc.IExchangeIdRowElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__solder<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._solder */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} model The model.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `getTransaction` _boolean_ Retrieves the transaction. Default `false`.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.Inputs} props The element props.
 * - `[id=null]` _&#42;?_ The ID of the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* Default `null`.
 * - `[getTransaction=null]` _&#42;?_ Retrieves the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* Default `null`.
 * - `[fixed=null]` _&#42;?_ Whether this is a fixed-rate transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangeIdRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIdRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIdLaOpts]` _!Object?_ The options to pass to the _ExchangeIdLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts* Default `{}`.
 * - `[exchangeIdFloatingLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFloatingLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts* Default `{}`.
 * - `[exchangeIdFixedLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFixedLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IExchangeIdRowElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeIdRowMemory, instance?: !xyz.swapee.wc.IExchangeIdRowScreen&xyz.swapee.wc.IExchangeIdRowController) => !engineering.type.VNode} xyz.swapee.wc.IExchangeIdRowElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__render<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._render */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} [model] The model for the view.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `getTransaction` _boolean_ Retrieves the transaction. Default `false`.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.IExchangeIdRowScreen&xyz.swapee.wc.IExchangeIdRowController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeIdRowElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, cores: !xyz.swapee.wc.IExchangeIdRowElement.build.Cores, instances: !xyz.swapee.wc.IExchangeIdRowElement.build.Instances) => ?} xyz.swapee.wc.IExchangeIdRowElement.__build
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__build<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._build */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.build} */
/**
 * Controls the VDUs using other components on the land.
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.build.Cores} cores The models of components on the land.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntentCore.Model_
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.build.Instances} instances The instances of other components on the land to invoke their.
 * - `ExchangeIntent` _!xyz.swapee.wc.IExchangeIntent_
 * @return {?}
 */
xyz.swapee.wc.IExchangeIdRowElement.build = function(cores, instances) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElement.build.Cores The models of components on the land.
 * @prop {!xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElement.build.Instances The instances of other components on the land to invoke their.
 * @prop {!xyz.swapee.wc.IExchangeIntent} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.IExchangeIntentCore.Model, instance: !xyz.swapee.wc.IExchangeIntent) => ?} xyz.swapee.wc.IExchangeIdRowElement.__buildExchangeIntent
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__buildExchangeIntent<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._buildExchangeIntent */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.buildExchangeIntent} */
/**
 * Controls the VDUs using the model of the _xyz.swapee.wc.IExchangeIntent_ component.
 * @param {!xyz.swapee.wc.IExchangeIntentCore.Model} model
 * @param {!xyz.swapee.wc.IExchangeIntent} instance
 * @return {?}
 */
xyz.swapee.wc.IExchangeIdRowElement.buildExchangeIntent = function(model, instance) {}

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.ExchangeIdRowMemory, ports: !xyz.swapee.wc.IExchangeIdRowElement.short.Ports, cores: !xyz.swapee.wc.IExchangeIdRowElement.short.Cores) => ?} xyz.swapee.wc.IExchangeIdRowElement.__short
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__short<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._short */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.short} */
/**
 * Shorts the model against peer's ports.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} model The model from which to feed properties to peer's ports.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `getTransaction` _boolean_ Retrieves the transaction. Default `false`.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.short.Ports} ports The ports of the peers.
 * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentPortInterface_
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.short.Cores} cores The cores of the peers.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntentCore.Model_
 * @return {?}
 */
xyz.swapee.wc.IExchangeIdRowElement.short = function(model, ports, cores) {}

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElement.short.Ports The ports of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentPortInterface} ExchangeIntent
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElement.short.Cores The cores of the peers.
 * @prop {xyz.swapee.wc.IExchangeIntentCore.Model} ExchangeIntent
 */

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeIdRowMemory, inputs: !xyz.swapee.wc.IExchangeIdRowElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IExchangeIdRowElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__server<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._server */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} memory The memory registers.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `getTransaction` _boolean_ Retrieves the transaction. Default `false`.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.Inputs} inputs The inputs to the port.
 * - `[id=null]` _&#42;?_ The ID of the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* Default `null`.
 * - `[getTransaction=null]` _&#42;?_ Retrieves the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* Default `null`.
 * - `[fixed=null]` _&#42;?_ Whether this is a fixed-rate transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangeIdRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIdRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIdLaOpts]` _!Object?_ The options to pass to the _ExchangeIdLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts* Default `{}`.
 * - `[exchangeIdFloatingLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFloatingLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts* Default `{}`.
 * - `[exchangeIdFixedLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFixedLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IExchangeIdRowElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.ExchangeIdRowMemory, port?: !xyz.swapee.wc.IExchangeIdRowElement.Inputs) => ?} xyz.swapee.wc.IExchangeIdRowElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowElement.__inducer<!xyz.swapee.wc.IExchangeIdRowElement>} xyz.swapee.wc.IExchangeIdRowElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} [model] The model of the component into which to induce the state.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `getTransaction` _boolean_ Retrieves the transaction. Default `false`.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.IExchangeIdRowElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[id=null]` _&#42;?_ The ID of the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* ⤴ *IExchangeIdRowOuterCore.WeakModel.Id* Default `null`.
 * - `[getTransaction=null]` _&#42;?_ Retrieves the transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* ⤴ *IExchangeIdRowOuterCore.WeakModel.GetTransaction* Default `null`.
 * - `[fixed=null]` _&#42;?_ Whether this is a fixed-rate transaction. ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* ⤴ *IExchangeIdRowOuterCore.WeakModel.Fixed* Default `null`.
 * - `[exchangeIntentSel=""]` _string?_ The query to discover the _ExchangeIntent_ VDU. ⤴ *IExchangeIdRowDisplay.Queries* Default empty string.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IExchangeIdRowElementPort.Inputs.NoSolder* Default `false`.
 * - `[exchangeIdLaOpts]` _!Object?_ The options to pass to the _ExchangeIdLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts* Default `{}`.
 * - `[exchangeIdFloatingLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFloatingLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts* Default `{}`.
 * - `[exchangeIdFixedLaOpts]` _!Object?_ The options to pass to the _ExchangeIdFixedLa_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts* Default `{}`.
 * - `[exchangeIntentOpts]` _!Object?_ The options to pass to the _ExchangeIntent_ vdu. ⤴ *IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IExchangeIdRowElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/140-IExchangeIdRowElementPort.xml}  be3d88fa25f175dbe6fe5140b0c4eb03 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IExchangeIdRowElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowElementPort)} xyz.swapee.wc.AbstractExchangeIdRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowElementPort} xyz.swapee.wc.ExchangeIdRowElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowElementPort
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowElementPort.constructor&xyz.swapee.wc.ExchangeIdRowElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowElementPort.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElementPort|typeof xyz.swapee.wc.ExchangeIdRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowElementPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElementPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElementPort|typeof xyz.swapee.wc.ExchangeIdRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElementPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowElementPort|typeof xyz.swapee.wc.ExchangeIdRowElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElementPort}
 */
xyz.swapee.wc.AbstractExchangeIdRowElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowElementPort.Initialese[]) => xyz.swapee.wc.IExchangeIdRowElementPort} xyz.swapee.wc.ExchangeIdRowElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IExchangeIdRowElementPort.Inputs>)} xyz.swapee.wc.IExchangeIdRowElementPort.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IExchangeIdRowElementPort
 */
xyz.swapee.wc.IExchangeIdRowElementPort = class extends /** @type {xyz.swapee.wc.IExchangeIdRowElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowElementPort.Initialese>)} xyz.swapee.wc.ExchangeIdRowElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort} xyz.swapee.wc.IExchangeIdRowElementPort.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IExchangeIdRowElementPort_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowElementPort
 * @implements {xyz.swapee.wc.IExchangeIdRowElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowElementPort.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowElementPort = class extends /** @type {xyz.swapee.wc.ExchangeIdRowElementPort.constructor&xyz.swapee.wc.IExchangeIdRowElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowElementPort}
 */
xyz.swapee.wc.ExchangeIdRowElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowElementPort.
 * @interface xyz.swapee.wc.IExchangeIdRowElementPortFields
 */
xyz.swapee.wc.IExchangeIdRowElementPortFields = class { }
/**
 * The inputs to the _IExchangeIdRowElement_'s controller via its element port.
 */
xyz.swapee.wc.IExchangeIdRowElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIdRowElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IExchangeIdRowElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IExchangeIdRowElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowElementPort} */
xyz.swapee.wc.RecordIExchangeIdRowElementPort

/** @typedef {xyz.swapee.wc.IExchangeIdRowElementPort} xyz.swapee.wc.BoundIExchangeIdRowElementPort */

/** @typedef {xyz.swapee.wc.ExchangeIdRowElementPort} xyz.swapee.wc.BoundExchangeIdRowElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _ExchangeIdLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts.exchangeIdLaOpts

/**
 * The options to pass to the _ExchangeIdFloatingLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts.exchangeIdFloatingLaOpts

/**
 * The options to pass to the _ExchangeIdFixedLa_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts.exchangeIdFixedLaOpts

/**
 * The options to pass to the _RestartBu_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts.restartBuOpts

/**
 * The options to pass to the _ExchangeIntent_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts.exchangeIntentOpts

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts)} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IExchangeIdRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeIdRowElementPort.Inputs
 */
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.constructor&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IExchangeIdRowElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts)} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts.typeof */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts.typeof */
/**
 * The inputs to the _IExchangeIdRowElement_'s controller via its element port.
 * @record xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs
 */
xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.constructor&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts.typeof&xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs

/**
 * Contains getters to cast the _IExchangeIdRowElementPort_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowElementPortCaster
 */
xyz.swapee.wc.IExchangeIdRowElementPortCaster = class { }
/**
 * Cast the _IExchangeIdRowElementPort_ instance into the _BoundIExchangeIdRowElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowElementPort}
 */
xyz.swapee.wc.IExchangeIdRowElementPortCaster.prototype.asIExchangeIdRowElementPort
/**
 * Access the _ExchangeIdRowElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowElementPort}
 */
xyz.swapee.wc.IExchangeIdRowElementPortCaster.prototype.superExchangeIdRowElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts The options to pass to the _ExchangeIdLa_ vdu (optional overlay).
 * @prop {!Object} [exchangeIdLaOpts] The options to pass to the _ExchangeIdLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdLaOpts_Safe The options to pass to the _ExchangeIdLa_ vdu (required overlay).
 * @prop {!Object} exchangeIdLaOpts The options to pass to the _ExchangeIdLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts The options to pass to the _ExchangeIdFloatingLa_ vdu (optional overlay).
 * @prop {!Object} [exchangeIdFloatingLaOpts] The options to pass to the _ExchangeIdFloatingLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFloatingLaOpts_Safe The options to pass to the _ExchangeIdFloatingLa_ vdu (required overlay).
 * @prop {!Object} exchangeIdFloatingLaOpts The options to pass to the _ExchangeIdFloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts The options to pass to the _ExchangeIdFixedLa_ vdu (optional overlay).
 * @prop {!Object} [exchangeIdFixedLaOpts] The options to pass to the _ExchangeIdFixedLa_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIdFixedLaOpts_Safe The options to pass to the _ExchangeIdFixedLa_ vdu (required overlay).
 * @prop {!Object} exchangeIdFixedLaOpts The options to pass to the _ExchangeIdFixedLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts The options to pass to the _RestartBu_ vdu (optional overlay).
 * @prop {!Object} [restartBuOpts] The options to pass to the _RestartBu_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.RestartBuOpts_Safe The options to pass to the _RestartBu_ vdu (required overlay).
 * @prop {!Object} restartBuOpts The options to pass to the _RestartBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {!Object} [exchangeIntentOpts] The options to pass to the _ExchangeIntent_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.Inputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {!Object} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts The options to pass to the _ExchangeIdLa_ vdu (optional overlay).
 * @prop {*} [exchangeIdLaOpts=null] The options to pass to the _ExchangeIdLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdLaOpts_Safe The options to pass to the _ExchangeIdLa_ vdu (required overlay).
 * @prop {*} exchangeIdLaOpts The options to pass to the _ExchangeIdLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts The options to pass to the _ExchangeIdFloatingLa_ vdu (optional overlay).
 * @prop {*} [exchangeIdFloatingLaOpts=null] The options to pass to the _ExchangeIdFloatingLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFloatingLaOpts_Safe The options to pass to the _ExchangeIdFloatingLa_ vdu (required overlay).
 * @prop {*} exchangeIdFloatingLaOpts The options to pass to the _ExchangeIdFloatingLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts The options to pass to the _ExchangeIdFixedLa_ vdu (optional overlay).
 * @prop {*} [exchangeIdFixedLaOpts=null] The options to pass to the _ExchangeIdFixedLa_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIdFixedLaOpts_Safe The options to pass to the _ExchangeIdFixedLa_ vdu (required overlay).
 * @prop {*} exchangeIdFixedLaOpts The options to pass to the _ExchangeIdFixedLa_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts The options to pass to the _RestartBu_ vdu (optional overlay).
 * @prop {*} [restartBuOpts=null] The options to pass to the _RestartBu_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.RestartBuOpts_Safe The options to pass to the _RestartBu_ vdu (required overlay).
 * @prop {*} restartBuOpts The options to pass to the _RestartBu_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu (optional overlay).
 * @prop {*} [exchangeIntentOpts=null] The options to pass to the _ExchangeIntent_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowElementPort.WeakInputs.ExchangeIntentOpts_Safe The options to pass to the _ExchangeIntent_ vdu (required overlay).
 * @prop {*} exchangeIntentOpts The options to pass to the _ExchangeIntent_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/170-IExchangeIdRowDesigner.xml}  4b8632f802402d27ac5f3935272f8f2f */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IExchangeIdRowDesigner
 */
xyz.swapee.wc.IExchangeIdRowDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.ExchangeIdRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeIdRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeIdRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IExchangeIdRow />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.ExchangeIdRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeIdRowDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ExchangeIdRow` _typeof IExchangeIdRowController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IExchangeIdRowDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `ExchangeIntent` _typeof xyz.swapee.wc.IExchangeIntentController_
   * - `ExchangeIdRow` _typeof IExchangeIdRowController_
   * - `This` _typeof IExchangeIdRowController_
   * @param {!xyz.swapee.wc.IExchangeIdRowDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `ExchangeIntent` _!xyz.swapee.wc.ExchangeIntentMemory_
   * - `ExchangeIdRow` _!ExchangeIdRowMemory_
   * - `This` _!ExchangeIdRowMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeIdRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.ExchangeIdRowClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IExchangeIdRowDesigner.prototype.constructor = xyz.swapee.wc.IExchangeIdRowDesigner

/**
 * A concrete class of _IExchangeIdRowDesigner_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowDesigner
 * @implements {xyz.swapee.wc.IExchangeIdRowDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.ExchangeIdRowDesigner = class extends xyz.swapee.wc.IExchangeIdRowDesigner { }
xyz.swapee.wc.ExchangeIdRowDesigner.prototype.constructor = xyz.swapee.wc.ExchangeIdRowDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowController} ExchangeIdRow
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IExchangeIntentController} ExchangeIntent
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowController} ExchangeIdRow
 * @prop {typeof xyz.swapee.wc.IExchangeIdRowController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.ExchangeIntentMemory} ExchangeIntent
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} ExchangeIdRow
 * @prop {!xyz.swapee.wc.ExchangeIdRowMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/200-ExchangeIdRowLand.xml}  cb1845e53225597bf8220f76923382cc */
/**
 * The surrounding of the _IExchangeIdRow_ that makes up its mesh together with the
 * component's controller.
 * @record xyz.swapee.wc.ExchangeIdRowLand
 */
xyz.swapee.wc.ExchangeIdRowLand = class { }
/**
 *
 */
xyz.swapee.wc.ExchangeIdRowLand.prototype.ExchangeIntent = /** @type {xyz.swapee.wc.IExchangeIntent} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/40-IExchangeIdRowDisplay.xml}  cf490c448bb452bdbd77744de0839454 */
/**
 * @typedef {Object} $xyz.swapee.wc.IExchangeIdRowDisplay.Initialese
 * @prop {HTMLSpanElement} [ExchangeIdLa]
 * @prop {HTMLSpanElement} [ExchangeIdFloatingLa]
 * @prop {HTMLSpanElement} [ExchangeIdFixedLa]
 * @prop {HTMLButtonElement} [RestartBu]
 * @prop {HTMLElement} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.IExchangeIdRowDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IExchangeIdRowDisplay.Settings>} xyz.swapee.wc.IExchangeIdRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowDisplay)} xyz.swapee.wc.AbstractExchangeIdRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowDisplay} xyz.swapee.wc.ExchangeIdRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowDisplay
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowDisplay.constructor&xyz.swapee.wc.ExchangeIdRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowDisplay.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowDisplay}
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.AbstractExchangeIdRowDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowDisplay.Initialese[]) => xyz.swapee.wc.IExchangeIdRowDisplay} xyz.swapee.wc.ExchangeIdRowDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.ExchangeIdRowMemory, !HTMLDivElement, !xyz.swapee.wc.IExchangeIdRowDisplay.Settings, xyz.swapee.wc.IExchangeIdRowDisplay.Queries, null>)} xyz.swapee.wc.IExchangeIdRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IExchangeIdRow_.
 * @interface xyz.swapee.wc.IExchangeIdRowDisplay
 */
xyz.swapee.wc.IExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.IExchangeIdRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIdRowDisplay.paint} */
xyz.swapee.wc.IExchangeIdRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese>)} xyz.swapee.wc.ExchangeIdRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowDisplay} xyz.swapee.wc.IExchangeIdRowDisplay.typeof */
/**
 * A concrete class of _IExchangeIdRowDisplay_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowDisplay
 * @implements {xyz.swapee.wc.IExchangeIdRowDisplay} Display for presenting information from the _IExchangeIdRow_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.ExchangeIdRowDisplay.constructor&xyz.swapee.wc.IExchangeIdRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.ExchangeIdRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowDisplay.
 * @interface xyz.swapee.wc.IExchangeIdRowDisplayFields
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IExchangeIdRowDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IExchangeIdRowDisplay.Queries} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.ExchangeIdLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.ExchangeIdFloatingLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.ExchangeIdFixedLa = /** @type {HTMLSpanElement} */ (void 0)
/**
 * Default `null`.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.RestartBu = /** @type {HTMLButtonElement} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer. Default `null`.
 */
xyz.swapee.wc.IExchangeIdRowDisplayFields.prototype.ExchangeIntent = /** @type {HTMLElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowDisplay} */
xyz.swapee.wc.RecordIExchangeIdRowDisplay

/** @typedef {xyz.swapee.wc.IExchangeIdRowDisplay} xyz.swapee.wc.BoundIExchangeIdRowDisplay */

/** @typedef {xyz.swapee.wc.ExchangeIdRowDisplay} xyz.swapee.wc.BoundExchangeIdRowDisplay */

/** @typedef {xyz.swapee.wc.IExchangeIdRowDisplay.Queries} xyz.swapee.wc.IExchangeIdRowDisplay.Settings The settings for the screen which will not be passed to the backend. */

/**
 * @typedef {Object} xyz.swapee.wc.IExchangeIdRowDisplay.Queries The queries to discover VDUs on the page by.
 * @prop {string} [exchangeIntentSel=""] The query to discover the _ExchangeIntent_ VDU. Default empty string.
 */

/**
 * Contains getters to cast the _IExchangeIdRowDisplay_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowDisplayCaster
 */
xyz.swapee.wc.IExchangeIdRowDisplayCaster = class { }
/**
 * Cast the _IExchangeIdRowDisplay_ instance into the _BoundIExchangeIdRowDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowDisplay}
 */
xyz.swapee.wc.IExchangeIdRowDisplayCaster.prototype.asIExchangeIdRowDisplay
/**
 * Cast the _IExchangeIdRowDisplay_ instance into the _BoundIExchangeIdRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowScreen}
 */
xyz.swapee.wc.IExchangeIdRowDisplayCaster.prototype.asIExchangeIdRowScreen
/**
 * Access the _ExchangeIdRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowDisplay}
 */
xyz.swapee.wc.IExchangeIdRowDisplayCaster.prototype.superExchangeIdRowDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.ExchangeIdRowMemory, land: null) => void} xyz.swapee.wc.IExchangeIdRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowDisplay.__paint<!xyz.swapee.wc.IExchangeIdRowDisplay>} xyz.swapee.wc.IExchangeIdRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} memory The display data.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/40-IExchangeIdRowDisplayBack.xml}  ca77e8933c7704e57aa49a017db4a05b */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIdLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIdFloatingLa]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIdFixedLa]
 * @prop {!com.webcircuits.IHtmlTwin} [RestartBu]
 * @prop {!com.webcircuits.IHtmlTwin} [ExchangeIntent] The via for the _ExchangeIntent_ peer.
 */
/** @typedef {$xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.ExchangeIdRowClasses>} xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIdRowDisplay)} xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay} xyz.swapee.wc.back.ExchangeIdRowDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIdRowDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIdRowDisplay
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.constructor&xyz.swapee.wc.back.ExchangeIdRowDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIdRowDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIdRowDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.ExchangeIdRowClasses, !xyz.swapee.wc.ExchangeIdRowLand>)} xyz.swapee.wc.back.IExchangeIdRowDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IExchangeIdRowDisplay
 */
xyz.swapee.wc.back.IExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.back.IExchangeIdRowDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IExchangeIdRowDisplay.paint} */
xyz.swapee.wc.back.IExchangeIdRowDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese>)} xyz.swapee.wc.back.ExchangeIdRowDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowDisplay} xyz.swapee.wc.back.IExchangeIdRowDisplay.typeof */
/**
 * A concrete class of _IExchangeIdRowDisplay_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIdRowDisplay
 * @implements {xyz.swapee.wc.back.IExchangeIdRowDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIdRowDisplay = class extends /** @type {xyz.swapee.wc.back.ExchangeIdRowDisplay.constructor&xyz.swapee.wc.back.IExchangeIdRowDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.ExchangeIdRowDisplay.prototype.constructor = xyz.swapee.wc.back.ExchangeIdRowDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.ExchangeIdRowDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowDisplay.
 * @interface xyz.swapee.wc.back.IExchangeIdRowDisplayFields
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields = class { }
/**
 *
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields.prototype.ExchangeIdLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields.prototype.ExchangeIdFloatingLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields.prototype.ExchangeIdFixedLa = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 *
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields.prototype.RestartBu = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)
/**
 * The via for the _ExchangeIntent_ peer.
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayFields.prototype.ExchangeIntent = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowDisplay} */
xyz.swapee.wc.back.RecordIExchangeIdRowDisplay

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowDisplay} xyz.swapee.wc.back.BoundIExchangeIdRowDisplay */

/** @typedef {xyz.swapee.wc.back.ExchangeIdRowDisplay} xyz.swapee.wc.back.BoundExchangeIdRowDisplay */

/**
 * Contains getters to cast the _IExchangeIdRowDisplay_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIdRowDisplayCaster
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayCaster = class { }
/**
 * Cast the _IExchangeIdRowDisplay_ instance into the _BoundIExchangeIdRowDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayCaster.prototype.asIExchangeIdRowDisplay
/**
 * Access the _ExchangeIdRowDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIdRowDisplay}
 */
xyz.swapee.wc.back.IExchangeIdRowDisplayCaster.prototype.superExchangeIdRowDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.ExchangeIdRowMemory, land?: !xyz.swapee.wc.ExchangeIdRowLand) => void} xyz.swapee.wc.back.IExchangeIdRowDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IExchangeIdRowDisplay.__paint<!xyz.swapee.wc.back.IExchangeIdRowDisplay>} xyz.swapee.wc.back.IExchangeIdRowDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.ExchangeIdRowMemory} [memory] The display data.
 * - `id` _string_ The ID of the transaction. Default empty string.
 * - `fixed` _boolean_ Whether this is a fixed-rate transaction. Default `false`.
 * @param {!xyz.swapee.wc.ExchangeIdRowLand} [land] The land data.
 * - `ExchangeIntent` _xyz.swapee.wc.IExchangeIntent_
 * @return {void}
 */
xyz.swapee.wc.back.IExchangeIdRowDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IExchangeIdRowDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/41-ExchangeIdRowClasses.xml}  cd2d36ef8c36431b39da23b76f42c717 */
/**
 * The classes of the _IExchangeIdRowDisplay_.
 * @record xyz.swapee.wc.ExchangeIdRowClasses
 */
xyz.swapee.wc.ExchangeIdRowClasses = class { }
/**
 *
 */
xyz.swapee.wc.ExchangeIdRowClasses.prototype.Class = /** @type {string|undefined} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.ExchangeIdRowClasses.prototype.props = /** @type {xyz.swapee.wc.ExchangeIdRowClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/50-IExchangeIdRowController.xml}  89fd7b0b86e581533ea4246a7056897a */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IExchangeIdRowController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IExchangeIdRowController.Inputs, !xyz.swapee.wc.IExchangeIdRowOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel>} xyz.swapee.wc.IExchangeIdRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowController)} xyz.swapee.wc.AbstractExchangeIdRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowController} xyz.swapee.wc.ExchangeIdRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowController` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowController
 */
xyz.swapee.wc.AbstractExchangeIdRowController = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowController.constructor&xyz.swapee.wc.ExchangeIdRowController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowController.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowController.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowController}
 */
xyz.swapee.wc.AbstractExchangeIdRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowController}
 */
xyz.swapee.wc.AbstractExchangeIdRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowController}
 */
xyz.swapee.wc.AbstractExchangeIdRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowController}
 */
xyz.swapee.wc.AbstractExchangeIdRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowController.Initialese[]) => xyz.swapee.wc.IExchangeIdRowController} xyz.swapee.wc.ExchangeIdRowControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IExchangeIdRowController.Inputs, !xyz.swapee.wc.IExchangeIdRowOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IExchangeIdRowOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IExchangeIdRowController.Inputs, !xyz.swapee.wc.IExchangeIdRowController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IExchangeIdRowController.Inputs, !xyz.swapee.wc.ExchangeIdRowMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IExchangeIdRowController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IExchangeIdRowController.Inputs>)} xyz.swapee.wc.IExchangeIdRowController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IExchangeIdRowController
 */
xyz.swapee.wc.IExchangeIdRowController = class extends /** @type {xyz.swapee.wc.IExchangeIdRowController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IExchangeIdRowController.resetPort} */
xyz.swapee.wc.IExchangeIdRowController.prototype.resetPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowController&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowController.Initialese>)} xyz.swapee.wc.ExchangeIdRowController.constructor */
/**
 * A concrete class of _IExchangeIdRowController_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowController
 * @implements {xyz.swapee.wc.IExchangeIdRowController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowController.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowController = class extends /** @type {xyz.swapee.wc.ExchangeIdRowController.constructor&xyz.swapee.wc.IExchangeIdRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowController}
 */
xyz.swapee.wc.ExchangeIdRowController.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowController.
 * @interface xyz.swapee.wc.IExchangeIdRowControllerFields
 */
xyz.swapee.wc.IExchangeIdRowControllerFields = class { }
/**
 * The inputs to the _IExchangeIdRow_'s controller.
 */
xyz.swapee.wc.IExchangeIdRowControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IExchangeIdRowController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IExchangeIdRowControllerFields.prototype.props = /** @type {xyz.swapee.wc.IExchangeIdRowController} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowController} */
xyz.swapee.wc.RecordIExchangeIdRowController

/** @typedef {xyz.swapee.wc.IExchangeIdRowController} xyz.swapee.wc.BoundIExchangeIdRowController */

/** @typedef {xyz.swapee.wc.ExchangeIdRowController} xyz.swapee.wc.BoundExchangeIdRowController */

/** @typedef {xyz.swapee.wc.IExchangeIdRowPort.Inputs} xyz.swapee.wc.IExchangeIdRowController.Inputs The inputs to the _IExchangeIdRow_'s controller. */

/** @typedef {xyz.swapee.wc.IExchangeIdRowPort.WeakInputs} xyz.swapee.wc.IExchangeIdRowController.WeakInputs The inputs to the _IExchangeIdRow_'s controller. */

/**
 * Contains getters to cast the _IExchangeIdRowController_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowControllerCaster
 */
xyz.swapee.wc.IExchangeIdRowControllerCaster = class { }
/**
 * Cast the _IExchangeIdRowController_ instance into the _BoundIExchangeIdRowController_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowController}
 */
xyz.swapee.wc.IExchangeIdRowControllerCaster.prototype.asIExchangeIdRowController
/**
 * Cast the _IExchangeIdRowController_ instance into the _BoundIExchangeIdRowProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowProcessor}
 */
xyz.swapee.wc.IExchangeIdRowControllerCaster.prototype.asIExchangeIdRowProcessor
/**
 * Access the _ExchangeIdRowController_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowController}
 */
xyz.swapee.wc.IExchangeIdRowControllerCaster.prototype.superExchangeIdRowController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IExchangeIdRowController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IExchangeIdRowController.__resetPort<!xyz.swapee.wc.IExchangeIdRowController>} xyz.swapee.wc.IExchangeIdRowController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IExchangeIdRowController.resetPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IExchangeIdRowController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/51-IExchangeIdRowControllerFront.xml}  8d7965c856e3415bf94d63d404a6a43a */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IExchangeIdRowController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIdRowController)} xyz.swapee.wc.front.AbstractExchangeIdRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIdRowController} xyz.swapee.wc.front.ExchangeIdRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIdRowController` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIdRowController
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIdRowController.constructor&xyz.swapee.wc.front.ExchangeIdRowController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIdRowController.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIdRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIdRowController}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowController}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowController}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowController}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIdRowController.Initialese[]) => xyz.swapee.wc.front.IExchangeIdRowController} xyz.swapee.wc.front.ExchangeIdRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIdRowControllerCaster&xyz.swapee.wc.front.IExchangeIdRowControllerAT)} xyz.swapee.wc.front.IExchangeIdRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIdRowControllerAT} xyz.swapee.wc.front.IExchangeIdRowControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IExchangeIdRowController
 */
xyz.swapee.wc.front.IExchangeIdRowController = class extends /** @type {xyz.swapee.wc.front.IExchangeIdRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IExchangeIdRowControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIdRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIdRowController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowController.Initialese>)} xyz.swapee.wc.front.ExchangeIdRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIdRowController} xyz.swapee.wc.front.IExchangeIdRowController.typeof */
/**
 * A concrete class of _IExchangeIdRowController_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIdRowController
 * @implements {xyz.swapee.wc.front.IExchangeIdRowController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowController.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIdRowController = class extends /** @type {xyz.swapee.wc.front.ExchangeIdRowController.constructor&xyz.swapee.wc.front.IExchangeIdRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIdRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIdRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowController}
 */
xyz.swapee.wc.front.ExchangeIdRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowController} */
xyz.swapee.wc.front.RecordIExchangeIdRowController

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowController} xyz.swapee.wc.front.BoundIExchangeIdRowController */

/** @typedef {xyz.swapee.wc.front.ExchangeIdRowController} xyz.swapee.wc.front.BoundExchangeIdRowController */

/**
 * Contains getters to cast the _IExchangeIdRowController_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIdRowControllerCaster
 */
xyz.swapee.wc.front.IExchangeIdRowControllerCaster = class { }
/**
 * Cast the _IExchangeIdRowController_ instance into the _BoundIExchangeIdRowController_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIdRowController}
 */
xyz.swapee.wc.front.IExchangeIdRowControllerCaster.prototype.asIExchangeIdRowController
/**
 * Access the _ExchangeIdRowController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIdRowController}
 */
xyz.swapee.wc.front.IExchangeIdRowControllerCaster.prototype.superExchangeIdRowController

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/52-IExchangeIdRowControllerBack.xml}  ed776ed147f0f64fecd2adcedca049c6 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IExchangeIdRowController.Inputs>&xyz.swapee.wc.IExchangeIdRowController.Initialese} xyz.swapee.wc.back.IExchangeIdRowController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIdRowController)} xyz.swapee.wc.back.AbstractExchangeIdRowController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIdRowController} xyz.swapee.wc.back.ExchangeIdRowController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIdRowController` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIdRowController
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIdRowController.constructor&xyz.swapee.wc.back.ExchangeIdRowController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIdRowController.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIdRowController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIdRowController}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowController}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowController}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowController|typeof xyz.swapee.wc.back.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowController}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIdRowController.Initialese[]) => xyz.swapee.wc.back.IExchangeIdRowController} xyz.swapee.wc.back.ExchangeIdRowControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIdRowControllerCaster&xyz.swapee.wc.IExchangeIdRowController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IExchangeIdRowController.Inputs>)} xyz.swapee.wc.back.IExchangeIdRowController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IExchangeIdRowController
 */
xyz.swapee.wc.back.IExchangeIdRowController = class extends /** @type {xyz.swapee.wc.back.IExchangeIdRowController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IExchangeIdRowController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIdRowController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowController.Initialese>)} xyz.swapee.wc.back.ExchangeIdRowController.constructor */
/**
 * A concrete class of _IExchangeIdRowController_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIdRowController
 * @implements {xyz.swapee.wc.back.IExchangeIdRowController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowController.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIdRowController = class extends /** @type {xyz.swapee.wc.back.ExchangeIdRowController.constructor&xyz.swapee.wc.back.IExchangeIdRowController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIdRowController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIdRowController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowController}
 */
xyz.swapee.wc.back.ExchangeIdRowController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowController} */
xyz.swapee.wc.back.RecordIExchangeIdRowController

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowController} xyz.swapee.wc.back.BoundIExchangeIdRowController */

/** @typedef {xyz.swapee.wc.back.ExchangeIdRowController} xyz.swapee.wc.back.BoundExchangeIdRowController */

/**
 * Contains getters to cast the _IExchangeIdRowController_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIdRowControllerCaster
 */
xyz.swapee.wc.back.IExchangeIdRowControllerCaster = class { }
/**
 * Cast the _IExchangeIdRowController_ instance into the _BoundIExchangeIdRowController_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIdRowController}
 */
xyz.swapee.wc.back.IExchangeIdRowControllerCaster.prototype.asIExchangeIdRowController
/**
 * Access the _ExchangeIdRowController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIdRowController}
 */
xyz.swapee.wc.back.IExchangeIdRowControllerCaster.prototype.superExchangeIdRowController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/53-IExchangeIdRowControllerAR.xml}  33522e39a691fe3eb927eef1965ed880 */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeIdRowController.Initialese} xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIdRowControllerAR)} xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR} xyz.swapee.wc.back.ExchangeIdRowControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIdRowControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.constructor&xyz.swapee.wc.back.ExchangeIdRowControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowControllerAR|typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowControllerAR|typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowControllerAR|typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowController|typeof xyz.swapee.wc.ExchangeIdRowController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese[]) => xyz.swapee.wc.back.IExchangeIdRowControllerAR} xyz.swapee.wc.back.ExchangeIdRowControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIdRowControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeIdRowController)} xyz.swapee.wc.back.IExchangeIdRowControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIdRowControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IExchangeIdRowControllerAR
 */
xyz.swapee.wc.back.IExchangeIdRowControllerAR = class extends /** @type {xyz.swapee.wc.back.IExchangeIdRowControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeIdRowController.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIdRowControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese>)} xyz.swapee.wc.back.ExchangeIdRowControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowControllerAR} xyz.swapee.wc.back.IExchangeIdRowControllerAR.typeof */
/**
 * A concrete class of _IExchangeIdRowControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIdRowControllerAR
 * @implements {xyz.swapee.wc.back.IExchangeIdRowControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IExchangeIdRowControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIdRowControllerAR = class extends /** @type {xyz.swapee.wc.back.ExchangeIdRowControllerAR.constructor&xyz.swapee.wc.back.IExchangeIdRowControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIdRowControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.ExchangeIdRowControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowControllerAR} */
xyz.swapee.wc.back.RecordIExchangeIdRowControllerAR

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowControllerAR} xyz.swapee.wc.back.BoundIExchangeIdRowControllerAR */

/** @typedef {xyz.swapee.wc.back.ExchangeIdRowControllerAR} xyz.swapee.wc.back.BoundExchangeIdRowControllerAR */

/**
 * Contains getters to cast the _IExchangeIdRowControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIdRowControllerARCaster
 */
xyz.swapee.wc.back.IExchangeIdRowControllerARCaster = class { }
/**
 * Cast the _IExchangeIdRowControllerAR_ instance into the _BoundIExchangeIdRowControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.IExchangeIdRowControllerARCaster.prototype.asIExchangeIdRowControllerAR
/**
 * Access the _ExchangeIdRowControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIdRowControllerAR}
 */
xyz.swapee.wc.back.IExchangeIdRowControllerARCaster.prototype.superExchangeIdRowControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/54-IExchangeIdRowControllerAT.xml}  2cbf2a4c7b41cd07d5be0d1798c19f9f */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIdRowControllerAT)} xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT} xyz.swapee.wc.front.ExchangeIdRowControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIdRowControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.constructor&xyz.swapee.wc.front.ExchangeIdRowControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowControllerAT|typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese[]) => xyz.swapee.wc.front.IExchangeIdRowControllerAT} xyz.swapee.wc.front.ExchangeIdRowControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIdRowControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IExchangeIdRowControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIdRowControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IExchangeIdRowControllerAT
 */
xyz.swapee.wc.front.IExchangeIdRowControllerAT = class extends /** @type {xyz.swapee.wc.front.IExchangeIdRowControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIdRowControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIdRowControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese>)} xyz.swapee.wc.front.ExchangeIdRowControllerAT.constructor */
/**
 * A concrete class of _IExchangeIdRowControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIdRowControllerAT
 * @implements {xyz.swapee.wc.front.IExchangeIdRowControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IExchangeIdRowControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIdRowControllerAT = class extends /** @type {xyz.swapee.wc.front.ExchangeIdRowControllerAT.constructor&xyz.swapee.wc.front.IExchangeIdRowControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIdRowControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.ExchangeIdRowControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowControllerAT} */
xyz.swapee.wc.front.RecordIExchangeIdRowControllerAT

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowControllerAT} xyz.swapee.wc.front.BoundIExchangeIdRowControllerAT */

/** @typedef {xyz.swapee.wc.front.ExchangeIdRowControllerAT} xyz.swapee.wc.front.BoundExchangeIdRowControllerAT */

/**
 * Contains getters to cast the _IExchangeIdRowControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIdRowControllerATCaster
 */
xyz.swapee.wc.front.IExchangeIdRowControllerATCaster = class { }
/**
 * Cast the _IExchangeIdRowControllerAT_ instance into the _BoundIExchangeIdRowControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.IExchangeIdRowControllerATCaster.prototype.asIExchangeIdRowControllerAT
/**
 * Access the _ExchangeIdRowControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIdRowControllerAT}
 */
xyz.swapee.wc.front.IExchangeIdRowControllerATCaster.prototype.superExchangeIdRowControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/70-IExchangeIdRowScreen.xml}  91b669aba8b17053f34daebf6ad616eb */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.front.ExchangeIdRowInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeIdRowDisplay.Settings, !xyz.swapee.wc.IExchangeIdRowDisplay.Queries, null>&xyz.swapee.wc.IExchangeIdRowDisplay.Initialese} xyz.swapee.wc.IExchangeIdRowScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowScreen)} xyz.swapee.wc.AbstractExchangeIdRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowScreen} xyz.swapee.wc.ExchangeIdRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowScreen` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowScreen
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowScreen.constructor&xyz.swapee.wc.ExchangeIdRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowScreen.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowScreen}
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowScreen}
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowScreen}
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IExchangeIdRowController|typeof xyz.swapee.wc.front.ExchangeIdRowController)|(!xyz.swapee.wc.IExchangeIdRowDisplay|typeof xyz.swapee.wc.ExchangeIdRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowScreen}
 */
xyz.swapee.wc.AbstractExchangeIdRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowScreen.Initialese[]) => xyz.swapee.wc.IExchangeIdRowScreen} xyz.swapee.wc.ExchangeIdRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.ExchangeIdRowMemory, !xyz.swapee.wc.front.ExchangeIdRowInputs, !HTMLDivElement, !xyz.swapee.wc.IExchangeIdRowDisplay.Settings, !xyz.swapee.wc.IExchangeIdRowDisplay.Queries, null, null>&xyz.swapee.wc.front.IExchangeIdRowController&xyz.swapee.wc.IExchangeIdRowDisplay)} xyz.swapee.wc.IExchangeIdRowScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IExchangeIdRowScreen
 */
xyz.swapee.wc.IExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.IExchangeIdRowScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IExchangeIdRowController.typeof&xyz.swapee.wc.IExchangeIdRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowScreen.Initialese>)} xyz.swapee.wc.ExchangeIdRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IExchangeIdRowScreen} xyz.swapee.wc.IExchangeIdRowScreen.typeof */
/**
 * A concrete class of _IExchangeIdRowScreen_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowScreen
 * @implements {xyz.swapee.wc.IExchangeIdRowScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.ExchangeIdRowScreen.constructor&xyz.swapee.wc.IExchangeIdRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowScreen}
 */
xyz.swapee.wc.ExchangeIdRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IExchangeIdRowScreen} */
xyz.swapee.wc.RecordIExchangeIdRowScreen

/** @typedef {xyz.swapee.wc.IExchangeIdRowScreen} xyz.swapee.wc.BoundIExchangeIdRowScreen */

/** @typedef {xyz.swapee.wc.ExchangeIdRowScreen} xyz.swapee.wc.BoundExchangeIdRowScreen */

/**
 * Contains getters to cast the _IExchangeIdRowScreen_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowScreenCaster
 */
xyz.swapee.wc.IExchangeIdRowScreenCaster = class { }
/**
 * Cast the _IExchangeIdRowScreen_ instance into the _BoundIExchangeIdRowScreen_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowScreen}
 */
xyz.swapee.wc.IExchangeIdRowScreenCaster.prototype.asIExchangeIdRowScreen
/**
 * Access the _ExchangeIdRowScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowScreen}
 */
xyz.swapee.wc.IExchangeIdRowScreenCaster.prototype.superExchangeIdRowScreen

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/70-IExchangeIdRowScreenBack.xml}  923317c02f956195a8428fb074ff1e2e */
/** @typedef {xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese} xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIdRowScreen)} xyz.swapee.wc.back.AbstractExchangeIdRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIdRowScreen} xyz.swapee.wc.back.ExchangeIdRowScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIdRowScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIdRowScreen
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIdRowScreen.constructor&xyz.swapee.wc.back.ExchangeIdRowScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIdRowScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreen|typeof xyz.swapee.wc.back.ExchangeIdRowScreen)|(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreen}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese[]) => xyz.swapee.wc.back.IExchangeIdRowScreen} xyz.swapee.wc.back.ExchangeIdRowScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIdRowScreenCaster&xyz.swapee.wc.back.IExchangeIdRowScreenAT)} xyz.swapee.wc.back.IExchangeIdRowScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IExchangeIdRowScreenAT} xyz.swapee.wc.back.IExchangeIdRowScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IExchangeIdRowScreen
 */
xyz.swapee.wc.back.IExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.back.IExchangeIdRowScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IExchangeIdRowScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIdRowScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese>)} xyz.swapee.wc.back.ExchangeIdRowScreen.constructor */
/**
 * A concrete class of _IExchangeIdRowScreen_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIdRowScreen
 * @implements {xyz.swapee.wc.back.IExchangeIdRowScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIdRowScreen = class extends /** @type {xyz.swapee.wc.back.ExchangeIdRowScreen.constructor&xyz.swapee.wc.back.IExchangeIdRowScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIdRowScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreen}
 */
xyz.swapee.wc.back.ExchangeIdRowScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowScreen} */
xyz.swapee.wc.back.RecordIExchangeIdRowScreen

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowScreen} xyz.swapee.wc.back.BoundIExchangeIdRowScreen */

/** @typedef {xyz.swapee.wc.back.ExchangeIdRowScreen} xyz.swapee.wc.back.BoundExchangeIdRowScreen */

/**
 * Contains getters to cast the _IExchangeIdRowScreen_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIdRowScreenCaster
 */
xyz.swapee.wc.back.IExchangeIdRowScreenCaster = class { }
/**
 * Cast the _IExchangeIdRowScreen_ instance into the _BoundIExchangeIdRowScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIdRowScreen}
 */
xyz.swapee.wc.back.IExchangeIdRowScreenCaster.prototype.asIExchangeIdRowScreen
/**
 * Access the _ExchangeIdRowScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIdRowScreen}
 */
xyz.swapee.wc.back.IExchangeIdRowScreenCaster.prototype.superExchangeIdRowScreen

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/73-IExchangeIdRowScreenAR.xml}  d428403bc4322bbbacccfc7bff6f928d */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IExchangeIdRowScreen.Initialese} xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.ExchangeIdRowScreenAR)} xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR} xyz.swapee.wc.front.ExchangeIdRowScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IExchangeIdRowScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.constructor&xyz.swapee.wc.front.ExchangeIdRowScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowScreenAR|typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowScreenAR|typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IExchangeIdRowScreenAR|typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IExchangeIdRowScreen|typeof xyz.swapee.wc.ExchangeIdRowScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.AbstractExchangeIdRowScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese[]) => xyz.swapee.wc.front.IExchangeIdRowScreenAR} xyz.swapee.wc.front.ExchangeIdRowScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IExchangeIdRowScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IExchangeIdRowScreen)} xyz.swapee.wc.front.IExchangeIdRowScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIdRowScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IExchangeIdRowScreenAR
 */
xyz.swapee.wc.front.IExchangeIdRowScreenAR = class extends /** @type {xyz.swapee.wc.front.IExchangeIdRowScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IExchangeIdRowScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IExchangeIdRowScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IExchangeIdRowScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese>)} xyz.swapee.wc.front.ExchangeIdRowScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IExchangeIdRowScreenAR} xyz.swapee.wc.front.IExchangeIdRowScreenAR.typeof */
/**
 * A concrete class of _IExchangeIdRowScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.ExchangeIdRowScreenAR
 * @implements {xyz.swapee.wc.front.IExchangeIdRowScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IExchangeIdRowScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.ExchangeIdRowScreenAR = class extends /** @type {xyz.swapee.wc.front.ExchangeIdRowScreenAR.constructor&xyz.swapee.wc.front.IExchangeIdRowScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IExchangeIdRowScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.ExchangeIdRowScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.ExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.ExchangeIdRowScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowScreenAR} */
xyz.swapee.wc.front.RecordIExchangeIdRowScreenAR

/** @typedef {xyz.swapee.wc.front.IExchangeIdRowScreenAR} xyz.swapee.wc.front.BoundIExchangeIdRowScreenAR */

/** @typedef {xyz.swapee.wc.front.ExchangeIdRowScreenAR} xyz.swapee.wc.front.BoundExchangeIdRowScreenAR */

/**
 * Contains getters to cast the _IExchangeIdRowScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IExchangeIdRowScreenARCaster
 */
xyz.swapee.wc.front.IExchangeIdRowScreenARCaster = class { }
/**
 * Cast the _IExchangeIdRowScreenAR_ instance into the _BoundIExchangeIdRowScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.IExchangeIdRowScreenARCaster.prototype.asIExchangeIdRowScreenAR
/**
 * Access the _ExchangeIdRowScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundExchangeIdRowScreenAR}
 */
xyz.swapee.wc.front.IExchangeIdRowScreenARCaster.prototype.superExchangeIdRowScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/74-IExchangeIdRowScreenAT.xml}  5999ed4c2cbb80724f5a8a113f96354b */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.ExchangeIdRowScreenAT)} xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT} xyz.swapee.wc.back.ExchangeIdRowScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IExchangeIdRowScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.constructor&xyz.swapee.wc.back.ExchangeIdRowScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IExchangeIdRowScreenAT|typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.AbstractExchangeIdRowScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese[]) => xyz.swapee.wc.back.IExchangeIdRowScreenAT} xyz.swapee.wc.back.ExchangeIdRowScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IExchangeIdRowScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IExchangeIdRowScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIdRowScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IExchangeIdRowScreenAT
 */
xyz.swapee.wc.back.IExchangeIdRowScreenAT = class extends /** @type {xyz.swapee.wc.back.IExchangeIdRowScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IExchangeIdRowScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IExchangeIdRowScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese>)} xyz.swapee.wc.back.ExchangeIdRowScreenAT.constructor */
/**
 * A concrete class of _IExchangeIdRowScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.ExchangeIdRowScreenAT
 * @implements {xyz.swapee.wc.back.IExchangeIdRowScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IExchangeIdRowScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.ExchangeIdRowScreenAT = class extends /** @type {xyz.swapee.wc.back.ExchangeIdRowScreenAT.constructor&xyz.swapee.wc.back.IExchangeIdRowScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IExchangeIdRowScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.ExchangeIdRowScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.ExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.ExchangeIdRowScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowScreenAT} */
xyz.swapee.wc.back.RecordIExchangeIdRowScreenAT

/** @typedef {xyz.swapee.wc.back.IExchangeIdRowScreenAT} xyz.swapee.wc.back.BoundIExchangeIdRowScreenAT */

/** @typedef {xyz.swapee.wc.back.ExchangeIdRowScreenAT} xyz.swapee.wc.back.BoundExchangeIdRowScreenAT */

/**
 * Contains getters to cast the _IExchangeIdRowScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IExchangeIdRowScreenATCaster
 */
xyz.swapee.wc.back.IExchangeIdRowScreenATCaster = class { }
/**
 * Cast the _IExchangeIdRowScreenAT_ instance into the _BoundIExchangeIdRowScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.IExchangeIdRowScreenATCaster.prototype.asIExchangeIdRowScreenAT
/**
 * Access the _ExchangeIdRowScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundExchangeIdRowScreenAT}
 */
xyz.swapee.wc.back.IExchangeIdRowScreenATCaster.prototype.superExchangeIdRowScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/exchange-id-row/ExchangeIdRow.mvc/design/80-IExchangeIdRowGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IExchangeIdRowDisplay.Initialese} xyz.swapee.wc.IExchangeIdRowGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.ExchangeIdRowGPU)} xyz.swapee.wc.AbstractExchangeIdRowGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.ExchangeIdRowGPU} xyz.swapee.wc.ExchangeIdRowGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IExchangeIdRowGPU` interface.
 * @constructor xyz.swapee.wc.AbstractExchangeIdRowGPU
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU = class extends /** @type {xyz.swapee.wc.AbstractExchangeIdRowGPU.constructor&xyz.swapee.wc.ExchangeIdRowGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractExchangeIdRowGPU.prototype.constructor = xyz.swapee.wc.AbstractExchangeIdRowGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.class = /** @type {typeof xyz.swapee.wc.AbstractExchangeIdRowGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractExchangeIdRowGPU}
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowGPU}
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowGPU}
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IExchangeIdRowGPU|typeof xyz.swapee.wc.ExchangeIdRowGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IExchangeIdRowDisplay|typeof xyz.swapee.wc.back.ExchangeIdRowDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.ExchangeIdRowGPU}
 */
xyz.swapee.wc.AbstractExchangeIdRowGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IExchangeIdRowGPU.Initialese[]) => xyz.swapee.wc.IExchangeIdRowGPU} xyz.swapee.wc.ExchangeIdRowGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IExchangeIdRowGPUCaster&com.webcircuits.IBrowserView<.!ExchangeIdRowMemory,.!ExchangeIdRowLand>&xyz.swapee.wc.back.IExchangeIdRowDisplay)} xyz.swapee.wc.IExchangeIdRowGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!ExchangeIdRowMemory,.!ExchangeIdRowLand>} com.webcircuits.IBrowserView<.!ExchangeIdRowMemory,.!ExchangeIdRowLand>.typeof */
/**
 * Handles the periphery of the _IExchangeIdRowDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IExchangeIdRowGPU
 */
xyz.swapee.wc.IExchangeIdRowGPU = class extends /** @type {xyz.swapee.wc.IExchangeIdRowGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!ExchangeIdRowMemory,.!ExchangeIdRowLand>.typeof&xyz.swapee.wc.back.IExchangeIdRowDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IExchangeIdRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IExchangeIdRowGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IExchangeIdRowGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowGPU.Initialese>)} xyz.swapee.wc.ExchangeIdRowGPU.constructor */
/**
 * A concrete class of _IExchangeIdRowGPU_ instances.
 * @constructor xyz.swapee.wc.ExchangeIdRowGPU
 * @implements {xyz.swapee.wc.IExchangeIdRowGPU} Handles the periphery of the _IExchangeIdRowDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IExchangeIdRowGPU.Initialese>} ‎
 */
xyz.swapee.wc.ExchangeIdRowGPU = class extends /** @type {xyz.swapee.wc.ExchangeIdRowGPU.constructor&xyz.swapee.wc.IExchangeIdRowGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IExchangeIdRowGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IExchangeIdRowGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IExchangeIdRowGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IExchangeIdRowGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.ExchangeIdRowGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.ExchangeIdRowGPU}
 */
xyz.swapee.wc.ExchangeIdRowGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IExchangeIdRowGPU.
 * @interface xyz.swapee.wc.IExchangeIdRowGPUFields
 */
xyz.swapee.wc.IExchangeIdRowGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IExchangeIdRowGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IExchangeIdRowGPU} */
xyz.swapee.wc.RecordIExchangeIdRowGPU

/** @typedef {xyz.swapee.wc.IExchangeIdRowGPU} xyz.swapee.wc.BoundIExchangeIdRowGPU */

/** @typedef {xyz.swapee.wc.ExchangeIdRowGPU} xyz.swapee.wc.BoundExchangeIdRowGPU */

/**
 * Contains getters to cast the _IExchangeIdRowGPU_ interface.
 * @interface xyz.swapee.wc.IExchangeIdRowGPUCaster
 */
xyz.swapee.wc.IExchangeIdRowGPUCaster = class { }
/**
 * Cast the _IExchangeIdRowGPU_ instance into the _BoundIExchangeIdRowGPU_ type.
 * @type {!xyz.swapee.wc.BoundIExchangeIdRowGPU}
 */
xyz.swapee.wc.IExchangeIdRowGPUCaster.prototype.asIExchangeIdRowGPU
/**
 * Access the _ExchangeIdRowGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundExchangeIdRowGPU}
 */
xyz.swapee.wc.IExchangeIdRowGPUCaster.prototype.superExchangeIdRowGPU

// nss:xyz.swapee.wc
/* @typal-end */