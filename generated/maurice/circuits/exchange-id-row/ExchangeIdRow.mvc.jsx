/** @extends {xyz.swapee.wc.AbstractExchangeIdRow} */
export default class AbstractExchangeIdRow extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractExchangeIdRowComputer} */
export class AbstractExchangeIdRowComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIdRowController} */
export class AbstractExchangeIdRowController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeIdRowPort} */
export class ExchangeIdRowPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractExchangeIdRowView} */
export class AbstractExchangeIdRowView extends (<view>
  <classes>
   <string opt name="Class" />

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIdRowElement} */
export class AbstractExchangeIdRowElement extends (<element v3 html mv>
 <block src="./ExchangeIdRow.mvc/src/ExchangeIdRowElement/methods/render.jsx" />
 <inducer src="./ExchangeIdRow.mvc/src/ExchangeIdRowElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractExchangeIdRowHtmlComponent} */
export class AbstractExchangeIdRowHtmlComponent extends (<html-ic>
  <connectors>

   <xyz.swapee.wc.IExchangeIntent via="ExchangeIntent" />
  </connectors>

</html-ic>) { }
// </class-end>