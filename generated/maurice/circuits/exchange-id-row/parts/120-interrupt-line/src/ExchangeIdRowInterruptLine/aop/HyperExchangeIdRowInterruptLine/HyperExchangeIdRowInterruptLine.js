import AbstractHyperExchangeIdRowInterruptLine from '../../../../gen/AbstractExchangeIdRowInterruptLine/hyper/AbstractHyperExchangeIdRowInterruptLine'
import ExchangeIdRowInterruptLine from '../../ExchangeIdRowInterruptLine'
import ExchangeIdRowInterruptLineGeneralAspects from '../ExchangeIdRowInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperExchangeIdRowInterruptLine} */
export default class extends AbstractHyperExchangeIdRowInterruptLine
 .consults(
  ExchangeIdRowInterruptLineGeneralAspects,
 )
 .implements(
  ExchangeIdRowInterruptLine,
 )
{}