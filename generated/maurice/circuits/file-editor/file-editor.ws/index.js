const d=new Date
let _FileEditorPage,_fileEditorArcsIds,_fileEditorMethodsIds
// _getFileEditor

// const PROD=true
const PROD=false

;({
 FileEditorPage:_FileEditorPage,
 // getFileEditor:_getFileEditor,
 fileEditorArcsIds:_fileEditorArcsIds,
 fileEditorMethodsIds:_fileEditorMethodsIds,
}=require(PROD?'xyz/swapee/rc/file-editor/prod':'./file-editor.page'))

const dd=new Date
console.log(`${PROD?`📦`:`📝`} Loaded %s in %sms`,'file-editor',dd.getTime()-d.getTime())

import 'xyz/swapee/rc/file-editor'

export const FileEditorPage=/**@type {typeof xyz.swapee.rc.FileEditorPage}*/(_FileEditorPage)
export const fileEditorArcsIds=/**@type {typeof xyz.swapee.rc.fileEditorArcsIds}*/(_fileEditorArcsIds)
export const fileEditorMethodsIds=/**@type {typeof xyz.swapee.rc.fileEditorMethodsIds}*/(_fileEditorMethodsIds)
// export const getFileEditor=/**@type {xyz.swapee.rc.getFileEditor}*/(_getFileEditor)