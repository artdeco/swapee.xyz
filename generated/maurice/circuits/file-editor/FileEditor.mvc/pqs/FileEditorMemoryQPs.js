import {FileEditorMemoryPQs} from './FileEditorMemoryPQs'
export const FileEditorMemoryQPs=/**@type {!xyz.swapee.wc.FileEditorMemoryQPs}*/(Object.keys(FileEditorMemoryPQs)
 .reduce((a,k)=>{a[FileEditorMemoryPQs[k]]=k;return a},{}))