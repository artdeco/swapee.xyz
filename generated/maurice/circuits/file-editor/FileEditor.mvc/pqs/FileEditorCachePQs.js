export const FileEditorCachePQs=/**@type {!xyz.swapee.wc.FileEditorCachePQs}*/({
 loadingReadFile:'74bbb',
 hasMoreReadFile:'81758',
 loadReadFileError:'c9e55',
 loadingSaveFile:'2a8db',
 hasMoreSaveFile:'dba15',
 loadSaveFileError:'23052',
})