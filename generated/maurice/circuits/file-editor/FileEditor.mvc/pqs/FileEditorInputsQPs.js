import {FileEditorInputsPQs} from './FileEditorInputsPQs'
export const FileEditorInputsQPs=/**@type {!xyz.swapee.wc.FileEditorInputsQPs}*/(Object.keys(FileEditorInputsPQs)
 .reduce((a,k)=>{a[FileEditorInputsPQs[k]]=k;return a},{}))