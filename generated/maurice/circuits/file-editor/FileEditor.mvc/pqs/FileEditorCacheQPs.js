import {FileEditorCachePQs} from './FileEditorCachePQs'
export const FileEditorCacheQPs=/**@type {!xyz.swapee.wc.FileEditorCacheQPs}*/(Object.keys(FileEditorCachePQs)
 .reduce((a,k)=>{a[FileEditorCachePQs[k]]=k;return a},{}))