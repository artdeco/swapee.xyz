import {FileEditorMemoryPQs} from './FileEditorMemoryPQs'
export const FileEditorInputsPQs=/**@type {!xyz.swapee.wc.FileEditorInputsQPs}*/({
 ...FileEditorMemoryPQs,
})