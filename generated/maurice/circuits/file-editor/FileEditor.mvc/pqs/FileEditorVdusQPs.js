import {FileEditorVdusPQs} from './FileEditorVdusPQs'
export const FileEditorVdusQPs=/**@type {!xyz.swapee.wc.FileEditorVdusQPs}*/(Object.keys(FileEditorVdusPQs)
 .reduce((a,k)=>{a[FileEditorVdusPQs[k]]=k;return a},{}))