/** @nocompile */
eval(`var xyz={}
xyz.swapee={}
xyz.swapee.wc={}
xyz.swapee.wc.IFileEditorComputer={}
xyz.swapee.wc.IFileEditorComputer.adaptReadFile={}
xyz.swapee.wc.IFileEditorComputer.adaptSaveFile={}
xyz.swapee.wc.IFileEditorOuterCore={}
xyz.swapee.wc.IFileEditorOuterCore.Model={}
xyz.swapee.wc.IFileEditorOuterCore.Model.Host={}
xyz.swapee.wc.IFileEditorOuterCore.Model.Content={}
xyz.swapee.wc.IFileEditorOuterCore.Model.Path={}
xyz.swapee.wc.IFileEditorOuterCore.Model.Filename={}
xyz.swapee.wc.IFileEditorOuterCore.Model.Lang={}
xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges={}
xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile={}
xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile={}
xyz.swapee.wc.IFileEditorOuterCore.WeakModel={}
xyz.swapee.wc.IFileEditorPort={}
xyz.swapee.wc.IFileEditorPort.Inputs={}
xyz.swapee.wc.IFileEditorPort.WeakInputs={}
xyz.swapee.wc.IFileEditorCore={}
xyz.swapee.wc.IFileEditorCore.Model={}
xyz.swapee.wc.IFileEditorCore.Model.LoadingReadFile={}
xyz.swapee.wc.IFileEditorCore.Model.HasMoreReadFile={}
xyz.swapee.wc.IFileEditorCore.Model.LoadReadFileError={}
xyz.swapee.wc.IFileEditorCore.Model.LoadingSaveFile={}
xyz.swapee.wc.IFileEditorCore.Model.HasMoreSaveFile={}
xyz.swapee.wc.IFileEditorCore.Model.LoadSaveFileError={}
xyz.swapee.wc.IFileEditorPortInterface={}
xyz.swapee.wc.IFileEditorProcessor={}
xyz.swapee.wc.front={}
xyz.swapee.wc.front.IFileEditorController={}
xyz.swapee.wc.front.IFileEditorControllerAT={}
xyz.swapee.wc.front.IFileEditorScreenAR={}
xyz.swapee.wc.IFileEditor={}
xyz.swapee.wc.IFileEditorHtmlComponent={}
xyz.swapee.wc.IFileEditorElement={}
xyz.swapee.wc.IFileEditorElementPort={}
xyz.swapee.wc.IFileEditorElementPort.Inputs={}
xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder={}
xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts={}
xyz.swapee.wc.IFileEditorElementPort.WeakInputs={}
xyz.swapee.wc.IFileEditorRadio={}
xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile={}
xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile={}
xyz.swapee.wc.IFileEditorDesigner={}
xyz.swapee.wc.IFileEditorDesigner.communicator={}
xyz.swapee.wc.IFileEditorDesigner.relay={}
xyz.swapee.wc.IFileEditorService={}
xyz.swapee.wc.IFileEditorService.filterReadFile={}
xyz.swapee.wc.IFileEditorService.filterSaveFile={}
xyz.swapee.wc.IFileEditorDisplay={}
xyz.swapee.wc.IFileEditorDisplay.paintFile={}
xyz.swapee.wc.back={}
xyz.swapee.wc.back.IFileEditorDisplay={}
xyz.swapee.wc.back.IFileEditorController={}
xyz.swapee.wc.back.IFileEditorControllerAR={}
xyz.swapee.wc.back.IFileEditorScreen={}
xyz.swapee.wc.back.IFileEditorScreenAT={}
xyz.swapee.wc.IFileEditorController={}
xyz.swapee.wc.IFileEditorScreen={}
xyz.swapee.wc.IFileEditorScreenJoinpointModel={}
xyz.swapee.wc.IFileEditorScreenAspectsInstaller={}
xyz.swapee.wc.IHyperFileEditorScreen={}
xyz.swapee.wc.IFileEditorScreenAspects={}
xyz.swapee.wc.IFileEditorGPU={}`)

/** */
var __$te_plain={}

/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/02-IFileEditorComputer.xml}  ac12f716f0fc6470297bc267eead5299 */
/** @typedef {com.webcircuits.IAdapter.Initialese} xyz.swapee.wc.IFileEditorComputer.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorComputer)} xyz.swapee.wc.AbstractFileEditorComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorComputer} xyz.swapee.wc.FileEditorComputer.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorComputer` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorComputer
 */
xyz.swapee.wc.AbstractFileEditorComputer = class extends /** @type {xyz.swapee.wc.AbstractFileEditorComputer.constructor&xyz.swapee.wc.FileEditorComputer.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorComputer.prototype.constructor = xyz.swapee.wc.AbstractFileEditorComputer
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorComputer.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorComputer} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorComputer}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorComputer.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorComputer}
 */
xyz.swapee.wc.AbstractFileEditorComputer.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorComputer}
 */
xyz.swapee.wc.AbstractFileEditorComputer.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorComputer}
 */
xyz.swapee.wc.AbstractFileEditorComputer.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!com.webcircuits.IAdapter|typeof com.webcircuits.Adapter)|(!com.webcircuits.ILanded|typeof com.webcircuits.Landed)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorComputer}
 */
xyz.swapee.wc.AbstractFileEditorComputer.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorComputer.Initialese[]) => xyz.swapee.wc.IFileEditorComputer} xyz.swapee.wc.FileEditorComputerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorComputerCaster&com.webcircuits.IAdapter<!xyz.swapee.wc.FileEditorMemory>&com.webcircuits.ILanded<null>)} xyz.swapee.wc.IFileEditorComputer.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @typedef {typeof com.webcircuits.IAdapter} com.webcircuits.IAdapter.typeof */
/** @typedef {typeof com.webcircuits.ILanded} com.webcircuits.ILanded.typeof */
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @interface xyz.swapee.wc.IFileEditorComputer
 */
xyz.swapee.wc.IFileEditorComputer = class extends /** @type {xyz.swapee.wc.IFileEditorComputer.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAdapter.typeof&com.webcircuits.ILanded.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorComputer.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorComputer.adaptReadFile} */
xyz.swapee.wc.IFileEditorComputer.prototype.adaptReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorComputer.adaptSaveFile} */
xyz.swapee.wc.IFileEditorComputer.prototype.adaptSaveFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorComputer.compute} */
xyz.swapee.wc.IFileEditorComputer.prototype.compute = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorComputer&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorComputer.Initialese>)} xyz.swapee.wc.FileEditorComputer.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorComputer} xyz.swapee.wc.IFileEditorComputer.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileEditorComputer_ instances.
 * @constructor xyz.swapee.wc.FileEditorComputer
 * @implements {xyz.swapee.wc.IFileEditorComputer} Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorComputer.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorComputer = class extends /** @type {xyz.swapee.wc.FileEditorComputer.constructor&xyz.swapee.wc.IFileEditorComputer.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorComputer* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorComputer.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorComputer* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorComputer.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorComputer.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorComputer}
 */
xyz.swapee.wc.FileEditorComputer.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorComputer} */
xyz.swapee.wc.RecordIFileEditorComputer

/** @typedef {xyz.swapee.wc.IFileEditorComputer} xyz.swapee.wc.BoundIFileEditorComputer */

/** @typedef {xyz.swapee.wc.FileEditorComputer} xyz.swapee.wc.BoundFileEditorComputer */

/**
 * Contains getters to cast the _IFileEditorComputer_ interface.
 * @interface xyz.swapee.wc.IFileEditorComputerCaster
 */
xyz.swapee.wc.IFileEditorComputerCaster = class { }
/**
 * Cast the _IFileEditorComputer_ instance into the _BoundIFileEditorComputer_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorComputer}
 */
xyz.swapee.wc.IFileEditorComputerCaster.prototype.asIFileEditorComputer
/**
 * Access the _FileEditorComputer_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorComputer}
 */
xyz.swapee.wc.IFileEditorComputerCaster.prototype.superFileEditorComputer

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form, changes: xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Return|void)>)} xyz.swapee.wc.IFileEditorComputer.__adaptReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorComputer.__adaptReadFile<!xyz.swapee.wc.IFileEditorComputer>} xyz.swapee.wc.IFileEditorComputer._adaptReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorComputer.adaptReadFile} */
/**
 * Reads the file on the filesystem and opens it in the editor.
 * @param {!xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form} form The form with inputs.
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * @param {xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form} changes The previous values of the form.
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileEditorComputer.adaptReadFile = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path_Safe} xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content} xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form, changes: xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Return|void)>)} xyz.swapee.wc.IFileEditorComputer.__adaptSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorComputer.__adaptSaveFile<!xyz.swapee.wc.IFileEditorComputer>} xyz.swapee.wc.IFileEditorComputer._adaptSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorComputer.adaptSaveFile} */
/**
 * Saves the file back to the filesystem.
 * @param {!xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form} form The form with inputs.
 * - `content` _?string_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content_Safe*
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * - `saveFile` _boolean_ . ⤴ *IFileEditorOuterCore.Model.SaveFile_Safe*
 * @param {xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form} changes The previous values of the form.
 * - `content` _?string_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content_Safe*
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * - `saveFile` _boolean_ . ⤴ *IFileEditorOuterCore.Model.SaveFile_Safe*
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileEditorComputer.adaptSaveFile = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content_Safe&xyz.swapee.wc.IFileEditorCore.Model.Path_Safe&xyz.swapee.wc.IFileEditorCore.Model.SaveFile_Safe} xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path} xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Return The form with outputs. */

/**
 * @typedef {(this: THIS, mem: xyz.swapee.wc.FileEditorMemory) => void} xyz.swapee.wc.IFileEditorComputer.__compute
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorComputer.__compute<!xyz.swapee.wc.IFileEditorComputer>} xyz.swapee.wc.IFileEditorComputer._compute */
/** @typedef {typeof xyz.swapee.wc.IFileEditorComputer.compute} */
/**
 * Used to provide implementations by the source code. Analysed by the controller
 * plugin and transfered into generated code as adapters.
 * @param {xyz.swapee.wc.FileEditorMemory} mem The memory.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorComputer.compute = function(mem) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorComputer
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/03-IFileEditorOuterCore.xml}  01113d201e6af88f72f48de894beef9c */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorOuterCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorOuterCore)} xyz.swapee.wc.AbstractFileEditorOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorOuterCore} xyz.swapee.wc.FileEditorOuterCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorOuterCore` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorOuterCore
 */
xyz.swapee.wc.AbstractFileEditorOuterCore = class extends /** @type {xyz.swapee.wc.AbstractFileEditorOuterCore.constructor&xyz.swapee.wc.FileEditorOuterCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorOuterCore.prototype.constructor = xyz.swapee.wc.AbstractFileEditorOuterCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorOuterCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorOuterCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorOuterCore}
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorOuterCore}
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorOuterCore}
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorOuterCore}
 */
xyz.swapee.wc.AbstractFileEditorOuterCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorOuterCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorOuterCoreCaster)} xyz.swapee.wc.IFileEditorOuterCore.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/**
 * The _IFileEditor_'s outer core that is reflected in the port.
 * @interface xyz.swapee.wc.IFileEditorOuterCore
 */
xyz.swapee.wc.IFileEditorOuterCore = class extends /** @type {xyz.swapee.wc.IFileEditorOuterCore.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorOuterCore.prototype.constructor = xyz.swapee.wc.IFileEditorOuterCore

/** @typedef {function(new: xyz.swapee.wc.IFileEditorOuterCore&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorOuterCore.Initialese>)} xyz.swapee.wc.FileEditorOuterCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorOuterCore} xyz.swapee.wc.IFileEditorOuterCore.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileEditorOuterCore_ instances.
 * @constructor xyz.swapee.wc.FileEditorOuterCore
 * @implements {xyz.swapee.wc.IFileEditorOuterCore} The _IFileEditor_'s outer core that is reflected in the port.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorOuterCore.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorOuterCore = class extends /** @type {xyz.swapee.wc.FileEditorOuterCore.constructor&xyz.swapee.wc.IFileEditorOuterCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorOuterCore.prototype.constructor = xyz.swapee.wc.FileEditorOuterCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorOuterCore}
 */
xyz.swapee.wc.FileEditorOuterCore.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorOuterCore.
 * @interface xyz.swapee.wc.IFileEditorOuterCoreFields
 */
xyz.swapee.wc.IFileEditorOuterCoreFields = class { }
/**
 * The _IFileEditor_'s memory available for manipulation via port buffering.
 */
xyz.swapee.wc.IFileEditorOuterCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IFileEditorOuterCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore} */
xyz.swapee.wc.RecordIFileEditorOuterCore

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore} xyz.swapee.wc.BoundIFileEditorOuterCore */

/** @typedef {xyz.swapee.wc.FileEditorOuterCore} xyz.swapee.wc.BoundFileEditorOuterCore */

/**
 * The host property.
 * @typedef {string}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.Host.host

/**
 * The read content of the file.
 * @typedef {string}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.Content.content

/**
 * The path to the file which acts as an ID.
 * @typedef {string}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.Path.path

/**
 * The name of the file.
 * @typedef {string}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.Filename.filename

/**
 * The language.
 * @typedef {string}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.Lang.lang

/**
 * Whether the file has changes.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges.hasChanges

/**
 * .
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile.readFile

/**
 * .
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile.saveFile

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Host&xyz.swapee.wc.IFileEditorOuterCore.Model.Content&xyz.swapee.wc.IFileEditorOuterCore.Model.Path&xyz.swapee.wc.IFileEditorOuterCore.Model.Filename&xyz.swapee.wc.IFileEditorOuterCore.Model.Lang&xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges&xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile&xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile} xyz.swapee.wc.IFileEditorOuterCore.Model The _IFileEditor_'s memory available for manipulation via port buffering. */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile} xyz.swapee.wc.IFileEditorOuterCore.WeakModel The _IFileEditor_'s memory available for manipulation via port buffering. */

/**
 * Contains getters to cast the _IFileEditorOuterCore_ interface.
 * @interface xyz.swapee.wc.IFileEditorOuterCoreCaster
 */
xyz.swapee.wc.IFileEditorOuterCoreCaster = class { }
/**
 * Cast the _IFileEditorOuterCore_ instance into the _BoundIFileEditorOuterCore_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorOuterCore}
 */
xyz.swapee.wc.IFileEditorOuterCoreCaster.prototype.asIFileEditorOuterCore
/**
 * Access the _FileEditorOuterCore_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorOuterCore}
 */
xyz.swapee.wc.IFileEditorOuterCoreCaster.prototype.superFileEditorOuterCore

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Host The host property (optional overlay).
 * @prop {string} [host=""] The host property. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Host_Safe The host property (required overlay).
 * @prop {string} host The host property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Content The read content of the file (optional overlay).
 * @prop {?string} [content=null] The read content of the file. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Content_Safe The read content of the file (required overlay).
 * @prop {?string} content The read content of the file.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Path The path to the file which acts as an ID (optional overlay).
 * @prop {string} [path=""] The path to the file which acts as an ID. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Path_Safe The path to the file which acts as an ID (required overlay).
 * @prop {string} path The path to the file which acts as an ID.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Filename The name of the file (optional overlay).
 * @prop {string} [filename=""] The name of the file. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Filename_Safe The name of the file (required overlay).
 * @prop {string} filename The name of the file.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Lang The language (optional overlay).
 * @prop {string} [lang=""] The language. Default empty string.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.Lang_Safe The language (required overlay).
 * @prop {string} lang The language.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges Whether the file has changes (optional overlay).
 * @prop {boolean} [hasChanges=false] Whether the file has changes. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges_Safe Whether the file has changes (required overlay).
 * @prop {boolean} hasChanges Whether the file has changes.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile  (optional overlay).
 * @prop {boolean} [readFile=false] . Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile_Safe  (required overlay).
 * @prop {boolean} readFile .
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile  (optional overlay).
 * @prop {boolean} [saveFile=false] . Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile_Safe  (required overlay).
 * @prop {boolean} saveFile .
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host The host property (optional overlay).
 * @prop {*} [host=null] The host property. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host_Safe The host property (required overlay).
 * @prop {*} host The host property.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content The read content of the file (optional overlay).
 * @prop {*} [content=null] The read content of the file. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content_Safe The read content of the file (required overlay).
 * @prop {*} content The read content of the file.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path The path to the file which acts as an ID (optional overlay).
 * @prop {*} [path=null] The path to the file which acts as an ID. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path_Safe The path to the file which acts as an ID (required overlay).
 * @prop {*} path The path to the file which acts as an ID.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename The name of the file (optional overlay).
 * @prop {*} [filename=null] The name of the file. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename_Safe The name of the file (required overlay).
 * @prop {*} filename The name of the file.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang The language (optional overlay).
 * @prop {*} [lang=null] The language. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang_Safe The language (required overlay).
 * @prop {*} lang The language.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges Whether the file has changes (optional overlay).
 * @prop {*} [hasChanges=null] Whether the file has changes. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges_Safe Whether the file has changes (required overlay).
 * @prop {*} hasChanges Whether the file has changes.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile  (optional overlay).
 * @prop {*} [readFile=null] . Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile_Safe  (required overlay).
 * @prop {*} readFile .
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile  (optional overlay).
 * @prop {*} [saveFile=null] . Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile_Safe  (required overlay).
 * @prop {*} saveFile .
 */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host} xyz.swapee.wc.IFileEditorPort.Inputs.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content} xyz.swapee.wc.IFileEditorPort.Inputs.Content The read content of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.Content_Safe The read content of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path} xyz.swapee.wc.IFileEditorPort.Inputs.Path The path to the file which acts as an ID (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.Path_Safe The path to the file which acts as an ID (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename} xyz.swapee.wc.IFileEditorPort.Inputs.Filename The name of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.Filename_Safe The name of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang} xyz.swapee.wc.IFileEditorPort.Inputs.Lang The language (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.Lang_Safe The language (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges} xyz.swapee.wc.IFileEditorPort.Inputs.HasChanges Whether the file has changes (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.HasChanges_Safe Whether the file has changes (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile} xyz.swapee.wc.IFileEditorPort.Inputs.ReadFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.ReadFile_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile} xyz.swapee.wc.IFileEditorPort.Inputs.SaveFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile_Safe} xyz.swapee.wc.IFileEditorPort.Inputs.SaveFile_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host} xyz.swapee.wc.IFileEditorPort.WeakInputs.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Host_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content} xyz.swapee.wc.IFileEditorPort.WeakInputs.Content The read content of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Content_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.Content_Safe The read content of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path} xyz.swapee.wc.IFileEditorPort.WeakInputs.Path The path to the file which acts as an ID (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Path_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.Path_Safe The path to the file which acts as an ID (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename} xyz.swapee.wc.IFileEditorPort.WeakInputs.Filename The name of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Filename_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.Filename_Safe The name of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang} xyz.swapee.wc.IFileEditorPort.WeakInputs.Lang The language (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.Lang_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.Lang_Safe The language (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges} xyz.swapee.wc.IFileEditorPort.WeakInputs.HasChanges Whether the file has changes (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.HasChanges_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.HasChanges_Safe Whether the file has changes (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile} xyz.swapee.wc.IFileEditorPort.WeakInputs.ReadFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.ReadFile_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.ReadFile_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile} xyz.swapee.wc.IFileEditorPort.WeakInputs.SaveFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.WeakModel.SaveFile_Safe} xyz.swapee.wc.IFileEditorPort.WeakInputs.SaveFile_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Host} xyz.swapee.wc.IFileEditorCore.Model.Host The host property (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Host_Safe} xyz.swapee.wc.IFileEditorCore.Model.Host_Safe The host property (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Content} xyz.swapee.wc.IFileEditorCore.Model.Content The read content of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Content_Safe} xyz.swapee.wc.IFileEditorCore.Model.Content_Safe The read content of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Path} xyz.swapee.wc.IFileEditorCore.Model.Path The path to the file which acts as an ID (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Path_Safe} xyz.swapee.wc.IFileEditorCore.Model.Path_Safe The path to the file which acts as an ID (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Filename} xyz.swapee.wc.IFileEditorCore.Model.Filename The name of the file (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Filename_Safe} xyz.swapee.wc.IFileEditorCore.Model.Filename_Safe The name of the file (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Lang} xyz.swapee.wc.IFileEditorCore.Model.Lang The language (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.Lang_Safe} xyz.swapee.wc.IFileEditorCore.Model.Lang_Safe The language (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges} xyz.swapee.wc.IFileEditorCore.Model.HasChanges Whether the file has changes (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.HasChanges_Safe} xyz.swapee.wc.IFileEditorCore.Model.HasChanges_Safe Whether the file has changes (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile} xyz.swapee.wc.IFileEditorCore.Model.ReadFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.ReadFile_Safe} xyz.swapee.wc.IFileEditorCore.Model.ReadFile_Safe  (required overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile} xyz.swapee.wc.IFileEditorCore.Model.SaveFile  (optional overlay). */

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model.SaveFile_Safe} xyz.swapee.wc.IFileEditorCore.Model.SaveFile_Safe  (required overlay). */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/04-IFileEditorPort.xml}  ea50caed56f045c1e0051e926ab8a412 */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IFileEditorPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorPort)} xyz.swapee.wc.AbstractFileEditorPort.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorPort} xyz.swapee.wc.FileEditorPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorPort` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorPort
 */
xyz.swapee.wc.AbstractFileEditorPort = class extends /** @type {xyz.swapee.wc.AbstractFileEditorPort.constructor&xyz.swapee.wc.FileEditorPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorPort.prototype.constructor = xyz.swapee.wc.AbstractFileEditorPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorPort.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorPort|typeof xyz.swapee.wc.FileEditorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorPort}
 */
xyz.swapee.wc.AbstractFileEditorPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorPort}
 */
xyz.swapee.wc.AbstractFileEditorPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorPort|typeof xyz.swapee.wc.FileEditorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorPort}
 */
xyz.swapee.wc.AbstractFileEditorPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorPort|typeof xyz.swapee.wc.FileEditorPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorPort}
 */
xyz.swapee.wc.AbstractFileEditorPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorPort.Initialese[]) => xyz.swapee.wc.IFileEditorPort} xyz.swapee.wc.FileEditorPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorPortFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IFileEditorPort.Inputs>)} xyz.swapee.wc.IFileEditorPort.constructor */
/** @typedef {typeof engineering.type.mvc.IParametric} engineering.type.mvc.IParametric.typeof */
/**
 * The port that serves as an interface to the _IFileEditor_, providing input
 * pins.
 * @interface xyz.swapee.wc.IFileEditorPort
 */
xyz.swapee.wc.IFileEditorPort = class extends /** @type {xyz.swapee.wc.IFileEditorPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorPort.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorPort.resetPort} */
xyz.swapee.wc.IFileEditorPort.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IFileEditorPort.resetFileEditorPort} */
xyz.swapee.wc.IFileEditorPort.prototype.resetFileEditorPort = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorPort&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorPort.Initialese>)} xyz.swapee.wc.FileEditorPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorPort} xyz.swapee.wc.IFileEditorPort.typeof */
/**
 * A concrete class of _IFileEditorPort_ instances.
 * @constructor xyz.swapee.wc.FileEditorPort
 * @implements {xyz.swapee.wc.IFileEditorPort} The port that serves as an interface to the _IFileEditor_, providing input
 * pins.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorPort.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorPort = class extends /** @type {xyz.swapee.wc.FileEditorPort.constructor&xyz.swapee.wc.IFileEditorPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorPort}
 */
xyz.swapee.wc.FileEditorPort.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorPort.
 * @interface xyz.swapee.wc.IFileEditorPortFields
 */
xyz.swapee.wc.IFileEditorPortFields = class { }
/**
 * The inputs to the _IFileEditor_'s controller via its port.
 */
xyz.swapee.wc.IFileEditorPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileEditorPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IFileEditorPortFields.prototype.props = /** @type {!xyz.swapee.wc.IFileEditorPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorPort} */
xyz.swapee.wc.RecordIFileEditorPort

/** @typedef {xyz.swapee.wc.IFileEditorPort} xyz.swapee.wc.BoundIFileEditorPort */

/** @typedef {xyz.swapee.wc.FileEditorPort} xyz.swapee.wc.BoundFileEditorPort */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorOuterCore.WeakModel)} xyz.swapee.wc.IFileEditorPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorOuterCore.WeakModel} xyz.swapee.wc.IFileEditorOuterCore.WeakModel.typeof */
/**
 * The inputs to the _IFileEditor_'s controller via its port.
 * @record xyz.swapee.wc.IFileEditorPort.Inputs
 */
xyz.swapee.wc.IFileEditorPort.Inputs = class extends /** @type {xyz.swapee.wc.IFileEditorPort.Inputs.constructor&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorPort.Inputs.prototype.constructor = xyz.swapee.wc.IFileEditorPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IFileEditorOuterCore.WeakModel)} xyz.swapee.wc.IFileEditorPort.WeakInputs.constructor */
/**
 * The inputs to the _IFileEditor_'s controller via its port.
 * @record xyz.swapee.wc.IFileEditorPort.WeakInputs
 */
xyz.swapee.wc.IFileEditorPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IFileEditorPort.WeakInputs.constructor&xyz.swapee.wc.IFileEditorOuterCore.WeakModel.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IFileEditorPort.WeakInputs

/**
 * The port interface.
 * @interface xyz.swapee.wc.IFileEditorPortInterface
 */
xyz.swapee.wc.IFileEditorPortInterface = class {
  constructor() {
    this.props=/** @type {!xyz.swapee.wc.IFileEditorPortInterface.Props} */ (void 0)
  }
}
xyz.swapee.wc.IFileEditorPortInterface.prototype.constructor = xyz.swapee.wc.IFileEditorPortInterface

/**
 * A concrete class of _IFileEditorPortInterface_ instances.
 * @constructor xyz.swapee.wc.FileEditorPortInterface
 * @implements {xyz.swapee.wc.IFileEditorPortInterface} The port interface.
 */
xyz.swapee.wc.FileEditorPortInterface = class extends xyz.swapee.wc.IFileEditorPortInterface { }
xyz.swapee.wc.FileEditorPortInterface.prototype.constructor = xyz.swapee.wc.FileEditorPortInterface

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorPortInterface.Props
 * @prop {string} host The host property.
 * @prop {?string} content The read content of the file.
 * @prop {string} path The path to the file which acts as an ID.
 * @prop {string} filename The name of the file.
 * @prop {string} lang The language.
 * @prop {boolean} hasChanges Whether the file has changes.
 * @prop {boolean} readFile .
 * @prop {boolean} saveFile .
 */

/**
 * Contains getters to cast the _IFileEditorPort_ interface.
 * @interface xyz.swapee.wc.IFileEditorPortCaster
 */
xyz.swapee.wc.IFileEditorPortCaster = class { }
/**
 * Cast the _IFileEditorPort_ instance into the _BoundIFileEditorPort_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorPort}
 */
xyz.swapee.wc.IFileEditorPortCaster.prototype.asIFileEditorPort
/**
 * Access the _FileEditorPort_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorPort}
 */
xyz.swapee.wc.IFileEditorPortCaster.prototype.superFileEditorPort

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorPort.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorPort.__resetPort<!xyz.swapee.wc.IFileEditorPort>} xyz.swapee.wc.IFileEditorPort._resetPort */
/** @typedef {typeof xyz.swapee.wc.IFileEditorPort.resetPort} */
/**
 * Resets the _IFileEditor_ port.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorPort.resetPort = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorPort.__resetFileEditorPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorPort.__resetFileEditorPort<!xyz.swapee.wc.IFileEditorPort>} xyz.swapee.wc.IFileEditorPort._resetFileEditorPort */
/** @typedef {typeof xyz.swapee.wc.IFileEditorPort.resetFileEditorPort} */
/**
 * Resets the port to the default values.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorPort.resetFileEditorPort = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorPort
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/09-IFileEditorCore.xml}  d57f0bcce074566bba61d095b5d086a9 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorCore.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorCore)} xyz.swapee.wc.AbstractFileEditorCore.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorCore} xyz.swapee.wc.FileEditorCore.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorCore` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorCore
 */
xyz.swapee.wc.AbstractFileEditorCore = class extends /** @type {xyz.swapee.wc.AbstractFileEditorCore.constructor&xyz.swapee.wc.FileEditorCore.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorCore.prototype.constructor = xyz.swapee.wc.AbstractFileEditorCore
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorCore.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorCore} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorCore}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorCore.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorCore}
 */
xyz.swapee.wc.AbstractFileEditorCore.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorCore}
 */
xyz.swapee.wc.AbstractFileEditorCore.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorCore}
 */
xyz.swapee.wc.AbstractFileEditorCore.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorOuterCore|typeof xyz.swapee.wc.FileEditorOuterCore)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorCore}
 */
xyz.swapee.wc.AbstractFileEditorCore.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorCoreFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorCoreCaster&xyz.swapee.wc.IFileEditorOuterCore)} xyz.swapee.wc.IFileEditorCore.constructor */
/**
 * Stores data in memory registers.
 * @interface xyz.swapee.wc.IFileEditorCore
 */
xyz.swapee.wc.IFileEditorCore = class extends /** @type {xyz.swapee.wc.IFileEditorCore.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileEditorOuterCore.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileEditorCore.resetCore} */
xyz.swapee.wc.IFileEditorCore.prototype.resetCore = function() {}
/** @type {xyz.swapee.wc.IFileEditorCore.resetFileEditorCore} */
xyz.swapee.wc.IFileEditorCore.prototype.resetFileEditorCore = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorCore&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorCore.Initialese>)} xyz.swapee.wc.FileEditorCore.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorCore} xyz.swapee.wc.IFileEditorCore.typeof */
/**
 * A concrete class of _IFileEditorCore_ instances.
 * @constructor xyz.swapee.wc.FileEditorCore
 * @implements {xyz.swapee.wc.IFileEditorCore} Stores data in memory registers.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorCore.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorCore = class extends /** @type {xyz.swapee.wc.FileEditorCore.constructor&xyz.swapee.wc.IFileEditorCore.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorCore.prototype.constructor = xyz.swapee.wc.FileEditorCore
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorCore}
 */
xyz.swapee.wc.FileEditorCore.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorCore.
 * @interface xyz.swapee.wc.IFileEditorCoreFields
 */
xyz.swapee.wc.IFileEditorCoreFields = class { }
/**
 * The _IFileEditor_'s memory.
 */
xyz.swapee.wc.IFileEditorCoreFields.prototype.model = /** @type {!xyz.swapee.wc.IFileEditorCore.Model} */ (void 0)
/**
 * Props to VSCode's param name suggestion on JSX tags.
 */
xyz.swapee.wc.IFileEditorCoreFields.prototype.props = /** @type {xyz.swapee.wc.IFileEditorCore.Model} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorCore} */
xyz.swapee.wc.RecordIFileEditorCore

/** @typedef {xyz.swapee.wc.IFileEditorCore} xyz.swapee.wc.BoundIFileEditorCore */

/** @typedef {xyz.swapee.wc.FileEditorCore} xyz.swapee.wc.BoundFileEditorCore */

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorCore.Model.LoadingReadFile.loadingReadFile

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorCore.Model.HasMoreReadFile.hasMoreReadFile

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.IFileEditorCore.Model.LoadReadFileError.loadReadFileError

/**
 * Whether the items are being loaded from the remote.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorCore.Model.LoadingSaveFile.loadingSaveFile

/**
 * Whether there are more items to load.
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorCore.Model.HasMoreSaveFile.hasMoreSaveFile

/**
 * An error during loading of items from the remote.
 * @typedef {Error}
 */
xyz.swapee.wc.IFileEditorCore.Model.LoadSaveFileError.loadSaveFileError

/** @typedef {xyz.swapee.wc.IFileEditorOuterCore.Model&xyz.swapee.wc.IFileEditorCore.Model.LoadingReadFile&xyz.swapee.wc.IFileEditorCore.Model.HasMoreReadFile&xyz.swapee.wc.IFileEditorCore.Model.LoadReadFileError&xyz.swapee.wc.IFileEditorCore.Model.LoadingSaveFile&xyz.swapee.wc.IFileEditorCore.Model.HasMoreSaveFile&xyz.swapee.wc.IFileEditorCore.Model.LoadSaveFileError} xyz.swapee.wc.IFileEditorCore.Model The _IFileEditor_'s memory. */

/**
 * Contains getters to cast the _IFileEditorCore_ interface.
 * @interface xyz.swapee.wc.IFileEditorCoreCaster
 */
xyz.swapee.wc.IFileEditorCoreCaster = class { }
/**
 * Cast the _IFileEditorCore_ instance into the _BoundIFileEditorCore_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorCore}
 */
xyz.swapee.wc.IFileEditorCoreCaster.prototype.asIFileEditorCore
/**
 * Access the _FileEditorCore_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorCore}
 */
xyz.swapee.wc.IFileEditorCoreCaster.prototype.superFileEditorCore

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadingReadFile Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingReadFile=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadingReadFile_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingReadFile Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.HasMoreReadFile Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreReadFile=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.HasMoreReadFile_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreReadFile Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadReadFileError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadReadFileError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadReadFileError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadReadFileError An error during loading of items from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadingSaveFile Whether the items are being loaded from the remote (optional overlay).
 * @prop {boolean} [loadingSaveFile=false] Whether the items are being loaded from the remote. Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadingSaveFile_Safe Whether the items are being loaded from the remote (required overlay).
 * @prop {boolean} loadingSaveFile Whether the items are being loaded from the remote.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.HasMoreSaveFile Whether there are more items to load (optional overlay).
 * @prop {?boolean} [hasMoreSaveFile=null] Whether there are more items to load. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.HasMoreSaveFile_Safe Whether there are more items to load (required overlay).
 * @prop {?boolean} hasMoreSaveFile Whether there are more items to load.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadSaveFileError An error during loading of items from the remote (optional overlay).
 * @prop {?Error} [loadSaveFileError=null] An error during loading of items from the remote. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorCore.Model.LoadSaveFileError_Safe An error during loading of items from the remote (required overlay).
 * @prop {?Error} loadSaveFileError An error during loading of items from the remote.
 */

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorCore.__resetCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorCore.__resetCore<!xyz.swapee.wc.IFileEditorCore>} xyz.swapee.wc.IFileEditorCore._resetCore */
/** @typedef {typeof xyz.swapee.wc.IFileEditorCore.resetCore} */
/**
 * Resets the _IFileEditor_ core.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorCore.resetCore = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorCore.__resetFileEditorCore
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorCore.__resetFileEditorCore<!xyz.swapee.wc.IFileEditorCore>} xyz.swapee.wc.IFileEditorCore._resetFileEditorCore */
/** @typedef {typeof xyz.swapee.wc.IFileEditorCore.resetFileEditorCore} */
/**
 * Resets the core to the default values.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorCore.resetFileEditorCore = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorCore
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/10-IFileEditorProcessor.xml}  f7dfb59d1c7463b05a42681b7fd9f5fa */
/** @typedef {xyz.swapee.wc.IFileEditorComputer.Initialese&xyz.swapee.wc.IFileEditorController.Initialese} xyz.swapee.wc.IFileEditorProcessor.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorProcessor)} xyz.swapee.wc.AbstractFileEditorProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorProcessor} xyz.swapee.wc.FileEditorProcessor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorProcessor` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorProcessor
 */
xyz.swapee.wc.AbstractFileEditorProcessor = class extends /** @type {xyz.swapee.wc.AbstractFileEditorProcessor.constructor&xyz.swapee.wc.FileEditorProcessor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorProcessor.prototype.constructor = xyz.swapee.wc.AbstractFileEditorProcessor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorProcessor.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorProcessor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorProcessor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorProcessor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorProcessor}
 */
xyz.swapee.wc.AbstractFileEditorProcessor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorProcessor}
 */
xyz.swapee.wc.AbstractFileEditorProcessor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorProcessor}
 */
xyz.swapee.wc.AbstractFileEditorProcessor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorCore|typeof xyz.swapee.wc.FileEditorCore)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorProcessor}
 */
xyz.swapee.wc.AbstractFileEditorProcessor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorProcessor.Initialese[]) => xyz.swapee.wc.IFileEditorProcessor} xyz.swapee.wc.FileEditorProcessorConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorProcessorCaster&xyz.swapee.wc.IFileEditorComputer&xyz.swapee.wc.IFileEditorCore&xyz.swapee.wc.IFileEditorController)} xyz.swapee.wc.IFileEditorProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController} xyz.swapee.wc.IFileEditorController.typeof */
/**
 * The processor to compute changes to the memory for the _IFileEditor_.
 * @interface xyz.swapee.wc.IFileEditorProcessor
 */
xyz.swapee.wc.IFileEditorProcessor = class extends /** @type {xyz.swapee.wc.IFileEditorProcessor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileEditorComputer.typeof&xyz.swapee.wc.IFileEditorCore.typeof&xyz.swapee.wc.IFileEditorController.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorProcessor.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorProcessor.pulseReadFile} */
xyz.swapee.wc.IFileEditorProcessor.prototype.pulseReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorProcessor.pulseSaveFile} */
xyz.swapee.wc.IFileEditorProcessor.prototype.pulseSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorProcessor&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorProcessor.Initialese>)} xyz.swapee.wc.FileEditorProcessor.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorProcessor} xyz.swapee.wc.IFileEditorProcessor.typeof */
/**
 * A concrete class of _IFileEditorProcessor_ instances.
 * @constructor xyz.swapee.wc.FileEditorProcessor
 * @implements {xyz.swapee.wc.IFileEditorProcessor} The processor to compute changes to the memory for the _IFileEditor_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorProcessor.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorProcessor = class extends /** @type {xyz.swapee.wc.FileEditorProcessor.constructor&xyz.swapee.wc.IFileEditorProcessor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorProcessor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorProcessor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorProcessor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorProcessor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorProcessor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorProcessor}
 */
xyz.swapee.wc.FileEditorProcessor.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorProcessor} */
xyz.swapee.wc.RecordIFileEditorProcessor

/** @typedef {xyz.swapee.wc.IFileEditorProcessor} xyz.swapee.wc.BoundIFileEditorProcessor */

/** @typedef {xyz.swapee.wc.FileEditorProcessor} xyz.swapee.wc.BoundFileEditorProcessor */

/**
 * Contains getters to cast the _IFileEditorProcessor_ interface.
 * @interface xyz.swapee.wc.IFileEditorProcessorCaster
 */
xyz.swapee.wc.IFileEditorProcessorCaster = class { }
/**
 * Cast the _IFileEditorProcessor_ instance into the _BoundIFileEditorProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorProcessor}
 */
xyz.swapee.wc.IFileEditorProcessorCaster.prototype.asIFileEditorProcessor
/**
 * Access the _FileEditorProcessor_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorProcessor}
 */
xyz.swapee.wc.IFileEditorProcessorCaster.prototype.superFileEditorProcessor

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorProcessor.__pulseReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorProcessor.__pulseReadFile<!xyz.swapee.wc.IFileEditorProcessor>} xyz.swapee.wc.IFileEditorProcessor._pulseReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorProcessor.pulseReadFile} */
/**
 * A method called to set the `readFile` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorProcessor.pulseReadFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorProcessor.__pulseSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorProcessor.__pulseSaveFile<!xyz.swapee.wc.IFileEditorProcessor>} xyz.swapee.wc.IFileEditorProcessor._pulseSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorProcessor.pulseSaveFile} */
/**
 * A method called to set the `saveFile` to _True_ and then
 * immediately reset it to _False_.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorProcessor.pulseSaveFile = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorProcessor
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/100-FileEditorMemory.xml}  111d1e100b68d376710f7a3da2045fdd */
/**
 * The memory of the _IFileEditor_ which can also be read by external
 * components.
 * @record xyz.swapee.wc.FileEditorMemory
 */
xyz.swapee.wc.FileEditorMemory = class { }
/**
 * The host property. Default empty string.
 */
xyz.swapee.wc.FileEditorMemory.prototype.host = /** @type {string} */ (void 0)
/**
 * The read content of the file. Default `null`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.content = /** @type {?string} */ (void 0)
/**
 * The path to the file which acts as an ID. Default empty string.
 */
xyz.swapee.wc.FileEditorMemory.prototype.path = /** @type {string} */ (void 0)
/**
 * The name of the file. Default empty string.
 */
xyz.swapee.wc.FileEditorMemory.prototype.filename = /** @type {string} */ (void 0)
/**
 * The language. Default empty string.
 */
xyz.swapee.wc.FileEditorMemory.prototype.lang = /** @type {string} */ (void 0)
/**
 * Whether the file has changes. Default `false`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.hasChanges = /** @type {boolean} */ (void 0)
/**
 * . Default `false`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.readFile = /** @type {boolean} */ (void 0)
/**
 * . Default `false`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.saveFile = /** @type {boolean} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.loadingReadFile = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.hasMoreReadFile = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.loadReadFileError = /** @type {?Error} */ (void 0)
/**
 * Whether the items are being loaded from the remote. Default `false`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.loadingSaveFile = /** @type {boolean} */ (void 0)
/**
 * Whether there are more items to load. Default `null`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.hasMoreSaveFile = /** @type {?boolean} */ (void 0)
/**
 * An error during loading of items from the remote. Default `null`.
 */
xyz.swapee.wc.FileEditorMemory.prototype.loadSaveFileError = /** @type {?Error} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/102-FileEditorInputs.xml}  bb0969a5f0c4c8cf1bdd172d23ea55e0 */
/**
 * The inputs of the _IFileEditor_ to transfer to the backend port.
 * @record xyz.swapee.wc.front.FileEditorInputs
 */
xyz.swapee.wc.front.FileEditorInputs = class { }
/**
 * The host property. Default empty string.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.host = /** @type {string|undefined} */ (void 0)
/**
 * The read content of the file. Default `null`.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.content = /** @type {(?string)|undefined} */ (void 0)
/**
 * The path to the file which acts as an ID. Default empty string.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.path = /** @type {string|undefined} */ (void 0)
/**
 * The name of the file. Default empty string.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.filename = /** @type {string|undefined} */ (void 0)
/**
 * The language. Default empty string.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.lang = /** @type {string|undefined} */ (void 0)
/**
 * Whether the file has changes. Default `false`.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.hasChanges = /** @type {boolean|undefined} */ (void 0)
/**
 * . Default `false`.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.readFile = /** @type {boolean|undefined} */ (void 0)
/**
 * . Default `false`.
 */
xyz.swapee.wc.front.FileEditorInputs.prototype.saveFile = /** @type {boolean|undefined} */ (void 0)

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/11-IFileEditor.xml}  e9949856808f08a24b7aaf327677dbb8 */
/**
 * An atomic wrapper for the _IFileEditor_ as a dependency to be referenced
 * in other components' milieux.
 * @record xyz.swapee.wc.FileEditorEnv
 */
xyz.swapee.wc.FileEditorEnv = class { }
/**
 * A component description.
 */
xyz.swapee.wc.FileEditorEnv.prototype.fileEditor = /** @type {xyz.swapee.wc.IFileEditor} */ (void 0)

/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorController.Inputs>&xyz.swapee.wc.IFileEditorProcessor.Initialese&xyz.swapee.wc.IFileEditorComputer.Initialese&xyz.swapee.wc.IFileEditorController.Initialese} xyz.swapee.wc.IFileEditor.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileEditor)} xyz.swapee.wc.AbstractFileEditor.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditor} xyz.swapee.wc.FileEditor.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditor` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditor
 */
xyz.swapee.wc.AbstractFileEditor = class extends /** @type {xyz.swapee.wc.AbstractFileEditor.constructor&xyz.swapee.wc.FileEditor.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditor.prototype.constructor = xyz.swapee.wc.AbstractFileEditor
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditor.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditor} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditor}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditor.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditor}
 */
xyz.swapee.wc.AbstractFileEditor.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditor}
 */
xyz.swapee.wc.AbstractFileEditor.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditor}
 */
xyz.swapee.wc.AbstractFileEditor.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditor}
 */
xyz.swapee.wc.AbstractFileEditor.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditor.Initialese[]) => xyz.swapee.wc.IFileEditor} xyz.swapee.wc.FileEditorConstructor */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditor.MVCOptions Options for establishing an MVC triad against an element on the page.
 * @prop {!xyz.swapee.wc.IFileEditor.Pinout} circuits Connections to the parent's port that will translate into the component's
 * own port.
 * @prop {!xyz.swapee.wc.IFileEditor.Pinout} [props] The inputs to pass to the port.
 * @prop {!xyz.swapee.wc.IFileEditor.Pinout} [derivedProps] Props that will be derived from a parent.
 * @prop {!xyz.swapee.wc.FileEditorMemory} [state] The initial component state that acts as a model.
 * @prop {!xyz.swapee.wc.FileEditorClasses} [classes] The hashmap of compiled (values) against virtual (keys) class names.
 */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorCaster&xyz.swapee.wc.IFileEditorProcessor&xyz.swapee.wc.IFileEditorComputer&xyz.swapee.wc.IFileEditorController&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorController.Inputs, null>)} xyz.swapee.wc.IFileEditor.constructor */
/** @typedef {typeof guest.maurice.IIntegratedComponent} guest.maurice.IIntegratedComponent.typeof */
/**
 * A component description.
 * @interface xyz.swapee.wc.IFileEditor
 */
xyz.swapee.wc.IFileEditor = class extends /** @type {xyz.swapee.wc.IFileEditor.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileEditorProcessor.typeof&xyz.swapee.wc.IFileEditorComputer.typeof&xyz.swapee.wc.IFileEditorController.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditor.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditor&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditor.Initialese>)} xyz.swapee.wc.FileEditor.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditor} xyz.swapee.wc.IFileEditor.typeof */
/**
 * A concrete class of _IFileEditor_ instances.
 * @constructor xyz.swapee.wc.FileEditor
 * @implements {xyz.swapee.wc.IFileEditor} A component description.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditor.Initialese>} ‎
 */
xyz.swapee.wc.FileEditor = class extends /** @type {xyz.swapee.wc.FileEditor.constructor&xyz.swapee.wc.IFileEditor.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditor* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditor.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditor* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditor.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditor.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditor}
 */
xyz.swapee.wc.FileEditor.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditor.
 * @interface xyz.swapee.wc.IFileEditorFields
 */
xyz.swapee.wc.IFileEditorFields = class { }
/**
 * The input pins of the _IFileEditor_ port.
 */
xyz.swapee.wc.IFileEditorFields.prototype.pinout = /** @type {!xyz.swapee.wc.IFileEditor.Pinout} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditor} */
xyz.swapee.wc.RecordIFileEditor

/** @typedef {xyz.swapee.wc.IFileEditor} xyz.swapee.wc.BoundIFileEditor */

/** @typedef {xyz.swapee.wc.FileEditor} xyz.swapee.wc.BoundFileEditor */

/** @typedef {xyz.swapee.wc.IFileEditorController.Inputs} xyz.swapee.wc.IFileEditor.Pinout The input pins of the _IFileEditor_ port. */

/** @typedef {function(new: engineering.type.mvc.IRegulator<!xyz.swapee.wc.IFileEditorController.Inputs>)} xyz.swapee.wc.IFileEditorBuffer.constructor */
/** @typedef {typeof engineering.type.mvc.IRegulator} engineering.type.mvc.IRegulator.typeof */
/**
 * The special type for port controller buffers to export them as modlets.
 * @interface xyz.swapee.wc.IFileEditorBuffer
 */
xyz.swapee.wc.IFileEditorBuffer = class extends /** @type {xyz.swapee.wc.IFileEditorBuffer.constructor&engineering.type.mvc.IRegulator.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorBuffer.prototype.constructor = xyz.swapee.wc.IFileEditorBuffer

/**
 * A concrete class of _IFileEditorBuffer_ instances.
 * @constructor xyz.swapee.wc.FileEditorBuffer
 * @implements {xyz.swapee.wc.IFileEditorBuffer} The special type for port controller buffers to export them as modlets.
 */
xyz.swapee.wc.FileEditorBuffer = class extends xyz.swapee.wc.IFileEditorBuffer { }
xyz.swapee.wc.FileEditorBuffer.prototype.constructor = xyz.swapee.wc.FileEditorBuffer

/**
 * Contains getters to cast the _IFileEditor_ interface.
 * @interface xyz.swapee.wc.IFileEditorCaster
 */
xyz.swapee.wc.IFileEditorCaster = class { }
/**
 * Cast the _IFileEditor_ instance into the _BoundIFileEditor_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditor}
 */
xyz.swapee.wc.IFileEditorCaster.prototype.asIFileEditor
/**
 * Access the _FileEditor_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditor}
 */
xyz.swapee.wc.IFileEditorCaster.prototype.superFileEditor

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/110-FileEditorSerDes.xml}  fbc2c8d5da4c0060dc3ab0b5949d6eb6 */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileEditorMemoryPQs
 */
xyz.swapee.wc.FileEditorMemoryPQs = class {
  constructor() {
    /**
     * `ja036`
     */
    this.content=/** @type {string} */ (void 0)
    /**
     * `d6fe1`
     */
    this.path=/** @type {string} */ (void 0)
    /**
     * `e35ed`
     */
    this.filename=/** @type {string} */ (void 0)
    /**
     * `j3f80`
     */
    this.hasChanges=/** @type {string} */ (void 0)
    /**
     * `dcc6a`
     */
    this.readFile=/** @type {string} */ (void 0)
    /**
     * `b182d`
     */
    this.saveFile=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileEditorMemoryPQs.prototype.constructor = xyz.swapee.wc.FileEditorMemoryPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileEditorMemoryQPs
 * @dict
 */
xyz.swapee.wc.FileEditorMemoryQPs = class { }
/**
 * `content`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.ja036 = /** @type {string} */ (void 0)
/**
 * `path`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.d6fe1 = /** @type {string} */ (void 0)
/**
 * `filename`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.e35ed = /** @type {string} */ (void 0)
/**
 * `hasChanges`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.j3f80 = /** @type {string} */ (void 0)
/**
 * `readFile`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.dcc6a = /** @type {string} */ (void 0)
/**
 * `saveFile`
 */
xyz.swapee.wc.FileEditorMemoryQPs.prototype.b182d = /** @type {string} */ (void 0)

/** @typedef {function(new: xyz.swapee.wc.FileEditorMemoryPQs)} xyz.swapee.wc.FileEditorInputsPQs.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorMemoryPQs} xyz.swapee.wc.FileEditorMemoryPQs.typeof */
/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileEditorInputsPQs
 */
xyz.swapee.wc.FileEditorInputsPQs = class extends /** @type {xyz.swapee.wc.FileEditorInputsPQs.constructor&xyz.swapee.wc.FileEditorMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorInputsPQs.prototype.constructor = xyz.swapee.wc.FileEditorInputsPQs

/** @typedef {function(new: xyz.swapee.wc.FileEditorMemoryPQs)} xyz.swapee.wc.FileEditorInputsQPs.constructor */
/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileEditorInputsQPs
 * @dict
 */
xyz.swapee.wc.FileEditorInputsQPs = class extends /** @type {xyz.swapee.wc.FileEditorInputsQPs.constructor&xyz.swapee.wc.FileEditorMemoryPQs.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorInputsQPs.prototype.constructor = xyz.swapee.wc.FileEditorInputsQPs

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileEditorCachePQs
 */
xyz.swapee.wc.FileEditorCachePQs = class {
  constructor() {
    /**
     * `h4bbb`
     */
    this.loadingReadFile=/** @type {string} */ (void 0)
    /**
     * `i1758`
     */
    this.hasMoreReadFile=/** @type {string} */ (void 0)
    /**
     * `c9e55`
     */
    this.loadReadFileError=/** @type {string} */ (void 0)
    /**
     * `ca8db`
     */
    this.loadingSaveFile=/** @type {string} */ (void 0)
    /**
     * `dba15`
     */
    this.hasMoreSaveFile=/** @type {string} */ (void 0)
    /**
     * `c3052`
     */
    this.loadSaveFileError=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileEditorCachePQs.prototype.constructor = xyz.swapee.wc.FileEditorCachePQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileEditorCacheQPs
 * @dict
 */
xyz.swapee.wc.FileEditorCacheQPs = class { }
/**
 * `loadingReadFile`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.h4bbb = /** @type {string} */ (void 0)
/**
 * `hasMoreReadFile`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.i1758 = /** @type {string} */ (void 0)
/**
 * `loadReadFileError`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.c9e55 = /** @type {string} */ (void 0)
/**
 * `loadingSaveFile`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.ca8db = /** @type {string} */ (void 0)
/**
 * `hasMoreSaveFile`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.dba15 = /** @type {string} */ (void 0)
/**
 * `loadSaveFileError`
 */
xyz.swapee.wc.FileEditorCacheQPs.prototype.c3052 = /** @type {string} */ (void 0)

/**
 * Contains stable substitution keys for serialisation _into_ inter-address
 * space data-exchange format.
 * @interface xyz.swapee.wc.FileEditorVdusPQs
 */
xyz.swapee.wc.FileEditorVdusPQs = class {
  constructor() {
    /**
     * `fc4a1`
     */
    this.Editor=/** @type {string} */ (void 0)
  }
}
xyz.swapee.wc.FileEditorVdusPQs.prototype.constructor = xyz.swapee.wc.FileEditorVdusPQs

/**
 * Contains stable substitution keys for deserialisation _from_ inter-address
 * space data-exchange format.
 * @constructor xyz.swapee.wc.FileEditorVdusQPs
 * @dict
 */
xyz.swapee.wc.FileEditorVdusQPs = class { }
/**
 * `Editor`
 */
xyz.swapee.wc.FileEditorVdusQPs.prototype.fc4a1 = /** @type {string} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/12-IFileEditorHtmlComponent.xml}  90f2ef7bc5e5b3149146e96034d1b381 */
/** @typedef {xyz.swapee.wc.back.IFileEditorController.Initialese&xyz.swapee.wc.back.IFileEditorScreen.Initialese&xyz.swapee.wc.IFileEditor.Initialese&xyz.swapee.wc.IFileEditorGPU.Initialese&com.webcircuits.IHtmlComponent.Initialese&xyz.swapee.wc.IFileEditorProcessor.Initialese&xyz.swapee.wc.IFileEditorComputer.Initialese} xyz.swapee.wc.IFileEditorHtmlComponent.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorHtmlComponent)} xyz.swapee.wc.AbstractFileEditorHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorHtmlComponent} xyz.swapee.wc.FileEditorHtmlComponent.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorHtmlComponent` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorHtmlComponent
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent = class extends /** @type {xyz.swapee.wc.AbstractFileEditorHtmlComponent.constructor&xyz.swapee.wc.FileEditorHtmlComponent.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorHtmlComponent.prototype.constructor = xyz.swapee.wc.AbstractFileEditorHtmlComponent
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorHtmlComponent} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorHtmlComponent|typeof xyz.swapee.wc.FileEditorHtmlComponent)|(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorHtmlComponent}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorHtmlComponent}
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorHtmlComponent}
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorHtmlComponent|typeof xyz.swapee.wc.FileEditorHtmlComponent)|(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorHtmlComponent}
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorHtmlComponent|typeof xyz.swapee.wc.FileEditorHtmlComponent)|(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.IFileEditor|typeof xyz.swapee.wc.FileEditor)|(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IHtmlComponent|typeof com.webcircuits.HtmlComponent)|(!xyz.swapee.wc.IFileEditorProcessor|typeof xyz.swapee.wc.FileEditorProcessor)|(!xyz.swapee.wc.IFileEditorComputer|typeof xyz.swapee.wc.FileEditorComputer)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorHtmlComponent}
 */
xyz.swapee.wc.AbstractFileEditorHtmlComponent.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorHtmlComponent.Initialese[]) => xyz.swapee.wc.IFileEditorHtmlComponent} xyz.swapee.wc.FileEditorHtmlComponentConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorHtmlComponentCaster&xyz.swapee.wc.back.IFileEditorController&xyz.swapee.wc.back.IFileEditorScreen&xyz.swapee.wc.IFileEditor&xyz.swapee.wc.IFileEditorGPU&com.webcircuits.IHtmlComponent<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorController.Inputs, !HTMLDivElement, null>&xyz.swapee.wc.IFileEditorProcessor&xyz.swapee.wc.IFileEditorComputer)} xyz.swapee.wc.IFileEditorHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorController} xyz.swapee.wc.back.IFileEditorController.typeof */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorScreen} xyz.swapee.wc.back.IFileEditorScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileEditorGPU} xyz.swapee.wc.IFileEditorGPU.typeof */
/** @typedef {typeof com.webcircuits.IHtmlComponent} com.webcircuits.IHtmlComponent.typeof */
/**
 * The _IFileEditor_'s HTML component for the web page.
 * @interface xyz.swapee.wc.IFileEditorHtmlComponent
 */
xyz.swapee.wc.IFileEditorHtmlComponent = class extends /** @type {xyz.swapee.wc.IFileEditorHtmlComponent.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IFileEditorController.typeof&xyz.swapee.wc.back.IFileEditorScreen.typeof&xyz.swapee.wc.IFileEditor.typeof&xyz.swapee.wc.IFileEditorGPU.typeof&com.webcircuits.IHtmlComponent.typeof&xyz.swapee.wc.IFileEditorProcessor.typeof&xyz.swapee.wc.IFileEditorComputer.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorHtmlComponent.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorHtmlComponent&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorHtmlComponent.Initialese>)} xyz.swapee.wc.FileEditorHtmlComponent.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorHtmlComponent} xyz.swapee.wc.IFileEditorHtmlComponent.typeof */
/**
 * A concrete class of _IFileEditorHtmlComponent_ instances.
 * @constructor xyz.swapee.wc.FileEditorHtmlComponent
 * @implements {xyz.swapee.wc.IFileEditorHtmlComponent} The _IFileEditor_'s HTML component for the web page.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorHtmlComponent.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorHtmlComponent = class extends /** @type {xyz.swapee.wc.FileEditorHtmlComponent.constructor&xyz.swapee.wc.IFileEditorHtmlComponent.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorHtmlComponent* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorHtmlComponent.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorHtmlComponent* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorHtmlComponent.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorHtmlComponent.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorHtmlComponent}
 */
xyz.swapee.wc.FileEditorHtmlComponent.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorHtmlComponent} */
xyz.swapee.wc.RecordIFileEditorHtmlComponent

/** @typedef {xyz.swapee.wc.IFileEditorHtmlComponent} xyz.swapee.wc.BoundIFileEditorHtmlComponent */

/** @typedef {xyz.swapee.wc.FileEditorHtmlComponent} xyz.swapee.wc.BoundFileEditorHtmlComponent */

/**
 * Contains getters to cast the _IFileEditorHtmlComponent_ interface.
 * @interface xyz.swapee.wc.IFileEditorHtmlComponentCaster
 */
xyz.swapee.wc.IFileEditorHtmlComponentCaster = class { }
/**
 * Cast the _IFileEditorHtmlComponent_ instance into the _BoundIFileEditorHtmlComponent_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorHtmlComponent}
 */
xyz.swapee.wc.IFileEditorHtmlComponentCaster.prototype.asIFileEditorHtmlComponent
/**
 * Access the _FileEditorHtmlComponent_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorHtmlComponent}
 */
xyz.swapee.wc.IFileEditorHtmlComponentCaster.prototype.superFileEditorHtmlComponent

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/130-IFileEditorElement.xml}  7c2909cb4411989d063ae598594c256d */
/** @typedef {guest.maurice.IIntegratedComponent.Initialese<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorElement.Inputs>&_findesiècle.IHTMLBlocker.Initialese&guest.maurice.IGuest.Initialese} xyz.swapee.wc.IFileEditorElement.Initialese The initialiser. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorElement)} xyz.swapee.wc.AbstractFileEditorElement.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorElement} xyz.swapee.wc.FileEditorElement.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorElement` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorElement
 */
xyz.swapee.wc.AbstractFileEditorElement = class extends /** @type {xyz.swapee.wc.AbstractFileEditorElement.constructor&xyz.swapee.wc.FileEditorElement.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorElement.prototype.constructor = xyz.swapee.wc.AbstractFileEditorElement
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorElement.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorElement} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorElement|typeof xyz.swapee.wc.FileEditorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElement}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorElement.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorElement}
 */
xyz.swapee.wc.AbstractFileEditorElement.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElement}
 */
xyz.swapee.wc.AbstractFileEditorElement.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorElement|typeof xyz.swapee.wc.FileEditorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElement}
 */
xyz.swapee.wc.AbstractFileEditorElement.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorElement|typeof xyz.swapee.wc.FileEditorElement)|(!_findesiècle.IHTMLBlocker|typeof _findesiècle.HTMLBlocker)|(!guest.maurice.IGuest|typeof guest.maurice.Guest)|(!guest.maurice.IIntegratedComponent|typeof guest.maurice.IntegratedComponent)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElement}
 */
xyz.swapee.wc.AbstractFileEditorElement.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorElement.Initialese[]) => xyz.swapee.wc.IFileEditorElement} xyz.swapee.wc.FileEditorElementConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElementFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorElementCaster&_findesiècle.IHTMLBlocker<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorElement.Inputs>&guest.maurice.IGuest&guest.maurice.IIntegratedComponent<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.IFileEditorElement.Inputs, null>)} xyz.swapee.wc.IFileEditorElement.constructor */
/** @typedef {typeof _findesiècle.IHTMLBlocker} _findesiècle.IHTMLBlocker.typeof */
/** @typedef {typeof guest.maurice.IGuest} guest.maurice.IGuest.typeof */
/**
 * A component description.
 *
 * The _IFileEditor_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @interface xyz.swapee.wc.IFileEditorElement
 */
xyz.swapee.wc.IFileEditorElement = class extends /** @type {xyz.swapee.wc.IFileEditorElement.constructor&engineering.type.IEngineer.typeof&_findesiècle.IHTMLBlocker.typeof&guest.maurice.IGuest.typeof&guest.maurice.IIntegratedComponent.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorElement.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorElement.solder} */
xyz.swapee.wc.IFileEditorElement.prototype.solder = function() {}
/** @type {xyz.swapee.wc.IFileEditorElement.render} */
xyz.swapee.wc.IFileEditorElement.prototype.render = function() {}
/** @type {xyz.swapee.wc.IFileEditorElement.server} */
xyz.swapee.wc.IFileEditorElement.prototype.server = function() {}
/** @type {xyz.swapee.wc.IFileEditorElement.inducer} */
xyz.swapee.wc.IFileEditorElement.prototype.inducer = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElement&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorElement.Initialese>)} xyz.swapee.wc.FileEditorElement.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElement} xyz.swapee.wc.IFileEditorElement.typeof */
/**
 * A concrete class of _IFileEditorElement_ instances.
 * @constructor xyz.swapee.wc.FileEditorElement
 * @implements {xyz.swapee.wc.IFileEditorElement} A component description.
 *
 * The _IFileEditor_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorElement.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorElement = class extends /** @type {xyz.swapee.wc.FileEditorElement.constructor&xyz.swapee.wc.IFileEditorElement.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorElement* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorElement.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorElement* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorElement.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorElement.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorElement}
 */
xyz.swapee.wc.FileEditorElement.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorElement.
 * @interface xyz.swapee.wc.IFileEditorElementFields
 */
xyz.swapee.wc.IFileEditorElementFields = class { }
/**
 * The element-specific inputs to the _IFileEditor_ component.
 */
xyz.swapee.wc.IFileEditorElementFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileEditorElement.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorElement} */
xyz.swapee.wc.RecordIFileEditorElement

/** @typedef {xyz.swapee.wc.IFileEditorElement} xyz.swapee.wc.BoundIFileEditorElement */

/** @typedef {xyz.swapee.wc.FileEditorElement} xyz.swapee.wc.BoundFileEditorElement */

/** @typedef {xyz.swapee.wc.IFileEditorPort.Inputs&xyz.swapee.wc.IFileEditorDisplay.Queries&xyz.swapee.wc.IFileEditorController.Inputs&guest.maurice.IGuestPort.Inputs&xyz.swapee.wc.IFileEditorElementPort.Inputs} xyz.swapee.wc.IFileEditorElement.Inputs The element-specific inputs to the _IFileEditor_ component. */

/**
 * Contains getters to cast the _IFileEditorElement_ interface.
 * @interface xyz.swapee.wc.IFileEditorElementCaster
 */
xyz.swapee.wc.IFileEditorElementCaster = class { }
/**
 * Cast the _IFileEditorElement_ instance into the _BoundIFileEditorElement_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorElement}
 */
xyz.swapee.wc.IFileEditorElementCaster.prototype.asIFileEditorElement
/**
 * Access the _FileEditorElement_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorElement}
 */
xyz.swapee.wc.IFileEditorElementCaster.prototype.superFileEditorElement

/**
 * @typedef {(this: THIS, model: !xyz.swapee.wc.FileEditorMemory, props: !xyz.swapee.wc.IFileEditorElement.Inputs) => Object<string, *>} xyz.swapee.wc.IFileEditorElement.__solder
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorElement.__solder<!xyz.swapee.wc.IFileEditorElement>} xyz.swapee.wc.IFileEditorElement._solder */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElement.solder} */
/**
 * Makes element be available as an _HTMLComponent_ on web pages. Each property
 * in the returned record will be rendered as data-{key} attribute, and the
 * HTML component can use a deducer to access it.
 * @param {!xyz.swapee.wc.FileEditorMemory} model The model.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileEditorElement.Inputs} props The element props.
 * - `[content=null]` _&#42;?_ The read content of the file. ⤴ *IFileEditorOuterCore.WeakModel.Content* ⤴ *IFileEditorOuterCore.WeakModel.Content* Default `null`.
 * - `[path=null]` _&#42;?_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.WeakModel.Path* ⤴ *IFileEditorOuterCore.WeakModel.Path* Default `null`.
 * - `[filename=null]` _&#42;?_ The name of the file. ⤴ *IFileEditorOuterCore.WeakModel.Filename* ⤴ *IFileEditorOuterCore.WeakModel.Filename* Default `null`.
 * - `[hasChanges=null]` _&#42;?_ Whether the file has changes. ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* Default `null`.
 * - `[readFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* Default `null`.
 * - `[saveFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileEditorElementPort.Inputs.NoSolder* Default `false`.
 * - `[editorOpts]` _!Object?_ The options to pass to the _Editor_ vdu. ⤴ *IFileEditorElementPort.Inputs.EditorOpts* Default `{}`.
 * @return {Object<string, *>} Can return `null` to disable auto-enabling of the screen on the page.
 */
xyz.swapee.wc.IFileEditorElement.solder = function(model, props) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.FileEditorMemory, instance?: !xyz.swapee.wc.IFileEditorScreen&xyz.swapee.wc.IFileEditorController) => !engineering.type.VNode} xyz.swapee.wc.IFileEditorElement.__render
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorElement.__render<!xyz.swapee.wc.IFileEditorElement>} xyz.swapee.wc.IFileEditorElement._render */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElement.render} */
/**
 * A JSX function that returns mark-up to be generated statically for
 * serving in HTML pages.
 * @param {!xyz.swapee.wc.FileEditorMemory} [model] The model for the view.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileEditorScreen&xyz.swapee.wc.IFileEditorController} [instance] The display with the interrupt line gestures as well as the view methods.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IFileEditorElement.render = function(model, instance) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.FileEditorMemory, inputs: !xyz.swapee.wc.IFileEditorElement.Inputs) => !engineering.type.VNode} xyz.swapee.wc.IFileEditorElement.__server
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorElement.__server<!xyz.swapee.wc.IFileEditorElement>} xyz.swapee.wc.IFileEditorElement._server */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElement.server} */
/**
 * A JSX function that returns mark-up only for the server code and whose
 * body won't be analysed to generate browser code for placement in HTML
 * components.
 * @param {!xyz.swapee.wc.FileEditorMemory} memory The memory registers.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileEditorElement.Inputs} inputs The inputs to the port.
 * - `[content=null]` _&#42;?_ The read content of the file. ⤴ *IFileEditorOuterCore.WeakModel.Content* ⤴ *IFileEditorOuterCore.WeakModel.Content* Default `null`.
 * - `[path=null]` _&#42;?_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.WeakModel.Path* ⤴ *IFileEditorOuterCore.WeakModel.Path* Default `null`.
 * - `[filename=null]` _&#42;?_ The name of the file. ⤴ *IFileEditorOuterCore.WeakModel.Filename* ⤴ *IFileEditorOuterCore.WeakModel.Filename* Default `null`.
 * - `[hasChanges=null]` _&#42;?_ Whether the file has changes. ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* Default `null`.
 * - `[readFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* Default `null`.
 * - `[saveFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileEditorElementPort.Inputs.NoSolder* Default `false`.
 * - `[editorOpts]` _!Object?_ The options to pass to the _Editor_ vdu. ⤴ *IFileEditorElementPort.Inputs.EditorOpts* Default `{}`.
 * @return {!engineering.type.VNode} A JSX tree.
 */
xyz.swapee.wc.IFileEditorElement.server = function(memory, inputs) {}

/**
 * @typedef {(this: THIS, model?: !xyz.swapee.wc.FileEditorMemory, port?: !xyz.swapee.wc.IFileEditorElement.Inputs) => ?} xyz.swapee.wc.IFileEditorElement.__inducer
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorElement.__inducer<!xyz.swapee.wc.IFileEditorElement>} xyz.swapee.wc.IFileEditorElement._inducer */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElement.inducer} */
/**
 * Parses attributes and inner tags of the element and transfers them either
 * into port or directly into memory.
 * @param {!xyz.swapee.wc.FileEditorMemory} [model] The model of the component into which to induce the state.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {!xyz.swapee.wc.IFileEditorElement.Inputs} [port] The inputs to the port and options of the view.
 * - `[content=null]` _&#42;?_ The read content of the file. ⤴ *IFileEditorOuterCore.WeakModel.Content* ⤴ *IFileEditorOuterCore.WeakModel.Content* Default `null`.
 * - `[path=null]` _&#42;?_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.WeakModel.Path* ⤴ *IFileEditorOuterCore.WeakModel.Path* Default `null`.
 * - `[filename=null]` _&#42;?_ The name of the file. ⤴ *IFileEditorOuterCore.WeakModel.Filename* ⤴ *IFileEditorOuterCore.WeakModel.Filename* Default `null`.
 * - `[hasChanges=null]` _&#42;?_ Whether the file has changes. ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* ⤴ *IFileEditorOuterCore.WeakModel.HasChanges* Default `null`.
 * - `[readFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* ⤴ *IFileEditorOuterCore.WeakModel.ReadFile* Default `null`.
 * - `[saveFile=null]` _&#42;?_ . ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* ⤴ *IFileEditorOuterCore.WeakModel.SaveFile* Default `null`.
 * - `[noSolder=false]` _boolean?_ Disable soldering (set by parent builders). ⤴ *IFileEditorElementPort.Inputs.NoSolder* Default `false`.
 * - `[editorOpts]` _!Object?_ The options to pass to the _Editor_ vdu. ⤴ *IFileEditorElementPort.Inputs.EditorOpts* Default `{}`.
 * @return {?}
 */
xyz.swapee.wc.IFileEditorElement.inducer = function(model, port) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorElement
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/140-IFileEditorElementPort.xml}  bda8863c8620edee22c25808446b604b */
/** @typedef {engineering.type.mvc.IParametric.Initialese} xyz.swapee.wc.IFileEditorElementPort.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorElementPort)} xyz.swapee.wc.AbstractFileEditorElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorElementPort} xyz.swapee.wc.FileEditorElementPort.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorElementPort` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorElementPort
 */
xyz.swapee.wc.AbstractFileEditorElementPort = class extends /** @type {xyz.swapee.wc.AbstractFileEditorElementPort.constructor&xyz.swapee.wc.FileEditorElementPort.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorElementPort.prototype.constructor = xyz.swapee.wc.AbstractFileEditorElementPort
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorElementPort.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorElementPort} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorElementPort|typeof xyz.swapee.wc.FileEditorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElementPort}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorElementPort.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorElementPort}
 */
xyz.swapee.wc.AbstractFileEditorElementPort.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElementPort}
 */
xyz.swapee.wc.AbstractFileEditorElementPort.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorElementPort|typeof xyz.swapee.wc.FileEditorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElementPort}
 */
xyz.swapee.wc.AbstractFileEditorElementPort.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorElementPort|typeof xyz.swapee.wc.FileEditorElementPort)|(!engineering.type.mvc.IParametric|typeof engineering.type.mvc.Parametric)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorElementPort}
 */
xyz.swapee.wc.AbstractFileEditorElementPort.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorElementPort.Initialese[]) => xyz.swapee.wc.IFileEditorElementPort} xyz.swapee.wc.FileEditorElementPortConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElementPortFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorElementPortCaster&engineering.type.mvc.IParametric<!xyz.swapee.wc.IFileEditorElementPort.Inputs>)} xyz.swapee.wc.IFileEditorElementPort.constructor */
/**
 * The port specific for the server-side element.
 * @interface xyz.swapee.wc.IFileEditorElementPort
 */
xyz.swapee.wc.IFileEditorElementPort = class extends /** @type {xyz.swapee.wc.IFileEditorElementPort.constructor&engineering.type.IEngineer.typeof&engineering.type.mvc.IParametric.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorElementPort.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElementPort&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorElementPort.Initialese>)} xyz.swapee.wc.FileEditorElementPort.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElementPort} xyz.swapee.wc.IFileEditorElementPort.typeof */
/**
 * A concrete class of _IFileEditorElementPort_ instances.
 * @constructor xyz.swapee.wc.FileEditorElementPort
 * @implements {xyz.swapee.wc.IFileEditorElementPort} The port specific for the server-side element.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorElementPort.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorElementPort = class extends /** @type {xyz.swapee.wc.FileEditorElementPort.constructor&xyz.swapee.wc.IFileEditorElementPort.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorElementPort* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorElementPort.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorElementPort* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorElementPort.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorElementPort.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorElementPort}
 */
xyz.swapee.wc.FileEditorElementPort.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorElementPort.
 * @interface xyz.swapee.wc.IFileEditorElementPortFields
 */
xyz.swapee.wc.IFileEditorElementPortFields = class { }
/**
 * The inputs to the _IFileEditorElement_'s controller via its element port.
 */
xyz.swapee.wc.IFileEditorElementPortFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileEditorElementPort.Inputs} */ (void 0)
/**
 * Props to VSCode's suggestions on JSX tags.
 */
xyz.swapee.wc.IFileEditorElementPortFields.prototype.props = /** @type {!xyz.swapee.wc.IFileEditorElementPort.Inputs} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorElementPort} */
xyz.swapee.wc.RecordIFileEditorElementPort

/** @typedef {xyz.swapee.wc.IFileEditorElementPort} xyz.swapee.wc.BoundIFileEditorElementPort */

/** @typedef {xyz.swapee.wc.FileEditorElementPort} xyz.swapee.wc.BoundFileEditorElementPort */

/**
 * Disable soldering (set by parent builders).
 * @typedef {boolean}
 */
xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder.noSolder

/**
 * The options to pass to the _Editor_ vdu.
 * @typedef {!Object}
 */
xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts.editorOpts

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder&xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts)} xyz.swapee.wc.IFileEditorElementPort.Inputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder} xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts} xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts.typeof */
/**
 * The inputs to the _IFileEditorElement_'s controller via its element port.
 * @record xyz.swapee.wc.IFileEditorElementPort.Inputs
 */
xyz.swapee.wc.IFileEditorElementPort.Inputs = class extends /** @type {xyz.swapee.wc.IFileEditorElementPort.Inputs.constructor&xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder.typeof&xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorElementPort.Inputs.prototype.constructor = xyz.swapee.wc.IFileEditorElementPort.Inputs

/** @typedef {function(new: xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder&xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts)} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts.typeof */
/**
 * The inputs to the _IFileEditorElement_'s controller via its element port.
 * @record xyz.swapee.wc.IFileEditorElementPort.WeakInputs
 */
xyz.swapee.wc.IFileEditorElementPort.WeakInputs = class extends /** @type {xyz.swapee.wc.IFileEditorElementPort.WeakInputs.constructor&xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder.typeof&xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts.typeof} */ (class {}) { }
xyz.swapee.wc.IFileEditorElementPort.WeakInputs.prototype.constructor = xyz.swapee.wc.IFileEditorElementPort.WeakInputs

/**
 * Contains getters to cast the _IFileEditorElementPort_ interface.
 * @interface xyz.swapee.wc.IFileEditorElementPortCaster
 */
xyz.swapee.wc.IFileEditorElementPortCaster = class { }
/**
 * Cast the _IFileEditorElementPort_ instance into the _BoundIFileEditorElementPort_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorElementPort}
 */
xyz.swapee.wc.IFileEditorElementPortCaster.prototype.asIFileEditorElementPort
/**
 * Access the _FileEditorElementPort_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorElementPort}
 */
xyz.swapee.wc.IFileEditorElementPortCaster.prototype.superFileEditorElementPort

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {boolean} [noSolder=false] Disable soldering (set by parent builders). Default `false`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.Inputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {boolean} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts The options to pass to the _Editor_ vdu (optional overlay).
 * @prop {!Object} [editorOpts] The options to pass to the _Editor_ vdu. Default `{}`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.Inputs.EditorOpts_Safe The options to pass to the _Editor_ vdu (required overlay).
 * @prop {!Object} editorOpts The options to pass to the _Editor_ vdu.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder Disable soldering (set by parent builders) (optional overlay).
 * @prop {*} [noSolder=null] Disable soldering (set by parent builders). Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.NoSolder_Safe Disable soldering (set by parent builders) (required overlay).
 * @prop {*} noSolder Disable soldering (set by parent builders).
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts The options to pass to the _Editor_ vdu (optional overlay).
 * @prop {*} [editorOpts=null] The options to pass to the _Editor_ vdu. Default `null`.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorElementPort.WeakInputs.EditorOpts_Safe The options to pass to the _Editor_ vdu (required overlay).
 * @prop {*} editorOpts The options to pass to the _Editor_ vdu.
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/160-IFileEditorRadio.xml}  b7166b62d78cc3e79d79ecd8d464cd69 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorRadio.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorRadio)} xyz.swapee.wc.AbstractFileEditorRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorRadio} xyz.swapee.wc.FileEditorRadio.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorRadio` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorRadio
 */
xyz.swapee.wc.AbstractFileEditorRadio = class extends /** @type {xyz.swapee.wc.AbstractFileEditorRadio.constructor&xyz.swapee.wc.FileEditorRadio.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorRadio.prototype.constructor = xyz.swapee.wc.AbstractFileEditorRadio
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorRadio.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorRadio} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileEditorRadio|typeof xyz.swapee.wc.FileEditorRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorRadio}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorRadio.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorRadio}
 */
xyz.swapee.wc.AbstractFileEditorRadio.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorRadio}
 */
xyz.swapee.wc.AbstractFileEditorRadio.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileEditorRadio|typeof xyz.swapee.wc.FileEditorRadio} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorRadio}
 */
xyz.swapee.wc.AbstractFileEditorRadio.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileEditorRadio|typeof xyz.swapee.wc.FileEditorRadio} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorRadio}
 */
xyz.swapee.wc.AbstractFileEditorRadio.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorRadioCaster)} xyz.swapee.wc.IFileEditorRadio.constructor */
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @interface xyz.swapee.wc.IFileEditorRadio
 */
xyz.swapee.wc.IFileEditorRadio = class extends /** @type {xyz.swapee.wc.IFileEditorRadio.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile} */
xyz.swapee.wc.IFileEditorRadio.prototype.adaptLoadReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile} */
xyz.swapee.wc.IFileEditorRadio.prototype.adaptLoadSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorRadio&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorRadio.Initialese>)} xyz.swapee.wc.FileEditorRadio.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorRadio} xyz.swapee.wc.IFileEditorRadio.typeof */
/**
 * A concrete class of _IFileEditorRadio_ instances.
 * @constructor xyz.swapee.wc.FileEditorRadio
 * @implements {xyz.swapee.wc.IFileEditorRadio} Transmits data _to-_ and _fro-_ off-chip system.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorRadio.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorRadio = class extends /** @type {xyz.swapee.wc.FileEditorRadio.constructor&xyz.swapee.wc.IFileEditorRadio.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorRadio.prototype.constructor = xyz.swapee.wc.FileEditorRadio
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorRadio}
 */
xyz.swapee.wc.FileEditorRadio.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorRadio} */
xyz.swapee.wc.RecordIFileEditorRadio

/** @typedef {xyz.swapee.wc.IFileEditorRadio} xyz.swapee.wc.BoundIFileEditorRadio */

/** @typedef {xyz.swapee.wc.FileEditorRadio} xyz.swapee.wc.BoundFileEditorRadio */

/**
 * Contains getters to cast the _IFileEditorRadio_ interface.
 * @interface xyz.swapee.wc.IFileEditorRadioCaster
 */
xyz.swapee.wc.IFileEditorRadioCaster = class { }
/**
 * Cast the _IFileEditorRadio_ instance into the _BoundIFileEditorRadio_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorRadio}
 */
xyz.swapee.wc.IFileEditorRadioCaster.prototype.asIFileEditorRadio
/**
 * Cast the _IFileEditorRadio_ instance into the _BoundIFileEditorComputer_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorComputer}
 */
xyz.swapee.wc.IFileEditorRadioCaster.prototype.asIFileEditorComputer
/**
 * Access the _FileEditorRadio_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorRadio}
 */
xyz.swapee.wc.IFileEditorRadioCaster.prototype.superFileEditorRadio

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Form, changes: IFileEditorComputer.adaptLoadReadFile.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Return|void)>)} xyz.swapee.wc.IFileEditorRadio.__adaptLoadReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorRadio.__adaptLoadReadFile<!xyz.swapee.wc.IFileEditorRadio>} xyz.swapee.wc.IFileEditorRadio._adaptLoadReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile} */
/**
 * Reads the file on the filesystem and opens it in the editor.
 * @param {!xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Form} form The form with inputs.
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * @param {IFileEditorComputer.adaptLoadReadFile.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path_Safe} xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content} xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Return The form with outputs. */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Form, changes: IFileEditorComputer.adaptLoadSaveFile.Form) => (void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Return|void)>)} xyz.swapee.wc.IFileEditorRadio.__adaptLoadSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorRadio.__adaptLoadSaveFile<!xyz.swapee.wc.IFileEditorRadio>} xyz.swapee.wc.IFileEditorRadio._adaptLoadSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile} */
/**
 * Saves the file back to the filesystem.
 * @param {!xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Form} form The form with inputs.
 * - `content` _?string_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content_Safe*
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * - `saveFile` _boolean_ . ⤴ *IFileEditorOuterCore.Model.SaveFile_Safe*
 * @param {IFileEditorComputer.adaptLoadSaveFile.Form} changes The previous values of the form.
 * @return {void|!Promise<void>|!Promise<(xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Return|void)>} The form with outputs.
 */
xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile = function(form, changes) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content_Safe&xyz.swapee.wc.IFileEditorCore.Model.Path_Safe&xyz.swapee.wc.IFileEditorCore.Model.SaveFile_Safe} xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Form The form with inputs. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path} xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Return The form with outputs. */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorRadio
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/170-IFileEditorDesigner.xml}  f88f2ffcd4eb7e1191eca0dda0f53e5e */
/**
 * The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 * @interface xyz.swapee.wc.IFileEditorDesigner
 */
xyz.swapee.wc.IFileEditorDesigner = class {
  /**
   * Borrows renamed classes from specified components.
   * @param {xyz.swapee.wc.FileEditorClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  borrowClasses(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IFileEditor />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.FileEditorClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  classes(classes) { }
  /**
   * ```js
   * (<xyz.swapee.wc.IFileEditor />).implements({
   * ‎ classes({ResetHandle:ResetHandle,RotateHandle:RotateHandle}){
   * ‎  return(<>
   * ‎   <com.webcircuits.ui.CollapsarClasses
   * ‎    CollapsarCollapsed={ResetHandle}
   * ‎    CollapsarUncollapsed={RotateHandle}
   * ‎   />
   * ‎  </>)
   * ‎ },
   * })
   * ```
   * Maps the classes of the component to the classes that the buildee or inner
   * component requires.
   * @param {xyz.swapee.wc.FileEditorClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias classes An alias for **classes**.
   */
  lendClasses(classes) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IFileEditorDesigner.communicator.Mesh} mesh The controller of the component and controllers of the peers.
   * - `FileEditor` _typeof IFileEditorController_
   * @return {?}
   */
  communicator(mesh) { }
  /**
   * Receives signals from controller's emitters and directs them to the
   * right place.
   * @param {!xyz.swapee.wc.IFileEditorDesigner.relay.Mesh} mesh The controller of the component and controllers of the peers.
   * - `FileEditor` _typeof IFileEditorController_
   * - `This` _typeof IFileEditorController_
   * @param {!xyz.swapee.wc.IFileEditorDesigner.relay.MemPool} memPool The collective memory of all components in the land, and the component itself.
   * - `FileEditor` _!FileEditorMemory_
   * - `This` _!FileEditorMemory_
   * @return {?}
   */
  relay(mesh, memPool) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.FileEditorClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   */
  lendClasses(classes) { }
  /**
   * An alias for `classes`.
   * @param {xyz.swapee.wc.FileEditorClasses} classes The classes mapping.
   * @return {!engineering.type.VNode}
   * @alias lendClasses An alias for **lendClasses**.
   */
  lendClasses(classes) { }
}
xyz.swapee.wc.IFileEditorDesigner.prototype.constructor = xyz.swapee.wc.IFileEditorDesigner

/**
 * A concrete class of _IFileEditorDesigner_ instances.
 * @constructor xyz.swapee.wc.FileEditorDesigner
 * @implements {xyz.swapee.wc.IFileEditorDesigner} The designer interface is used to specify information that is not subject
 * to rendering or executing at runtime, but allows to gather structural metadata
 * about components and its relations with the land using dynamic analysis.
 */
xyz.swapee.wc.FileEditorDesigner = class extends xyz.swapee.wc.IFileEditorDesigner { }
xyz.swapee.wc.FileEditorDesigner.prototype.constructor = xyz.swapee.wc.FileEditorDesigner

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorDesigner.communicator.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IFileEditorController} FileEditor
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorDesigner.relay.Mesh The controller of the component and controllers of the peers.
 * @prop {typeof xyz.swapee.wc.IFileEditorController} FileEditor
 * @prop {typeof xyz.swapee.wc.IFileEditorController} This
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorDesigner.relay.MemPool The collective memory of all components in the land, and the component itself.
 * @prop {!xyz.swapee.wc.FileEditorMemory} FileEditor
 * @prop {!xyz.swapee.wc.FileEditorMemory} This
 */

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/190-IFileEditorService.xml}  fc5e1b9d143767666d1268dce1d25510 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorService.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorService)} xyz.swapee.wc.AbstractFileEditorService.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorService} xyz.swapee.wc.FileEditorService.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorService` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorService
 */
xyz.swapee.wc.AbstractFileEditorService = class extends /** @type {xyz.swapee.wc.AbstractFileEditorService.constructor&xyz.swapee.wc.FileEditorService.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorService.prototype.constructor = xyz.swapee.wc.AbstractFileEditorService
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorService.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorService} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileEditorService|typeof xyz.swapee.wc.FileEditorService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorService}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorService.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorService}
 */
xyz.swapee.wc.AbstractFileEditorService.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorService}
 */
xyz.swapee.wc.AbstractFileEditorService.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileEditorService|typeof xyz.swapee.wc.FileEditorService} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorService}
 */
xyz.swapee.wc.AbstractFileEditorService.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileEditorService|typeof xyz.swapee.wc.FileEditorService} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorService}
 */
xyz.swapee.wc.AbstractFileEditorService.__trait = function(...Implementations) {}

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorServiceCaster)} xyz.swapee.wc.IFileEditorService.constructor */
/**
 * A service for the IFileEditor.
 * @interface xyz.swapee.wc.IFileEditorService
 */
xyz.swapee.wc.IFileEditorService = class extends /** @type {xyz.swapee.wc.IFileEditorService.constructor&engineering.type.IEngineer.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.IFileEditorService.filterReadFile} */
xyz.swapee.wc.IFileEditorService.prototype.filterReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorService.filterSaveFile} */
xyz.swapee.wc.IFileEditorService.prototype.filterSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorService&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorService.Initialese>)} xyz.swapee.wc.FileEditorService.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorService} xyz.swapee.wc.IFileEditorService.typeof */
/**
 * A concrete class of _IFileEditorService_ instances.
 * @constructor xyz.swapee.wc.FileEditorService
 * @implements {xyz.swapee.wc.IFileEditorService} A service for the IFileEditor.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorService.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorService = class extends /** @type {xyz.swapee.wc.FileEditorService.constructor&xyz.swapee.wc.IFileEditorService.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.FileEditorService.prototype.constructor = xyz.swapee.wc.FileEditorService
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorService}
 */
xyz.swapee.wc.FileEditorService.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorService} */
xyz.swapee.wc.RecordIFileEditorService

/** @typedef {xyz.swapee.wc.IFileEditorService} xyz.swapee.wc.BoundIFileEditorService */

/** @typedef {xyz.swapee.wc.FileEditorService} xyz.swapee.wc.BoundFileEditorService */

/**
 * Contains getters to cast the _IFileEditorService_ interface.
 * @interface xyz.swapee.wc.IFileEditorServiceCaster
 */
xyz.swapee.wc.IFileEditorServiceCaster = class { }
/**
 * Cast the _IFileEditorService_ instance into the _BoundIFileEditorService_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorService}
 */
xyz.swapee.wc.IFileEditorServiceCaster.prototype.asIFileEditorService
/**
 * Access the _FileEditorService_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorService}
 */
xyz.swapee.wc.IFileEditorServiceCaster.prototype.superFileEditorService

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorService.filterReadFile.Form) => !Promise<xyz.swapee.wc.IFileEditorService.filterReadFile.Return>} xyz.swapee.wc.IFileEditorService.__filterReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorService.__filterReadFile<!xyz.swapee.wc.IFileEditorService>} xyz.swapee.wc.IFileEditorService._filterReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorService.filterReadFile} */
/**
 * Reads the file on the filesystem and opens it in the editor.
 * @param {!xyz.swapee.wc.IFileEditorService.filterReadFile.Form} form The form.
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * @return {!Promise<xyz.swapee.wc.IFileEditorService.filterReadFile.Return>}
 */
xyz.swapee.wc.IFileEditorService.filterReadFile = function(form) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path_Safe} xyz.swapee.wc.IFileEditorService.filterReadFile.Form The form. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content} xyz.swapee.wc.IFileEditorService.filterReadFile.Return */

/**
 * @typedef {(this: THIS, form: !xyz.swapee.wc.IFileEditorService.filterSaveFile.Form) => !Promise<xyz.swapee.wc.IFileEditorService.filterSaveFile.Return>} xyz.swapee.wc.IFileEditorService.__filterSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorService.__filterSaveFile<!xyz.swapee.wc.IFileEditorService>} xyz.swapee.wc.IFileEditorService._filterSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorService.filterSaveFile} */
/**
 * Saves the file back to the filesystem.
 * @param {!xyz.swapee.wc.IFileEditorService.filterSaveFile.Form} form The form.
 * - `saveFile` _boolean_ . ⤴ *IFileEditorOuterCore.Model.SaveFile_Safe*
 * - `content` _?string_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content_Safe*
 * - `path` _string_ The path to the file which acts as an ID. ⤴ *IFileEditorOuterCore.Model.Path_Safe*
 * @return {!Promise<xyz.swapee.wc.IFileEditorService.filterSaveFile.Return>}
 */
xyz.swapee.wc.IFileEditorService.filterSaveFile = function(form) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.SaveFile_Safe&xyz.swapee.wc.IFileEditorCore.Model.Content_Safe&xyz.swapee.wc.IFileEditorCore.Model.Path_Safe} xyz.swapee.wc.IFileEditorService.filterSaveFile.Form The form. */

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Path} xyz.swapee.wc.IFileEditorService.filterSaveFile.Return */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorService
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/40-IFileEditorDisplay.xml}  d47457b6ec7480643b757ae3362b6152 */
/**
 * @typedef {Object} $xyz.swapee.wc.IFileEditorDisplay.Initialese
 * @prop {HTMLDivElement} [Editor] The editor container for Monaco.
 */
/** @typedef {$xyz.swapee.wc.IFileEditorDisplay.Initialese&com.webcircuits.IDisplay.Initialese<!HTMLDivElement, !xyz.swapee.wc.IFileEditorDisplay.Settings>} xyz.swapee.wc.IFileEditorDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileEditorDisplay)} xyz.swapee.wc.AbstractFileEditorDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorDisplay} xyz.swapee.wc.FileEditorDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorDisplay` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorDisplay
 */
xyz.swapee.wc.AbstractFileEditorDisplay = class extends /** @type {xyz.swapee.wc.AbstractFileEditorDisplay.constructor&xyz.swapee.wc.FileEditorDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorDisplay.prototype.constructor = xyz.swapee.wc.AbstractFileEditorDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorDisplay.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorDisplay}
 */
xyz.swapee.wc.AbstractFileEditorDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorDisplay}
 */
xyz.swapee.wc.AbstractFileEditorDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorDisplay}
 */
xyz.swapee.wc.AbstractFileEditorDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)|(!com.webcircuits.IDisplay|typeof com.webcircuits.Display)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorDisplay}
 */
xyz.swapee.wc.AbstractFileEditorDisplay.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorDisplay.Initialese[]) => xyz.swapee.wc.IFileEditorDisplay} xyz.swapee.wc.FileEditorDisplayConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorDisplayCaster&com.webcircuits.IDisplay<!xyz.swapee.wc.FileEditorMemory, !HTMLDivElement, !xyz.swapee.wc.IFileEditorDisplay.Settings, xyz.swapee.wc.IFileEditorDisplay.Queries, null>)} xyz.swapee.wc.IFileEditorDisplay.constructor */
/** @typedef {typeof com.webcircuits.IDisplay} com.webcircuits.IDisplay.typeof */
/**
 * Display for presenting information from the _IFileEditor_.
 * @interface xyz.swapee.wc.IFileEditorDisplay
 */
xyz.swapee.wc.IFileEditorDisplay = class extends /** @type {xyz.swapee.wc.IFileEditorDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorDisplay.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorDisplay.paint} */
xyz.swapee.wc.IFileEditorDisplay.prototype.paint = function() {}
/** @type {xyz.swapee.wc.IFileEditorDisplay.paintFile} */
xyz.swapee.wc.IFileEditorDisplay.prototype.paintFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorDisplay.Initialese>)} xyz.swapee.wc.FileEditorDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorDisplay} xyz.swapee.wc.IFileEditorDisplay.typeof */
/**
 * A concrete class of _IFileEditorDisplay_ instances.
 * @constructor xyz.swapee.wc.FileEditorDisplay
 * @implements {xyz.swapee.wc.IFileEditorDisplay} Display for presenting information from the _IFileEditor_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorDisplay.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorDisplay = class extends /** @type {xyz.swapee.wc.FileEditorDisplay.constructor&xyz.swapee.wc.IFileEditorDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorDisplay* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorDisplay.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorDisplay* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorDisplay.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorDisplay.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorDisplay}
 */
xyz.swapee.wc.FileEditorDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorDisplay.
 * @interface xyz.swapee.wc.IFileEditorDisplayFields
 */
xyz.swapee.wc.IFileEditorDisplayFields = class { }
/**
 * The settings for the screen which will not be passed to the backend.
 */
xyz.swapee.wc.IFileEditorDisplayFields.prototype.settings = /** @type {!xyz.swapee.wc.IFileEditorDisplay.Settings} */ (void 0)
/**
 * The queries to discover VDUs on the page by.
 */
xyz.swapee.wc.IFileEditorDisplayFields.prototype.queries = /** @type {!xyz.swapee.wc.IFileEditorDisplay.Queries} */ (void 0)
/**
 * The editor container for Monaco. Default `null`.
 */
xyz.swapee.wc.IFileEditorDisplayFields.prototype.Editor = /** @type {HTMLDivElement} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorDisplay} */
xyz.swapee.wc.RecordIFileEditorDisplay

/** @typedef {xyz.swapee.wc.IFileEditorDisplay} xyz.swapee.wc.BoundIFileEditorDisplay */

/** @typedef {xyz.swapee.wc.FileEditorDisplay} xyz.swapee.wc.BoundFileEditorDisplay */

/** @typedef {xyz.swapee.wc.IFileEditorDisplay.Queries} xyz.swapee.wc.IFileEditorDisplay.Settings The settings for the screen which will not be passed to the backend. */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorDisplay.Queries The queries to discover VDUs on the page by. */

/**
 * Contains getters to cast the _IFileEditorDisplay_ interface.
 * @interface xyz.swapee.wc.IFileEditorDisplayCaster
 */
xyz.swapee.wc.IFileEditorDisplayCaster = class { }
/**
 * Cast the _IFileEditorDisplay_ instance into the _BoundIFileEditorDisplay_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorDisplay}
 */
xyz.swapee.wc.IFileEditorDisplayCaster.prototype.asIFileEditorDisplay
/**
 * Cast the _IFileEditorDisplay_ instance into the _BoundIFileEditorScreen_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorScreen}
 */
xyz.swapee.wc.IFileEditorDisplayCaster.prototype.asIFileEditorScreen
/**
 * Access the _FileEditorDisplay_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorDisplay}
 */
xyz.swapee.wc.IFileEditorDisplayCaster.prototype.superFileEditorDisplay

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.FileEditorMemory, land: null) => void} xyz.swapee.wc.IFileEditorDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorDisplay.__paint<!xyz.swapee.wc.IFileEditorDisplay>} xyz.swapee.wc.IFileEditorDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.IFileEditorDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.FileEditorMemory} memory The display data.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `lang` _string_ The language. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {null} land The land data.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorDisplay.paint = function(memory, land) {}

/**
 * @typedef {(this: THIS, memory: !xyz.swapee.wc.IFileEditorDisplay.paintFile.Memory) => ?} xyz.swapee.wc.IFileEditorDisplay.__paintFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorDisplay.__paintFile<!xyz.swapee.wc.IFileEditorDisplay>} xyz.swapee.wc.IFileEditorDisplay._paintFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorDisplay.paintFile} */
/**
 * @param {!xyz.swapee.wc.IFileEditorDisplay.paintFile.Memory} memory The memory.
 * - `content` _?string_ The read content of the file. ⤴ *IFileEditorOuterCore.Model.Content_Safe*
 * - `lang` _string_ The language. ⤴ *IFileEditorOuterCore.Model.Lang_Safe*
 * @return {?}
 */
xyz.swapee.wc.IFileEditorDisplay.paintFile = function(memory) {}

/** @typedef {xyz.swapee.wc.IFileEditorCore.Model.Content_Safe&xyz.swapee.wc.IFileEditorCore.Model.Lang_Safe} xyz.swapee.wc.IFileEditorDisplay.paintFile.Memory The memory. */

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/40-IFileEditorDisplayBack.xml}  4b2eeaf5f49badf33339b7804ee4db13 */
/**
 * @typedef {Object} $xyz.swapee.wc.back.IFileEditorDisplay.Initialese
 * @prop {!com.webcircuits.IHtmlTwin} [Editor] The editor container for Monaco.
 */
/** @typedef {$xyz.swapee.wc.back.IFileEditorDisplay.Initialese&com.webcircuits.IGraphicsDriverBack.Initialese<!xyz.swapee.wc.FileEditorClasses>} xyz.swapee.wc.back.IFileEditorDisplay.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.FileEditorDisplay)} xyz.swapee.wc.back.AbstractFileEditorDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileEditorDisplay} xyz.swapee.wc.back.FileEditorDisplay.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileEditorDisplay` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileEditorDisplay
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay = class extends /** @type {xyz.swapee.wc.back.AbstractFileEditorDisplay.constructor&xyz.swapee.wc.back.FileEditorDisplay.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileEditorDisplay.prototype.constructor = xyz.swapee.wc.back.AbstractFileEditorDisplay
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileEditorDisplay} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorDisplay}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileEditorDisplay}
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorDisplay}
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorDisplay}
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)|(!com.webcircuits.IGraphicsDriverBack|typeof com.webcircuits.GraphicsDriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorDisplay}
 */
xyz.swapee.wc.back.AbstractFileEditorDisplay.__trait = function(...Implementations) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorDisplayFields&engineering.type.IEngineer&xyz.swapee.wc.back.IFileEditorDisplayCaster&com.webcircuits.IGraphicsDriverBack<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.FileEditorClasses, null>)} xyz.swapee.wc.back.IFileEditorDisplay.constructor */
/** @typedef {typeof com.webcircuits.IGraphicsDriverBack} com.webcircuits.IGraphicsDriverBack.typeof */
/**
 * The backend interface for the display.
 * @interface xyz.swapee.wc.back.IFileEditorDisplay
 */
xyz.swapee.wc.back.IFileEditorDisplay = class extends /** @type {xyz.swapee.wc.back.IFileEditorDisplay.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IGraphicsDriverBack.typeof} */ (class {}) { }
/** @type {xyz.swapee.wc.back.IFileEditorDisplay.paint} */
xyz.swapee.wc.back.IFileEditorDisplay.prototype.paint = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorDisplay&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorDisplay.Initialese>)} xyz.swapee.wc.back.FileEditorDisplay.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorDisplay} xyz.swapee.wc.back.IFileEditorDisplay.typeof */
/**
 * A concrete class of _IFileEditorDisplay_ instances.
 * @constructor xyz.swapee.wc.back.FileEditorDisplay
 * @implements {xyz.swapee.wc.back.IFileEditorDisplay} The backend interface for the display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorDisplay.Initialese>} ‎
 */
xyz.swapee.wc.back.FileEditorDisplay = class extends /** @type {xyz.swapee.wc.back.FileEditorDisplay.constructor&xyz.swapee.wc.back.IFileEditorDisplay.typeof&engineering.type.IInitialiser.typeof} */ (class {}) { }
xyz.swapee.wc.back.FileEditorDisplay.prototype.constructor = xyz.swapee.wc.back.FileEditorDisplay
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileEditorDisplay}
 */
xyz.swapee.wc.back.FileEditorDisplay.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorDisplay.
 * @interface xyz.swapee.wc.back.IFileEditorDisplayFields
 */
xyz.swapee.wc.back.IFileEditorDisplayFields = class { }
/**
 * The editor container for Monaco.
 */
xyz.swapee.wc.back.IFileEditorDisplayFields.prototype.Editor = /** @type {!com.webcircuits.IHtmlTwin} */ (void 0)

/** @typedef {xyz.swapee.wc.back.IFileEditorDisplay} */
xyz.swapee.wc.back.RecordIFileEditorDisplay

/** @typedef {xyz.swapee.wc.back.IFileEditorDisplay} xyz.swapee.wc.back.BoundIFileEditorDisplay */

/** @typedef {xyz.swapee.wc.back.FileEditorDisplay} xyz.swapee.wc.back.BoundFileEditorDisplay */

/**
 * Contains getters to cast the _IFileEditorDisplay_ interface.
 * @interface xyz.swapee.wc.back.IFileEditorDisplayCaster
 */
xyz.swapee.wc.back.IFileEditorDisplayCaster = class { }
/**
 * Cast the _IFileEditorDisplay_ instance into the _BoundIFileEditorDisplay_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileEditorDisplay}
 */
xyz.swapee.wc.back.IFileEditorDisplayCaster.prototype.asIFileEditorDisplay
/**
 * Access the _FileEditorDisplay_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileEditorDisplay}
 */
xyz.swapee.wc.back.IFileEditorDisplayCaster.prototype.superFileEditorDisplay

/**
 * @typedef {(this: THIS, memory?: !xyz.swapee.wc.FileEditorMemory, land?: null) => void} xyz.swapee.wc.back.IFileEditorDisplay.__paint
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IFileEditorDisplay.__paint<!xyz.swapee.wc.back.IFileEditorDisplay>} xyz.swapee.wc.back.IFileEditorDisplay._paint */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorDisplay.paint} */
/**
 * Performs a repaint of VDUs.
 * @param {!xyz.swapee.wc.FileEditorMemory} [memory] The display data.
 * - `content` _?string_ The read content of the file. Default `null`.
 * - `path` _string_ The path to the file which acts as an ID. Default empty string.
 * - `filename` _string_ The name of the file. Default empty string.
 * - `hasChanges` _boolean_ Whether the file has changes. Default `false`.
 * - `readFile` _boolean_ . Default `false`.
 * - `saveFile` _boolean_ . Default `false`.
 * - `loadingReadFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreReadFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadReadFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * - `loadingSaveFile` _boolean_ Whether the items are being loaded from the remote. Default `false`.
 * - `hasMoreSaveFile` _?boolean_ Whether there are more items to load. Default `null`.
 * - `loadSaveFileError` _?Error_ An error during loading of items from the remote. Default `null`.
 * @param {null} [land] The land data.
 * @return {void}
 */
xyz.swapee.wc.back.IFileEditorDisplay.paint = function(memory, land) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IFileEditorDisplay
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/41-FileEditorClasses.xml}  ed9970a8b182a99d36227e8c94168ade */
/**
 * The classes of the _IFileEditorDisplay_.
 * @record xyz.swapee.wc.FileEditorClasses
 */
xyz.swapee.wc.FileEditorClasses = class { }
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.FileEditorClasses.prototype.props = /** @type {xyz.swapee.wc.FileEditorClasses} */ (void 0)

// nss:xyz.swapee.wc
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/50-IFileEditorController.xml}  1f3907236cf5eb2b9d6c906681fcda96 */
/** @typedef {engineering.type.mvc.IIntegratedController.Initialese<!xyz.swapee.wc.IFileEditorController.Inputs>&com.webcircuits.IPort.Initialese<!xyz.swapee.wc.IFileEditorController.Inputs, !xyz.swapee.wc.IFileEditorOuterCore.Model>&com.webcircuits.IBuffer.Initialese<!xyz.swapee.wc.IFileEditorOuterCore.WeakModel>} xyz.swapee.wc.IFileEditorController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileEditorController)} xyz.swapee.wc.AbstractFileEditorController.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorController} xyz.swapee.wc.FileEditorController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorController` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorController
 */
xyz.swapee.wc.AbstractFileEditorController = class extends /** @type {xyz.swapee.wc.AbstractFileEditorController.constructor&xyz.swapee.wc.FileEditorController.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorController.prototype.constructor = xyz.swapee.wc.AbstractFileEditorController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorController.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorController}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorController}
 */
xyz.swapee.wc.AbstractFileEditorController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorController}
 */
xyz.swapee.wc.AbstractFileEditorController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorController}
 */
xyz.swapee.wc.AbstractFileEditorController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IPort|typeof com.webcircuits.Port)|(!com.webcircuits.IBuffer|typeof com.webcircuits.Buffer)|(!engineering.type.mvc.ITransformer|typeof engineering.type.mvc.Transformer)|(!engineering.type.mvc.IIntegratedController|typeof engineering.type.mvc.IntegratedController)|(!engineering.type.mvc.IRegulator|typeof engineering.type.mvc.Regulator)|(!engineering.type.mvc.ICalibrator|typeof engineering.type.mvc.Calibrator)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorController}
 */
xyz.swapee.wc.AbstractFileEditorController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorController.Initialese[]) => xyz.swapee.wc.IFileEditorController} xyz.swapee.wc.FileEditorControllerConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorControllerFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorControllerCaster&com.webcircuits.IPort<!xyz.swapee.wc.IFileEditorController.Inputs, !xyz.swapee.wc.IFileEditorOuterCore.Model>&com.webcircuits.IBuffer<!xyz.swapee.wc.IFileEditorOuterCore.WeakModel>&engineering.type.mvc.ITransformer<!xyz.swapee.wc.IFileEditorController.Inputs, !xyz.swapee.wc.IFileEditorController.WeakInputs>&engineering.type.mvc.IIntegratedController<!xyz.swapee.wc.IFileEditorController.Inputs, !xyz.swapee.wc.FileEditorMemory>&engineering.type.mvc.IRegulator<!xyz.swapee.wc.IFileEditorController.Inputs>&engineering.type.mvc.ICalibrator<!xyz.swapee.wc.IFileEditorController.Inputs>)} xyz.swapee.wc.IFileEditorController.constructor */
/** @typedef {typeof com.webcircuits.IPort} com.webcircuits.IPort.typeof */
/** @typedef {typeof com.webcircuits.IBuffer} com.webcircuits.IBuffer.typeof */
/** @typedef {typeof engineering.type.mvc.ITransformer} engineering.type.mvc.ITransformer.typeof */
/** @typedef {typeof engineering.type.mvc.IIntegratedController} engineering.type.mvc.IIntegratedController.typeof */
/** @typedef {typeof engineering.type.mvc.ICalibrator} engineering.type.mvc.ICalibrator.typeof */
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @interface xyz.swapee.wc.IFileEditorController
 */
xyz.swapee.wc.IFileEditorController = class extends /** @type {xyz.swapee.wc.IFileEditorController.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IPort.typeof&com.webcircuits.IBuffer.typeof&engineering.type.mvc.ITransformer.typeof&engineering.type.mvc.IIntegratedController.typeof&engineering.type.mvc.IRegulator.typeof&engineering.type.mvc.ICalibrator.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorController.resetPort} */
xyz.swapee.wc.IFileEditorController.prototype.resetPort = function() {}
/** @type {xyz.swapee.wc.IFileEditorController.openFile} */
xyz.swapee.wc.IFileEditorController.prototype.openFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorController.loadReadFile} */
xyz.swapee.wc.IFileEditorController.prototype.loadReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorController.loadSaveFile} */
xyz.swapee.wc.IFileEditorController.prototype.loadSaveFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorController.pulseReadFile} */
xyz.swapee.wc.IFileEditorController.prototype.pulseReadFile = function() {}
/** @type {xyz.swapee.wc.IFileEditorController.pulseSaveFile} */
xyz.swapee.wc.IFileEditorController.prototype.pulseSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorController&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorController.Initialese>)} xyz.swapee.wc.FileEditorController.constructor */
/**
 * A concrete class of _IFileEditorController_ instances.
 * @constructor xyz.swapee.wc.FileEditorController
 * @implements {xyz.swapee.wc.IFileEditorController} Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorController.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorController = class extends /** @type {xyz.swapee.wc.FileEditorController.constructor&xyz.swapee.wc.IFileEditorController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorController}
 */
xyz.swapee.wc.FileEditorController.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorController.
 * @interface xyz.swapee.wc.IFileEditorControllerFields
 */
xyz.swapee.wc.IFileEditorControllerFields = class { }
/**
 * The inputs to the _IFileEditor_'s controller.
 */
xyz.swapee.wc.IFileEditorControllerFields.prototype.inputs = /** @type {!xyz.swapee.wc.IFileEditorController.Inputs} */ (void 0)
/**
 * The props for _VSCode_.
 */
xyz.swapee.wc.IFileEditorControllerFields.prototype.props = /** @type {xyz.swapee.wc.IFileEditorController} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorController} */
xyz.swapee.wc.RecordIFileEditorController

/** @typedef {xyz.swapee.wc.IFileEditorController} xyz.swapee.wc.BoundIFileEditorController */

/** @typedef {xyz.swapee.wc.FileEditorController} xyz.swapee.wc.BoundFileEditorController */

/** @typedef {xyz.swapee.wc.IFileEditorPort.Inputs} xyz.swapee.wc.IFileEditorController.Inputs The inputs to the _IFileEditor_'s controller. */

/** @typedef {xyz.swapee.wc.IFileEditorPort.WeakInputs} xyz.swapee.wc.IFileEditorController.WeakInputs The inputs to the _IFileEditor_'s controller. */

/**
 * Contains getters to cast the _IFileEditorController_ interface.
 * @interface xyz.swapee.wc.IFileEditorControllerCaster
 */
xyz.swapee.wc.IFileEditorControllerCaster = class { }
/**
 * Cast the _IFileEditorController_ instance into the _BoundIFileEditorController_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorController}
 */
xyz.swapee.wc.IFileEditorControllerCaster.prototype.asIFileEditorController
/**
 * Cast the _IFileEditorController_ instance into the _BoundIFileEditorProcessor_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorProcessor}
 */
xyz.swapee.wc.IFileEditorControllerCaster.prototype.asIFileEditorProcessor
/**
 * Access the _FileEditorController_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorController}
 */
xyz.swapee.wc.IFileEditorControllerCaster.prototype.superFileEditorController

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorController.__resetPort
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__resetPort<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._resetPort */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.resetPort} */
/**
 * Resets the port.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorController.resetPort = function() {}

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => ?} xyz.swapee.wc.IFileEditorController.__openFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__openFile<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._openFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.openFile} */
/**
 * Opens the new file immediately, unless changes were made, in which case the
 * user will be prompted to save the file first.
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {?}
 */
xyz.swapee.wc.IFileEditorController.openFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorController.__loadReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__loadReadFile<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._loadReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.loadReadFile} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorController.loadReadFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorController.__loadSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__loadSaveFile<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._loadSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.loadSaveFile} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorController.loadSaveFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorController.__pulseReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__pulseReadFile<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._pulseReadFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.pulseReadFile} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorController.pulseReadFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.IFileEditorController.__pulseSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorController.__pulseSaveFile<!xyz.swapee.wc.IFileEditorController>} xyz.swapee.wc.IFileEditorController._pulseSaveFile */
/** @typedef {typeof xyz.swapee.wc.IFileEditorController.pulseSaveFile} */
/**
 * The pulse method after which the memory value will be set to `null`.
 * @return {void}
 */
xyz.swapee.wc.IFileEditorController.pulseSaveFile = function() {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/51-IFileEditorControllerFront.xml}  2f7ac075e3cd4e8f65c9b5eb33f5b772 */
/** @typedef {typeof __$te_plain} xyz.swapee.wc.front.IFileEditorController.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileEditorController)} xyz.swapee.wc.front.AbstractFileEditorController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileEditorController} xyz.swapee.wc.front.FileEditorController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileEditorController` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileEditorController
 */
xyz.swapee.wc.front.AbstractFileEditorController = class extends /** @type {xyz.swapee.wc.front.AbstractFileEditorController.constructor&xyz.swapee.wc.front.FileEditorController.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileEditorController.prototype.constructor = xyz.swapee.wc.front.AbstractFileEditorController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileEditorController.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileEditorController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorController}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileEditorController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileEditorController}
 */
xyz.swapee.wc.front.AbstractFileEditorController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorController}
 */
xyz.swapee.wc.front.AbstractFileEditorController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorController}
 */
xyz.swapee.wc.front.AbstractFileEditorController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorController}
 */
xyz.swapee.wc.front.AbstractFileEditorController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileEditorController.Initialese[]) => xyz.swapee.wc.front.IFileEditorController} xyz.swapee.wc.front.FileEditorControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileEditorControllerCaster&xyz.swapee.wc.front.IFileEditorControllerAT)} xyz.swapee.wc.front.IFileEditorController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorControllerAT} xyz.swapee.wc.front.IFileEditorControllerAT.typeof */
/**
 * The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @interface xyz.swapee.wc.front.IFileEditorController
 */
xyz.swapee.wc.front.IFileEditorController = class extends /** @type {xyz.swapee.wc.front.IFileEditorController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.front.IFileEditorControllerAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileEditorController.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.front.IFileEditorController.openFile} */
xyz.swapee.wc.front.IFileEditorController.prototype.openFile = function() {}
/** @type {xyz.swapee.wc.front.IFileEditorController.loadReadFile} */
xyz.swapee.wc.front.IFileEditorController.prototype.loadReadFile = function() {}
/** @type {xyz.swapee.wc.front.IFileEditorController.loadSaveFile} */
xyz.swapee.wc.front.IFileEditorController.prototype.loadSaveFile = function() {}
/** @type {xyz.swapee.wc.front.IFileEditorController.pulseReadFile} */
xyz.swapee.wc.front.IFileEditorController.prototype.pulseReadFile = function() {}
/** @type {xyz.swapee.wc.front.IFileEditorController.pulseSaveFile} */
xyz.swapee.wc.front.IFileEditorController.prototype.pulseSaveFile = function() {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileEditorController&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorController.Initialese>)} xyz.swapee.wc.front.FileEditorController.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController} xyz.swapee.wc.front.IFileEditorController.typeof */
/**
 * A concrete class of _IFileEditorController_ instances.
 * @constructor xyz.swapee.wc.front.FileEditorController
 * @implements {xyz.swapee.wc.front.IFileEditorController} The front-end interface for the controller that will pass the inputs to the
 * back controller when set, and invoke its methods via _uart_.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorController.Initialese>} ‎
 */
xyz.swapee.wc.front.FileEditorController = class extends /** @type {xyz.swapee.wc.front.FileEditorController.constructor&xyz.swapee.wc.front.IFileEditorController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileEditorController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileEditorController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileEditorController}
 */
xyz.swapee.wc.front.FileEditorController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileEditorController} */
xyz.swapee.wc.front.RecordIFileEditorController

/** @typedef {xyz.swapee.wc.front.IFileEditorController} xyz.swapee.wc.front.BoundIFileEditorController */

/** @typedef {xyz.swapee.wc.front.FileEditorController} xyz.swapee.wc.front.BoundFileEditorController */

/**
 * Contains getters to cast the _IFileEditorController_ interface.
 * @interface xyz.swapee.wc.front.IFileEditorControllerCaster
 */
xyz.swapee.wc.front.IFileEditorControllerCaster = class { }
/**
 * Cast the _IFileEditorController_ instance into the _BoundIFileEditorController_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileEditorController}
 */
xyz.swapee.wc.front.IFileEditorControllerCaster.prototype.asIFileEditorController
/**
 * Access the _FileEditorController_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileEditorController}
 */
xyz.swapee.wc.front.IFileEditorControllerCaster.prototype.superFileEditorController

/**
 * @typedef {(this: THIS, path: string, name: string, lang: string) => ?} xyz.swapee.wc.front.IFileEditorController.__openFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileEditorController.__openFile<!xyz.swapee.wc.front.IFileEditorController>} xyz.swapee.wc.front.IFileEditorController._openFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController.openFile} */
/**
 * Opens the new file immediately, unless changes were made, in which case the
 * user will be prompted to save the file first.
 * @param {string} path The path on the FS.
 * @param {string} name The name of the file.
 * @param {string} lang The language.
 * @return {?}
 */
xyz.swapee.wc.front.IFileEditorController.openFile = function(path, name, lang) {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileEditorController.__loadReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileEditorController.__loadReadFile<!xyz.swapee.wc.front.IFileEditorController>} xyz.swapee.wc.front.IFileEditorController._loadReadFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController.loadReadFile} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.IFileEditorController.loadReadFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileEditorController.__loadSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileEditorController.__loadSaveFile<!xyz.swapee.wc.front.IFileEditorController>} xyz.swapee.wc.front.IFileEditorController._loadSaveFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController.loadSaveFile} */
/**
 * Manually invokes loading.
 * @return {void}
 */
xyz.swapee.wc.front.IFileEditorController.loadSaveFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileEditorController.__pulseReadFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileEditorController.__pulseReadFile<!xyz.swapee.wc.front.IFileEditorController>} xyz.swapee.wc.front.IFileEditorController._pulseReadFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController.pulseReadFile} */
/** @return {void} */
xyz.swapee.wc.front.IFileEditorController.pulseReadFile = function() {}

/**
 * @typedef {(this: THIS) => void} xyz.swapee.wc.front.IFileEditorController.__pulseSaveFile
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.front.IFileEditorController.__pulseSaveFile<!xyz.swapee.wc.front.IFileEditorController>} xyz.swapee.wc.front.IFileEditorController._pulseSaveFile */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController.pulseSaveFile} */
/** @return {void} */
xyz.swapee.wc.front.IFileEditorController.pulseSaveFile = function() {}

// nss:xyz.swapee.wc.front,xyz.swapee.wc.front.IFileEditorController
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/52-IFileEditorControllerBack.xml}  9190867e465652ba20efafa8e56dab01 */
/** @typedef {com.webcircuits.IDriverBack.Initialese<!xyz.swapee.wc.IFileEditorController.Inputs>&xyz.swapee.wc.IFileEditorController.Initialese} xyz.swapee.wc.back.IFileEditorController.Initialese */

/** @typedef {function(new: xyz.swapee.wc.back.FileEditorController)} xyz.swapee.wc.back.AbstractFileEditorController.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileEditorController} xyz.swapee.wc.back.FileEditorController.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileEditorController` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileEditorController
 */
xyz.swapee.wc.back.AbstractFileEditorController = class extends /** @type {xyz.swapee.wc.back.AbstractFileEditorController.constructor&xyz.swapee.wc.back.FileEditorController.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileEditorController.prototype.constructor = xyz.swapee.wc.back.AbstractFileEditorController
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileEditorController.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileEditorController} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorController}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileEditorController.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileEditorController}
 */
xyz.swapee.wc.back.AbstractFileEditorController.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorController}
 */
xyz.swapee.wc.back.AbstractFileEditorController.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorController}
 */
xyz.swapee.wc.back.AbstractFileEditorController.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileEditorController|typeof xyz.swapee.wc.back.FileEditorController)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)|(!com.webcircuits.IDriverBack|typeof com.webcircuits.DriverBack)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorController}
 */
xyz.swapee.wc.back.AbstractFileEditorController.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileEditorController.Initialese[]) => xyz.swapee.wc.back.IFileEditorController} xyz.swapee.wc.back.FileEditorControllerConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileEditorControllerCaster&xyz.swapee.wc.IFileEditorController&com.webcircuits.IDriverBack<!xyz.swapee.wc.IFileEditorController.Inputs>)} xyz.swapee.wc.back.IFileEditorController.constructor */
/** @typedef {typeof com.webcircuits.IDriverBack} com.webcircuits.IDriverBack.typeof */
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @interface xyz.swapee.wc.back.IFileEditorController
 */
xyz.swapee.wc.back.IFileEditorController = class extends /** @type {xyz.swapee.wc.back.IFileEditorController.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileEditorController.typeof&com.webcircuits.IDriverBack.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileEditorController.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorController&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorController.Initialese>)} xyz.swapee.wc.back.FileEditorController.constructor */
/**
 * A concrete class of _IFileEditorController_ instances.
 * @constructor xyz.swapee.wc.back.FileEditorController
 * @implements {xyz.swapee.wc.back.IFileEditorController} The back-end controller with a UART driver to receive commands from the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorController.Initialese>} ‎
 */
xyz.swapee.wc.back.FileEditorController = class extends /** @type {xyz.swapee.wc.back.FileEditorController.constructor&xyz.swapee.wc.back.IFileEditorController.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorController* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileEditorController.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorController* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorController.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileEditorController.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileEditorController}
 */
xyz.swapee.wc.back.FileEditorController.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileEditorController} */
xyz.swapee.wc.back.RecordIFileEditorController

/** @typedef {xyz.swapee.wc.back.IFileEditorController} xyz.swapee.wc.back.BoundIFileEditorController */

/** @typedef {xyz.swapee.wc.back.FileEditorController} xyz.swapee.wc.back.BoundFileEditorController */

/**
 * Contains getters to cast the _IFileEditorController_ interface.
 * @interface xyz.swapee.wc.back.IFileEditorControllerCaster
 */
xyz.swapee.wc.back.IFileEditorControllerCaster = class { }
/**
 * Cast the _IFileEditorController_ instance into the _BoundIFileEditorController_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileEditorController}
 */
xyz.swapee.wc.back.IFileEditorControllerCaster.prototype.asIFileEditorController
/**
 * Access the _FileEditorController_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileEditorController}
 */
xyz.swapee.wc.back.IFileEditorControllerCaster.prototype.superFileEditorController

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/53-IFileEditorControllerAR.xml}  a439d113b2893c11087f528057fd16fa */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IFileEditorController.Initialese} xyz.swapee.wc.back.IFileEditorControllerAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileEditorControllerAR)} xyz.swapee.wc.back.AbstractFileEditorControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileEditorControllerAR} xyz.swapee.wc.back.FileEditorControllerAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileEditorControllerAR` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileEditorControllerAR
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR = class extends /** @type {xyz.swapee.wc.back.AbstractFileEditorControllerAR.constructor&xyz.swapee.wc.back.FileEditorControllerAR.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileEditorControllerAR.prototype.constructor = xyz.swapee.wc.back.AbstractFileEditorControllerAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileEditorControllerAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileEditorControllerAR|typeof xyz.swapee.wc.back.FileEditorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorControllerAR}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileEditorControllerAR}
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorControllerAR}
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileEditorControllerAR|typeof xyz.swapee.wc.back.FileEditorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorControllerAR}
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileEditorControllerAR|typeof xyz.swapee.wc.back.FileEditorControllerAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorController|typeof xyz.swapee.wc.FileEditorController)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorControllerAR}
 */
xyz.swapee.wc.back.AbstractFileEditorControllerAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileEditorControllerAR.Initialese[]) => xyz.swapee.wc.back.IFileEditorControllerAR} xyz.swapee.wc.back.FileEditorControllerARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileEditorControllerARCaster&com.webcircuits.IAR&xyz.swapee.wc.IFileEditorController)} xyz.swapee.wc.back.IFileEditorControllerAR.constructor */
/** @typedef {typeof com.webcircuits.IAR} com.webcircuits.IAR.typeof */
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IFileEditorControllerAT_ to send data.
 * @interface xyz.swapee.wc.back.IFileEditorControllerAR
 */
xyz.swapee.wc.back.IFileEditorControllerAR = class extends /** @type {xyz.swapee.wc.back.IFileEditorControllerAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IFileEditorController.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileEditorControllerAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorControllerAR&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorControllerAR.Initialese>)} xyz.swapee.wc.back.FileEditorControllerAR.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorControllerAR} xyz.swapee.wc.back.IFileEditorControllerAR.typeof */
/**
 * A concrete class of _IFileEditorControllerAR_ instances.
 * @constructor xyz.swapee.wc.back.FileEditorControllerAR
 * @implements {xyz.swapee.wc.back.IFileEditorControllerAR} Asynchronously receives commands to the controller from a remote system that
 * used _IFileEditorControllerAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorControllerAR.Initialese>} ‎
 */
xyz.swapee.wc.back.FileEditorControllerAR = class extends /** @type {xyz.swapee.wc.back.FileEditorControllerAR.constructor&xyz.swapee.wc.back.IFileEditorControllerAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorControllerAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileEditorControllerAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorControllerAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorControllerAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileEditorControllerAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileEditorControllerAR}
 */
xyz.swapee.wc.back.FileEditorControllerAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileEditorControllerAR} */
xyz.swapee.wc.back.RecordIFileEditorControllerAR

/** @typedef {xyz.swapee.wc.back.IFileEditorControllerAR} xyz.swapee.wc.back.BoundIFileEditorControllerAR */

/** @typedef {xyz.swapee.wc.back.FileEditorControllerAR} xyz.swapee.wc.back.BoundFileEditorControllerAR */

/**
 * Contains getters to cast the _IFileEditorControllerAR_ interface.
 * @interface xyz.swapee.wc.back.IFileEditorControllerARCaster
 */
xyz.swapee.wc.back.IFileEditorControllerARCaster = class { }
/**
 * Cast the _IFileEditorControllerAR_ instance into the _BoundIFileEditorControllerAR_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileEditorControllerAR}
 */
xyz.swapee.wc.back.IFileEditorControllerARCaster.prototype.asIFileEditorControllerAR
/**
 * Access the _FileEditorControllerAR_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileEditorControllerAR}
 */
xyz.swapee.wc.back.IFileEditorControllerARCaster.prototype.superFileEditorControllerAR

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/54-IFileEditorControllerAT.xml}  d63645c4224172f0bb99497b38fce185 */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.front.IFileEditorControllerAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileEditorControllerAT)} xyz.swapee.wc.front.AbstractFileEditorControllerAT.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileEditorControllerAT} xyz.swapee.wc.front.FileEditorControllerAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileEditorControllerAT` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileEditorControllerAT
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT = class extends /** @type {xyz.swapee.wc.front.AbstractFileEditorControllerAT.constructor&xyz.swapee.wc.front.FileEditorControllerAT.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileEditorControllerAT.prototype.constructor = xyz.swapee.wc.front.AbstractFileEditorControllerAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileEditorControllerAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorControllerAT}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileEditorControllerAT}
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorControllerAT}
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorControllerAT}
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileEditorControllerAT|typeof xyz.swapee.wc.front.FileEditorControllerAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorControllerAT}
 */
xyz.swapee.wc.front.AbstractFileEditorControllerAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileEditorControllerAT.Initialese[]) => xyz.swapee.wc.front.IFileEditorControllerAT} xyz.swapee.wc.front.FileEditorControllerATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileEditorControllerATCaster&com.webcircuits.IAT)} xyz.swapee.wc.front.IFileEditorControllerAT.constructor */
/** @typedef {typeof com.webcircuits.IAT} com.webcircuits.IAT.typeof */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileEditorControllerAR_ trait.
 * @interface xyz.swapee.wc.front.IFileEditorControllerAT
 */
xyz.swapee.wc.front.IFileEditorControllerAT = class extends /** @type {xyz.swapee.wc.front.IFileEditorControllerAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileEditorControllerAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileEditorControllerAT&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorControllerAT.Initialese>)} xyz.swapee.wc.front.FileEditorControllerAT.constructor */
/**
 * A concrete class of _IFileEditorControllerAT_ instances.
 * @constructor xyz.swapee.wc.front.FileEditorControllerAT
 * @implements {xyz.swapee.wc.front.IFileEditorControllerAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileEditorControllerAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorControllerAT.Initialese>} ‎
 */
xyz.swapee.wc.front.FileEditorControllerAT = class extends /** @type {xyz.swapee.wc.front.FileEditorControllerAT.constructor&xyz.swapee.wc.front.IFileEditorControllerAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorControllerAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileEditorControllerAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorControllerAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorControllerAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileEditorControllerAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileEditorControllerAT}
 */
xyz.swapee.wc.front.FileEditorControllerAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileEditorControllerAT} */
xyz.swapee.wc.front.RecordIFileEditorControllerAT

/** @typedef {xyz.swapee.wc.front.IFileEditorControllerAT} xyz.swapee.wc.front.BoundIFileEditorControllerAT */

/** @typedef {xyz.swapee.wc.front.FileEditorControllerAT} xyz.swapee.wc.front.BoundFileEditorControllerAT */

/**
 * Contains getters to cast the _IFileEditorControllerAT_ interface.
 * @interface xyz.swapee.wc.front.IFileEditorControllerATCaster
 */
xyz.swapee.wc.front.IFileEditorControllerATCaster = class { }
/**
 * Cast the _IFileEditorControllerAT_ instance into the _BoundIFileEditorControllerAT_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileEditorControllerAT}
 */
xyz.swapee.wc.front.IFileEditorControllerATCaster.prototype.asIFileEditorControllerAT
/**
 * Access the _FileEditorControllerAT_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileEditorControllerAT}
 */
xyz.swapee.wc.front.IFileEditorControllerATCaster.prototype.superFileEditorControllerAT

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/70-IFileEditorScreen.xml}  f9d035c554d99cd43dc8fb54519c4354 */
/** @typedef {com.webcircuits.IScreen.Initialese<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.front.FileEditorInputs, !HTMLDivElement, !xyz.swapee.wc.IFileEditorDisplay.Settings, !xyz.swapee.wc.IFileEditorDisplay.Queries, null>&xyz.swapee.wc.IFileEditorDisplay.Initialese} xyz.swapee.wc.IFileEditorScreen.Initialese */

/** @typedef {function(new: xyz.swapee.wc.FileEditorScreen)} xyz.swapee.wc.AbstractFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorScreen} xyz.swapee.wc.FileEditorScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorScreen` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorScreen
 */
xyz.swapee.wc.AbstractFileEditorScreen = class extends /** @type {xyz.swapee.wc.AbstractFileEditorScreen.constructor&xyz.swapee.wc.FileEditorScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorScreen.prototype.constructor = xyz.swapee.wc.AbstractFileEditorScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorScreen.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorScreen}
 */
xyz.swapee.wc.AbstractFileEditorScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreen}
 */
xyz.swapee.wc.AbstractFileEditorScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreen}
 */
xyz.swapee.wc.AbstractFileEditorScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)|(!com.webcircuits.IScreen|typeof com.webcircuits.Screen)|(!xyz.swapee.wc.front.IFileEditorController|typeof xyz.swapee.wc.front.FileEditorController)|(!xyz.swapee.wc.IFileEditorDisplay|typeof xyz.swapee.wc.FileEditorDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreen}
 */
xyz.swapee.wc.AbstractFileEditorScreen.__trait = function(...Implementations) {}

/**
 * A hyper slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSyncConfirm=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel._beforeSyncConfirm|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel._beforeSyncConfirm>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSyncConfirm=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirm|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirm>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSyncConfirmThrows=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmThrows|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmThrows>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSyncConfirmReturns=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmReturns|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmReturns>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSyncConfirmCancels=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmCancels|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmCancels>} */ (void 0)
  }
}
xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice

/**
 * A concrete class of _IFileEditorScreenJoinpointModelHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreenJoinpointModelHyperslice
 * @implements {xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice} A hyper slice includes all combined methods of an interface.
 */
xyz.swapee.wc.FileEditorScreenJoinpointModelHyperslice = class extends xyz.swapee.wc.IFileEditorScreenJoinpointModelHyperslice { }
xyz.swapee.wc.FileEditorScreenJoinpointModelHyperslice.prototype.constructor = xyz.swapee.wc.FileEditorScreenJoinpointModelHyperslice

/**
 * A hyper-slice includes all combined methods of an interface.
 * @interface xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice
 * @template THIS
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice = class {
  constructor() {
    /**
     * Before the method executes. Allows to cancel the method.
     */
    this.beforeSyncConfirm=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__beforeSyncConfirm<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__beforeSyncConfirm<THIS>>} */ (void 0)
    /**
     * After the method.
     */
    this.afterSyncConfirm=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirm<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirm<THIS>>} */ (void 0)
    /**
     * After the method throw.
     */
    this.afterSyncConfirmThrows=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmThrows<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmThrows<THIS>>} */ (void 0)
    /**
     * After the method returned.
     */
    this.afterSyncConfirmReturns=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmReturns<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmReturns<THIS>>} */ (void 0)
    /**
     * After the method cancels.
     */
    this.afterSyncConfirmCancels=/** @type {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmCancels<THIS>|!engineering.type.RecursiveArray<!xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmCancels<THIS>>} */ (void 0)
  }
}
xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice

/**
 * A concrete class of _IFileEditorScreenJoinpointModelBindingHyperslice_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreenJoinpointModelBindingHyperslice
 * @implements {xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice<THIS>} A hyper-slice includes all combined methods of an interface.
 * @template THIS
 * @extends {xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice<THIS>}
 */
xyz.swapee.wc.FileEditorScreenJoinpointModelBindingHyperslice = class extends xyz.swapee.wc.IFileEditorScreenJoinpointModelBindingHyperslice { }
xyz.swapee.wc.FileEditorScreenJoinpointModelBindingHyperslice.prototype.constructor = xyz.swapee.wc.FileEditorScreenJoinpointModelBindingHyperslice

/**
 * An interface that enumerates the joinpoints of `IFileEditorScreen`'s methods.
 * @interface xyz.swapee.wc.IFileEditorScreenJoinpointModel
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel = class { }
/** @type {xyz.swapee.wc.IFileEditorScreenJoinpointModel.beforeSyncConfirm} */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.prototype.beforeSyncConfirm = function() {}
/** @type {xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirm} */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.prototype.afterSyncConfirm = function() {}
/** @type {xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmThrows} */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.prototype.afterSyncConfirmThrows = function() {}
/** @type {xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmReturns} */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.prototype.afterSyncConfirmReturns = function() {}
/** @type {xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmCancels} */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.prototype.afterSyncConfirmCancels = function() {}

/**
 * A concrete class of _IFileEditorScreenJoinpointModel_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreenJoinpointModel
 * @implements {xyz.swapee.wc.IFileEditorScreenJoinpointModel} An interface that enumerates the joinpoints of `IFileEditorScreen`'s methods.
 */
xyz.swapee.wc.FileEditorScreenJoinpointModel = class extends xyz.swapee.wc.IFileEditorScreenJoinpointModel { }
xyz.swapee.wc.FileEditorScreenJoinpointModel.prototype.constructor = xyz.swapee.wc.FileEditorScreenJoinpointModel

/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel} */
xyz.swapee.wc.RecordIFileEditorScreenJoinpointModel

/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel} xyz.swapee.wc.BoundIFileEditorScreenJoinpointModel */

/** @typedef {xyz.swapee.wc.FileEditorScreenJoinpointModel} xyz.swapee.wc.BoundFileEditorScreenJoinpointModel */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorScreenAspectsInstaller)} xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller} xyz.swapee.wc.FileEditorScreenAspectsInstaller.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorScreenAspectsInstaller` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller = class extends /** @type {xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.constructor&xyz.swapee.wc.FileEditorScreenAspectsInstaller.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.prototype.constructor = xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller|typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller|typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller|typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese[]) => xyz.swapee.wc.IFileEditorScreenAspectsInstaller} xyz.swapee.wc.FileEditorScreenAspectsInstallerConstructor */

/** @typedef {function(new: engineering.type.IEngineer)} xyz.swapee.wc.IFileEditorScreenAspectsInstaller.constructor */
/** @typedef {typeof engineering.type.IEngineer} engineering.type.IEngineer.typeof */
/** @interface xyz.swapee.wc.IFileEditorScreenAspectsInstaller */
xyz.swapee.wc.IFileEditorScreenAspectsInstaller = class extends /** @type {xyz.swapee.wc.IFileEditorScreenAspectsInstaller.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
  constructor() {
    super(...init)
    this.beforeSyncConfirm=/** @type {number} */ (void 0)
    this.afterSyncConfirm=/** @type {number} */ (void 0)
    this.afterSyncConfirmThrows=/** @type {number} */ (void 0)
    this.afterSyncConfirmReturns=/** @type {number} */ (void 0)
    this.afterSyncConfirmCancels=/** @type {number} */ (void 0)
  }
  /**
   * A joinpointer.
   * @return {?}
   */
  syncConfirm() { }
}
/**
 * Create a new *IFileEditorScreenAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorScreenAspectsInstaller.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorScreenAspectsInstaller&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese>)} xyz.swapee.wc.FileEditorScreenAspectsInstaller.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenAspectsInstaller} xyz.swapee.wc.IFileEditorScreenAspectsInstaller.typeof */
/** @typedef {typeof engineering.type.IInitialiser} engineering.type.IInitialiser.typeof */
/**
 * A concrete class of _IFileEditorScreenAspectsInstaller_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreenAspectsInstaller
 * @implements {xyz.swapee.wc.IFileEditorScreenAspectsInstaller} ‎
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorScreenAspectsInstaller = class extends /** @type {xyz.swapee.wc.FileEditorScreenAspectsInstaller.constructor&xyz.swapee.wc.IFileEditorScreenAspectsInstaller.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreenAspectsInstaller* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreenAspectsInstaller* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspectsInstaller.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorScreenAspectsInstaller.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspectsInstaller}
 */
xyz.swapee.wc.FileEditorScreenAspectsInstaller.__extend = function(...Extensions) {}

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorScreen.SyncConfirmNArgs
 * @prop {string} message The props to set when confirmed.
 */

/**
 * @typedef {Object} xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData The general pointcut data.
 * @prop {symbol} ticket A symbol unique to the particular execution.
 * @prop {xyz.swapee.wc.IFileEditorScreen.SyncConfirmNArgs} args The arguments passed to the method converted to a lambda-n record.
 * @prop {!Function} proc Allows to work with the process.
 */

/**
 * @typedef {Object} $xyz.swapee.wc.IFileEditorScreenJoinpointModel.BeforeSyncConfirmPointcutData
 * @prop {(arg0: !Object<string, *>) => void} cond Conditions the object to the process.
 * @prop {(args: xyz.swapee.wc.IFileEditorScreen.SyncConfirmNArgs) => void} setArgs Allows to set or modify arguments passed to the method and futher aspects.
 * @prop {(reason?: string) => void} cancel Allows to stop the `syncConfirm` method from being executed.
 */
/** @typedef {$xyz.swapee.wc.IFileEditorScreenJoinpointModel.BeforeSyncConfirmPointcutData&xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData} xyz.swapee.wc.IFileEditorScreenJoinpointModel.BeforeSyncConfirmPointcutData The pointcut data passed to _before_ aspects. */

/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData} xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterSyncConfirmPointcutData The pointcut data passed to _after_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterThrowsSyncConfirmPointcutData
 * @prop {!Error} err The error with which the method failed.
 * @prop {() => void} hide Hides the error in the `syncConfirm` (use only at the top-level of program flow).
 */
/** @typedef {$xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterThrowsSyncConfirmPointcutData&xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData} xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterThrowsSyncConfirmPointcutData The pointcut data passed to _afterThrows_ aspects. */

/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData} xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterReturnsSyncConfirmPointcutData The pointcut data passed to _afterReturns_ aspects. */

/**
 * @typedef {Object} $xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterCancelsSyncConfirmPointcutData
 * @prop {!Set<string>} reasons The list of reasons why the method was cancelled.
 */
/** @typedef {$xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterCancelsSyncConfirmPointcutData&xyz.swapee.wc.IFileEditorScreenJoinpointModel.SyncConfirmPointcutData} xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterCancelsSyncConfirmPointcutData The pointcut data passed to _afterCancels_ aspects. */

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorScreen.Initialese[]) => xyz.swapee.wc.IFileEditorScreen} xyz.swapee.wc.FileEditorScreenConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorScreenFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorScreenCaster&com.webcircuits.IScreen<!xyz.swapee.wc.FileEditorMemory, !xyz.swapee.wc.front.FileEditorInputs, !HTMLDivElement, !xyz.swapee.wc.IFileEditorDisplay.Settings, !xyz.swapee.wc.IFileEditorDisplay.Queries, null, null>&xyz.swapee.wc.front.IFileEditorController&xyz.swapee.wc.IFileEditorDisplay)} xyz.swapee.wc.IFileEditorScreen.constructor */
/** @typedef {typeof com.webcircuits.IScreen} com.webcircuits.IScreen.typeof */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorController} xyz.swapee.wc.front.IFileEditorController.typeof */
/** @typedef {typeof xyz.swapee.wc.IFileEditorDisplay} xyz.swapee.wc.IFileEditorDisplay.typeof */
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @interface xyz.swapee.wc.IFileEditorScreen
 */
xyz.swapee.wc.IFileEditorScreen = class extends /** @type {xyz.swapee.wc.IFileEditorScreen.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IScreen.typeof&xyz.swapee.wc.front.IFileEditorController.typeof&xyz.swapee.wc.IFileEditorDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.IFileEditorScreen.syncConfirm} */
xyz.swapee.wc.IFileEditorScreen.prototype.syncConfirm = function() {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreen.Initialese>)} xyz.swapee.wc.FileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreen} xyz.swapee.wc.IFileEditorScreen.typeof */
/**
 * A concrete class of _IFileEditorScreen_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreen
 * @implements {xyz.swapee.wc.IFileEditorScreen} The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreen.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorScreen = class extends /** @type {xyz.swapee.wc.FileEditorScreen.constructor&xyz.swapee.wc.IFileEditorScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorScreen}
 */
xyz.swapee.wc.FileEditorScreen.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorScreen.
 * @interface xyz.swapee.wc.IFileEditorScreenFields
 */
xyz.swapee.wc.IFileEditorScreenFields = class { }
/**
 * The reference to the editor instance present on the page.
 */
xyz.swapee.wc.IFileEditorScreenFields.prototype.editor = /** @type {*} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorScreen} */
xyz.swapee.wc.RecordIFileEditorScreen

/** @typedef {xyz.swapee.wc.IFileEditorScreen} xyz.swapee.wc.BoundIFileEditorScreen */

/** @typedef {xyz.swapee.wc.FileEditorScreen} xyz.swapee.wc.BoundFileEditorScreen */

/** @typedef {xyz.swapee.wc.IFileEditorScreenAspects.Initialese&xyz.swapee.wc.IFileEditorScreen.Initialese} xyz.swapee.wc.IHyperFileEditorScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.HyperFileEditorScreen)} xyz.swapee.wc.AbstractHyperFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.HyperFileEditorScreen} xyz.swapee.wc.HyperFileEditorScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IHyperFileEditorScreen` interface.
 * @constructor xyz.swapee.wc.AbstractHyperFileEditorScreen
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen = class extends /** @type {xyz.swapee.wc.AbstractHyperFileEditorScreen.constructor&xyz.swapee.wc.HyperFileEditorScreen.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractHyperFileEditorScreen.prototype.constructor = xyz.swapee.wc.AbstractHyperFileEditorScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.class = /** @type {typeof xyz.swapee.wc.AbstractHyperFileEditorScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IHyperFileEditorScreen|typeof xyz.swapee.wc.HyperFileEditorScreen)|(!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperFileEditorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractHyperFileEditorScreen}
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperFileEditorScreen}
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IHyperFileEditorScreen|typeof xyz.swapee.wc.HyperFileEditorScreen)|(!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperFileEditorScreen}
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IHyperFileEditorScreen|typeof xyz.swapee.wc.HyperFileEditorScreen)|(!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.HyperFileEditorScreen}
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.__trait = function(...Implementations) {}
/**
 * Consult aides for their advice on methods to be implemented, so that all implementation methods provided on subsequent `.implements` calls will be advised according to this consultation.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects|function(new: xyz.swapee.wc.IFileEditorScreenAspects)} aides The list of aides that advise the IFileEditorScreen to implement aspects.
 * @return {typeof xyz.swapee.wc.AbstractHyperFileEditorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractHyperFileEditorScreen.consults = function(...aides) {}

/** @typedef {new (...args: !xyz.swapee.wc.IHyperFileEditorScreen.Initialese[]) => xyz.swapee.wc.IHyperFileEditorScreen} xyz.swapee.wc.HyperFileEditorScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IHyperFileEditorScreenCaster&xyz.swapee.wc.IFileEditorScreenAspects&xyz.swapee.wc.IFileEditorScreen)} xyz.swapee.wc.IHyperFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenAspects} xyz.swapee.wc.IFileEditorScreenAspects.typeof */
/**
 * The hyper screen with aspects.
 * @interface xyz.swapee.wc.IHyperFileEditorScreen
 */
xyz.swapee.wc.IHyperFileEditorScreen = class extends /** @type {xyz.swapee.wc.IHyperFileEditorScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.IFileEditorScreenAspects.typeof&xyz.swapee.wc.IFileEditorScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IHyperFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IHyperFileEditorScreen.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IHyperFileEditorScreen&engineering.type.IInitialiser<!xyz.swapee.wc.IHyperFileEditorScreen.Initialese>)} xyz.swapee.wc.HyperFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.IHyperFileEditorScreen} xyz.swapee.wc.IHyperFileEditorScreen.typeof */
/**
 * A concrete class of _IHyperFileEditorScreen_ instances.
 * @constructor xyz.swapee.wc.HyperFileEditorScreen
 * @implements {xyz.swapee.wc.IHyperFileEditorScreen} The hyper screen with aspects.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IHyperFileEditorScreen.Initialese>} ‎
 */
xyz.swapee.wc.HyperFileEditorScreen = class extends /** @type {xyz.swapee.wc.HyperFileEditorScreen.constructor&xyz.swapee.wc.IHyperFileEditorScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IHyperFileEditorScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IHyperFileEditorScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IHyperFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IHyperFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.HyperFileEditorScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.HyperFileEditorScreen}
 */
xyz.swapee.wc.HyperFileEditorScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IHyperFileEditorScreen} */
xyz.swapee.wc.RecordIHyperFileEditorScreen

/** @typedef {xyz.swapee.wc.IHyperFileEditorScreen} xyz.swapee.wc.BoundIHyperFileEditorScreen */

/** @typedef {xyz.swapee.wc.HyperFileEditorScreen} xyz.swapee.wc.BoundHyperFileEditorScreen */

/** @typedef {typeof __$te_plain} xyz.swapee.wc.IFileEditorScreenAspects.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorScreenAspects)} xyz.swapee.wc.AbstractFileEditorScreenAspects.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorScreenAspects} xyz.swapee.wc.FileEditorScreenAspects.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorScreenAspects` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorScreenAspects
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects = class extends /** @type {xyz.swapee.wc.AbstractFileEditorScreenAspects.constructor&xyz.swapee.wc.FileEditorScreenAspects.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorScreenAspects.prototype.constructor = xyz.swapee.wc.AbstractFileEditorScreenAspects
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspects} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspects}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorScreenAspects}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspects}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspects}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects|typeof xyz.swapee.wc.FileEditorScreenAspects} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspects}
 */
xyz.swapee.wc.AbstractFileEditorScreenAspects.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorScreenAspects.Initialese[]) => xyz.swapee.wc.IFileEditorScreenAspects} xyz.swapee.wc.FileEditorScreenAspectsConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.IFileEditorScreenAspectsCaster)} xyz.swapee.wc.IFileEditorScreenAspects.constructor */
/**
 * The aspects of the screen.
 * @interface xyz.swapee.wc.IFileEditorScreenAspects
 */
xyz.swapee.wc.IFileEditorScreenAspects = class extends /** @type {xyz.swapee.wc.IFileEditorScreenAspects.constructor&engineering.type.IEngineer.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorScreenAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorScreenAspects.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorScreenAspects&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreenAspects.Initialese>)} xyz.swapee.wc.FileEditorScreenAspects.constructor */
/**
 * A concrete class of _IFileEditorScreenAspects_ instances.
 * @constructor xyz.swapee.wc.FileEditorScreenAspects
 * @implements {xyz.swapee.wc.IFileEditorScreenAspects} The aspects of the screen.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorScreenAspects.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorScreenAspects = class extends /** @type {xyz.swapee.wc.FileEditorScreenAspects.constructor&xyz.swapee.wc.IFileEditorScreenAspects.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreenAspects* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorScreenAspects.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreenAspects* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorScreenAspects.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorScreenAspects.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorScreenAspects}
 */
xyz.swapee.wc.FileEditorScreenAspects.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.IFileEditorScreenAspects} */
xyz.swapee.wc.RecordIFileEditorScreenAspects

/** @typedef {xyz.swapee.wc.IFileEditorScreenAspects} xyz.swapee.wc.BoundIFileEditorScreenAspects */

/** @typedef {xyz.swapee.wc.FileEditorScreenAspects} xyz.swapee.wc.BoundFileEditorScreenAspects */

/**
 * Contains getters to cast the _IFileEditorScreen_ interface.
 * @interface xyz.swapee.wc.IFileEditorScreenCaster
 */
xyz.swapee.wc.IFileEditorScreenCaster = class { }
/**
 * Cast the _IFileEditorScreen_ instance into the _BoundIFileEditorScreen_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorScreen}
 */
xyz.swapee.wc.IFileEditorScreenCaster.prototype.asIFileEditorScreen
/**
 * Access the _FileEditorScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorScreen}
 */
xyz.swapee.wc.IFileEditorScreenCaster.prototype.superFileEditorScreen

/**
 * Contains getters to cast the _IHyperFileEditorScreen_ interface.
 * @interface xyz.swapee.wc.IHyperFileEditorScreenCaster
 */
xyz.swapee.wc.IHyperFileEditorScreenCaster = class { }
/**
 * Cast the _IHyperFileEditorScreen_ instance into the _BoundIHyperFileEditorScreen_ type.
 * @type {!xyz.swapee.wc.BoundIHyperFileEditorScreen}
 */
xyz.swapee.wc.IHyperFileEditorScreenCaster.prototype.asIHyperFileEditorScreen
/**
 * Access the _HyperFileEditorScreen_ prototype.
 * @type {!xyz.swapee.wc.BoundHyperFileEditorScreen}
 */
xyz.swapee.wc.IHyperFileEditorScreenCaster.prototype.superHyperFileEditorScreen

/**
 * Contains getters to cast the _IFileEditorScreenAspects_ interface.
 * @interface xyz.swapee.wc.IFileEditorScreenAspectsCaster
 */
xyz.swapee.wc.IFileEditorScreenAspectsCaster = class { }
/**
 * Cast the _IFileEditorScreenAspects_ instance into the _BoundIFileEditorScreenAspects_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorScreenAspects}
 */
xyz.swapee.wc.IFileEditorScreenAspectsCaster.prototype.asIFileEditorScreenAspects
/**
 * Cast the _IFileEditorScreenAspects_ instance into the _BoundIFileEditorScreen_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorScreen}
 */
xyz.swapee.wc.IFileEditorScreenAspectsCaster.prototype.asIFileEditorScreen
/**
 * Access the _FileEditorScreenAspects_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorScreenAspects}
 */
xyz.swapee.wc.IFileEditorScreenAspectsCaster.prototype.superFileEditorScreenAspects

/**
 * @typedef {(this: THIS, message: string, data: *) => ?} xyz.swapee.wc.IFileEditorScreen.__confirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreen.__confirm<!xyz.swapee.wc.IFileEditorScreen>} xyz.swapee.wc.IFileEditorScreen._confirm */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreen.confirm} */
/**
 * Asks the user to confirm the choice.
 * @param {string} message
 * @param {*} data The props to set when confirmed.
 * @return {?}
 */
xyz.swapee.wc.IFileEditorScreen.confirm = function(message, data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IFileEditorScreenJoinpointModel.BeforeSyncConfirmPointcutData) => void} xyz.swapee.wc.IFileEditorScreenJoinpointModel.__beforeSyncConfirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.__beforeSyncConfirm<!xyz.swapee.wc.IFileEditorScreenJoinpointModel>} xyz.swapee.wc.IFileEditorScreenJoinpointModel._beforeSyncConfirm */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenJoinpointModel.beforeSyncConfirm} */
/**
 * Before the method executes. Allows to cancel the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.BeforeSyncConfirmPointcutData} [data] Metadata passed to the pointcuts of _syncConfirm_ at the `before` joinpoint.
 * - `cond` _(arg0: !Object&lt;string, &#42;&gt;) =&gt; void_ Conditions the object to the process.
 * - `setArgs` _(args: IFileEditorScreen.SyncConfirmNArgs) =&gt; void_ Allows to set or modify arguments passed to the method and futher aspects.
 * - `cancel` _(reason?: string) =&gt; void_ Allows to stop the `syncConfirm` method from being executed.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `args` _IFileEditorScreen.SyncConfirmNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.beforeSyncConfirm = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterSyncConfirmPointcutData) => void} xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirm<!xyz.swapee.wc.IFileEditorScreenJoinpointModel>} xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirm */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirm} */
/**
 * After the method. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterSyncConfirmPointcutData} [data] Metadata passed to the pointcuts of _syncConfirm_ at the `after` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `args` _IFileEditorScreen.SyncConfirmNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirm = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterThrowsSyncConfirmPointcutData) => void} xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmThrows
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmThrows<!xyz.swapee.wc.IFileEditorScreenJoinpointModel>} xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmThrows */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmThrows} */
/**
 * After the method throw. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterThrowsSyncConfirmPointcutData} [data] Metadata passed to the pointcuts of _syncConfirm_ at the `afterThrows` joinpoint.
 * - `err` _!Error_ The error with which the method failed.
 * - `hide` _() =&gt; void_ Hides the error in the `syncConfirm` (use only at the top-level of program flow).
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `args` _IFileEditorScreen.SyncConfirmNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmThrows = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterReturnsSyncConfirmPointcutData) => void} xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmReturns
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmReturns<!xyz.swapee.wc.IFileEditorScreenJoinpointModel>} xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmReturns */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmReturns} */
/**
 * After the method returned. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterReturnsSyncConfirmPointcutData} [data] Metadata passed to the pointcuts of _syncConfirm_ at the `afterReturns` joinpoint.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `args` _IFileEditorScreen.SyncConfirmNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmReturns = function(data) {}

/**
 * @typedef {(this: THIS, data?: !xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterCancelsSyncConfirmPointcutData) => void} xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmCancels
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreenJoinpointModel.__afterSyncConfirmCancels<!xyz.swapee.wc.IFileEditorScreenJoinpointModel>} xyz.swapee.wc.IFileEditorScreenJoinpointModel._afterSyncConfirmCancels */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmCancels} */
/**
 * After the method cancels. `🔗 $combine`
 * @param {!xyz.swapee.wc.IFileEditorScreenJoinpointModel.AfterCancelsSyncConfirmPointcutData} [data] Metadata passed to the pointcuts of _syncConfirm_ at the `afterCancels` joinpoint.
 * - `reasons` _!Set&lt;string&gt;_ The list of reasons why the method was cancelled.
 * - `ticket` _symbol_ A symbol unique to the particular execution. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `args` _IFileEditorScreen.SyncConfirmNArgs_ The arguments passed to the method converted to a lambda-n record. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * - `proc` _!Function_ Allows to work with the process. ⤴ *IFileEditorScreenJoinpointModel.SyncConfirmPointcutData*
 * @return {void}
 */
xyz.swapee.wc.IFileEditorScreenJoinpointModel.afterSyncConfirmCancels = function(data) {}

/**
 * @typedef {(this: THIS, message: string, data: *) => ?} xyz.swapee.wc.IFileEditorScreen.__syncConfirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.IFileEditorScreen.__syncConfirm<!xyz.swapee.wc.IFileEditorScreen>} xyz.swapee.wc.IFileEditorScreen._syncConfirm */
/** @typedef {typeof xyz.swapee.wc.IFileEditorScreen.syncConfirm} */
/**
 * Asks the user to confirm the choice.
 * @param {string} message
 * @param {*} data The props to set when confirmed.
 * @return {?}
 */
xyz.swapee.wc.IFileEditorScreen.syncConfirm = function(message, data) {}

// nss:xyz.swapee.wc,xyz.swapee.wc.IFileEditorScreen,xyz.swapee.wc.IFileEditorScreenJoinpointModel
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/70-IFileEditorScreenBack.xml}  a77e9b9e313f31da60a2bccc21191d5c */
/** @typedef {xyz.swapee.wc.back.IFileEditorScreenAT.Initialese} xyz.swapee.wc.back.IFileEditorScreen.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileEditorScreen)} xyz.swapee.wc.back.AbstractFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileEditorScreen} xyz.swapee.wc.back.FileEditorScreen.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileEditorScreen` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileEditorScreen
 */
xyz.swapee.wc.back.AbstractFileEditorScreen = class extends /** @type {xyz.swapee.wc.back.AbstractFileEditorScreen.constructor&xyz.swapee.wc.back.FileEditorScreen.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileEditorScreen.prototype.constructor = xyz.swapee.wc.back.AbstractFileEditorScreen
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreen} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreen}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileEditorScreen}
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreen}
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreen}
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreen|typeof xyz.swapee.wc.back.FileEditorScreen)|(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreen}
 */
xyz.swapee.wc.back.AbstractFileEditorScreen.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileEditorScreen.Initialese[]) => xyz.swapee.wc.back.IFileEditorScreen} xyz.swapee.wc.back.FileEditorScreenConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileEditorScreenCaster&xyz.swapee.wc.back.IFileEditorScreenAT)} xyz.swapee.wc.back.IFileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorScreenAT} xyz.swapee.wc.back.IFileEditorScreenAT.typeof */
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @interface xyz.swapee.wc.back.IFileEditorScreen
 */
xyz.swapee.wc.back.IFileEditorScreen = class extends /** @type {xyz.swapee.wc.back.IFileEditorScreen.constructor&engineering.type.IEngineer.typeof&xyz.swapee.wc.back.IFileEditorScreenAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileEditorScreen.prototype.constructor = function(...init) {}
/** @type {xyz.swapee.wc.back.IFileEditorScreen.syncConfirm} */
xyz.swapee.wc.back.IFileEditorScreen.prototype.syncConfirm = function() {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorScreen&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorScreen.Initialese>)} xyz.swapee.wc.back.FileEditorScreen.constructor */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorScreen} xyz.swapee.wc.back.IFileEditorScreen.typeof */
/**
 * A concrete class of _IFileEditorScreen_ instances.
 * @constructor xyz.swapee.wc.back.FileEditorScreen
 * @implements {xyz.swapee.wc.back.IFileEditorScreen} The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorScreen.Initialese>} ‎
 */
xyz.swapee.wc.back.FileEditorScreen = class extends /** @type {xyz.swapee.wc.back.FileEditorScreen.constructor&xyz.swapee.wc.back.IFileEditorScreen.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileEditorScreen.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreen* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorScreen.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileEditorScreen.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileEditorScreen}
 */
xyz.swapee.wc.back.FileEditorScreen.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileEditorScreen} */
xyz.swapee.wc.back.RecordIFileEditorScreen

/** @typedef {xyz.swapee.wc.back.IFileEditorScreen} xyz.swapee.wc.back.BoundIFileEditorScreen */

/** @typedef {xyz.swapee.wc.back.FileEditorScreen} xyz.swapee.wc.back.BoundFileEditorScreen */

/**
 * Contains getters to cast the _IFileEditorScreen_ interface.
 * @interface xyz.swapee.wc.back.IFileEditorScreenCaster
 */
xyz.swapee.wc.back.IFileEditorScreenCaster = class { }
/**
 * Cast the _IFileEditorScreen_ instance into the _BoundIFileEditorScreen_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileEditorScreen}
 */
xyz.swapee.wc.back.IFileEditorScreenCaster.prototype.asIFileEditorScreen
/**
 * Access the _FileEditorScreen_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileEditorScreen}
 */
xyz.swapee.wc.back.IFileEditorScreenCaster.prototype.superFileEditorScreen

/**
 * @typedef {(this: THIS, message: string, data: *) => ?} xyz.swapee.wc.back.IFileEditorScreen.__confirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IFileEditorScreen.__confirm<!xyz.swapee.wc.back.IFileEditorScreen>} xyz.swapee.wc.back.IFileEditorScreen._confirm */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorScreen.confirm} */
/**
 * Asks the user to confirm the choice.
 * @param {string} message
 * @param {*} data The props to set when confirmed.
 * @return {?}
 */
xyz.swapee.wc.back.IFileEditorScreen.confirm = function(message, data) {}

/**
 * @typedef {(this: THIS, message: string, data: *) => ?} xyz.swapee.wc.back.IFileEditorScreen.__syncConfirm
 * @template THIS
 */
/** @typedef {xyz.swapee.wc.back.IFileEditorScreen.__syncConfirm<!xyz.swapee.wc.back.IFileEditorScreen>} xyz.swapee.wc.back.IFileEditorScreen._syncConfirm */
/** @typedef {typeof xyz.swapee.wc.back.IFileEditorScreen.syncConfirm} */
/**
 * Asks the user to confirm the choice.
 * @param {string} message
 * @param {*} data The props to set when confirmed.
 * @return {?}
 */
xyz.swapee.wc.back.IFileEditorScreen.syncConfirm = function(message, data) {}

// nss:xyz.swapee.wc.back,xyz.swapee.wc.back.IFileEditorScreen
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/73-IFileEditorScreenAR.xml}  55cdd489ea8ce8a6a97a964ff755403b */
/** @typedef {com.webcircuits.IAR.Initialese&xyz.swapee.wc.IFileEditorScreen.Initialese} xyz.swapee.wc.front.IFileEditorScreenAR.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.front.FileEditorScreenAR)} xyz.swapee.wc.front.AbstractFileEditorScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.FileEditorScreenAR} xyz.swapee.wc.front.FileEditorScreenAR.typeof */
/**
 * An abstract class of `xyz.swapee.wc.front.IFileEditorScreenAR` interface.
 * @constructor xyz.swapee.wc.front.AbstractFileEditorScreenAR
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR = class extends /** @type {xyz.swapee.wc.front.AbstractFileEditorScreenAR.constructor&xyz.swapee.wc.front.FileEditorScreenAR.typeof} */ (class {}) { }
xyz.swapee.wc.front.AbstractFileEditorScreenAR.prototype.constructor = xyz.swapee.wc.front.AbstractFileEditorScreenAR
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.class = /** @type {typeof xyz.swapee.wc.front.AbstractFileEditorScreenAR} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.front.IFileEditorScreenAR|typeof xyz.swapee.wc.front.FileEditorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorScreenAR}
 * @nosideeffects
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.front.AbstractFileEditorScreenAR}
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorScreenAR}
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.front.IFileEditorScreenAR|typeof xyz.swapee.wc.front.FileEditorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorScreenAR}
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.front.IFileEditorScreenAR|typeof xyz.swapee.wc.front.FileEditorScreenAR)|(!com.webcircuits.IAR|typeof com.webcircuits.AR)|(!xyz.swapee.wc.IFileEditorScreen|typeof xyz.swapee.wc.FileEditorScreen)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.front.FileEditorScreenAR}
 */
xyz.swapee.wc.front.AbstractFileEditorScreenAR.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.front.IFileEditorScreenAR.Initialese[]) => xyz.swapee.wc.front.IFileEditorScreenAR} xyz.swapee.wc.front.FileEditorScreenARConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.front.IFileEditorScreenARCaster&com.webcircuits.IAR&xyz.swapee.wc.IFileEditorScreen)} xyz.swapee.wc.front.IFileEditorScreenAR.constructor */
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IFileEditorScreenAT_ to send data.
 * @interface xyz.swapee.wc.front.IFileEditorScreenAR
 */
xyz.swapee.wc.front.IFileEditorScreenAR = class extends /** @type {xyz.swapee.wc.front.IFileEditorScreenAR.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAR.typeof&xyz.swapee.wc.IFileEditorScreen.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.IFileEditorScreenAR.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.front.IFileEditorScreenAR&engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorScreenAR.Initialese>)} xyz.swapee.wc.front.FileEditorScreenAR.constructor */
/** @typedef {typeof xyz.swapee.wc.front.IFileEditorScreenAR} xyz.swapee.wc.front.IFileEditorScreenAR.typeof */
/**
 * A concrete class of _IFileEditorScreenAR_ instances.
 * @constructor xyz.swapee.wc.front.FileEditorScreenAR
 * @implements {xyz.swapee.wc.front.IFileEditorScreenAR} Asynchronously receives commands to the screen from a remote system that
 * used _IFileEditorScreenAT_ to send data.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.front.IFileEditorScreenAR.Initialese>} ‎
 */
xyz.swapee.wc.front.FileEditorScreenAR = class extends /** @type {xyz.swapee.wc.front.FileEditorScreenAR.constructor&xyz.swapee.wc.front.IFileEditorScreenAR.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreenAR* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.front.IFileEditorScreenAR.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreenAR* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.front.IFileEditorScreenAR.Initialese} init The initialisation options.
 */
xyz.swapee.wc.front.FileEditorScreenAR.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.front.FileEditorScreenAR}
 */
xyz.swapee.wc.front.FileEditorScreenAR.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.front.IFileEditorScreenAR} */
xyz.swapee.wc.front.RecordIFileEditorScreenAR

/** @typedef {xyz.swapee.wc.front.IFileEditorScreenAR} xyz.swapee.wc.front.BoundIFileEditorScreenAR */

/** @typedef {xyz.swapee.wc.front.FileEditorScreenAR} xyz.swapee.wc.front.BoundFileEditorScreenAR */

/**
 * Contains getters to cast the _IFileEditorScreenAR_ interface.
 * @interface xyz.swapee.wc.front.IFileEditorScreenARCaster
 */
xyz.swapee.wc.front.IFileEditorScreenARCaster = class { }
/**
 * Cast the _IFileEditorScreenAR_ instance into the _BoundIFileEditorScreenAR_ type.
 * @type {!xyz.swapee.wc.front.BoundIFileEditorScreenAR}
 */
xyz.swapee.wc.front.IFileEditorScreenARCaster.prototype.asIFileEditorScreenAR
/**
 * Access the _FileEditorScreenAR_ prototype.
 * @type {!xyz.swapee.wc.front.BoundFileEditorScreenAR}
 */
xyz.swapee.wc.front.IFileEditorScreenARCaster.prototype.superFileEditorScreenAR

// nss:xyz.swapee.wc.front
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/74-IFileEditorScreenAT.xml}  e7a99df4cbfd592b8ff179d1783e550f */
/** @typedef {com.webcircuits.IAT.Initialese} xyz.swapee.wc.back.IFileEditorScreenAT.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.back.FileEditorScreenAT)} xyz.swapee.wc.back.AbstractFileEditorScreenAT.constructor */
/** @typedef {typeof xyz.swapee.wc.back.FileEditorScreenAT} xyz.swapee.wc.back.FileEditorScreenAT.typeof */
/**
 * An abstract class of `xyz.swapee.wc.back.IFileEditorScreenAT` interface.
 * @constructor xyz.swapee.wc.back.AbstractFileEditorScreenAT
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT = class extends /** @type {xyz.swapee.wc.back.AbstractFileEditorScreenAT.constructor&xyz.swapee.wc.back.FileEditorScreenAT.typeof} */ (class {}) { }
xyz.swapee.wc.back.AbstractFileEditorScreenAT.prototype.constructor = xyz.swapee.wc.back.AbstractFileEditorScreenAT
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.class = /** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreenAT} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreenAT}
 * @nosideeffects
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.back.AbstractFileEditorScreenAT}
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreenAT}
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreenAT}
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.back.IFileEditorScreenAT|typeof xyz.swapee.wc.back.FileEditorScreenAT)|(!com.webcircuits.IAT|typeof com.webcircuits.AT)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.back.FileEditorScreenAT}
 */
xyz.swapee.wc.back.AbstractFileEditorScreenAT.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.back.IFileEditorScreenAT.Initialese[]) => xyz.swapee.wc.back.IFileEditorScreenAT} xyz.swapee.wc.back.FileEditorScreenATConstructor */

/** @typedef {function(new: engineering.type.IEngineer&xyz.swapee.wc.back.IFileEditorScreenATCaster&com.webcircuits.IAT)} xyz.swapee.wc.back.IFileEditorScreenAT.constructor */
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileEditorScreenAR_ trait.
 * @interface xyz.swapee.wc.back.IFileEditorScreenAT
 */
xyz.swapee.wc.back.IFileEditorScreenAT = class extends /** @type {xyz.swapee.wc.back.IFileEditorScreenAT.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IAT.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.IFileEditorScreenAT.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.back.IFileEditorScreenAT&engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorScreenAT.Initialese>)} xyz.swapee.wc.back.FileEditorScreenAT.constructor */
/**
 * A concrete class of _IFileEditorScreenAT_ instances.
 * @constructor xyz.swapee.wc.back.FileEditorScreenAT
 * @implements {xyz.swapee.wc.back.IFileEditorScreenAT} A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileEditorScreenAR_ trait.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.back.IFileEditorScreenAT.Initialese>} ‎
 */
xyz.swapee.wc.back.FileEditorScreenAT = class extends /** @type {xyz.swapee.wc.back.FileEditorScreenAT.constructor&xyz.swapee.wc.back.IFileEditorScreenAT.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorScreenAT* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.back.IFileEditorScreenAT.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorScreenAT* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.back.IFileEditorScreenAT.Initialese} init The initialisation options.
 */
xyz.swapee.wc.back.FileEditorScreenAT.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.back.FileEditorScreenAT}
 */
xyz.swapee.wc.back.FileEditorScreenAT.__extend = function(...Extensions) {}

/** @typedef {xyz.swapee.wc.back.IFileEditorScreenAT} */
xyz.swapee.wc.back.RecordIFileEditorScreenAT

/** @typedef {xyz.swapee.wc.back.IFileEditorScreenAT} xyz.swapee.wc.back.BoundIFileEditorScreenAT */

/** @typedef {xyz.swapee.wc.back.FileEditorScreenAT} xyz.swapee.wc.back.BoundFileEditorScreenAT */

/**
 * Contains getters to cast the _IFileEditorScreenAT_ interface.
 * @interface xyz.swapee.wc.back.IFileEditorScreenATCaster
 */
xyz.swapee.wc.back.IFileEditorScreenATCaster = class { }
/**
 * Cast the _IFileEditorScreenAT_ instance into the _BoundIFileEditorScreenAT_ type.
 * @type {!xyz.swapee.wc.back.BoundIFileEditorScreenAT}
 */
xyz.swapee.wc.back.IFileEditorScreenATCaster.prototype.asIFileEditorScreenAT
/**
 * Access the _FileEditorScreenAT_ prototype.
 * @type {!xyz.swapee.wc.back.BoundFileEditorScreenAT}
 */
xyz.swapee.wc.back.IFileEditorScreenATCaster.prototype.superFileEditorScreenAT

// nss:xyz.swapee.wc.back
/* @typal-end */
/* @typal-start {/Volumes/Job/clients/swapee.xyz/generated/maurice/circuits/file-editor/FileEditor.mvc/design/80-IFileEditorGPU.xml}  c88ba726d320e23b2553b67f4b1f92fc */
/** @typedef {xyz.swapee.wc.back.IFileEditorDisplay.Initialese} xyz.swapee.wc.IFileEditorGPU.Initialese A record with object's initial values, dependencies and other configuration. */

/** @typedef {function(new: xyz.swapee.wc.FileEditorGPU)} xyz.swapee.wc.AbstractFileEditorGPU.constructor */
/** @typedef {typeof xyz.swapee.wc.FileEditorGPU} xyz.swapee.wc.FileEditorGPU.typeof */
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorGPU` interface.
 * @constructor xyz.swapee.wc.AbstractFileEditorGPU
 */
xyz.swapee.wc.AbstractFileEditorGPU = class extends /** @type {xyz.swapee.wc.AbstractFileEditorGPU.constructor&xyz.swapee.wc.FileEditorGPU.typeof} */ (class {}) { }
xyz.swapee.wc.AbstractFileEditorGPU.prototype.constructor = xyz.swapee.wc.AbstractFileEditorGPU
/**
 * Assign to `class.prototype` to enable type-checking inside protypes' methods.
 */
xyz.swapee.wc.AbstractFileEditorGPU.class = /** @type {typeof xyz.swapee.wc.AbstractFileEditorGPU} */ (void 0)
/**
 * Copy methods from passed implementations onto the abstract class.
 * @param {...(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorGPU}
 * @nosideeffects
 */
xyz.swapee.wc.AbstractFileEditorGPU.implements = function(...Implementations) {}
/**
 * Create an identical abstract class but with a separate prototype chain.
 * @param {{ aspectsInstaller?: !Function, aspectsInstallers?: !Array }} [data] The data needed to clone the method.
 * @return {typeof xyz.swapee.wc.AbstractFileEditorGPU}
 */
xyz.swapee.wc.AbstractFileEditorGPU.clone = function(data) {}
/**
 * Create a new class from the abstract one which becomes fully independent
 * from its abstract prototype (changes to the abstract's prototype in future
 * won't affect the new class returned by this static method).
 *
 * Allows to branch off a new class from the abstract one, while adding the
 * supplied implementations to the fresh prototype.
 * @param {...*} Extensions The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorGPU}
 */
xyz.swapee.wc.AbstractFileEditorGPU.__extend = function(...Extensions) {}
/**
 * Forks the abstract class to create an independent implementation of it, so
 * that other classes that continue the abstract class don't inherit the
 * methods implemented by other forks.
 * @param {...(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)} Implementations The extensions in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorGPU}
 */
xyz.swapee.wc.AbstractFileEditorGPU.continues = function(...Implementations) {}
/**
 * Creates a trait of the type of the class which can be added to the
 * abstract class later on with the `__implement` or `__extend` static
 * methods.
 * @param {...(!xyz.swapee.wc.IFileEditorGPU|typeof xyz.swapee.wc.FileEditorGPU)|(!com.webcircuits.IBrowserView|typeof com.webcircuits.BrowserView)|(!xyz.swapee.wc.back.IFileEditorDisplay|typeof xyz.swapee.wc.back.FileEditorDisplay)} Implementations The implementations in form of prototypes or classes to copy methods
 * and/or accessors from.
 * @return {typeof xyz.swapee.wc.FileEditorGPU}
 */
xyz.swapee.wc.AbstractFileEditorGPU.__trait = function(...Implementations) {}

/** @typedef {new (...args: !xyz.swapee.wc.IFileEditorGPU.Initialese[]) => xyz.swapee.wc.IFileEditorGPU} xyz.swapee.wc.FileEditorGPUConstructor */

/** @typedef {function(new: xyz.swapee.wc.IFileEditorGPUFields&engineering.type.IEngineer&xyz.swapee.wc.IFileEditorGPUCaster&com.webcircuits.IBrowserView<.!FileEditorMemory,>&xyz.swapee.wc.back.IFileEditorDisplay)} xyz.swapee.wc.IFileEditorGPU.constructor */
/** @typedef {typeof com.webcircuits.IBrowserView<.!FileEditorMemory,>} com.webcircuits.IBrowserView<.!FileEditorMemory,>.typeof */
/**
 * Handles the periphery of the _IFileEditorDisplay_, including the display and its VDUs.
 * @interface xyz.swapee.wc.IFileEditorGPU
 */
xyz.swapee.wc.IFileEditorGPU = class extends /** @type {xyz.swapee.wc.IFileEditorGPU.constructor&engineering.type.IEngineer.typeof&com.webcircuits.IBrowserView<.!FileEditorMemory,>.typeof&xyz.swapee.wc.back.IFileEditorDisplay.typeof} */ (class {}) {
}
/**
 * Create a new *IFileEditorGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.IFileEditorGPU.prototype.constructor = function(...init) {}

/** @typedef {function(new: xyz.swapee.wc.IFileEditorGPU&engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorGPU.Initialese>)} xyz.swapee.wc.FileEditorGPU.constructor */
/**
 * A concrete class of _IFileEditorGPU_ instances.
 * @constructor xyz.swapee.wc.FileEditorGPU
 * @implements {xyz.swapee.wc.IFileEditorGPU} Handles the periphery of the _IFileEditorDisplay_, including the display and its VDUs.
 * @implements {engineering.type.IInitialiser<!xyz.swapee.wc.IFileEditorGPU.Initialese>} ‎
 */
xyz.swapee.wc.FileEditorGPU = class extends /** @type {xyz.swapee.wc.FileEditorGPU.constructor&xyz.swapee.wc.IFileEditorGPU.typeof&engineering.type.IInitialiser.typeof} */ (class {}) {
  /**
   * Create a new *IFileEditorGPU* instance and automatically initialise it with options.
   * @param {...!xyz.swapee.wc.IFileEditorGPU.Initialese} init The initialisation options.
   */
  constructor(...init) {
    super(...init)
  }
}
/**
 * Create a new *IFileEditorGPU* instance and automatically initialise it with options.
 * @param {...!xyz.swapee.wc.IFileEditorGPU.Initialese} init The initialisation options.
 */
xyz.swapee.wc.FileEditorGPU.prototype.constructor = function(...init) {}
/**
 * Produces a new class from the abstract one and adds supplied implementations.
 * @param {...*} Extensions
 * @return {typeof xyz.swapee.wc.FileEditorGPU}
 */
xyz.swapee.wc.FileEditorGPU.__extend = function(...Extensions) {}

/**
 * Fields of the IFileEditorGPU.
 * @interface xyz.swapee.wc.IFileEditorGPUFields
 */
xyz.swapee.wc.IFileEditorGPUFields = class { }
/**
 * The hashmap of encoded vdu names against design names. Can be used to
 * resolve data-ids of vdus.
 */
xyz.swapee.wc.IFileEditorGPUFields.prototype.vdusPQs = /** @type {!Object<string, string>} */ (void 0)

/** @typedef {xyz.swapee.wc.IFileEditorGPU} */
xyz.swapee.wc.RecordIFileEditorGPU

/** @typedef {xyz.swapee.wc.IFileEditorGPU} xyz.swapee.wc.BoundIFileEditorGPU */

/** @typedef {xyz.swapee.wc.FileEditorGPU} xyz.swapee.wc.BoundFileEditorGPU */

/**
 * Contains getters to cast the _IFileEditorGPU_ interface.
 * @interface xyz.swapee.wc.IFileEditorGPUCaster
 */
xyz.swapee.wc.IFileEditorGPUCaster = class { }
/**
 * Cast the _IFileEditorGPU_ instance into the _BoundIFileEditorGPU_ type.
 * @type {!xyz.swapee.wc.BoundIFileEditorGPU}
 */
xyz.swapee.wc.IFileEditorGPUCaster.prototype.asIFileEditorGPU
/**
 * Access the _FileEditorGPU_ prototype.
 * @type {!xyz.swapee.wc.BoundFileEditorGPU}
 */
xyz.swapee.wc.IFileEditorGPUCaster.prototype.superFileEditorGPU

// nss:xyz.swapee.wc
/* @typal-end */