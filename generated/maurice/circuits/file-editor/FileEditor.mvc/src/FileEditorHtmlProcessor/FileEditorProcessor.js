import FileEditorSharedProcessor from '../FileEditorSharedProcessor'
import AbstractFileEditorProcessor from '../../gen/AbstractFileEditorProcessor'

/** @extends {xyz.swapee.wc.FileEditorProcessor} */
export default class extends AbstractFileEditorProcessor.implements(
 FileEditorSharedProcessor,
){}