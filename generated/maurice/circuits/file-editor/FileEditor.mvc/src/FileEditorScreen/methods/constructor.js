import {scheduleLast} from '@type.engineering/type-engineer'

/** @type {xyz.swapee.wc.IFileEditorScreen._constructor} */
export default function constructor() {
 scheduleLast(this,()=>{
  const{
   asIFileEditorDisplay:{Editor:Editor},asIFileEditorScreen:{setInputs},
  }=/**@type {!xyz.swapee.wc.IFileEditorScreen}*/(this)

  const load=()=>{
   const editor=this.monacoEditor['create'](Editor,{
    minimap:{enabled:false},
    lineNumbers: "on",
   })
   this.editor=editor
   // window['monaco']['editor']
   const model=editor['getModel']()
   this.model=model
   // debugger
   editor['onDidChangeModelContent']((data)=>{
    // console.log('model change',data)
    setInputs({hasChanges:true})
   })
   // =(data)=>{
   //
   //  setInputs({hasChanges:true})
   // }
  }

  if(!('monaco' in window)) {
   // ok then
   window.addEventListener('load',load)
  }else{
   load()
  }
 })
 // this.editor=window['_editor']
 // var model = monacoeditor.getModel(); // we'll create a model for you if the editor created from string value.
 // monaco.editor.setModelLanguage(model, "javascript")
}