/** @type {xyz.swapee.wc.IFileEditorScreen._syncConfirm} */
export default function syncConfirm(message,data) {
 if(!message) return

 const c=confirm(message)
 if(!c) return
 this.setInputs(data)
}