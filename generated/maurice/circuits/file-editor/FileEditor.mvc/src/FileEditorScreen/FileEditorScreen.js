import syncConfirm from './methods/sync-confirm'
import constructor from './methods/constructor'
import AbstractFileEditorScreen from '../../gen/AbstractFileEditorScreen'
import AbstractFileEditorControllerAT from '../../gen/AbstractFileEditorControllerAT'
import FileEditorDisplay from '../FileEditorDisplay'
import AbstractHyperFileEditorScreen from '../../gen/AbstractHyperFileEditorScreen'

/** @extends {xyz.swapee.wc.HyperFileEditorScreen} */
export default class extends AbstractHyperFileEditorScreen.implements(
 AbstractFileEditorScreen,
 AbstractFileEditorControllerAT,
 FileEditorDisplay,
 /**@type {!xyz.swapee.wc.IFileEditorScreen}*/({get queries(){return this.settings}}),
 /** @type {!xyz.swapee.wc.IFileEditorScreen} */ ({
  syncConfirm:syncConfirm,
  constructor:constructor,
  __$id:9961973136,
 }),
/**@type {!xyz.swapee.wc.IFileEditorScreen}*/({
   get monacoEditor() {
    return window['monaco']['editor']
   },
   // deduceInputs(el) {},
  }),
){}