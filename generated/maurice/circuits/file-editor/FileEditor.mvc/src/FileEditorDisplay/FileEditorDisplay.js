import paintFile from './methods/paint-file'
import prepaintFile from '../../../parts/40-display/src/methods/prepaint-file'
import AbstractFileEditorDisplay from '../../gen/AbstractFileEditorDisplay'

/** @extends {xyz.swapee.wc.FileEditorDisplay} */
export default class extends AbstractFileEditorDisplay.implements(
 /**@type {!xyz.swapee.wc.IFileEditorDisplay}*/({
  paint:[
   prepaintFile,
  ],
 }),
 /** @type {!xyz.swapee.wc.IFileEditorDisplay} */ ({
  paintFile:paintFile,
 }),
){}