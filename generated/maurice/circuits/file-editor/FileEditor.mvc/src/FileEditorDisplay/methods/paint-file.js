/** @type {xyz.swapee.wc.IFileEditorDisplay._paintFile} */
export default function paintFile({content:content,lang:lang}) {
 const{asIFileEditorScreen:{editor:editor,model,monacoEditor}}=/**@type {!xyz.swapee.wc.IFileEditorDisplay}*/(this)
 editor.setValue(content)
 monacoEditor['setModelLanguage'](model,lang)
}