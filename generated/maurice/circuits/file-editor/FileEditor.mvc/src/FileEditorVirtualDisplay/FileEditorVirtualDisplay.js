import {Painter} from '@webcircuits/webcircuits'

const FileEditorVirtualDisplay=Painter.__trait(
 /**@type {!xyz.swapee.wc.IFileEditorVirtualDisplay}*/({
  paint:[
   function paintFile({content:content,lang:lang}){
    const{
     asIGraphicsDriverBack:{serMemory:serMemory,t_pa},
    }=this
    if(!content) return
    const _mem=serMemory({content:content,lang:lang})
    t_pa({
     pid:'9b82d71',
     mem:_mem,
    })
   },
  ],
 }),
)
export default FileEditorVirtualDisplay