import solder from './methods/solder'
import server from './methods/server'
import render from './methods/render'
import FileEditorServerController from '../FileEditorServerController'
import FileEditorServerProcessor from '../FileEditorServerProcessor'
import FileEditorService from '../../src/FileEditorService'
import 'headers'
import {Guest,IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {HTMLBlocker} from '@mauriceguest/fin-de-siecle'
import AbstractFileEditorElement from '../../gen/AbstractFileEditorElement'

/** @extends {xyz.swapee.wc.FileEditorElement} */
export default class FileEditorElement extends AbstractFileEditorElement.implements(
 FileEditorServerController,
 FileEditorServerProcessor,
 FileEditorService,
 Guest,
 HTMLBlocker,
 IntegratedComponentInitialiser,
 /** @type {!xyz.swapee.wc.IFileEditorElement} */ ({
  solder:solder,
  server:server,
  render:render,
 }),
/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
   classesMap: true,
   rootSelector:     `.FileEditor`,
   stylesheet:       'html/styles/FileEditor.css',
   blockName:        'html/FileEditorBlock.html',
  }),
){}

// thank you for using web circuits
