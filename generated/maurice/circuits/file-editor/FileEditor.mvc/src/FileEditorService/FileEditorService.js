import AbstractFileEditorService from '../../gen/AbstractFileEditorService'
import filterReadFile from './methods/filterReadFile/filter-read-file'

/**@extends {xyz.swapee.wc.FileEditorService} */
export default class extends AbstractFileEditorService.implements(
 AbstractFileEditorService.class.prototype=/**@type {!xyz.swapee.wc.FileEditorService}*/({
  filterReadFile:filterReadFile,
 }),
){}