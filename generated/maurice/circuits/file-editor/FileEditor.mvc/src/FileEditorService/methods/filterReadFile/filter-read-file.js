import '@type.engineering/type-engineer'
import {readFile} from 'fs/promises'

/**@type {xyz.swapee.wc.IFileEditorService._filterReadFile} */
export default async function filterReadFile({path:path}){
 const file=await readFile(path)+''
 return{
  content:file,
 }
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS1lZGl0b3IvZmlsZS1lZGl0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBaU9HLE1BQU0sU0FBUyxjQUFjLEVBQUUsU0FBUyxDQUFDO0NBQ3hDLE1BQU0sSUFBSSxDQUFDLE1BQU0sUUFBUSxDQUFDO0NBQzFCO0VBQ0MsWUFBWTtBQUNkO0FBQ0EsQ0FBRiJ9