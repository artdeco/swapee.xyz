/** @type {xyz.swapee.wc.IFileEditorProcessor._openFile} */
export default function openFile(path,name,lang) {
 // console.log('open file',path,name,lang)
 const{model:{hasChanges:hasChanges},setInputs,syncConfirm}=/**@type {!xyz.swapee.wc.IFileEditorProcessor}*/(this)
 const props={path:path,filename:name,hasChanges:false,lang:lang}
 if(hasChanges) {
  const res=syncConfirm('Changes will be lost, continue?',props)
  if(!res) return
 }else{
  setInputs(props)
 }
 // hello?
 // check if not saved?
}