import openFile from './methods/open-file'
import AbstractFileEditorProcessor from '../../gen/AbstractFileEditorProcessor'

const FileEditorSharedProcessor=AbstractFileEditorProcessor.__trait(
 /** @type {!xyz.swapee.wc.IFileEditorProcessor} */ ({
  openFile:openFile,
 }),
)
export default FileEditorSharedProcessor