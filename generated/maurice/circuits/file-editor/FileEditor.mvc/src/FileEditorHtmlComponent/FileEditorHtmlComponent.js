import FileEditorHtmlController from '../FileEditorHtmlController'
import FileEditorHtmlProcessor from '../FileEditorHtmlProcessor'
import FileEditorVirtualDisplay from '../FileEditorVirtualDisplay'
import {IntegratedComponentInitialiser} from '@mauriceguest/guest2'
import {AbstractFileEditorHtmlComponent} from '../../gen/AbstractFileEditorHtmlComponent'

/** @extends {xyz.swapee.wc.FileEditorHtmlComponent} */
export default class extends AbstractFileEditorHtmlComponent.implements(
 FileEditorHtmlController,
 FileEditorHtmlProcessor,
 FileEditorVirtualDisplay,
 IntegratedComponentInitialiser,
){}