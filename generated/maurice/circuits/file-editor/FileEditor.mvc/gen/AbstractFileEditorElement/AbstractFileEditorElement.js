import FileEditorRenderVdus from './methods/render-vdus'
import FileEditorElementPort from '../FileEditorElementPort'
import {ElementBase} from '@mauriceguest/fin-de-siecle'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {FileEditorInputsPQs} from '../../pqs/FileEditorInputsPQs'
import {FileEditorCachePQs} from '../../pqs/FileEditorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractFileEditor from '../AbstractFileEditor'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorElement}
 */
function __AbstractFileEditorElement() {}
__AbstractFileEditorElement.prototype = /** @type {!_AbstractFileEditorElement} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorElement}
 */
class _AbstractFileEditorElement { }
/**
 * A component description.
 *
 * The _IFileEditor_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.AbstractFileEditorElement} ‎
 */
class AbstractFileEditorElement extends newAbstract(
 _AbstractFileEditorElement,996197313613,null,{
  asIFileEditorElement:1,
  superFileEditorElement:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorElement} */
AbstractFileEditorElement.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorElement} */
function AbstractFileEditorElementClass(){}

export default AbstractFileEditorElement


AbstractFileEditorElement[$implementations]=[
 __AbstractFileEditorElement,
 ElementBase,
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  calibrate:function induceColonInputs({
   ':no-solder':noSolderColAttr,
   ':host':hostColAttr,
   ':content':contentColAttr,
   ':path':pathColAttr,
   ':filename':filenameColAttr,
   ':lang':langColAttr,
   ':has-changes':hasChangesColAttr,
   ':read-file':readFileColAttr,
   ':save-file':saveFileColAttr,
  }){
   const{attributes:{
    'no-solder':noSolderAttr,
    'host':hostAttr,
    'content':contentAttr,
    'path':pathAttr,
    'filename':filenameAttr,
    'lang':langAttr,
    'has-changes':hasChangesAttr,
    'read-file':readFileAttr,
    'save-file':saveFileAttr,
   }}=this
   return{
    ...(noSolderAttr===undefined?{'no-solder':noSolderColAttr}:{}),
    ...(hostAttr===undefined?{'host':hostColAttr}:{}),
    ...(contentAttr===undefined?{'content':contentColAttr}:{}),
    ...(pathAttr===undefined?{'path':pathColAttr}:{}),
    ...(filenameAttr===undefined?{'filename':filenameColAttr}:{}),
    ...(langAttr===undefined?{'lang':langColAttr}:{}),
    ...(hasChangesAttr===undefined?{'has-changes':hasChangesColAttr}:{}),
    ...(readFileAttr===undefined?{'read-file':readFileColAttr}:{}),
    ...(saveFileAttr===undefined?{'save-file':saveFileColAttr}:{}),
   }
  },
 }),
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  calibrate:({
   'no-solder':noSolderAttr,
   'host':hostAttr,
   'content':contentAttr,
   'path':pathAttr,
   'filename':filenameAttr,
   'lang':langAttr,
   'has-changes':hasChangesAttr,
   'read-file':readFileAttr,
   'save-file':saveFileAttr,
  })=>{
   return{
    noSolder:noSolderAttr,
    host:hostAttr,
    content:contentAttr,
    path:pathAttr,
    filename:filenameAttr,
    lang:langAttr,
    hasChanges:hasChangesAttr,
    readFile:readFileAttr,
    saveFile:saveFileAttr,
   }
  },
 }),
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  __$constructor(){
   this.vdusOpts=new Map
  },
 }),
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  render:FileEditorRenderVdus,
 }),
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  inputsPQs:FileEditorInputsPQs,
  cachePQs:FileEditorCachePQs,
  vdus:{
   'Editor': 'fc4a1',
  },
 }),
 IntegratedComponent,
 AbstractFileEditorElementClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElement}*/({
  constructor(){
   Object.assign(this,{
    knownInputs:new Set(['noSolder','host','content','path','filename','lang','hasChanges','readFile','saveFile','no-solder',':no-solder',':host',':content',':path',':filename',':lang','has-changes',':has-changes','read-file',':read-file','save-file',':save-file','fe646','4fc44','67b3d','9a036','d6fe1','435ed','75725','93f80','dcc6a','b182d','children']),
   })
  },
  get Port(){
   return FileEditorElementPort
  },
 }),
]



AbstractFileEditorElement[$implementations]=[AbstractFileEditor,
 /** @type {!AbstractFileEditorElement} */ ({
  rootId:'FileEditor',
  __$id:9961973136,
  fqn:'xyz.swapee.wc.IFileEditor',
  maurice_element_v3:true,
 }),
]