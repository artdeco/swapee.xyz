
import AbstractFileEditor from '../AbstractFileEditor'

/** @abstract {xyz.swapee.wc.IFileEditorElement} */
export default class AbstractFileEditorElement { }



AbstractFileEditorElement[$implementations]=[AbstractFileEditor,
 /** @type {!AbstractFileEditorElement} */ ({
  rootId:'FileEditor',
  __$id:9961973136,
  fqn:'xyz.swapee.wc.IFileEditor',
  maurice_element_v3:true,
 }),
]