import { $advice, newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorScreenAspectsInstaller}
 */
function __FileEditorScreenAspectsInstaller() {}
__FileEditorScreenAspectsInstaller.prototype = /** @type {!_FileEditorScreenAspectsInstaller} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller}
 */
class _FileEditorScreenAspectsInstaller { }

_FileEditorScreenAspectsInstaller.prototype[$advice]=__FileEditorScreenAspectsInstaller

/** @extends {xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller} ‎ */
class FileEditorScreenAspectsInstaller extends newAbstract(
 _FileEditorScreenAspectsInstaller,996197313634,null,{},false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller} */
FileEditorScreenAspectsInstaller.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspectsInstaller} */
function FileEditorScreenAspectsInstallerClass(){}

export default FileEditorScreenAspectsInstaller


FileEditorScreenAspectsInstaller[$implementations]=[
 FileEditorScreenAspectsInstallerClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorScreenAspectsInstaller}*/({
  syncConfirm(){
   this.beforeSyncConfirm=1
   this.afterSyncConfirm=2
   this.aroundSyncConfirm=3
   this.afterSyncConfirmThrows=4
   this.afterSyncConfirmReturns=5
   this.afterSyncConfirmCancels=7
   return {
    message:1,
    data:2,
   }
  },
 }),
 __FileEditorScreenAspectsInstaller,
]