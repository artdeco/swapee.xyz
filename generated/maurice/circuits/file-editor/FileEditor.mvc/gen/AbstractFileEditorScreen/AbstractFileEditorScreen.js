import AbstractFileEditorScreenAR from '../AbstractFileEditorScreenAR'
import {CoreCache,Screen} from '@webcircuits/front'
import {FileEditorInputsPQs} from '../../pqs/FileEditorInputsPQs'
import {FileEditorMemoryQPs} from '../../pqs/FileEditorMemoryQPs'
import {FileEditorCacheQPs} from '../../pqs/FileEditorCacheQPs'
import {FileEditorVdusPQs} from '../../pqs/FileEditorVdusPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorScreen}
 */
function __AbstractFileEditorScreen() {}
__AbstractFileEditorScreen.prototype = /** @type {!_AbstractFileEditorScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorScreen}
 */
class _AbstractFileEditorScreen { }
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.AbstractFileEditorScreen} ‎
 */
class AbstractFileEditorScreen extends newAbstract(
 _AbstractFileEditorScreen,996197313627,null,{
  asIFileEditorScreen:1,
  superFileEditorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreen} */
AbstractFileEditorScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreen} */
function AbstractFileEditorScreenClass(){}

export default AbstractFileEditorScreen


AbstractFileEditorScreen[$implementations]=[
 __AbstractFileEditorScreen,
 AbstractFileEditorScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorScreen}*/({
  inputsPQs:FileEditorInputsPQs,
  memoryQPs:FileEditorMemoryQPs,
  cacheQPs:FileEditorCacheQPs,
 }),
 Screen,
 CoreCache,
 AbstractFileEditorScreenAR,
 AbstractFileEditorScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorScreen}*/({
  vdusPQs:FileEditorVdusPQs,
 }),
 AbstractFileEditorScreenClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorScreen}*/({
  deduceInputs(){
   const{asIFileEditorScreen:{syncConfirm:syncConfirm}}=this

   const Confirm=syncConfirm()
   return{...Confirm}
  },
 }),
]