
/**@this {xyz.swapee.wc.IFileEditorComputer}*/
export function preadaptReadFile(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileEditorComputer.adaptReadFile.Form}*/
 const _inputs={
  path:inputs.path,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.path) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptReadFile(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IFileEditorComputer}*/
export function preadaptSaveFile(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileEditorComputer.adaptSaveFile.Form}*/
 const _inputs={
  content:inputs.content,
  path:inputs.path,
  saveFile:inputs.saveFile,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.saveFile) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptSaveFile(__inputs,__changes)
 return RET
}