import {Adapter} from '@webcircuits/webcircuits'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorComputer}
 */
function __AbstractFileEditorComputer() {}
__AbstractFileEditorComputer.prototype = /** @type {!_AbstractFileEditorComputer} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorComputer}
 */
class _AbstractFileEditorComputer { }
/**
 * Handles memory registers across all cores, and contains interrupt service
 * routines for adaption.
 * @extends {xyz.swapee.wc.AbstractFileEditorComputer} ‎
 */
export class AbstractFileEditorComputer extends newAbstract(
 _AbstractFileEditorComputer,99619731361,null,{
  asIFileEditorComputer:1,
  superFileEditorComputer:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorComputer} */
AbstractFileEditorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorComputer} */
function AbstractFileEditorComputerClass(){}


AbstractFileEditorComputer[$implementations]=[
 __AbstractFileEditorComputer,
 Adapter,
]


export default AbstractFileEditorComputer