import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorElementPort}
 */
function __FileEditorElementPort() {}
__FileEditorElementPort.prototype = /** @type {!_FileEditorElementPort} */ ({ })
/** @this {xyz.swapee.wc.FileEditorElementPort} */ function FileEditorElementPortConstructor() {
  /**@type {!xyz.swapee.wc.IFileEditorElementPort.Inputs}*/
  this.inputs={
    noSolder: false,
    editorOpts: {},
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorElementPort}
 */
class _FileEditorElementPort { }
/**
 * The port specific for the server-side element.
 * @extends {xyz.swapee.wc.AbstractFileEditorElementPort} ‎
 */
class FileEditorElementPort extends newAbstract(
 _FileEditorElementPort,996197313614,FileEditorElementPortConstructor,{
  asIFileEditorElementPort:1,
  superFileEditorElementPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorElementPort} */
FileEditorElementPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorElementPort} */
function FileEditorElementPortClass(){}

export default FileEditorElementPort


FileEditorElementPort[$implementations]=[
 __FileEditorElementPort,
 FileEditorElementPortClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorElementPort}*/({
  constructor(){
   Object.assign(this.inputs,{
    'no-solder':undefined,
    'editor-opts':undefined,
    'has-changes':undefined,
    'read-file':undefined,
    'save-file':undefined,
   })
  },
 }),
]