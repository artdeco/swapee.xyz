import AbstractFileEditorGPU from '../AbstractFileEditorGPU'
import AbstractFileEditorScreenBack from '../AbstractFileEditorScreenBack'
import AbstractFileEditorRadio from '../AbstractFileEditorRadio'
import {HtmlComponent,mvc} from '@webcircuits/webcircuits'
import {FileEditorInputsQPs} from '../../pqs/FileEditorInputsQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'
import AbstractFileEditor from '../AbstractFileEditor'
/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorHtmlComponent}
 */
function __AbstractFileEditorHtmlComponent() {}
__AbstractFileEditorHtmlComponent.prototype = /** @type {!_AbstractFileEditorHtmlComponent} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorHtmlComponent}
 */
class _AbstractFileEditorHtmlComponent {
  /** @nocollapse */
  static mvc(el, opts, parent) {
    /** @suppress {checkTypes} */
    const res = mvc(this, el, opts, null, parent)
    return /** @type {!xyz.swapee.wc.FileEditorHtmlComponent} */ (res)
  }
}
/**
 * The _IFileEditor_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.AbstractFileEditorHtmlComponent} ‎
 */
export class AbstractFileEditorHtmlComponent extends newAbstract(
 _AbstractFileEditorHtmlComponent,996197313612,null,{
  asIFileEditorHtmlComponent:1,
  superFileEditorHtmlComponent:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorHtmlComponent} */
AbstractFileEditorHtmlComponent.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorHtmlComponent} */
function AbstractFileEditorHtmlComponentClass(){}


AbstractFileEditorHtmlComponent[$implementations]=[
 __AbstractFileEditorHtmlComponent,
 HtmlComponent,
 AbstractFileEditor,
 AbstractFileEditorGPU,
 AbstractFileEditorScreenBack,
 AbstractFileEditorRadio,
 AbstractFileEditorHtmlComponentClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorHtmlComponent}*/({
  inputsQPs:FileEditorInputsQPs,
 }),
]