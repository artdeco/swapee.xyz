import AbstractFileEditorScreenAT from '../AbstractFileEditorScreenAT'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorScreenBack}
 */
function __AbstractFileEditorScreenBack() {}
__AbstractFileEditorScreenBack.prototype = /** @type {!_AbstractFileEditorScreenBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileEditorScreen}
 */
class _AbstractFileEditorScreenBack { }
/**
 * The backend interface for the screen. Invoking methods on it from the back
 * will send a _uart_ message to the front.
 * @extends {xyz.swapee.wc.back.AbstractFileEditorScreen} ‎
 */
class AbstractFileEditorScreenBack extends newAbstract(
 _AbstractFileEditorScreenBack,996197313628,null,{
  asIFileEditorScreen:1,
  superFileEditorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreen} */
AbstractFileEditorScreenBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreen} */
function AbstractFileEditorScreenBackClass(){}

export default AbstractFileEditorScreenBack


AbstractFileEditorScreenBack[$implementations]=[
 __AbstractFileEditorScreenBack,
 AbstractFileEditorScreenAT,
]