import {AR} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorScreenAR}
 */
function __AbstractFileEditorScreenAR() {}
__AbstractFileEditorScreenAR.prototype = /** @type {!_AbstractFileEditorScreenAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractFileEditorScreenAR}
 */
class _AbstractFileEditorScreenAR { }
/**
 * Asynchronously receives commands to the screen from a remote system that
 * used _IFileEditorScreenAT_ to send data.
 * @extends {xyz.swapee.wc.front.AbstractFileEditorScreenAR} ‎
 */
class AbstractFileEditorScreenAR extends newAbstract(
 _AbstractFileEditorScreenAR,996197313629,null,{
  asIFileEditorScreenAR:1,
  superFileEditorScreenAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractFileEditorScreenAR} */
AbstractFileEditorScreenAR.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractFileEditorScreenAR} */
function AbstractFileEditorScreenARClass(){}

export default AbstractFileEditorScreenAR


AbstractFileEditorScreenAR[$implementations]=[
 __AbstractFileEditorScreenAR,
 AR,
 AbstractFileEditorScreenARClass.prototype=/**@type {!xyz.swapee.wc.front.IFileEditorScreenAR}*/({
  allocator(){
   this.methods={
    syncConfirm:'003b7',
   }
  },
 }),
]
export {AbstractFileEditorScreenAR}