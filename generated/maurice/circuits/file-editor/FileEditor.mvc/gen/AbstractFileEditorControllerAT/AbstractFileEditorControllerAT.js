import {UartUniversal} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorControllerAT}
 */
function __AbstractFileEditorControllerAT() {}
__AbstractFileEditorControllerAT.prototype = /** @type {!_AbstractFileEditorControllerAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.front.AbstractFileEditorControllerAT}
 */
class _AbstractFileEditorControllerAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * controller that installed an _IFileEditorControllerAR_ trait.
 * @extends {xyz.swapee.wc.front.AbstractFileEditorControllerAT} ‎
 */
class AbstractFileEditorControllerAT extends newAbstract(
 _AbstractFileEditorControllerAT,996197313626,null,{
  asIFileEditorControllerAT:1,
  superFileEditorControllerAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.front.AbstractFileEditorControllerAT} */
AbstractFileEditorControllerAT.class=function(){}
/** @type {typeof xyz.swapee.wc.front.AbstractFileEditorControllerAT} */
function AbstractFileEditorControllerATClass(){}

export default AbstractFileEditorControllerAT


AbstractFileEditorControllerAT[$implementations]=[
 __AbstractFileEditorControllerAT,
 UartUniversal,
 AbstractFileEditorControllerATClass.prototype=/**@type {!xyz.swapee.wc.front.IFileEditorControllerAT}*/({
  get asIFileEditorController(){
   return this
  },
  openFile(){
   this.uart.t("inv",{mid:'833e7',args:[...arguments]})
  },
  loadReadFile(){
   this.uart.t("inv",{mid:'2c187'})
  },
  loadSaveFile(){
   this.uart.t("inv",{mid:'244b2'})
  },
  pulseReadFile(){
   this.uart.t("inv",{mid:'5cce1'})
  },
  pulseSaveFile(){
   this.uart.t("inv",{mid:'40d6e'})
  },
 }),
]