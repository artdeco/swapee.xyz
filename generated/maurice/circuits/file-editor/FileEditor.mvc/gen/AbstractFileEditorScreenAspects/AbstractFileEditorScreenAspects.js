import { newAspects, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorScreenAspects}
 */
function __AbstractFileEditorScreenAspects() {}
__AbstractFileEditorScreenAspects.prototype = /** @type {!_AbstractFileEditorScreenAspects} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorScreenAspects}
 */
class _AbstractFileEditorScreenAspects { }
/**
 * The aspects of the screen.
 * @extends {xyz.swapee.wc.AbstractFileEditorScreenAspects} ‎
 */
class AbstractFileEditorScreenAspects extends newAspects(
 _AbstractFileEditorScreenAspects,996197313636,null,{
  asIFileEditorScreenAspects:1,
  superFileEditorScreenAspects:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspects} */
AbstractFileEditorScreenAspects.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorScreenAspects} */
function AbstractFileEditorScreenAspectsClass(){}

export default AbstractFileEditorScreenAspects


AbstractFileEditorScreenAspects[$implementations]=[
 __AbstractFileEditorScreenAspects,
]