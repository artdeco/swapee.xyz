import {Parametric} from '@type.engineering/real-mvc'
import {mountPins} from '@webcircuits/webcircuits'
import {FileEditorInputsPQs} from '../../pqs/FileEditorInputsPQs'
import {FileEditorOuterCoreConstructor} from '../FileEditorCore'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorPort}
 */
function __FileEditorPort() {}
__FileEditorPort.prototype = /** @type {!_FileEditorPort} */ ({ })
/** @this {xyz.swapee.wc.FileEditorPort} */ function FileEditorPortConstructor() {
  const self=/** @type {!xyz.swapee.wc.FileEditorOuterCore} */ ({model:null})
  FileEditorOuterCoreConstructor.call(self)
  /** @suppress {checkTypes} */ this.inputs=self.model
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorPort}
 */
class _FileEditorPort { }
/**
 * The port that serves as an interface to the _IFileEditor_, providing input
 * pins.
 * @extends {xyz.swapee.wc.AbstractFileEditorPort} ‎
 */
export class FileEditorPort extends newAbstract(
 _FileEditorPort,99619731365,FileEditorPortConstructor,{
  asIFileEditorPort:1,
  superFileEditorPort:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorPort} */
FileEditorPort.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorPort} */
function FileEditorPortClass(){}

export const FileEditorPortConnector=/** @type {!engineering.type.mvc.IConnector<!xyz.swapee.wc.IFileEditor.Pinout>}*/({
 get Port() { return FileEditorPort },
})

FileEditorPort[$implementations]=[
 FileEditorPortClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorPort}*/({
  resetPort(){
   this.resetFileEditorPort()
  },
  resetFileEditorPort(){
   FileEditorPortConstructor.call(this)
  },
 }),
 __FileEditorPort,
 Parametric,
 FileEditorPortClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorPort}*/({
  constructor(){
   mountPins(this.inputs,'',FileEditorInputsPQs)
  },
 }),
]


export default FileEditorPort