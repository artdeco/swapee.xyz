import FileEditorScreenAspectsInstaller from '../AbstractFileEditorScreen/aspects-installers/FileEditorScreenAspectsInstaller'
import {SetInputs} from '@webcircuits/front'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractHyperFileEditorScreen}
 */
function __AbstractHyperFileEditorScreen() {}
__AbstractHyperFileEditorScreen.prototype = /** @type {!_AbstractHyperFileEditorScreen} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractHyperFileEditorScreen}
 */
class _AbstractHyperFileEditorScreen {
  /** @suppress {checkTypes} */
  static'consults'(...args) {
   return AbstractHyperFileEditorScreen
    .clone({aspectsInstaller:FileEditorScreenAspectsInstaller})
    .consults(...args)
  }
}
/**
 * The hyper screen with aspects.
 * @extends {xyz.swapee.wc.AbstractHyperFileEditorScreen} ‎
 */
class AbstractHyperFileEditorScreen extends newAbstract(
 _AbstractHyperFileEditorScreen,996197313635,null,{
  asIHyperFileEditorScreen:1,
  superHyperFileEditorScreen:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractHyperFileEditorScreen} */
AbstractHyperFileEditorScreen.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractHyperFileEditorScreen} */
function AbstractHyperFileEditorScreenClass(){}

export default AbstractHyperFileEditorScreen


AbstractHyperFileEditorScreen[$implementations]=[
 __AbstractHyperFileEditorScreen,
]
AbstractHyperFileEditorScreen[$implementations]=[
 FileEditorScreenAspectsInstaller,
 /**@type {!xyz.swapee.wc.IFileEditorScreenAspects}*/(/**@type {!xyz.swapee.wc.IFileEditorScreenAspectsPointcuts} */({
  afterSyncConfirmReturns: SetInputs,
 })),
]