import {makeBuffers} from '@webcircuits/webcircuits'

export const FileEditorBuffer=/**@type {!engineering.type.mvc.IRegulator}*/ ({
 regulate:makeBuffers({
  host:String,
  content:String,
  path:String,
  filename:String,
  lang:String,
  hasChanges:Boolean,
  readFile:Boolean,
  saveFile:Boolean,
 }),
})

export default FileEditorBuffer