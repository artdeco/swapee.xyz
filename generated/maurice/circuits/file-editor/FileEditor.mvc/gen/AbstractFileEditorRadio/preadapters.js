
/**@this {xyz.swapee.wc.IFileEditorRadio}*/
export function preadaptLoadReadFile(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileEditorRadio.adaptLoadReadFile.Form}*/
 const _inputs={
  host:inputs.host,
  path:inputs.path,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.path) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadReadFile(__inputs,__changes)
 return RET
}

/**@this {xyz.swapee.wc.IFileEditorRadio}*/
export function preadaptLoadSaveFile(inputs,changes,mapForm) {
 /**@type {!xyz.swapee.wc.IFileEditorRadio.adaptLoadSaveFile.Form}*/
 const _inputs={
  host:inputs.host,
  content:inputs.content,
  path:inputs.path,
  saveFile:inputs.saveFile,
 }
 const __inputs=mapForm?mapForm(_inputs):_inputs
 if(!__inputs.saveFile) return
 const __changes=mapForm?mapForm(changes):changes

 const RET=this.adaptLoadSaveFile(__inputs,__changes)
 return RET
}