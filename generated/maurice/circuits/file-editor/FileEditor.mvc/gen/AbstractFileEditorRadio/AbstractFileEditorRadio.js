import {preadaptLoadReadFile,preadaptLoadSaveFile} from './preadapters'
import {makeLoaders} from '@webcircuits/webcircuits'
import {StatefulLoader} from '@mauriceguest/guest2'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorRadio}
 */
function __AbstractFileEditorRadio() {}
__AbstractFileEditorRadio.prototype = /** @type {!_AbstractFileEditorRadio} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorRadio}
 */
class _AbstractFileEditorRadio { }
/**
 * Transmits data _to-_ and _fro-_ off-chip system.
 * @extends {xyz.swapee.wc.AbstractFileEditorRadio} ‎
 */
class AbstractFileEditorRadio extends newAbstract(
 _AbstractFileEditorRadio,996197313615,null,{
  asIFileEditorRadio:1,
  superFileEditorRadio:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorRadio} */
AbstractFileEditorRadio.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorRadio} */
function AbstractFileEditorRadioClass(){}

export default AbstractFileEditorRadio


AbstractFileEditorRadio[$implementations]=[
 __AbstractFileEditorRadio,
 makeLoaders(9961973136,{
  adaptLoadReadFile:[
   'e8abe',
   {
    path:'d6fe1',
   },
   {
    content:'9a036',
   },
   {
    loadingReadFile:1,
    loadReadFileError:2,
   },
  ],
  adaptLoadSaveFile:[
   'f0d42',
   {
    content:'9a036',
    path:'d6fe1',
    saveFile:'b182d',
   },
   {
    path:'d6fe1',
   },
   {
    loadingSaveFile:1,
    loadSaveFileError:2,
   },
  ],
 }),
 StatefulLoader,
 {adapt:[preadaptLoadReadFile,preadaptLoadSaveFile]},
]