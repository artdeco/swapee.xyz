import {AR} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorControllerAR}
 */
function __AbstractFileEditorControllerAR() {}
__AbstractFileEditorControllerAR.prototype = /** @type {!_AbstractFileEditorControllerAR} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileEditorControllerAR}
 */
class _AbstractFileEditorControllerAR { }
/**
 * Asynchronously receives commands to the controller from a remote system that
 * used _IFileEditorControllerAT_ to send data.
 * @extends {xyz.swapee.wc.back.AbstractFileEditorControllerAR} ‎
 */
class AbstractFileEditorControllerAR extends newAbstract(
 _AbstractFileEditorControllerAR,996197313625,null,{
  asIFileEditorControllerAR:1,
  superFileEditorControllerAR:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorControllerAR} */
AbstractFileEditorControllerAR.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorControllerAR} */
function AbstractFileEditorControllerARClass(){}

export default AbstractFileEditorControllerAR


AbstractFileEditorControllerAR[$implementations]=[
 __AbstractFileEditorControllerAR,
 AR,
 AbstractFileEditorControllerARClass.prototype=/**@type {!xyz.swapee.wc.back.IFileEditorControllerAR}*/({
  allocator(){
   this.methods={
    openFile:'833e7',
    loadReadFile:'2c187',
    loadSaveFile:'244b2',
    pulseReadFile:'5cce1',
    pulseSaveFile:'40d6e',
   }
  },
 }),
]