import FileEditorBuffer from '../FileEditorBuffer'
import {IntegratedController} from '@type.engineering/real-mvc'
import {FileEditorPortConnector} from '../FileEditorPort'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorController}
 */
function __AbstractFileEditorController() {}
__AbstractFileEditorController.prototype = /** @type {!_AbstractFileEditorController} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorController}
 */
class _AbstractFileEditorController { }
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.AbstractFileEditorController} ‎
 */
export class AbstractFileEditorController extends newAbstract(
 _AbstractFileEditorController,996197313622,null,{
  asIFileEditorController:1,
  superFileEditorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorController} */
AbstractFileEditorController.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorController} */
function AbstractFileEditorControllerClass(){}


AbstractFileEditorController[$implementations]=[
 AbstractFileEditorControllerClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorController}*/({
  resetPort(){
   /** @type {!xyz.swapee.wc.IFileEditorPort}*/
   (/** @type {engineering.type.mvc.IIntegratedController}*/(this).port).resetPort()
  },
 }),
 __AbstractFileEditorController,
 /**@type {!xyz.swapee.wc.IFileEditorController}*/({
  calibrate:[
    /**@this {!xyz.swapee.wc.IFileEditorController}*/ function calibrateReadFile({readFile:readFile}){
     if(!readFile) return
     setTimeout(()=>{
      const{asIFileEditorController:{setInputs:setInputs}}=this
      setInputs({
       readFile:false,
      })
     },1)
    },
    /**@this {!xyz.swapee.wc.IFileEditorController}*/ function calibrateSaveFile({saveFile:saveFile}){
     if(!saveFile) return
     setTimeout(()=>{
      const{asIFileEditorController:{setInputs:setInputs}}=this
      setInputs({
       saveFile:false,
      })
     },1)
    },
  ],
 }),
 FileEditorBuffer,
 IntegratedController,
 /**@type {!AbstractFileEditorController}*/(FileEditorPortConnector),
]


export default AbstractFileEditorController