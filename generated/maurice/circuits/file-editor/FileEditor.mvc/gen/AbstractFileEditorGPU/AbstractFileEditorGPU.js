import AbstractFileEditorDisplay from '../AbstractFileEditorDisplayBack'
import {BrowserView} from '@webcircuits/webcircuits'
import {FileEditorVdusPQs} from '../../pqs/FileEditorVdusPQs'
import {FileEditorVdusQPs} from '../../pqs/FileEditorVdusQPs'
import {FileEditorMemoryPQs} from '../../pqs/FileEditorMemoryPQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorGPU}
 */
function __AbstractFileEditorGPU() {}
__AbstractFileEditorGPU.prototype = /** @type {!_AbstractFileEditorGPU} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorGPU}
 */
class _AbstractFileEditorGPU { }
/**
 * Handles the periphery of the _IFileEditorDisplay_, including the display and its VDUs.
 * @extends {xyz.swapee.wc.AbstractFileEditorGPU} ‎
 */
class AbstractFileEditorGPU extends newAbstract(
 _AbstractFileEditorGPU,996197313618,null,{
  asIFileEditorGPU:1,
  superFileEditorGPU:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorGPU} */
AbstractFileEditorGPU.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorGPU} */
function AbstractFileEditorGPUClass(){}

export default AbstractFileEditorGPU


AbstractFileEditorGPU[$implementations]=[
 __AbstractFileEditorGPU,
 AbstractFileEditorGPUClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorGPU}*/({
  vdusPQs:FileEditorVdusPQs,
  vdusQPs:FileEditorVdusQPs,
  memoryPQs:FileEditorMemoryPQs,
 }),
 AbstractFileEditorDisplay,
 BrowserView,
]