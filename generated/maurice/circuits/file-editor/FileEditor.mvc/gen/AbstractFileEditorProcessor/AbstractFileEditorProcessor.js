import {preadaptLoadReadFile,preadaptLoadSaveFile} from '../AbstractFileEditorRadio/preadapters'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorProcessor}
 */
function __AbstractFileEditorProcessor() {}
__AbstractFileEditorProcessor.prototype = /** @type {!_AbstractFileEditorProcessor} */ ({
  /** @return {void} */
  pulseReadFile() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     readFile:true,
    })
  },
  /** @return {void} */
  pulseSaveFile() {
    const{
     asIIntegratedController:{setInputs:setInputs},
    }=this
    setInputs({
     saveFile:true,
    })
  },
})
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorProcessor}
 */
class _AbstractFileEditorProcessor { }
/**
 * The processor to compute changes to the memory for the _IFileEditor_.
 * @extends {xyz.swapee.wc.AbstractFileEditorProcessor} ‎
 */
class AbstractFileEditorProcessor extends newAbstract(
 _AbstractFileEditorProcessor,99619731369,null,{
  asIFileEditorProcessor:1,
  superFileEditorProcessor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorProcessor} */
AbstractFileEditorProcessor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorProcessor} */
function AbstractFileEditorProcessorClass(){}

export default AbstractFileEditorProcessor


AbstractFileEditorProcessor[$implementations]=[
 __AbstractFileEditorProcessor,
 AbstractFileEditorProcessorClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorProcessor}*/({
  loadReadFile(){
   return preadaptLoadReadFile.call(this,this.model).then(this.setInfo)
  },
  loadSaveFile(){
   return preadaptLoadSaveFile.call(this,this.model).then(this.setInfo)
  },
 }),
]