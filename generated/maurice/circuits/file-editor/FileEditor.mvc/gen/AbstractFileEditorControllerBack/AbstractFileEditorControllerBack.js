import AbstractFileEditorControllerAR from '../AbstractFileEditorControllerAR'
import {AbstractFileEditorController} from '../AbstractFileEditorController'
import {DriverBack} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorControllerBack}
 */
function __AbstractFileEditorControllerBack() {}
__AbstractFileEditorControllerBack.prototype = /** @type {!_AbstractFileEditorControllerBack} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileEditorController}
 */
class _AbstractFileEditorControllerBack { }
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.AbstractFileEditorController} ‎
 */
class AbstractFileEditorControllerBack extends newAbstract(
 _AbstractFileEditorControllerBack,996197313624,null,{
  asIFileEditorController:1,
  superFileEditorController:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorController} */
AbstractFileEditorControllerBack.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorController} */
function AbstractFileEditorControllerBackClass(){}

export default AbstractFileEditorControllerBack


AbstractFileEditorControllerBack[$implementations]=[
 __AbstractFileEditorControllerBack,
 AbstractFileEditorController,
 AbstractFileEditorControllerAR,
 DriverBack,
]