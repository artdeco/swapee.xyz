import {GraphicsDriverBack} from '@webcircuits/back'
import { newAbstract, $implementations, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorDisplay}
 */
function __AbstractFileEditorDisplay() {}
__AbstractFileEditorDisplay.prototype = /** @type {!_AbstractFileEditorDisplay} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileEditorDisplay}
 */
class _AbstractFileEditorDisplay { }
/**
 * The backend interface for the display.
 * @extends {xyz.swapee.wc.back.AbstractFileEditorDisplay} ‎
 */
class AbstractFileEditorDisplay extends newAbstract(
 _AbstractFileEditorDisplay,996197313621,null,{
  asIFileEditorDisplay:1,
  superFileEditorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorDisplay} */
AbstractFileEditorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorDisplay} */
function AbstractFileEditorDisplayClass(){}

export default AbstractFileEditorDisplay


AbstractFileEditorDisplay[$implementations]=[
 __AbstractFileEditorDisplay,
 GraphicsDriverBack,
 AbstractFileEditorDisplayClass.prototype=/**@type {!xyz.swapee.wc.back.IFileEditorDisplay}*/({
  constructor:function mockTwinsConstructor(){
   const{asIGraphicsDriverBack:{twinMock:twinMock}}=this
   Object.assign(this,/**@type {!xyz.swapee.wc.back.IFileEditorDisplay}*/({
    Editor:twinMock,
   }))
  },
 }),
 /** @type {!xyz.swapee.wc.back.FileEditorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.back.IFileEditorDisplay.Initialese}*/({
   Editor:1,
  }),
  initializer({
   Editor:_Editor,
  }) {
   if(_Editor!==undefined) this.Editor=_Editor
  },
 }),
]