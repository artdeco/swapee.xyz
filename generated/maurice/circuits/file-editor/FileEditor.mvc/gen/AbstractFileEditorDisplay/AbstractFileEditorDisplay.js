import {Display,getDataIds} from '@webcircuits/front'
import { newAbstract, $implementations, scheduleFirst, $initialese } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorDisplay}
 */
function __AbstractFileEditorDisplay() {}
__AbstractFileEditorDisplay.prototype = /** @type {!_AbstractFileEditorDisplay} */ ({ })
/** @this {xyz.swapee.wc.FileEditorDisplay} */ function FileEditorDisplayConstructor() {
  /** @type {HTMLDivElement} */ this.Editor=null
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorDisplay}
 */
class _AbstractFileEditorDisplay { }
/**
 * Display for presenting information from the _IFileEditor_.
 * @extends {xyz.swapee.wc.AbstractFileEditorDisplay} ‎
 */
class AbstractFileEditorDisplay extends newAbstract(
 _AbstractFileEditorDisplay,996197313619,FileEditorDisplayConstructor,{
  asIFileEditorDisplay:1,
  superFileEditorDisplay:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorDisplay} */
AbstractFileEditorDisplay.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorDisplay} */
function AbstractFileEditorDisplayClass(){}

export default AbstractFileEditorDisplay


AbstractFileEditorDisplay[$implementations]=[
 __AbstractFileEditorDisplay,
 Display,
 AbstractFileEditorDisplayClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorDisplay}*/({
  constructor(){
   scheduleFirst(this,()=>{
    const{queries:{}}=this
    this.scan({
    })
   })
  },
  scan:function vduScan(){
   const{element:element,asIFileEditorScreen:{vdusPQs:{
    Editor:Editor,
   }}}=this
   const children=getDataIds(element)
   Object.assign(this,{
    Editor:/**@type {HTMLDivElement}*/(children[Editor]),
   })
  },
 }),
 /** @type {!xyz.swapee.wc.FileEditorDisplay} */ ({
  [$initialese]:/**@type {!xyz.swapee.wc.IFileEditorDisplay.Initialese}*/({
   Editor:1,
  }),
  initializer({
   Editor:_Editor,
  }) {
   if(_Editor!==undefined) this.Editor=_Editor
  },
 }),
]