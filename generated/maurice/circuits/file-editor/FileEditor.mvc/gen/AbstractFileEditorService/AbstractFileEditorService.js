import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorService}
 */
function __AbstractFileEditorService() {}
__AbstractFileEditorService.prototype = /** @type {!_AbstractFileEditorService} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorService}
 */
class _AbstractFileEditorService { }
/**
 * A service for the IFileEditor.
 * @extends {xyz.swapee.wc.AbstractFileEditorService} ‎
 */
class AbstractFileEditorService extends newAbstract(
 _AbstractFileEditorService,996197313617,null,{
  asIFileEditorService:1,
  superFileEditorService:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorService} */
AbstractFileEditorService.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorService} */
function AbstractFileEditorServiceClass(){}

export default AbstractFileEditorService


AbstractFileEditorService[$implementations]=[
 __AbstractFileEditorService,
]

export {AbstractFileEditorService}