import {mountPins} from '@webcircuits/webcircuits'
import {FileEditorMemoryPQs} from '../../pqs/FileEditorMemoryPQs'
import {FileEditorCachePQs} from '../../pqs/FileEditorCachePQs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorCore}
 */
function __FileEditorCore() {}
__FileEditorCore.prototype = /** @type {!_FileEditorCore} */ ({ })
/** @this {xyz.swapee.wc.FileEditorCore} */ function FileEditorCoreConstructor() {
  /**@type {!xyz.swapee.wc.IFileEditorCore.Model}*/
  this.model={
    loadingReadFile: false,
    hasMoreReadFile: null,
    loadReadFileError: null,
    loadingSaveFile: false,
    hasMoreSaveFile: null,
    loadSaveFileError: null,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorCore}
 */
class _FileEditorCore { }
/**
 * Stores data in memory registers.
 * @extends {xyz.swapee.wc.AbstractFileEditorCore} ‎
 */
class FileEditorCore extends newAbstract(
 _FileEditorCore,99619731368,FileEditorCoreConstructor,{
  asIFileEditorCore:1,
  superFileEditorCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorCore} */
FileEditorCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorCore} */
function FileEditorCoreClass(){}

export default FileEditorCore

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_FileEditorOuterCore}
 */
function __FileEditorOuterCore() {}
__FileEditorOuterCore.prototype = /** @type {!_FileEditorOuterCore} */ ({ })
/** @this {xyz.swapee.wc.FileEditorOuterCore} */
export function FileEditorOuterCoreConstructor() {
  /**@type {!xyz.swapee.wc.IFileEditorOuterCore.Model}*/
  this.model={
    host: '',
    content: null,
    path: '',
    filename: '',
    lang: '',
    hasChanges: false,
    readFile: false,
    saveFile: false,
  }
}
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditorOuterCore}
 */
class _FileEditorOuterCore { }
/**
 * The _IFileEditor_'s outer core that is reflected in the port.
 * @extends {xyz.swapee.wc.AbstractFileEditorOuterCore} ‎
 */
export class FileEditorOuterCore extends newAbstract(
 _FileEditorOuterCore,99619731363,FileEditorOuterCoreConstructor,{
  asIFileEditorOuterCore:1,
  superFileEditorOuterCore:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditorOuterCore} */
FileEditorOuterCore.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorOuterCore} */
function FileEditorOuterCoreClass(){}


FileEditorOuterCore[$implementations]=[
 __FileEditorOuterCore,
 FileEditorOuterCoreClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorOuterCore}*/({
  constructor(){
   mountPins(this.model,'',FileEditorMemoryPQs)
   mountPins(this.model,'',FileEditorCachePQs)
  },
 }),
]

FileEditorCore[$implementations]=[
 FileEditorCoreClass.prototype=/**@type {!xyz.swapee.wc.IFileEditorCore}*/({
  resetCore(){
   this.resetFileEditorCore()
  },
  resetFileEditorCore(){
   FileEditorCoreConstructor.call(
    /**@type {xyz.swapee.wc.FileEditorCore}*/(this),
   )
   FileEditorOuterCoreConstructor.call(
    /**@type {!xyz.swapee.wc.FileEditorOuterCore}*/(
     /**@type {!xyz.swapee.wc.IFileEditorOuterCore}*/(this)),
   )
  },
 }),
 __FileEditorCore,
 FileEditorOuterCore,
]

export {FileEditorCore}