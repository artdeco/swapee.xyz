import {UartUniversal} from '@webcircuits/back'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditorScreenAT}
 */
function __AbstractFileEditorScreenAT() {}
__AbstractFileEditorScreenAT.prototype = /** @type {!_AbstractFileEditorScreenAT} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.back.AbstractFileEditorScreenAT}
 */
class _AbstractFileEditorScreenAT { }
/**
 * A trait to asynchronously transmit commands from a frontend system to a backend
 * screen that installed an _IFileEditorScreenAR_ trait.
 * @extends {xyz.swapee.wc.back.AbstractFileEditorScreenAT} ‎
 */
class AbstractFileEditorScreenAT extends newAbstract(
 _AbstractFileEditorScreenAT,996197313630,null,{
  asIFileEditorScreenAT:1,
  superFileEditorScreenAT:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreenAT} */
AbstractFileEditorScreenAT.class=function(){}
/** @type {typeof xyz.swapee.wc.back.AbstractFileEditorScreenAT} */
function AbstractFileEditorScreenATClass(){}

export default AbstractFileEditorScreenAT


AbstractFileEditorScreenAT[$implementations]=[
 __AbstractFileEditorScreenAT,
 UartUniversal,
 AbstractFileEditorScreenATClass.prototype=/**@type {!xyz.swapee.wc.back.IFileEditorScreenAT}*/({
  syncConfirm(){
   const{asIFileEditorScreenAT:{uart:uart}}=this
   uart.t('inv',{mid:'003b7',args:[...arguments]})
  },
 }),
]