import {makeBuffers} from '@webcircuits/webcircuits'

export const regulateFileEditorCache=makeBuffers({
 loadingReadFile:Boolean,
 hasMoreReadFile:Boolean,
 loadReadFileError:5,
 loadingSaveFile:Boolean,
 hasMoreSaveFile:Boolean,
 loadSaveFileError:5,
},{silent:true})