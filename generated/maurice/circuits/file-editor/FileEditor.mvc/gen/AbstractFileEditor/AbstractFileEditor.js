import AbstractFileEditorProcessor from '../AbstractFileEditorProcessor'
import {FileEditorCore} from '../FileEditorCore'
import {IntegratedComponent} from '@mauriceguest/guest2'
import {AbstractFileEditorComputer} from '../AbstractFileEditorComputer'
import {AbstractFileEditorController} from '../AbstractFileEditorController'
import {regulateFileEditorCache} from './methods/regulateFileEditorCache'
import {FileEditorCacheQPs} from '../../pqs/FileEditorCacheQPs'
import { newAbstract, $implementations } from '@type.engineering/type-engineer'

/**
 * @suppress {checkTypes}
 * @constructor
 * @extends {_AbstractFileEditor}
 */
function __AbstractFileEditor() {}
__AbstractFileEditor.prototype = /** @type {!_AbstractFileEditor} */ ({ })
/**
 * @abstract
 * @extends {xyz.swapee.wc.AbstractFileEditor}
 */
class _AbstractFileEditor { }
/**
 * A component description.
 * @extends {xyz.swapee.wc.AbstractFileEditor} ‎
 */
class AbstractFileEditor extends newAbstract(
 _AbstractFileEditor,996197313610,null,{
  asIFileEditor:1,
  superFileEditor:2,
 },false) {}

/** @type {typeof xyz.swapee.wc.AbstractFileEditor} */
AbstractFileEditor.class=function(){}
/** @type {typeof xyz.swapee.wc.AbstractFileEditor} */
function AbstractFileEditorClass(){}

export default AbstractFileEditor


AbstractFileEditor[$implementations]=[
 __AbstractFileEditor,
 FileEditorCore,
 AbstractFileEditorProcessor,
 IntegratedComponent,
 AbstractFileEditorComputer,
 AbstractFileEditorController,
 AbstractFileEditorClass.prototype=/**@type {!xyz.swapee.wc.IFileEditor}*/({
  regulateState:regulateFileEditorCache,
  stateQPs:FileEditorCacheQPs,
 }),
]


export {AbstractFileEditor}