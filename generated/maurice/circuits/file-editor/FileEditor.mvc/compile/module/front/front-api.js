import { FileEditorDisplay, FileEditorScreen } from './front-exports'

/** @lazy @api {xyz.swapee.wc.FileEditorDisplay} */
export { FileEditorDisplay }
/** @lazy @api {xyz.swapee.wc.FileEditorScreen} */
export { FileEditorScreen }