import { AbstractFileEditor, FileEditorPort, AbstractFileEditorController,
 FileEditorElement, FileEditorBuffer, AbstractFileEditorComputer,
 FileEditorProcessor, FileEditorController } from './element-exports'

/** @lazy @api {xyz.swapee.wc.AbstractFileEditor} */
export { AbstractFileEditor }
/** @lazy @api {xyz.swapee.wc.FileEditorPort} */
export { FileEditorPort }
/** @lazy @api {xyz.swapee.wc.AbstractFileEditorController} */
export { AbstractFileEditorController }
/** @lazy @api {xyz.swapee.wc.FileEditorElement} */
export { FileEditorElement }
/** @lazy @api {xyz.swapee.wc.FileEditorBuffer} */
export { FileEditorBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractFileEditorComputer} */
export { AbstractFileEditorComputer }
/** @lazy @api {xyz.swapee.wc.FileEditorProcessor} */
export { FileEditorProcessor }
/** @lazy @api {xyz.swapee.wc.FileEditorController} */
export { FileEditorController }