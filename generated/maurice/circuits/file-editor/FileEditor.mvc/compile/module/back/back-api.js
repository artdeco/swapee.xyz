import { AbstractFileEditor, FileEditorPort, AbstractFileEditorController,
 FileEditorHtmlComponent, FileEditorBuffer, AbstractFileEditorComputer,
 FileEditorProcessor, FileEditorController } from './back-exports'

/** @lazy @api {xyz.swapee.wc.AbstractFileEditor} */
export { AbstractFileEditor }
/** @lazy @api {xyz.swapee.wc.FileEditorPort} */
export { FileEditorPort }
/** @lazy @api {xyz.swapee.wc.AbstractFileEditorController} */
export { AbstractFileEditorController }
/** @lazy @api {xyz.swapee.wc.FileEditorHtmlComponent} */
export { FileEditorHtmlComponent }
/** @lazy @api {xyz.swapee.wc.FileEditorBuffer} */
export { FileEditorBuffer }
/** @lazy @api {xyz.swapee.wc.AbstractFileEditorComputer} */
export { AbstractFileEditorComputer }
/** @lazy @api {xyz.swapee.wc.FileEditorProcessor} */
export { FileEditorProcessor }
/** @lazy @api {xyz.swapee.wc.back.FileEditorController} */
export { FileEditorController }