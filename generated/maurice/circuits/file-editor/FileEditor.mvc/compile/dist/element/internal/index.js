import Module from './element'

/**@extends {xyz.swapee.wc.AbstractFileEditor}*/
export class AbstractFileEditor extends Module['99619731361'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileEditor} */
AbstractFileEditor.class=function(){}
/** @type {typeof xyz.swapee.wc.FileEditorPort} */
export const FileEditorPort=Module['99619731363']
/**@extends {xyz.swapee.wc.AbstractFileEditorController}*/
export class AbstractFileEditorController extends Module['99619731364'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorController} */
AbstractFileEditorController.class=function(){}
/** @type {typeof xyz.swapee.wc.FileEditorElement} */
export const FileEditorElement=Module['99619731368']
/** @type {typeof xyz.swapee.wc.FileEditorBuffer} */
export const FileEditorBuffer=Module['996197313611']
/**@extends {xyz.swapee.wc.AbstractFileEditorComputer}*/
export class AbstractFileEditorComputer extends Module['996197313630'] {}
/** @type {typeof xyz.swapee.wc.AbstractFileEditorComputer} */
AbstractFileEditorComputer.class=function(){}
/** @type {typeof xyz.swapee.wc.FileEditorProcessor} */
export const FileEditorProcessor=Module['996197313651']
/** @type {typeof xyz.swapee.wc.FileEditorController} */
export const FileEditorController=Module['996197313661']