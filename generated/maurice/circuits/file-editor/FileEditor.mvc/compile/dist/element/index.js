/**
 * An abstract class of `xyz.swapee.wc.IFileEditor` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditor}
 */
class AbstractFileEditor extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IFileEditor_, providing input
 * pins.
 * @extends {xyz.swapee.wc.FileEditorPort}
 */
class FileEditorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorController` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditorController}
 */
class AbstractFileEditorController extends (class {/* lazy-loaded */}) {}
/**
 * A component description.
 *
 * The _IFileEditor_'s element for server-side rendering with support for
 * back-end code for regulators/calibrators/adapters.
 * @extends {xyz.swapee.wc.FileEditorElement}
 */
class FileEditorElement extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.FileEditorBuffer}
 */
class FileEditorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditorComputer}
 */
class AbstractFileEditorComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IFileEditor_.
 * @extends {xyz.swapee.wc.FileEditorProcessor}
 */
class FileEditorProcessor extends (class {/* lazy-loaded */}) {}
/**
 * Handles input pins, and contains interrupt service routines for calibration,
 * inversion and rectification.
 * @extends {xyz.swapee.wc.FileEditorController}
 */
class FileEditorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractFileEditor = AbstractFileEditor
module.exports.FileEditorPort = FileEditorPort
module.exports.AbstractFileEditorController = AbstractFileEditorController
module.exports.FileEditorElement = FileEditorElement
module.exports.FileEditorBuffer = FileEditorBuffer
module.exports.AbstractFileEditorComputer = AbstractFileEditorComputer
module.exports.FileEditorProcessor = FileEditorProcessor
module.exports.FileEditorController = FileEditorController

Object.defineProperties(module.exports, {
 'AbstractFileEditor': {get: () => require('./precompile/internal')[99619731361]},
 [99619731361]: {get: () => module.exports['AbstractFileEditor']},
 'FileEditorPort': {get: () => require('./precompile/internal')[99619731363]},
 [99619731363]: {get: () => module.exports['FileEditorPort']},
 'AbstractFileEditorController': {get: () => require('./precompile/internal')[99619731364]},
 [99619731364]: {get: () => module.exports['AbstractFileEditorController']},
 'FileEditorElement': {get: () => require('./precompile/internal')[99619731368]},
 [99619731368]: {get: () => module.exports['FileEditorElement']},
 'FileEditorBuffer': {get: () => require('./precompile/internal')[996197313611]},
 [996197313611]: {get: () => module.exports['FileEditorBuffer']},
 'AbstractFileEditorComputer': {get: () => require('./precompile/internal')[996197313630]},
 [996197313630]: {get: () => module.exports['AbstractFileEditorComputer']},
 'FileEditorProcessor': {get: () => require('./precompile/internal')[996197313651]},
 [996197313651]: {get: () => module.exports['FileEditorProcessor']},
 'FileEditorController': {get: () => require('./precompile/internal')[996197313661]},
 [996197313661]: {get: () => module.exports['FileEditorController']},
})