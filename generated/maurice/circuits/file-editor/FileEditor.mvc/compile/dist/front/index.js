/**
 * Display for presenting information from the _IFileEditor_.
 * @extends {xyz.swapee.wc.FileEditorDisplay}
 */
class FileEditorDisplay extends (class {/* lazy-loaded */}) {}
/**
 * The front-end screen for communicating with the circuit's back-end port,
 * presenting data on a display.
 * @extends {xyz.swapee.wc.FileEditorScreen}
 */
class FileEditorScreen extends (class {/* lazy-loaded */}) {}

module.exports.FileEditorDisplay = FileEditorDisplay
module.exports.FileEditorScreen = FileEditorScreen