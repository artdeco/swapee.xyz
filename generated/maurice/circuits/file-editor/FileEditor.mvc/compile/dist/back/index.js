/**
 * An abstract class of `xyz.swapee.wc.IFileEditor` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditor}
 */
class AbstractFileEditor extends (class {/* lazy-loaded */}) {}
/**
 * The port that serves as an interface to the _IFileEditor_, providing input
 * pins.
 * @extends {xyz.swapee.wc.FileEditorPort}
 */
class FileEditorPort extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorController` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditorController}
 */
class AbstractFileEditorController extends (class {/* lazy-loaded */}) {}
/**
 * The _IFileEditor_'s HTML component for the web page.
 * @extends {xyz.swapee.wc.FileEditorHtmlComponent}
 */
class FileEditorHtmlComponent extends (class {/* lazy-loaded */}) {}
/**
 * The special type for port controller buffers to export them as modlets.
 * @extends {xyz.swapee.wc.FileEditorBuffer}
 */
class FileEditorBuffer extends (class {/* lazy-loaded */}) {}
/**
 * An abstract class of `xyz.swapee.wc.IFileEditorComputer` interface.
 * @extends {xyz.swapee.wc.AbstractFileEditorComputer}
 */
class AbstractFileEditorComputer extends (class {/* lazy-loaded */}) {}
/**
 * The processor to compute changes to the memory for the _IFileEditor_.
 * @extends {xyz.swapee.wc.FileEditorProcessor}
 */
class FileEditorProcessor extends (class {/* lazy-loaded */}) {}
/**
 * The back-end controller with a UART driver to receive commands from the front.
 * @extends {xyz.swapee.wc.back.FileEditorController}
 */
class FileEditorController extends (class {/* lazy-loaded */}) {}

module.exports.AbstractFileEditor = AbstractFileEditor
module.exports.FileEditorPort = FileEditorPort
module.exports.AbstractFileEditorController = AbstractFileEditorController
module.exports.FileEditorHtmlComponent = FileEditorHtmlComponent
module.exports.FileEditorBuffer = FileEditorBuffer
module.exports.AbstractFileEditorComputer = AbstractFileEditorComputer
module.exports.FileEditorProcessor = FileEditorProcessor
module.exports.FileEditorController = FileEditorController