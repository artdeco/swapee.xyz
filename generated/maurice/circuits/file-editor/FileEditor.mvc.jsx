/** @extends {xyz.swapee.wc.AbstractFileEditor} */
export default class AbstractFileEditor extends (<mvc ns={xyz.swapee.wc}>
 A component description.
</mvc>) { }
// </class-end>
// export const records = (<records>
//
// </records>)

/** @extends {xyz.swapee.wc.AbstractFileEditorComputer} */
export class AbstractFileEditorComputer extends (<computer>
</computer>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileEditorController} */
export class AbstractFileEditorController extends (<controller>
  Buffers inputs at least.
</controller>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractFileEditorPort} */
export class FileEditorPort extends (<ic-port>
  <inputs>
  </inputs>
</ic-port>) { }
// </class-end>

/** @extends {xyz.swapee.wc.AbstractFileEditorView} */
export class AbstractFileEditorView extends (<view>
  <classes>

  </classes>

</view>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileEditorElement} */
export class AbstractFileEditorElement extends (<element v3 html mv>
 <block src="./FileEditor.mvc/src/FileEditorElement/methods/render.jsx" />
 <inducer src="./FileEditor.mvc/src/FileEditorElement/methods/inducer.jsx" />
</element>) { }
// </class-end>
/** @extends {xyz.swapee.wc.AbstractFileEditorHtmlComponent} */
export class AbstractFileEditorHtmlComponent extends (<html-ic>
</html-ic>) { }
// </class-end>