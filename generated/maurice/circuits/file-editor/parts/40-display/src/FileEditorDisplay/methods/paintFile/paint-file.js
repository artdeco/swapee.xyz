import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IFileEditorDisplay._paintFile} */
export default function paintFile({content:content,lang:lang}) {
 const{asIFileEditorScreen:{editor:editor,model,monacoEditor}}=this
 editor.setValue(content)
 monacoEditor['setModelLanguage'](model,lang)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS1lZGl0b3IvZmlsZS1lZGl0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTRLRyxTQUFTLFNBQVMsRUFBRSxlQUFlLENBQUMsU0FBUyxDQUFDO0NBQzdDLE1BQU0scUJBQXFCLGFBQWEsQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHO0NBQzlELE1BQU0sQ0FBQyxRQUFRLENBQUM7Q0FDaEIsZ0NBQWdDLENBQUMsS0FBSyxDQUFDO0FBQ3hDLENBQUYifQ==