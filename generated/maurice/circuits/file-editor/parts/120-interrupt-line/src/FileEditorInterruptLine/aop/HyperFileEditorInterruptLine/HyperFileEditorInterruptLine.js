import AbstractHyperFileEditorInterruptLine from '../../../../gen/AbstractFileEditorInterruptLine/hyper/AbstractHyperFileEditorInterruptLine'
import FileEditorInterruptLine from '../../FileEditorInterruptLine'
import FileEditorInterruptLineGeneralAspects from '../FileEditorInterruptLineGeneralAspects'

/**@extends {xyz.swapee.wc.HyperFileEditorInterruptLine} */
export default class extends AbstractHyperFileEditorInterruptLine
 .consults(
  FileEditorInterruptLineGeneralAspects,
 )
 .implements(
  FileEditorInterruptLine,
 )
{}