import '@type.engineering/type-engineer'
/**@type {xyz.swapee.wc.IFileEditorScreen._syncConfirm} */
export default function syncConfirm(message,data) {
 if(!message) return

 const c=confirm(message)
 if(!c) return
 this.setInputs(data)
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS1lZGl0b3IvZmlsZS1lZGl0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQTZIRyxTQUFTLFdBQVcsQ0FBQyxPQUFPLENBQUM7Q0FDNUIsRUFBRSxDQUFDLFVBQVU7O0NBRWIsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDO0NBQ2hCLEVBQUUsQ0FBQyxJQUFJO0NBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQztBQUNoQixDQUFGIn0=