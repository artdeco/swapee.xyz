import '@type.engineering/type-engineer'
import {scheduleLast} from '@type.engineering/type-engineer'

/**@type {xyz.swapee.wc.IFileEditorScreen._allocator} */
export default function allocator() {
 scheduleLast(this,()=>{
  const{asIFileEditorDisplay:{Editor:Editor}}=this
  const editor=window['monaco']['editor']['create'](Editor,{
   minimap:{enabled:false},
   lineNumbers: "on",
  })
  this.editor=editor
 })
 // this.editor=window['_editor']
 // var model = monacoeditor.getModel(); // we'll create a model for you if the editor created from string value.
 // monaco.editor.setModelLanguage(model, "javascript")
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS1lZGl0b3IvZmlsZS1lZGl0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBNkdHLFNBQVMsU0FBUyxDQUFDO0NBQ2xCLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztFQUNsQixNQUFNLHNCQUFzQixhQUFhLEdBQUc7RUFDNUMsTUFBTSxNQUFNLENBQUMsb0NBQW9DLENBQUMsTUFBTTtHQUN2RCxTQUFTLGFBQWE7R0FDdEIsYUFBYSxJQUFJO0FBQ3BCLEdBQUc7RUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDO0FBQ2QsRUFBRTtDQUNELEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztDQUNmLEdBQUcsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsR0FBRyxNQUFNLE9BQU8sRUFBRSxNQUFNLElBQUksSUFBSSxHQUFHLElBQUksT0FBTyxRQUFRLEtBQUssT0FBTyxLQUFLO0NBQy9HLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUU7QUFDMUMsQ0FBRiJ9