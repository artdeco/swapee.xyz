import '@type.engineering/type-engineer'
import {scheduleLast} from '@type.engineering/type-engineer'

/**@type {xyz.swapee.wc.IFileEditorScreen._constructor} */
export default function constructor() {
 scheduleLast(this,()=>{
  const{
   asIFileEditorDisplay:{Editor:Editor},asIFileEditorScreen:{setInputs},
  }=this

  const load=()=>{
   const editor=this.monacoEditor['create'](Editor,{
    minimap:{enabled:false},
    lineNumbers: "on",
   })
   this.editor=editor
   // window['monaco']['editor']
   const model=editor['getModel']()
   this.model=model
   // debugger
   editor['onDidChangeModelContent']((data)=>{
    // console.log('model change',data)
    setInputs({hasChanges:true})
   })
   // =(data)=>{
   //
   //  setInputs({hasChanges:true})
   // }
  }

  if(!('monaco' in window)) {
   // ok then
   window.addEventListener('load',load)
  }else{
   load()
  }
 })
 // this.editor=window['_editor']
 // var model = monacoeditor.getModel(); // we'll create a model for you if the editor created from string value.
 // monaco.editor.setModelLanguage(model, "javascript")
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL21hdXJpY2UvY2lyY3VpdHMvZmlsZS1lZGl0b3IvZmlsZS1lZGl0b3Iud2NrdCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBcUlHLFNBQVMsV0FBVyxDQUFDO0NBQ3BCLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztFQUNsQjtHQUNDLHNCQUFzQixhQUFhLEVBQUUscUJBQXFCLFNBQVM7SUFDbEU7O0VBRUYsTUFBTSxJQUFJLENBQUMsQ0FBQztHQUNYLE1BQU0sTUFBTSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxNQUFNO0lBQzlDLFNBQVMsYUFBYTtJQUN0QixhQUFhLElBQUk7QUFDckIsSUFBSTtHQUNELElBQUksQ0FBQyxNQUFNLENBQUM7R0FDWixHQUFHO0dBQ0gsTUFBTSxLQUFLLENBQUMsa0JBQWtCLENBQUM7R0FDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQztHQUNYLEdBQUc7R0FDSCxpQ0FBaUMsQ0FBQyxDQUFDO0lBQ2xDLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLE9BQU8sQ0FBQztJQUM5QixTQUFTLEVBQUUsZUFBZSxDQUFDO0FBQy9CLElBQUk7R0FDRCxFQUFFLEVBQUUsQ0FBQztHQUNMO0dBQ0EsSUFBSSxTQUFTLEVBQUUsZUFBZSxDQUFDO0dBQy9CLEVBQUU7QUFDTDs7RUFFRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRztHQUNoQixHQUFHLEdBQUc7R0FDTixNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDO0FBQ2xDLEdBQUc7R0FDQSxJQUFJLENBQUM7QUFDUjtBQUNBLEVBQUU7Q0FDRCxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7Q0FDZixHQUFHLElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLEdBQUcsTUFBTSxPQUFPLEVBQUUsTUFBTSxJQUFJLElBQUksR0FBRyxJQUFJLE9BQU8sUUFBUSxLQUFLLE9BQU8sS0FBSztDQUMvRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFO0FBQzFDLENBQUYifQ==