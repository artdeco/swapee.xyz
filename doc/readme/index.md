# @swapee.xyz/swapee.xyz

The Swapee Crypto Aggregator Website.

A new website made with [Maurice][1]: https://swapee.xyz.

## Copyright

(c) [Art Deco™][2] 2024

[1]: https://mauriceguest.com
[2]: https://artd.eco