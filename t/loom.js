// let i
let s = ''
let mobs=''

const fix=(s)=>{
 return s.replace(/0+$/,'').replace(/\.$/,'')
}

for (let i=0; i<=100; i++) {
 const del=(i/16)
 const rem=fix(del.toFixed(3))
 const coof = i<16?1.155:1.445
 //  const K=Math.ceil(*16)/16/coof
 const srem=fix((del/coof).toFixed(3))
 s+=`
.FS${i}, .FontSize${i} { font-size: ${rem}rem; }`
 mobs+=`
  .FS${i}, .FontSize${i} { font-size: ${srem}rem; }`
}

for (let i=1; i<10; i++) {
 const j=i*100
 s+=`
.FW${j}, .FontWeight${j} { font-weight: ${j}; }`
}

// const weights=[100,200,300,400,500,600,700]

console.log(s.trim())

console.log('@media(max-width:576px) {')
console.log(' ',mobs.trim())
console.log('}')